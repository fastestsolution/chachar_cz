<?php 
error_reporting(-1);
ini_set('display_errors', 'On');


// add css files list to compile
$files = [
	['path'=>'./css/','name'=>'small'],
	['path'=>'./css/','name'=>'default'],
	['path'=>'./css/','name'=>'response'],
	['path'=>'./css/','name'=>'mobile'],
	['path'=>'../plugins/Shop/webroot/css/','name'=>'shop'], // shop css
	['path'=>'../plugins/Shop/webroot/css/','name'=>'shop_response'], // shop css
];

// save log file
function saveLog($data){
	$file = './css/css_log.log';
	$current = file_get_contents($file);
	$current = json_encode($data);
	file_put_contents($file, $current);
}

// generate css files
function saveCss($files){
	require_once "../config/lessc.inc.php";
	 
	foreach ($files AS $file){
	
		$filename = $file['path'].$file['name'].'.less';
		$filename2 = './css/css_compile/'.$file['name'].'.css';
		
		$less = new lessc;
		$less->setFormatter("compressed");
		$less->compileFile($filename,$filename2);
		
	}
}

// get LESS timestamp from files
$save_log = array();
foreach ($files AS $file){
	
	$filename = $file['path'].$file['name'].'.less';
	
	if (file_exists($filename)) {
		$save_log[$file['name']] = filemtime($filename);
	}
}
$current_log = file_get_contents('./css/css_log.log');

//print_r($current_log);
//print_r($save_log);

// if current timestamp is different from save log	
if (!isset($current_log) || json_decode($current_log,true) != $save_log){
	
	saveLog($save_log);
	saveCss($files);
	if (!isset($_GET['render']))
	die(json_encode(['r'=>true,'m'=>'Vygenerovano '.count($files).' souboru']));

} else {
	
	if (!isset($_GET['render']))
	die(json_encode(['r'=>false,'m'=>'Neni co generovat']));
}

?>