<?php

if(isset($_SERVER) && in_array($_SERVER['REMOTE_ADDR'], array('89.103.18.65'))){
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
    error_reporting(E_ALL);
}

function print_empty(){
    $file = './css/layout/noimg.png';
    $type = 'image/png';
    header('Content-Type:'.$type);
    header('Content-Length: ' . filesize($file));
    readfile($file);
    die();
}    

function pr($string = null) {
    echo "<pre>";
    print_r($string);
    echo "</pre>";
}


class image_resize {

    var $image = null;
    var $cache = false;
    var $tmp_cache_path = './uploaded/product_temp/';
    function __construct($file, $w = 200, $h = 200, $bg = 'transparent') {

        //if($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
            $this->cache = true;  
        //}
        
        if (isset($_GET['amp;w'])) {
            $_GET['w'] = $_GET['amp;w'];
            $w = $_GET['amp;w'];
        }
        if (isset($_GET['amp;h'])) {
            $_GET['h'] = $_GET['amp;h'];
            $h = $_GET['amp;h'];
        }
        if (isset($_GET['amp;left'])) {
            $_GET['left'] = $_GET['amp;left'];
            $left = $_GET['amp;left'];
        }
        if (isset($_GET['amp;top'])) {
            $_GET['top'] = $_GET['amp;t'];
            $top = $_GET['amp;top'];
        }
        if (isset($_GET['amp;bg'])) {
            $_GET['bg'] = $_GET['amp;bg'];
            $bg = '#' . $_GET['amp;bg'];
        }
        if (isset($_GET['amp;type'])) {
            $_GET['type'] = $_GET['amp;type'];
            $type = $_GET['amp;type'];
        }
        if (isset($_GET['type'])) {
            $_GET['amp;type'] = $_GET['type'];
            $type = $_GET['type'];
        }
        if (isset($_GET['left'])) {
            $_GET['amp;left'] = $_GET['left'];
            $left = $_GET['left'];
        }
        if (isset($_GET['top'])) {
            $_GET['amp;top'] = $_GET['top'];
            $top = $_GET['top'];
        }
        //pr($_GET);

        $pos = strpos($file, 'http://');

        if ($pos !== false)
            $file = ltrim($file, '.');

        if (!isset($type))
            $type = '';
        if (!isset($left))
            $left = 100;
        if (!isset($top))
            $top = 0;


        $this->image = $file;
        $this->width = $w;
        $this->height = $h;
        $this->type = $type;
        $this->top = $top;
        $this->left = $left;
        $this->bg = $bg;
        $this->color = $this->hexa2rgb($bg);

        if ($pos === false) {
            if($this->cache && $this->load_cache()){
                $img_type = image_type_to_mime_type(exif_imagetype($this->image));
                header('Content-Type:'. $img_type);
                header('Content-Length: ' . filesize($this->image));
                readfile($this->image);
				//echo file_get_contents( $this->image );
            }else if (file_exists($file)) {
                if ($this->type == 'crop'){
                    $this->crop($this->top, $this->left, $this->width, $this->height);
                }else{
                    try{
                        $this->render();
                    }catch(Exception $e){
                        pr($e->getMesasge());
                    }
                }
            } else {
                $file = './css/layout/noimg.png';
                $this->image = $file;
                $this->render();
            }
        } else {

            if ($this->url_exists($file))
                $this->render();
            else
                die('Soubor nebyl nalezen');
        }
    }

    function generate_tmp_name(){
        $tmp_file_name = explode('/', $this->image);
        $file_name = end($tmp_file_name);
        $complete_tmp_name = $this->width. 'x'.$this->height . '_'. $file_name;
        return $complete_tmp_name;
    }
    function load_cache(){
        $complete_tmp_name = $this->generate_tmp_name();
      
        if(file_exists($this->tmp_cache_path . $complete_tmp_name)){
            $this->image = $this->tmp_cache_path . $complete_tmp_name;
            return true;
        }else{
            return false;
        }
    }
    
    function save_cache($file_content, $type = 'jpg'){
        $result = false;
        $complete_tmp_name = $this->generate_tmp_name();
        switch($type){
            case 'jpg':
                if(imagejpeg($file_content, $this->tmp_cache_path . $complete_tmp_name)){
                    $result = true;
                }
                break;
            case 'png':
                if(imagepng($file_content,  $this->tmp_cache_path . $complete_tmp_name)){
                    $result = true;
                }
                break;
            case 'gif':
                if(imagegif($file_content,  $this->tmp_cache_path . $complete_tmp_name)){
                    $result = true;
                }
                break;
        }
        return $result;
    }
    
    function url_exists($url) {
        // Version 4.x supported
        $handle = curl_init($url);
        if (false === $handle) {
            return false;
        }
        curl_setopt($handle, CURLOPT_HEADER, false);
        curl_setopt($handle, CURLOPT_FAILONERROR, true);  // this works
        curl_setopt($handle, CURLOPT_HTTPHEADER, Array("User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.15) Gecko/20080623 Firefox/2.0.0.15")); // request as if Firefox   
        curl_setopt($handle, CURLOPT_NOBODY, true);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, false);
        $connectable = curl_exec($handle);
        curl_close($handle);
        return $connectable;
    }

    function get_size() {
        return getimagesize($this->image);
    }

    function crop($top, $left, $w, $h) {

        $height = $h;
        $width = $w;

        if (empty($top))
            $top = 0;
        if (empty($left))
            $left = 0;

        $dest = imagecreatetruecolor($width, $height);
        $png = exif_imagetype($this->image);
        if ($png == 2)
            $image = imagecreatefromjpeg($this->image);

        if ($png == 3)
            $image = imagecreatefrompng($this->image);

        imagecopy($dest, $image, 0, 0, $left, $top, $width, $height);
        if($this->cache == true){
            $this->save_cache($dest, 'png');
        }
        header('Content-type: image/png');
        imagejpeg($dest, null, 100);
    }

    function calc_new_size($max_width, $max_height) {
        list($orig_width, $orig_height) = $this->get_size();
        if ($orig_width > $max_width || $orig_height > $max_height) {
            if ($orig_width > $orig_height) {
                $new_width = $max_width;
                $new_height = $new_width * $orig_height / $orig_width;
            } else {

                $new_height = $max_height;
                $new_width = $new_height * $orig_width / $orig_height;
            }
        } else {
            $new_width = $orig_width;
            $new_height = $orig_height;
        }

        return array($orig_width, $orig_height, $new_width, $new_height);
    }

    function hexa2rgb($color) {
        if ($color[0] == '#')
            $color = substr($color, 1);

        if (strlen($color) == 6)
            list($r, $g, $b) = array(
                $color[0] . $color[1],
                $color[2] . $color[3],
                $color[4] . $color[5]
            );
        elseif (strlen($color) == 3)
            list($r, $g, $b) = array($color[0] . $color[0], $color[1] . $color[1], $color[2] . $color[2]);
        else
            return false;

        $r = hexdec($r);
        $g = hexdec($g);
        $b = hexdec($b);

        return array($r, $g, $b);
    }

    function check_modify() {
        //get the last-modified-date of this very file
        $lastModified = filemtime(__FILE__);
        //get a unique hash of this file (etag)
        $etagFile = md5_file(__FILE__);
        //get the HTTP_IF_MODIFIED_SINCE header if set
        $ifModifiedSince = (isset($_SERVER['HTTP_IF_MODIFIED_SINCE']) ? $_SERVER['HTTP_IF_MODIFIED_SINCE'] : false);
        //get the HTTP_IF_NONE_MATCH header if set (etag: unique file hash)
        $etagHeader = (isset($_SERVER['HTTP_IF_NONE_MATCH']) ? trim($_SERVER['HTTP_IF_NONE_MATCH']) : false);

        //set last-modified header
        header("Last-Modified: " . gmdate("D, d M Y H:i:s", $lastModified) . " GMT");
        //set etag-header
        header("Etag: $etagFile");
        //make sure caching is turned on
        header('Cache-Control: public');

        //check if page has changed. If not, send 304 and exit
        if (@strtotime($_SERVER['HTTP_IF_MODIFIED_SINCE']) == $lastModified || $etagHeader == $etagFile) {
            header("HTTP/1.1 304 Not Modified");
            exit;
        }
    }

    function render() {
        $this->check_modify();
        list($orig_width, $orig_height, $new_width, $new_height) = $this->calc_new_size($this->width, $this->height);
             
        // Resample
        if ($this->width < $new_width) {
            $image_p = imagecreatetruecolor($this->width, $this->height);
        } else {
            $image_p = imagecreatetruecolor($new_width, $new_height);
        }
                
        if ($this->bg == 'transparent') {
            imagealphablending($image_p, false);
            imagesavealpha($image_p, true);
            # important part two
            $white = imagecolorallocatealpha($image_p, 255, 255, 255, 127);
            imagefill($image_p, 0, 0, $white);
            # do whatever you want with transparent image
            //$lime = imagecolorallocate($image_p, 204, 255, 51);
            //imagettftext($im, $font, 0, 0, $font - 3, $lime, "captcha.ttf", $string);
        }

        if (function_exists('exif_imagetype')) {
             $png = exif_imagetype($this->image);
        } else {
              print_empty();
        }
          
        if ($png == 1) {
            $image = imagecreatefromgif($this->image);
            $white = imagecolorallocate($image_p, $this->color[0], $this->color[1], $this->color[2]);
            imagefilledrectangle($image_p, 0, 0, $this->width, $this->height, $white);
            imagecopyresampled($image_p, $image, ($this->width / 2 - $new_width / 2), ($this->height / 2 - $new_height / 2), 0, 0, $new_width, $new_height, $orig_width, $orig_height);
            if($this->cache == true){
                $this->save_cache($image_p, 'gif');
            }
            header('Content-type: image/gif');
            // Output
            imagejpeg($image_p, null, 100);
        }
        if ($png == 2) {
            $image = imagecreatefromjpeg($this->image);
            $white = imagecolorallocate($image_p, $this->color[0], $this->color[1], $this->color[2]);
            imagefilledrectangle($image_p, 0, 0, $this->width, $this->height, $white);

            if ($this->width > $new_width) {

                imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $orig_width, $orig_height);
            } else {
                //imagecopyresampled($image_p, $image, ($this->width/2 - $new_width/2), ($this->height/2 - $new_height/2), 0, 0, $new_width, $new_height, $orig_width, $orig_height);
                imagecopyresampled($image_p, $image, ($this->width / 2 - $new_width / 2), 0, 0, 0, $new_width, $new_height, $orig_width, $orig_height);
            }
            if($this->cache == true){
                $this->save_cache($image_p, 'jpg');
            }
            header('Content-type: image/jpeg');
            // Output
            imagejpeg($image_p, null, 100);
        }
        /* if ($png == 3) {
          $image = imagecreatefrompng($this->image);
          //$white = imagecolorallocate($image_p, $this->color[0], $this->color[1], $this->color[2]);
          //imagefilledrectangle($image_p, 0, 0, $this->width, $this->height, $white);

          $im = @imagecreatetruecolor(100, 25);

          if ($this->width > $new_width) {

          imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $orig_width, $orig_height);
          } else {
          imagecopyresampled($image_p, $image, ($this->width / 2 - $new_width / 2), ($this->height / 2 - $new_height / 2), 0, 0, $new_width, $new_height, $orig_width, $orig_height);
          }
          header("Content-type: image/png");

          // Create a 55x30 image
          $red = imagecolorallocate($image_p, 255, 0, 0);
          $black = imagecolorallocate($image_p, 0, 0, 0);

          // Make the background transparent
          imagecolortransparent($image_p, $black);

          // Make the background transparent
          imagepng($image_p);
          } */
        if ($png == 3) {
            $image = imagecreatefrompng($this->image);
            imagecopyresampled($image_p, $image, 0, 0, 0, 0, $new_width, $new_height, $orig_width, $orig_height);
            if($this->cache == true){
                $this->save_cache($image_p, 'png');
            }
            header("Content-type: image/png");
            imagepng($image_p);
        }
    }

}

/* * * DECODE LONG URL * */

function decode_long_url($data) {
    $data2 = explode('?', $data);
    $data = $data2[0];
    return json_decode(gzuncompress(stripslashes(base64_decode(strtr($data, '-_,', '+/=')))), true);
}

if (isset($_GET['params'])) {
    $params = decode_long_url($_GET['params']);
    //print_r($params);
    //die();
    $w = $_GET['w'] = $params['w'];
    $h = $_GET['h'] = $params['h'];
    if(isset( $params['bg'])){
        $bg = $_GET['bg'] = $params['bg'];
    }
    if (isset($params['type']))
        $type = $_GET['type'] = $params['type'];
    if (isset($params['left']))
        $left = $_GET['left'] = $params['left'];
    if (isset($params['top']))
        $top = $_GET['top'] = $params['top'];
    $file = $_GET['file'] = $params['file'];
}

$ir = new image_resize($_GET['file'], isset($_GET['w']) ? $_GET['w'] : 200, isset($_GET['h']) ? $_GET['h'] : 200, isset($_GET['bg']) ? $_GET['bg'] : 'transparent');


?>