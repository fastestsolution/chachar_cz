/*
		<div id="map_canvas" class="load_google_map" data-load_data="map_data" data-load_db="1"></div>
		<div id="map_data" class="none">
		<?php 
		$mapData = [];
		$mapData[] = [
			'GoogleMap'=>[
				"name"=>"test",
				"ulice"=>"Pelclova",
				"mesto"=>"Ostrava",
				"psc"=>"70200",
				"lat"=>49.848168,
				"lng"=>18.285625
			],
		];
		echo json_encode($mapData);
		?>
		</div>
*/
var GoogleMap = this.FstPages = new Class({
	Implements:[Options,Events],
	
	initialize:function(options){
		$$('.load_google_map').each(function(el_inject){
			
			if (el_inject.get('data-load_data')){
				if (!($(el_inject.get('data-load_data')))){
					console.log('Neni ID s daty pro mapu');
					return false
				}
				json_data = JSON.decode($(el_inject.get('data-load_data')).get('text'));
			}
			
			el_inject.addClass('preloader');
			
			//var styleMap = this.styles_map();
			
			// render map
			function renderMap(data){
				el_inject.removeClass('preloader');
				new google_maps({
					map_data:data,
					scrollwheel: false,
					/*
					styles: styleMap,
					no_cr: true,
					cluster:true,
					marker_ico:'/path ico/',
					clusterStyles: [
						{textColor: 'white',url: 'path/to/smallclusterimage.png',height: 50,width: 50},
						{textColor: 'white',url: 'path/to/mediumclusterimage.png',height: 50,width: 50},
						{textColor: 'white',url: 'path/to/largeclusterimage.png',height: 50,width: 50}
					]
					*/
									
				});
			}
			
			function doneMap(){
				
				if (typeof json_data == 'undefined' && !el_inject.get('data-load_db')){
					console.log('Neni definovano data-load_data="" nebo data-load_db="" na elementu ',el_inject);
					return false;
				}
				// pokud je data z DB
				if (typeof json_data == 'undefined'){
							
					this.req_form = new Request.JSON({
						url:'/loadGoogleMap/'+el_inject.get('data-load_db'),
						onError: this.req_error = (function(data){
						
						}).bind(this),

						onComplete :(function(json){
							if (json.r == true){
								renderMap(json.data);
							}
						}).bind(this)
					});
					this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
					this.req_form.send();
				
				} else {
					renderMap(json_data);
				}
			}
			
			this.google_maps_api(doneMap);
			
		}.bind(this));
	},
	
	// styles map
	styles_map: function(){
		this.stylesMap = [
			{
				"featureType": "administrative",
				"elementType": "labels.text.fill",
				"stylers": [
					{
						"color": "#444444"
					}
				]
			},
			{
				"featureType": "landscape",
				"elementType": "all",
				"stylers": [
					{
						"color": "#f2f2f2"
					}
				]
			},
			{
				"featureType": "poi",
				"elementType": "all",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "road",
				"elementType": "all",
				"stylers": [
					{
						"saturation": -100
					},
					{
						"lightness": 45
					}
				]
			},
			{
				"featureType": "road.highway",
				"elementType": "all",
				"stylers": [
					{
						"visibility": "simplified"
					}
				]
			},
			{
				"featureType": "road.arterial",
				"elementType": "labels.icon",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "transit",
				"elementType": "all",
				"stylers": [
					{
						"visibility": "off"
					}
				]
			},
			{
				"featureType": "water",
				"elementType": "all",
				"stylers": [
					{
						"color": "#6fcaef"
					},
					{
						"visibility": "on"
					}
				]
			}
		];
		
		return this.stylesMap;
	},
	
	
	
	// load google maps api
	google_maps_api: function(complete){
		var script_googleMap = Asset.javascript('http://maps.google.com/maps/api/js?sensor=false', {
			id: 'script_googleMap',
			onLoad: function(){
				if (window.debug) console.log('script_googleMap');
				var script_googleMapFastest = Asset.javascript('http://scripts.fastesthost.cz/js/google_maps/google_maps_gps.js', {
					id: 'script_googleMapFastest',
					onLoad: function(){
						if (window.debug) console.log('script_googleMapFastest');
						complete();
						
					}
				});
			
			}
		});
	},
});

window.addEvent('domready',function(){
	GoogleMap = new GoogleMap();
	//console.log(this.Fst.test());
});