/*
Fst History
created Jakub Tyson Fastest Solution 
copyright 2016

#history_scroll_top scroll after pagination

*/
var FstHistory = this.FstHistory = new Class({
	Implements:[Options,Events],
	debug : false,
	element_refresh: 'fst_history',
	//complete_fce: null ,
	
	// init fce
	initialize:function(options){
		this.setOptions(options);
		//console.log($(this.element_refresh));
		
		if (this.options.complete_fce){
			this.complete_fce = this.options.complete_fce;
		}
		
		if (this.options.element_refresh){
			this.element_refresh = this.options.element_refresh;
		}
		
		if ($(this.element_refresh)){
			this.init_elements_events();
			this.load_from_hash();
		}
		
		
	},
	
	// add events to elements
	init_elements_events: function(){
		this.history_els = $$('.fst_h'); 
		
		if (this.history_els){
			this.history_els.each((function(item){
				
				// type A
				if (item.nodeName == 'A'){
					item.removeEvents('click');
					item.addEvent('click',this.element_change.bind(this));
				}
				
				// type INPUT
				if (item.nodeName == 'INPUT'){
					//console.log(item);
					item.removeEvents('change');
					item.addEvent('change',this.element_change.bind(this));
					
					
				}
				// type SELECT
				if (item.nodeName == 'SELECT'){
					
					item.removeEvents('change');
					item.addEvent('change',this.element_change.bind(this));
				}
				
			}).bind(this));
			//console.log(this.history_els);
		}
	},
	
	// remove Events from elements
	remove_elements_events: function(){
		if (this.history_els){
			this.history_els.removeEvents('click');
			this.history_els.removeEvents('change');
			
		}
	},
	
	
	// event click to element
	element_change: function(e){
		
		if (e.stop){
			e.stop();
		} else {
			e.target = e;
		}
		//console.log(e);
		
		this.store_params();
		
		if (e.target.get('href')){
			this.history_url = e.target.get('href');
		} else {
			var myURI = new URI(window.location);
			//console.log(myURI.parsed.directory);
			//console.log(myURI.parsed.file);
			this.history_url = myURI.parsed.directory+((myURI.parsed.file!='')?myURI.parsed.file:'')+'?'+this.filtr_params;
		}
		//console.log(this.history_url);
		this.dont_create_hash = false;
		this.history_request();
		
		
		//console.log(this.history_url);
	},
	
	// store params
	store_params: function(){
		
		var params = [];
		this.history_els.each(function(item){
			if (item.get('data-fparams') && item.value !=''){
				if(item.get('type') == 'checkbox'){
					if( item.get('checked') == true){
						params.push(item.get('data-fparams')+'='+item.value);
					}
				}else{
					params.push(item.get('data-fparams')+'='+item.value);
				}
				
				//params.push(item.get('data-fparams')+'='+item.value);
			}
		});
		this.filtr_params = params.join('&');
		//console.log(this.filtr_params);
		//console.log(params);
	},
	
	
	// history request
	history_request: function(){
		this.preloader();
		var history_reg = new Request.HTML({
			url:this.history_url,
			onComplete :(function(html){
				if ($(this.element_refresh)){
					this.preloader();
		
					this.remove_elements_events();
					
					$(this.element_refresh).empty();
					$(this.element_refresh).adopt(html);
					
					this.init_elements_events();
					if (!this.dont_create_hash){
						this.create_hash();
					}
					this.hightlight_last_id();
					
					if(typeof(this.complete_fce) == 'function'){
						this.complete_fce();
					}
					
					
					if ($('history_scroll_top')){
						var myFx = new Fx.Scroll(window).toElement($('history_scroll_top'), 'y');	
					}
					
				}
			}).bind(this)
		})
		history_reg.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
		history_reg.send();	
		
	},
	
	// hightlight last_id
	hightlight_last_id: function(){
		if (window.fsthistory.last_id){
			$$('.item-row').each(function(item){
				if (item.get('click-id') == window.fsthistory.last_id){
					item.addClass('selected');
				}
			});
		}
	},
	
	// create hash
	create_hash: function(){
		var params = this.history_url.split('?');
		//console.log(params[1]);
		if (params[1]){
			var hash = Base64.encode(JSON.encode(params[1]));
			//console.log(hash);
			window.history.pushState(null,null,params[0]+'?h='+hash);
		} else {
			window.history.pushState(null,null,params[0]);
		}
	},
	
	
	
	// check location url
	check_location_url: function(){
		if (!$('page_url')) console.log('neni page url');
		if ('http://'+window.location.host != 'http://'+window.location.hostname){
			if (window.location.origin != 'http://'+window.location.hostname && $('page_url').get('text') != window.location.origin){
				console.log('error location');
				return false;
			}
		}
		
		window.onpopstate = function () {
			this.load_from_hash();
		}.bind(this);
		
	},
	
	// load hash
	load_from_hash: function(){
		var myURI = new URI(window.location);
		this.check_location_url();	
		
		//console.log(window.location);
		if (myURI.parsed.query){
			var par = myURI.parsed.query.split('h=');
			
			if (par[1]){
				par = Base64.decode(par[1]);
				par = JSON.decode(par);
				
				this.conditions_from_hash(par);
				//console.log(par);
				this.history_url = myURI.parsed.directory+((myURI.parsed.file!='')?myURI.parsed.file:'')+'?'+par;
				
				this.history_request();
			
			}
			
		} else if (this.dont_create_hash){
			this.history_url = myURI.parsed.directory+((myURI.parsed.file!='')?myURI.parsed.file:'');
			
			this.history_request();
			
		}
		//console.log(this.history_url);
	},
	
	// reinit table after modal save
	reinit_history: function(){
		
		this.dont_create_hash = true;
		//console.log(window.fsthistory);
		//console.log(this.history_url);
		this.load_from_hash();		
	},
	
	// set conditions from hash
	conditions_from_hash: function(par){
		
		if (par){
			par_list = {}
			params = par.split('&');
			params.each(function(item){
				it = item.split('=');
				par_list[it[0]] = it[1];
			});
			//console.log(par_list);
			this.history_els.each(function(item){
				if (item.get('data-fparams')){
					Object.each(par_list,function(par_value,par_key){
						if (item.get('data-fparams') == par_key){
							item.value = par_value;
						}
					});
				}
				//console.log(item.get('data-fparams'));
			});
		
		}
	},
	
	// clear history
	clear_history: function(){
		this.history_els.each(function(item){
			item.value = '';
		});
		
		var myURI = new URI(window.location);
		this.history_url = myURI.parsed.directory+((myURI.parsed.file!='')?myURI.parsed.file:'');
			
		this.history_request();
	},
	
	preloader: function(){
		if (!$('history_preloader')){
			if ($(this.element_refresh)){
			var cor = $(this.element_refresh).getPosition();
			var cor2 = $(this.element_refresh).getComputedSize();
			//console.log(cor2);
			this.preload_el = new Element('div',{'id':'history_preloader'}).inject($(this.element_refresh),'after'); 
			this.preload_el.setStyles({
				//'top':cor.y - getScrollOffsets().y,
				//'left':cor.x,
				'height':cor2.totalHeight,
				'width':cor2.width,
			});
			cor = null;
			}
		} else {
			$('history_preloader').set('tween',{
				onComplete:function(){
					if ($('history_preloader'))
					$('history_preloader').destroy();
				}
			});
			$('history_preloader').fade(0);
			this.preload_el = null;
		}
	}
	
	
});

//})();	
window.addEvent('domready', function () {
	//window.fsthistory = new FstHistory();
	//console.log(window.fsthistory);
});


var Base64={_keyStr:"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=",encode:function(e){var t="";var n,r,i,s,o,u,a;var f=0;e=Base64._utf8_encode(e);while(f<e.length){n=e.charCodeAt(f++);r=e.charCodeAt(f++);i=e.charCodeAt(f++);s=n>>2;o=(n&3)<<4|r>>4;u=(r&15)<<2|i>>6;a=i&63;if(isNaN(r)){u=a=64}else if(isNaN(i)){a=64}t=t+this._keyStr.charAt(s)+this._keyStr.charAt(o)+this._keyStr.charAt(u)+this._keyStr.charAt(a)}return t},decode:function(e){var t="";var n,r,i;var s,o,u,a;var f=0;e=e.replace(/[^A-Za-z0-9\+\/\=]/g,"");while(f<e.length){s=this._keyStr.indexOf(e.charAt(f++));o=this._keyStr.indexOf(e.charAt(f++));u=this._keyStr.indexOf(e.charAt(f++));a=this._keyStr.indexOf(e.charAt(f++));n=s<<2|o>>4;r=(o&15)<<4|u>>2;i=(u&3)<<6|a;t=t+String.fromCharCode(n);if(u!=64){t=t+String.fromCharCode(r)}if(a!=64){t=t+String.fromCharCode(i)}}t=Base64._utf8_decode(t);return t},_utf8_encode:function(e){e=e.replace(/\r\n/g,"\n");var t="";for(var n=0;n<e.length;n++){var r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r)}else if(r>127&&r<2048){t+=String.fromCharCode(r>>6|192);t+=String.fromCharCode(r&63|128)}else{t+=String.fromCharCode(r>>12|224);t+=String.fromCharCode(r>>6&63|128);t+=String.fromCharCode(r&63|128)}}return t},_utf8_decode:function(e){var t="";var n=0;var r=c1=c2=0;while(n<e.length){r=e.charCodeAt(n);if(r<128){t+=String.fromCharCode(r);n++}else if(r>191&&r<224){c2=e.charCodeAt(n+1);t+=String.fromCharCode((r&31)<<6|c2&63);n+=2}else{c2=e.charCodeAt(n+1);c3=e.charCodeAt(n+2);t+=String.fromCharCode((r&15)<<12|(c2&63)<<6|c3&63);n+=3}}return t}}