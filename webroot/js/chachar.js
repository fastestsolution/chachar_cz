function edit_address(){
	if ($('user-addresses-0-street')){
		function address_title(type){
			$$('.address_title').each(function(item){
				if (!item.hasClass(type)){
					item.addClass('none');
				} else {
					item.removeClass('none');
				}
			});
		}
		address_title('add');
	}
	if ($('address_list')){
		// vyprazdneni vychozi adresy
		$$('.clear_address').each(function(item){
			item.value = '';
		});
		
		
		
		// editace adresa
		$('address_list').getElements('.select_address').addEvent('click',function(e){
			var li = e.target;
			var address_data = JSON.decode(li.get('data-address'));
			$$('.clear_address').each(function(item){
				item.value = ((address_data[item.get('data-type')])?address_data[item.get('data-type')]:'');
				
			});
			if ($('shop-provoz-id')){
				$('shop-provoz-id').fireEvent('change',$('shop-provoz-id'));
			}
			if ($('search-address-tel2')){
				$('search-address-tel2').value = li.getElement('.col-md-8').get('text');
				$('search-address-tel2').fireEvent('change',$('search-address-tel2'));
			}
			
			var myFx = new Fx.Scroll(window,{offset: {
				x: 0,
				y: -120
			}}).toElement('search-address-tel2');
		});
		
		// editace adresa
		$('address_list').getElements('.edit_address').addEvent('click',function(e){
			e.stop();
			var button = e.target;
			var li = e.target.getParent('li');
			var address_data = JSON.decode(li.get('data-address'));
			//console.log(address_data);
			$('address_list').getElements('li').each(function(item){
				item.removeClass('active');
			});
			
			li.addClass('active');
			
			$('hide_address').removeClass('none');
			
			$$('.clear_address').each(function(item){
				item.value = ((address_data[item.get('data-type')])?address_data[item.get('data-type')]:'');
				
			});
			//address_title('edit');
			
			var myFx = new Fx.Scroll(window).toElement('hide_address', 'y');
			
		});
		
		// new address
		if ($('new_address'))
		$('new_address').addEvent('click',function(e){
			e.stop();
			var button = e.target;
			var li = e.target.getParent('li');
				
				if ($('search-address-tel2')){
					$('search-address-tel2').value = '';
					$('search-address-tel2').focus();
				}
				
			
			
			$('hide_address').removeClass('none');
			
			$$('.clear_address').each(function(item){
				item.value = '';
				
			});
			$('user-addresses-0-street').focus();
			var myFx = new Fx.Scroll(window).toElement('hide_address', 'y');
			//address_title('add');
			
		});
		
		// smazani adresa
		$('address_list').getElements('.delete_address').addEvent('click',function(e){
			e.stop();
			if (confirm(e.target.get('data-confirm'))){
				
				var li = e.target.getParent('li');
				var button = e.target;
				button_preloader(button);
					
				this.req_form = new Request.JSON({
					url:e.target.href,
					onError: this.req_error = (function(data){
						button_preloader(button);
					}).bind(this),

					onComplete :(function(json){
						button_preloader(button);
						if (json.r == true){
							FstAlert(json.m);
							li.destroy();
						} else {
							FstError(json.m);
						}
						
					}).bind(this)
				});
				this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
				this.req_form.send();
			}
			
		});
	}
}

function intro() {
	window.addEvent('domready', function() {
		var height = $('body').getScrollSize().y;
		//$('overlay').setStyle('height', height);
		//$('overlay').show();
		//$('overlay').fade(1);
		
		$('close-intro').addEvent('click', function(e) {
			new Event(e).stop();
			
			//$('overlay').hide();
			$('intro-vanoce').hide();
			var myCookie  =  Cookie.write('confirm',true, {duration: 1});
		});
	});
}

function init_chachar(){
	edit_address();
}