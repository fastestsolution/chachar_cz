noSleep = new NoSleep();
var FstMobile = this.FstMobile = new Class({
	Implements:[Options,Events],
	
	// init fce
	initialize:function(options){
		if ($('mobile_items')){
			this.init_after_load();
			
			this.show_map();
			this.updateIp.periodical(1000*30);
			
		} else {
			this.show_map();
		}
		//FstError('Ahoj test');
		
	},
	
	init_after_load: function(){
		this.init_sortable();
		this.init_events();
		this.loadScriptMap();
		this.sortable();
		
		this.countDown();
		window.addEvent('load',function(){
			$('mobile_page_preloader').addClass('none');
			
		});
		
		
	},
	
	timeCount: function(el){
			date = new Date(el.get('data-time'));
			//console.log(date);
			
			rozdil =  new Date().getTime() - date.getTime() ;
			//console.log(rozdil);
			//timePlus = el.get('data-timePlus').toInt();
			//timePlus += 1000;
			//el.set('data-timePlus',timePlus);
			//console.log(this.timePlus);
			var millis = rozdil,
				time = Math.floor(millis / 1000),
				stop = time == 0,
				countdown = {
			
					days: Math.floor(time / (60 * 60 * 24)), 
					time: time,
					millis: millis
				};
			
			time %= (60 * 60 * 24);
			
	        countdown.hours = Math.floor(time / (60 * 60));
			time %= (60 * 60);
	        countdown.minutes = Math.floor(time / 60);
	        countdown.second = time % 60;
			//console.log(countdown);
			var text = '';
						
			if(countdown.days > 0) text = countdown.days + ' d ';
						
			text += (countdown.hours > 10 ? '' : '0') + countdown.hours + ':';
			text += (countdown.minutes >= 10 ? '' : '0') + countdown.minutes + ':';
			text += (countdown.second > 10 ? '' : '0') + countdown.second;
						
			el.set('text', text);
			//console.log(countdown);
	},
	
	// odpocet predpokladane doby
	countDown: function(){
		this.timePlus = 0;
			
		
		///timeCount();
		$$('.doba_date').each(function(doba){
			time = Date.parse(doba.get('data-time'));
			//console.log(time);
			coundown = new CountDown({
			
					//initialized 30s from now
					//date: new Date(new Date().getTime() + 30000),
					date: new Date(new Date(time).getTime()),
					//update every 100ms
					frequency: 1000, 
					//update the div#counter
					onChange: function(counter) {
					
						var text = '';
						
						if(counter.days > 0) text = counter.days + ' d ';
						
						text += (counter.hours > 10 ? '' : '0') + counter.hours + ':';
						text += (counter.minutes >= 10 ? '' : '0') + counter.minutes + ':';
						text += (counter.second >= 10 ? '' : '0') + counter.second;
						//text += ' - ' + (counter.millis > 10 ? '' : '0') + counter.millis + ':';
						
						doba.set('text', text);
						if (counter.minutes < 4 && counter.hours < 1){
							doba.getParent('.order_item').addClass('order_item_blinkOrange');
							
						}
					},
					//complete
					onComplete: (function () {
						doba.getParent('.order_item').removeClass('order_item_blinkOrange');
						doba.getParent('.order_item').addClass('order_item_blinkRed');
						//doba.set('html', '<strong>Musíte vyložit!</strong>')
						//(function(){
							this.timeCount.periodical(1000,this,doba);
							//this.timeCount(doba);
		
					}).bind(this)
				})
		}.bind(this));
	},
	
	sortable: function(){
		
var sortable = new Sortable($('mobile_items'), {
	//group: "name",  // or { name: "...", pull: [true, false, clone], put: [true, false, array] }
	sort: true,  // sorting inside list
	delay: 0, // time in milliseconds to define when the sorting should start
	disabled: false, // Disables the sortable if set to true.
	store: null,  // @see Store
	animation: 150,  // ms, animation speed moving items when sorting, `0` — without animation
	handle: ".my-handle",  // Drag handle selector within list items
	filter: ".ignore-elements",  // Selectors that do not lead to dragging (String or Function)
	preventOnFilter: true, // Call `event.preventDefault()` when triggered `filter`
	draggable: ".item",  // Specifies which items inside the element should be draggable
	ghostClass: "sortable-ghost",  // Class name for the drop placeholder
	chosenClass: "sortable-chosen",  // Class name for the chosen item
	dragClass: "sortable-drag",  // Class name for the dragging item
	dataIdAttr: 'data-id',

	forceFallback: false,  // ignore the HTML5 DnD behaviour and force the fallback to kick in

	fallbackClass: "sortable-fallback",  // Class name for the cloned DOM Element when using forceFallback
	fallbackOnBody: false,  // Appends the cloned DOM Element into the Document's Body
	fallbackTolerance: 0, // Specify in pixels how far the mouse should move before it's considered as a drag.        
	
	scroll: true, // or HTMLElement
	//scrollFn: function(offsetX, offsetY, originalEvent) { }, // if you have custom scrollbar scrollFn may be used for autoscrolling
	scrollSensitivity: 30, // px, how near the mouse must be to an edge to start scrolling.
	scrollSpeed: 10, // px

	setData: function (/** DataTransfer */dataTransfer, /** HTMLElement*/dragEl) {
		dataTransfer.setData('Text', dragEl.textContent); // `dataTransfer` object of HTML5 DragEvent
	},

	// Element is chosen
	onChoose: function (/**Event*/evt) {
		evt.oldIndex;  // element index within parent
	},

	// Element dragging started
	onStart: function (/**Event*/evt) {
		evt.oldIndex;  // element index within parent
	},

	// Element dragging ended
	onEnd: function (/**Event*/evt) {
		evt.oldIndex;  // element's old index within parent
		evt.newIndex;  // element's new index within parent
	},

	// Element is dropped into the list from another list
	onAdd: function (/**Event*/evt) {
		var itemEl = evt.item;  // dragged HTMLElement
		evt.from;  // previous list
		// + indexes from onEnd
	},

	// Changed sorting within list
	onUpdate: function (/**Event*/evt) {
		var itemEl = evt.item;  // dragged HTMLElement
		// + indexes from onEnd
	},

	// Called by any change to the list (add / update / remove)
	onSort: function (/**Event*/evt) {
		// same properties as onUpdate
	},

	// Element is removed from the list into another list
	onRemove: function (/**Event*/evt) {
		// same properties as onUpdate
	},

	// Attempt to drag a filtered element
	onFilter: function (/**Event*/evt) {
		var itemEl = evt.item;  // HTMLElement receiving the `mousedown|tapstart` event.
	},

	// Event when you move an item in the list or between lists
	onMove: function (/**Event*/evt, /**Event*/originalEvent) {
		// Example: http://jsbin.com/tuyafe/1/edit?js,output
		evt.dragged; // dragged HTMLElement
		evt.draggedRect; // TextRectangle {left, top, right и bottom}
		evt.related; // HTMLElement on which have guided
		evt.relatedRect; // TextRectangle
		originalEvent.clientY; // mouse position
		// return false; — for cancel
	},
	
	// Called when creating a clone of element
	onClone: function (/**Event*/evt) {
		var origEl = evt.item;
		var cloneEl = evt.clone;
	}
});
	},
	
	enableNoSleep: function() {
	  //console.log('ss');
	  noSleep.enable();
	  $('body').removeEvents('click');
			
	 
	},
	saveGpsRun: function(){
		//console.log(noSleep);
		
		this.saveGpsTimer = (function(){
			this.saveGps();
		}).periodical(5000,this);

	},
	
	saveGps: function(){
		//console.log(this.current_location);
		if (typeof window.current_location == 'undefined'){
			FstError('Neni zjištěna Vaše GPS poloha');
		} else {
			this.save_gps = new Request.JSON({
				url:'/saveGps/'+window.current_location.lat+'/'+window.current_location.lng+'/'+window.current_id,
				onError: this.req_error = (function(data){
				}).bind(this),

				onComplete :(function(json){
					
				}).bind(this)
			});
			this.save_gps.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			this.save_gps.send();
		}
	},
	
	clearSaveGps: function(){
		
		noSleep.disable();
		
		clearInterval(this.saveGpsTimer);
		this.saveGpsTimer	= null;
	},
	
	
	
	init_events: function(){
		
		$('mobile_items').getElements('.vylozeno').removeEvents('click');
		$('mobile_items').getElements('.vylozeno').addEvent('click',function(e){
			e.stop();

			if (confirm(e.target.get('data-title'))){
				button = e.target;
				parent_el = e.target.getParent('.order_item');
				if (parent_el.getNext('.order_item:not(.vezu_active)')){
					next_el = parent_el.getNext('.order_item:not(.vezu_active)');
				} else {
					next_el = null;
				}
				
				/*
				if (parent_el.getNext('.order_item')){
					next_el = parent_el.getNext('.order_item');
				} else {
					next_el = null;
				}
				*/
				button_preloader(button);
				this.req_form = new Request.JSON({
					url:e.target.href,
					onError: this.req_error = (function(data){
						button_preloader(button);
					}).bind(this),

					onComplete :(function(json){
						this.clearSaveGps();
						button_preloader(button);
						if (json.r == true){
							FstAlert('Vyloženo');
							parent_el.destroy();		
							if (next_el && next_el.getElement('.vezu')){
								if (!next_el.getElement('.vezu').hasClass('active')){
									next_el.getElement('.vezu').fireEvent('click',next_el.getElement('.vezu'));
								}
							} else {
								// todo poslani na provozovnu
							}
							//console.log(next_el);
						} else {
							FstError(json.m);
							
						}
								
					}).bind(this)
				});
				this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
				this.req_form.send();
				
			}
		}.bind(this));
		
		$('mobile_items').getElements('.vezu').removeEvents('click');
		
		//console.log($('mobile_items').getElements('.order_item:not(.vezu_active)'));
		$('mobile_items').getElements('.vezu').addEvent('click',function(e){
			
			if (e.stop){
				e.stop();
				elClick = e.target;
				elClick.set('data-title',elClick.get('data-title').replace('povezete další','vezete'));
				no_next = false;
			} else {
				e.target = e;
				elClick = e;
				elClick.set('data-title',elClick.get('data-title').replace('vezete','povezete další'));
				no_next = true;
			}
			
			//if (confirm(elClick.get('data-title'))){
				this.enableNoSleep();
				button = elClick;
				elParent = elClick.getParent('.order_item');
				
				
				//console.log(elParent.getNext('.order_item:not(.vezu_active)'));
				
				window.current_id = button.get('data-id');
				button_preloader(button);
				this.req_form = new Request.JSON({
					url:elClick.href,
					onError: this.req_error = (function(data){
						button_preloader(button);
					}).bind(this),

					onComplete :(function(json){
						button_preloader(button);
						if (json.r == true){
							
							elParent.addClass('vezu_active');
							button.addClass('active');
							FstAlert('Posíláte polohu zákazníkovi');
							if (!no_next){
								if (elParent.getNext('.order_item:not(.vezu_active)')){
									next_el = elParent.getNext('.order_item:not(.vezu_active)');
									next_el.getElement('.vezu').fireEvent('click',next_el.getElement('.vezu'));
								}
							}
							//this.saveGpsRun();
						} else {
							FstError(json.m);
							
						}
								
					}).bind(this)
				});
				this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
				this.req_form.send();
				
			//}
		}.bind(this));
		
		$('mobile_items').getElements('.polozky').removeEvents('click');
		$('mobile_items').getElements('.polozky').addEvent('click',function(e){
			e.stop();
			e.target.getParent('.order_item').getElement('.products').toggleClass('none');
		});
		
		if ($('mobile_items').getElement('.vezu.active')){
			this.clearSaveGps();
			window.current_id = $('mobile_items').getElement('.vezu.active').get('data-id');
				$('body').addEvent('click',this.enableNoSleep);
			
			$('mobile_items').getElement('.vezu.active').fireEvent('mousedown');
			//this.saveGpsRun();
			//console.log($('mobile_items').getElement('.vezu.active'));
		}
		
	},
	
	line_color: function(){
		$('mobile_items').getElements('.order_item').each(function(item,k){
			item.removeClass('even');
			item.removeClass('odd');
			item.addClass((k++ % 2) ? "even" : "odd");
		});
	},
	
	// init sortable
	init_sortable: function(){
		line_color = this.line_color; 
		var sortable = Sortable.create($('mobile_items'),{
		   // Element dragging ended
			handle:".move",
			group: "localStorage-orders",
			store: {
				/**
				 * Get the order of elements. Called once during initialization.
				 * @param   {Sortable}  sortable
				 * @returns {Array}
				 */
				get: function (sortable) {
					var order = localStorage.getItem(sortable.options.group.name);
					if (order){
					list = order.split('|');
					window.order_list = list;
					
						$('mobile_items').getElements('.order_item').each(function(order_item,k){
							if (!list.contains(order_item.get('data-id'))){
								order_item.addClass('new');
							}
							
					});
					}
					return order ? order.split('|') : [];
				},

				/**
				 * Save the order of elements. Called onEnd (when the item is dropped).
				 * @param {Sortable}  sortable
				 */
				set: function (sortable) {
					var order = sortable.toArray();
					localStorage.setItem(sortable.options.group.name, order.join('|'));
				}
			},
			onChoose: function (/**Event*/evt) {
				evt.item.addClass('choose');
				//console.log(evt.item);
				evt.oldIndex;  // element index within parent
			},
			onEnd: function (/**Event*/evt) {
				line_color();
				evt.item.removeClass('choose');
				order = sortable.toArray();
				
				if (order){
					window.order_list = order;
					//console.log(order);
					//list = order.split('|');
					this.getPoints();
					this.renderMarker();
					//google.maps.event.trigger(this.map, 'resize');
				}
				//evt.oldIndex;  // element's old index within parent
				//evt.newIndex;  // element's new index within parent
			}.bind(this),
		});
	},
	loadScriptMap: function(){
		//console.log('load');
		if (typeof google == 'undefined'){
		var script = document.createElement("script");
		script.type = "text/javascript";
		//script.src = "https://maps.googleapis.com/maps/api/js?v=3&libraries=places,geometry&sensor=false&callback=FstMobile.initMap&key=AIzaSyBmF2BDQuPrbixSZF0n72mEOaoncj9bp6E";
		script.src = "https://maps.googleapis.com/maps/api/js?v=3&libraries=places,geometry&sensor=false&callback=FstMobile.initMap&key=AIzaSyAmRIyQUbK5x_c_cm78ZV5ODf-sqVO1-2s";
		document.body.appendChild(script);
		} else {
			this.initMap();
		}
			 
	},
	
	getPoints: function(){
		
		this.point_list = [];
		//console.log(window.order_list);
		$('mobile_items').getElements('.map_data').each(function(item){
			if (typeof window.order_list != 'undefined'){
				window.order_list.each(function(it){
					//console.log(it);
					//console.log(item.getParent('.order_item').get('data-id'));
					if (it == item.getParent('.order_item').get('data-id')){
						this.point_list.push({'pos':new google.maps.LatLng(item.get('data-lat'), item.get('data-lng')),'name':item.getParent('.order_item').getElement('.client_name').get('text')});
					}
				}.bind(this));

				} else {
				this.point_list.push({'pos':new google.maps.LatLng(item.get('data-lat'), item.get('data-lng')),'name':item.getParent('.order_item').getElement('.client_name').get('text')});
			}
			
		}.bind(this));
		
		
		//console.log(this.point_list);
		//console.log(window.order_list);
	},
	
	renderMarker: function(){
		if (typeof this.markers != 'undefined'){
		for (var i=0; i<this.markers.length; i++) {
			this.markers[i].setMap(null);
		}
		}
		
		this.markers = [];
		this.point_list.each(function(point,k){
			
			marker = new google.maps.Marker({
				position: point.pos,
				title: point.name,
				label: (k+1).toString(),
				map: this.map
			});
			this.markers.push(marker);
			this.bounds.extend(marker.position);
		}.bind(this));
		this.map.fitBounds(this.bounds);
	},
	
	show_map: function(){
		if ($('Reload'))
		$('Reload').addEvent('click',function(e){
			if (!e.target){
				e.target = $('Reload');
			}
			button = e.target;
			button_preloader(button);
			$('reload_mobile').addClass('preloader');
			if ($('mobile_page_preloader'))
			$('mobile_page_preloader').removeClass('none');			
			this.req_form = new Request.HTML({
				url:'/mobile/'+$('hash_url').get('data-value'),
				update:'reload_mobile',
				timeout: 1000*3,
				onTimeout: this.req_timeout = (function(data){
					$('mobileNoResponse').removeClass('none');
					if ($('reload_mobile')) $('reload_mobile').removeClass('preloader');
					if ($('mobile_page_preloader')) $('mobile_page_preloader').addClass('none');
			
					console.log('timeout');
					this.req_form.cancel();
					button_preloader(button);
				}).bind(this),
				onError: this.req_error = (function(data){
					button_preloader(button);
					$('mobileNoResponse').addClass('none');
				}).bind(this),

				onComplete :(function(json){
					button_preloader(button);
					$('mobileNoResponse').addClass('none');	
					$('reload_mobile').removeClass('preloader');
					$('mobile_page_preloader').addClass('none');
			
					//console.log($('reload_mobile'));
					this.init_after_load();					
				}).bind(this)
			});
			this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			this.req_form.send();
	
		}.bind(this));
		$('Reload').click();
		if ($('ShowMap'))
		$('ShowMap').addEvent('click',function(e){
			e.stop();
			$('map-canvas').toggleClass('none');
			google.maps.event.trigger(this.map, 'resize');
			this.map.fitBounds(this.bounds);
			var zoom = this.map.getZoom();
			this.map.setZoom(zoom > 18 ? 18 : zoom);
		}.bind(this));
	},
	
	// update ip periodical
	updateIp: function(){
		//saveGspRozvoce
			this.send_ip = new Request.HTML({
				url:'/mobiles/saveGspRozvoce/true',
				onError: this.req_error = (function(data){
				}).bind(this),

				onComplete :(function(json){
				}).bind(this)
			});
			this.send_ip.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			this.send_ip.send();
	},
	
	currentLocation: function(){
		// Try HTML5 geolocation.
        //console.log('cur');
		if (navigator.geolocation) {
         
		
		  
		  navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
			window.current_location = this.current_location = pos;
			//console.log(window.current_location);
			this.currentMarker();
			
            
          }.bind(this), function(err) {
            console.log(err);
			//handleLocationError(true, infoWindow, this.map.getCenter());
          }.bind(this),{maximumAge:10000, enableHighAccuracy: true});
        
		 //Tracking users position
		 /*
			watchId = navigator.geolocation.watchPosition(
				 function(position) {
				//console.log(position.coords.latitude);
				//console.log(position.coords.longitude);
				var pos = {
				  lat: position.coords.latitude,
				  lng: position.coords.longitude
				};
				window.current_location = this.current_location = pos;
				this.currentMarker();
				

				
			  }.bind(this),
				 // Optional settings below
				 function(err){
					 
				 },
				 {
					 timeout: 0,
					 enableHighAccuracy: true,
					 maximumAge: Infinity
				 }
			);
		  */
		
		} else {
          // Browser doesn't support Geolocation
        }
		
		/*
		this.current_location = {
              lat: 49.8359303,
              lng: 18.272929299999978
        };
		*/
		
		//console.log(this.current_location);


	},
	
	currentMarker: function(){
		
		svg = '<?xml version="1.0" encoding="iso-8859-1"?><!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0) --><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 336 336" style="enable-background:new 0 0 336 336;" xml:space="preserve" width="26px" height="26px"><g><g><g><path d="M312,123.2c0-0.4,0-0.8,0-1.2l-18.4-82.8c-4-18.4-20.4-31.6-39.2-31.6H77.6c-18.8,0-35.2,13.2-39.2,31.6L20,122.4 c-0.4,1.2-0.4,2,0,3.2c-12,6.8-20,20-20,34.8v80c0,12.4,6.8,22.8,16.4,30c0,0.4-0.4,1.2-0.4,2v28c0,15.6,12.4,28,28,28h8 c15.6,0,28-12.4,28-28v-20h176v20c0,15.6,12.4,28,28,28h8c15.6,0,28-12.4,28-28v-28c9.6-7.2,16-18.8,16-32V160 C336,143.6,326,129.6,312,123.2z M54.4,42.8C56.8,32,66.4,24,77.6,24h176.8c11.2,0,21.2,8,23.2,18.8l17.2,77.2H40 c-0.8,0-2,0-2.8,0L54.4,42.8z M64,300c0,6.8-5.2,12-12,12h-8c-6.8,0-12-5.2-12-12v-22c4,1.2,8,2,12,2h20V300z M304,300 c0,6.8-5.2,12-12,12h-8c-6.8,0-12-5.2-12-12v-20h24c2.8,0,5.6-0.4,8-0.8V300z M320,240c0,13.2-10.8,24-24,24H44 c-12.8,0-28-10.8-28-24v-80c0-13.2,10.8-24,24-24h256c13.2,0,24,10.8,24,24V240z" fill="#D80027"/><path d="M60,172c-13.2,0-24,10.8-24,24s10.8,24,24,24s24-10.8,24-24S73.2,172,60,172z M60,204c-4.4,0-8-3.6-8-8c0-4.4,3.6-8,8-8 c4.4,0,8,3.6,8,8C68,200.4,64.4,204,60,204z" fill="#D80027"/><path d="M276,172c-13.2,0-24,10.8-24,24s10.8,24,24,24s24-10.8,24-24S289.2,172,276,172z M276,204c-4.4,0-8-3.6-8-8 c0-4.4,3.6-8,8-8c4.4,0,8,3.6,8,8C284,200.4,280.4,204,276,204z" fill="#D80027"/></g></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>';
		
		marker = new google.maps.Marker({
			position: new google.maps.LatLng(this.current_location.lat, this.current_location.lng),
			title: 'Moje poloha',
			//label: 'X',
			map: this.map,
			icon: { url: 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(svg) }
		});
		this.point_list.each(function(point,k){
			//console.log(this.current_location);
			//console.log(point);
			this.calcRoute(k,this.current_location,point.pos);
		
		}.bind(this));
	},
	
	initMap: function() {
		this.getPoints();
		this.bounds = new google.maps.LatLngBounds();
		
		
		var pointA = new google.maps.LatLng(51.7519, -1.2578),
			pointB = new google.maps.LatLng(50.8429, -0.1313);
			
			if (typeof this.point_list[0] != 'undefined'){
			var myOptions = {
				zoom: 7,
				center:  this.point_list[0].pos
			};
			} else {
			var myOptions = {
				zoom: 7,
			};
				
			}
			
			this.map = new google.maps.Map(document.getElementById('map-canvas'), myOptions);
			//// Instantiate a directions service.
			directionsService = new google.maps.DirectionsService;
			directionsDisplay = new google.maps.DirectionsRenderer;
			
			/*
			directionsDisplay = new google.maps.DirectionsRenderer({
				map: this.map
			});
			*/
		this.renderMarker();	
		this.currentLocation();	
		$('map-canvas').addClass('none');
		//console.log(pointA);
		// get route from A to B
		//calculateAndDisplayRoute(directionsService, directionsDisplay, pointA, pointB);

	},
	
	calcRoute: function(pointKey,start,end) {
	  orderList = $('mobile_items').getElements('li.order_item');
	  //console.log(orderList);
	  if (orderList[pointKey].getElement('.km') && orderList[pointKey].getElement('.km').get('text') == ''){
	  start = new google.maps.LatLng(start.lat, start.lng);
	  //var start = document.getElementById('start').value;
	  //var end = document.getElementById('end').value;
	  //console.log(start.lat());
	  //console.log(end.lat());
	  var request = {
		origin:start,
		destination:end,
		travelMode: 'DRIVING'
	  };
	  
	  directionsService.route(request, function(response, status) {
		//console.log(response);
		if (status == 'OK') {
			directionsDisplay.setDirections(response);
			km = directionsDisplay.directions.routes[0].legs[0].distance.text.replace(',','.').toFloat();
			time = directionsDisplay.directions.routes[0].legs[0].duration.text;
			//console.log(pointKey);
			//console.log(km);
			//console.log(directionsDisplay.directions.routes[0].legs[0].duration.text);
			if (orderList[pointKey].getElement('.km')){
				orderList[pointKey].getElement('.km').set('text',km+'km, '+time);
			
			//	console.log(orderList[pointKey].getElement('.km'));
			} else {
				//console.log(orderList[pointKey]);
			}
			//directionsDisplay.directions.routes[0].legs[0].distance.text
		}
	  });
	  }
	}

});
window.addEvent('domready',function(){
	FstMobile = new FstMobile();	
	
});
