var RozvozceMap = this.RozvozceMap = new Class({
	Implements:[Options,Events],
	
	// init fce
	initialize:function(options){
		$('map_canvas_rozvozces').setStyle('height',window.getSize().y - 70);	
		this.loadScriptMap();
		this.loadMapData();
		//alert('Ahoj test');
		this.loadMapData.periodical(15000,this);
		
		
	},
	
	loadMapData: function(){
		$('map_preloader').addClass('preloader');	
			this.save_gps = new Request.JSON({
				url:'/mobiles/load_map/'+$('pokladna_id').get('data-id'),
				onError: this.req_error = (function(data){
				}).bind(this),

				onComplete :(function(json){
					
					$('map_preloader').removeClass('preloader');
					if (json.r == true){
						this.getPoints(json.data);
					}
					//console.log(json);
				}).bind(this)
			});
			this.save_gps.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			this.save_gps.send();
	},
	
	loadScriptMap: function(){
		//console.log('load');
		if (typeof google == 'undefined'){
		var script = document.createElement("script");
		script.type = "text/javascript";
		//script.src = "https://maps.googleapis.com/maps/api/js?v=3&libraries=places,geometry&sensor=false&callback=FstMobile.initMap&key=AIzaSyBmF2BDQuPrbixSZF0n72mEOaoncj9bp6E";
		script.src = "https://maps.googleapis.com/maps/api/js?v=3&libraries=places,geometry&sensor=false&callback=RozvozceMap.initMapBig&key=AIzaSyAmRIyQUbK5x_c_cm78ZV5ODf-sqVO1-2s";
		document.body.appendChild(script);
		} else {
			this.initMapBig();
		}
			 
	},
	
	getPoints: function(data){
		this.bounds = new google.maps.LatLngBounds();
		this.point_list = [];
		$('rozvozce_list').empty();
		rozvozce_count = 0;
		Object.each(data,function(item){
			//console.log(item.lat);
			if (item.lat != null){
				rozvozce_count ++;
				new Element('li').set('html','<strong>'+item.rozvozce_name+'</strong> <span class="time">'+item.update_gps+'</span><span class="count_orders">'+item.count_orders+' objednávek naloženo</span>').inject($('rozvozce_list'));
				this.point_list.push({'pos':new google.maps.LatLng(item.lat, item.lng),'name':item.rozvozce_name,'update_gps':item.update_gps});
			}
		}.bind(this));
		
		if (rozvozce_count>0){
			this.renderMarker();
		}
		//console.log(this.point_list);
	},
	
	renderMarker: function(){
		if (typeof this.markers != 'undefined'){
		for (var i=0; i<this.markers.length; i++) {
			this.markers[i].setMap(null);
		}
		}
		svg = '<?xml version="1.0" encoding="iso-8859-1"?><!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0) --><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 336 336" style="enable-background:new 0 0 336 336;" xml:space="preserve" width="26px" height="26px"><g><g><g><path d="M312,123.2c0-0.4,0-0.8,0-1.2l-18.4-82.8c-4-18.4-20.4-31.6-39.2-31.6H77.6c-18.8,0-35.2,13.2-39.2,31.6L20,122.4 c-0.4,1.2-0.4,2,0,3.2c-12,6.8-20,20-20,34.8v80c0,12.4,6.8,22.8,16.4,30c0,0.4-0.4,1.2-0.4,2v28c0,15.6,12.4,28,28,28h8 c15.6,0,28-12.4,28-28v-20h176v20c0,15.6,12.4,28,28,28h8c15.6,0,28-12.4,28-28v-28c9.6-7.2,16-18.8,16-32V160 C336,143.6,326,129.6,312,123.2z M54.4,42.8C56.8,32,66.4,24,77.6,24h176.8c11.2,0,21.2,8,23.2,18.8l17.2,77.2H40 c-0.8,0-2,0-2.8,0L54.4,42.8z M64,300c0,6.8-5.2,12-12,12h-8c-6.8,0-12-5.2-12-12v-22c4,1.2,8,2,12,2h20V300z M304,300 c0,6.8-5.2,12-12,12h-8c-6.8,0-12-5.2-12-12v-20h24c2.8,0,5.6-0.4,8-0.8V300z M320,240c0,13.2-10.8,24-24,24H44 c-12.8,0-28-10.8-28-24v-80c0-13.2,10.8-24,24-24h256c13.2,0,24,10.8,24,24V240z" fill="#D80027"/><path d="M60,172c-13.2,0-24,10.8-24,24s10.8,24,24,24s24-10.8,24-24S73.2,172,60,172z M60,204c-4.4,0-8-3.6-8-8c0-4.4,3.6-8,8-8 c4.4,0,8,3.6,8,8C68,200.4,64.4,204,60,204z" fill="#D80027"/><path d="M276,172c-13.2,0-24,10.8-24,24s10.8,24,24,24s24-10.8,24-24S289.2,172,276,172z M276,204c-4.4,0-8-3.6-8-8 c0-4.4,3.6-8,8-8c4.4,0,8,3.6,8,8C284,200.4,280.4,204,276,204z" fill="#D80027"/></g></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>';
		this.markers = [];
		this.point_list.each(function(point,k){
			
			marker = new google.maps.Marker({
				position: point.pos,
				title: point.name,
				label: point.name +" "+ point.update_gps,
				icon: { url: 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(svg),labelOrigin: new google.maps.Point(11, 40) },
				map: this.map
			});
			this.markers.push(marker);
			this.bounds.extend(marker.position);
		}.bind(this));
		this.map.fitBounds(this.bounds);
		var zoom = this.map.getZoom();
		this.map.setZoom(zoom > 16 ? 16 : zoom);
	},
	
	show_map: function(){
		
	},
	
	currentLocation: function(){
		// Try HTML5 geolocation.
        //console.log('cur');
		if (navigator.geolocation) {
         
		
		  
		  navigator.geolocation.getCurrentPosition(function(position) {
            var pos = {
              lat: position.coords.latitude,
              lng: position.coords.longitude
            };
			window.current_location = this.current_location = pos;
			this.currentMarker();
			
            
          }.bind(this), function(err) {
            console.log(err);
			//handleLocationError(true, infoWindow, this.map.getCenter());
          }.bind(this),{maximumAge:10000, enableHighAccuracy: true});
        
		 //Tracking users position
		 /*
			watchId = navigator.geolocation.watchPosition(
				 function(position) {
				//console.log(position.coords.latitude);
				//console.log(position.coords.longitude);
				var pos = {
				  lat: position.coords.latitude,
				  lng: position.coords.longitude
				};
				window.current_location = this.current_location = pos;
				this.currentMarker();
				

				
			  }.bind(this),
				 // Optional settings below
				 function(err){
					 
				 },
				 {
					 timeout: 0,
					 enableHighAccuracy: true,
					 maximumAge: Infinity
				 }
			);
		  */
		
		} else {
          // Browser doesn't support Geolocation
        }
		
		/*
		this.current_location = {
              lat: 49.8359303,
              lng: 18.272929299999978
        };
		*/
		
		//console.log(this.current_location);


	},
	
	
	initMapBig: function() {
		//this.getPoints();
		//this.bounds = new google.maps.LatLngBounds();
		//console.log('a');
		
		var pointA = new google.maps.LatLng(51.7519, -1.2578),
			pointB = new google.maps.LatLng(50.8429, -0.1313);
			
			var myOptions = {
				zoom: 7,
				center:  new google.maps.LatLng(49.823290, 15.461077)
			};
			
			this.map = new google.maps.Map(document.getElementById('map_canvas_rozvozces'), myOptions);
			//// Instantiate a directions service.
			//directionsService = new google.maps.DirectionsService;
			//directionsDisplay = new google.maps.DirectionsRenderer;
			
			/*
			directionsDisplay = new google.maps.DirectionsRenderer({
				map: this.map
			});
			*/
		//this.renderMarker();	
		//this.currentLocation();	
		//console.log(pointA);
		// get route from A to B
		//calculateAndDisplayRoute(directionsService, directionsDisplay, pointA, pointB);

	},
	
	calcRoute: function(pointKey,start,end) {
	  orderList = $('mobile_items').getElements('li.order_item');
	  
	  if (orderList[pointKey].getElement('.km') && orderList[pointKey].getElement('.km').get('text') == ''){
	  start = new google.maps.LatLng(start.lat, start.lng);
	  //var start = document.getElementById('start').value;
	  //var end = document.getElementById('end').value;
	  //console.log(start.lat());
	  //console.log(end.lat());
	  var request = {
		origin:start,
		destination:end,
		travelMode: 'DRIVING'
	  };
	  
	  directionsService.route(request, function(response, status) {
		//console.log(response);
		if (status == 'OK') {
			directionsDisplay.setDirections(response);
			km = directionsDisplay.directions.routes[0].legs[0].distance.text.replace(',','.').toFloat();
			time = directionsDisplay.directions.routes[0].legs[0].duration.text;
			//console.log(pointKey);
			//console.log(km);
			//console.log(directionsDisplay.directions.routes[0].legs[0].duration.text);
			if (orderList[pointKey].getElement('.km')){
				orderList[pointKey].getElement('.km').set('text',km+'km, '+time);
			
			//	console.log(orderList[pointKey].getElement('.km'));
			} else {
				//console.log(orderList[pointKey]);
			}
			//directionsDisplay.directions.routes[0].legs[0].distance.text
		}
	  });
	  }
	}

});
window.addEvent('domready',function(){
	RozvozceMap = new RozvozceMap();	
	
});
