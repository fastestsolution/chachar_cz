
var loadDeferredStyles = function() {
    
	var addStylesNode = document.getElementById("deferred-styles");
    var replacement = document.createElement("div");
        replacement.innerHTML = addStylesNode.textContent;
        document.body.appendChild(replacement);
        addStylesNode.parentElement.removeChild(addStylesNode);
        /*setTimeout(function(){
            document.getElementById("body").style.display = null;
        }, 500);*/
};
var raf = requestAnimationFrame || mozRequestAnimationFrame ||
    webkitRequestAnimationFrame || msRequestAnimationFrame;
//if (raf) raf(function() { window.setTimeout(loadDeferredStyles, 0); });
//else window.addEventListener('load', loadDeferredStyles);
window.addEventListener('load', loadDeferredStyles);
