
window.addEvent('domready',function(){
	$$('.filtr').addEvent('change',function(e){
		if (e.target.value == ''){
		window.location = '/admin/products/';
		} else {
		window.location = window.location+'?category_id='+$('filtr-category-id').value+'&name='+$('filtr-name').value;
		
		}
		//window.location = window.location+'?category_id='+$('filtr-category-id').value+'&name='+$('filtr-name').value;
	});
	function clear_data(id){
		if ($('product'+id)){
			$('product'+id).destroy();
		}
	}
	
	$$('.delete').addEvent('click',function(e){
		e.stop();
		parent = e.target.getParent('.product');
		if (confirm('Opravdu?')){
			this.req_form = new Request.HTML({
				url:e.target.href,
				onError: this.req_error = (function(data){
					
					//button_preloader(button);
				}).bind(this),

				onComplete :(function(data){
					parent.destroy();
				}).bind(this)
			});
			this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			this.req_form.send();
		}
	});
	function load_product(id){
		if ($('product'+id)){
			
			$('name-internal').addEvent('change',function(e){
				$('product'+id).getElements('.trans_value_name').each(function(item){
					if (item.value == ''){
						item.value = $('product'+id).getElement('.trans_name').value;
					}
					
				});
			});
			if ($('price-vat'))
			$('price-vat').addEvent('change',function(e){
				$('product'+id).getElements('.trans_value_price').each(function(item){
					if (item.value == ''){
						item.value = $('price-vat').value;
					}
					
				});
			});
			
			$('product'+id).getElements('.trans_value_name').each(function(item){
				if (item.value == ''){
					item.value = $('product'+id).getElement('.trans_name').value;
				}
			});
			$('product'+id).getElements('.trans_value_description').each(function(item){
				if (item.value == ''){
					item.value = $('product'+id).getElement('.trans_description').value;
				}
			});
			$('product'+id).getElements('.trans_value_price').each(function(item){
				if (item.value == ''){
					item.value = $('product'+id).getElement('.trans_price').value;
				}
			});
			
			$('translations-cz1-description').addEvent('keyup',function(e){
				$('product'+id).getElements('.trans_cz_change_all').each(function(item){
					item.value = e.target.value;
				});
			});
			$('translations-en1-description').addEvent('keyup',function(e){
				$('product'+id).getElements('.trans_en_change_all').each(function(item){
					item.value = e.target.value;
				});
			});
			
			$('product'+id).getElements('.open_varianta').removeEvents('click');
			$('product'+id).getElements('.open_varianta').addEvent('click',function(e){
				e.stop();
				e.target.getNext('.varianty').toggleClass('none');
			});
			
			// upload foto
			$('product'+id).getElements('.fst_uploader').each(function(uploader){
				//console.log(uploader);
				//console.log(JSON.encode(uploader.value));
				
				upl = new FstUploader($(uploader.get('id')),{	
					uploaded_files:((uploader.value!='')?uploader.value:''),
					multi:((uploader.get('data-type')=='multi')?true:''),
					file_ext:((uploader.get('data-ext'))?uploader.get('data-ext'):''),
					count_file:((uploader.get('data-count'))?uploader.get('data-count'):''),
					special_name:((uploader.get('data-special_name'))?uploader.get('data-special_name'):''),
					paths:	{
						img_path:((uploader.get('data-path'))?uploader.get('data-path'):''),
						upload_script:'/fst_upload/'
					}
								
				},function() {});
				
			});
			
			
			// label
			$('admin_products').getElements('input.data_label').each(function(item){
				new Element('span',{'class':'data_label none'}).set('data-label',item.get('data-label')).inject(item,'before');
				item.addEvents({
					'mouseenter':function(e){
						e.target.getPrevious('span').removeClass('none');
					},
					'mouseleave':function(e){
						e.target.getPrevious('span').addClass('none');
					},
				});
			});
			
			save_product(id);
			
			
		}
	}
	
	// new product
	$('new_product').addEvent('click',function(e){
		e.stop();
		button = e.target;
		element = $('new_prod_element');			
		product_id = 'new';
		button_preloader(button);
		element.toggleClass('none');
		element.addClass('preloader');
			this.req_form = new Request.HTML({
				url:'/admin/save_product/'+product_id,
				update:element,
				onError: this.req_error = (function(data){
					
					button_preloader(button);
				}).bind(this),

				onComplete :(function(data){
					button_preloader(button);
					element.removeClass('preloader');
					load_product(product_id);	
				}).bind(this)
			});
			this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			this.req_form.send();
	});
	
	// show data product
	$('admin_products').getElements('.headline').addEvent('click',function(e){
		e.stop();
		element = e.target.getNext('.data');			
		element.toggleClass('none');
		product_id = e.target.get('data-id');
		element.addClass('preloader');
		element.empty();
		if (!element.hasClass('none')){
			this.req_form = new Request.HTML({
				url:'/admin/save_product/'+product_id,
				update:element,
				onError: this.req_error = (function(data){
				}).bind(this),

				onComplete :(function(data){
					element.removeClass('preloader');
					load_product(product_id);	
				}).bind(this)
			});
			this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			this.req_form.send();
		}
	});
	
	
	
	
	function save_product(id){
		$('admin_products').getElements('.saveProduct').removeEvents('click');
		$('admin_products').getElements('.saveProduct').addEvent('click',function(e){
			e.stop();
			var form = e.target.getParent('form');
			var button = e.target;
			button_preloader(button);
					
				this.req_form = new Request.JSON({
					url:form.get('action'),
					onError: this.req_error = (function(data){
						button_preloader(button);
					}).bind(this),

					onComplete :(function(json){
						
						button_preloader(button);
						
						if (json.r == true){
							FstAlert(json.m);
							if (json.redirect != ''){
								(function(){
									window.location = window.location;
								}).delay(500);
							}
							form.getParent('div.data').addClass('none');
							form.getParent('div.data').empty();
							
						} else {
							FstError(json.m);
						}
						
					}).bind(this)
				});
				this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
				this.req_form.post(form);
		});
	}
	
});