/**
 * Created by Fastest Solution s.r.o. Jakub Tyson on 10.1.2017.
 */

function fstInit(){
	console.log('a');
}	

function flipcards(){
	window.addEvent('domready',function(){
		if (Browser.ie && Browser.version < 10){
		
			$$('.flipcard').each(function(flip){
				flip.getElement('.back').fade('hide');
				flip.addEvents({
					mouseenter:function(e){
						flip.getElement('.front').fade(0);
						flip.getElement('.back').fade(1);
				
					},
					mouseleave:function(e){
						flip.getElement('.front').fade(1);
						flip.getElement('.back').fade(0);
				
					}
				});
			});
		}
	});

}


// preloader element
function el_preloader(el,class_name){
	//console.log(el); 
		if(typeof el.nodeName != 'undefined'){
			elData = [];
			elData.push(el);
		} else {
			elData = el;
		}
		//console.log(elData);
		//console.log(class_name);
		if (!class_name) class_name = 'el_preloader';
		
		elData.each(function(item){
			if (item.hasClass(class_name)){
				item.removeClass(class_name);
			} else {
				
				item.addClass(class_name);
			}
		});
}
 
/** PRICE FORMAT */
function price(price,params){
	if (!params){
	params = {
		'symbol_before':'',
		'kurz':'1',
		'count':'1',
		'decimal': 0,
		'symbol_after':',-'
	}
	}
	price = (price/params.kurz)* params.count;		
	return params.symbol_before + number_format(price, params.decimal, '.', ' ') + params.symbol_after;	
}

/** GET SCROLL OFFSET DATA*/
function getScrollOffsets() {

    // This works for all browsers except IE versions 8 and before
    if ( window.pageXOffset != null ) 
       return {
           x: window.pageXOffset, 
           y: window.pageYOffset
       };

    // For browsers in Standards mode
    var doc = window.document;
    if ( document.compatMode === "CSS1Compat" ) {
        return {
            x: doc.documentElement.scrollLeft, 
            y: doc.documentElement.scrollTop
        };
    }

    // For browsers in Quirks mode
    return { 
        x: doc.body.scrollLeft, 
        y: doc.body.scrollTop 
    }; 
}

/** DATE PICKER */
function date_picker(){
	//window.addEvent('domready', function() {
	if ($$('.date_range').length>0){
	var picker = new Picker.Date.Range($$('.date_range'), {
		timePicker: false,
		columns: 3,
		months: lang.mesice, 
		months_title: lang.mesice, 
		months_abbr:lang.mesice_short,
		days_abbr:lang.dny,
		shortDate: '%Y-%m-%d',
		dateOrder: ['year', 'month','date'],
		shortTime: '%H:%M:%S',
		format:'%Y-%m-%d',
		allowEmpty:true,
		positionOffset: {x: 5, y: 0}
	});
	}
	if ($$('.date').length>0){
	var picker = new Picker.Date($$('.date'), {
		timePicker: false,
		columns: 1,
		//months: lang[CURRENT_LANGUAGE].mesice, 
		//'abbr',{
		months_abbr:lang.mesice_short,
		days_abbr:lang.dny,
		//},
		//shortDate: '%d-%m-%Y',
		//dateOrder: ['date', 'month','year`'],
		//shortTime: '%H:%M:%S',
		format:'%d.%m.%Y',
		allowEmpty:true,
		positionOffset: {x: 5, y: 0}
	});
	
	}
	if ($$('.date_time').length>0){
	$$('.date_time').each(function(item){
		if (item.value == '0000-00-00 00:00:00')
			item.value = '';
	});
	
	var picker = new Picker.Date($$('.date_time'), {
		
		timePicker: true,
		columns: 1,
		months: lang.mesice, 
		months_abbr:lang.mesice_short,
		days_abbr:lang.dny,
			//months_title: ['ja', 'fe', 'ma', 'ap', 'me', 'jn', 'jl', 'au', 'ok', 'se', 'no', 'de'],
			//days_abbr: ['zon', 'maa', 'din', 'woe', 'don', 'vri', 'zat'],
		
		
		shortDate: '%d-%m-%Y',
		dateOrder: ['date', 'month','year'],
		shortTime: '%H:%M:%S',
		format:'%d.%m.%Y %H:%M',
		allowEmpty:true,
		
		//months_abbr: ['ja', 'fe', 'ma', 'ap', 'me', 'jn', 'jl', 'au', 'ok', 'se', 'no', 'de'],
		
		positionOffset: {x: 5, y: 0}
	});
	}
	
	//});
}

// check outside element
function outsideElement(element,e){
	 var isClickInside = element.contains(e.target);
	 if (!isClickInside){
		 
		 return false;
	 } else {
		 return true;
	 }
}

/** FASTEST OWN ALERT */
function FstAlert(value,vibrate,modal){
	if (vibrate){
		if (navigator.vibrate)
		navigator.vibrate(50);
	}
	
	//console.log($(window));
	//console.log($(parent.window));
	new mBox.Notice({
		type: 'ok',
		target:((modal)?$(window.parent.document):$(window)),
		inject:((modal)?window.parent.document.body:null),
		fadeDuration: 500,
		delayClose:3000,
		position: {
				x: 'center',
				y: 'top'
			},
		content: value
	});
}

/** FASTEST OWN ERROR */
function FstError(value,modal){
	new mBox.Notice({
		type: 'error',
		target:((modal)?$(window.parent.document):$(window)),
		inject:((modal)?window.parent.document.body:null),
		fadeDuration: 500,
		delayClose:3000,
		position: {
				x: 'center',
				y: 'top'
			},
		content: value
	});
}


/** GYROSCOPE **/
function fst_gyro(){
	window.addEvent('domready',function(){
		   var background = $('test');
		   var test_move = $('test_move');
	background.setStyles({
		'background-size':($('body').getSize().x+20)+'px '+($('body').getSize().y+20)+'px' 
		//'background-size':0 
	});
	centerTop = $('body').getSize().y / 2 - test_move.getSize().y/2;
	centerLeft = $('body').getSize().x / 2 - test_move.getSize().x/2;

	maxX = $('body').getSize().x;
	maxY = $('body').getSize().y;

	test_move.setStyles({
		'top':$('body').getSize().y / 2 - test_move.getSize().y/2,
		'left':$('body').getSize().x / 2 - test_move.getSize().x/2,
	});

	move_stepX = window.getSize().x / test_move.getSize().x;
	move_stepY = window.getSize().y / test_move.getSize().y;

	elW = test_move.getSize().x;
	elH = test_move.getSize().y;

	console.log(move_stepX);
	console.log(move_stepY);
	window.addEventListener('deviceorientation', function(eventData) {
	  // Retrieving the front/back tilting of the device and moves the
	  // background in the opposite way of the tilt
	$('gx').set('text',Math.round(eventData.alpha));
	$('gy').set('text',Math.round(eventData.beta));
	$('gz').set('text',Math.round(eventData.gamma));


	moveTop = centerTop - Math.round(eventData.beta)*move_stepX*-1;
	moveLeft = centerLeft - Math.round(eventData.gamma)*move_stepY*-1;

	if (moveTop + elH > maxY){
		moveTop = maxY - elH;
		navigator.vibrate(10);
	}
	if (moveTop < 0){
		moveTop = 0;
		
		navigator.vibrate(10);
	}
	if (moveLeft + elW > maxX){
		moveLeft = maxX - elW;
		navigator.vibrate(10);
	}
	if (moveLeft < 0){
		moveLeft = 0;
		
		navigator.vibrate(10);
	}
	test_move.setStyles({
		'top':moveTop,
		'left':moveLeft,
	});
	  //var yTilt = Math.round((-eventData.beta + 90) * (40/180) - 40);
	  var yTilt = Math.round((-eventData.beta + 90) * (40/180) - 20);

	  // Retrieve the side to side tilting of the device and move the
	  // background the opposite direction.

	  //var xTilt = Math.round(-eventData.gamma * (10/180) - 10);
	  var xTilt = Math.round(eventData.gamma * (50/180) - 10);

	  // Thi 'if' statement checks if the phone is upside down and corrects
	  // the value that is returned.
	  /*
	  if (xTilt &amp;gt; 0) {
		xTilt = -xTilt;
	  } else if (xTilt &amp;lt; -40) {
		xTilt = -(xTilt + 80);
	  }
	*/
	  var backgroundPositionValue = yTilt + 'px ' + xTilt + 'px';
	$('gp').set('text',backgroundPositionValue);

	  background.style.backgroundPosition = backgroundPositionValue;
	}, false);
	});

}