(function () {

Object.append(Element.NativeEvents, {
	dragenter: 2, dragleave: 2, dragover: 2, dragend: 2, drop: 2
});

var FstUploader = this.FstUploader = new Class({
	Implements:[Options,Events],
	options: {
		multi : false,
		count_file : 20,
		file_ext : ['jpg'],
		paths:	{
			img_path	: '/uploaded/logos/',
			upload_script	:'/fst_upload/',					
		}
	},
	
	/* initialization */
	initialize: function(element,options) {
		//clearInterval(this.options.mrk2); 
		
		if( window.FormData === undefined ){
			//console.log(element);
			new Element('var').set('text','Váš prohlížeč nepodporuje nahrávání souborů').inject(element,'after');
			element.addClass('none');
			//(FstError)?FstError('Váš prohlížeč nepodporuje nahrávání souborů'):alert('Váš prohlížeč nepodporuje nahrávání souborů');
			
		} else {
		
				
		/* set options */
		this.element = element;
		this.setOptions(options);
		//console.log(this.options);
		
		this.create_over_input();
		this.drop_events();
		this.input_events();
		
		this.uploaded_files();
		
		}
	},
	
	uploaded_files: function(){
		this.options.uploaded_files = JSON.decode(this.options.uploaded_files);
		if (this.options.uploaded_files){
			var data = this.options.uploaded_files;
			if (data != 'false' && data){
			
			data.each((function(item){
				var rand = Math.floor((Math.random() * 100000));
				
				//console.log(file);
						
				this.ParseFile(item,rand,'uploaded');
						
				
			}).bind(this));
			}
			this.element.value = '';
		} else {
			this.element.value = '';
		
		}
	},
	
	
	b64_encode: function( str ) {
	  return window.btoa(unescape(encodeURIComponent( str )));
	},

	b64_decode: function( str ) {
	  return decodeURIComponent(escape(window.atob( str )));
	},
	
	create_over_input: function(){
		
		this.obal = new Element('div',{}).inject(this.element,'before');
		this.obal.adopt(this.element);
		
		//console.log(this.element.getStyle('width'));
		//window.addEvent('load', (function(){
			this.obal.setStyles({
				'position':'relative',
				'display':'inline-block',
				//'float':'left',
				'width':this.element.getStyle('width'),
			});
			this.obal.addClass('file_upload');
			this.element.setStyle('width','100%');
		//}).bind(this));
		
			var dropable = new Element('div',{'class':'dropable'}).inject(this.obal,'bottom');
			dropable.set('text','Přetáhněte zde soubory');
			dropable.setStyles({
				'width':'100%',
				'clear':'both',
				'position':'relative',
				'line-height':35,
				'height':35,
				'text-align':'center',
				'background':'#EAE8E8',
				'color':'#666',
				'border':'1px solid #ccc',
				'font-size':'11px',
			});
			
		this.icon = new Element('div',{}).inject(this.element,'after');
		this.icon.setStyles({
			'position':'absolute',
			'top':5,
			'right':0,
			'cursor':'pointer',
			'display':'block',
			'width':16,
			'height':16,
			'background':'url(\'/js/fst_uploader/upload_browse.png\') no-repeat right center',
		});
		
		this.file_fake = file_fake = new Element('input',{'type':'file'}).inject(this.element,'after');
		file_fake.setStyles({
			'cursor':'pointer',
			'opacity':0,
			'position':'absolute',
			'z-index':20,
			'top':0,
			'right':0,
		});
		
		file_fake.addEvent('change',(function(e){
			
			this.element.value = e.target.value;
		}).bind(this));
		
		
		
		
		
	},
	// file drag hover
	FileDragHover: function(e,blink) {
		
		
		clearInterval(this.hover_timer);
		/*
		
		*/
		if (blink){
			this.hover_timer = (function(){
			
				e.target.set('tween',{
					'duration':150,
					'onComplete':function(){
						e.target.tween('background-color','#EAE8E8');
						
					}
				})
				e.target.tween('background-color','#ccc');
			}).periodical(300);
		} else {
			/*
			if (!e.target.hasClass('dropable_hover')){
			
				e.target.addClass('dropable_hover');
			} else {	
				e.target.removeClass('dropable_hover');
			}
			*/
		}
	},
	
	
	
	
	// output file information
	ParseFile: function(file,rand,uploaded) {
		
		if (!this.obal.getElement('ul'))
			var file_info = new Element('ul',{}).inject(this.obal);
		else
			var file_info = this.obal.getElement('ul');
			
		file_info.setStyles({
		
			'position':'relative',
			'clear':'both',
		
		});
		
		// new line li
		this.li = new Element('li',{'id':'li'+rand}).inject(file_info);
		
		
		
		// delete file icon
		delete_file = new Element('a',{'href':'#','class':'delete_file','title':'Smazat'}).set('text','X').inject($('li'+rand),'bottom');
		delete_file.addEvent('click',(function(e){
			e.stop();
			if (confirm('Opravdu smazat položku?')){
				this.dispose_line(rand);
			
			}
		}).bind(this));
		
		
		// display input
		if (this.options.multi == false){
			this.file_uploaded = new Element('input',{'class':'uploaded_file','type':'hidden','name':this.element.getProperty('name')}).inject($('li'+rand));
			
		} else {
			this.file_uploaded = new Element('input',{'class':'uploaded_file','type':'hidden','name':this.element.getProperty('name')+'[]'}).inject($('li'+rand));
		
		}
		
		// uploaded files data from server
		if (uploaded){
			this.file_uploaded.value = JSON.encode(file);
			file_tmp = file;
			file = [];
			file[0] = file_tmp.file;
			file[1] = file_tmp.name;
			//console.log(file);
			
			//file = file.split('|');
			imgs = ['jpg','png','gif'];
			flnms = file[0].split('.');
			ext = flnms[flnms.length-1];
			
			
			new Element('span',{}).set('text','aa').inject($('li'+rand));
			
			$('li'+rand).getElement('span').set('text','');
			
			link = new Element('a',{'href':'/uploaded'+this.options.paths.img_path+file[0],'target':'_blank','title':'Stáhnout'}).set('text',file[1]).inject($('li'+rand).getElement('span'));
			
			//new Element('div',{'class':'clear'}).inject($('li'+rand));
			
			if (imgs.indexOf(ext.toLowerCase()) != -1){
				new Element('img',{'width':50,'src':'/uploaded'+this.options.paths.img_path+file[0]}).inject($('li'+rand));
				new Element('div',{'class':'clear'}).inject($('li'+rand));
			}
			
			
				
			
		// after upload local data
		} else {
			new Element('span',{}).set('text',file.name).inject($('li'+rand));
		
			$('li'+rand).getElement('span').set('text',file.name);
		
		
		
		
		
			// display an image
			if (typeof FileReader !== "undefined")
			if (file.type.indexOf("image") == 0) {
				var reader = new FileReader();
				reader.onload = (function(e) {
					new Element('img',{'width':50,'src':e.target.result}).inject($('li'+rand));
					new Element('div',{'class':'clear'}).inject($('li'+rand));
					
				}).bind(this)
				reader.readAsDataURL(file);
			}
			/*
			// display text
			if (file.type.indexOf("text") == 0) {
				var reader = new FileReader();
				reader.onload = (function(e) {
					
					
					new Element('span').set('text',e.target.result.replace(/</g, "&lt;").replace(/>/g, "&gt;")).inject($('li'+rand));
					new Element('div',{'class':'clear'}).inject($('li'+rand));
				}).bind(this)
				reader.readAsText(file);
			}
			*/
		}
		
		
		
		
	},


	// upload JPEG files
	UploadFile: function(file,rand) {
		// following line is not necessary: prevents running on SitePoint servers
		if (location.host.indexOf("sitepointstatic") >= 0) return
		
		var xhr = new Array();
		
		xhr[rand] = new XMLHttpRequest();
			var params = {
				'file_ext':this.options.file_ext,
				'count_file':this.options.count_file,
				'upload_path':this.options.paths.img_path?'/uploaded'+this.options.paths.img_path:'/uploaded/',
			}
			params_encode = this.b64_encode(JSON.encode(params));
			// upload form tmp
			var upload_form = new Element('form',{
				action	:	this.options.paths.upload_script+'?params='+params_encode,
				method	:	"POST",
				enctype	:	"multipart/form-data",
				encoding:  "multipart/form-data",
				target	:	"upload_iframe"
			}).inject(this.obal);
			
			
			this.icon.setStyle('background','url(\'/js/fst_uploader/progress_preloader.gif\') no-repeat right center');
			
			// create progress bar
			this.progress = new Element('div',{'class':'progress'}).inject($('li'+rand));
			this.progress.setStyles({
				'position':'absolute',
				'bottom':0,
				'left':0,
				'border':'0px solid red',
				'width':'0%',
				'height':'3px',
				'z-index':20,
				'background':'#ccc'
			});
			
			// preloader text
			this.upload_preloader = new Element('div',{'class':'upload_preloader'}).inject($('li'+rand));
			this.upload_preloader.setStyles({
				'position':'absolute',
				'background-color':'#fff',
				'opacity':0.6,
				'top':0,
				'left':0,
				'text-align':'center',
				'width':'100%',
				'height':'100%',
				'line-height':25,
			});
			new Element('span').set('text','Počkejte na nahrání souboru...').inject(this.upload_preloader);
		
			// progress bar
			xhr[rand].upload.addEventListener("progress", (function(e) {
				var pc = parseInt((e.loaded / e.total * 100));
				
				
				$('li'+rand).getElement('.progress').setStyle('width',pc+'%');
			}).bind(this), false);
			
			
			// file received/failed
			xhr[rand].onreadystatechange = (function(e) {
				if (xhr[rand].readyState == 4) {
					if ($('li'+rand).getElement('.progress')){
						$('li'+rand).getElement('.progress').dispose();
					}
					if ($('li'+rand).getElement('.upload_preloader')){
						$('li'+rand).getElement('.upload_preloader').dispose();
					}
					this.icon.setStyle('background','url(\'/js/fst_uploader/upload_browse.png\') no-repeat right center');
					
					
					

				}
			}).bind(this);
			
			
			
			// start upload
			//console.log(unescape(encodeURIComponent(file.name)));
			xhr[rand].open("POST", upload_form.action, true);
			xhr[rand].setRequestHeader("X_FILENAME", unescape(encodeURIComponent(file.name)));
			
			var formData = new FormData();
			var inputname = 'file[]';
			formData.append(inputname , file);
			xhr[rand].setRequestHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			xhr[rand].send(formData);
			//xhr[rand].send(file);
			
			// oncomlete PHP upload
			xhr[rand].addEventListener("load", (function () {
				json = xhr[rand].response;
				json = JSON.decode(json);
				if (!json){
					alert('Chyba nahrání souboru');
				} else if (json.result == false){
					(typeof FstError != 'undefined')?FstError(json.message):alert(json.message);
					this.dispose_line(rand);
				} else {
					$('li'+rand).getElement('.uploaded_file').value = JSON.encode({'file':json.uploaded_file,'name':json.original_file});
					
					name = $('li'+rand).getElement('span').get('text');
					$('li'+rand).getElement('span').set('text','');
					link = new Element('a',{'href':'/uploaded'+this.options.paths.img_path+json.uploaded_file,'target':'_blank','title':'Stáhnout'}).set('text',name).inject($('li'+rand).getElement('span'));
				}
				
			}).bind(this), false);

		

	},
	
	dispose_line: function(rand){
		if ($('li'+rand)){
			var params = {
				'file':$('li'+rand).getElement('.uploaded_file').value,
				'upload_path':this.options.paths.img_path?'/uploaded'+this.options.paths.img_path:'/uploaded/',
			}
			params_encode = this.b64_encode(JSON.encode(params));
			
			
			var del_reg = new Request.JSON({
				url:'/fst_upload/delete/?params='+params_encode,	
				onComplete:function(json){
					
				}
			})
			del_reg.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			del_reg.send();
			
			$('li'+rand).set('tween',{
				'onComplete':function(){
					$('li'+rand).dispose();
				}
			});
			$('li'+rand).fade(0);
			
		}
	},
	
	// dataTransfer files
	data_transfer: function(e){
		var dataTransfer = e.target.files || e.event.dataTransfer.files;
				
				
		//console.log(dataTransfer);		
		this.FileDragHover(e);
		 if (this.obal.getElement('li')){
					var count_uploaded = this.obal.getElements('.uploaded_file').length;
				} else {
					var count_uploaded = 0;
				}
		if (dataTransfer){
			// hlidani pocet souboru
			//this.check_count();
					
			if (!this.disable_upload){
				// e.target.files :: from select file,  e.event.dataTransfer.files :: from drop area
				var files = e.target.files || e.event.dataTransfer.files;
				var file_count = files.length;
				
				 
				var celkem = file_count.toInt()+count_uploaded.toInt();
				
				// limit count files
				if (celkem > this.options.count_file){
					(typeof FstError != 'undefined')?FstError('Překročen limit souborů, maximálně povoleno '+this.options.count_file+' ks'):alert('Překročen limit souborů, maximálně povoleno '+this.options.count_file+' ks');
					
						
				} else {
					//foreach files to upload
					for (var i=0, l; i<file_count; i++) {
						var rand = Math.floor((Math.random() * 100000));
						
						flnms = files[i].name.split('.');
						ext = flnms[flnms.length-1];
						
						// check file ext if posibility
						if (this.options.file_ext.indexOf(ext.toLowerCase()) != -1){
							this.ParseFile(files[i],rand);
							this.UploadFile(files[i],rand);
						} else {
							(typeof FstError != 'undefined')?FstError('Nepovolený typ souboru'):alert('Nepovolený typ souboru');
						
						}
						
					}
				
				}
				this.file_fake.value = '';
				this.element.value = '';
				
			}
					
		}
	
	},
	
	// file input events
	input_events:function(){
		this.file_fake.addEvent('change',(function(e){
			this.data_transfer(e);
		}).bind(this));
	
	
	},
	
	// drop events
	drop_events:function(){
		$$('.dropable').removeEvents('dragenter');
		$$('.dropable').removeEvents('dragleave');
		$$('.dropable').removeEvents('drop');
		$$('.dropable').removeEvents('dragover');
		
		$$('.dropable').addEvent('dragenter',(function(e){this.FileDragHover(e,'blink');}).bind(this));		
		$$('.dropable').addEvent('dragleave',(function(e){this.FileDragHover(e);}).bind(this));
			
		$$('.dropable').addEvent('drop',function(e){
				e.preventDefault();
				this.data_transfer(e);
		}.bind(this));
			
		$$('.dropable').addEvent('dragover',function(e){
			e.preventDefault();
		});
	},
	
		
	
});

})();	