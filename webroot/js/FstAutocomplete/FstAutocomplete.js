/**
 * Created by Fastest Solution s.r.o. Jakub Tyson on 10.1.2017.
 global pages function
 */
 
 var FstAutocomplete = this.FstAutocomplete = new Class({
	Implements:[Options,Events],
	options: {
		'result_class':'auto_search_result',
		'result_class_element':true,
		'result_class_template':'<div class="search_item products"></div><div class="sep"></div><div class="search_item categories"></div>',
		'search_length':2,
		'search_request':'/products_search/',
	},
	
	// init fce
	initialize:function(options){
		this.setOptions(options);
		
		this.init_events();
		this.body_events();
		//this.search_result();
		//console.log(lazyload);
	},
	
	// init events
	init_events: function(){
		this.options.element.setProperty('autocomplete','off');
		this.options.element.addEvent('keyup',this.keyup_event.bind(this));
		this.options.element.addEvent('keydown',this.keydown_event.bind(this));
	},
	
	// keyup event
	keyup_event: function(e){
		this.elementValue = this.options.element.value;
		if (this.elementValue.length >= this.options.search_length){
			this.search_request();
		} else {
			this.destroy_result();
		}
	},
	
	// keydown event
	keydown_event: function(e){
		if (e.key == 'enter'){
			return false;
		}
	},
	
	// search result
	search_result: function(){
		if(this.search_result_el){
			this.search_result_el.destroy();
			this.search_result_el = null;
		}
		
		
		this.search_result_el = new Element('div',{'class':this.options.result_class,'id':this.options.result_class}).set('html',this.options.result_class_template).inject(this.options.element,'after');
		this.search_result_el.fade('hide');
		this.search_result_el.fade(1);
		
		if (this.options.result_class_element){
			//console.log(this.options.element.getPosition());
			this.search_result_el.setStyles({
				//'top':this.options.element.getCoordinates().bottom - getScrollOffsets().y,
				'top':this.options.element.getPosition($('search_products')).y + this.options.element.getCoordinates().height,
				'left':this.options.element.getCoordinates().x,
				'width':this.options.element.getCoordinates().width,
			});
		}
		
		
	},
	
	// create separator
	create_separator: function(){
		this.search_result_el.getElements('.sep').each(function(sep){
			sep.setStyle('height',this.search_result_el.getSize().y);
		}.bind(this));
	},
	
	// preloader
	preloader: function(){
		if (!this.search_preloader){
			this.search_preloader = new Element('div',{'class':'search_preloader'}).inject(this.options.element,'after');
			this.search_preloader.setStyles({
				'position':'fixed',
				'top':this.options.element.getCoordinates().top + ((this.options.element.getCoordinates().height - this.search_preloader.getCoordinates().height)/2)  - getScrollOffsets().y,
				'left':this.options.element.getCoordinates().right - this.search_preloader.getCoordinates().width - 5,
			});
			if (this.search_result_el){
				this.search_preloader_content = new Element('div',{'class':'search_preloader_content'}).inject(this.search_result_el);
			}
		}
	},
	
	// remove preloader
	remove_preloader: function(){
		if (this.search_preloader){
			this.search_preloader.destroy();
			this.search_preloader = null;
		}
	},
	
	// search request
	search_request: function(){
		this.preloader();
		
		if (this.req_form){
			this.req_form.cancel();
		}
		
		this.req_form = new Request.JSON({
			url:this.options.search_request+this.elementValue,
			onError: this.req_error = (function(data){
				
			}).bind(this),

			onComplete :(function(json){
				this.remove_preloader();
		
				if (json.r == true){
					//console.log(this.search_result_el.getElement);	
					//console.log(json);
					this.search_result();
					//console.log(this.search_result_el);
					Object.each(json.data,function(group,class_el){
						if (this.search_result_el.getElement('.'+class_el)){
							Object.each(group,function(item,key){
								line = new Element('div',{'class':((item.title)?'search_title':'search_line')}).inject(this.search_result_el.getElement('.'+class_el));
								if (item.title){
									line.set('text',item.name);
								} else {
										
									if (item.name){
										line.set('data-name',item.name);
										line.set('text',item.name);
									}
									if (item.url){
										line.set('data-url',item.url);
									}
									//console.log(item);
									if (item.img){
										
										img_div = new Element('div',{'class':'img','data-original':item.img}).inject(line);
										img = new Element('img',{'class':'img lazy preloader','data-original':item.img}).inject(img_div);
										img.setProperties({
											'width':img.getStyle('width'),
											//'height':img.getStyle('height')
										});
										img = new Element('div',{'class':'clear'}).inject(line);
										lazzy_load(this.options.result_class);
									}
									if (!item.noresult){
										line.addEvent('click',this.line_click.bind(this));
									} else {
										line.addClass('noresult');
									}
								}
								this.create_separator();
							}.bind(this));
						}
					}.bind(this));
				}
				if (json.r == false){
				}
			}).bind(this)
		});
		this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
		this.req_form.send();
	},
	
	
	// destroy search result element
	destroy_result: function(){
		if (this.search_result_el){
			this.search_result_el.set('tween',{
				onComplete: function() {
					this.search_result_el.destroy();
					this.search_result_el = null;
					
				}.bind(this)
			});
			this.search_result_el.fade(0);
		}
	},
	
	// body click
	body_events: function(){
		$('body').addEvent('click',function(e){
			 if (this.search_result_el){
				 if(!outsideElement($(this.options.result_class),e)) {
					this.destroy_result();
				 }
			 }
			 
		}.bind(this));
	},
	
	
	// search_result click
	line_click: function(e){
		// pokud ma url presmeruj
		if (e.target.get('data-url')){
			window.location = e.target.get('data-url');
			this.destroy_result();
		} else {
			this.options.element.value = e.target.get('data-name');
			this.destroy_result();
		}
		
	},
});