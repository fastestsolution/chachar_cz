//(function () {
var FstMap = this.FstMap = new Class({
	Implements:[Options,Events],
	
	input_name: 'AreaName',
	element: 'maparea_canvas',
	delete_button: 'delete-button',
	rename_button: 'rename-button',
	gen_button: 'gen_coords',
	
	colors : ['#1E90FF', '#FF1493', '#32CD32', '#FF8C00', '#4B0082','#3FBA59','#7B82E0','#E0C57B','#E0987B','#E07BDB'],
	
	polygons : {},
	colorButtons:[],
		
	
	// init fce
	initialize:function(options){
		this.setOptions(options);
		this.element = $(this.element);
		
		this.default_map();
		//this.default_map_events();
		/*
		if(options && options.cestaks){
			this.calc_routes();
		} else {
		
		
			this.init_draw_manager();
		}
		*/
		this.load_areas();
		
	},
	
	
	// create default map
	default_map: function(){
		var myLatLng = new google.maps.LatLng(49.6000, 16.9356);
		
		var myStyles =[
			{
				featureType: "poi",
				elementType: "labels",
				stylers: [
					  { visibility: "off" }
				]
			}
		];
		
		var myOptions = {
			zoom: 7,
			//panControl: pan_cntrl,
			center: myLatLng,
			scrollwheel: true,
			//mapTypeControl: false,
			//streetViewControl: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			styles: myStyles, 
			mapTypeControl: false,
			mapTypeControlOptions: {
				style: google.maps.MapTypeControlStyle.HORIZONTAL_BAR,
				position: google.maps.ControlPosition.BOTTOM_CENTER
			},
			zoomControl: true,
			zoomControlOptions: {
				style: google.maps.ZoomControlStyle.LARGE,
				position: google.maps.ControlPosition.RIGHT_CENTER
			},
			scaleControl: true,
			streetViewControl: true,
			streetViewControlOptions: {
				position: google.maps.ControlPosition.RIGHT_TOP
			}



		};
		if (Browser.Platform.ios || Browser.Platform.android){
			myOptions['disableDefaultUI'] = true;
		}
		
		this.GoogleMap = new google.maps.Map(this.element, myOptions);
				
	},
	
	// add default map events
	default_map_events: function(){
		google.maps.event.addListener(this.GoogleMap, 'idle', (function() {
			//console.log(this.current_marker.title);
			//google.maps.event.trigger(this.current_marker,"click");
		}).bind(this));
		
	},
	
	load_areas: function(){
		var data = $('marea_map_data_load').get('text');
		var pokladna_list = $('pokladna_list').get('text');
		var data_json = JSON.decode(data);
		var pokladna_list_json = JSON.decode(pokladna_list);
		//console.log(data_json);
		//console.log(pokladna_list_json);
		
		this.polygons = {};
		polygons_array = [];
		
		
		
		Object.each(data_json,(function(system_data,system_id){
		if (pokladna_list_json[system_id]){
			new Element('li',{'data-id':system_id,'class':'select_area'}).set('text',pokladna_list_json[system_id]).inject($('maparea_name'));
		}
		Object.each(system_data,(function(item,k){
		//console.log(item.color);
		//for (var i = 0; i < polyLayer.length; i++) {
			poly = new google.maps.Polygon({
				strokeWeight:0,
				fillOpacity:0.45,
				fillColor:item.color,
				//editable: true,
				path: google.maps.geometry.encoding.decodePath(String(item.coords)), 
				//levels: decodeLevels(String(polyLayer[i][1])), 
			}); 
			if (!this.polygons[k]){ 
			
				this.polygons[k] = {
					'shape':poly,
					'name':item.name,
					'color':item.color,
					'system_id':system_id
				};
			
			}
			
			
			poly.uid = k;
			polygons_array.push(poly);
			
			
			
			this.clearSelection();	
			//this.setSelection(poly);
			//console.log(poly);	
			
			poly.setMap(this.GoogleMap);
		}).bind(this));
		}).bind(this));
		
		$$('.select_area').addEvents({
			mouseenter: function(e){
				system_id = e.target.get('data-id');
				Object.each(this.polygons,(function(item){
					if (system_id == item.system_id){
						item.shape.setMap(this.GoogleMap);
					
					} else {
						item.shape.setMap(null);
					}
				}).bind(this));
				//console.log(e.target);
			}.bind(this),
			mouseleave: function(e){
				
			}.bind(this),
		});
		
		
		
		polygons_array.each((function(poly,k){
			
			google.maps.event.addListener(poly, 'click', (function(e) {
				this.setSelection(polygons_array[k]);
			}).bind(this));
			
		}).bind(this));
		
		//$('AreaCoords').value = JSON.encode(data);
		//$('coords').set('html',JSON.encode(data));
		this.search_map();	
		//google.maps.event.addListener(this.GoogleMap, 'click', this.clearSelection.bind(this));
		//console.log(this.polygons);
	},
	
		search_map: function(){
			

			// Create the search box and link it to the UI element.
			var input = document.getElementById('street');
			var searchBox = new google.maps.places.SearchBox(input);
			this.GoogleMap.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

			// Bias the SearchBox results towards current map's viewport.
			this.GoogleMap.addListener('bounds_changed', function() {
			  searchBox.setBounds(this.GoogleMap.getBounds());
			}.bind(this));

			var markers = [];
			// Listen for the event fired when the user selects a prediction and retrieve
			// more details for that place.
			searchBox.addListener('places_changed', function() {
			  var places = searchBox.getPlaces();

			  if (places.length == 0) {
				return;
			  }

			  // Clear out the old markers.
			  markers.forEach(function(marker) {
				marker.setMap(null);
			  });
			  markers = [];

			  // For each place, get the icon, name and location.
			  var bounds = new google.maps.LatLngBounds();
			  places.forEach(function(place) {
				if (!place.geometry) {
				  console.log("Returned place contains no geometry");
				  return;
				}
				var icon = {
				  //url: place.icon,
				  size: new google.maps.Size(71, 71),
				  origin: new google.maps.Point(0, 0),
				  anchor: new google.maps.Point(17, 34),
				  scaledSize: new google.maps.Size(25, 25)
				};

				// Create a marker for each place.
				markers.push(new google.maps.Marker({
				  map: this.GoogleMap,
				  title: place.name,
				  position: place.geometry.location
				}));
				 markers.each(function(marker) {
					 console.log(this.GoogleMap);
					marker.setMap(this.GoogleMap);
				  }.bind(this));
				//console.log(markers);
				//console.log(place.geometry.location);

				if (place.geometry.viewport) {
				  // Only geocodes have viewport.
				  bounds.union(place.geometry.viewport);
				} else {
				  bounds.extend(place.geometry.location);
				}
			  }.bind(this));
			  this.GoogleMap.fitBounds(bounds);
			}.bind(this));
		  

		},
	// init draw manager
	init_draw_manager: function(){
		var polyOptions = {
		  strokeWeight: 0,
		  fillOpacity: 0.45,
		  editable: true
		};
			
		// Creates a drawing manager attached to the map that allows the user to draw
		// markers, lines, and shapes.
		
		drawingManager = new google.maps.drawing.DrawingManager({
			  //drawingMode: google.maps.drawing.OverlayType.POLYGON,
			  drawingMode: null,
			  markerOptions: {
				draggable: true
			  },
			  polylineOptions: {
				editable: true
			  },
			  rectangleOptions: polyOptions,
			  circleOptions: polyOptions,
			  polygonOptions: polyOptions,
			  map: this.GoogleMap,
			  drawingControlOptions: {
				  position: google.maps.ControlPosition.TOP_LEFT,
				  drawingModes: [
					//google.maps.drawing.OverlayType.MARKER,
					//google.maps.drawing.OverlayType.CIRCLE,
					google.maps.drawing.OverlayType.POLYGON,
					//google.maps.drawing.OverlayType.POLYLINE,
					//google.maps.drawing.OverlayType.RECTANGLE
				  ]
				},
			
		});
		
		google.maps.event.addListener(drawingManager, 'overlaycomplete', (function(e) {
				if (e.type != google.maps.drawing.OverlayType.MARKER) {
				// Switch back to non-drawing mode after drawing a shape.
				drawingManager.setDrawingMode(null);
				
				// Add an event listener that selects the newly-drawn shape when the user
				// mouses down on it.
				var newShape = e.overlay;
				var uid = String.uniqueID();
				newShape.uid = uid;
				newShape.type = e.type;
				google.maps.event.addListener(newShape, 'click', (function() {
				  this.setSelection(newShape);
				}).bind(this));
				
				this.setSelection(newShape);
				
				if (!this.polygons[uid]){
					this.polygons[uid] = {
						'shape':'',
						'name':'',
						'color':'#1E90FF'
					};
				}
				this.polygons[uid]['shape'] = newShape;
				this.window_create('name',uid);
				
			  }
		}).bind(this));

			// Clear the current selection when the drawing mode is changed, or when the
			// map is clicked.
			google.maps.event.addListener(drawingManager, 'drawingmode_changed', this.clearSelection.bind(this));
			google.maps.event.addListener(this.GoogleMap, 'click', this.clearSelection.bind(this));
			google.maps.event.addDomListener($(this.delete_button), 'click', this.deleteSelectedShape.bind(this));
			google.maps.event.addDomListener($(this.rename_button), 'click', this.renameShape.bind(this));
			google.maps.event.addDomListener($(this.gen_button), 'click', this.gen_coords.bind(this));
			
			google.maps.event.addListener(drawingManager, 'polygoncomplete', function (polygon) {
				//var coordinates = (polygon.getPath().getArray());
				var coordinates = (polygon.getPath().getArray());
				//console.log(coordinates);
			});
			this.buildColorPalette();
	},
	
	
	// gen json data to save mysql
	gen_coords: function(){
		
			var coords = {};
			
			Object.each(this.polygons,function(item,k){
				path = item.shape.getPath();
				encodeString = google.maps.geometry.encoding.encodePath(path);
				//console.log(encodeString);
				coords[k] = {
					'name':item.name,
					'color':item.color,
					'coords':encodeString
				};
			});
			
			$('AreaCoords').value = JSON.encode(coords);
			$('coords').set('html',JSON.encode(coords));
			
			new Request.JSON({
				url:$('FormArea').action,
				onComplete: function(json){
					if (json.result == true){
						FstAlert(json.message);
					
						
					} else {
						FstError(json.message);
					}
				}
			}).post($('FormArea'));
		
		
	},
	
	getLatLngByOffset: function( map, offsetX, offsetY ){
		var currentBounds = map.getBounds();
		var topLeftLatLng = new google.maps.LatLng( currentBounds.getNorthEast().lat(),
													currentBounds.getSouthWest().lng());
		var point = map.getProjection().fromLatLngToPoint( topLeftLatLng );
		point.x += offsetX / ( 1<<map.getZoom() );
		point.y += offsetY / ( 1<<map.getZoom() );
		return map.getProjection().fromPointToLatLng( point );
	},
	
	// window modal create
	window_create: function(type,uid){
		$('win_name').removeClass('none');
		$('win_name_inner').empty();
		
		if (type == 'name'){
			var input = new Element('input',{'type':'text','class':'text','placeholder':'Zadejte název oblasti'}).inject($('win_name_inner'));
			var button = new Element('input',{'class':'button','type':'button','value':'Zadat'}).inject($('win_name_inner'));
			input.focus();
			
			button.addEvent('click',(function(e){
				new Event(e).stop();
				this.polygons[uid]['name'] = input.value;
				
				$('win_name').addClass('none');
				
				$(this.input_name).value = this.polygons[this.selectedShape.uid].name;
	
	
			}).bind(this));
		}
	},
	
	
	
	// rename shape from input
	renameShape: function() {
			if (this.selectedShape) {
			  this.polygons[this.selectedShape.uid].name = $(this.input_name).value;
			  FstAlert('Oblast prejmenována');
			
			} else {
				FstError('Musíte vybrat oblast');
			}
	},

	// clear all selection
	clearSelection: function() {
			if (this.selectedShape) {
			  this.selectedShape.setEditable(false);
			  this.selectedShape = null;
			  $(this.input_name).value = '';
			}
	},

	// create selection shape
	setSelection: function(shape) {
		this.clearSelection();
		//console.log(shape);
		this.selectedShape = shape;
		shape.setEditable(true);
		this.selectColor(shape.get('fillColor') || shape.get('strokeColor'));
		
		if (this.polygons[this.selectedShape.uid])
		$(this.input_name).value = this.polygons[this.selectedShape.uid].name;
	},

	// delete select shape
	deleteSelectedShape: function() {
		if (this.selectedShape) {
			this.selectedShape.setMap(null);
			Object.erase(this.polygons,this.selectedShape.uid);
			FstAlert('Oblast smazána');
			// clear polygons array
		  
		} else {
			FstError('Musíte vybrat oblast');
		}
	},
	
	// select color from list
	selectColor: function(color,uid) {
			selectedColor = color;
			for (var i = 0; i < this.colors.length; ++i) {
			  var currColor = this.colors[i];
			  this.colorButtons[currColor].style.border = currColor == color ? '2px solid #789' : '2px solid #fff';
			}

			// Retrieves the current options from the drawing manager and replaces the
			// stroke or fill color as appropriate.
			var polylineOptions = drawingManager.get('polylineOptions');
			polylineOptions.strokeColor = color;
			drawingManager.set('polylineOptions', polylineOptions);

			var rectangleOptions = drawingManager.get('rectangleOptions');
			rectangleOptions.fillColor = color;
			drawingManager.set('rectangleOptions', rectangleOptions);

			var circleOptions = drawingManager.get('circleOptions');
			circleOptions.fillColor = color;
			drawingManager.set('circleOptions', circleOptions);

			var polygonOptions = drawingManager.get('polygonOptions');
			polygonOptions.fillColor = color;
			drawingManager.set('polygonOptions', polygonOptions);
			
			
			
			
	},
	
	// set selected color to shape
	setSelectedShapeColor: function(color) {
			if (this.selectedShape) {
			  if (this.selectedShape.type == google.maps.drawing.OverlayType.POLYLINE) {
				this.selectedShape.set('strokeColor', color);
			  } else {
				this.selectedShape.set('fillColor', color);
			  }
			 
			  this.polygons[this.selectedShape.uid]['color'] = color;
			}
	},
	
	// create color button
	makeColorButton: function(color) {
			var button = document.createElement('span');
			button.className = 'color-button';
			button.style.backgroundColor = color;
			google.maps.event.addDomListener(button, 'click', (function() {
			  this.selectColor(color);
			  this.setSelectedShapeColor(color);
			}).bind(this));

			return button;
	},

	// create color palete 
	buildColorPalette: function() {
			 var colorPalette = $('color-palette');
			 for (var i = 0; i < this.colors.length; ++i) {
			   var currColor = this.colors[i];
			   var colorButton = this.makeColorButton(currColor);
			   colorPalette.appendChild(colorButton);
			   this.colorButtons[currColor] = colorButton;
			 }
			 this.selectColor(this.colors[0]);
	},
	
	
	
	calc_routes: function(){
		var directionsDisplay;

		
		var directionsService = new google.maps.DirectionsService();
		
		$$('.clear_value').each(function(item){
			item.value = '';
		})
		

		
		directionsDisplay = new google.maps.DirectionsRenderer();
		  var input = /** @type {HTMLInputElement} */(
			  document.getElementById('pac-input'));
		 
			directionsDisplay.setMap(this.GoogleMap);

		  var defaultBounds = new google.maps.LatLng(new google.maps.LatLng($('SettingGpsLat').value, $('SettingGpsLng').value));
		  var options = {componentRestrictions: {country: 'cz'}, location: defaultBounds,radius:20};
		  var autocomplete = new google.maps.places.Autocomplete(input,options);
		  
		  autocomplete.bindTo('bounds', this.GoogleMap);
		  
		  var infowindow = new google.maps.InfoWindow();
		  var marker = new google.maps.Marker({
			map: this.GoogleMap
		  });

		  google.maps.event.addListener(autocomplete, 'place_changed', (function() {
			infowindow.close();
			marker.setVisible(false);
			var place = autocomplete.getPlace();
			if (!place.geometry) {
			  return;
			}

			// If the place has a geometry, then present it on a map.
			if (place.geometry.viewport) {
			  this.GoogleMap.fitBounds(place.geometry.viewport);
			 	
			} else {
			  this.GoogleMap.setCenter(place.geometry.location);
			  this.GoogleMap.setZoom(17);  // Why 17? Because it looks good.
				
			}
			marker.setIcon(/** @type {google.maps.Icon} */({
			  url: place.icon,
			  size: new google.maps.Size(71, 71),
			  origin: new google.maps.Point(0, 0),
			  anchor: new google.maps.Point(17, 34),
			  scaledSize: new google.maps.Size(35, 35)
			}));
			marker.setPosition(place.geometry.location);
			marker.setVisible(true);
			
			calcRoute(place.geometry.location);
			
			
			var address = '';
			if (place.address_components) {
			  address = [
				(place.address_components[0] && place.address_components[0].short_name || ''),
				(place.address_components[1] && place.address_components[1].short_name || ''),
				(place.address_components[2] && place.address_components[2].short_name || '')
			  ].join(' ');
			}

			infowindow.setContent('<div><strong>' + place.name + '</strong><br>' + address);
			infowindow.open(this.GoogleMap, marker);
		  }).bind(this));
		  
		  this.markersArray = [];
		  
		 
		  
		 
		  
		  google.maps.event.addListener(this.GoogleMap, "click", (function(event)
            {
				this.deleteOverlays();
                // place a marker
                var marker = new google.maps.Marker({
					position: event.latLng, 
					map: this.GoogleMap
				});

				// add marker in markers array
				this.markersArray.push(marker);
				calcRoute(event.latLng);
                // display the lat/lng in your form's lat/lng fields
                $("GoogleLat").value = event.latLng.lat();
                $("GoogleLng").value = event.latLng.lng();
            }).bind(this));
		  
		//calcRoute();
  
			
		 
					
		function calcRoute(point) {
			if ($("GoogleLat").value != ''){
			
		  if ($("GoogleLat").value == ''){
			//var start = new google.maps.LatLng($('SettingGpsLat').value, $('SettingGpsLng').value);
		  
		  } else {
			var start = new google.maps.LatLng($('GoogleLat').value, $('GoogleLng').value);
		  
		  }
		  
		  var end =point;
		  var request = {
			origin:start,
			destination:end,
			travelMode: google.maps.TravelMode.DRIVING
		  };
		  directionsService.route(request, function(result, status) {
			if (status == google.maps.DirectionsStatus.OK) {
			  //console.log(result.routes[0].legs[0]);
			  
              $("GoogleStartAdr").value = result.routes[0].legs[0].start_address;
              $("GoogleEndAdr").value = result.routes[0].legs[0].end_address;
              $("GoogleKm").value = result.routes[0].legs[0].distance.text;
              $("GoogleKmValue").value = result.routes[0].legs[0].distance.value;
			  directionsDisplay.setDirections(result);
			  
			  add_route();
			}
		  });
		  
		  }
		}
		
		$('PrintCestak').addEvent('click',function(e){
			new Event(e).stop();
			window.print();
		});
		
		function add_route(){
			var table = $('cestak_list').getElement('tbody');
			var tr = new Element('tr',{}).inject(table);
				var td1 = new Element('td',{}).inject(tr);
				var td2 = new Element('td',{}).inject(tr);
				var td3 = new Element('td',{}).inject(tr);
				var td4 = new Element('td',{}).inject(tr);
				
				var km_all = $('celkem_km').get('text').toInt();
				
				td1.set('text',$("GoogleStartAdr").value);
				td2.set('text',$("GoogleEndAdr").value);
				td3.set('text',$("GoogleKm").value);
				
				var km = new Element('input',{'class':'km_value','type':'hidden','value':$("GoogleKm").value.toInt()}).inject(td3);
				
				
				km_all += $("GoogleKm").value.toInt();
				
				$('celkem_km').set('text',km_all);
				
				var del = new Element('a',{'href':'#','class':'noprint'}).set('text','Smazat').inject(td4);
				
				del.addEvent('click',function(e){
					new Event(e).stop();
					//console.log(this.getParent('tr').getElement('.km_value').value);
					$('celkem_km').set('text',km_all - this.getParent('tr').getElement('.km_value').value);
					this.getParent('tr').destroy();
				});
			
		}
		
	
	},
	
	deleteOverlays: function() {
		if (this.markersArray) {
			this.markersArray.each(function(marker,k){
				
				marker.setMap(null);
			});
			this.markersArray.length = 0;
		}
	},
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	draw_manager_default: function(){
		
	  var drawingManager = new google.maps.drawing.DrawingManager({
		drawingMode: google.maps.drawing.OverlayType.MARKER,
		drawingControl: true,
		markerOptions: {
			draggable: true
		},
		polylineOptions: {
			editable: true
		},
		polygonOptions: {
			editable: true
		},
		
		drawingControlOptions: {
		  position: google.maps.ControlPosition.TOP_CENTER,
		  drawingModes: [
			//google.maps.drawing.OverlayType.MARKER,
			//google.maps.drawing.OverlayType.CIRCLE,
			google.maps.drawing.OverlayType.POLYGON,
			google.maps.drawing.OverlayType.POLYLINE,
			//google.maps.drawing.OverlayType.RECTANGLE
		  ]
		},
		markerOptions: {
		  //icon: 'images/beachflag.png'
		},
		circleOptions: {
		  fillColor: '#ffff00',
		  fillOpacity: 1,
		  strokeWeight: 5,
		  clickable: true,
		  editable: true,
		  zIndex: 1
		}
	  });
	  drawingManager.setMap(this.GoogleMap);
	  
	  google.maps.event.addListener(drawingManager, 'polygoncomplete', function (polygon) {
        //var coordinates = (polygon.getPath().getArray());
        var coordinates = (polygon.getPath().getArray());
        console.log(coordinates);
      });
	}
	
	/*
	draw_manager: function(){
		
		  function clearSelection() {
			if (selectedShape) {
			  selectedShape.setEditable(false);
			  selectedShape = null;
			}
		  }

		  function setSelection(shape) {
			clearSelection();
			selectedShape = shape;
			shape.setEditable(true);
			selectColor(shape.get('fillColor') || shape.get('strokeColor'));
		  }

		  function deleteSelectedShape() {
			if (selectedShape) {
			  selectedShape.setMap(null);
			}
		  }

		  function selectColor(color) {
			selectedColor = color;
			for (var i = 0; i < colors.length; ++i) {
			  var currColor = colors[i];
			  this.colorButtons[currColor].style.border = currColor == color ? '2px solid #789' : '2px solid #fff';
			}

			// Retrieves the current options from the drawing manager and replaces the
			// stroke or fill color as appropriate.
			var polylineOptions = drawingManager.get('polylineOptions');
			polylineOptions.strokeColor = color;
			drawingManager.set('polylineOptions', polylineOptions);

			var rectangleOptions = drawingManager.get('rectangleOptions');
			rectangleOptions.fillColor = color;
			drawingManager.set('rectangleOptions', rectangleOptions);

			var circleOptions = drawingManager.get('circleOptions');
			circleOptions.fillColor = color;
			drawingManager.set('circleOptions', circleOptions);

			var polygonOptions = drawingManager.get('polygonOptions');
			polygonOptions.fillColor = color;
			drawingManager.set('polygonOptions', polygonOptions);
		  }

		  function setSelectedShapeColor(color) {
			if (selectedShape) {
			  if (selectedShape.type == google.maps.drawing.OverlayType.POLYLINE) {
				selectedShape.set('strokeColor', color);
			  } else {
				selectedShape.set('fillColor', color);
			  }
			}
		  }

		  function makeColorButton(color) {
			var button = document.createElement('span');
			button.className = 'color-button';
			button.style.backgroundColor = color;
			google.maps.event.addDomListener(button, 'click', function() {
			  selectColor(color);
			  setSelectedShapeColor(color);
			});

			return button;
		  }

		   function buildColorPalette() {
			 var colorPalette = document.getElementById('color-palette');
			 for (var i = 0; i < colors.length; ++i) {
			   var currColor = colors[i];
			   var colorButton = makeColorButton(currColor);
			   colorPalette.appendChild(colorButton);
			   this.colorButtons[currColor] = colorButton;
			 }
			 selectColor(colors[0]);
		   }

		  function init_draw_manager(){
			var polyOptions = {
			  strokeWeight: 0,
			  fillOpacity: 0.45,
			  editable: true
			};
			
			// Creates a drawing manager attached to the map that allows the user to draw
			// markers, lines, and shapes.
			drawingManager = new google.maps.drawing.DrawingManager({
			  drawingMode: google.maps.drawing.OverlayType.POLYGON,
			  markerOptions: {
				draggable: true
			  },
			  polylineOptions: {
				editable: true
			  },
			  rectangleOptions: polyOptions,
			  circleOptions: polyOptions,
			  polygonOptions: polyOptions,
			  map: this.GoogleMap
			});
			google.maps.event.addListener(drawingManager, 'overlaycomplete', function(e) {
				if (e.type != google.maps.drawing.OverlayType.MARKER) {
				// Switch back to non-drawing mode after drawing a shape.
				drawingManager.setDrawingMode(null);

				// Add an event listener that selects the newly-drawn shape when the user
				// mouses down on it.
				var newShape = e.overlay;
				newShape.type = e.type;
				google.maps.event.addListener(newShape, 'click', function() {
				  setSelection(newShape);
				});
				setSelection(newShape);
			  }
			});

			// Clear the current selection when the drawing mode is changed, or when the
			// map is clicked.
			google.maps.event.addListener(drawingManager, 'drawingmode_changed', clearSelection);
			google.maps.event.addListener(this.GoogleMap, 'click', clearSelection);
			google.maps.event.addDomListener($('delete-button'), 'click', deleteSelectedShape);

			buildColorPalette();
		  }
		  
		  this.init_draw_manager();
		
	},
	*/
	
	
});

//})();	
window.addEvent('domready', function () {
//var fstmap = new FstMap();
});
	function init_map(){
		var fstmap = new FstMap();
		
		
	}
	
	function init_map_cestaks(){
		var opt = {
			'cestaks':true
		}
		var fstmap = new FstMap(opt);
		
		
	}
	function loadScript(callback) {
		if (!callback) callback = 'init_map';
		  var script = document.createElement('script');
		  script.type = 'text/javascript';
		  script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=drawing,geometry,places&key=AIzaSyAmRIyQUbK5x_c_cm78ZV5ODf-sqVO1-2s' +
			  '&signed_in=true&callback='+callback;
		  document.body.appendChild(script);
		  
	}
	