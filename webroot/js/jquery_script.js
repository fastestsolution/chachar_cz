window.lazy_list = [];
window.lazyLoaded = false;
(function($){
var bootstrapLoaded = (typeof $().carousel == 'function');
var mootoolsLoaded = (typeof MooTools != 'undefined');
	
		// load lazzy load
		lazzy_load();
		// bootstrap JS
		if (bootstrapLoaded && mootoolsLoaded) {
			Element.implement({
				hide: function () {
					return this;
				},
				show: function (v) {
					return this;
				},
				slide: function (v) {
					return this;
				}
			});
		}
})(jQuery);


// form helper phone
function form_helper(){
	if ($$('.bfh-phone').length > 0){
	(function($){
		$.getScript( "/js/BootStrap/formhelpers-phone.js" )
			.done(function( script, textStatus ) {
				$.getScript( "/js/BootStrap/formhelpers-phone.format.js" )
					.done(function( script, textStatus ) {
					});
				
			})
			.fail(function( jqxhr, settings, exception ) {
				//$( "div.log" ).text( "Triggered ajaxError handler." );
			});
		
	})(jQuery);
	}
}

function load_lazy_load_js(fce){
	(function($){
	if (typeof($('img').lazyload) !== "function"){
		window.lazzy = $.getScript( "/js/LazzyLoad/lazzy_load.js" )
			.done(function( script, textStatus ) {
				fce();
				lazzy_load($('.lazy_force'));

			});
		
	}	
	})(jQuery);
}
// lazzy load images
function lazzy_load(el){
	
		
	(function($){
		function init_lazy(){
			//console.log('run');
			$(".lazy").lazyload({
				effect : "fadeIn",
				load : function(){
					$(this).removeClass('preloader');
					$(this).removeClass('lazy');
						
				}
			});
			
		}
		//console.log($(".lazy"));
		if (!el && typeof($('img').lazyload) !== "function"){
			if ($(".lazy").length > 0)
			load_lazy_load_js(init_lazy);
		} else {
			// prevod pole jquert mootools bez klicu
			el_list = [];
			el.each(function(k,item){
				if ($.isNumeric(k)){
					el_list.push(item);
				} else {
					el_list.push(k);
					
				}
			});
			
			if (typeof($('img').lazyload) === "function"){
				init_lazy();
			}
			//console.log(el_list);
			if(el_list.length == 1){
				$(el_list).trigger('appear');
			
			} else {
				el_list.each(function(item){
					/*
					console.log(item);
					console.log($(item));
					*/
					//init_lazy();
					$(item).trigger('appear');
				});
			}
			
		}
		/*
		//console.log(el);
		if (typeof el != 'undefined'){
			if(typeof el === 'string' ) {
				window.lazy_list.push(el);
			} else {
				window.lazy_list = el;
				
			}
			console.log(window.lazy_list);
			
		}
		
		
		if (typeof($('img').lazyload) !== "function"){
		$.getScript( "/js/LazzyLoad/lazzy_load.js" )
			.done(function( script, textStatus ) {
				
				$(".lazy").lazyload({
					effect : "fadeIn",
					load : function(){
						$(this).removeClass('preloader');
						$(this).removeClass('lazy');
						
					}
				});
				console.log(window.lazy_list);
				window.lazy_list.each(function(item){
					$(item).trigger('appear');
				});
					
				
				
			})
			.fail(function( jqxhr, settings, exception ) {
				//console.log('lazy error');
				//$( "div.log" ).text( "Triggered ajaxError handler." );
			});
		} else {
				$(".lazy").lazyload({
					effect : "fadeIn",
					load : function(){
						
						$(this).removeClass('preloader');
						$(this).removeClass('lazy');
						
			
					}
				});
				console.log(window.lazy_list);
				window.lazy_list.each(function(item){
					$(item).trigger('appear');
				});
				
			
		}
		*/
	})(jQuery);
	
}