/**
 * Created by Fastest Solution s.r.o. Jakub Tyson on 10.1.2017.
 global pages function
 */ 
window.records_ids = [22,2,10,24,25,26,32,28,46]; 
window.halusky_ids = [24,25,26]; 
window.debug = true; 
var FstPages = this.FstPages = new Class({
	Implements:[Options,Events],
	
	// init fce
	initialize:function(options){
		this.init_jquery();
		this.init_delay_scripts();
		this.introVanoce();
		
		this.init_adopt_element();
		this.init_elements_events();
		if (typeof init_chachar != 'undefined'){
			init_chachar();
		}
		this.show_spokojenost();
		this.vtip_list();
		this.txt_item();
		this.page_preload();
		//this.tips();
		this.scrollMobile();
		this.rozcestnik();
		//this.intro_adresa();
		//this.load_intro_adresa();
		this.change_provoz();
		this.check_orders();
		if ($('new_address') || $('intro_adresa')){
		this.load_map_data();
		this.thumb_photo();
		this.Strips_close_variants();
		this.loginPage();

		}
		this.hide_records();
		
		if ($('maparea_canvas')){
			console.log('aa');
			loadScript();
		}
		
		
				
		//FstError('Ahoj test');
		
	},
	
	loginPage: function(){
		if ($('loginPage') && $('login_line').getElement('.show_login')){
			$('show_login_el').getElement('.login_close').addClass('none');
			//$('show_login_el').set('data-height','1000');
			/*
			$('show_login_el').setStyles({
				'top':'0px',
				'width':'100%',
				'height':'100%',
			});
			*/
			$('login_line').getElement('.show_login').fireEvent('click');
		}
	},
	
	Strips_close_variants: function(){
		//console.log('strips close variants inited');

        if($('stripsSelect')){
            if($('stripsSelect').hasClass('none')){
                $$('.second').setStyle('pointer-events', 'none');
            }
        }

		/*else{
        	console.log('test');
            $$('.second').setStyle('pointer-events', 'all');
		}*/



		if($$('.next_step')){
            //var zIndexNeeded = $$('.next_step'getz
			$$('.next_step').addEvent('click', function(){
				//console.log('clicked button');
				this.getParent('.item').setStyle('z-index', '999999');

				$('close_variants').removeClass('hiddenCloseVariants');
                $$('.second').setStyle('pointer-events', 'all');
			});
		}
		if($('close_variants')){
			$('close_variants').addEvent('click', function(){
				//console.log('clicked background');
				$('close_variants').addClass('hiddenCloseVariants');
				$('stripsSelect').addClass('none');
                $$('.second').setStyle('pointer-events', 'none');
                $$('.next_step').getParent('.item').setStyle('z-index', '1');

            });
		}
		
	},
	
	
	
	countAddonLi: function(){
				if ($('nextMenu')){
					$('nextMenu').destroy();
				}
				if (this.stripsConfig.addons){
				//console.log(this.stripsConfig.addons.length);
				//console.log(this.stripsConfig['countAddon']);
				addonCount = (this.stripsConfig['countAddon'] - this.stripsConfig['addons'].length);
				}
				if (this.stripsConfig.addons && this.stripsConfig['addons'].length <= this.stripsConfig['countAddon']){
					li = new Element('li',{'class':'next','text':'Nyní si vybete '+addonCount+'x přílohu','id':'nextMenu'}).inject($('menuList'));
				} else {
					li = new Element('li',{'class':'next','text':'Nyní si vybete 2x přílohu','id':'nextMenu'}).inject($('menuList'));
					
				}
				if (addonCount == 0){
					
				}
				
	},
			
			
	
    thumb_photo: function() {
        $$('.cover_img').each(function(item) {
            var src = item.get('data-src');
            item.setStyles({
                'background-image':'url("'+src+'")',
            });
        });
    },
	introVanoce: function() {
		if($('close-intro')) { 
			$('close-intro').addEvent('click', function(e) {

				//$('overlay').hide();
				$('intro-vanoce').addClass('none');
				var myCookie  =  Cookie.write('confirm',true, {duration: 1});
			});	
		}
	},
	
	check_orders: function(){
		if ($('check_orders')){
			if ($('FindOrders')){
				if ($('body').getSize().x < 1000){
					$('EnterPhone').set('type','number');
				}
				
				$('FindOrders').addEvent('click',function(e){
					//alert('');
					button_preloader(e.target);
					e.target.getParent('form').submit();
				});
			}
			if ($('LoginCheckOrders')){
				$('LoginCheckOrders').addEvent('click',function(e){
				e.stop();
				button = e.target;
				button_preloader(button);
				req_form = new Request.JSON({
					url:e.target.getParent('form').action,
					onError: this.req_error = (function(data){
						button_preloader(button);
					}).bind(this),

					onComplete :(function(json){
						button_preloader(button);
						if (json.r == true){
							FstAlert(json.m);
							window.location = window.location+'?logged';
						} else {
							FstError(json.m);
						}
								
					}).bind(this)
				});
				req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
				req_form.post(e.target.getParent('form'));
				});
		   }
		   
		   if ($('FindClients')){
				$$('.show_client').addClass('none');
				$('FindClients').addEvent('click',function(e){
				$$('.clear_client').each(function(item){
					item.value = '';
				});
				e.stop();
				button = e.target;
				button_preloader(button);
				req_form = new Request.JSON({
					url:e.target.getParent('form').action,
					onError: this.req_error = (function(data){
						button_preloader(button);
					}).bind(this),

					onComplete :(function(json){
						button_preloader(button);
						if (json.r == true){
							
							$$('.show_client').removeClass('none');
							$('client-first-name').value = json.data.first_name;
							$('client-last-name').value = json.data.last_name;
							$('client-credits').value = json.data.credits;
							$('client-id').value = json.data.id;
							//FstAlert(json.m);
						} else {
							FstError(json.m);
							$$('.show_client').addClass('none');
						}
								
					}).bind(this)
				});
				req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
				req_form.post(e.target.getParent('form'));
				});
		   }
		   
		   if ($('EditClients')){
				$('EditClients').addEvent('click',function(e){
				e.stop();
				button = e.target;
				button_preloader(button);
				req_form = new Request.JSON({
					url:e.target.getParent('form').action,
					onError: this.req_error = (function(data){
						button_preloader(button);
					}).bind(this),

					onComplete :(function(json){
						button_preloader(button);
						if (json.r == true){
							FstAlert(json.m);
							$$('.clear_client').each(function(item){
								item.value = '';
							});
							$$('.show_client').addClass('none');
						} else {
							FstError(json.m);
							$$('.clear_client').each(function(item){
								item.value = '';
							});
							$$('.show_client').addClass('none');
						}
								
					}).bind(this)
				});
				req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
				req_form.post(e.target.getParent('form'));
				});
		   }
		}

	},
	
	// prepinani akce na HP
	akce_switch: function(){
		if ($('intro_akce').getElement('.akce_switch')){
			akce_list = $$('.akce_switch');
			index = 0;
			max_index = akce_list.length - 1;
			akce_list.each(function(item,k){
				//console.log(item); 
				if (k > 0){
					item.fade('hide');
					item.removeClass('none');
					
				}
				
			});
			
			(function(){
				akce_list[index].fade(0);
				index ++;
				if (index > max_index){
					index = 0;
				}
				
				akce_list[index].fade(1);
				
			}).periodical(7500);
		}
	},
	
	// zmena provozovny z leveho menu
	change_provoz: function(){
		if ($('change_provoz')){
			$('change_provoz').addEvent('click',function(e){
				$('provoz_select_list').removeClass('none');
				$('h2_provoz_name').addClass('none');
				window.history.pushState({}, document.title, window.location.pathname)
			});
		}
	},
	
	// schovani intro adresa
	hide_intro_adresa: function(){
		if ($('intro_adresa')){
			
			$('body').setStyle('overflow','auto');
			$('intro_adresa').destroy();
			if ($('provoz'+window.current_system_id)){
				$$('.provoz_select').removeClass('active');
				$$('.provoz_select_mobile').removeClass('active');
				$('provoz'+window.current_system_id).addClass('active');
				$('provoz_mobile'+window.current_system_id).addClass('active');
				provoz_name = $('provoz'+window.current_system_id).get('text');
				html = '<h2 id="h2_provoz_name">Změnit provozovnu: <strong id="change_provoz">'+provoz_name+'</strong></h2>';
				$('h2_provoz_name_inject').set('html',html);
				this.change_provoz();
			}
		}
	},
	
	// load request into adresa
	load_intro_adresa: function(){
		
		if ($('intro_adresa')){
			$('intro_adresa').setStyle('height','100%');
			
			$('body').setStyles({
				'overflow':'hidden',
				//'height':$('intro_adresa').getSize().y,
			});
		}
		
		if ($('search_address_load') || $('intro_adresa')){
			this.req_form = new Request.JSON({
				url:'/load_intro/',
				onError: this.req_error = (function(data){
				}).bind(this),

				onComplete :(function(json){
					//console.log(json);
					//console.log(json.redirect);
					$('redirect_system_ids').set('data-json',JSON.encode(json.redirect));
					$('map_coords').set('data-json',JSON.encode(json.map_coords_search));
					this.intro_adresa();
					this.init_google_autocomplete();
				}.bind(this))
			});
			this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			this.req_form.send();
		}
	},
	
	load_map_data: function(){
			this.req_form = new Request.JSON({
				url:'/load_intro/',
				onError: this.req_error = (function(data){
				}).bind(this),

				onComplete :(function(json){
					//console.log($('map_coords'));
					//console.log(json.redirect);
					//$('redirect_system_ids').set('data-json',JSON.encode(json.redirect));
					if ($('redirect_system_ids'))
					$('redirect_system_ids').set('data-json',JSON.encode(json.redirect));
					
					if ($('map_coords'))
					$('map_coords').set('data-json',JSON.encode(json.map_coords_search));
					
					
				}.bind(this))
			});
			this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			this.req_form.send();
	},
	
	// intro adresa
	intro_adresa: function(){
		//console.log('ccc');
		
		if ($('intro_adresa')){
			$('intro_adresa').setStyle('height','100%');
			//console.log('ddd');
		
			$('body').setStyles({
				'overflow':'hidden',
				//'height':$('intro_adresa').getSize().y,
			});
			
			$$('.clear_address').each(function(item){
				item.value = '';
			});
			
			this.akce_switch();
			
			$('address_clear').addEvent('click',function(e){
				$('address-value').value = '';
				$('address-value').focus();
			});
			
			$('address-value').addEvent('click',function(e){
				//e.stop();
				//alert(Browser.platform);
				//console.log(Browser.platform);
				if (Browser.platform != 'ios'){
					var myFx = new Fx.Scroll('intro_adresa').start(0, $('address-value').getCoordinates().top - 50);
				}
			});
			
			$('address-value').addEvent('keydown',function(e){
				//console.log('a');
				if (e.key == 'enter'){
					$('SearchAdresa').fireEvent('click',$('SearchAdresa'));
				}
			});
			
			
			$('SearchAdresa').addEvent('click',function(e){
				if (e.stop){
					e.stop();
				}
				
				if ($('address-value').value == ''){
					FstError('Musíte zadat adresu pro doručení. Děkujeme Vám...');
					return false;
				}
				if (!$('search_address_tip').hasClass('none')){
					//FstError('Musíte vybrat adresu ze seznamu. Děkujeme Vám...');
					//return false;
				}
				//console.log(window.polygons_search);
					
					lat = $('address-lat').value;
					lng = $('address-lng').value;
					ulice = $('address-street').value;
					//console.log(ulice);
					if (ulice == ''){
						
						button_preloader($('SearchAdresa'));
						
						this.gps_form = new Request.JSON({
							url:'/search_google_gps/'+$('address-value').value,
							onError: this.req_error = (function(data){
								button_preloader($('SearchAdresa'));
							}).bind(this),

							onComplete :(function(json){
								button_preloader($('SearchAdresa'));	
								//button_preloader(button);
								if (json.r == true){
									var item = $('address-value');
									
									if ($(item.get('data-mesto'))) $(item.get('data-mesto')).value = json.data.city;
									if ($(item.get('data-psc'))) $(item.get('data-psc')).value = json.data.zip;
									if ($(item.get('data-cp'))) $(item.get('data-cp')).value = json.data.cp+((json.data.co!='')?'/'+json.data.co:'');
									if ($(item.get('data-ulice'))) $(item.get('data-ulice')).value = json.data.street;
									if ($(item.get('data-lat'))) $(item.get('data-lat')).value = json.data.lat;
									if ($(item.get('data-lng'))) $(item.get('data-lng')).value = json.data.lng;
									/*
									if ($(item.get('data-cp')).value == ''){
										item_value = $('address-value').value.split(' ');
										//console.log(item_value);
										$('address-value').value = item_value[0];
									}
									*/
									
									this.parse_provoz_redirect(json.data.street,json.data.lat,json.data.lng);
					
								} else {
									FstError('Tuto adresu jsme nenašli. Prosím zkontrolujte, zda jste adresu zadali ve správném formátu (ULICE, Č.P. , MĚSTO)');
								}
										
							}).bind(this)
						});
						this.gps_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
						this.gps_form.post();
					} else {
						this.parse_provoz_redirect(ulice,lat,lng);
					}
					
					
				
				
			}.bind(this));
		}
	},
	
	parse_provoz_redirect: function(ulice,lat,lng){
			window.myURI = null;
			redirect_system_ids = JSON.decode($('redirect_system_ids').get('data-json'));
			//if ($('debug_js')){
				//alert('');
				//console.log(redirect_system_ids);
			
				//return false;
			//}
			function selectUrl(ulice,system_id){
				//console.log('system id',system_id);
				//console.log('ulice',ulice);
				if (ulice != ''){
					// pizza records
					if ($('intro_adresa').get('data-template') == 'records'){
						window.URI_params = {'provoz': system_id,records: 1}
										
					} else if ($('intro_adresa').get('data-template') == 'cina'){
						window.URI_params = {'provoz': system_id,cina: 1}
										
					} else {
						window.URI_params = {'provoz': system_id}
											
					}
					//url_redirect = 'http://'+redirect_system_ids[system_id]+'/?pr='+system_id;
				} else {
					if ($('intro_adresa').get('data-template') == 'records'){
						window.URI_params = {records: 1}
										
					}else if ($('intro_adresa').get('data-template') == 'cina'){
						window.URI_params = {cina: 1}
										
					} else {
					}
					//url_redirect = 'http://'+redirect_system_ids[system_id];
										
				}
					//console.log(window.URI_params);
					//return false;
					
					
			}
		
					lat = ''+lat;
					lng = ''+lng;
					find_result = false;
					url_current = null;
					url_redirect = null;
					latLng = new google.maps.LatLng(lat, lng);
					//console.log(lat);
					
					/*
					console.log(lat);
					console.log(lng);
					console.log(latLng);
					*/
					var address_chachar = new Hash.Cookie('addressChachar', {duration: 360});
					address_chachar.set('address_lat', $('address-lat').value);
					address_chachar.set('address_lng', $('address-lng').value);
					address_chachar.set('address_street', $('address-street').value);
					address_chachar.set('address_city', $('address-city').value);
					address_chachar.set('address_cp', $('address-cp').value);
					//console.log(address_chachar);
					window.URI_params = null;
					window.URI_noresult = null;
					window.current_site = false;
					//console.log(window.myURI);
					//console.log(window.polygons_search);
					//console.log(lat);
					//console.log(lng);
					//console.log(latLng);
					Object.each(window.polygons_search,function(system_data,system_id){
						//console.log(system_id);
						system_data.each(function(poly){
              //console.log(poly);
							if (google.maps.geometry.poly.containsLocation(latLng, poly)) {
								window.myURI = new URI('https://'+redirect_system_ids[system_id]);
									
								if (document.location.hostname == redirect_system_ids[system_id]){
									selectUrl(ulice,system_id);
									window.current_system_id = system_id;
									window.current_site = true;
									
									//console.log(current);
								} else {
									
									selectUrl(ulice,system_id);
									
									window.current_site = false;
								}
								
								
							} else {
								window.URI_noresult = true;
								//console.log('Nenalezeno');
							}
						});
					});
					//console.log(window.URI_params);
					if (window.URI_params){
						window.myURI.setData(window.URI_params);
					}
					if (window.myURI){
						//console.log('myURI',window.myURI.toString());
					} else {
						if (window.URI_noresult){
							
							FstError('Tuto adresu jsme nenašli. Prosím zkontrolujte, zda jste adresu zadali ve správném formátu (ULICE, Č.P. , MĚSTO)');
							
						} else {
						FstError('Musíte zadat adresu, nebo alespoň město');
							
						}
					}
					
					//console.log(window.myURI.toString);
					//var address_chachar = new Hash.Cookie('addressChachar');
					//console.log(address_chachar.load());
					
					if (!window.current_site){
						//console.log(document.location.hostname);
						//console.log('redirect',window.myURI.toString());
						
						if(window.myURI){
							window.location = window.myURI.toString();
						}
						//alert('presmerovani na '+ window.myURI.toString());
					}
					else {	
						if ($('provoz_select_list'))
						$('provoz_select_list').addClass('none');
						//console.log(window.myURI.toString());
						link_params = window.myURI.toString().split('?');
						//console.log('current',url_current);
						history.pushState({urlPath:'?'+link_params[1]},"",'?'+link_params[1]);
						var myCookie = Cookie.write('provoz_id', window.current_system_id, {duration: 5});
						
                                                window.location.href = window.location.href; 
                                                //this.hide_intro_adresa();
								
					}
					$$('.clear_address').each(function(item){
						item.value = '';
					});
	},
	
	rozcestnik: function(){
		if ($('rozcestnik')){
			roz_height = $('rozcestnik').getSize().y;
			$('rozcestnik').setStyles({
				'overflow-y':'auto',
				'height':$('body').getSize().y ,
			});
			if (!$('rozcestnik').hasClass('none')){
				$('body').setStyles({
					'overflow':'hidden',
					'height':roz_height - 50,
				});
			}
			
			$('rozcestnik').getElements('.roz_item').addEvent('click',function(e){
				e.stop();
				if (e.target.get('href') == '#'){
					if ($('provoz_info_id_'+e.target.get('data-id'))){
						$('provoz_info_id_'+e.target.get('data-id')).fade('show');
					}
					id = e.target.get('data-id');
					/*
					if ($('provoz'+e.target.get('data-id'))){
						$('provoz'+e.target.get('data-id')).fireEvent('click',$('provoz'+e.target.get('data-id')));
					}
					*/
					$$('.provoz_info').each(function(item){
						item.fade('hide');
					});
					
					if ($('provoz_info_id_'+id)){
						$('provoz_info_id_'+id).fade('show');
					}
					localStorage.setItem('rozItem',e.target.get('data-id'));
					$('provoz_info_id_'+e.target.get('data-id')).fade('show');
		
					this.hide_rozcestnik();
				} else {
					window.location = e.target.get('href');
				}
			}.bind(this));
			
			
			if (localStorage.getItem('rozItem')){
					id = localStorage.getItem('rozItem');
					//console.log('id',id);
					$$('.provoz_info').each(function(item){
						item.fade('hide');
					});
					if ($('provoz_info_id_'+id)){
						$('provoz_info_id_'+id).fade('show');
					}
			}
		
			
		
			
			//console.log(roz_height);
			
		} 
		if ($('rozcestnik_link'))
		$('rozcestnik_link').addEvent('click',function(e){
			e.stop();
			Cookie.dispose('rozcesnik_hide_'+e.target.get('data-id'));
			window.location = e.target.get('data-link');
				
		}.bind(this));
	},
	hide_rozcestnik: function(){
		if ($('rozcestnik')){
			var myCookie = Cookie.write('rozcesnik_hide_'+$('rozcestnik').get('data-id'), true, {duration: 1});
			$('rozcestnik').addClass('none');
			$('body').removeProperty('style');
		}
	},
	
	scrollMobile: function(){
		if ($('scrollMobile')){
		(function(){
			$('scrollMobile').toggleClass('move');
		
		}).periodical(500);
		
		$('pridavky_modal').addEvent('scroll',function(){
			$('scrollMobile').addClass('none');
		});
		}
		if ($('oteviraci_doba')){
		$('oteviraci_doba').addEvent('click',function(e){
			e.stop();
			$('oteviraci_doba_win').removeClass('none');
		});
		$('close_doba').addEvent('click',function(e){
			e.stop();
			$('oteviraci_doba_win').addClass('none');
		});
		
		if ($('provoz_doba_list'))
		$('provoz_doba_list').getElements('strong').addEvent('click',function(e){
			e.stop();
			$('provoz_doba_list').getElements('strong').removeClass('active');
			e.target.addClass('active');
			
			$('provoz_doba_element').getElements('.doba_el').addClass('none');
			$('provoz_doba_element').getElement('.doba_el'+e.target.get('data-id')).removeClass('none');
			
		});
		
		}
		
		
		prevpos = 0;
		if (window.getSize().x < 500){	
			
			
			if ($('navigation')){
			
			if ($('mobile_provoz_show')){
				$('mobile_provoz_show').addEvent('click',function(e){
					$('mobile_provoz').toggleClass('close_el');
					$('body').toggleClass('overflow');
					if ($('mobile_provoz_show').getElement('.fa').hasClass('fa-angle-down')){
						$('mobile_provoz_show').getElement('.fa').removeClass('fa-angle-down');
						$('mobile_provoz_show').getElement('.fa').addClass('fa-angle-up');
						$('mobile_provoz_content').scrollTop = 0;
					} else {
						$('mobile_provoz_show').getElement('.fa').removeClass('fa-angle-up');
						$('mobile_provoz_show').getElement('.fa').addClass('fa-angle-down');
						$('mobile_provoz_content').scrollTop = -10;
						
					}
				});
				
			}
			
			window.addEvent('scroll',function(e){
				curpos = getScrollOffsets().y;
				if (prevpos < curpos && curpos > 120){
					$('navigation').addClass('none');
					if ($('provoz_select_list_mobile')){
						$('provoz_select_list_mobile').setStyle('top',0);
						$('provoz_select_list_mobile').getElement('li.active').setStyle('top',0);
					}
					if ($('mobile_provoz')){
						$('mobile_provoz').addClass('top');
					}
					if ($('subcats'))
						$('subcats').setStyle('top',30);
					
				} else {
					if ($('mobile_provoz')){
						$('mobile_provoz').removeClass('top');
					}
					
					$('navigation').removeClass('none');
					if ($('provoz_select_list_mobile')){
					$('provoz_select_list_mobile').setStyle('top',80);
					$('provoz_select_list_mobile').getElement('li.active').setStyle('top',80);
					}
					//if ($('subcats'))
						//$('subcats').setStyle('top',104);
					//console.log('up');
				}
				
				prevpos = curpos;
			});
			}
		}
	},
	
	show_tips: function(){
		
		$$('.tips').each(function(tip){
			var tip_list = window.tip_list = new Hash.Cookie('tip_list', {duration: 360000});
			
			tip.addEvent('click',function(e){
					
				id = e.target.get('id');
				tip_list.set(id,true);
				//console.log(id);
				//console.log(tip_list.get(id));
				e.target.destroy();
			
			});
			/*
			(function(){	
			
			if ($('item1') && !tip_list.get('tip_velka')){
				$('tip_velka').removeClass('none');
				$('tip_velka').setStyles({
					'left':$('item1').getElement('.varianta_list_item').getCoordinates().right - $('item1').getElement('.varianta_list_select').getSize().x,
					'top':$('item1').getElement('.varianta_list_item').getCoordinates().bottom + 15,
				});
			}
			}).delay(1000);
			*/
			(function(){	
			if ($('show_search') && !tip_list.get('tip_filtr')){
				$('tip_filtr').removeClass('none');
				$('tip_filtr').setStyles({
					'left':$('show_search').getCoordinates().right - 150,
					'top':$('show_search').getCoordinates().top - $('show_search').getCoordinates().height,
				});
			}
			}).delay(2000);
			
			(function(){	
			if ($('layout_kosik_element') && !tip_list.get('tip_basket')){
				$('tip_basket').removeClass('none');
				$('tip_basket').setStyles({
					'right':50 ,
					'bottom':70, 
					'left':'auto',
					'top':'auto',
					'z-index':10000, 
				});
			}
			
			}).delay(3000);
			if ($('provoz_list') && !tip_list.get('tip_provoz')){ 
				$('tip_provoz').removeClass('none');
				$('tip_provoz').setStyles({
					'left':$('provoz_list').getCoordinates().right - 80,
					'top':$('provoz_list').getCoordinates().top - 80,
				});
			}
		});
	},
	
	tips: function(){
		if (window.getSize().x > 400){
		window.addEvent('load',function(){
		(function(){	
		this.show_tips();
		$$('.tips').each(function(tip){
			if (!tip.hasClass('none')){
			tip.getElement('.bull').addClass('move');
			(function(){
				tip.getElement('.bull').removeClass('move');
			}).delay(500);
			(function(){
				tip.getElement('.bull').toggleClass('active');
			}).periodical(600);
			
			}
		});
		}).delay(1000,this);
		}.bind(this));
		}
	},
	
	page_preload: function(){
		if ($('body_style_preload')){
			$('body_style_preload').destroy();
		}
		
		

		window.addEvent('domready',function(){
			/*
			if ($('load_css')){
				$('load_css').set('media','all');
			}
			*/
			/*
			if ($('debug_js')){
			
			} else {
			*/
			(function(){
			if ($('page_preloader'))	
			$('page_preloader').removeClass('load');
			(function(){
				
					if ($('page_preloader'))
					$('page_preloader').destroy();
				
			}).delay(1000);
			
			$$('.page_preload').removeClass('page_preload');
			}).delay(300);
			
			//}
		});
		//}
	},
	
	txt_item: function(){
		if ($('vop')){
			$('vop').getElements('.txt_item').each(function(item){
				if (item.getElement('p')){
				delka = item.getElement('p').get('text').length;
				if (delka > 250){
					item.getElement('p').addClass('over');
					more = new Element('span',{'class':'more','html':'... <span>pokračování</span>'}).inject(item.getElement('p'));
					more.addEvent('click',function(e){
						e.stop();
						item.getElement('.more').addClass('none');
						item.getElement('p').removeClass('over');
						item.addClass('open');
						close = new Element('div',{'class':'close_txt'}).inject(item);
						close.addEvent('click',function(){
							close.destroy();
							item.getElement('.more').removeClass('none');
							item.getElement('p').addClass('over');
							item.removeClass('open');
						});
					});
				}
				}
			});
		}
	},
	
	vtip_list: function(){
		if ($('vtip_list')){
			$('vtip_list_over').addClass('none');
			if ($('show_vtip')){
			$('show_vtip').addEvent('click',function(e){
				e.stop();
				$('contents').getElement('.basket_done').addClass('none');
				$('vtip_list_over').removeClass('none');
			});
			} 
			
			list = $('vtip_list').getElements('.vtip');
			count = list.length;
			list.fade('hide');
			step = 0;
			list[step].fade('show');
			$('next_vtip').addEvent('click',function(e){
				e.stop();
				step++;
				if (step > count -1) step = 0;
				list.fade(0);
				list[step].fade(1);
				
			});
		}
	},
	
	show_spokojenost: function(){
		if ($('spokojenostShow')){
			$('spokojenost_form').fade('hide');
			$('spokojenost_form').removeClass('none');
			$('spokojenostShow').addEvent('click',function(e){
				e.stop();
				e.target.fade(0);
				$('spokojenost_form').fade(1);
			});
		}
	},
	
	// init elements events
	init_elements_events: function(){
		// detect screen size
		this.detect_screen_size();
		// form save
		this.form_save_button();
		// contact form
		this.contact_form();
		// google maps
		this.google_maps();
		// zoom foto
		this.zoom_foto();
		
		// flip cards
		//flipcards();
		
		// hide dorucovaci adresa
		this.hide_dorucovaci();
		
		// load google api
		this.load_google_api();
		
		// open nav bar
		this.open_navbar();
		
		// init autocomplete
		this.init_autocomplete();
	
		// show login element
		this.show_login_element();
		
		
		// show login element
		this.reg_progress();
		
		// placeholder
		this.placeholder();
		
		// provoz list
		this.provoz_list();
		
	
		// init history
		var opt = {
			complete_fce: function(){
				window.FstShop.event_add_basket();
				lazzy_load();
			}
		}
		window.fsthistory = new FstHistory(opt);
		
		
	},
	
	// provozovny list side
	provoz_list: function(){
		
			provoz_info_list = $$('.provoz_info');
			//console.log('a',provoz_info_list.length);
			if (provoz_info_list.length > 1){
				provoz_info_list.fade('hide');
			}
		
		if ($('provoz_list_info_search') && $('provoz_list_info_search').hasClass('none')){
			provoz_info_list.fade('show');
		}
		
		$$('.provoz_save').addEvent('click',function(e){
			if (!e.target) e.target = e;
			//console.log(e.target.get('data-id'));
			var myCookie = Cookie.write('provoz_id', e.target.get('data-id').toInt());
			
		});
		
		// mobile provoz
		if ($('provoz_select_list_mobile')){
			$('provoz_select_list_mobile').addEvent('click',function(e){
				$('provoz_select_list_mobile').toggleClass('close_mobile');
			});
			
			
			$('provoz_select_list_mobile').getElements('.tel').addEvent('click',function(e){
				e.stop();
				window.location.href = e.target.get('href');
			});
			
			$('provoz_select_list_mobile').getElements('.mobile_provoz_name').addEvent('click',function(e){
				e.stop();
				$('provoz_select_list_mobile').getElements('.mobile_provoz_name').removeClass('active');
				e.target.addClass('active');
				$('provoz_select_list_mobile').toggleClass('close_mobile');
			});
		}
		
		if (Object.getLength($$('.provoz_select')) == 0){
			if ($('provoz_list_info'))
			window.selectProvozId = $('provoz_list_info').getElement('.provoz_info').get('data-id').toInt();
			//console.log(window.selectProvozId);
		}
		
		$$('.provoz_select').addEvent('click',function(e){
			if (!e.target){
				e.target = e;
			}
			$$('.provoz_select').removeClass('active');
			e.target.addClass('active');
			
			
			if ($('h1_change')){
				$('h1_change').set('text',e.target.get('text'));
			}
			if (provoz_info_list.length > 1){
				provoz_info_list.fade(0);
			}
			
			provoz_id = e.target.get('data-id');
			//console.log(provoz_id);
			//console.log(window.records_ids);
			if (window.halusky_ids.contains(provoz_id.toInt())){ 
				if ($('menu_id_6')){
				$('menu_id_6').getElement('a').set('text','Špecle / Halušky');
				//$$('.shop_category_id_13').removeClass('none');
				}
			} 
			
			window.selectProvozId = provoz_id.toInt();
			
			if (window.records_ids.contains(provoz_id.toInt())){ 
				if ($('menu_id_4')){	
				$('menu_id_4').getElement('a').set('text','Burgery / Pizza Records');
				//console.log($('menu_id_4').getElement('a'));
				$$('.shop_category_id_13').removeClass('none');
				}
			} else {
				//console.log('aa');
				if ($('menu_id_4')){
				$('menu_id_4').getElement('a').set('text','Burgery');
				$$('.shop_category_id_13').addClass('none');
				}
			}
			
			$('provoz_list_info').getElement('.provoz_info_id' + provoz_id).removeClass('none').fade(1);
			if ($('provoz_list_info_search'))
			$('provoz_list_info_search').fade(1);
		});
		if ($('provoz_select_list') && !$('provoz_select_list').getElement('.active')){
			$('provoz_select_list').getElement('.provoz_select').addClass('active');
		}
		if ($('provoz_select_list')){
                    $('provoz_select_list').getElement('.active').fireEvent('click',$('provoz_select_list').getElement('.active'));
                }
		// oteviraci doba
		$$('.open_more_date').addEvent('click',function(e){
			e.stop();
			
			if (e.target.getElement('span').hasClass('fa-angle-down')){
				e.target.getElement('span').removeClass('fa-angle-down');
				e.target.getElement('span').addClass('fa-angle-up');
			} else {
				e.target.getElement('span').addClass('fa-angle-down');
				e.target.getElement('span').removeClass('fa-angle-up');

			}
			e.target.getNext('.more_date').toggleClass('none');
		});
	},
	
	// placeholder show
	placeholder: function(){
		$$('.placeholder').each(function(item){
			place = new Element('span',{'class':'placeholder_show none','data-title':item.get('placeholder')}).inject(item,'before');
			item.addEvents({
				'focus': function(e){
					e.target.getPrevious('.placeholder_show').removeClass('none');
				},
				'blur': function(e){
					e.target.getPrevious('.placeholder_show').addClass('none');
				},
			});
		});
	},
	
	
	valid_registrace: function(el,complete){
		var form = el.getParent('form');
		var button = el;
		if (!form.getElement('.spam')){
				
			new Element('input',{'type':'hidden','name':'spam','value':123,'class':'spam'}).inject(form);
			step_input = new Element('input',{'type':'hidden','class':'step','name':'step','value':button.get('data-id')}).inject(form);
		} else {
			step_input.value = button.get('data-id');
		}
		
		button_preloader(button);
		this.req_form = new Request.JSON({
			url:form.get('action'),
			onError: this.req_error = (function(data){
				button_preloader(button);
			}).bind(this),

			onComplete :(function(json){
					
				button_preloader(button);
				if (json.r == true){
							if (json.close){
								console.log('close win');
								(function(){
								window.open('','_self').close();
								}).delay(2000);
							}
							
					complete();
							
				} else {
					//console.log('aa');
					FstError(json.m);
					if (json.invalid){
						$$('.invalid').removeClass('invalid');
						
						json.invalid.each(function(item){
							//console.log(item);
							item = item.replace('_','-');
							//console.log(item);
							if($(item)){
								$(item).addClass('invalid');	
							}
						});
					}
				}
						
			}).bind(this)
		});
		this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
		this.req_form.post(form);
	},
	
	// progress registrace
	reg_progress: function(){
		
		
		if ($('reg_progress')){
			var reg_fields = $$('.reg_field');
			reg_fields.each(function(item,k){
				if (k > 0){
					item.addClass('hide_opacity');
					$('RegButton').addClass('none');
				} else {
				}
			});
			step = 20;
			$$('.next_field').addEvent('click',function(e){
				e.stop();
				
				function complete(){
						
					new Fx.Scroll(window).toElement($('white_page'));
					reg_fields.each(function(item){
						item.addClass('hide_opacity');
					});
					reg_fields[e.target.get('data-id')].removeClass('hide_opacity');
					$('h1_adopt').set('text',reg_fields[e.target.get('data-id')].getElement('.adopt_h1').get('text'));
					$$('.address_title').addClass('none');
					
					width = $('bar').get('data-width').toInt() + step;
					$('bar').set('data-width',width);
					
					new Fx.Tween('bar', {
						unit: '%'
					}).start('width', width);
				}
				
				
				if (e.target.hasClass('only_next')){
					complete();
				} else {
					this.valid_registrace(e.target,complete);
					
				}
				
				
			}.bind(this));
			
			$$('.prev_field').addEvent('click',function(e){
				e.stop();
				new Fx.Scroll(window).toElement($('white_page'));
				
				$('h1_adopt').set('text',reg_fields[e.target.get('data-id')].getElement('.adopt_h1').get('text'));
				reg_fields.each(function(item){
					item.addClass('hide_opacity');
				});
				
				reg_fields[e.target.get('data-id')].removeClass('hide_opacity');
				
				width = $('bar').get('data-width').toInt() - step;
				$('bar').set('data-width',width);
				new Fx.Tween('bar', {
					unit: '%'
				}).start('width', width);
			});
			
			// avatar list
			if ($('avatar_list')){
				var avatar_list = $('avatar_list').getElements('.avatar');
				avatar_list.addEvent('click',function(e){
					if (!e.target) e.target = e;
					if (e.stop){
						e.stop();
					}
					avatar_list.removeClass('active');
					e.target.addClass('active');
					$('avatar_list').getElement('.avatar_id').value = e.target.get('data-id');
				});
				if ($('avatar_list').getElement('.avatar_id').value == ''){
					avatar_list[0].fireEvent('click',avatar_list[0]);
				} else {
					$('avatar_list').getElement('.avatar'+$('avatar_list').getElement('.avatar_id').value).fireEvent('click',$('avatar_list').getElement('.avatar'+$('avatar_list').getElement('.avatar_id').value));
					
				}
			}
			
		}
	},
	
	// init lazzy load bez scroll
	init_lazy_load: function(){
		if (typeof lazzy_load != 'undefined'){
			if(Object.getLength($$('.lazy_force'))>0){
				lazzy_load($$('.lazy_force'));
			}
		}
	},
	
	// init form helper
	init_form_helper: function(){
		form_helper();
		
	},
	
	// detect screen size
	detect_screen_size: function(){
		window.screen = window.getSize();
	},
	
	// load autocomplete to element
	init_autocomplete: function(){
		if ($('body').getElement('.autocomplete')){
			var script_autocomplete = Asset.javascript('/js/FstAutocomplete/FstAutocomplete.js', {
				id: 'script_autocomplete',
				onLoad: function(){
					if (window.debug) console.log('loaded script_autocomplete');
					$$('.autocomplete').each(function(el){
						options = {
							'element': el,
							'search_request': el.getParent('form').action,
						}
						fstAutocomplete = new FstAutocomplete(options);
						//console.log(el);
					});
				}
			});
		}
		
	},
	
	// hide dorucovaci
	hide_dorucovaci: function(){
		if ($('check-dorucovaci')){
			$('check-dorucovaci').addEvent('click',function(e){
				if (!e.target){
					e.target = e;
				}
				if (e.target.checked){
					$('address_type2').removeClass('none');
					if ($('address_type2').getElement('legend'))
					$('address_type2').getElement('legend').removeClass('none');
					if (!$('address_type2').get('data-height')){
						$('address_type2').set('data-height',$('address_type2').getCoordinates().height + $('address_type2').getStyle('margin-top').toInt() + $('address_type2').getStyle('margin-bottom').toInt());
					}
					$('address_type2').setStyles({
						'height':0,
						'overflow':'hidden',
					});
					$('address_type2').tween('height',$('address_type2').get('data-height'));
					if ($('address_type2').getElement('input').value == $('address_type1').getElement('input').value){
						$('address_type2').getElements('.clear_address').each(function(item){
							item.value = '';
						});
					}
					if ($('user-addresses-1-street'))
					$('user-addresses-1-street').focus();
				} else {
					if (!$('address_type2').get('data-height')){
						$('address_type2').set('data-height',$('address_type2').getCoordinates().height + $('address_type2').getStyle('margin-top').toInt() + $('address_type2').getStyle('margin-bottom').toInt());
					}
					if ($('address_type2').getElement('legend'))
					$('address_type2').getElement('legend').addClass('none');
					$('address_type2').tween('height',0);
					$('address_type2').addClass('none');
				}
			});
			$('check-dorucovaci').fireEvent('click',$('check-dorucovaci'));
		}
	},
	
	
	// open nav bar 
	open_navbar: function(){
		$('body').getElements('nav').each(function(nav){
			if (nav.getElement('.fa')){
				nav.getElements('.fa').addEvent('click',function(e){
					
					if (e.target.hasClass('fa-caret-down')){
						e.target.removeClass('fa-caret-down');
						e.target.addClass('fa-caret-up');
					} else {
						e.target.addClass('fa-caret-down');
						e.target.removeClass('fa-caret-up');
					}
				});
			}
			if (nav.getElement('.active')){
				function show_menu(ul){
					ul.addClass('in');
					if (ul.getPrevious('.fa')){
						ul.getPrevious('.fa').removeClass('fa-caret-down');
						ul.getPrevious('.fa').addClass('fa-caret-up');
					}
				}
				show_menu(nav.getElement('.active').getParent('ul'));
				
				if (nav.getElement('.active').getParent('ul').getParent('ul')){
					show_menu(nav.getElement('.active').getParent('ul').getParent('ul'));
				}
			}
			
			//console.log(nav.getElement('.active'));
		});
		
		// login element show
		/*
		if ($('mobile_login')){
			$('mobile_login').addEvent('click',function(e){
				$('login_element').toggleClass('show');
			});
		}
		*/
	},
	
	// load zoom foto 
	zoom_foto: function(){
		$$('.lazy').each(function(img){
				
			img.removeProperty('src');
			if (img.get('data-nosize') == null){
			img.setStyles({
				'width':img.get('data-width')+'px',
				'height':img.get('data-height')+'px'
			});
			}
			img.addClass('preloader');
		});
		
		if ($('body').getElement('.zoom_foto')){
			
			var script_slim_box = Asset.javascript('http://scripts.fastesthost.cz/js/slimbox/slimbox.js', {
				id: 'script_slim_box',
				onLoad: function(){
					if (this.debug) console.log('loaded script_slim_box');
				}
			});
		}
		
	},
	
	// load google maps 
	google_maps: function(){
		if ($('body').getElement('.load_google_map')){
			var script_googleMap = Asset.javascript('/js/GoogleMaps/google_maps.js', {
				id: 'script_googleMap',
				onLoad: function(){
				}
			});
		}
		
	},
	
	// load contact form
	contact_form: function(){
		$$('.load_contact_form').each(function(el_inject){
			el_inject.addClass('preloader');
			this.req_form = new Request.HTML({
				url:'/loadContactForm/'+el_inject.get('data-type'),
				update: el_inject,
				onError: this.req_error = (function(data){
				
				}).bind(this),

				onComplete :(function(html){
					el_inject.removeClass('preloader');
					this.form_save_button();	
				}).bind(this)
			});
			this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			this.req_form.send();
		}.bind(this));
	},
	
	// SaveForm button click
	form_save_button: function(){
			$$('.reset_password').addEvent('click',function(e){
				$$('.reset_password').each(function(item){
					item.value = '';
				});
				$$('.reset_password2').each(function(item){
					item.value = '';
				});
			});
			
			$$('.SaveForm').each(function(item){
				if (item.getParent('form')){
					item.getParent('form').getElements('.float, .integer').inputLimit();
					if (window.screen.x < 800){
						item.getParent('form').getElements('.integer').each(function(item){
							item.set('type','number');
						});
					}
					
				}
			});
			
			$$('.SaveForm').removeEvents('click');
			$$('.SaveForm').addEvent('click',function(e){
				e.stop();
				button = e.target;
				button_preloader(button);
				
				var form = e.target.getParent('form');
				if (form.getElement('.step')){
					form.getElement('.step').destroy();
				}
				if (!form.getElement('.spam')){
					new Element('input',{'type':'hidden','value':123,'name':'spam','class':'spam'}).inject(form);
				}
				this.req_form = new Request.JSON({
					url:form.get('action'),
					onError: this.req_error = (function(data){
						button_preloader(button);
					}).bind(this),

					onComplete :(function(json){
						
						button_preloader(button);
						
							//console.log(json);
						if (json.r == true){
							FstAlert(json.m);
							// pokud je json.clear vycisti formular
							if (json.clear){
								form.getElements('.form-control').each(function(item){
									item.value = '';
								});
							}
							if (json.user_id){
								ga('set', 'userId', json.user_id); // Nastavit ID uživatele pomocí hodnoty user_id použité k přihlášení.
							}
							if (json.close){
								console.log('close win');
								(function(){
								window.open('','_self').close();
								}).delay(2000);
							}
							// pokud je json.redirect presmeruj
							if (json.redirect){
								(function(){
									if (json.redirect == 'self'){
										window.location = window.location;
									} else {
										window.location = json.redirect;
									}
								}).delay(1000);
							}
							
							// pokud je gopay vloz url do formulare
							if (json.gopay && $('gopay-payment-button')){
								window.FstShop.show_gopay(json.gopay);
							}
							
							
						} else {
							FstError(json.m);
							if (json.invalid){
								$$('.invalid').removeClass('invalid');
								json.invalid.each(function(item){
									if (item == 'shop_payment_id'){
										var myFx = new Fx.Scroll(window).toElement('scroll-platba', 'y');
									} else {
										if ($('scroll-osobni')){
											
											var myFx = new Fx.Scroll(window).toElement('scroll-osobni', 'y');
											
										}
									}
									
									item = item.replace('_','-');
									item = item.replace('_','-');
									//console.log(item);
									if($(item)){
										$(item).addClass('invalid');	
									}
								});
							}
						}
						
					}).bind(this)
				});
				this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
				this.req_form.post(form);
			});
	},
	
	// show / hide login element
	show_login_element: function(){
		
		if ($('login_element') && $('login_element').getElement('.show_login') && $('show_login_el')){
			var height = $('show_login_el').getDimensions().height;
			$('show_login_el').removeClass('load');
			$('show_login_el').set('data-height',height);
			$('show_login_el').addClass('close');
			$('show_login_el').addClass('overflow');
			$('show_login_el').setStyle('height',0);
			$('show_login_el').getElement('.login_roh').addClass('none');
			$('login_over').addClass('none');
			$('login_over').fade('hide');
					
			
			// click show
			$('login_element').getElements('.show_login').addEvent('click',function(e){
				if (e){
					e.stop();
				}
				console.log('show');
				if ($('show_login_el').hasClass('close')){
					$('login_over').removeClass('none');
					$('login_over').fade(1);
			
					$('show_login_el').getElement('.login_roh').removeClass('none');
					$('show_login_el').removeClass('close');
					$('show_login_el').tween('height',$('show_login_el').get('data-height'));
					$('username').focus();
				} else {
					$('login_over').fade(0);
					$('show_login_el').getElement('.login_roh').addClass('none');
					$('show_login_el').addClass('close');
					$('show_login_el').tween('height',0);

				}
			});
			
		}
	},
	
	// init delay scripts
	init_delay_scripts: function(){
		var txt = 'text/delayscript';
		
		document.getElements("script").each(function(script){
			if (script.get('type').toLowerCase() == txt){
				new Element('script',{'type':'text/javascript','text':script.get('html')}).inject($('addon'));
				script.destroy();
			}
		});
	},
	
	// init adopt element in body by class .el_adopt with data-el
	init_adopt_element:function(){
		// element adopt with class .el_adopt and data-el
		if($('body').getElements('.el_adopt')) {
			$('body').getElements('.el_adopt').each(function (item) {
				if ($(item.get('data-el')))
				$(item.get('data-el')).adopt(item);
			});
		}
		
		// hide template elements TEMPO
		$$('.hide_template').each(function (item) {
			item.setStyle('display','none');
			item.removeClass('hide_template');
		});
		
		// hide specific element
		$$('.hide_element').addEvent('change',function(e){
			if ($(e.target.get('data-hide'))){
				if (e.target.value == ''){
					$(e.target.get('data-hide')).addClass('none');	
				} else {
					
					$(e.target.get('data-hide')).removeClass('none');
				}
			}
		});
		
		// show_element if display none
		$$('.show_element').addEvent('click',function(e){
			e.stop();
			if ($(e.target.get('data-el'))){
				$(e.target.get('data-el')).toggleClass('none');
				e.target.addClass('none');
			}
		});
		
		// redirect after click
		
		$$('.redirect').addEvent('click',function(e){
			e.stop();
			window.location = e.target.get('data-url');
		});
		
	},
	
	init_mooswipe: function(fce){
		//if (this.detect_mobile()){
			var script_swipe = Asset.javascript('/js/MooSwipe/MooSwipe.js', {
				id: 'script_jquery',
				onLoad: function(){
					//console.log('load MooSwipe');
					fce();
				}
			});
		//}
		
	},
	
	
	// init jquery 
	init_jquery: function(){
		//var script_jquery = Asset.javascript('https://scripts.fastesthost.cz/js/jquery/jquery.js', {
		var script_jquery = Asset.javascript('/js/fastesthost/jquery.js', {
			id: 'script_jquery',
			onLoad: function(){
				if (window.debug) console.log('jquery is loaded!');
				//var script_bootstrap = Asset.javascript('https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js', {
				var script_bootstrap = Asset.javascript('/js/fastesthost/bootstrap.js', {
					id: 'script_bootstrap',
					properties: {
						integrity: 'Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa',
						crossorigin: 'anonymous',
					},
					onLoad: function(){
						if (window.debug) console.log('script_bootstrap is loaded!');
						
						var script_jquery = Asset.javascript('/js/jquery_script.js', {
							id: 'script_jquery',
							onLoad: function(){
								
								if (this.debug) console.log('load script_jquery');
								
								// init form helper
								this.init_form_helper();
								
								// init lazy load to element
								this.init_lazy_load();
							}.bind(this)
						});
				
					}.bind(this)
				});
				
				
			}.bind(this)
		});
	},
	
	// detect is mobile
	detect_mobile: function(){
		var platform = [
			'android',
			'ios',
			'webos',
		];
		if (platform.contains(Browser.platform)){
			return true;
		} else {
			return false;
		}
	},
	
	hide_records: function(provoz_id){
		//console.log(window.selectProvozId);
		if (window.selectProvozId && !provoz_id){
			provoz_id = window.selectProvozId;
		}
		if (!provoz_id) provoz_id = 0;	
			if (window.halusky_ids.contains(provoz_id.toInt())){ 
				if ($('menu_id_6')){
				$('menu_id_6').getElement('a').set('text','Špecle / Halušky');
				//$$('.shop_category_id_13').removeClass('none');
				}
			} 
			if (window.records_ids.contains(provoz_id.toInt())){ 
				if ($('menu_id_4')){
				$('menu_id_4').getElement('a').set('text','Burgery / Pizza Records');
				$$('.shop_category_id_13').removeClass('none');
				}
			} else {
				
				if ($('menu_id_4')){
				$('menu_id_4').getElement('a').set('text','Burgery');
				$$('.shop_category_id_13').addClass('none');
				}
			}
	},
	
	switch_provoz_tel: function(){
		if ($('tel_provoz_id')){
				if (!$('search-address-tel').get('data-tmp_lat') && $('user-user-addresses-0-lat')){
					$('search-address-tel').set('data-tmp_lat',$('user-user-addresses-0-lat').value);
					$('search-address-tel').set('data-tmp_lng',$('user-user-addresses-0-lng').value);
				}
				//console.log($('search-address-tel').get('data-tmp_lat'));
				latLng = new google.maps.LatLng($('search-address-tel').get('data-tmp_lat'), $('search-address-tel').get('data-tmp_lng'));
				//console.log($('search-address-tel').get('data-tmp_lat'));
				result = false;
				//console.log(polygons);
				Object.each(polygons,function(system_data,system_id){
						system_data.each(function(poly){
						
						if (google.maps.geometry.poly.containsLocation(latLng, poly)) {
							result = true;
							result_system_id = system_id
							console.log('systemId',system_id);
							
						} else {
							//console.log('neni');
						}
					});
				});
				if (result == true){
				//console.log(result_system_id);
					if ($$('.provoz_info').length > 1){
						$$('.provoz_info').fade(0);
					}
					if ($('provoz_info_id_'+result_system_id)){
						$('provoz_info_id_'+result_system_id).fade(1);
						this.hide_records(result_system_id);
					} else {
						FstError('Tuto adresu jsme nenašli. Prosím zkontrolujte, zda jste adresu zadali ve správném formátu (ULICE, Č.P. , MĚSTO)');
					}
					if ($('search-address-tel')){
						localStorage.setItem('search_address_system_id',result_system_id);
						localStorage.setItem('search_address',$('search-address-tel').value);
						
						if (!$('search-address-tel').get('data-tmp_cp')){
							input = $('search-address-tel2');
						} else {
							input = $('search-address-tel');
						}
						
						localStorage.setItem('search_address_json',JSON.encode({
							'cp':input.get('data-tmp_cp'),
							'ulice':input.get('data-tmp_ulice'),
							'mesto':input.get('data-tmp_mesto'),
							'lat':input.get('data-tmp_lat'),
							'lng':input.get('data-tmp_lng'), 
							'provoz_id':result_system_id,
							
						})); 
								//console.log($('search-address-tel').value);
							
							
					}
							
				} else {
					if ($$('.provoz_info').length > 1){
						$$('.provoz_info').fade(0);
					}
					FstError('Tuto adresu jsme nenašli. Prosím zkontrolujte, zda jste adresu zadali ve správném formátu (ULICE, Č.P. , MĚSTO)');
					
				}
			}
	},
	
	map_area_data: function(){
		if ($('map_coords').get('data-json')){
			json_data = $('map_coords').get('data-json');
		} else {
			json_data = $('map_coords').get('text');
				
		}
		if (json_data == ''){
			return false;
		}
		map_data = JSON.decode(json_data);
		var polygons = {}
			Object.each(map_data,function(provoz_item,provoz_id){
				//console.log(provoz_id);
				if (!polygons[provoz_id]){
					polygons[provoz_id] = [];
				}	
				
				Object.each(provoz_item,function(item){
					poly = new google.maps.Polygon({
						path: google.maps.geometry.encoding.decodePath(String(item.coords)), 
					});
					polygons[provoz_id].push(poly);
				});
				
			});
		window.polygons = polygons;	
		//console.log(window.polygons);
	},
	
	// map area
	map_area: function(){
		if ($('map_coords')){
			if ($('map_coords').get('data-json')){
				json_data = $('map_coords').get('data-json');
			} else {
				json_data = $('map_coords').get('text');
				
			}
			if (json_data == ''){
				return false;
			}
			map_data = JSON.decode(json_data);
			//console.log(map_data);
			
			
			lat = 49.8481683;
			lng = 18.28562510000006;
			
			var polygons = {}
			Object.each(map_data,function(provoz_item,provoz_id){
				//console.log(provoz_id);
				if (!polygons[provoz_id]){
					polygons[provoz_id] = [];
				}	
				
				Object.each(provoz_item,function(item){
					poly = new google.maps.Polygon({
						path: google.maps.geometry.encoding.decodePath(String(item.coords)), 
					});
					polygons[provoz_id].push(poly);
				});
				
			});
				
			if ($('intro_adresa')){
				//console.log('a');
				window.polygons_search = polygons;
				//console.log(window.polygons_search);
			}
			//console.log(polygons);
			//console.log($('shop-provoz-id'));
			
			
			
			if ($('shop-provoz-id')){
				window.provoz_list = {}
				$('shop-provoz-id').getElements('option').each(function(item){
					window.provoz_list[item.get('value')] = item.get('text');
				}.bind(this));
			
				// zmena provozovny
				$('shop-provoz-id').addEvent('change',function(e){
					if (!e.target){
						e.target = e;
					}
					if (!e.target.hasClass('form-control')){
						e.target = e.target.getParent('select');
					}
					if (e.target.hasClass('fastest')){
						return false;
					}
					select_provoz_id = e.target.value;
					select_title = e.target.get('data-title');
					
					if ($('user-user-addresses-0-lng').value != ''){
					
					lat = $('user-user-addresses-0-lat').value;
					lng = $('user-user-addresses-0-lng').value;
					
					find_result = false;
					latLng = new google.maps.LatLng(lat, lng);
					//console.log(latLng);
					Object.each(polygons,function(system_data,system_id){
						system_data.each(function(poly){
							if (google.maps.geometry.poly.containsLocation(latLng, poly)) {
								if (select_provoz_id == system_id){
									
									if (window.debug){
										console.log('nalezeno ',system_id);
									}
									find_result = {'r':true,'provoz_id':system_id,'m':window.provoz_list[system_id],};
															
								} else {
									if (window.debug){
										console.log('nalezeno ale jiny provoz ',window.provoz_list[system_id]);
									}
									//console.log('nalezeno ale jiny provoz ',window.provoz_list[system_id]);
									//console.log('a');
									find_result = {'r':false,'m':window.provoz_list[system_id],'provoz_id':system_id};
										
								}
								
							} else {
								console.log('neni');
							}
						});
					});
					//console.log(find_result);
					if (find_result){
						$('shop-provoz-id').value = find_result.provoz_id;
						$('provoz_error').getElement('strong').set('text',find_result.m);
						$('provoz_error').removeClass('none');
						$('provoz_error').fade(1);
						(function(){
							$('provoz_error').fade(0);
						}).delay(1000);
						//console.log(find_result);
					}
					
					}
						
				});
			
			}

			
			//console.log(polygons);
			//console.log(map_data);
		}
	},
	
	showAddressClear: function(){
		$$('.address_clear').each(function(item){
			if (item.getNext('div').getElement('input')){
				if (item.getNext('div').getElement('input').value == ''){
					item.addClass('none');
				} else {
					item.removeClass('none');
				}
			}
		});
	},
	
	
	showProvozGoogle: function(item){
		
				if ($('adr_preloader')) $('adr_preloader').removeClass('none');
				if ($('adr_preloader2')) $('adr_preloader2').removeClass('none');
				req_form = new Request.JSON({
					url:'/search_google_gps/'+item.value,
					onError: this.req_error = (function(data){
					}).bind(this),

					onComplete :(function(json){
						
						if ($('adr_preloader')) $('adr_preloader').addClass('none');
						if ($('adr_preloader2')) $('adr_preloader2').addClass('none');
						if (json.r == true){
							//console.log(json);
							var mesto = json.data.city;
							var ulice = json.data.street;
							var cp = json.data.cp;
							var co = json.data.co;
							var lat = json.data.lat;
							var lng = json.data.lng;
							if ($(item.get('data-mesto'))) $(item.get('data-mesto')).value = mesto;
							if ($(item.get('data-psc'))) $(item.get('data-psc')).value = psc;
							if ($(item.get('data-cp'))) $(item.get('data-cp')).value = cp+co;
							if ($(item.get('data-ulice'))) $(item.get('data-ulice')).value = ulice;
							if ($(item.get('data-lat'))) $(item.get('data-lat')).value = lat;
							if ($(item.get('data-lng'))) $(item.get('data-lng')).value = lng;
							item.set('data-tmp_lat',lat);
							item.set('data-tmp_lng',lng);
							item.set('data-tmp_ulice',ulice);
							item.set('data-tmp_cp',co + ((co && cp)?'/':'') +((cp)?cp:''));
							item.set('data-tmp_mesto',mesto);
							item.value = ulice + ' '+co + ((co && cp)?'/':'') +((cp)?cp:'')+', '+mesto;
							this.map_area_data();
							this.switch_provoz_tel();
							
							if ($('user-user-addresses-0-street')){
								$('user-user-addresses-0-street').value = ulice;
								$('user-user-addresses-0-cp').value = co + ((co && cp)?'/':'') +((cp)?cp:'');
								$('user-user-addresses-0-city').value = mesto;
							}
						} else {
							FstError(json.message);
						}
								
					}).bind(this)
				});
				req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
				req_form.send();
				
			
	},
	
	showAddress: function(preloader,button,json,mapy){
		
		//console.log(button);
								preloader.addClass('none');
								if (!mapy)
								button_preloader(button);
								
								console.log(json);
								//button_preloader($('SearchAdresa'));	
								//button_preloader(button);
								if (json && json.r == true){
									//console.log(json);
									latLng = new google.maps.LatLng(json.data.lat, json.data.lng);
									result = false;
									//console.log(latLng);
									
									Object.each(polygons,function(system_data,system_id){
											system_data.each(function(poly){
											
											if (google.maps.geometry.poly.containsLocation(latLng, poly)) {
												result = true;
												result_system_id = system_id
												//console.log('systemId',system_id);
												
											} else {
												//console.log('neni');
											}
										});
									});
									
									if (result == true){
									var addressInput = json.data.street + ' '+json.data.co + ((json.data.co && json.data.cp)?'/':'') +((json.data.cp)?json.data.cp:'')+', '+json.data.city;
									localStorage.setItem('address_google',JSON.encode(json.data));
									localStorage.setItem('address_google_input',addressInput);
											
									
									//console.log(json);
									//console.log($('redirect_system_ids'));
									if ($('redirect_system_ids'))
									redirect_system_ids = JSON.decode($('redirect_system_ids').get('data-json'));
									//console.log(redirect_system_ids);
									if (!$('jsonProvoz')){
										if (typeof redirect_system_ids != 'undefined' && redirect_system_ids[result_system_id]){
											//console.log(redirect_system_ids[result_system_id]);
											url = 'https://'+redirect_system_ids[result_system_id];
											if ($('intro_adresa')){
												if ($('intro_adresa').hasClass('records')){
													url += '/burgery/';
												}
												if ($('intro_adresa').hasClass('cina')){
													url += '/cina/';
												}
											}
											console.log(url);
											window.location = url;
											return false;
										}
									}
									window.provoz_list = {}
									if ($('jsonProvoz')){
										data = JSON.decode($('jsonProvoz').get('text'));
										//console.log(data);
										data.each(function(item,k){
											window.provoz_list[item.id] = item.name;
										}.bind(this));
									}
									//console.log(window.provoz_list);
								
									localStorage.setItem('address_google_system_id',result_system_id);
									if ($('jsonProvoz') && window.provoz_list[result_system_id]){
										var message = 'Na Vaši adresu doveze pobočka '+window.provoz_list[result_system_id];
										//FstAlert(message);
										if ($('provoz_name')){
											$('provoz_name').removeClass('none');
											$('provoz_name').set('text',message);
											if ($('user-user-addresses-0-street')){
												var street = $('user-user-addresses-0-street');
												var city = $('user-user-addresses-0-city');
												var lat = $('user-user-addresses-0-lat');
												var lng = $('user-user-addresses-0-lng');
												var cp = $('user-user-addresses-0-cp');
											} else {
												var street = $('user-addresses-0-street');
												var city = $('user-addresses-0-city');
												var lat = $('user-addresses-0-lat');
												var lng = $('user-addresses-0-lng');
												var cp = $('user-addresses-0-cp');
											}
											
											street.value = json.data.street;
											cp.value = ((json.data.co && json.data.cp)?json.data.co+'/':'') +((json.data.cp)?json.data.cp:'');
											city.value = json.data.city;
											lat.value = json.data.lat;
											lng.value = json.data.lng;
											if ($('shop-provoz-id')){
											$('shop-provoz-id').value = result_system_id;
											console.log($('shop-provoz-id').value);
											}
										}
										
										if ($('search-address-tel2')){
											$('search-address-tel2').value = addressInput;
										}
										
										if ($('search-address-tel')){
											$('search-address-tel').value = addressInput;
											if ($$('.provoz_info').length > 1){
												$$('.provoz_info').fade(0);
											}
											if ($('provoz_info_id_'+result_system_id)){
												$('provoz_info_id_'+result_system_id).fade(1);
												this.hide_records(result_system_id);
											}
										}
										
									} else {
										$$('.clear_address').each(function(item){
											item.value = '';
										});
										if ($('provoz_name'))
										$('provoz_name').addClass('none');
										FstError('Tuto adresu jsme nenašli. Prosím zkontrolujte, zda jste adresu zadali ve správném formátu (ULICE, Č.P. , MĚSTO)');
										
									}
									
									} else {
										$$('.clear_address').each(function(item){
											item.value = '';
										});
										if ($('provoz_name'))
										$('provoz_name').addClass('none');
										FstError('Tuto adresu jsme nenašli. Prosím zkontrolujte, zda jste adresu zadali ve správném formátu (ULICE, Č.P. , MĚSTO)');
										if ($$('.provoz_info').length > 1){
											$$('.provoz_info').fade(0);
										}
										
									}
									/*
									var item = $('address-value');
									
									if ($(item.get('data-mesto'))) $(item.get('data-mesto')).value = json.data.city;
									if ($(item.get('data-psc'))) $(item.get('data-psc')).value = json.data.zip;
									if ($(item.get('data-cp'))) $(item.get('data-cp')).value = json.data.cp+((json.data.co!='')?'/'+json.data.co:'');
									if ($(item.get('data-ulice'))) $(item.get('data-ulice')).value = json.data.street;
									if ($(item.get('data-lat'))) $(item.get('data-lat')).value = json.data.lat;
									if ($(item.get('data-lng'))) $(item.get('data-lng')).value = json.data.lng;
									/*
									if ($(item.get('data-cp')).value == ''){
										item_value = $('address-value').value.split(' ');
										//console.log(item_value);
										$('address-value').value = item_value[0];
									}
									*/
									
									//this.parse_provoz_redirect(json.data.street,json.data.lat,json.data.lng);
					
								} else {
									//console.log(json.adr);
									function odpoved(geocoder) { /* Odpověď */
										if (!geocoder.getResults()[0].results.length) {
											//alert("Tohle neznáme.");
											this.res = {
												'r':false,
											}
											return;
										}
										
										var vysledky = geocoder.getResults()[0].results;
										//console.log(geocoder.getResults()[0]);
										var data = [];
										while (vysledky.length) { /* Zobrazit všechny výsledky hledání */
											var item = vysledky.shift()
											address = item.label.split(', ');
											adrList = this.adr.street.split(' ');
											cp = adrList[adrList.length-1];
											adrList.splice(adrList.length-1);
											this.res = {
												'r':true,
												'data':{
													'lat':item.coords.y,
													'lng':item.coords.x,
													'street':adrList,
													'cp':this.adr.cp,
													'co':'',
													'city':this.adr.city,
												}
											}
											//console.log(this.res);
											//console.log(address);
											data.push(item.label + " (" + item.coords.toWGS84(2).reverse().join(", ") + ")");
										}
										this.showAddress(preloader,button,this.res,true);		
											
										//alert(data.join("\n"));
									}		
									if (!mapy && Object.getLength(json.adr) > 0){
										this.adr = json.adr;
										new SMap.Geocoder(this.address, odpoved.bind(this));
									} else {
										FstError('Tuto adresu jsme nenašli. Prosím zkontrolujte, zda jste adresu zadali ve správném formátu (ULICE, Č.P. , MĚSTO)');
										
									}
								}
								
	},
	
	// init google api autocomplete	 AKTUALNI
	init_google_autocomplete: function(){
		init_order_map();
		
		
		
		if ($('newAdr')){
			
			//console.log('aaa');
			$$('.searchAddress').addEvent('change',function(e){
				if (!e.target) e.target = e;
				var address = e.target.value;
				var inputAddress = e.target;
				var button = $(e.target.get('data-button'));
				var preloader = $(e.target.get('data-preloader'));
				//console.log(preloader);
				//console.log(button);
				preloader.removeClass('none');
				button_preloader(button);
				this.map_area_data();
				//console.log(encodeURIComponent('Polní 2066/4, Český Těšín'));
				//if ($('debug_js')){
					//console.log('deb');
					address = address.replace('/','|');
				//}
		//console.log('aaa');			
				//if (!this.res){
					//new SMap.Geocoder(address, odpoved.bind(this));
					//	this.showAddress(preloader,button,this.res);		
					
				//} else {
				this.address = address;
				this.gps_form = new Request.JSON({
							url:'/search_google_gps/'+address,
							onError: this.req_error = (function(data){
								button_preloader(button);
								preloader.addClass('none');
							}).bind(this),

							onComplete :(function(json){
								this.showAddress(preloader,button,json);		
							}).bind(this)
						});
						this.gps_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
						this.gps_form.post();
				//} 
				
			}.bind(this));
			
			
			
			if ($('SearchAdresa')){
				$('SearchAdresa').addEvent('click',function(){
					$('address-value').fireEvent('change',$('address-value'));
				});
			}
			if ($('ShowProvozBtn2')){
				$('ShowProvozBtn2').addEvent('click',function(){
					$('search-address-tel2').fireEvent('change',$('search-address-tel2'));
				});
			}
			if ($('search-address-tel2')){
				(function(){
				$('search-address-tel2').fireEvent('change',$('search-address-tel2'));
				}).delay(2000);
			}
			
			if (localStorage.getItem('address_google_input')){
				inputValue = localStorage.getItem('address_google_input');
				$$('.searchAddress').each(function(item,k){
					
					item.value = inputValue;
					if (k == 0){
						if (localStorage.getItem('address_google_system_id')){
							result_system_id = localStorage.getItem('address_google_system_id');
							console.log(result_system_id);
							if ($$('.provoz_info').length > 1){
								$$('.provoz_info').fade(0);
							}
							if ($('provoz_info_id_'+result_system_id)){
								$('provoz_info_id_'+result_system_id).fade(1);
								this.hide_records(result_system_id);
							}
						} else {

						(function(){
							if (!item.hasClass('intro')){
								
								item.fireEvent('change',item);
							}
						
						}).delay(2000);
						
						}
					}
				}.bind(this));
			}
			
		} else {
		if ($('ShowProvozBtn')){
			$('ShowProvozBtn').addEvent('click',function(e){
				
				var item = $('search-address-tel');
				this.showProvozGoogle(item);
			}.bind(this));
		}
		if ($('ShowProvozBtn2')){
			$('ShowProvozBtn2').removeEvents('click');
			$('ShowProvozBtn2').addEvent('click',function(e){
				var item = $('search-address-tel2');
				this.showProvozGoogle(item);
			}.bind(this));
		}
		$$('.google_autocomplete2').addEvent('keydown',function(e){ 
			//console.log(e);
			var item = e.target;
			if (e.key == 'enter'){
				this.showProvozGoogle(item);
			}
			
		}.bind(this));
		
		if ($('new_address')){
			
			$$('.address_clear').addEvent('click',function(e){
				
				if (e.target.getNext('div').getElement('input')){
					nextInput = e.target.getNext('div').getElement('input');
					nextInput.value = '';
					nextInput.fireEvent('change',nextInput);
					
					this.hide_records(0);
					e.target.addClass('none');
				}
			}.bind(this));
			
			
			if ($('search-address-tel')){
				if (window.getSize().x < 1000){
				$('search-address-tel').addEvent('click',function(e){
					window.scrollTo(0,0);
				});
				}
				
				search_address_value = localStorage.getItem('search_address');
				search_address_value_json = JSON.decode(localStorage.getItem('search_address_json'));
				search_address_system_id = localStorage.getItem('search_address_system_id');
				if (search_address_system_id){
					window.selectProvozId = search_address_system_id.toInt();
					
					this.hide_records();
				}
				//console.log(window.selectProvozId);
				//console.log(search_address_system_id);
				if (search_address_value != ''){
					$('search-address-tel').value = search_address_value;
					
					this.showAddressClear();
					
					
					if ($('user-user-addresses-0-street') && search_address_value_json){
						$('user-user-addresses-0-street').value = search_address_value_json.ulice;
						$('user-user-addresses-0-cp').value = search_address_value_json.cp;
						$('user-user-addresses-0-city').value = search_address_value_json.mesto;
						if ($('user-user-addresses-0-lat')) $('user-user-addresses-0-lat').value = search_address_value_json.lat;
						if ($('user-user-addresses-0-lng')) $('user-user-addresses-0-lng').value = search_address_value_json.lng;
						$('shop-provoz-id').value = search_address_value_json.provoz_id;
						$('shop-provoz-id').fireEvent('change',$('shop-provoz-id'));
					}
					if (search_address_system_id != ''){
						$$('.provoz_info').fade('hide');
						if ($('provoz_list_info_search') && $('provoz_list_info_search').hasClass('none')){

							$$('.provoz_info').fade('show');
						}
						if ($('provoz_info_id_'+search_address_system_id)){
							$('provoz_info_id_'+search_address_system_id).removeClass('none');
							$('provoz_info_id_'+search_address_system_id).fade(1);
						} else {
							//FstError('Na Vaši adresu nevozíme');
						}
					}
				}
			}
			
			
			$$('.google_autocomplete').addEvent('keyup',function(e){
				//console.log($('address-value').value);
					
				var regex = /\d/g;
				var res = regex.test(e.target.value);
				if (res == false){
					$$('.search_address_tip').removeClass('none');
						
					$$('.pac-container').each(function(item){
						item.addClass('none');
					});
				} else {
					//google.maps.event.trigger(autocomplete, 'place_changed');
					$$('.search_address_tip').addClass('none');
					$$('.pac-container').removeClass('none');
				}
					
			});
		} else {
			//console.log('aaa');
		}
		}
		window.autocomplete = {};
		$$('.google_autocomplete').each(function(item,key){
			item.addEvent('change',function(e){
				if (!e.target){
					e.target = e;
				}
				value = e.target.value.split(' ');
				if (value[1]){
					cp_user = value.getLast();
				} else {
					cp_user = '';
				}
				if (e.target.value == ''){
					if ($$('.provoz_info').length > 1){
						$$('.provoz_info').fade(0);
					}
					localStorage.removeItem('search_address_system_id');
					localStorage.removeItem('search_address');
					localStorage.removeItem('search_address_json');
				}
				
			});
			this.map_area();
			item.addEvent('keydown',function(e){
				if ($('search_address_tip')){
					$('search_address_tip').removeClass('nonea');
				}
					
			});
			//console.log(k);
			var options = {componentRestrictions: {country: 'cz'}, radius:20};
			window.autocomplete[key] = new google.maps.places.Autocomplete(item,options);
			if ($('search_address_noresult')){
			item.addEvent('keyup',function(e){
				if (!$('body').getElement('.pac-item')){
					if ($('search_address_noresult')){
						$('search_address_noresult').removeClass('nonea');
						//item_value = item.value.split(' ');
						//console.log(item_value);
						//item.value = item_value[0];
					}
					//console.log('neni');
				} else {
					if ($('search_address_noresult')){
						$('search_address_noresult').addClass('none');
					}
					
				}
			});
			}
			//console.log(window.autocomplete);
			autocompleteListener = google.maps.event.addListener(window.autocomplete[key], 'place_changed', (autocomplete_change = function(key_data) {
					//console.log('change',key_data);
					$$('.clear_address').each(function(item){
						item.value = '';
					});
					//console.log(key);
					//console.log(window.autocomplete[key]);
					//key = 0;
					var place = window.autocomplete[key].getPlace();
					//console.log(autocomplete);
					if ($('search_address_tip')){
						$('search_address_tip').addClass('none');
					}
					//console.log(item);
					
					if (!place || !place.geometry) {
					  return;
					}
					//console.log(place.geometry);
					ulice = '';	
					mesto = '';	
					psc = '';	
					cp = '';	
					co = '';
					lat = '';
					lng = '';
					if (place.geometry){
						if (place.geometry.location){
							lat = place.geometry.location.lat();
							lng = place.geometry.location.lng();
						}
					}	
						
					Object.each(place.address_components, function(gitem){
					//console.log(item.types[0]);
						//console.log(gitem);
						
						switch(gitem.types[0]){
							case "street_address":
								
								ulice = gitem.short_name;
								break;
							case "route":
								ulice = gitem.short_name;
								break;
							case "premise":
								cp = ' '+gitem.short_name;
								break;
							case "street_number":
								co = '/'+gitem.short_name;
								break;
							case "sublocality_level_1":
								mesto = gitem.short_name;
								break;  
							case "locality":
								mesto = gitem.short_name;
								break;  
							case "postal_code":
								psc = gitem.short_name.replace(" ","");
								break;                  
						}
						if (cp == ''){
							if (typeof cp_user != 'undefined'){
								cp = cp_user;
							} else {
								cp = '';
							}
							
						}
						if ($(item.get('data-mesto'))) $(item.get('data-mesto')).value = mesto;
						if ($(item.get('data-psc'))) $(item.get('data-psc')).value = psc;
						if ($(item.get('data-cp'))) $(item.get('data-cp')).value = cp+co;
						if ($(item.get('data-ulice'))) $(item.get('data-ulice')).value = ulice;
						if ($(item.get('data-lat'))) $(item.get('data-lat')).value = lat;
						if ($(item.get('data-lng'))) $(item.get('data-lng')).value = lng;
						item.set('data-tmp_lat',lat);
						item.set('data-tmp_lng',lng);
						item.set('data-tmp_ulice',ulice);
						item.set('data-tmp_cp',cp+co);
						item.set('data-tmp_mesto',mesto);
						//console.log(item);
						
						
					});
					this.showAddressClear();
						if ($('search-address-tel') && item.get('id') == 'search-address-tel'){
							this.map_area_data();
							this.switch_provoz_tel();
							//console.log(window.polygons);
						}
						// kontrola provozovna dle GPS
						if ($('shop-provoz-id')){
							$('shop-provoz-id').fireEvent('change',$('shop-provoz-id'));
						}
						
						// rozcestnik click search
						if ($('SearchAdresa')){
							$('SearchAdresa').fireEvent('click',$('SearchAdresa'));
						}
						
						
						
					//console.log(mesto);
			}.bind(this)));
			if (item.value != ''){
				//console.log(item);
				//console.log(autocomplete);
				//console.log(key);
				if ($('search-address-tel')){
					//google.maps.event.trigger(item, 'place_changed');
					//google.maps.event.trigger( item, 'focus', {} );
					//console.log('trg');
					
					//google.maps.event.trigger(window.autocomplete[key], 'place_changed',key);
					//google.maps.event.trigger('place_changed');
				}
			}
		
		
		}.bind(this));
	
		
	},
	
	// load google api
	load_google_api: function(){
			function loadScriptMap(){
			  if(typeof google == 'undefined'){
			  var script = document.createElement("script");
			  //console.log(google);
			  script.type = "text/javascript";
			  //script.src = "http://maps.googleapis.com/maps/api/js?&libraries=places&sensor=false&callback=initialize";
			  script.src = "https://maps.googleapis.com/maps/api/js?v=3&libraries=places,geometry&sensor=false&callback=Fst.init_google_autocomplete&key=AIzaSyAmRIyQUbK5x_c_cm78ZV5ODf-sqVO1-2s";
			  document.body.appendChild(script);
			  }
			}
			if ($('body').getElement('.google_autocomplete')){
				
				//loadScriptMap();
			}
			if ($('body').getElement('.google_autocomplete2')){
				
				loadScriptMap();
			}
			
		
		//});
	}
	
	
	
	
	
});
window.addEvent('domready',function(){
	Fst = new FstPages();
	//console.log(this.Fst.test());
});
	



