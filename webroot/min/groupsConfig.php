<?php
@define('MIN_ROOT_PATH','/p-chachar');
@define('MIN_WEBROOT','/webroot');

// JS
$internal_js = [
	'//js/fastesthost/core1.6.js', // hezke alerty a error
	'//js/fastesthost/c_more1.6.js', // hezke alerty a error
	'//js/fastesthost/fst_history.js', // hezke alerty a error
	'//js/tempo/tempo.js', // hezke alerty a error
	'//js/mbox/min/mBox.Core.min.js', // hezke alerty a error
	'//js/mbox/min/mBox.Notice.min.js', // hezke alerty a error
	//'//js/GoogleMaps/MarkerWithLabel.js',   // map marker
	
	
	'//js/chachar.js',   // Chachar JS
	
	'//js/pages.js',   //obdoba c_page_load.js
	'//js/lang/cz.js',   // preklady JS
    '//js/global_fce.js',   // globalni funkce pro JS
    
    
	'//shop/js/shop.js',   // Shop JS
	'//js/mapAreas/mapAreas.js'
	
];
$external_js = [
	//'https://scripts.fastesthost.cz/js/mootools1.4/core1.6.js',
	//'https://scripts.fastesthost.cz/js/mootools1.4/c_more1.6.js',
	//'https://scripts.fastesthost.cz/js/history/fst_history.js',
];

// CSS
$internal_css = [
	//'//css/Bootstrap/font-awesome.min.css',
	//'//css/Bootstrap/bootstrap.min.css',
	'//js/fastesthost/bootstrap.css',
	'//css/awesome.css',
	'//css/css_compile/default.css',
	'//css/css_compile/response.css',
	'//js/mbox/assets/mBoxNotice.css',
	
	'//css/css_compile/shop.css', // shop css
	'//css/css_compile/shop_response.css', // shop css
];

$external_css = [
	//'https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css',
	//'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css',
	//'https://fonts.googleapis.com/css?family=Catamaran:400,600,900',
];

$mobile_css = [  
	'//css/Bootstrap/font-awesome.min.css',
	//'//css/Bootstrap/bootstrap.min.css',
	'//css/css_compile/default.css',
	'//css/css_compile/mobile.css',
	'//js/mbox/assets/mBoxNotice.css',
	
];

$mobile_js = [
	'//js/fastesthost/core1.6.js', // hezke alerty a error
	'//js/fastesthost/c_more1.6.js', // hezke alerty a error
	'//js/fastesthost/fst_history.js', // hezke alerty a error
	
	'//js/mbox/min/mBox.Core.min.js', // hezke alerty a error
	'//js/mbox/min/mBox.Notice.min.js', // hezke alerty a error
	'//js/global_fce.js',
	'//js/pages.js',
	'//js/nosleep/nosleep.js',
	'//js/CountDown/PeriodicalExecuter.js',
	'//js/CountDown/CountDown.js',
	'//js/mobile.js',
	
	//'//js/fst_uploader/fst_uploader.js',
];




/* neupravovat dale*/

$ext_css = [];
foreach($external_css AS $k=>$path){
	
	$content = file_get_contents($path);
	//$src = new Minify_Source(array('id' => 'source'.$k,'content' => $content,'contentType' => Minify::TYPE_CSS,'lastModified' => ($_SERVER['REQUEST_TIME'] - $_SERVER['REQUEST_TIME'] % 86400),));
	$src = new Minify_Source(array('id' => 'source'.$k,'content' => $content,'contentType' => Minify::TYPE_CSS,'lastModified' => 604800,));
	
	
	$ext_css[] = $src; 
}

$ext_js = [];
foreach($external_js AS $k=>$path){
	$content = file_get_contents($path);
	$src = new Minify_Source(array('id' => 'source'.$k,'content' => $content,'contentType' => Minify::TYPE_JS,'lastModified' => 604800,));
	$ext_js[] = $src; 
}

$result = [
	'css'=>[],
	'js'=>[],
	'js_mobile'=>[],
	'css_mobile'=>[],
];
$result['css'] = array_merge($ext_css,$internal_css);

$result['css_mobile'] = array_merge($ext_css,$mobile_css);
$result['js'] = array_merge($ext_js,$internal_js);
$result['js_mobile'] = array_merge($ext_js,$mobile_js);

if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65a'  || (isset($_SERVER['HTTP_USER_AGENT']) && stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') == true)){
	//pr($result['css']);
	//unset($result['css'][0]);
	unset($result['css'][1]);
	unset($result['css'][2]);
	unset($result['css'][3]);
	unset($result['css'][4]);
	unset($result['css'][5]);
	unset($result['css'][6]);
	//pr($result['css']);die('a');
}
foreach($internal_js AS $k=>$i){
	$internal_js[$k] = '/'.ltrim($i,'/');
}
foreach($internal_css AS $k=>$i){
	$internal_css[$k] = '/'.ltrim($i,'/');
}
foreach($mobile_js AS $k=>$i){
	$mobile_js[$k] = '/'.ltrim($i,'/');
}
foreach($mobile_css AS $k=>$i){
	$mobile_css[$k] = '/'.ltrim($i,'/');
}

$result['js_links'] = array_merge($external_js,$internal_js);
$result['js_links_mobile'] = array_merge($external_js,$mobile_js);
$result['css_links'] = array_merge($external_css,$internal_css);
$result['css_links_mobile'] = array_merge($external_css,$mobile_css);
//pr($result['js_mobile']);die();

    
return $result;