var changingHash = false;
var FstBarcodes = this.FstPages = new Class({
	Implements:[Options,Events],
	
	options: {
		input_class: 'scan_code',
		input_class_multi: 'scan_code_multi',
	},
	
	
	// init fce
	initialize:function(options){
		this.setOptions(options);
		
		console.log('run barcodes');
		this.init_input_events();
	},
	
	// input events
	init_input_events: function(){
		$$('.'+this.options.input_class).addEvent('click',function(e){
			input_el = e.target.get('data-el');
			input_id = e.target.get('data-id');
			this.getScan(input_el,input_id);
			
		}.bind(this));
		
		$$('.'+this.options.input_class_multi).addEvent('change',function(e){
			if (!e.target) e.target = e;
			el = e.target;
			clone = el.clone().cloneEvents(el);
			clone.set('data-id',clone.get('data-id').toInt() + 1);
			clone.value = '';
			clone.inject(el,'after');
		});
		
		window.addEventListener("hashchange", function(e){
			this.onbarcode(e);
		}.bind(this), false);
	},
	
	
	// get scanner
    getScan: function(input_el,input_id){
        var href = window.location.href;
        var ptr = href.lastIndexOf("#");
        if(ptr>0){
            href = href.substr(0,ptr);
        }
		
        window.addEventListener("storage", function(e){
			this.onbarcode(e);
		}.bind(this), false);
        setTimeout('window.removeEventListener("storage", (function(e){this.onbarcode(e)}.bind(this)), false)', 15000);
       localStorage.removeItem("barcode");
        
		//window.open  (href + "#zx" + new Date().toString());
		redirect_url = "zxing://scan/?ret=" + encodeURIComponent(href + "#zx{CODE}|"+input_el+"|"+input_id);	
		//console.log(Browser.Platform);
		if (Browser.Platform.name == 'android'){
		
        if(navigator.userAgent.match(/Firefox/i)){
            //Used for Firefox. If Chrome uses this, it raises the "hashchanged" event only.
            
			window.location.href =  (redirect_url);
        }else{
            //Used for Chrome. If Firefox uses this, it leaves the scan window open.
            window.open   (redirect_url);
        }
		} else {
			console.log('bude presmerovano na aplikaci '+redirect_url);
		}
    },
	
	// zpracovani barcode do inputu
    processBarcode: function(bc){
        bc = bc.split('|');
		select_input = false;
		console.log(bc);
		$$('.'+this.options.input_class).each(function(item){
			if (item.get('data-el') == bc[1]){
				if (bc[2] != 'null' && item.get('data-id') == bc[2]){
					select_input = item;
				} else {
					select_input = item;
				}
			}
		});
		console.log(select_input);
		if (select_input){
			select_input.value = bc[0];
			select_input.fireEvent('change',select_input);
		}
		//document.getElementById("scans").innerHTML += "<div>" + bc + "</div>";
	},
	
	// detect change hash barcode url
	onbarcode: function(event){
        switch(event.type){
            case "hashchange":{
                if(changingHash == true){
                    return;
                }
                var hash = window.location.hash;
                if(hash.substr(0,3) == "#zx"){
                    hash = window.location.hash.substr(3);
                    changingHash = true;
                    window.location.hash = event.oldURL.split("\#")[1] || ""
                    changingHash = false;
                    this.processBarcode(hash);
                }

                break;
            }
            case "storage":{
                window.focus();
                if(event.key == "barcode"){
                    window.removeEventListener("storage", function(e){
						this.onbarcode(e);
					}.bind(this), false);
                    this.processBarcode(event.newValue);
                }
                break;
            }
            default:{
                console.log(event)
                break;
            }
        }
    }

	
});
