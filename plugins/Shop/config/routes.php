<?php
use Cake\Routing\Router;
use Cake\Core\Configure;

@define('DPH_CON',0.1736);
@define('DETAIL_LINK','detail');
@define('KOSIK_LINK','basket');
@define('ORDERS_LINK','orders');
@define('vase_objednavky','orders/user/');
@define('GOPAY_DONE_URL','/basket/complete/');
@define('GOPAY_NOTIFY_URL','gopay_notify');


Router::scope('/', function ($routes) {
	
	$routes->connect('/'.GOPAY_NOTIFY_URL.'/*', ['controller' => 'ShopOrders','plugin'=>'Shop','action'=>'gopay_notify','direct'=>true]);
	$routes->connect('/products_search/*', ['controller' => 'ShopProducts','plugin'=>'Shop','action'=>'products_search']);
	$routes->connect('/'.ORDERS_LINK.'/:action/*', ['controller' => 'ShopOrders','plugin'=>'Shop']);
	$routes->connect('/'.vase_objednavky.'/*', ['controller' => 'ShopOrders','plugin'=>'Shop','action'=>'client_orders']);
	$routes->connect('/loadGps/*', ['controller' => 'ShopOrders', 'plugin'=>'Shop','action' => 'loadGps']);
	$routes->connect('/rozvozce_add/*', ['controller' => 'ShopOrders', 'plugin'=>'Shop','action' => 'rozvoceAddOrder']);
	
	$routes->connect('/djApi/:action/*', ['controller' => 'ShopDjApis','plugin'=>'Shop']);
	
	$routes->connect('/api/:action/*', ['controller' => 'ShopApis','plugin'=>'Shop']);
	$routes->connect('/api/add/*', ['controller' => 'ShopApis','plugin'=>'Shop','action'=>'addApi']);
	$routes->connect('/confirm_email/*', ['controller' => 'ShopOrders','plugin'=>'Shop','action'=>'confirmEmail']);
	$routes->connect('/export_order/*', ['controller' => 'ShopApis','plugin'=>'Shop','action'=>'exportOrder']);
	$routes->connect('/switch_online/*', ['controller' => 'ShopApis','plugin'=>'Shop','action'=>'switch_online']);
	$routes->connect('/change_order/*', ['controller' => 'ShopOrders','plugin'=>'Shop','action'=>'changeOrder']);
	$routes->connect('/products/json/*', ['controller' => 'ShopApis','plugin'=>'Shop','action'=>'products']);
	$routes->connect('/products_atr/*', ['controller' => 'ShopApis','plugin'=>'Shop','action'=>'productAtr']);
	$routes->connect('/export/:action/*', ['controller' => 'ShopExports','plugin'=>'Shop']);
	$routes->connect('/'.KOSIK_LINK.'/*', ['controller' => 'ShopOrders','action'=>'basket_detail','plugin'=>'Shop']);
	$routes->connect('/'.DETAIL_LINK.'/*', ['controller' => 'ShopProducts','action'=>'detail','plugin'=>'Shop']);
	$routes->connect('/', ['controller' => 'ShopProducts', 'action' => 'homepage','plugin'=>'Shop']);
	
	
	$routes->connect('/google.xml', ['controller' => 'ShopExports', 'action' => 'google','plugin'=>'Shop']);
	$routes->connect('/zbozi.xml', ['controller' => 'ShopExports', 'action' => 'zbozi','plugin'=>'Shop']);
	$routes->connect('/heureka.xml', ['controller' => 'ShopExports', 'action' => 'heureka','plugin'=>'Shop']);
	
	$routes->connect('/*', ['controller' => 'ShopProducts', 'action' => 'index','plugin'=>'Shop']);
	
	$routes->fallbacks('DashedRoute');
});

$scopes = function ($routes) {
	$routes->connect('/products_search/*', ['controller' => 'ShopProducts','plugin'=>'Shop','action'=>'products_search']);
	$routes->connect('/', ['controller' => 'ShopProducts', 'action' => 'homepage','plugin'=>'Shop']);
	$routes->connect('/*', ['controller' => 'ShopProducts', 'action' => 'index','plugin'=>'Shop']);
	$routes->connect('/'.DETAIL_LINK.'/*', ['controller' => 'ShopProducts','action'=>'detail','plugin'=>'Shop']);
	$routes->connect('/'.ORDERS_LINK.'/:action/*', ['controller' => 'ShopOrders','plugin'=>'Shop']);
	$routes->connect('/'.KOSIK_LINK.'/*', ['controller' => 'ShopOrders','action'=>'basket_detail','plugin'=>'Shop']);
	
};

$languages = Configure::read('languages_list');
foreach ($languages as $lang) {
	Router::scope("/$lang", ['lang' => $lang], $scopes);
}
	

