/**
 * Created by Fastest Solution s.r.o. Jakub Tyson on 10.1.2017.
 Shop function
 */
 
window.template = {};
window.debug = false; 

var FstShopJs = this.FstShopJs = new Class({
	Implements:[Options,Events],
	options: {
		php_element_kosik:'php_basket_products',
	},
	// init fce 
	initialize:function(options){
		this.setOptions(options);
		if (!$('debug_js')){
			this.products_basket_element();
		}
		this.elements_events();
		
						
		//this.products_basket(JSON.decode($('php_basket_products').get('text')));
	},
	
	// global elements events
	elements_events: function(){
		this.event_add_basket();
		this.basket_detail_events();
		this.ga_ecommerce();
		this.price_slider();
		this.other_imgs_slider();
		this.scroll_subcats();
		this.basket_width();
		this.continue_kosik_but();
		this.user_orders();
		this.fulltext();
		this.favorite();
		this.stripsMenus();
		
		this.load_basket_content();
		
		$$('.tel_click').addEvent('click',function(e){
			e.stop();
			ga('send', 'event', 'Click telefon', e.target.get('data-name'), e.target.get('text'));
			ga('send', {
                'hitType' : 'pageview',
                'page' : '/phone_call/'
            });
			window.location = e.target.href;
		});
		
	},
	
	
	
	stripsMenus: function(){
		if ($('stripsMenus')){
			//console.log($$('.stripsMenuAdd'));
			
			this.template = Tempo.prepare("menuList");
		
			this.stripsConfig = {};
			this.strips = {
				'items': {
					1: {'id':'93','price':'129','name':'3x STRIPS + 1x PŘÍLOHA + 2x OMÁČKA','addon':1,'img':'/css/stripsy/stripsy_nepalive.png'},
					2: {'id':'94','price':'179','name':'5x STRIPS + 2x PŘÍLOHA + 2x OMÁČKA','addon':2,'img':'/css/stripsy/stripsy_nepalive.png'},
					3: {'id':'95','price':'199','name':'7x STRIPS + 2x PŘÍLOHA + 2x OMÁČKA','addon':2,'img':'/css/stripsy/stripsy_nepalive.png'},
					
					4: {'id':'96','code':'323','name':'Salátek coleslaw','img':'/css/stripsy/colleslaw.png'},
					5: {'id':'97','code':'300','name':'Hranolky běžné','img':'/css/stripsy/hranolky.png'},
					6: {'id':'98','code':'303','name':'Bramborová kaše','img':'/css/stripsy/kase.png'},
					
					7: {'id':'99','code':'324','name':'Tatarská omáčka','img':'/css/stripsy/tatarka.png'},
					8: {'id':'100','code':'313','name':'Sweet chilli','img':'/css/stripsy/sweet_chili.png'},
					9: {'id':'101','code':'320','name':'BBQ','img':'/css/stripsy/bbq.png'},
					10: {'id':'102','code':'312','name':'Kečup','img':'/css/stripsy/kecup.png'},
					11: {'id':'106','code':'326','name':'Belgická','img':'/css/stripsy/belgicka.png'},
					12: {'id':'105','code':'327','name':'Mexická','img':'/css/stripsy/mexicka.png'},
				},
				'addons': {
					1: {'name':'Salátek coleslaw','img':'/css/stripsy/colleslaw.png'},
					2: {'name':'Hranolky velké','img':'/css/stripsy/hranolky.png'},
					3: {'name':'Bramborová kaše','img':'/css/stripsy/kase.png'},
				},
				'sauces': {
					1: {'name':'Domácí tatarka','img':'/css/stripsy/tatarka.png'},
					2: {'name':'Sweet chilli','img':'/css/stripsy/sweet_chili.png'},
					3: {'name':'BBQ','img':'/css/stripsy/bbq.png'},
					4: {'name':'Kečup','img':'/css/stripsy/kecup.png'},
				}
			}
			
			this.sMenu = [];
			//this.sMenu.push({'items':[{'name':'test'},{'name':'test2'},{'name':'test3'}]});
			//console.log(this.sMenu);
			/*
			this.stripsList = {
				1: {'name':'3x STRIPS + 1x PŘÍLOHA + 2x OMÁČKA','addon':1,'img':''},
				2: {'name':'5x STRIPS + 2x PŘÍLOHA + 2x OMÁČKA','addon':2,'img':''},
				3: {'name':'7x STRIPS + 2x PŘÍLOHA + 2x OMÁČKA','addon':2,'img':''},
			}
			this.stripsAddonList = {
				1: {'name':'Salátek coleslaw','img':''},
				2: {'name':'Hranolky velké','img':''},
				3: {'name':'Bramborová kaše','img':''},
			}
			*/
			$$('.step').addClass('none');
			$('step1').removeClass('none');
			
			
			$('stripsMenus').getElements('.next_step').addEvent('click',function(e){
				e.stop();
				// first
				if (e.target.get('data-type') == 'firstStep'){
					$('tabPrilohy').removeClass('none');
					var id = e.target.get('data-id').toInt();
					this.stripsConfig['type'] = id;
					if (!this.stripsConfig['addons']){
						this.stripsConfig['addons'] = 0;
						this.stripsConfig['souces'] = 0;
						this.stripsConfig['countSouces'] = 2;
					}
					e.target.getParent('.item').getElement('.second').adopt($('stripsSelect'));
					
					$('stripsSelect').removeClass('none');

					//this.countAddonLi();
				}
				
				if (e.target.get('data-type') == 'addonStep'){
					$('tabPrilohy').removeClass('none');
					id = e.target.get('data-id').toInt();
					
					if (this.stripsConfig['addons']+1 <= this.stripsConfig['countAddon']){
						this.stripsConfig['addons']++;
						this.stripsMenuRender(id);
						var countAddon = (this.stripsConfig['countAddon'] - this.stripsConfig['addons']);
				
						this.stripsMenuRenderDesc('Vyberte si ještě '+(countAddon)+' přílohu v ceně menu');
						
						//console.log('countAddon',countAddon);
						if (countAddon == 0){
							
							this.stripsMenuRenderDesc('Vyberte si ještě '+(this.stripsConfig['countSouces'])+' '+((this.stripsConfig['countSouces']>1)?'omáčky':'omáčku')+' v ceně menu');
						
							$('tabPrilohy').addClass('none');
							$('tabOmacky').removeClass('none');
							
							$('stripSteps').getElements('li.active').addClass('filled');
							$('stripSteps').getElements('li').removeClass('active');
							$('stripStep3').addClass('active');

						}
				
						//li = new Element('li',{'text':this.stripsAddonList[id].name}).inject($('menuList'));
						//img = new Element('img',{'src':this.stripsAddonList[id].img}).inject(li);
						//this.countAddonLi(); 
						
					} else {
						FstError('Můžete přidat pouze '+this.stripsConfig['countAddon']+' přílohy');
					}
					
					
				}
				
				if (e.target.get('data-type') == 'omackyStep'){
					$('tabOmacky').removeClass('none');
					id = e.target.get('data-id').toInt();
					//console.log(this.stripsConfig['souces']);
					//console.log(this.stripsConfig['countSouces']);
					if (this.stripsConfig['souces']+1 <= this.stripsConfig['countSouces']){
						this.stripsConfig['souces']++;
						this.stripsMenuRender(id);
						var countSouces = (this.stripsConfig['countSouces'] - this.stripsConfig['souces']);
				
						this.stripsMenuRenderDesc('Vyberte si ještě '+(countSouces)+' '+((countSouces>1)?'omáčky':'omáčku')+' v ceně menu');
						
						//console.log('countAddon',countAddon);
						if (countSouces == 0){
							
							this.stripsMenuRenderDesc('');
							
							$('tabPrilohy').addClass('none');
							$('tabOmacky').addClass('none');
							$('stripsMenus').addClass('none');
							
							//console.log(this.sMenu);	
							//console.log(this.stripsConfig);	
							req_form = new Request.JSON({
								url:'/orders/add_basket/',
								data:{'data':JSON.encode(this.sMenu),'config':JSON.encode(this.stripsConfig),'img':$('item423').getElement('.img').get('data-original')},
								onError: this.req_error = (function(data){
									//button_preloader(button);
								}).bind(this),

								onComplete :(function(json){
									this.init_after_basket_change(json);
									this.sMenu = [];
									this.stripsConfig = {};
									FstAlert('Přidáno do košíku');
									
									$('stripSteps').getElements('li').removeClass('filled');
									$('stripSteps').getElements('li').removeClass('active');
									$('stripStep1').addClass('active');
									this.stripsMenuRender();
									$$('.step').addClass('none');
									$('step1').removeClass('none');
									$('stripsSelect').addClass('none');
									$('stripsMenus').getElements('.select').removeClass('fa-check');
									$('products_list').removeClass('none');
									//window.location = window.location;
											
								}).bind(this)
							});
							req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
							req_form.post();
						}
				
						//li = new Element('li',{'text':this.stripsAddonList[id].name}).inject($('menuList'));
						//img = new Element('img',{'src':this.stripsAddonList[id].img}).inject(li);
						//this.countAddonLi(); 
						
					} else {
						FstError('Můžete přidat pouze '+this.stripsConfig['countSouces']+' přílohy');
					}
					
					
				}
				
				console.log(this.stripsConfig);
			}.bind(this));
			
			// second select count
			$('stripsMenus').getElements('.select').addEvent('click',function(e){
				$('stripsMenus').getElements('.select').each(function(item){
					item.removeClass('fa fa-check');
				});
				e.target.addClass('fa fa-check');
				var id = e.target.get('data-id').toInt();
				this.stripsConfig['idItems'] = id;
				this.stripsConfig['countAddon'] = this.strips['items'][id].addon;
				
				this.stripsMenuRender(id);
				
				$('stripSteps').getElements('li.active').addClass('filled');
				$('stripSteps').getElements('li').removeClass('active');

				//přidat id stripSteps classu filled když byl krok vyplněn

				$('stripStep2').addClass('active');
				
				$$('.step').addClass('none');
				$('step2').removeClass('none');
				
				//console.log(this.stripsConfig['addons']);
				//console.log(this.stripsConfig['countAddon']);
				var countAddon = (this.stripsConfig['countAddon'] - this.stripsConfig['addons']);
				this.stripsMenuRenderDesc('Vyberte si ještě '+countAddon+' '+((countAddon>1)?'přílohy':'přílohu')+' v ceně menu');
				 
				//$('menuListCountName').set('text',this.stripsList[this.stripsConfig['idItems']].name);
				//img = new Element('img',{'src':this.stripsList[this.stripsConfig['idItems']].img}).inject($('menuListCountName'));
				
				//this.countAddonLi(); 
			}.bind(this));
			
			
			
		}
	},
	
	
	stripsMenuRender: function(id){
		if (id)
		this.sMenu.push(this.strips['items'][id]);
		//console.log('this.sMenu',this.sMenu);				
		//Tempo.prepare("menuList").render(this.sMenu);
	
		this.template.render(this.sMenu);
			
	},
	
	stripsMenuRenderDesc: function(desc){
		$('menuDesc').set('text',desc);
	},
	
	load_basket_content: function(){
		if ($('load_basket_content')){
			if ($('basket_product_table')) 
			$('basket_product_table').addClass('preloader');
			this.req_form = new Request.HTML({
					url:'/orders/basketContent/',
					update:'load_basket_content',
					onError: this.req_error = (function(data){
						//button_preloader(button);
					
					}).bind(this),
		
					onComplete :(function(json){
						if ($('basket_product_table'))
						$('basket_product_table').removeClass('preloader');
						this.products_basket_element();
						this.continue_kosik_but();
		
					}).bind(this)
				});
				this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
				this.req_form.send();
					
		}
	},
	
	favorite_reorder: function(){
		var favorite_list = new Hash.Cookie('favorite_list', {duration: 3600});
		fav_list = favorite_list.get('favorite');
		//console.log(fav_list);
		Object.each(fav_list,function(item){
			if ($('fav'+item)){
				$('fav'+item).getParent('.product_item').inject($('products_list').getElement('.product_item'),'after');
			}
		});
	},
	
	favorite: function(){
		if ($('products_list')){
			var favorite_list = new Hash.Cookie('favorite_list', {duration: 360000});
			//console.log(favorite_list.get('favorite'));
			if (favorite_list.get('favorite') == null){
				fav_list = {}
			} else {
				fav_list = favorite_list.get('favorite');
				this.favorite_reorder()
			}
			if (Object.getLength(fav_list)>0){
				Object.each(fav_list,function(item){
					if ($('fav'+item)){
						$('fav'+item).addClass('active');
						$('fav'+item).set('title','Odebrat z oblíbených');
					}
				});
			}
			//console.log(fav_list);
			$('products_list').getElements('.favorite').addEvent('click',function(e){
				e.stop();
				if (e.target.hasClass('active')){
					e.target.removeClass('active');
					delete fav_list[e.target.get('data-id')];
					favorite_list.set('favorite', fav_list);
				} else {
					e.target.addClass('active');
					fav_list[e.target.get('data-id')] = e.target.get('data-id');
					favorite_list.set('favorite', fav_list);
				}
				
				
				this.req_form = new Request.JSON({
					url:'/users/save_favorite/',
					onError: this.req_error = (function(data){
						//button_preloader(button);
					
					}).bind(this),

					onComplete :(function(json){
					}).bind(this)
				});
				this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
				this.req_form.send();
				
			});
			//favorite_list.set('favorite', fav_list);
			
		}
	},
	
	replaceDiacritics: function(text){

	  var diacritics = {
		  a: 'ÀÁÂÃÄÅàáâãäåĀāąĄ',
		  c: 'ÇçćĆčČ',
		  d: 'đĐďĎ',
		  e: 'ÈÉÊËèéêëěĚĒēęĘ',
		  i: 'ÌÍÎÏìíîïĪī',
		  l: 'łŁ',
		  n: 'ÑñňŇńŃ',
		  o: 'ÒÓÔÕÕÖØòóôõöøŌō',
		  r: 'řŘ',
		  s: 'ŠšśŚ',
		  t: 'ťŤ',
		  u: 'ÙÚÛÜùúûüůŮŪū',
		  y: 'ŸÿýÝ',
		  z: 'ŽžżŻźŹ'
		}

		  for(var toLetter in diacritics) if(diacritics.hasOwnProperty(toLetter)) {
			for(var i = 0, ii = diacritics[toLetter].length, fromLetter, toCaseLetter; i < ii; i++) {
			  fromLetter = diacritics[toLetter][i];
			  if(text.indexOf(fromLetter) < 0) continue;
			  toCaseLetter = fromLetter == fromLetter.toUpperCase() ? toLetter.toUpperCase() : toLetter;
			  text = text.replace(new RegExp(fromLetter, 'g'), toCaseLetter);
			}
		  }
	

	  return text;
	},
	
	
	fulltext: function(){
		if ($('search-txt')){
			$('search-txt').value = '';
			$('search-txt').addEvent('change',function(e){
				if (e && e.key == 'enter'){
					$('search-txt').fireEvent('change');
				}
			});
			
			if ($('scrollToTop')){
				$('scrollToTop').addEvent('click',function(e){
					var myFx = new Fx.Scroll($(document.body)).start(0, 0);
				});
			}
			
			
			$('show_search').addEvent('click',function(e){
				$('search_line').toggleClass('close_side');
				if (!$('search_line').hasClass('close_side')){
					$('search-txt').focus();
					if (window.getSize().x < 400){
						$('provoz_disable_online').addClass('none');
						$('layout_kosik_element').addClass('none');
					}
				} else {
					if ($('provoz_disable_online'))
					$('provoz_disable_online').removeClass('none');
					$('layout_kosik_element').removeClass('none');
					
				}
			});
			
			$('search-txt').addEvent('change',function(e){
				value = this.replaceDiacritics($('search-txt').value);
				if (value == ''){
					$('noresult').addClass('none');
					$$('.product_item').removeClass('none');
					$('resetFiltr').addClass('none');
					
					$('tooltip').addClass('none');
					$('show_search').fireEvent('click');
				} else {
				$('resetFiltr').removeClass('none');
				ga('send', 'event', 'Hledej', 'search', value);

				var show_list = [];
				$$('h2.title').each(function(item){
					val = this.replaceDiacritics(item.get('data-title'));
					if (val.test(value,'i')){
						show_list.push(item.getParent('.item').get('id'));
					}
				}.bind(this));
				$$('p.description').each(function(item){
					val = this.replaceDiacritics(item.get('data-title'));
					if (val.test(value,'i')){
						show_list.push(item.getParent('.item').get('id'));
					}
				}.bind(this));
				$$('span.num').each(function(item){
					//if (item.get('text').test(value,'i')){
					if (item.get('text').toInt() == value.toInt()){
						show_list.push(item.getParent('.item').get('id'));
					}
				});
				$$('.product_item').addClass('none');
				if (show_list.length > 0){
					$('noresult').addClass('none');
				
					show_list.each(function(it){
						if ($(it)){
							$(it).getParent('.product_item').removeClass('none');
						}
					});
					//console.log(hide_list);
				} else {
					$('noresult').removeClass('none');
				}
					
				var myFx = new Fx.Scroll(window,{
				offset: {
					x: 0,
					y: ((window.getSize().x < 400)?-120:0)
				}
				}).toElement('contents');
				
				window.scrollTo(window.scrollX, window.scrollY +1);
                window.scrollTo(window.scrollX, window.scrollY -1);
				//if (window.getSize().x < 400){
					$('show_search').fireEvent('click');
				//}
				$('tooltip').removeClass('none');
				}
			}.bind(this));
			
			
			$('resetFiltr').addEvent('click',function(e){
				$('search-txt').value = '';
				$('search-txt').fireEvent('change');
				
			});
			
		}
	},
	
	user_orders: function(){
		//console.log(init_order_map);
		if ($('map_close')){	 
			 /*
			 var script = document.createElement('script');
			  script.type = 'text/javascript';
			  script.src = 'https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=drawing,geometry,places' + '&signed_in=true&callback=init_order_map';
			  document.body.appendChild(script);
			*/
			$('map_close').addEvent('click',function(){
				$('map_orders').addClass('none');
				
			});
			if ($('map_orders').hasClass('load')){
				window.googleMapId = $('map_orders').get('data-id');
				this.loadGps();
				ga('send', 'event', 'Zobrazeni mapy z aplikace', 'show_map', window.googleMapId);
				(function(){
					this.loadGps();
				}).periodical(5000,this);
				
			}
		}
		if ($('user_orders')){
			$('user_orders').getElements('.show_detail').addEvent('click',function(e){
				target_class = e.target.get('data-show');
				$('user_orders').getElements('.'+target_class).toggleClass('none');
			});
			
			
			
			$('user_orders').getElements('.show_map').addEvent('click',function(e){
				$('map_orders').removeClass('none');
				google.maps.event.trigger(this.GoogleMap, 'resize');
			
				map_id = e.target.get('data-show');
				window.googleMapId = map_id;
				this.loadGps();
				(function(){
					this.loadGps();
				}).periodical(5000,this);
				
				//$('user_orders').getElements('.'+target_class).toggleClass('none');
			}.bind(this));
		}
	},
	
	loadGps: function(){
		this.req_form = new Request.JSON({
			url:'/loadGps/'+window.googleMapId,
			onError: this.req_error = (function(data){
				
			}).bind(this),

			onComplete :(function(json){
				 if (typeof google == 'undefined'){
					 console.log('neni');
				 }
				 var bounds = new google.maps.LatLngBounds();
				 
				 if (window.currentMarker){
					 window.currentMarker.setMap(null);
				 }
				 if (window.currentMarker2){
					 window.currentMarker2.setMap(null);
				 }
								
				svg = '<?xml version="1.0" encoding="iso-8859-1"?><!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0) --><svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" viewBox="0 0 336 336" style="enable-background:new 0 0 336 336;" xml:space="preserve" width="26px" height="26px"><g><g><g><path d="M312,123.2c0-0.4,0-0.8,0-1.2l-18.4-82.8c-4-18.4-20.4-31.6-39.2-31.6H77.6c-18.8,0-35.2,13.2-39.2,31.6L20,122.4 c-0.4,1.2-0.4,2,0,3.2c-12,6.8-20,20-20,34.8v80c0,12.4,6.8,22.8,16.4,30c0,0.4-0.4,1.2-0.4,2v28c0,15.6,12.4,28,28,28h8 c15.6,0,28-12.4,28-28v-20h176v20c0,15.6,12.4,28,28,28h8c15.6,0,28-12.4,28-28v-28c9.6-7.2,16-18.8,16-32V160 C336,143.6,326,129.6,312,123.2z M54.4,42.8C56.8,32,66.4,24,77.6,24h176.8c11.2,0,21.2,8,23.2,18.8l17.2,77.2H40 c-0.8,0-2,0-2.8,0L54.4,42.8z M64,300c0,6.8-5.2,12-12,12h-8c-6.8,0-12-5.2-12-12v-22c4,1.2,8,2,12,2h20V300z M304,300 c0,6.8-5.2,12-12,12h-8c-6.8,0-12-5.2-12-12v-20h24c2.8,0,5.6-0.4,8-0.8V300z M320,240c0,13.2-10.8,24-24,24H44 c-12.8,0-28-10.8-28-24v-80c0-13.2,10.8-24,24-24h256c13.2,0,24,10.8,24,24V240z" fill="#D80027"/><path d="M60,172c-13.2,0-24,10.8-24,24s10.8,24,24,24s24-10.8,24-24S73.2,172,60,172z M60,204c-4.4,0-8-3.6-8-8c0-4.4,3.6-8,8-8 c4.4,0,8,3.6,8,8C68,200.4,64.4,204,60,204z" fill="#D80027"/><path d="M276,172c-13.2,0-24,10.8-24,24s10.8,24,24,24s24-10.8,24-24S289.2,172,276,172z M276,204c-4.4,0-8-3.6-8-8 c0-4.4,3.6-8,8-8c4.4,0,8,3.6,8,8C284,200.4,280.4,204,276,204z" fill="#D80027"/></g></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>';	

				svg_home = '<?xml version="1.0" encoding="iso-8859-1"?><!-- Generator: Adobe Illustrator 16.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0) --><!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd"><svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" width="26px" height="26px" viewBox="0 0 460.298 460.297" style="enable-background:new 0 0 460.298 460.297;" xml:space="preserve"><g><g><path d="M230.149,120.939L65.986,256.274c0,0.191-0.048,0.472-0.144,0.855c-0.094,0.38-0.144,0.656-0.144,0.852v137.041c0,4.948,1.809,9.236,5.426,12.847c3.616,3.613,7.898,5.431,12.847,5.431h109.63V303.664h73.097v109.64h109.629c4.948,0,9.236-1.814,12.847-5.435c3.617-3.607,5.432-7.898,5.432-12.847V257.981c0-0.76-0.104-1.334-0.288-1.707L230.149,120.939z"/><path d="M457.122,225.438L394.6,173.476V56.989c0-2.663-0.856-4.853-2.574-6.567c-1.704-1.712-3.894-2.568-6.563-2.568h-54.816c-2.666,0-4.855,0.856-6.57,2.568c-1.711,1.714-2.566,3.905-2.566,6.567v55.673l-69.662-58.245c-6.084-4.949-13.318-7.423-21.694-7.423c-8.375,0-15.608,2.474-21.698,7.423L3.172,225.438c-1.903,1.52-2.946,3.566-3.14,6.136c-0.193,2.568,0.472,4.811,1.997,6.713l17.701,21.128c1.525,1.712,3.521,2.759,5.996,3.142c2.285,0.192,4.57-0.476,6.855-1.998L230.149,95.817l197.57,164.741c1.526,1.328,3.521,1.991,5.996,1.991h0.858c2.471-0.376,4.463-1.43,5.996-3.138l17.703-21.125c1.522-1.906,2.189-4.145,1.991-6.716C460.068,229.007,459.021,226.961,457.122,225.438z"/></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></svg>';
				
				 // Create a marker.
				var myLatLng = new google.maps.LatLng(json.lat, json.lng);
				window.currentMarker2 = new google.maps.Marker({
				  map: this.GoogleMap,
				  icon: { url: 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(svg),labelOrigin: new google.maps.Point(11, 40)},
				  title: json.update_gps,
				  label:{text:'Váš rozvozce',color:'red'},
				  position: myLatLng
				});

				// Create home marker.
				
				var myLatLngHome = new google.maps.LatLng(json.client_lat, json.client_lng);
				//console.log(MarkerLabel);
				//console.log(MarkerLabel_);
				window.currentMarker = new google.maps.Marker({
				//window.currentMarker = new MarkerWithLabel({
				  map: this.GoogleMap,
				  icon: { 
					url: 'data:image/svg+xml;charset=UTF-8,' + encodeURIComponent(svg_home),
					labelOrigin: new google.maps.Point(11, 40)
				  },
				  title: 'Vaše adresa',
				  position: myLatLngHome,
				  label:'Vaše adresa',
				  
				  
				});

				bounds.extend(myLatLng);
				
			  this.GoogleMap.fitBounds(bounds);
				
				var zoom = this.GoogleMap.getZoom();
				this.GoogleMap.setZoom(zoom > 16 ? 16 : zoom);
				
				
			}).bind(this)
		});
		this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
		this.req_form.send();
	},
	
	load_user_map: function(){
		if ($('map_canvas')){
		var myLatLng = new google.maps.LatLng(49.6000, 16.9356);
		
		var myOptions = {
			zoom: 7,
			//panControl: pan_cntrl,
			center: myLatLng,
			scrollwheel: true,
			//mapTypeControl: false,
			//streetViewControl: false,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
		};
		if (Browser.Platform.ios || Browser.Platform.android){
			myOptions['disableDefaultUI'] = true;
		}
		//console.log($('map_canvas'));
		this.GoogleMap = new google.maps.Map($('map_canvas'), myOptions);
		}
			
	},
	
	load_orders_map: function(){
		var myLatLng = new google.maps.LatLng(49.6000, 16.9356);
		var myOptions = {
			zoom: 7,
			scrollwheel: true,
			mapTypeId: google.maps.MapTypeId.ROADMAP,
			
		};
		if (Browser.Platform.ios || Browser.Platform.android){
			myOptions['disableDefaultUI'] = true;
		}
		
		this.GoogleMap = new google.maps.Map($('map_canvas'), myOptions);
	},
	
	continue_kosik_but: function(){
			$$('.basket_link').addEvent('click',function(e){
				e.stop();
				
				if ($('basket_login')){
					
					window.history.pushState('basket', '', e.target.href);
					$('basket_login').removeClass('none');
					
					if ($('contents')){
						$('basket_login').setStyles({
							'top':$('fst_history').getCoordinates().top,
							'left':$('contents').getCoordinates().left,
							'width':$('contents').getCoordinates().width,
							'height':'115%',
							'z-index':100,
						});
						if ($('search_line'))
						$('search_line').addClass('none');
						var myFx = new Fx.Scroll($(document.body)).start(0, 0);
				
						//window.scrollTo(0,0);
					}
					
					if ($('fst_history')){
					
						$('fst_history').setStyles({
							'overflow':'hidden',
							'height':$('basket_login').getCoordinates().height - 130,
						});
					}
						
					$('but_basket_login1').addEvent('click',function(e){
						e.stop();
						$('login_line').getElement('.show_login').fireEvent('click');
					});
						
					$('but_basket_login2').addEvent('click',function(e){
						e.stop();
						window.location = e.target.href;
					});
						
					$('but_basket_login3').addEvent('click',function(e){
						e.stop();
						window.location = e.target.href+'?noreg';
					});
				
				} else {
					window.location = e.target.href;
				}
				
				
			});
			
			$$('.continue_kosik_but').addEvent('click',function(e){
				e.stop();
				e.target.getParent('.item').getElement('.done').removeClass('icon-arrow');
				//$('continue_kosik').addClass('none');
			});
			
		
	},
	
	// basket width
	basket_width: function(){
		if ($('layout_kosik_element') && $('layout_kosik_element').getElement('ul'))
		$('layout_kosik_element').getElement('ul').setStyle('width','calc(100% - 280px)');
	},
	
	// detect scroll subcats
	scroll_subcats: function(){
		if ($('subcats')){
			$('contents').addClass('pizza');
			var cat_scroll = {};
			$$('.cat_scroll').each(function(item){
				if (item.getNext('.product_item')){
				ctop = item.getNext('.product_item').getCoordinates().top;
				cat_scroll[ctop] = item.get('data-id');
				}
				//console.log(ctop);
			});
			function clear_subcats(){
				$('subcats').getElements('li').each(function(item){
					item.removeClass('active');
				});
				
			}
			window.addEvent('scroll',function(e){
				
				scroll_cat = '';
				Object.each(cat_scroll,function(id,pos){
					//if (getScrollOffsets().y + $('body').getSize().y/2 > pos){
					if (getScrollOffsets().y + $('subcats').getComputedSize().totalHeight + 50 >= pos){
						scroll_cat = id;
					}
				});
				// oznacit kategorii podle id
				if ($('select_cat'+scroll_cat)){
					
					clear_subcats();
					$('select_cat'+scroll_cat).addClass('active');
				}
				// pokud je zacatek vypisu nastavit vychozi kategorii
				if (getScrollOffsets().y < 100){
					clear_subcats();
					$('subcats').getElement('li').addClass('active');
				}	
				//console.log(scroll_cat);
				
				if (getScrollOffsets().y > ((window.screen.x < 500)?0:100)){
					$('subcats_tmp').removeClass('none');
					if (window.screen.x > 400){
						$('subcats').setStyles({
							'border-bottom':'1px solid #00A651',
							'position':'fixed',
							'margin-top':0,
							'z-index':100,
							'width':$('subcats').getCoordinates().width,
							'height':$('subcats').getCoordinates().height,
							//'top':$('subcats').getCoordinates().top,
							'top':((window.screen.x < 500)?$('navigation').getComputedSize().totalHeight + 24:0),
							'left':$('subcats').getCoordinates().left,
						});
					} else {
						$('subcats').setStyles({
							'border-bottom':'1px solid #00A651',
							'position':'fixed',
							'z-index':100,
							'width':'100%',
							'margin-top':0,
							'height':$('subcats').getCoordinates().height,
							//'top':$('subcats').getCoordinates().top,
							'top':((window.screen.x < 500)?30:0),
							'left':0,
						});

					}
				} else {
					$('subcats_tmp').addClass('none');
					$('subcats').removeProperty('style');
					if (window.screen.x < 400){
						$('subcats').setStyle('top',$('navigation').getComputedSize().totalHeight + 20);
					}
				}
			});
			
			function scroll_subcat(e){
				clear_subcats();
				e.target.addClass('active');
					
				var pos = $('cat_scroll_'+e.target.get('data-id')).getNext('.product_item').getCoordinates().top.toInt();
				pos = pos - $('subcats').getComputedSize().totalHeight;
				var myFx = new Fx.Scroll($(document.body)).start(0, pos);
				
			}
			
			// open subcats desktop
			if (window.screen.x > 400){
				$('subcats').getElements('li').addEvent('click',function(e){
					e.stop();
					scroll_subcat(e);
				});
				
			// open subcats mobile	
			} else {
				$('subcats').addEvent('click',function(e){
					$('subcats').addClass('open');
					
					$('subcats').getElements('li').addEvent('click',function(e){
						e.stop();
						if ($('subcats').hasClass('open')){
							scroll_subcat(e);	
							$('subcats').removeClass('open');
							$('subcats').getElements('li').removeEvents('click');
							
						}
						
						
					});
					
				});
			}
			
			// show foto
			
			if (Cookie.read('nofoto')){
				$$('.switch_fotos').each(function(item){
					if (!item.hasClass('nofoto')){
						item.removeClass('active');
					} else {
						item.addClass('active');
						
					}
					$$('.item').addClass('nofoto');
					$('subcats').getElements('li').removeEvents('click');
					
				});	
				
			}
			
			$$('.switch_fotos').addEvent('click',function(e){
				e.stop();
					$$('.switch_fotos').each(function(item){
						item.removeClass('active');
					});	
					
					e.target.addClass('active');
					if (e.target.hasClass('nofoto')){
						$$('.item').addClass('nofoto');
						Cookie.write('nofoto', 1, {duration: 10});
					} else {
						$$('.item').removeClass('nofoto');
						Cookie.dispose('nofoto');
						
					}
					$('subcats').removeClass('open');
				
			});
			
			
			$('subcats').getElement('li').addClass('active');
		}
	},
	
	other_imgs_slider: function(){
		if ($('body').getElement('.other_imgs')){
			var slider_in = $('body').getElement('.other_imgs');
			var slider_el = $('body').getElement('.other_imgs').getElement('div.img');
			var slider_count = $('body').getElement('.other_imgs').getElements('div.img').length;
			var slider_over = new Element('div',{'id':'other_imgs_slider'}).inject($('body').getElement('.other_imgs'),'before');
			var slider_left = new Element('div',{'class':'slider_move fa fa-angle-left'}).inject(slider_over);
			var slider_right = new Element('div',{'class':'slider_move fa fa-angle-right'}).inject(slider_over);
			
			slider_over.adopt($('body').getElement('.other_imgs'));
			
			var max_width = slider_in.getComputedSize().totalWidth;
			var move_step = slider_in.getElement('a.link').getStyle('width').toInt();
			
			//var move_step = slider_el.getSize().x;
			
			slider_in.addClass('mooswipe');
			
			slider_in.setStyles({
				'position':'absolute',
				'width': move_step * slider_count
			});
			//console.log(slider_in.getCoordinates());
			
			slider_left.addClass('none');
			
			if (slider_in.getComputedSize().totalWidth < slider_over.getSize().x){
				slider_right.addClass('none');
				
			}
			
			
			
			//console.log(move_step);
			
			function swipe(){
				new MooSwipe(slider_in, {
					onSwipeLeft: function() {
						move_right();
					},
					onSwipeRight: function() {
						move_left();
					},
					onSwipe: function(direction) {
					}
				});
		
			}
			
			window.Fst.init_mooswipe(swipe);
			
			//console.log(move_step);
			
			function move_left(){
				if (slider_in.getStyle('left') == 'auto'){
					slider_in.setStyle('left', 0);
				}
				move = slider_in.getStyle('left').toInt() + move_step;
				slider_right.removeClass('none');
					
				if (move > 0) {
					move = 0;
					slider_left.addClass('none');
				}
				slider_in.tween('left',move);
			}
			function move_right(){
				if (slider_in.getStyle('left') == 'auto'){
					slider_in.setStyle('left',0);
				}
				move = slider_in.getStyle('left').toInt() - move_step;
				slider_left.removeClass('none');
				
				lazzy_load('other_imgs');
		
				if (slider_in.getCoordinates().right < slider_over.getCoordinates().right) {
					
					slider_right.addClass('none');
					return false;
				}
				slider_in.tween('left',move);
			}
			
			slider_left.addEvent('click',move_left);
			slider_right.addEvent('click',move_right);
			
		}
	},
	
	// add event price slider http://seiyria.com/bootstrap-slider/
	price_slider: function(){
		
		if ($('price_slider')){
			var price_slider_script = Asset.css('/shop/js/BootstrapSlider/slider.css', {
				onLoad: function(){
					
					if (window.debug) console.log('loaded price slider CSS');
					
				}.bind(this)
			
			});	
			var price_slider_script = Asset.javascript('/shop/js/BootstrapSlider/slider.js', {
				id: 'price_slider_script',
				onLoad: function(){
					
					if (window.debug) console.log('loaded price slider');
					
					// format tooltip slider
					function tooltipFormat(){
						var sliderTooltip = $('price_slider_element').getElements('.tooltip-inner');
						sliderTooltip.each(function(item){
							//console.log(item.get('text'));
							txt_split = item.get('text').split(':');
							txt = price(txt_split[0]);
							if (txt_split[1]){
								txt += ' až '+price(txt_split[1]);
							}
							item.set('text',txt)
						});
					}
					
					// set default value after refresh
					if ($('price-values') && $('price-values').value != ''){
						$('price_slider').set('data-slider-value','['+$('price-values').value+']');
						value = $('price-values').value.split(',');
						$('price_slider_from').getElement('span').set('text',price(value[0]));
						$('price_slider_to').getElement('span').set('text',price(value[1]));
					}
					//console.log();
					
					var mySlider = new Slider($('price_slider'), {});
					
					// po slidovani elementu
					mySlider.on('slide', function(res) {
						var value = {'min':res[0],'max':res[1]}
						if ($('price_slider_from')){
							$('price_slider_from').getElement('span').set('text',price(value.min));
						}
						if ($('price_slider_to')){
							$('price_slider_to').getElement('span').set('text',price(value.max));
						}
						
						tooltipFormat();
						//console.log(value);
					});
					
					// po dokoneceni slider
					mySlider.on('slideStop', function(res) {
						var value = {'min':res[0],'max':res[1]}
						if ($('price-values')){
							$('price-values').value = res;
							//console.log(res);
							$('price-values').fireEvent('change',$('price-values'));
						}
						tooltipFormat();
						
						//console.log(value);
					});
					tooltipFormat();
				}.bind(this)
			});
			
			
		}
	},
	
	// show attributes modal
	show_attributes_modal: function(){
		if ($('pridavky_modal_all').hasClass('nonea')){
			this.attributes_ids = {};
			this.attributes_price = 0;
			this.pizza_price = 0;
			
			$('pridavky_modal_all').removeClass('nonea');
				this.scrollPos = getScrollOffsets().y;
				
				$('pridavky_modal').setStyles({
					'z-index':1000,
					'top':0,
					'position':'fixed',
					'overflow-y':'auto',
					'height':'100%',
				});
				$('pridavky_modal').scrollTop = 0;
				
				if ($('UpravitPridavky')){
					$('UpravitPridavky').removeEvents('click');
					$('UpravitPridavky').addEvent('click',function(e){
						e.stop();
						$('pridavky_mobile').removeClass('mobile_hide');
						$('mobile_pridavky_active').addClass('none');
					});
				}
				
				//$('body').addClass('fixed');
		} else {
			$('pridavky_modal_all').addClass('nonea');
			$('body').removeClass('fixed');
			window.scrollTo(0, this.scrollPos);
		}		
	},
	
	// add basket event
	event_add_basket: function(){
		
		// zmena selectu v detailu produktu pro vyber varianty
		$$('.change_varianta').removeEvents('change');
		$$('.change_varianta').addEvent('change',function(e){
			e.stop();
			var select_variant = e.target.value;
			var form = e.target.getParent('form');
			var variants_list = JSON.decode(form.getElement('.variants_list').value);
			$$('.variants').addClass('none');
			$('body').getElement('.variant'+select_variant).removeClass('none');
			
			if ($('variant_foto_'+select_variant)){
				$('main_image').getElement('.img').setStyle('background-image','url("'+$('variant_foto_'+select_variant).get('data-src')+'")');
			}
			
			
			//console.log(variants_list[select_variant]);
		});
		
		// zavreni seznamu z LI seznamu vypis
		$$('.variant_close').removeEvents('click');
		$$('.variant_close').addEvent('click',function(e){
			e.target.getParent('.varianta_list_item').addClass('none');
		});
		
		// vybrani varianty z LI seznamu ve vypise produktu
		$$('.varianta_list_select').removeEvents('click');
		$$('.varianta_list_select').addEvent('click',function(e){
			if (e.target.hasClass('stripsMenuAdd')){
				$('stripsMenus').removeClass('none');
				$('products_list').addClass('none');
			
			} else {
				var form = e.target.getParent('form');
				if (form.getElement('.pridavky_modal_in')){
					this.show_attributes_modal();
					// uprava pridavku na pizze
					img = e.target.getParent('.item').getElement('.img').get('data-original');
					//console.log(this.pizza_price);
					
					variants = form.getElement('.variants_attributes') || false;
					
					this.change_attributes_list(form.getElement('.pridavky_modal_in').get('data-id'),img,variants,e.target.get('data-id'),form.getParent('.item'));
				
				} else {
					if (form.getElement('.variants_select_list_el'))
					form.getElement('.variants_select_list_el').value = e.target.get('data-id');
					form.getElement('.add_to_basket').fireEvent('click',form.getElement('.add_to_basket'));
				}
			}
		}.bind(this));
		
		// zavreni pridavky modal
		if ($('pridavky_modal_close')){
		$('pridavky_modal_close').removeEvents('click');
		$('pridavky_modal_close').addEvent('click',function(e){
			e.stop();
			this.show_attributes_modal();
			
		}.bind(this));
		}
		
		
		
		// pridani do kosiku tlacitko
		$$('.add_to_basket').removeEvents('click');
		$$('.add_to_basket').addEvent('click',function(e){
			if (!e.target) e.target = e;
			if (e.stop){
				e.stop();
			}
			var form = e.target.getParent('form');
			
			// pokud ma produkt varianty na vypise zobraz LI seznam variant
			
			variant_el = form.getElement('.varianta_list_item') || false;
			if (variant_el && variant_el.hasClass('none')){
				variant_el.removeClass('none');
				variant_el.fade('hide');
				variant_el.fade(1);
				return false;
			} else {
				if (variant_el){
					//variant_el.addClass('none');
					//variant_el.addClass('preloader');
				}
			}
			
			
			button = e.target;
			div_done = e.target.getParent('.item').getElement('.done');
			preloader = e.target.getParent('.item').getElement('.preloader');
			
			// img
			img = e.target.getParent('.item').getElement('.img').get('data-original');
			if (e.target.getParent('.item').getElement('.AttributesImg')){
				AttributesImg = e.target.getParent('.item').getElement('.AttributesImg');
				AttributesImg.value = img;
			}
			
			//button_preloader(button);
			el_preloader(preloader,'active');
			
			
			this.req_form = new Request.JSON({
				url:form.action,
				onError: this.req_error = (function(data){
					//button_preloader(button);
				
				}).bind(this),

				onComplete :(function(json){
					//button_preloader(button);
					el_preloader(preloader,'active');
					
					button.addClass('icon-arrow');
					div_done.addClass('icon-arrow');
					(function(){
						button.removeClass('icon-arrow');
						div_done.removeClass('icon-arrow');
						
						if($('IP')) {
							
							var posX = div_done.getParent('.item').getCoordinates().left;
							var posY = div_done.getParent('.item').getCoordinates().top;
							
							//console.log();
							
							var newDone = div_done.clone();
							newDone.inject($('addon'));
							var h = $$('.item')[0].getSize().y;
							var w = $$('.item')[0].getSize().x;

							newDone.setStyles({'height':h,'width':w,'top':posY,'left':posX});
							newDone.addClass('icon-arrow').addClass('zoomOutDown');
						}
						
						
						
					}).delay(4500);
					
					if ($('layout_kosik_element').getElement('.foo_basket_ico')){
						$('layout_kosik_element').getElement('.foo_basket_ico').addClass('scale');
						(function(){
						$('layout_kosik_element').getElement('.foo_basket_ico').removeClass('scale');
						}).delay(500);
					}
					/*
					if ($('continue_kosik')){
						$('continue_kosik').fade(1);
						(function(){
							$('continue_kosik').fade(0);
							
						}).delay(2000);
					}
					*/
					if (json.r == true){
						FstAlert(json.m,true);
						this.init_after_basket_change(json);
						//variant_el.removeClass('preloader');
						
					}
					if (json.r == false){
						FstError(json.m);
						//variant_el.removeClass('preloader');
					}
				}).bind(this)
			});
			this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			this.req_form.post(form);
		}.bind(this));
		
	},
	
	// check count remove pridavky
	check_remove_default: function(is_default){
		if (is_default){
			console.log('def',this.remove_default);
			if (this.remove_default >= 2){
				FstError($('pridavky_modal_in').get('data-error'));
				return false;
			} else {
				return true;
			}
		}
	},
	
	// set pridavek to input
	set_attribute: function(id,type,default_attr){
		if (typeof default_attr == 'undefined'){
			default_attr = false;
		}
		//console.log(default_attr);
		if (type == 'plus'){
			
			if (!this.attributes_ids[id]){
				this.attributes_ids[id] = 1;
			} else {
				this.attributes_ids[id]++;
				
			}
			//console.log(this.attributes_ids[id]);
			//console.log(this.attributes_ids);
			if (this.attributes_ids[id] > 0)
			this.attributes_price += $('attribute'+id).getElement('.price').get('data-price').toInt();
			
		} else {
			if (!this.attributes_ids[id]){
				this.attributes_ids[id] = -1;
			} else {
				this.attributes_ids[id]--;
				
			}
			//console.log(this.attributes_ids);
			if (!default_attr || this.attributes_ids[id] > -1){
			
				this.attributes_price -= $('attribute'+id).getElement('.price').get('data-price').toInt();
			}
			if (this.attributes_price < 0){
				this.attributes_price = 0;
			}
			//console.log(this.attributes_price);
		}
		//if (!default_attr){
			this.convert_price();
		//}
		//console.log(this.attributes_price);
		//$('AttributesPrice').value = this.attributes_price;
		//console.log(product_item);
		this.product_item.getElement('.AttributesIds').value = JSON.encode(this.attributes_ids);
	},
	
	// convert price
	convert_price: function(){
		//console.log(this.pizza_price);
		//console.log(this.pizza_price_min);
		if (typeof this.attributes_price2 == 'undefined') this.attributes_price2 = 0;
		/*
		console.log('pizza',this.pizza_price);
		console.log('atr',this.attributes_price);
		console.log('atr2',this.attributes_price2);
		*/
		this.total_price = this.pizza_price + this.attributes_price+ this.attributes_price2;
		//console.log(this.total_price);
		if (this.total_price < this.pizza_price_min){
			this.total_price = this.pizza_price_min;
		}
		//console.log(this.product_item);
		this.product_item.getElement('.AttributesPrice').value = this.total_price;
		$('attribute_price_total').set('text',this.total_price);
		if (!$('attribute_price_total').get('data-default-price')){
			$('attribute_price_total').set('data-default-price',this.total_price);
		}
		$('attribute_price_total').addClass('anim');
		(function(){
			$('attribute_price_total').removeClass('anim');
		}).delay(1000);
	},
	
	// switch varianta in modal
	selectVar: function(id){
			this.select_variant_id = id;		
				//console.log('Varianta'+id);
				//console.log($('Varianta'+id));
				$('Varianta'+id).addClass('grey');
				$('Varianta'+id).set('text',$('Varianta'+id).get('data-title'));
				$('Varianta'+id).set('title',$('Varianta'+id).get('data-title'));
				
				this.select_variant_size = $('Varianta'+id).get('data-prumer');
				//console.log($('Varianta'+id).get('data-prumer'));
				
				$('attribute_imgs').removeProperty('class');
				$('attribute_imgs').addClass($('Varianta'+id).get('data-prumer'));
				this.pizza_price_min = this.pizza_price = $('Varianta'+id).get('data-price').toInt();
				
				$('attribute_price_total').set('text',this.pizza_price);
				$('attribute_price_total').set('data-default-price',this.pizza_price);
				this.convert_price();
				//$('AttributesPrice').value = this.pizza_price;
				//console.log(this.pizza_price);
			
	},
	
	// save pridavky modal
	save_attributes_modal: function(form){
		$$('.SaveAttributes').removeEvents('click');
		$$('.SaveAttributes').addEvent('click',function(e){
			e.stop();
			if (e.target.hasClass('NoAddon')){
				no_addon = true;
			} else {
				no_addon = false;
			}
			
			if (!$('own_pizza').hasClass('none')){
				if ($('sugo').value == 0){
					FstError('Musíte vybrat Omáčku');
				//} else if ($('zaklad').value == 0){ 
					//FstError('Musíte vybrat Sýr do základu');
				} else if ($('attribute_price_total').get('text').toInt() < 149){ 
					FstError('Minimální cena je 149,-');
					
				} else {
					if (no_addon){
						form.getElement('.AttributesIds').value = '';
					
						
					}
					form.getElement('.variants_select_list_el').value = this.select_variant_id;
					
					form.getElement('.add_to_basket').fireEvent('click',form.getElement('.add_to_basket'));
					this.show_attributes_modal();
						
				}
			} else {
				
					if (no_addon){
						form.getElement('.AttributesIds').value = '';
					
						
					}
				//console.log(this.select_variant_id);
				form.getElement('.variants_select_list_el').value = this.select_variant_id;
				form.getElement('.add_to_basket').fireEvent('click',form.getElement('.add_to_basket'));
				this.show_attributes_modal();
					
			}
			
				
		}.bind(this));
	},
	
	
	// change attributes list
	change_attributes_list: function(ids,img,variants,select_variant,product_item){
		this.remove_default = 0;
		this.attributes_ids = {};
		this.attributes_price = 0;
		this.attributes_price2 = 0;
		this.pizza_price = 0;
		this.syr_price = 0;
		this.sugo_price = 0;
		this.product_item = product_item;
		
		/*
		$('pridavky_mobile').getElements('.count').each(function(item){
			console.log(item);
			console.log(item.get('text'));
		});
		*/
		/*
			(function(){	
			if ($('own_pizza') && !window.tip_list.get('tip_own_pizza')){
				$('tip_own_pizza').removeClass('none');
				$('tip_own_pizza').setStyles({
					'left':150,
					'top':$('own_pizza').getCoordinates().top - $('own_pizza').getCoordinates().height,
				});
			}
			}).delay(2000);
		*/
		
		$$('.prid_select').each(function(item){
			item.getElement('span').removeClass('active');
		});
		
		//console.log(window.tip_list);
		$('attribute_name').set('html',product_item.getElement('.title').get('html'));
		$('attribute_description').set('text',product_item.getElement('.description').get('text'));
		if (product_item.getElement('.palivost')){
			$('palivost').set('html',product_item.getElement('.palivost').get('html'));
		}
		// pokud je vlastni pizza
		if (product_item.get('data-code').toInt() == 500){
			$('own_pizza').removeClass('none');
			$$('.own_pizza_hide').addClass('none');
			$('variants_adopt').addClass('none');
		} else {
			$('own_pizza').addClass('none');
			$$('.own_pizza_hide').removeClass('none');
			$('variants_adopt').removeClass('none');
		}
		
		function setActive(){
			$$('.attribute_item').each(function(item){
				if (item.get('data-default')){
					item.addClass('active_plus');
					item.getElement('.count').removeClass('none');
					item.getElement('.minus').removeClass('none');
					item.getElement('.count').set('text',1);
				}
			});
		}
		
		if (variants){
			variants = JSON.decode(variants.get('data-variants'));
			$('variants_adopt').set('html',variants);
			//console.log(variants);
			//console.log(select_variant);
			//console.log($$('.switch_varianta'));
			//console.log($('Varianta'+select_variant));
			//this.selectVar(select_variant);
			$$('.switch_varianta').removeEvents('click');
			$$('.switch_varianta').addEvent('click',function(e){
				//console.log(e);
				if (!e.target) {
					//e.target = e;
				} else {
					e.stop();
				}
				//console.log(e);
				$$('.switch_varianta').each(function(it){
					it.removeClass('grey');
					it.set('text',it.get('data-title2'));
					it.set('title',it.get('data-title2'));
				});
				if (e.target){
				varId = e.target.get('data-id');
				} else {
					varId = e;
				}
				this.selectVar(varId);
			}.bind(this));
			
			$$('.switch_varianta').each(function(item){
				//console.log(item);
				if (item.get('data-id').toInt() == select_variant.toInt()){
					item.fireEvent('click',item.get('data-id').toInt());
					//console.log(item);
				}
			});
			
			product_item.getElement('form').getElement('.zaklad_sugo').value = '';
			product_item.getElement('form').getElement('.zaklad_syr').value = '';
			$$('.select_zaklad').each(function(item){
				item.value = 0;
			}); 
			
			$$('.prid_select').addEvent('click',function(e){
				if (e.stop){
					e.stop();
				} else {
					e.target = e;
				}
				e.target.getParent('ul').getElements('li').each(function(item){
					item.getElement('span').removeClass('active');
				});
				e.target.addClass('active');
				el = e.target.getParent('li').get('data-el');
				el_id = e.target.getParent('li').get('data-id');
				$(el).value = el_id;
				//console.log($(el));
				$(el).fireEvent('change',$(el));
			});
			
			
			$$('.select_zaklad').removeEvents('change');
			$$('.select_zaklad').addEvent('change',function(e){
				if (e.stop){
					e.stop();
				} else {
					e.target = e;
				}
				total_price = $('attribute_price_total').get('data-default-price').toInt();
				//console.log(total_price);
				if (typeof this.syr_price == 'undefined') this.syr_price = 0;		
				if (typeof this.sugo_price == 'undefined') this.sugo_price = 0;		
				if (typeof this.attributes_price2 == 'undefined') this.attributes_price2 = 0;		
				//console.log(e.target.get('data-type'));
				if (e.target.get('data-type') == 'zaklad_sugo'){
					product_item.getElement('form').getElement('.zaklad_sugo').value = e.target.value;
						if (e.target.get('data-price')){
							//total_price += e.target.get('data-price').toInt();
						}
					//console.log(total_price);	
					if (e.target.value == 0){
						var add_price = 0;
					}	
					//console.log(this.select_variant_size);
					if (e.target.value == 1){
						
						//var add_price = $('attribute79').getElement('.price').get('data-price').toInt();
						if (this.select_variant_size == 'small') var add_price = $('attribute79').getElement('.price').get('data-price').toInt();
						if (this.select_variant_size == 'large') var add_price = $('attribute85').getElement('.price').get('data-price').toInt();
					}
					
					if (e.target.value == 2){
						//var add_price = $('attribute80').getElement('.price').get('data-price').toInt();
						if (this.select_variant_size == 'small') var add_price = $('attribute80').getElement('.price').get('data-price').toInt();
						if (this.select_variant_size == 'large') var add_price = $('attribute86').getElement('.price').get('data-price').toInt();
					}
					if (e.target.value == 3){
						//var add_price = $('attribute81').getElement('.price').get('data-price').toInt();
						if (this.select_variant_size == 'small') var add_price = $('attribute81').getElement('.price').get('data-price').toInt();
						if (this.select_variant_size == 'large') var add_price = $('attribute87').getElement('.price').get('data-price').toInt();
					}
					e.target.set('data-price',add_price);
					//console.log(add_price);
					this.sugo_price = add_price.toInt();
					total = total_price+this.syr_price.toInt() + this.sugo_price.toInt();
					this.pizza_price = total_price;
					this.attributes_price2 = this.syr_price.toInt() + this.sugo_price.toInt();
					$('attribute_price_total').set('text',total);
					this.convert_price();
					//product_item.getElement('form').getElement('.AttributesPrice').value = total;
					//console.log(this.pizza_price);	
				}
				if (e.target.get('data-type') == 'zaklad_syr'){
					product_item.getElement('form').getElement('.zaklad_syr').value = e.target.value;
					//console.log(this.select_variant_size);
					//total_price = $('attribute_price_total').get('text').toInt();
						//if (e.target.get('data-price')){
							//total_price = total_price - e.target.get('data-price').toInt();
						//}
					if (e.target.value == 0){
						var add_price = 0;
					}	
					if (e.target.value == 1){
						if (this.select_variant_size == 'small') var add_price = $('attribute82').getElement('.price').get('data-price').toInt();
						if (this.select_variant_size == 'large') var add_price = $('attribute88').getElement('.price').get('data-price').toInt();
					}
					
					if (e.target.value == 2){
						if (this.select_variant_size == 'small') var add_price = $('attribute83').getElement('.price').get('data-price').toInt();
						if (this.select_variant_size == 'large') var add_price = $('attribute89').getElement('.price').get('data-price').toInt();
					}
					if (e.target.value == 3){
						if (this.select_variant_size == 'small') var add_price = $('attribute84').getElement('.price').get('data-price').toInt();
						if (this.select_variant_size == 'large') var add_price = $('attribute90').getElement('.price').get('data-price').toInt();
					}
					e.target.set('data-price',add_price);
					//$('attribute_price_total').set('text',total_price+add_price.toInt());
					//console.log(add_price);
					this.syr_price = add_price.toInt();
					total = total_price+this.syr_price.toInt() + this.sugo_price.toInt();
					this.pizza_price = total_price;
					this.attributes_price2 = this.syr_price.toInt() + this.sugo_price.toInt();
					//console.log(this.attributes_price);
					$('attribute_price_total').set('text',total);
					this.convert_price();
					//product_item.getElement('form').getElement('.AttributesPrice').value = total;

				}
			}.bind(this));
			
		}
		
		
			if ($('syrSelect1')){
				//$('syrSelect1').fireEvent('click',$('syrSelect1').getElement('span')); 
			}
		$('pridavky_modal_in').removeClass('preloader');
		$('pridavky_modal').addClass('active');
		
		this.save_attributes_modal(product_item.getElement('form'));
		
		$('pridavky_modal_in').getElement('.img').set('data-original',img);
		$('pridavky_modal_in').getElement('.img').addClass('lazy');
		lazzy_load($('attribute_imgs').getElements('.img'));
		
		$$('.attribute_item').each(function(item){
			//console.log(item.getElement('.count'));
			if (!item.hasClass('default')){
				item.getElement('.count').set('text',0);
			}
			item.getElement('.count').addClass('none');
			item.getElement('.minus').addClass('none');
			item.removeClass('active_minus');
			item.removeClass('active_plus');
			item.removeProperty('data-default');
			
		});
		if (ids != ''){
			var ids = JSON.decode(ids);
			Object.each(ids,function(id){
				if ($('attribute'+id)){
					$('attribute'+id).set('data-default',1);
					$('attribute'+id).addClass('default');
				}
			});
			setActive();
		}
		
		// pridani pridavku
		$('attributes_list').getElements('.plus').removeEvents('click');
		$('attributes_list').getElements('.plus').addEvent('click',function(e){
			e.stop();
			var li = e.target.getParent('li');
			li.removeClass('active_minus');
			li.addClass('active_plus');
			li.getElement('.count').removeClass('none');
			li.getElement('.minus').removeClass('none');
			li.getElement('.count').set('text',li.getElement('.count').get('text').toInt() + 1);
			
			if (li.hasClass('default') && li.getElement('.count').get('text').toInt() == 1){
				this.remove_default--;
			}
			
			this.set_attribute(li.get('data-id'),'plus');
			
		}.bind(this));
		// odebrani pridavku
		$('attributes_list').getElements('.minus').removeEvents('click');
		$('attributes_list').getElements('.minus').addEvent('click',function(e){
			e.stop();
			var li = e.target.getParent('li');
			if (li.getElement('.count').get('text').toInt() > 0){
				//console.log('aa',this.check_remove_default(li.hasClass('default')));
			
			if (this.check_remove_default(li.hasClass('default')) || !li.hasClass('default')){
				li.getElement('.count').removeClass('none');
				li.getElement('.minus').removeClass('none');
				li.getElement('.count').set('text',li.getElement('.count').get('text').toInt() - 1);
				
				if (li.hasClass('default') && li.getElement('.count').get('text').toInt() == 0){
					this.remove_default++;
				}
				
					
				if (li.getElement('.count').get('text').toInt() == 0){
					li.removeClass('active_plus');
					li.addClass('active_minus');
				} else {
					li.addClass('active_plus');
					
				}
				
				
				this.set_attribute(li.get('data-id'),'minus',li.hasClass('default'));
		
			
			}
			
			}
			
		}.bind(this));
	},
	
	// change php_basket_products element
	change_php_basket_products: function(data){
		//console.log(data);
		if ($(this.options.php_element_kosik)){
			
			$(this.options.php_element_kosik).set('text',JSON.encode(data));
		}
	},
	
	// init after basket change
	init_after_basket_change: function(json){
		//console.log(json);
		
		// change php element
		this.change_php_basket_products(json.basket_products);
						
		// reinit basket elements
		this.products_basket_element();
		
		// basket detail events
		this.basket_detail_events();
		
		
		
	},
	
	
	// basket detail events
	basket_detail_events: function(){
		if ($('basket_detail')){
			//alert('');
			if ($('user-phone')){
				$('user-phone').addEvent('click',function(e){
					var val = e.target.value;
					e.target.value = '';
					e.target.value = val;
					
					//console.log('aaa');
				});
			}
			if ($('debug_js')){
				$$('.check_emaila').addEvent('change',function(e){
					this.req_form = new Request.JSON({
						url:'/orders/check_email/'+e.target.value,
						onError: this.req_error = (function(data){
							
						}).bind(this),

						onComplete :(function(json){
							
							if (json.r == true){
								FstAlert(json.m);
								//this.change_kosik_total_price(json.total);
							}
							if (json.r == false){
							}
						}).bind(this)
					});
					this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
					this.req_form.send();
				});
			}
			
			
			if ($('show_napoje'))
			$('show_napoje').addEvent('click',function(e){
				e.stop();
				e.target.addClass('none');
				$('basket_napoj_mobile').addClass('show');
				window.scrollTo(window.scrollX, window.scrollY +1);
                window.scrollTo(window.scrollX, window.scrollY -1);
				
			});
			
			//if ($('debug_js')){
				if ($('scroll-napoje')){
					$('scroll-napoje').getElements('.varianta_list_select').addEvent('click',function(e){
						//e.stop();
						var name = e.target.getParent('.item').getElement('.title').get('text');
						ga('send', 'event', 'Pridani v kosiku napoj', 'add_basket', name,1);
					
						//console.log(ga);
					});
				}
			//}
			
			if ($('provoz_disable_online')){
				$('provoz_disable_online').setStyles({'bottom':57,'padding':'5px'});
			}
			
			// delete from table basket
			this.basket_detail_delete_events();
			
			// hide payment on transport click 
			//this.hide_payment_on_transport();
			
			// select payment
			this.select_payment();
			this.credit_use_element();
			
			// map area
			//this.map_area();
			if ($('debug_js')){
			//console.log('run');
			//this.credit_use_element();
			}

			// change ks basket
			this.change_ks_basket();
			
			// spropitne
			this.spropitne();
			
			this.mooScroll();
			
			$('layout_kosik_element').addClass('none');
			// move to login
			//this.move_to_login();
		}
	},
	
	mooScroll: function(){
		var mySmoothScroll = new Fx.SmoothScroll({
			offset: {
				x: 0,
				y: -120
			},
			links: '.scroll_moo',
			wheelStops: false
		});
	},
	
	// spropitne
	spropitne: function(){
		
		$('spropitne').value = '';
		$('payment_video').fade('hide');
		$('spropitne_list').getElements('a').addEvent('click',function(e){
			if (e.stop){
				e.stop();
			} else {
				e.target = e;
			}
			$('spropitne_list').getElements('li').removeClass('active');
			e.target.getParent('li').addClass('active');
			var spropitne = e.target.get('data-value').toInt();
			$('spropitne').value = spropitne;
			if (typeof window.total_price == 'undefined'){
				window.total_price = $('sum_total_price').get('text').replace(' ','').replace(',-','').toInt();
			}
			//console.log(window.total_price);
			$('sum_total_price').set('text',price(window.total_price+spropitne));
			//console.log(total_price);
			/*
			var myFx = new Fx.Scroll(window,{
			offset: {
				x: 0,
				y: ((window.getSize().x < 400)?-120:0)
			}
			}).toElement('scroll-platba');
			*/
			var vibrate_list = {
				0:220,
				10:150,
				20:90,
				50:20,
			}
			
			//if (navigator.vibrate)
				//navigator.vibrate(vibrate_list[spropitne]);			
			
			$('payment_video').setStyle('height',$('platba_element').getSize().y);
			$('payment_video').set('text','Děkujeme Vám za dýško '+e.target.get('data-value')+',-');
			$('payment_video').fade(1);
			
			
			
			(function(){
				$('payment_video').fade(0);
				
			}).delay(5000);
		});
	},
	
	// map area
	map_area: function(){
		if ($('map_coords')){
			map_data = JSON.decode($('map_coords').get('text'));
			lat = 49.8481683;
			lng = 18.28562510000006;
			var polygons = {}
			Object.each(map_data,function(item){
				polygons[1000] = google.maps.geometry.encoding.decodePath(String(item.coords));
				//console.log(item);
			});
			//console.log(polygons);
			//console.log(map_data);
		}
	},
	
	// select payment method
	select_payment: function(){
		if ($('payment_list'))
		$('payment_list').getElements('a').addEvent('click',function(e){
			if (e.stop){
				e.stop();
			}
			if (e.target){
				el = e.target;
			} else {
				el = e;
			}
			$('payment_list').getElements('a').removeClass('active');
			$('payment_list').getElements('li').addClass('in_active');
			el.getParent('li').removeClass('in_active');
			el.addClass('active');
			$('shop-payment-id').value = el.get('data-id');
				
			if (el.get('data-id') == 2){
				$('spropitne_list').removeClass('none');
				
				var myFx = new Fx.Scroll(window,{
				offset: {
					x: 0,
					y: ((window.getSize().x < 400)?-120:0)
				}
				}).toElement('spropitne_list');
			} else {
				$('spropitne_list').addClass('none');
			}
		});
		if ($('shop-payment-id').value > 0){
			$('PaymentId'+$('shop-payment-id').value).fireEvent('click',$('PaymentId'+$('shop-payment-id').value));
		}
		
	},
	
	credit_use_element: function(){
		if ($('credit_use_element')){
			$('credit_use').set('text',0);
			function press_mouse(e){
				if (e){
					el = e.target;
				} else {
					el = this;
				}
				//console.log(this);
					
				if(run_fce){
					run_fce = false;
				

				
				
				var count_credit = $('count_credit').get('text').toInt();
				var credit_use = $('credit_use').get('text').toInt();
				var total_price = $('basket_detail').getElement('.shop_basket_total_price_vat').get('text').replace(' ','').toInt();
				var total_price_credit = $('credit_use_element').getElement('.shop_basket_total_price_vat').get('text').replace(' ','').toInt();
				
				
				max = $('count_credit').get('data-max').toInt();
				min = 0;
				/*
				console.log(total_price_credit);
				console.log(count_credit);
				console.log(min);
				console.log(max);
				*/
				// plus
				if (el.hasClass('plus')){
					if (count_credit > min && count_credit <= max && total_price_credit > 1){
						count_credit--;
						credit_use++;
						$('credit_use').set('text',credit_use);
						$('count_credit').set('text',count_credit);
						total_price_credit--;
					
					}
				}
				// minus
				if (el.hasClass('minus')){
					if (count_credit >= min && count_credit < max && total_price_credit >= 1){
						count_credit++;
						credit_use--;
						$('credit_use').set('text',credit_use);
						$('count_credit').set('text',count_credit);
						total_price_credit++;
					
					}
						
				}
				$('credit_use_element').getElement('.shop_basket_total_price_vat').set('text',price(total_price_credit));
				$('credit-use').value = credit_use;
				$('credit-use-after').value = count_credit;
				$('sum_total_price').set('text',price(total_price_credit));
				//(function(){
				run_fce = true;
				//}).delay(110);
				}
			}
			run_fce = true;
			//if (window.getSize().x > 1000){
				$('credit_use_element').getElements('.pocet_change').removeEvents('touchstart');
				$('credit_use_element').getElements('.pocet_change').addEvent('touchstart',function(e){
					timer = press_mouse.periodical(150,this);
					
					
				});
				
				$('credit_use_element').getElements('.pocet_change').removeEvents('mousedown');
				$('credit_use_element').getElements('.pocet_change').addEvent('mousedown',function(e){
					timer = press_mouse.periodical(150,this);
					
				});
				
				$('credit_use_element').getElements('.pocet_change').removeEvents('mouseup');
				$('credit_use_element').getElements('.pocet_change').addEvent('mouseup',function(e){
					clearInterval(timer);
				});
				
				$('credit_use_element').getElements('.pocet_change').removeEvents('touchend');
				$('credit_use_element').getElements('.pocet_change').addEvent('touchend',function(e){
					clearInterval(timer);
				});
				
				
				$('credit_use_element').getElements('.pocet_change').removeEvents('click');
				$('credit_use_element').getElements('.pocet_change').addEvent('click',function(e){
					press_mouse(e);
				});
			/*
			} else {
				$('credit_use_element').getElements('.pocet_change').addEvent('click',function(e){
					press_mouse(e);
				});
				
			}
			*/
		}
	},
	
	// move to login element
	move_to_login: function(){
		if ($('username') && $('move_to_login')){
			$('move_to_login').addEvent('click',function(e){
				e.stop();
				var myFx = new Fx.Scroll(window).toElement('username');
				$('username').focus();
			});
		} else {
			if (window.debug) console.log('neni username nebo move_to_login');
		}
	},
	
	// change ks basket
	change_ks_basket: function(){
		$$('.change_ks').removeEvents('change');
		$$('.change_ks').addEvent('change',function(e){
			var parent_preload = e.target.getParent('.products_basket');
			el_preloader(parent_preload);
			//console.log(e.target.value);
		
			if (e.target.value < 1) e.target.value = 1;
			this.req_form = new Request.JSON({
				url:'/orders/basket_change_ks/'+e.target.get('data-id')+'/'+e.target.value,
				onError: this.req_error = (function(data){
					el_preloader(el.getParent('.products_basket'));
					
				}).bind(this),

				onComplete :(function(json){
					el_preloader(parent_preload);
			
					if (json.r == true){
						this.init_after_basket_change(json);
						//this.change_kosik_total_price(json.total);
					}
					if (json.r == false){
						FstError(json.m);
					}
				}).bind(this)
			});
			this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			this.req_form.send();
		}.bind(this));
	},
	
	// hide_payment_on_transport
	hide_payment_on_transport: function(){
		
		function hide_row(el,noclear){
			payments_list = JSON.decode(el.get('data-payment_ids'));
			if (payments_list.length > 0){
				if (!noclear){	
					$('basket_detail').getElements('.payment_line').each(function(item){
						if (item.getElement('.check_payment')){
							item.getElement('.check_payment').set('checked',false);
						} else {
							item.getParent('select').value = '';
						}
					});
				}
				$('basket_detail').getElements('.payment_line').addClass('none');
				payments_list.each(function(item){
					$('basket_detail').getElement('.payment_line_'+item).removeClass('none');
				});
			} else {
				$('basket_detail').getElements('.payment_line').removeClass('none');
				
			}
			
		}
		
		$$('.check_transport').addEvent('click',function(e){
			hide_row(e.target);
			
		}.bind(this));
		
		
		// add to session transport or payment
		$$('.add_transport').addEvent('click',function(e){
			this.add_transport_payment_price(e.target,e.target.get('data-type'),e.target.get('data-id'),e.target.get('data-price'));
				
			
		}.bind(this));
		
		// default hide after refresh
		$$('.check_transport').each(function(item){
			if (item.nodeName == 'OPTION'){
				if (item.getParent('select').value == item.get('data-id')){
					hide_row(item,true)
					
				}
				
			} else {
				
				if (item.checked){
					hide_row(item,true)
				}	
			}
			
		});
	},
	
	// add transport and payment price
	add_transport_payment_price: function(el,type,id,price){
			el_preloader(el.getParent('.field_select'));
			
			this.req_form = new Request.JSON({
				url:'/orders/set_order_transport/'+type+'/'+id+'/'+price,
				onError: this.req_error = (function(data){
					el_preloader(el.getParent('.field_select'));
				
				}).bind(this),

				onComplete :(function(json){
					el_preloader(el.getParent('.field_select'));
			
					if (json.r == true){
						this.change_kosik_total_price(json.total);
					}
					if (json.r == false){
						FstError(json.m);
					}
				}).bind(this)
			});
			this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			this.req_form.send();
	},
	
	// check free shiping
	check_free_shipping: function(){
		if ($('products_price')){
		var products_price = $('products_price').get('text').toInt();
		//console.log(products_price);
		$$('.check_transport').each(function(item){
			if (item.get('data-free-shipping') && item.get('data-free-shipping').toInt()>0){
				if (item.get('data-free-shipping').toInt() < products_price){
					item.set('data-price',0);
					if (item.getParent('.transport_line')){
						item.getParent('.transport_line').getElement('.transport_price').set('text',price(0));
					}
				} else {
					item.set('data-price',item.get('data-price_default'));
					if (item.getParent('.transport_line')){
						item.getParent('.transport_line').getElement('.transport_price').set('text',price(item.get('data-price_default')));
					}
				}
				//console.log(item);
			}
		});
		}
	},
	
	// detail basket delete events
	basket_detail_delete_events: function(){
		$('h1').addClass('none');
		$$('.delete_basket').removeEvents('click');
			$$('.delete_basket').addEvent('click',function(e){
				e.stop();
				//if (confirm(e.target.get('data-confirm'))){
					if (e.target.getParent('li')){
						var tr = e.target.getParent('li');
					} else {
						var tr = e.target.getParent('tr');
						
					}
					tr.fade(0);
					el_preloader($$('.products_basket'));
					
					this.req_form = new Request.JSON({
						url:e.target.href,
						onError: this.req_error = (function(data){
							
						}).bind(this),
						onComplete :(function(json){
							
							el_preloader($$('.products_basket'));
							if (json.r == true){
								tr.destroy();
								this.init_after_basket_change(json);
							}
							if (json.r == false){
								FstError(json.m);
							}
						}).bind(this)
					});
					this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
					this.req_form.send();
				//}
			}.bind(this));
			
	},
	
	
	
	// change kosik products elements CLASS .products_basket
	products_basket_element: function(products){
		//console.log(products);
		
		if ($('php_basket_products') && !products){
			basket_data = JSON.decode($(this.options.php_element_kosik).get('text'));
			if (basket_data){
				if (basket_data.products){
					products = basket_data.products;
				}
				if (basket_data.total){
					total = basket_data.total;
								
					// change kosik price
					this.change_kosik_total_price(total);
				}
			} else {
				// change kosik price
				total = {
					total:0,
					total_tax:0,
					ks:0,
				}
				this.change_kosik_total_price(total);
				
			}
		}
	
		
		
		
		data = [];
		if (products)
		Object.each(products,function(item,product_id){
			
			// format price
			item.price = price(item.price);
			if (item.price_total){ item.price_total = price(item.price_total);}
			if (item.price_vat){ item.price_vat = price(item.price_vat);}
			if (item.price_total_vat){ item.price_total_vat = price(item.price_total_vat);}
			item.product_id = product_id;
			data.push(item);
			
			//console.log(item);
			//console.log(product_id);
		});
		//console.log(data);
		$$('.products_basket').each(function(item,k){
			template_id = item.get('data-template_id');
			//console.log(products);
			if (!products || products == '' || products.length == 0){
				//console.log('neni $('+this.options.php_element_kosik+')');
				$$('.hide_template').addClass('none');
				if (window.template[template_id])
					window.template[template_id].clear();
				
				$$('.basket_empty').each(function(item){
					item.removeProperty('style');
				});
				
				return false;
			} else {
				$$('.hide_template').removeClass('none');
				$$('.basket_empty').each(function(item){
					item.setProperty('style','display:none');
				});
			}
			if (!window.template[template_id]){
				window.template[template_id] = {}
				window.template[template_id] = Tempo.prepare(item);
				
			}
			window.template[template_id].clear();
			window.template[template_id].when(TempoEvent.Types.RENDER_COMPLETE, function (event) {
				if ($(this.options.php_element_kosik)){
					$(this.options.php_element_kosik).empty();
				}
				// image
				Fst.init_lazy_load();
				this.basket_detail_delete_events();
				this.attributes_list();
				this.change_ks_basket();
			
		
			}.bind(this)).render(data);
			
		}.bind(this));
		
	},
	
	// attributes list in basket
	attributes_list: function(){
		
		if ($('attributes_list_json') && $('attributes_list_json').get('text') != ''){
			attributes_list = JSON.decode($('attributes_list_json').get('text'));
			//console.log(attributes_list);
			if ($('basket_detail') && $('basket_detail').getElement('.prod_attributes_list'))
			$('basket_detail').getElements('.prod_attributes_list').each(function(item){
				if (item.get('text') != '' && !item.hasClass('load')){
					ids = JSON.decode(item.get('text'));
					var text_list = new Element('div',{'class':'pridavky_text_list no_mobile'}).inject(item,'after');
					new Element('p',{'text':item.get('data-title')}).inject(text_list);
					ul = new Element('ul').inject(text_list);
					Object.each(ids,function(ks,id){
						if (attributes_list[id]){
							new Element('li',{'html':ks+' ks '+attributes_list[id].name+((attributes_list[id].gramaz)?'<span>'+attributes_list[id].gramaz+' g</span>':'')}).inject(ul);
						}
						//console.log(id);
					});
					item.addClass('load');
				}
				//console.log(ids);
			});
		
		}
	},
	
	// change kosik price elements CLASS .shop_basket_total_ks
	change_kosik_total_price: function(json){
		price_products_vat = json.products_vat;
		price_value = json.total;
		price_value_vat = json.total_vat;
		ks_value = json.count;
		// doprava free products price
		if ($('products_price')){
			$('products_price').set('text',price_products_vat);
		}
		
		if ($('spropitne')){
			var spropitne = $('spropitne').value.toInt();
			if (spropitne > 0){
				price_value_vat = price_value_vat+spropitne;
			}
		}
		
		// change total price basket
		$$('.shop_basket_total_price').each(function(item){
			if (item.nodeName == 'INPUT'){
				item.value = price_value;
			} else {
				item.set('text',price(price_value));
			}
		});
		
		$$('.shop_basket_total_price_vat').each(function(item){
			if (item.nodeName == 'INPUT'){
				item.value = price_value_vat;
			} else {
				item.set('text',price(price_value_vat));
			}
		});
		
		
		// check free shipping
		this.check_free_shipping();
			
		
		// change total ks value basket
		if (ks_value || ks_value == 0){
			$$('.shop_basket_total_ks').each(function(item){
				if (item.nodeName == 'INPUT'){
					item.value = ks_value;
				} else {
					item.set('text',ks_value+' x');
				}
			});
		} else {
			$$('.shop_basket_total_ks').each(function(item){
				if (item.nodeName == 'INPUT'){
					item.value = 0;
				} else {
					item.set('text',0+' x');
				}
			});
		}
	},
	
	// track GA ecommerce
	ga_ecommerce: function(){
		window.addEvent('domready', function(){ 
			if ($('ga_ecommerce')){	
				var data = JSON.decode($('ga_ecommerce').get('text'));
				//console.log(data);
				ga('require', 'ecommerce');
				ga('ecommerce:addTransaction', {
				  'id': data.order.order_id,                     // Transaction ID. Required.
				  'affiliation': data.domena,   // Affiliation or store name.
				  'revenue': data.order.price,               // Grand Total.
				  'shipping': data.order.price_transport,                  // Shipping.
				  'tax': '1.21'                     // Tax.
				});
				Object.each(data.products,function(prod){
					//console.log(prod);
					ga('ecommerce:addItem', {
					  'id': data.order.order_id,                     // Transaction ID. Required.
					  'name': prod.name,    // Product name. Required.
					  'sku': (prod.ean)?prod.ean:'',                 // SKU/code.
					 // 'category': 'Party Toys',         // Category or variation.
					  'price': prod.price,                 // Unit price.
					  'quantity': prod.quantity                   // Quantity.
					});
					
				});
				ga('ecommerce:send');
			
			}
		});
		
		
	},
	
	
	// show gopay
	show_gopay: function(json){
		$('OrderSaveButton').addClass('none');
		
		if (json.r == true){
			$('gopay-payment-button').set('action',json.gateway_url);
			$('gopay-payment-button').submit();
			//console.log('a');				
		} else {
			$('gopay_error').removeClass('none');
		}
	}
	
	
});
window.addEvent('domready',function(){
	window.FstShop = new FstShopJs();
	//console.log(this.Fst.test());
});
	


function init_order_map(){
	window.FstShop.load_user_map();
}

