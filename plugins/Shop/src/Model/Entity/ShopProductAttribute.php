<?php
namespace Shop\Model\Entity;
use Cake\ORM\Behavior\Translate\TranslateTrait;
use Cake\ORM\Entity;

class ShopProductAttribute extends Entity
{
    use TranslateTrait;
}
?>