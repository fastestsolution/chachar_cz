<?php

namespace Shop\Model\Table;

use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;


class CinajedePetansTable extends Table
{
     
    public function initialize(array $config)
    {
        parent::initialize($config);
		
		$this->table('fastest__rozvoz_foods');
		
    }
    
    public static function defaultConnectionName() {
        return 'cinajede_petan';
    }
      
}