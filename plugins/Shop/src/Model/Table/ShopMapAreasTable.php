<?php
namespace Shop\Model\Table;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Network\Session;

use Cake\Utility\Hash;
use Cake\I18n\Time;

class ShopMapAreasTable extends Table
{

  public function initialize(array $config)
  { 
    parent::initialize($config);
	$this->table('fastest__mapareas');
	//$this->addBehavior('Trash');
	$this->addBehavior('Timestamp');
  }
  
	public static function defaultConnectionName() {
		return 'stats_data';
	}
	
	
	public function mapAreaList(){
		//$ShopMapAreas = TableRegistry::get("ShopMapAreas");
		$mapper = function ($row, $key, $mapReduce) {
			//$row->coords = json_decode($row->coords);
			$mapReduce->emit($row);
		};
		$query = $this->find('list',['keyField' => 'id','valueField' => 'coords'])
		  //->contain(['ZakazkaConnects',])
		  ->where([])
		  ->select([
			'id',
			'coords',
		  ])
		  ->mapReduce($mapper)
		  ->cache(function ($query) {
			return 'mapAreaList-' . md5(serialize($query->clause('where')));
		});
		  
		$data_list =   $query->toArray();
		$map_area = [];
		foreach($data_list AS $system_id=>$d){
			$json = json_decode($d,true);
			$data_coords = [];
			if (isset($json) && !empty($json) && is_array($json)){
				//pr($json);
				foreach($json AS $k=>$j){
					if (isset($j['name'])){
						$data_coords = $j;
						$map_area[$system_id][$k] = $data_coords;
					}
				}
			}
		}
		//pr($data_list);
		return $map_area;  
	}	
	
	public function mapAreaListData(){
		//$ShopMapAreas = TableRegistry::get("ShopMapAreas");
		$mapper = function ($row, $key, $mapReduce) {
			//$row->coords = json_decode($row->coords);
			$mapReduce->emit($row);
		};
		$query = $this->find('list',['keyField' => 'id','valueField' => 'coords_data'])
		  //->contain(['ZakazkaConnects',])
		  ->where([])
		  ->select([
			'id',
			'coords_data',
		  ])
		  ->mapReduce($mapper)
		  ->cache(function ($query) {
			return 'mapAreaListDat-' . md5(serialize($query->clause('where')));
		});  
		$data_list =   $query->toArray();
		//pr($data_list);
		$map_area = [];
		foreach($data_list AS $system_id=>$d){
			$json = json_decode($d,true);
			//$json = $d;
			//pr($d);
			$data_coords = [];
			if (isset($json) && !empty($json)){
				//pr($json);
				//pr($json);
				$data_coords = [];
				foreach($json AS $k=>$j){
					//pr($j);
					//if (isset($j['name'])){
						foreach($j AS $jj){
							$data_coords[$k][] = $jj['lat'].' '.$jj['lng'];
							//pr($jj);
						}
						$data_coords[$k][] = $data_coords[$k][0];
						//$data_coords = $j;
						$map_area[$system_id][$k] = $data_coords[$k];
					//}
				}
				//	$map_area[$system_id] = $json;
					
			}
		}
		//pr($map_area);
		return $map_area;  
	}	
	
  

}
