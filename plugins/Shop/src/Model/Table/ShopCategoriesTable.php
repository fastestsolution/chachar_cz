<?php
namespace Shop\Model\Table;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Cache\Cache;
use Cake\Utility\Hash;
use Cake\I18n\Time;
use Cake\Core\Configure;

class ShopCategoriesTable extends Table
{

  public function initialize(array $config)
  {
	
	parent::initialize($config);
	//$this->table('shop_categories');  
    $this->addBehavior('Timestamp');
	//$this->addBehavior('Trash');
	 $this->addBehavior('Tree',[
            'parent' => 'parent_id',
            'left' => 'lft',
            'right' => 'rght'
        ]);
	if (Configure::read('language')){
		
		$this->addBehavior('Translate', [
			'fields' => ['name','alias','description'],
			'translationTable' => 'I18n'
		]);
	}
	
  }
  
	// detail kategorie z cat_id
	public function getCategoryDetail($cat_id){
		//$ShopCats = TableRegistry::get("ShopCategories");
		//$this->contrloadModel('ShopCategories');
		$conditions = ['ShopCategories.id'=>$cat_id];
		$con_hash = md5(json_encode($conditions));
		
				
				
			$query = $this->find()
				->where($conditions)
				->select([
					'id',
					'name',
					'alias',
					'description',
					'images',
				])
				//->cache(function ($query) {
					//return 'cat_detail_-' . md5(serialize($query->clause('where')).serialize($_SESSION['lang']));
				//});
			;
		$cat_data = $query->first();
		return $cat_data;  
	}	
	
	
	// gen path recursive
	function genPath($data,$parent=null){
			
		foreach($data AS $d){
			if (empty($d->spec_url)){
				$url = (($parent != null)?$this->path_list[$parent]:'/').$d->alias.'/';
				$url_google = (($parent != null)?$this->path_list_google[$parent]['url']:'/').(($parent == null)?$this->prefix:'').$d->alias.'/';
			} else {
				$url = $url_google = $d->spec_url;
			}
			$this->path_list[$d->id] = $url;
			$this->path_list_google[$d->id] = [
				'url'=>$url_google,
				'modified'=>$d->modified,
			];
			if (!empty($d->children)){
				$this->genPath($d->children,$d->id);
			}
		}
	}
	
	// get full path
	public function getFullPath($type=null,$prefix=null){
		//$ShopCats = TableRegistry::get("ShopCategories");
		
		$this->prefix = $prefix;
		$this->path_list = [];
		$this->path_list_google = [];
		
		if (($path_data = Cache::read('path_data_categories'.$_SESSION['lang'])) === false) {
			$query = $this->find('threaded')
				->where([])
				->select([
					'id',
					'name',
					'alias',
					'level',
					'parent_id',
					'lft',
					'rght',
					'spec_url',
				])
				->cache(function ($query) {
					return 'path_data_categories-' . md5(serialize($query->clause('where')).serialize($_SESSION['lang']));
				});
				
			;
			$path_data = $query->toArray();
			Cache::write('path_data_categories'.$_SESSION['lang'], $path_data);	
		}
			
		//pr($path_data);
		if ($path_data){
			$this->genPath($path_data);
		}
		//pr($data);
		//pr($this->path_list_google);
		if ($type == 'google'){
			return $this->path_list_google;  
			
		} else {
			return $this->path_list;  
			
		}
	}	
 
  

  

}
