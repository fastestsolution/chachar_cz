<?php
namespace Shop\Model\Table;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Network\Session;

use Cake\Utility\Hash;
use Cake\I18n\Time;

class ShopProvozsTable extends Table
{

  public function initialize(array $config)
  {
	  
    parent::initialize($config);
	$this->addBehavior('Trash');
	$this->addBehavior('Timestamp');
  }
	
	public function pokladnaList(){
		$ShopProvozs = TableRegistry::get("ShopProvozs");
		$query = $ShopProvozs->find()
			  //->contain(['ZakazkaConnects',])
			  ->where([])
			  ->select([
				'id',
				'provoz_id',
				'pokladna_id',
				'name',
				'system_id',
			  ])
			  ->cache(function ($query) {
				return 'pokladnaListAll-' . md5(serialize($query->clause('where')));
			});
		  
		$pokladna_list_load = $query->toArray();
		$pokladna_list = [];
		$pokladna_list['system_id'][1000] = 1;
		$pokladna_list['list'][1000] = 1000;
		$pokladna_list['provoz_list'][1000] = 1000;
		foreach($pokladna_list_load AS $p){
			$pokladna_list['provoz_list'][$p->pokladna_id] = $p->provoz_id; 
			$pokladna_list['list'][$p->pokladna_id] = $p->id; 
			$pokladna_list['system_id'][$p->id] = $p->system_id; 
		}
		//pr($pokladna_list);
		return($pokladna_list);
	}
	public function pokladnaListAll(){
		$ShopProvozs = TableRegistry::get("ShopProvozs");
		$query = $ShopProvozs->find()
			  //->contain(['ZakazkaConnects',])
			  ->where(['pokladna_id IS NOT'=>null])
			  ->select([
				'id',
				'provoz_id',
				'pokladna_id',
				'gopay_id',
				'name',
				'system_id',
			  ])
			  ->order('system_id ASC')
			  ->cache(function ($query) {
				return 'pokladnaListAll2-' . md5(serialize($query->clause('where')));
			  })
			  ;
		  
		$pokladna_list_load = $query->toArray();
		$pokladna_list = [];
		$pokladna_list['data'][1000] = ['name'=>'Testovaci'];
		$pokladna_list['provoz_pokladna_id'][1000] = 1000;
		foreach($pokladna_list_load AS $p){
			$pokladna_list['data'][$p->id] = $p; 
			$pokladna_list['provoz_pokladna_id'][$p->pokladna_id] = $p->id; 
		}
		//pr($pokladna_list);
		return($pokladna_list);
	}
	
	public function provozListAll($no_active=null){
		if ($no_active == null){
			$conditions = ['pokladna_id IS NOT'=>null];
		} else {
			$conditions = ['pokladna_id IS NOT'=>null,'online'=>1];
		}
		//pr($no_active);
		$mapper = function ($row, $key, $mapReduce) {
			if (!empty($row->doba)){
				$week_day = date('w') - 1;
				$doba = explode("\n",$row->doba);
				
				
				$doba_list = [];
				foreach($doba AS $k=>$d){
					$d = strtr($d,[' '=>'']);
					$time = explode('-',$d);
					//pr($k);
					if (isset($time[1])){
					
					$doba_list[$k] = [
						'from'=>$time[0],
						'to'=>$time[1],
					];
					}
				
					//pr($time);
				}
				//pr($doba_list);
				if (isset($doba_list[$week_day])){
				
				$today_open = $doba_list[$week_day];
				} else {
					$today_open = '04:00 - 22:00';
				}
				//pr($doba_list);
				//pr($doba);
				$row->doba = $doba;
				$row->today_open = $today_open;
			}
			
			//pr($row->doba);
			$mapReduce->emit($row);
		};
		$query = $this->find()
		  //->contain(['ZakazkaConnects',])
		  ->where($conditions)
		  ->select([
			'id',
			'provoz_id',
			'pokladna_id',
			'name',
			'email',
			'telefon',
			'name2',
			'doba',
			'online',
			'provoz_email',
			'order_email',
			'gopay_id',
			'gopay_client_id',
			'gopay_secure',
			'system_id',
		  ])
		  ->order('poradi ASC')
		  ->mapReduce($mapper)
		  ->cache(function ($query) {
			return 'provozListAll3-' . md5(serialize($query->clause('where')));
		});
		  
		$data_list_load = $query->toArray();
		//pr($data_list_load);
		$data_list = [];
		foreach($data_list_load AS $d){
			$data_list[$d->id] = $d;
		}
		//pr($data_list);
		$provoz_list = [
			'data'=>$data_list,
			'provoz_list'=>[],
			'order_email'=>[],
			'provoz_email'=>[],
			'provoz_list_online'=>[],
			'provoz_list_online_api'=>[],
			'provoz_api_ids'=>[],
			'provoz_list_id'=>[],
			'provoz_pokladna_id'=>[],
			'doba_list'=>[],
		];
		
		$provoz_list['provoz_list'][''] = __d('shop','Vyberte provozovnu');
		$provoz_list['provoz_list_id'][''] = __d('shop','Vyberte provozovnu');
		
		foreach($data_list AS $d){   
			//pr($d->provoz_id);
			$provoz_list['doba_list'][$d->id] = $d->doba;
			$provoz_list['provoz_list'][$d->id] = $d->name;
			$provoz_list['order_email'][$d->id] = $d->order_email;
			$provoz_list['provoz_email'][$d->id] = $d->email; //$d->provoz_email;
			$provoz_list['provoz_api_ids'][$d->provoz_id] = $d->id;
			if ($d->online == 1){
			
				$provoz_list['provoz_list_online'][$d->id] = $d->name;
				$provoz_list['provoz_list_online_api'][$d->provoz_id] = $d->name;
			}
			$provoz_list['provoz_list_id'][$d->provoz_id] = $d->name;
			$provoz_list['provoz_pokladna_id'][$d->pokladna_id] = $d->id;
			
		}
		
		$provoz_list['provoz_api_ids'][1000] = 1000;
		if (in_array($_SERVER['REMOTE_ADDR'],['89.103.18.65','94.242.105.214','94.112.134.23'])){
			//$provoz_list['data'][1000] = ['name'=>'Testovací Fastest','telefon'=>'123456','order_email'=>'test.fastest@seznam.cz'];
			$provoz_list['data'][1000] = ['name'=>'Testovací Fastest','telefon'=>'123456','order_email'=>'test@fastest.cz'];
			$provoz_list['provoz_list'][1000] = 'Testovací Fastest';
			//$provoz_list['order_email'][1000] = 'test.fastest@seznam.cz';
			$provoz_list['order_email'][1000] = 'test@fastest.cz';
			$provoz_list['provoz_email'][1000] = 'test@fastest.cz';
			$provoz_list['provoz_list_online'][1000] = 'Testovací Fastest';
			$provoz_list['provoz_list_id'][1000] = 'Testovací Fastest';
			$provoz_list['provoz_pokladna_id'][1000] = 1000;
		}
		//pr($provoz_list);
		return $provoz_list;  
	}	
	
	public function beforeFind($event, $query, $options, $primary){
		$session = new Session();
		$system_id = $session->read('system_id');
		//pr($system_id);
		$query->where(['ShopProvozs.system_id' => $system_id]);
		//pr($event->data);
	}
	
  

}
