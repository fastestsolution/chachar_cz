<?php
namespace Shop\Model\Table;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Utility\Hash;
use Cake\I18n\Time;
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Network\Session;

class ShopProductVariantsTable extends Table
{

  public function initialize(array $config)
  {
	  
    parent::initialize($config);
	$this->addBehavior('Timestamp');
	$this->addBehavior('Trash');
	if (Configure::read('language')){
		/*
		$this->addBehavior('Translate', [
			'fields' => ['name'],
			'translationTable' => 'ShopProductVariantsI18n'
		]);
		*/
	}
	
  }
  
  
	public function beforeFind($event, $query, $options, $primary){
		$url = explode('/',$_SERVER['SCRIPT_URL']);
		//pr($url);
		if ($url[1] != 'admin'){
		
		$session = new Session();
		$system_id = $session->read('system_id');
		$query->where(['ShopProductVariants.system_id' => $system_id]);
		}
	}
	
  
  

}
