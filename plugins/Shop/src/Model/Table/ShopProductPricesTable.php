<?php
namespace Shop\Model\Table;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Utility\Hash;
use Cake\I18n\Time;

use Cake\Network\Session;
class ShopProductPricesTable extends Table
{

	public function initialize(array $config){
	  
		parent::initialize($config);
		
		$this->addBehavior('Timestamp');
		$this->addBehavior('Trash');
	}
	
	
	public function beforeFind($event, $query, $options, $primary){
		$session = new Session();
		$system_id = $session->read('system_id');
		$query->where(['ShopProductPrices.system_id' => $system_id]);
		
	}
  
  
  
  

  

}
