<?php
namespace Shop\Model\Table;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Utility\Hash;
use Cake\I18n\Time;

class ShopOrderItemsTable extends Table
{

  public function initialize(array $config)
  {
	  
    parent::initialize($config);
	$this->addBehavior('Timestamp');
	$this->addBehavior('Trash');
  }
  
  /*
	public function validationDefault(Validator $validator)
	{
		//$validator
		/*
		->notEmpty('email',__("Musíte zadat email"))
		->add('email', 'validFormat', [
			'rule' => 'email',
			'message' => __('Email není ve správném formátu')
		])
		->notEmpty('jmeno',__("Musíte zadat Vaše jméno"))
		->notEmpty('text_tmp',__("Musíte zadat Vaši zprávu"))
		
		->requirePresence('shop_transport_id', 'create',  __("Musíte zvolit Dopravu"))
		->requirePresence('shop_payment_id', 'create',  __("Musíte zvolit Platbu"))
		->notEmpty('shop_transport_id',__("Musíte zvolit Dopravu"))
		->notEmpty('shop_payment_id',__("Musíte zvolit Platbu"))
		
		;
		//return $validator;
	}
  	*/
	

}
