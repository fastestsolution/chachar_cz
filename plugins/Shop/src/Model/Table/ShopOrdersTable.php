<?php
namespace Shop\Model\Table;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Utility\Hash;
use Cake\I18n\Time;

class ShopOrdersTable extends Table
{

  public function initialize(array $config)
  {
	  
    parent::initialize($config);
	$this->hasMany("Shop.ShopOrderItems",['foreignKey' => 'shop_order_id','bindingKey' => 'id']);
	$this->belongsTo("UserAddresses",['foreignKey'=>'user_address_id']);
	$this->belongsTo("Users",['foreignKey'=>'shop_client_id']);
	$this->addBehavior('Trash');
	
	$this->addBehavior('Timestamp');
  }
  
	public function beforeSave($event, $entity, $options) {
		if (isset($_SESSION['dev_project'])){
			$entity->shop_provoz_id = 1000;
		}
		//print_r($_SESSION['dev_project']);
		//print_r($event);
		//print_r($entity);
		//print_r($options);
		//die();
	}
	public function getOrder($id){
		
		$order = $this->find()
			->contain(['Users','Users.UserAddresses','ShopOrderItems'])
			->select([
				'shop_payment.name',
			])
			->join([
				'payment' => [
					'table' => 'shop_payments',
					'alias' => 'shop_payment',
					'type' => 'LEFT',
					'conditions' => 'shop_payment.id = ShopOrders.shop_payment_id',
				]
			])
			->autoFields(true)
			->where(['ShopOrders.id'=>$id])
			->map(function($row){
				if (isset($row->user->user_addresses))
				foreach($row->user->user_addresses AS $k=>$adr){
					//pr($row->user_address_id);
					//pr($adr);
					if ($adr->id == $row->user_address_id){
						$row->address1 = $adr;
					}
					/*
					if ($adr->shop_address_type_id == 1){
						$row->address1 = $adr;
					} else {
						$row->address2 = $adr;
					}
					*/
					
					unset($row->user->user_addresses[$k]);
					
				}
				return $row;
			})
			->first();
		;
		return $order;
			
	}
	
	public function validationDefault(Validator $validator)
	{
		$validator
		/*
		->notEmpty('email',__("Musíte zadat email"))
		->add('email', 'validFormat', [
			'rule' => 'email',
			'message' => __('Email není ve správném formátu')
		])
		->notEmpty('jmeno',__("Musíte zadat Vaše jméno"))
		->notEmpty('text_tmp',__("Musíte zadat Vaši zprávu"))
		*/
		//->requirePresence('shop_transport_id', 'create',  __("Musíte zvolit Dopravu"))
		->requirePresence('shop_payment_id', 'create',  __("Musíte zvolit Platbu"))
		//->notEmpty('shop_transport_id',__("Musíte zvolit Dopravu"))
		->notEmpty('shop_payment_id',__("Musíte vybrat Typ platby"))
		->notEmpty('shop_provoz_id',__("Musíte zvolit provozovnu"))
		
		->add('vop', 'custom', [
			'rule' => [$this, 'AcceptTerm'],
            'message' => 'Musíte souhlasit se Všeobecnými obchodními podmínkami'
		]);
		
		;
		return $validator;
	}
	
	public function AcceptTerm($value,$context){
    	if($context['data']['vop']==1)
			return true;
		else
			return false;
	}
	
	
  

}
