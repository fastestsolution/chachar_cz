<?php
namespace Shop\Model\Table;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Utility\Hash;
use Cake\I18n\Time;

class ShopAvailabilitiesTable extends Table
{

	public function initialize(array $config){
	  
		parent::initialize($config);
		
		$this->addBehavior('Timestamp');
		$this->addBehavior('Trash');
	}
  
  
	public function availabilitiesList(){
		$ShopAvailabilities = TableRegistry::get("ShopAvailabilities");
		
		$query = $ShopAvailabilities->find('list',[
				'keyField' => 'id',
				'valueField' => 'name'
			])
		  //->contain(['ZakazkaConnects',])
		  ->where([])
		  ->cache(function ($query) {
				return 'availabilitiesList-' . md5(serialize($query->clause('where')));
			});
		$data_list = $query->toArray();  
		return $data_list;  
	}	
  
  

  

}
?>
