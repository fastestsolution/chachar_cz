<?php
namespace Shop\Model\Table;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Utility\Hash;
use Cake\I18n\Time;
use Cake\Cache\Cache;
use Cake\Core\Configure;
use Cake\Network\Session;

class ShopProductsTable extends Table
{

  public function initialize(array $config)
  {
	  
    parent::initialize($config);
	$this->hasMany('ShopProductCategories');
	$this->hasMany('ShopProductPrices');
	$this->hasMany('Shop.ShopProductVariants');
	$this->hasMany('Shop.ShopProductConAttributes');
	$this->addBehavior('Timestamp');
	$this->addBehavior('Trash');
	
	if (Configure::read('language')){
		$this->addBehavior('Translate', [
			'fields' => ['name','alias','description'],
			'translationTable' => 'ShopProductsI18n'
		]);
	}
	
  }
  
	public function beforeFind($event, $query, $options, $primary){
		/*
		$session = new Session();
		$system_id = $session->read('system_id');
		$query->where(['ShopProducts.system_id' => $system_id]);
		*/
	}
	
	
	// generate list from threaded
	public function findCatsId($path,$data){
		$ids = [];
		foreach($path AS $k=>$p){
			$ids[] = array_search($p,$data);
			
		}
		if (empty($ids[0])){
			$ids = '';
		}
		return $ids;
		
	}
  
	
	// generate list from threaded
	public function shopMenuItems($path,$data,$group_id,$category_seo){
		
		$this->shop_menu_items_group_id = [];
		$this->shop_menu_items_group_list = [];
		$this->shop_menu_items_list = [];
		$this->shop_menu_items_list_name = [];
		$this->generateList($data,$group_id);
		//$this->controller->set('cat_group_id',$group_id);
		//pr($group_id);
		//pr($this->shop_menu_items_list);
		//$ids_cats = $this->findCatsId($path,$this->shop_menu_items_list);
		$ids_cats = [];
		foreach($this->shop_menu_items_list AS $k=>$m){
			$ids_cats[] = $k;
		}
		//pr($this->shop_menu_items_list);die();
		//pr($this->shop_menu_items_list);
		//$ids_cats = [1];
		
		if (!empty($ids_cats)){
		//pr($ids_cats);
		
		rsort($ids_cats);
		$breadcrumb = [];
		foreach($ids_cats AS $i){
			$breadcrumb[$i] = $this->shop_menu_items_list_name[$i];
		}
		//pr($this->shop_menu_items_list_name);
		//pr($_SESSION['system_id_data']);
		$breadcrumb = $category_seo[$group_id].'<span id="h1_change">'.$_SESSION['system_id_data']['seo_category_city'].'</span>';
		
		//pr($breadcrumb);
		$ids_prod = $this->getConnectionList($ids_cats);
		
		
		$result = [
			'shop_menu_items_group_id'=>$this->shop_menu_items_group_id,
			'shop_menu_items_group_list'=>$this->shop_menu_items_group_list,
			'menu_items_list_name'=>$this->shop_menu_items_list_name,
			'menu_list'=>$this->shop_menu_items_list,
			'cat_group_id'=>$group_id,
			'ids'=>$ids_prod,
			'breadcrumb'=>$breadcrumb,
		]; 
		
		} else {
			$result = '';
		}
		return $result;
	}
	
	
	
	
	public function generateList($data,$group_id){
			//pr($group_id);
				
			//pr($data);die();
			foreach($data AS $d){
				if ($d->group_id == $group_id){
				//pr($d);
				$this->shop_menu_items_group_id[$d->id] = $d->group_id;
				$this->shop_menu_items_group_list[$d->group_id] = [];
				$this->shop_menu_items_list[$d->id] = $d->alias;
				$this->shop_menu_items_list_name[$d->id] = $d->name;
				if (isset($d->children)){
					$this->generateList($d->children,$group_id);
				}
				
				}
			}
			
			
			
			//die('aaa');
	}
	
	// ziskani product id z connection dle cat_id
	public function getConnectionList($cat_id){
		
		//$ShopProductCategories = TableRegistry::get("ShopProductCategories");
		$ShopProductCategories = TableRegistry::get("ShopProducts");
		
		//pr($cat_id);
		$conditions = ['shop_category_id IN'=>$cat_id];
		
			$query = $ShopProductCategories->find('list',[
				'keyField' => 'id',
				'valueField' => 'id'
			]);
			$query->where($conditions);
			$query->cache(function ($query) {
				return 'products_categories_-' . md5(serialize($query->clause('where')).serialize($_SESSION['lang']));
			});
			$data_load = $query->toArray();
		
		$data = [];
		foreach($data_load AS $k=>$d){
			$data[] = $k;
		}
		//$data = array_flip($data);
		//pr($data);
		return $data;  
	}	
	
	public function getEan($id){
		$ShopProducts = TableRegistry::get("ShopProducts");
		$find = $ShopProducts->find()
			->where(['id'=>$id])
			->hydrate(false)
			->select(['ean'])
			->first();
			//pr($find);
		return $find;
	}
  
	
	// get full path
	public function getFullPath(){
		$ShopProducts = TableRegistry::get("ShopProducts");
		$this->path_list_google = [];
		
			$query = $ShopProducts->find()
				->where([])
				->select([
					'id',
					'name',
					'alias',
					'modified',
				])
				->order('id DESC')
				->cache(function ($query) {
					return 'path_data_products-' . md5(serialize($query->clause('where')).serialize($query->clause('order').serialize($_SESSION['lang'])));
				});
			
			$path_data = $query->toArray();
			if ($path_data)
			foreach($path_data AS $p){
				$this->path_list_google[] = [
					'url'=>'/'.DETAIL_LINK.'/'.$p->alias.'/'.$p->id.'/',
					'modified'=>$p->modified,
				];
			}
		//pr($this->path_list_google);
		
		return $this->path_list_google;  
	}	
  
  

}
