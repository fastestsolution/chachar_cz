<?php
namespace Shop\Model\Table;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;
use Cake\Cache\Cache;
use Cake\Utility\Hash;
use Cake\I18n\Time;
use Cake\Core\Configure;

class ShopProductAttributesTable extends Table
{

  public function initialize(array $config)
  {
	
	parent::initialize($config);
	//$this->table('shop_categories');  
    $this->addBehavior('Timestamp');
	//$this->addBehavior('Trash');
	if (Configure::read('language')){
		
		$this->addBehavior('Translate', [
			'fields' => ['name'],
			'translationTable' => 'I18n'
		]);
	}
	
  }
  
	public function attributesList(){
		
		$query = $this->find()
		  //->contain(['ZakazkaConnects',])
		  ->where([])
		  ->select([
			'id',
			'name',
			'price_vat',
			'gramaz',
			'code',
		  ])
		  ->order('name ASC')
		  ->cache(function ($query) {
			return 'attributesList-' . md5(serialize($query->clause('where')));
			})
		;
		  
		$data_list_load =   $query->toArray();
		$data_list = [];
		foreach($data_list_load AS $d){
			$data_list[$d->id] = $d;
		}
		return $data_list;  
	}	
	

  

}
