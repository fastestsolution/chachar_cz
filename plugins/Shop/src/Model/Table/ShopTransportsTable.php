<?php
namespace Shop\Model\Table;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Utility\Hash;
use Cake\I18n\Time;

class ShopTransportsTable extends Table
{

	public function initialize(array $config){
		
		$this->hasMany("Shop.ShopTransportPayments",['foreignKey' => 'shop_transport_id','bindingKey' => 'id']);
		parent::initialize($config);
		
		$this->addBehavior('Timestamp');
		$this->addBehavior('Trash');
	}
  
  
	public function transportList(){
		$ShopTransports = TableRegistry::get("Shop.ShopTransports");
		
		$data_list = $ShopTransports->find()
		  
			->contain(['ShopTransportPayments'=> function ($q) {
                 return $q
                ->select([
                    'ShopTransportPayments.id',
                    'ShopTransportPayments.shop_transport_id',
                    'ShopTransportPayments.shop_payment_id',
                 ])
				;
            }])
			->where([])
			->select([
				'id',
				'name',
				'price',
				'price_vat',
				'tax_id',
				'free_shipping_order_price',
				
			])
			->map(function($row) {
				$row_payment_ids = [];
				if ($row->shop_transport_payments){
					foreach($row->shop_transport_payments AS $r){
						$row_payment_ids[] = $r['shop_payment_id'];
					}
					
				}
				$row->payment_ids = $row_payment_ids;
				return $row;
			})
			->toArray();
		return $data_list;  
		
	}	
  
  

  

}
