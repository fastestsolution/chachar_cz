<?php
namespace Shop\Model\Table;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Utility\Hash;
use Cake\I18n\Time;

class ShopPaymentsTable extends Table
{

	public function initialize(array $config){
	  
		parent::initialize($config);
		
		$this->addBehavior('Timestamp');
		$this->addBehavior('Trash');
	}
  
  
	public function paymentList(){
		$ShopPayments = TableRegistry::get("ShopPayments");
		
		$query = $ShopPayments->find()
		  //->contain(['ZakazkaConnects',])
		  ->where([])
		  ->select([
			'id',
			'name',
			'price',
			'price_vat',
			'tax_id',
		  ])
		  ->cache(function ($query) {
			return 'paymentList-' . md5(serialize($query->clause('where')));
		});
		  
		$data_list =   $query->toArray();
		return $data_list;  
	}	
  
  

  

}
