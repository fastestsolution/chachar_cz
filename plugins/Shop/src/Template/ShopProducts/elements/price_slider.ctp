<?php //pr($price_range); ?>
<?php if (isset($price_range) && $price_range['min'] != $price_range['max']){ ?>

<?php $range_value = '['.$price_range['min'].','.$price_range['max'].']';?>
<?php //pr($range_value); ?>
<div id="price_slider_element">
	<span id="price_slider_from"><span><?php echo $this->Fastest->price($price_range['min']);?></span></span> 
	<input id="price_slider" type="text" class="span2" value="" 
		data-slider-min="<?php echo $price_range['min']?>" 
		data-slider-max="<?php echo $price_range['max']?>" 
		data-slider-step="100" 
		data-slider-value="<?php echo $range_value ?>"
	/> 
	<span id="price_slider_to"><span><?php echo $this->Fastest->price($price_range['max']);?></span></span> 
	<?php echo $this->Form->input('price_values',array('label'=>false,'class'=>'fst_h','data-fparams'=>'price_values','type'=>'hidden')); ?>
</div>


<?php } ?>