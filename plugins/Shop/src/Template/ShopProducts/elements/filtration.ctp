<?php  
/* 
pokud chete upravit parametry filtrace tak nastavit CLASS fst_h a DATA-FPARAMS na get parametr
*/
?>
<div id="filtration">
	<div class="row">
		<div class="col-md-3">
			<?php echo $this->Form->input('name', ['placeholder'=>__('jmeno'),'class'=>'form-control fst_h','label'=>false,'data-fparams'=>'name']);?>
		</div>
		<div class="col-md-3">
			<?php echo $this->Form->input('shop_manufacturer_id', ['class'=>'form-control fst_h','label'=>false,'data-fparams'=>'shop_manufacturer_id','options'=>[0=>'Vyrobce',1=>'test1',2=>'test2'],'empty'=>false]);?>
		</div>
		<div class="col-md-3">
			<?php echo $this->Form->input('order', ['class'=>'form-control fst_h','label'=>false,'data-fparams'=>'order','options'=>$products_order,'empty'=>false]);?>
		</div>
		<div class="col-md-3">
			<?php echo $this->Form->input('result_type', ['class'=>'form-control fst_h','label'=>false,'data-fparams'=>'result_type','options'=>$products_result_type,'empty'=>false,'value'=>$result_type]);?>
		</div>
	</div>
</div>