<?php //pr($item); ?>
<?php //pr($i); ?>
<table class="table <?php echo (isset($variants)?'variants variant'.$item->id.' '.(($i>0)?'none':''):''); ?>">
	<tr><th><?php echo __d('shop', 'Cena');?></th><td><span class="price_vat"><?php echo $this->Fastest->price($item->price_vat);?></span></td></tr>
	<tr><th><?php echo __d('shop', 'Výrobce');?></th><td><?php echo (isset($manufacturers_list[$item->shop_manufacturer_id])?$manufacturers_list[$item->shop_manufacturer_id]:__d('shop', 'Nevybráno'));?></td></tr>
	<tr><th><?php echo __d('shop', 'Dostupnost');?></th><td><?php echo (isset($availability_list[$item->shop_availability_id])?$availability_list[$item->shop_availability_id]:__('Nevybráno'));?></td></tr>
	<tr><th><?php echo __d('shop', 'Počet ks skladem');?></th><td><?php echo $item->stock_units;?></td></tr>
</table>
<?php
// varianty fotografie
//pr($item->images);
if (isset($variants) && count($item->images)>0){ 
	$img_set = [
		'imgs'=>$item->images,
		'width'=>600,
		'height'=>400,
		'bg'=>'#fff',		
		'path'=>'/uploaded/product_variants/',
		'only_url'=>true,
	];
	echo '<div class="none" id="variant_foto_'.$item->id.'" data-src="'.$this->Fastest->img($img_set).'"></div>';
}
?>