<?php $product_url = '/'.DETAIL_LINK.'/'.$i->alias.'/'.$i->id.'/'; ?>
<tr>
	<td>
		<div class="image">
		<?php 
		
		$img_set = array(
				'imgs'=>$i->images,
				'first'=>true,
				'link'=>$product_url,
				'class'=>'',
				'title'=>$i->name,
				'width'=>50,
				'height'=>40,
				'path'=>'/uploaded/products/',
				
			
		);
		//pr($img_set);
		echo $this->Fastest->img($img_set);
		//echo $this->Html->image('http://placekitten.com/150/150', array('class'=>'pull-left img-responsive thumb margin10 img-thumbnail','alt'=>'')) 
		//echo $this->Html->image('http://placekitten.com/150/150', array('class'=>'pull-left img-responsive thumb margin10 img-thumbnail','alt'=>'')) 
		?>
		</div>
	</td>
	<td><?php echo $this->Html->link($i->name,$product_url,['title'=>$i->name]);?></td>
	<td><?php echo $this->Fastest->price($i->price_vat);?></td>
	<td class="text_right">
	<?php
		$opt = [
		'id'=>$i->id,
		'price'=>$i->price_vat,
		'ks'=>1,
		'hidden_ks'=>false,
		'btn_class'=>'btn-md',
		'variants_list'=>(isset($i->variants_list)?$i->variants_list:''),
		'variants_select_list_item'=>(isset($i->variants_select_list)?$i->variants_select_list:''),
	];
	echo $this->Shop->addBasketElement($opt); 
	?>
	</td>
</tr>