<div class="col-md-4 col-lg-3 col-sm-6 product_item <?php echo 'item'.$i->code; ?> <?php echo ((!isset($addStrips) && $i->code == 211)?'nonea':'') ?>">
	<?php if ($i->code == 211){ ?>
	<div class="fullAdd varianta_list_select stripsMenuAdd"></div>
	<?php } ?>
	<div class="item shop_category_id_<?php echo $i->shop_category_id;?> <?php echo 'item'.$i->code?>" id="item<?php echo $i->id?>" data-code="<?php echo $i->code?>" data-id="<?php echo $i->id?>">
	<?php if ($i->id == 395){ 
		echo '<a href="#" id="donuty" name="donuty"></a>';
	}?>
	<?php $product_url = DETAIL_LINK.'/'.$i->alias.'/'.$i->id.'/'; ?>
	<div class="done <?php /*echo 'icon-arrow'; */?>">
		<span class="ico"><?php echo __('Přidáno do košíku');?></span>
	<?php 
		echo '<div class="continue_kosik">';
			echo $this->Html->link(__d('shop','Pokračovat ve výběru'),'#',['title'=>__d('shop','Pokračovat ve výběru'),'class'=>'btn btn-primary red continue_kosik_but']).'<br />';
			echo $this->Html->link(__d('shop','Přejít do košíku'),'/'.KOSIK_LINK.'/',['title'=>__d('shop','Přejít do košíku'),'class'=>'btn btn-primary white basket basket_link']);
		echo '</div>';	
	?>
	</div>
	<div class="preloader"></div>
	<?php  
	//pr($i);
		if (in_array($i->id,[332,333,334])){
			$spec_size = [
				'width'=>1000,
				'height'=>1000,
			];
		}
		// cola
		//if (in_array($i->id,[325,326,327,328,336,337,338,339,340,83])){
			//$spec_class="cola";
		//}
		
		// burger
		if (in_array($i->id,[324,384,322,385,383,323,358,359,])){
			$spec_class = 'burger';
			$spec_size = [
				'width'=>600,
				'height'=>300,
			];
		}
		// specle
		if (in_array($i->id,[167,168,169,303,304])){
			$spec_class = 'specle';
			$spec_size = [
				'width'=>600,
				'height'=>300,
			];
		}
		// donut
		if (in_array($i->id,[395,396])){
			$spec_class = 'donuty';
			$spec_size = [
				'width'=>500,
				'height'=>300,
			];
		}
		// napoje
		if (in_array($i->id,[93,94,106,331,116,308,174,351,352])){
			$spec_class = 'napoje';
			$spec_size = [
				'width'=>500,
				'height'=>300,
			];
		}
		// salat
		if (in_array($i->id,[393])){
			$spec_class = 'salat';
		}
		// stripsy
		if (in_array($i->id,[333,332,334])){
			$spec_class = 'stripsy';
		}
		// stripsy 2
		if (in_array($i->id,[311,392,335])){
			$spec_class = 'stripsy2';
		}
		// salat2
		if (in_array($i->id,[310])){
			$spec_class = 'salat2';
		}
		
		// pizza
		if (in_array($i->shop_category_id,[1])){
			$spec_size = [
				'width'=>500,
				'height'=>300,
			];
		
		}
		
		$spec_class = $i->el_class;
		if ($i->is_pizza == 1){
		//if (in_array($i->id,[183,56,401])){
			$new_class = 'new_pizza';
		} else {
			$new_class = '';
		
		}
	?>
	
	<?php 
	if ($i->is_pizza == 1){
		unset($spec_size);
	}
	//pr($spec_size); ?>
	<?php //pr($i->id); ?>
	
	<div class="image <?php echo $new_class.' '.$i->el_class.'a '.(empty($i->images)?'noimg':'').'  imageGroup'.$i->shop_category_id.' '.(isset($spec_class)?$spec_class:'').' '.(isset($spec_size)?'spec_size':'').' '.(($i->group_id == 1)?'pizza':'').(($_SERVER['REMOTE_ADDR'] == '89.103.18.65')?' new_item':' new_item');  ?>">
		<?php
		//pr($i->id);
		// pr($i->images);
		$img_set = array(
				'imgs'=>$i->images,
				'first'=>true,
				//'link'=>$product_url,
				'class'=>'',
				'title'=>$i->name,
				'width'=>(isset($spec_size)?$spec_size['width']:600),
				'height'=>(isset($spec_size)?$spec_size['height']:280),
				'path'=>'/uploaded/products/'
				
			
		);
		if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65a'){
		$img_set['width'] = (isset($spec_size)?$spec_size['width']:400);
		$img_set['height'] = (isset($spec_size)?$spec_size['height']:200);
		}
		
		//pr($img_set);
		echo $this->Fastest->img($img_set);
		//echo $this->Html->image('http://placekitten.com/150/150', array('class'=>'pull-left img-responsive thumb margin10 img-thumbnail','alt'=>'')) 
		//echo $this->Html->image('http://placekitten.com/150/150', array('class'=>'pull-left img-responsive thumb margin10 img-thumbnail','alt'=>'')) 
		
		?>
	</div>
	<?php //pr($i); ?>
	<h2 data-title="<?php echo $i->name; ?>" class="title <?php echo (strlen($i->name)>55?'small':'');?>"><?php echo (($i->is_pizza == 1)?'<span class="seo_txt">'.__d('shop',strip_tags($title).' č.').' </span>':'').(!empty($i->number)?'<span class="num">'.$i->number.'. </span>':'').$i->name; //pr($i->id);?></h2>
	<?php //pr($i->is_pizza); ?>
	<?php 
	//if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
		//pr($i->id);
		//pr($i->hot);
		if (isset($i->hot) && !empty($i->hot) && $i->hot > 0){
			echo '<div class="relative palivost">';
			if($i->hot == 1){
				echo '<div class="paprika pap1"><span>&nbsp;</span>'.__d('shop','pálivá').'</div>';
			}
			if($i->hot == 2){
				echo '<div class="paprika pap2"><span>&nbsp;</span>'.__d('shop','<strong>hodně</strong> pálivá').'</div>';
			}
			echo '</div>';
		}
	//}
	
	?>
	<p class="description" data-title="<?php echo $i->description;?>"><?php echo ($i->shop_category_id != 10 && !empty($i->gramaz)?$i->gramaz.', ':'').$i->description; ?></p>
	<div title="<?php echo __d('shop','Přidat do oblíbených');?>" id="fav<?php echo $i->id;?>" class="favorite" data-id="<?php echo $i->id;?>"></div>
	<?php if ($i->credit > 0){ ?>
	<div class="plus_credit" title="<?php echo __d('shop','Přidá Vám '.$i->credit.' kreditů');?>">+<?php echo $i->credit?></div>
	<?php } ?>
	
	
	<?php if (in_array($i->number,[107])){ ?>
	<div class="hot"></div>
	<?php } ?>
	<div class="basket_add_el">
	<?php
	if ($i->code != 211){
	if (isset($i->shop_product_con_attributes) && count($i->shop_product_con_attributes)>0){
		$pridavky_list = [];
		foreach($i->shop_product_con_attributes AS $p){
			$pridavky_list[$p->shop_product_attribute_id] = $p->shop_product_attribute_id;
		}
	} else {
		$pridavky_list = '';
	}
		$opt = [
		'id'=>$i->id,
		'code'=>$i->code,
		'price'=>$i->price_vat,
		'ks'=>1,
		'img'=>true,
		'hidden_ks'=>true,
		'btn_class'=>'btn-md',
		'variants_list'=>(isset($i->variants_list)?$i->variants_list:''),
		'variants_select_list_item'=>(isset($i->variants_select_list)?$i->variants_select_list:''),
		'pridavky_list'=>$pridavky_list,
	];
	//pr($i);
	
	echo $this->Shop->addBasketElement($opt); 
	} else {
	
	echo '<ul class="varianta_list_item">';
		echo '<li class="varianta_list_select clearfix full stripsMenuAdd">Sestav si své STRIPS MENU</li>';
	echo '</ul>';
	}
	?>
	</div>
	</div>
</div>
