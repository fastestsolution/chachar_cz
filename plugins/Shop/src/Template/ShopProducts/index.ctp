<div id="tooltip" class="none"><?php echo __d('shop','Máte zvolen filtr');?><span>&nbsp;</span></div>
<div id="search_line" class="close_side">

<span class="fa fa-search" id="show_search">&nbsp;</span>
<?php echo $this->Form->input('search_txt',['label'=>false,'class'=>'form-control','placeholder'=>__d('shop','Název, číslo, suroviny...')]); ?>
<div class="fa fa-close none" id="resetFiltr"></div>
<?php echo $this->Form->button(__d('shop','Hledej'),['label'=>false,'class'=>'btn btn-primary']); ?>
</div>
<?php 

if (isset($category_detail)){ 
	echo $category_detail->description;
}
echo '<p class="seo_txt">'.__d('shop','Rozvoz originál czech made pizzy zdarma.').' '.strip_tags($title).' '.__d('shop','vám nabízí následující nabídku: ').'</p>';
/*
echo '<div id="continue_kosik" class="no_mobile">';
	echo '<strong>'.__d('shop','Přidáno do košíku').'</strong>';
	echo $this->Html->link(__d('shop','Pokračovat ve výběru'),'#',['title'=>__d('shop','Pokračovat ve výběru'),'class'=>'btn btn-primary green','id'=>'continue_kosik_but']);
	echo $this->Html->link(__d('shop','Přejít do košíku'),'/'.KOSIK_LINK.'/',['title'=>__d('shop','Přejít do košíku'),'class'=>'btn btn-primary basket']);
echo '</div>';
*/
?>
<?php 
$this->Html->templates([
	'link' => '<a href="'.$url_lang_prefix.'{{url}}"{{attrs}}>{{content}}</a>'
]);
?>

<?php //echo $this->element('Shop.../ShopProducts/elements/price_slider'); ?>
<?php //echo $this->element('Shop.../ShopProducts/elements/filtration'); ?>
<?php echo $this->element('Shop.../ShopProducts/elements/subcats'); ?>
<div class="relative">
	<div id="history_scroll_top"></div>
	<div id="fst_history">
		<?php echo $this->element('Shop.../ShopProducts/items'); ?>
		<div id="noresult" class="none"><?php echo __d('shop','Nenalezeny žádné produkty, upravte hledání')?></div>
	</div>
</div>
<div id="scrollToTop" class="no_mobile fa fa-angle-up"></div>

