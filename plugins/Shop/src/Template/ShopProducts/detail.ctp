<div id="product_detail">
	<div class="row">
		<div id="kosik_adopt" class="col col-md-6 pull-right">
		
		</div>
		<div id="img_adopt" class="col col-md-6">
		
		</div>
	</div>
	<div class="description"><?php echo $product->description; ?></div>
	<div class="technical_specification">
		<?php 
		if (!empty($product->technical_specification)){
			echo '<h2>'.__d('shop', 'Technická specifikace produktu ').$product->name.'</h2>';
			echo $product->technical_specification; 
		}
		?>
	</div>
	
	<div class="row">
		<div class="description"></div>
		
		<div class="col col-md-6">
			<div class="product_imgs el_adopt" data-el="img_adopt">
			<?php
				$img_set = array(
					'imgs'=>$product->images,
					//'link'=>$product_url,
					'class'=>'',
					'title'=>$product->name,
					'width'=>600,
					'height'=>400,
					'other_imgs'=>true,		
					'zoom'=>true,		
					'bg'=>'#fff',		
					'width_other'=>320,		
					'height_other'=>220,		
					'path'=>'/uploaded/products/',		
					
				);
				echo $this->Fastest->img($img_set);
			?>
			</div>
		</div>
		<div class="col col-md-6">
		<div class="kosik_el el_adopt variant_change" data-el="kosik_adopt">
			<div class="description_mobile"><?php echo $product->description; ?></div>
			<?php 
			if (isset($product->variants_list) && !empty($product->variants_list)){
				// pokud je produkt s variantama
				$i = 0;
				foreach($product->variants_list AS $v){
					$item = (object) array_merge(json_decode(json_encode($product),true), json_decode(json_encode($v),true));
					echo $this->element('Shop.../ShopProducts/elements/product_attributes',['item'=>$item,'variants'=>true,'i'=>$i]);
					$i++;
				}
			} else {
				// pokud neni
				echo $this->element('Shop.../ShopProducts/elements/product_attributes',['item'=>$product]);
			}
			
			?>
			<div id="add_cart">
			<?php
				$opt = [
				'id'=>$product->id,
				'price'=>$product->price_vat,
				'ks'=>1,
				'hidden_ks'=>false,
				'variants_list'=>(isset($variants_list)?$variants_list:''),
				'variants_select_list'=>(isset($variants_select_list)?$variants_select_list:''),
				'btn_class'=>'btn-lg',
			];
			echo $this->Shop->addBasketElement($opt); 
			?>
			</div>
		</div>
		</div>
	</div>
	<?php 
	$opt = [
		'title'=>__d('shop', 'Související produkty'),
		'type'=>'related_products',
		'limit'=>6,
		'ids'=>json_decode($product->related_products),
		'class'=>'col col-md-4 col-xs-6 product_item centered',
	];
	if (!empty($opt['ids']))
	echo $cell = $this->cell('Shop.Products',[$opt], ['cache' => false]);?>	
</div>
<?php //pr($product->_translations['en']); ?>
<?php pr($product); ?>

