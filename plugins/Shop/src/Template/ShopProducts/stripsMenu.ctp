<div id="stripsMenus" class="<?php echo (isset($addStrips)?'none':'none');?>">
	<ul id="stripSteps">
		<li id="stripStep1" class="active"><span>1.</span>Vyber si stripsy</li>
		<li id="stripStep2"><span>2.</span>Vyber si přílohu</li>
		<li id="stripStep3"><span>3.</span>Vyber si omáčku</li>
	</ul>
    <?php
    //if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
        echo '
					
						<div id="close_variants" class="hiddenCloseVariants">
						</div>
					
					';
    //}
    ?>
	<div id="step1" class="step">
		<div class="row">
			<div class="col-sm-12 col-md-6">
			<div class=" nepalive item">
				<span class="ico"></span>
				<div class="text">
				<div class="title">STRIPSY</div>
				<div class="title2">NEPÁLIVÉ</div>
				</div>
				<div class="line"></div>
				<a href="#" data-id="1" class="next_step" data-type="firstStep">Chci vybrat počet</a>
				<div class="img"></div>
				<div class="second">
				</div>
			</div>
			</div>
			<div class="col-sm-12 col-md-6">
			<div class=" pikantni item">
				<span class="ico"></span>
				<div class="text">
				<div class="title">STRIPSY</div>
				<div class="title2">JEMNĚ PIKANTNÍ</div>
				</div>
				<div class="line"></div>
				<a href="#" data-id="2" class="next_step" data-type="firstStep">Chci vybrat počet</a>
				<div class="img"></div>
				<div class="second"></div>
			</div>
			</div>
		</div>
	</div>

	<div id="step2" class="step">
		<div class="row">
			<div class="col-lg-3 col-md-4 col-sm-4 box-strips">
			
				<div class="shadow">
                    <ul class="menuList" >
                        <li class="title">TVÉ STRIPS MENU JIŽ OBSAHUJE</li>
                    </ul>
					<ul class="menuList" id="menuList" > 
                        <li class="mitem" data-template>
                            <div class="background cover_img" data-src="{{img}}"></div>
                            <div class="title-wrapper">
                                <div class="title">
                                    {{name}}
                                </div>
                            </div>
                        </li>
                    </ul>
                    <ul class="menuList">
                        <li id="menuDesc"></li>
                    </ul>
                </div>
			</div>
			<div class="col-sm-8 col-md-8 col-lg-9" id="tabPrilohy">
				<div class="col-xs-12 col-lg-6 box-strips">
					<div class="item">
						<div class="title">Salátek coleslaw</div>
						<div class="line"></div>
						<a href="#" data-id="4" class="next_step" data-type="addonStep">Chci ho</a>
						<div class="img coleslaw"></div>
					</div>
				</div>
				<div class="col-xs-12 col-lg-6 box-strips">
					<div class="item">
						<div class="title">Hranolky velké</div>
						<div class="line"></div>
						<a href="#" data-id="5" class="next_step" data-type="addonStep">Chci je</a>
						<div class="img hranolky"></div>
					</div>
				</div>
				<div class="col-xs-12 col-lg-6 box-strips">
					<div class="item">
						<div class="title">Bramborová kaše</div>
						<div class="line"></div>
						<a href="#" data-id="6" class="next_step" data-type="addonStep">Chci ji</a>
						<div class="img kase"></div>
					</div>
				</div>
			</div>
		
			<div class="col-sm-8 col-md-8 col-lg-9 none" id="tabOmacky">
				<div class="col-xs-12 col-lg-6 box-strips">
					<div class="item">
						<div class="title">Tatarská omáčka</div>
						<div class="line"></div>
						<a href="#" data-id="7" class="next_step" data-type="omackyStep">Chci ji</a>
						<div class="img tatarka"></div>
					</div>
				</div>
				<div class="col-xs-12 col-lg-6 box-strips">
					<div class="item">
						<div class="title">Sweet chilli</div>
						<div class="line"></div>
						<a href="#" data-id="8" class="next_step" data-type="omackyStep">Chci ji</a>
						<div class="img chilli"></div>
					</div>
				</div>
				<div class="col-xs-12 col-lg-6 box-strips">
					<div class="item">
						<div class="title">BBQ</div>
						<div class="line"></div>
						<a href="#" data-id="9" class="next_step" data-type="omackyStep">Chci ji</a>
						<div class="img bbq"></div>
					</div>
				</div>
				<div class="col-xs-12 col-lg-6 box-strips">
					<div class="item">
						<div class="title">Kečup</div>
						<div class="line"></div>
						<a href="#" data-id="10" class="next_step" data-type="omackyStep">Chci ho</a>
						<div class="img kecup"></div>
					</div>
				</div>
				<div class="col-xs-12 col-lg-6 box-strips">
					<div class="item">
						<div class="title">Belgická</div>
						<div class="line"></div>
						<a href="#" data-id="11" class="next_step" data-type="omackyStep">Chci ji</a>
						<div class="img belgicka"></div>
					</div>
				</div>
				<div class="col-xs-12 col-lg-6 box-strips">
					<div class="item">
						<div class="title">Mexická</div>
						<div class="line"></div>
						<a href="#" data-id="12" class="next_step" data-type="omackyStep">Chci ji</a>
						<div class="img mexicka"></div>
					</div>
				</div>
			</div>
			
		</div>
	</div>
	
	<div id="stripsSelect" class="none">
	
				<div class="triangle"></div>
		<div class="desc">Vyberte počet kusů. Přílohy a omáčky vybereš v dalším kroku</div>
		<ul>
			<li><span class="select " data-type="secondStep" data-id="1"></span>3x STRIPS + 1x PŘÍLOHA + 2x OMÁČKA <span class="price">129,-</span></li>
			<li><span class="select " data-type="secondStep" data-id="2"></span>5x STRIPS + 2x PŘÍLOHA + 2x OMÁČKA <span class="price">179,-</span></li>
			<li><span class="select " data-type="secondStep" data-id="3"></span>7x STRIPS + 2x PŘÍLOHA + 2x OMÁČKA <span class="price">199,-</span></li>
		</ul>
	</div>
</div>