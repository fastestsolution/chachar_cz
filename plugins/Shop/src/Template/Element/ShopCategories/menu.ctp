<?php /* ?>
<nav>
    <ul class="nav">
	    <li><a href="#">Link 1</a></li>
		<li><a href="#" id="btn-1" data-toggle="collapse" data-target="#submenu1" aria-expanded="false">Link 2 (toggle) <i class="fa fa-caret-down"></i></a>
			<ul class="nav collapse" id="submenu1" role="menu" aria-labelledby="btn-1">
				<li><a href="#">Link 2.1</a></li>
				<li><a href="#">Link 2.2</a></li>
				<li><a href="#">Link 2.3</a></li>
			</ul>
		</li>
		<li><a href="#">Link 3</a></li>
		<li><a href="#">Link 4</a></li>
	</ul>
</nav>
<?php */ ?>
<?php 
//pr($shop_menu_items);
		
$options = [
	'class'=>'nav',
	'enable_arrow'=>true,
	'submenu_pref'=>'shop_submenu',
	'path'=>$shop_menu_items_path,
	'data-target'=>true,
			
	'open_cat_id'=>(isset($open_cat_id)?$open_cat_id:'')
];
echo '<div id="shop_navigace">';
	echo '<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#shop_navbar" aria-expanded="false" aria-controls="shop_navbar">';
		echo '<span class="sr-only">Toggle navigation</span>';
		echo '<span class="icon-bar"></span>';
		echo '<span class="icon-bar"></span>';
		echo '<span class="icon-bar"></span>';
	echo '</button>';
	echo '<div id="shop_navbar" class="navbar-collapse collapse">';
	echo '<nav class="shop_nav">';
		echo $this->Fastest->generateMenu($shop_menu_items,$options);
	echo '</nav>';
	echo '</div>';
echo '</div>';
?>
<?php //pr($shop_menu_items); ?>
