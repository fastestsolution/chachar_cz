<div class="side_left">
		<?php echo $this->Html->link('<span class="shop_basket_total_ks"></span>','/'.KOSIK_LINK.'/',['title'=>__('Přejít do košíku'),'class'=>'foo_basket_ico basket_link','escape'=>false]); ?>
		<?php /* ?><div class="foo_basket_ico"><span class="shop_basket_total_ks"></span></div><?php */ ?>
		<div class="foo_basket_price">
			<?php echo __('Cena košíku'); ?>
			<span class="shop_basket_total_price_vat">0,-</span>
		</div>
		</div>
		
		<ul class="products_basket" data-template_id="kosik_el">
			<li class="hide_template clearfix" data-template="data-template">
				<div class="img_round ">
					<div class="img lazy lazy_force" data-original="{{img}}"></div>
				</div>
				<span class="ks">{{count}}x</span>
				
				<span class="name">{{name}}</span>
				<span class="price">{{price_total_vat}}</span>
				<?php echo $this->Html->link('','/orders/delete_basket/{{product_id}}',['title'=>__('Smazat'),'class'=>'delete_basket remove','data-confirm'=>__d('shop', 'Opravdu smazat?')],null,false);?>
				
			</li>
			<li class="basket_empty" data-template-fallback="data-template-fallback"><?php echo __d('shop', 'Váš košík je prázdný'); ?></li>
		</ul>
		<div class="side_right">
		<?php echo $this->Html->link(__d('shop', 'Objednat'),'/'.KOSIK_LINK.'/',['title'=>__d('shop', 'Přejít do košíku'),'class'=>'chbut right basket_link'],null,false); ?>
		</div>
		<?php //pr($basket_products); ?>
		
		<?php  echo '<div id="php_basket_products" class="none">'.(!empty($basket_products)?json_encode($basket_products):'').'</div>';?>
		</div>
		<?php
		$opt = [
		'id'=>1,
		//'varianta_select'=>1,
		//'varianta_list'=>[1=>'a',2=>'b'],
		'price'=>555,
		'ks'=>1,
		'hidden_ks'=>true,
		];
		?>