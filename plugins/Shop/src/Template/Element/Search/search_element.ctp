<?php
echo '<div id="search_products" class="el_adopt" data-el="search_adopt">';
	echo $this->Form->create('',['url'=>$url_lang_prefix.'/products_search/']);
		echo '<div class="row">';
			echo '<div class="col col-sm-12">';
				echo $this->Form->input('search',['label'=>false,'class'=>'form-control autocomplete']);
			echo '</div>';
			//echo '<div class="col col-sm-2">';
				//echo $this->Form->button(__('Hledej'), ['class' => 'btn btn-md btn-default']);
			//echo '</div>';
		echo '</div>';
	echo $this->Form->end();
echo '</div>';
?>