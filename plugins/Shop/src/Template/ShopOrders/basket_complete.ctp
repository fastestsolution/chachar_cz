<?php   
if (isset($payment_result)){
	if ($payment_result['r'] == true){
		echo '<div class="basket_done">';
			echo '<div class="ico"></div>';
			echo '<strong class="title">'.__d('shop','Vyřízeno. Platba je v pořádku. Už se to vaří!').'</strong>';
			echo '<p>'.__d('shop','Děkujeme za Vaši objednávku! Vaše objednávka je zaplacena. Nyní vyčkejte na potvrzovací SMS s aktuální dobou doručení').'</p>';
			if (isset($credit_add)){
				echo '<p>'.__d('shop','Za Vaši objednávku Vám bude připočteno ').'<strong>'.$credit_add.'</strong>'.__d('shop',' kreditů').'</p>';
			}
			//echo $this->Html->link(__d('shop', 'Zpět na úvod'),'/',array('title'=>__d('shop', 'Zpět do úvod'),'class'=>'btn btn-primary btn-green',null,false));
			//echo $this->Html->link(__d('shop', 'Zatím si přečtu vtipy'),'#',array('title'=>__d('shop', 'Zatím si přečtu vtipy'),'class'=>'btn btn-primary btn-green','id'=>'show_vtip',null,false));

		echo '</div>';
	} else {
		echo '<div class="basket_done">';
			echo '<div class="ico"></div>';
			echo '<strong class="title alert">'.__d('shop','Chyba platby. Platbu nebylo možné ověřit!').'</strong>';
			echo '<p>'.__d('shop','Vratťe se zpět do košíku a vyberte jinou platební metodu').'</p>';
			echo $this->Html->link(__d('shop', 'Zpět do košíku'),'/'.KOSIK_LINK.'/',array('title'=>__d('shop', 'Zpět do košíku'),'class'=>'btn btn-primary btn-green',null,false));
		echo '</div>';
	}
} else {

echo '<div class="basket_done">';
	echo '<div class="ico"></div>';
	echo '<strong class="title">'.__d('shop','Vyřízeno. Už se to vaří!').'</strong>';
	echo '<p>'.__d('shop','Děkujeme za Vaši objednávku! <strong>Uděláme vše proto, abychom Vám ji doručili co nejdříve!   </strong>').'</p>';
	echo '<p>'.__d('shop','Do 5ti minut Vám dorazí potvrzovací SMS s aktuální doručovací dobou.').'</p>';
	if (isset($credit_add)){
		echo '<p>'.__d('shop','Za Vaši objednávku Vám bude připočteno ').'<strong>'.$credit_add.'</strong>'.__d('shop',' kreditů').'</p>';
	}
	//echo $this->Html->link(__d('shop', 'Zpět na úvod'),'/',array('title'=>__d('shop', 'Zpět do úvod'),'class'=>'btn btn-primary btn-green',null,false));
	//echo $this->Html->link(__d('shop', 'Zatím si přečtu vtipy'),'#',array('title'=>__d('shop', 'Zatím si přečtu vtipy'),'class'=>'btn btn-primary btn-green','id'=>'show_vtip',null,false));
	
echo '</div>';
}
/*
	echo '<div id="vtip_list_over">';
		echo '<div id="vtip_list">';
			echo '<div class="vtip">Víte jaký je rozdíl mezi picařem a gynekologem? Žádný, oba si akorát přivoní, ale ani jeden neochutná...</div>';
			echo '<div class="vtip">dalsi vtip</div>';
			echo '<div class="vtip">dalsi vtip 2</div>';
		echo '</div>';
		echo $this->Html->link(__d('shop', 'Další vtip'),'#',array('title'=>__d('shop', 'Další vtip'),'class'=>'btn btn-primary btn-green','id'=>'next_vtip',null,false));
	echo '</div>';
*/
/*
echo '<div class="alert alert-success">';
	echo '<strong>'.__d('shop', 'Děkujeme za objednávku').'</strong>, '.__d('shop', 'Vaše objednávky byla dokončena, nyní počkejte na její potvrzení.');
	echo '<div class="col-md-4 col-md-offset-4">';
	echo '</div>';
	echo '<div class="clear"></div>';
			
echo '</div>';
*/
?>

<?php 
if (isset($ga_ecommerce)){
	echo '<div id="ga_ecommerce" class="none">';
	echo json_encode($ga_ecommerce);
	echo '</div>';
}
?>