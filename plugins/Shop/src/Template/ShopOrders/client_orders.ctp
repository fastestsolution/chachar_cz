<?php 
echo $this->element('../ShopOrders/elements/map_orders');
//pr($orders);
if (isset($orders)){ 
	echo '<table id="user_orders" class="table-responsive">';
		echo '<thead>';
			echo '<th>'.__d('shop','Datum').'</th>';
			echo '<th>'.__d('shop','Provoz').'</th>';
			echo '<th>'.__d('shop','Adresa').'</th>';
			echo '<th>'.__d('shop','Platba').'</th>';
			echo '<th>'.__d('shop','Zaplaceno').'</th>';
			echo '<th>'.__d('shop','Částka').'</th>';
			echo '<th>'.__d('shop','Stav').'</th>';
			echo '<th>'.__d('shop','Detail').'</th>';
			//echo '<th>'.__d('shop','Stav').'</th>';
		echo '</thead>';
		echo '<tbody>';
			foreach($orders AS $k=>$o){
				echo '<tr class="'. (($k++ % 2) ? "even" : "odd") .'">';
				//pr($chachar_payment_list);
				//pr($o);
				echo '<td>'.$o['created']->format('d.m.Y H:i:s').'</td>';
				echo '<td>'.$basket_provoz_list['provoz_list'][$o['shop_provoz_id']].'</td>';
				echo '<td>'.$o['user_address']['street'].' '.$o['user_address']['cp'].', '.$o['user_address']['city'].'</td>';
				echo '<td>'.$chachar_payment_list[$o['shop_payment_id']]['name'].'</td>';
				echo '<td>'.(!empty($o['payment_recieved'])?__d('shop','Ano'):__d('shop','Ne')).'</td>';
				echo '<td>'.$this->Fastest->price($o['price_vat']).'</td>';
				echo '<td>'.$order_stav_list[$o['stav_id']].'</td>';
				echo '<td>';
					echo $this->Html->link(__d('shop','Detail'),'#',['title'=>__d('shop','Zobrazit položky'),'class'=>'show_detail','data-show'=>'detail_'.$o['id']]);
					if ($o['stav_id'] == 6){
						echo ' | '.$this->Html->link(__d('shop','Zobrazit na mapě'),'#',['title'=>__d('shop','Zobrazit na mapě'),'class'=>'show_map ','data-show'=>$o['id']]);
						
					}
				echo '</td>';
				echo '</tr>';
				if (!empty($o['shop_order_items'])){
					foreach($o['shop_order_items'] AS $oi){
						echo '<tr class="none detail detail_'.$o['id'].'">';
							echo '<td colspan="2">'.$oi['count'].'ks - '.$oi['name'].'</td>';
							echo '<td></td>';
							
							echo '<td colspan="5"></td>';
						echo '</tr>';
					}
				}
			}
		echo '</tbody>';
	echo '</table>';
} else {
	echo '<div class="alert alert-danger">';
		echo __d('shop','Zatím nemáte žádné objednávky');
	echo '</div>';
}
?>