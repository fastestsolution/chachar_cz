<div id="basket_detail">
<?php //pr($loggedUser); ?>
<?php echo $this->element('Shop.../ShopOrders/elements/basket_content'); ?>

<?php if (isset($basket_products['products']) && !empty($basket_products['products'])){ ?>



<?php //pr($basket_products['products']); ?>
<?php echo $this->element('Shop.../ShopOrders/elements/napoje',[]); ?>
<?php echo $this->Form->create($orders,['url'=>'/'.KOSIK_LINK.'/']);?>
<?php //echo $this->element('Shop.../ShopOrders/elements/transport'); ?>
<?php echo $this->element('Shop.../ShopOrders/elements/payment'); ?>
<?php echo $this->element('Shop.../ShopOrders/elements/credit_use'); ?>
<?php //echo $this->element('Shop.../ShopOrders/elements/logged_info'); ?>

<?php //pr($map_area_list); ?> 
<div id="map_coords" class="none"> 
<?php echo json_encode($map_area_list); ?>
</div>

<div class="basket_content" id="scroll-osobni">
	<h2><?php echo __('Osobní údaje');?></h2> 
	<?php echo $this->Html->link(__('Přejít na adresu doručení'),'#scroll-adresa',['title'=>__('Přejít na adresu doručení'),'class'=>'scroll_moo scroll_but']); ?>
	<div class="basket_line"></div>

	<?php echo $this->element('../Users/elements/user_data',['pref'=>'user.','noPassword'=>true]); ?>
</div>

<div class="basket_content"  id="scroll-adresa">
	<h2><?php echo __('Adresa doručení');?> (ulice č.p. , město)</h2>
	<?php //echo $this->Html->link(__('Přejít na dokončení'),'#scroll-sumace',['title'=>__('Přejít na dokončení objednávky'),'class'=>'scroll_moo scroll_but']); ?>
	<div class="basket_line"></div>
	<?php echo $this->element('../Users/elements/address_data',['address_type'=>1,'pref_order'=>'user.']); ?>

	<?php if (!isset($loggedUser)){ ?>
	<?php echo $this->Form->input('vop', ['label' => ['text'=>__d('shop', 'Musíte souhlasit s <a href="/obchodni-podminky/" target="_blank">všeobecnými podmínkami</a>'),'escape' => false],'class'=>'checkbox','type'=>'checkbox']); //data-toggle="modal" data-target="#vopModal" ?>

	<?php echo $this->Form->input('newsletter', ['label' => __d('shop', 'Chci zasílat novinky emailem'),'class'=>'checkbox','type'=>'checkbox','checked'=>'checked']); ?>

	<?php echo $this->element('Shop.../ShopOrders/elements/vop_modal'); ?>
	<?php } ?>
</div>
<?php echo $this->element('Shop.../ShopOrders/elements/note'); ?>
<div class="basket_content"  id="scroll-sumace">
	<h2><?php echo __('Celkem za objednávku');?></h2>
	<div class="basket_line"></div>
	
	<p class="let18"><small>Alkoholické nápoje si mohou objednat pouze osoby starší <strong>18 let</strong>.</small></p>
	
	<?php 
	if (isset($is_doprava)){	
		echo '<p class="doprava_desc">Cena je včetně <strong>19,-</strong> za dopravu</p>';
	} 
	?>
	
	<p><?php echo __('Za objednávku zaplatíte celkem');?>: <span class="shop_basket_total_price_vat" id="sum_total_price">0,-</span></p>
	
	
	
</div>

<?php /* ?>
<div class="row justify-content-end" >
	<div class="col-md-6 col-md-offset-6 text-right basket_detail_total_price">
		<?php echo __('Cena celkem bez DPH');?>: <span class="shop_basket_total_price">0,-</span><br />
		<?php echo __('Cena celkem s DPH');?>: <span class="shop_basket_total_price_vat">0,-</span>
	</div>
</div>
<?php */ ?>


<div class="col-sm-12 text-center" id="scroll-done">
<?php echo $this->Form->button(__d('shop', 'Odeslat objednávku'),['class'=>'btn btn-lg btn-primary center-block SaveForm','label'=>false,'id'=>'OrderSaveButton']); ?>
</div>

<?php echo $this->Form->end(); ?>
<?php echo $this->element('Shop.../ShopOrders/elements/gopay'); ?>

<?php } ?>

</div>

<?php //pr($basket_products['products']); ?>