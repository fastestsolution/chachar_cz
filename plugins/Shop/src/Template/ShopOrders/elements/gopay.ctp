<div id="gopay">
	<form action="https://gw.sandbox.gopay.com/gw/v3/c7de8b2fd05d6dc8abe59ef095e700a2" method="post" id="gopay-payment-button" class="none">
		
		<?php echo $this->Form->submit(__d('shop', 'Zaplatit kartou'),array('class'=>'btn','id'=>'zaplatit','name'=>'pay')); ?>
	  <script type="text/javascript" src="https://testgw.gopay.cz/gp-gw/js/embed.js"></script>
	</form>
	<div id="gopay_error" class="alert alert-danger none">
		<?php echo __d('shop', 'Není založena platba! Kontaktujte telefonicky provozovnu.')?>
	</div>
	
</div>