<div class="basket_content">
<h2><?php echo __('Váš košík');?></h2>
<?php echo $this->Html->link(__('Přejít na výběr platby'),'#scroll-platba',['title'=>__('Přejít na výběr platby'),'class'=>'scroll_moo scroll_but']); ?>
<div class="basket_line"></div>

<ul id="basket_product_table" class="products_basket table table-striped" data-template_id="basket_detail">
	
	<li class="hide_template" data-template="data-template">
		<div class="row">	
			<div class="col col-sm-2 itm img_round no_mobile pizza_{{is_pizza}}">
				<div class="img lazy lazy_force" data-original="{{img}}"></div>
			</div>
			<div class="col col-sm-4 col-xs-12 itm">
				<div class="itm_title"><?php echo __d('shop', 'Název'); ?></div>
				<span class="number">{{number}}.</span> <span class="name">{{name}}</span>
				<div class="prod_attributes_list none " data-title="<?php echo __('Vybranné přídavky navíc	:')?>">{{attributes_ids}}</div>
			</div>
			<div class="col col-sm-2 col-xs-12 itm">
				<div class="itm_title"><?php echo __d('shop', 'Cena za 1 kus'); ?></div>
				<span class="strong">{{price_vat}}</span>
			</div>
			
			<div class="col col-sm-1 col-xs-12 itm">
				<div class="itm_title"><?php echo __d('shop', 'Množství'); ?></div>
				
				<?php echo $this->Form->input('count',['class'=>'change_ks input_number form-control','value'=>'{{count}}','label'=>false,'data-id'=>'{{product_id}}','type'=>'number']); ?>
			</div>
			<div class="col col-sm-2 col-xs-12 itm price">
				<div class="itm_title"><?php echo __d('shop', 'Celková cena'); ?></div>
				<span class="total_price">{{price_total_vat}}</span>
			</div>
			
			<div class="col col-sm-1 itm delete">
				<div class="itm_title"></div>
				<?php echo $this->Html->link('','/orders/delete_basket/{{product_id}}',['title'=>__('Smazat'),'class'=>'delete_basket','data-confirm'=>__d('shop', 'Opravdu smazat?')],null,false);?>
			</div>
		</div>	

	</li>
	<li class="basket_empty" data-template-fallback="data-template-fallback">
		<td colspan="6"><?php echo __d('shop', 'Váš košík je prázdný'); ?></td>
	</li>
</ul>
<div id="attributes_list_json" class="none">
	<?php echo json_encode($attributes_list); ?>
</div>
	<div class="total_price_txt">
		<div class="row">
			<div class="col col-sm-6"></div>
			<div class="col col-sm-3 col-xs-8 text_right"><?php echo __('Celková cena za objednávku');?>:
			<?php 
			if (isset($is_doprava)){
				echo '<span class="doprava_desc2">včetně '.$doprava_price.',- za dopravu</span>';
			}
			?>
			</div>
			<div class="col col-sm-3 col-xs-4"><span class="shop_basket_total_price_vat">0,-</span></div>
		</div>
		 
	</div>
</div>