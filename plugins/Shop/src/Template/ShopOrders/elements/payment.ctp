<div class="basket_content" id="scroll-platba">
<div id="payment_video" class="nonea">Děkujeme Vám za dýško...</div>
<h2><?php echo __d('shop', 'Výběr platby');?></h2>
<?php echo $this->Html->link(__('Přejít na osobní údaje'),'#scroll-osobni',['title'=>__('Přejít na osobní údaje'),'class'=>'scroll_moo scroll_but']); ?>
<div class="basket_line"></div>

<div class="field_select" id="platba_element">
<?php
echo '<ul id="payment_list">';
foreach($chachar_payment_list AS $p_id =>$p){
	
	echo '<li><div class="ico ico'.$p_id.'"></div>'. $this->Html->link($p['name'],'#',['title'=>$p['name'],'data-id'=>$p_id,'id'=>'PaymentId'.$p_id,'class'=>'btn btn-default']) .'</li>';
}
echo '</ul>';

echo '<div id="spropitne_list" class="clearfix none">';
	echo '<h2>'.__('Přejete si nás odměnit spropitným? Vyberte prosím částku... (nepovinné)').'</h2>';
	echo '<ul>';
		echo '<li class="none">';
			echo '<div class="price">0,-</div>';
			echo $this->Html->link(__(''),'#',['title'=>__('Vybrat spropitné'),'data-value'=>0]);
		echo '</li>';
		echo '<li>';
			echo '<div class="price">10,-</div>';
			echo $this->Html->link(__(''),'#',['title'=>__('Vybrat spropitné'),'data-value'=>10]);
		echo '</li>';
		echo '<li>';
			echo '<div class="price">20,-</div>';
			echo $this->Html->link(__(''),'#',['title'=>__('Vybrat spropitné'),'data-value'=>20]);
		echo '</li>';
		echo '<li>';
			echo '<div class="price">50,-</div>';
			echo $this->Html->link(__(''),'#',['title'=>__('Vybrat spropitné'),'data-value'=>50]);
		echo '</li>';
	echo '</ul>';
echo '</div>';
echo $this->Form->input('shop_payment_id',['type'=>'hidden']);
echo $this->Form->input('spropitne',['type'=>'hidden']);
?>
</div>
<?php
/*
if (isset($payment_list)){
	if ($is_radio_button == true){
	
	echo '<table class="table_doprava table">';
	foreach($payment_list AS $dop){
		echo '<tr class="row payment_line payment_line_'.$dop->id.'">';
			echo '<td>';
			$opt = [
				'select_type'=>'radio',
				'data'=>$dop,
				'label'=>false,
				'name'=>'shop_payment_id',
				'type'=>'payment',
				'basket_products'=>$basket_products,
				'data_list'=>$payment_list,
			];
			echo $this->Shop->selectTransportElement($opt);
						
			echo '</td>';
			echo '<td class="">'.$dop->name.'</td>';
			echo '<td class="transport_price">'.$this->Fastest->price($dop->price_vat).'</td>';
		echo '</tr>';
	}
	echo '</table>';
	
	} else {
		
		$opt = [
			//'type'=>'radio'
			//'data'=>$data
			'label'=>__('Platba'),
			'name'=>'shop_payment_id',
			'type'=>'payment',
			'basket_products'=>$basket_products,
			'data_list'=>$payment_list,
		];
		echo $this->Shop->selectTransportElement($opt);
	}
} else {
	echo '<div class="alert alert-danger">';
		echo __d('shop', 'Není definovaná žádná platba');
	echo '</div>';
}
*/
?>
</div>