
<div class="basket_content" id="scroll-napoje">
	<h2><?php echo __('Nemáte žízeň?');?></h2>
	<?php echo $this->Html->link(__('Přejít na platbu'),'#scroll-platba',['title'=>__('Přejít na platbu'),'class'=>'scroll_moo scroll_but']); ?>
	<div class="basket_line"></div>
	<?php echo $this->Html->link(__d('shop','Chci objednat nápoj'),'#',['title'=>__d('shop','Chci objednat nápoj'),'class'=>'btn btn-default only_mobile','id'=>'show_napoje']); ?>
	<div id="basket_napoj_mobile">
	<?php  
	
		$opt = [
			'type'=>'category',
			'limit'=>4,
			'order'=>'sort ASC',
			'shop_category_id'=>[8],
			'shop_product_ids'=>[93,331,117,116],
		];
		echo '<div id="products_list">';
		echo $cell = $this->cell('Shop.Products',[$opt], ['cache' => false]);
		echo '</div>';
	?>
	</div>
</div>