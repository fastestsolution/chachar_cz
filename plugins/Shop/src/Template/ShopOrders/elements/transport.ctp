<h2><?php echo __d('shop', 'Doprava');?></h2>
<div class="field_select">
<div id="products_price" class="none"></div>
<?php 
if (isset($transport_list)){
	
	if ($is_radio_button == true){
	
	echo '<table class="table_doprava  table">';
	foreach($transport_list AS $dop){
		//pr($dop);
		//pr($basket_transport);
		echo '<tr class="row transport_line transport_line_'.$dop->id.'">';
			echo '<td>';
			
			$opt = [
				'select_type'=>'radio',
				'data'=>$dop,
				'label'=>false,
				'name'=>'shop_transport_id',
				'type'=>'transport',
				'basket_products'=>$basket_products,
			];
			echo $this->Shop->selectTransportElement($opt);
			
			echo '</td>';
			echo '<td>'.$dop->name.'</td>';
			echo '<td class="transport_price">'.$this->Fastest->price($dop->price_vat).'</td>';
		echo '</tr>';
	
	}
	echo '</table>';
	
	} else {
		
		$opt = [
			'label'=>__('Doprava'),
			'name'=>'shop_transport_id',
			'type'=>'transport',
			'basket_products'=>$basket_products,
			'data_list'=>$transport_list,
		];
		echo $this->Shop->selectTransportElement($opt);
	
	}
} else {
	echo '<div class="alert alert-danger">';
		echo __d('shop', 'Není definovaná žádná doprava');
	echo '</div>';
}
//pr($basket_products);
?>
</div>