<div class="basket_content" id="scroll-platba">
<h2><?php echo __d('shop', 'Poznámka k objednávce');?></h2>

<div class="basket_line"></div>

<?php echo $this->Form->input('note',['type'=>'textarea','label'=>false,'class'=>'form-control','placeholder'=>__('Zde si můžete napsat poznámku k objednávce. Případně upřesnit doručovací adresu. Poznámka neslouží k úpravě pizz.')]);?>
</div>