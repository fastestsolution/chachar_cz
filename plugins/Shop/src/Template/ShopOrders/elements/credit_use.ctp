<?php if (isset($loggedUser)){ ?>
<div class="basket_content">
<h2><?php echo __d('shop', 'Kreditní účet');?></h2>
<?php //pr($loggedUser); ?>
<div class="basket_line"></div>
<div id="credit_use_element">	
	<?php echo '<h2 class="big">'.__('Kolik si přejete odečíst kreditů ze svého účtu? <span class="sm">(min. 100 kreditů)</span>').'</h2>'; ?>
	<div class="col col-md-4">
		<strong><?php echo __('Váš aktuální počet kreditů');?></strong>
		<span class="pocet" id="count_credit" data-max="<?php echo $loggedUser['credits']?>"><?php echo $loggedUser['credits']?></span>
		<span class="pocet_desc"> <?php echo __('kreditů');?></span>
	</div>
	<div class="col col-md-4">
		<strong><?php echo __('Chci zaplatit kredity ve výši');?></strong>
		<span class="relative">
			<span class="pocet_change plus red">+</span>
			<span class="pocet_change minus red">-</span>
		</span>
		<span class="pocet red" id="credit_use">0</span>
		<span class="pocet_desc red"> <?php echo __('kreditů');?></span>
	
	</div>
	<div class="col col-md-4">
		<strong><?php echo __('Za objednávku zaplatíte');?></strong>
		<span class="pocet shop_basket_total_price_vat"></span>
	
	</div>
</div>
<div class="clear"></div>
<?php echo $this->Form->input('credit_use',['type'=>'hidden']);?>
<?php echo $this->Form->input('credit_use_before',['type'=>'hidden','value'=>$loggedUser['credits']]);?>
<?php echo $this->Form->input('credit_use_after',['type'=>'hidden']);?>
</div>
<?php } ?>