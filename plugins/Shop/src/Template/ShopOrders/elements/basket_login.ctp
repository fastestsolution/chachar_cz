<?php if (!isset($loggedUser)){ ?>
		<div id="basket_login" class="none">
			<div class="ico"></div>
			<strong class="title"><?php echo __d('shop','Košík plný výhod!'); ?></strong>
			<p><?php echo __d('shop','Abychom Vám mohli <strong>objednávku rychle doručit</strong>, potřebujeme se s Vámi lépe seznámit...');?></p>
			<div class="row">
				<div class="col col-md-4 first">
					<?php echo $this->Html->link(__d('shop','Již se známe, mám účet'),'#',['title'=>__d('shop','Již se známe, mám účet'),'class'=>'btn btn-primary btn-green','id'=>'but_basket_login1']);?>
				</div>
				<div class="col col-md-4 middle">
					<?php echo $this->Html->link(__d('shop','Chci výhody a chci se registrovat'),'/'.KOSIK_LINK.'/',['title'=>__d('shop','Chci výhody a chci se registrovat'),'class'=>'btn btn-primary red','id'=>'but_basket_login2']);?>
					<span><?php echo __d('shop','> 20 kreditů = 20 Kč ZDARMA')?></span>
				</div>
				<div class="col col-md-4 last">
					<?php echo $this->Html->link(__d('shop','Nechci výhody, ani registraci'),'/'.KOSIK_LINK.'/',['title'=>__d('shop','Nechci výhody, ani registraci'),'class'=>'btn btn-primary btn-white','id'=>'but_basket_login3']);?>
				</div>
			</div>
		</div>
<?php } ?>