<?php if (!isset($loggedUser)){ ?>
<br />
<div class="alert alert-info" role="alert">
  <strong><?php echo __('Tip:');?></strong> <?php echo __d('shop', 'Pokud jste již dělali objednávku přihlašte se. <a href="#" id="move_to_login" class="btn btn-primary">Přihlásit se</a>');?>
</div>
<?php } ?>
