<div id="pridavky_modal_all" class="nonea">
<div class="modal_over"></div>
	
<div id="pridavky_modal">
	
	<div id="pridavky_modal_in" class="preloader" data-error="<?php echo __('Můžete odebrat jen 2 výchozí přídavky');?>">
		<div class="container">
			<div class="only_mobile fa fa-arrow-circle-down" id="scrollMobile"></div>
			<div class="row">
				<div class="col col-md-5">
					<h2 id="attribute_name"></h2>
					<p id="attribute_description"></p>
					<p id="palivost"></p>
					<div id="variants_adopt">
					
					</div>
					<div id="attribute_imgs">
						<div id="prumer" class=""></div>
						<div class="img lazy" id="attribute_img"></div>
					</div>
					<div id="own_pizza" class="none">
						<div class="tips filtr none" id="tip_own_pizza">
							<div class="bull"></div>
							<strong><?php echo __('Náš TIP!'); ?></strong>
							<p><?php echo __('Vyberte si dle své chuti...')?></p>
							<div class="tria"></div>
						</div>
						<?php //if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){ ?>
							<?php
							echo '<label>'.__d('shop','Vyberte si omáčku do základu').':</label>';
							echo '<ul class="sugo_list omacka">';
							foreach($sugo_list AS $k=>$s){ 
								if ($k > 0)
								echo '<li class="prid_select prid'.$k.'" data-el="sugo"  data-id="'.$k.'"><span>'.$s.'</span></li>';
							}
							echo '</ul>';
							?>
							<?php echo $this->Form->input('sugo',['label'=>false,'class'=>'own_select select_zaklad none','empty'=>false,'data-type'=>'zaklad_sugo']); ?>
							
							<?php
							echo '<div class="none">';
							echo '<label>'.__d('shop','Vyberte si sýr do základu').':</label>';
							echo '<ul class="sugo_list syr">';
							foreach($sugo_zaklad_list AS $k=>$s){ 
								if ($k > 0)
								echo '<li id="syrSelect'.$k.'" class="prid_select prid'.$k.'" data-el="zaklad" data-id="'.$k.'"><span>'.$s.'</span></li>';
							}
							echo '</ul>';
							?>
							<?php echo $this->Form->input('zaklad',['label'=>false,'class'=>'own_select select_zaklad none','empty'=>false,'data-type'=>'zaklad_syr']); ?>
							<?php echo '</div>'; ?>
						<?php 
						/*
						} else {?>
							<?php echo $this->Form->input('sugo',['label'=>__d('shop','Omáčka:'),'options'=>$sugo_list,'class'=>'own_select select_zaklad','empty'=>false,'data-type'=>'zaklad_sugo']); ?>
							<?php echo $this->Form->input('zaklad',['label'=>__d('shop','Sýr do základu:'),'options'=>$sugo_zaklad_list,'class'=>'own_select select_zaklad','empty'=>false,'data-type'=>'zaklad_syr']); ?>
						<?php } 
						*/
						?>
					</div>
					<div class="atr_price">
						<span class="title"><?php echo __('Celková cena za pizzu'); ?></span>
						<span id="attribute_price_total">200</span><span class="suf"><?php echo __('Kč'); ?></span>
						<div id="cara"></div>
					</div>
					<div class="atr_info">
					<strong><span><?php echo __('Upozornění');?>:</span> <?php echo __('Lze odebrat max. 2 suroviny, přídavky jsou neomezeny.'); ?></strong>
					<?php echo __('Vážený zákazníku, pokud odebíráte suroviny, tak se výsledná cena pizzy nemění.'); ?>
					<?php 
					if($system_id == 1){ 
						if (date('Y-m-d') < '2017-07-10'){
							echo '<br /><strong class="atr_alert">Pizzy s mozzarelou a veganem vozíme od 17.7. 2017</strong>';
						}
					}
					?>
					</div>
				</div>
				<div class="col col-md-7">
					<div id="attributes_list">
						<div class="mobile_hide" id="pridavky_mobile">
							<h2><?php echo __('Přidat přídavky');?> <span class="seo_txt"><?php echo __d('shop','na pizzu');?></span></h2>
							<div class="row">
							<?php foreach($products_attributes AS $group_id=>$group){?>
								<div class="attributes_item col col-xs-12 col-md-6 group<?php echo $group_id?>">
								<h2 class="group_title"><span class="seo_txt"><?php echo __d('shop','Přidavek na pizzu');?></span> <?php echo $product_attributes_group_list[$group_id] ?></h2>
								<ul>
								<?php foreach($group AS $g){ ?>
								
									<li class="attribute_item <?php echo (in_array($g->code,[671,672,673,674,675,676,677,678,679,680,681,682])?'none':'')?>" id="attribute<?php echo $g->id;?>" data-id="<?php echo $g->id;?>">
										<span class="plus point">+</span>
										<div class="active">
											<span class="count none">0</span>
											<span class="name"><?php echo $g->name;?></span>
											<span class="gramaz"><?php echo $g->gramaz.(($g->gramaz>=8)?'g':'ks');?></span>
											<span class="price" data-price="<?php echo $g->price_vat;?>"><?php echo $this->Fastest->price($g->price_vat);?></span>
										</div>
										<span class="minus  point none">-</span>
									</li>
								<?php } ?>
								</ul>
								</div>
							<?php } ?>
							<div class="attribute_button col col-xs-12 col-md-6">
								<?php echo $this->Html->link(__('Objednat s úpravami'),'#',['title'=>__('Objednat s úpravami'),'class'=>'chbut big margin-bottom SaveAttributes']); ?>
								<?php echo $this->Html->link(__('Objednat v původním složení'),'#',['title'=>__('Objednat bez úprav'),'class'=>'chbut grey SaveAttributes NoAddon own_pizza_hide']); ?>
							</div>		
							</div>
						</div>
						<div class="only_mobile" id="mobile_pridavky_active">
							<div class="attribute_button col col-xs-12 col-md-6">
								<?php echo $this->Html->link(__('Upravit přídavky'),'#',['title'=>__('Upravit přídavky'),'class'=>'chbut','id'=>'UpravitPridavky']); ?>
								<?php echo $this->Html->link(__('Objednat v původním složení'),'#',['title'=>__('Objednat bez úprav'),'class'=>'chbut grey SaveAttributes NoAddon own_pizza_hide']); ?>
							
							</div>
						</div>
					</div>
					
				
				</div>
			</div>
		</div>
	</div>
	<div id="pridavky_modal_close"></div>
</div>
</div>