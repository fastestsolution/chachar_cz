<?php 
$this->Html->templates([
	'link' => '<a href="'.$_SESSION['url_lang_prefix'].'{{url}}"{{attrs}}>{{content}}</a>'
]);
//pr('a');
$new_address = 1;
//pr($provoz_list);
if (isset($provoz_list)){
if (isset($new_address)){
if (count($provoz_list) == 1){
	$simpleProvoz = true;
	//$only_once = true;
}
echo '<div id="mobile_provoz" class="close_el">';
	echo '<div id="mobile_provoz_show" class="only_mobile"><i class="fa fa-phone" aria-hidden="true"></i>
 Telefonická objednávka / Otevírací doba <span class="fa fa-angle-down"></span></div>';
	echo '<div id="mobile_provoz_content">';	
		echo '<div id="provoz_list_info_search" class="'.(isset($only_once)?'none':'').'">';
			echo '<div class="row">';
				echo '<span class="ico mobile">&nbsp;</span>';
				echo '<span class="tel_title">'.__('Chcete nám zavolat?');
				echo '<span class="tel_title2">'.__('Zadejte adresu kam pojedeme...').'<br /> (ulice c.p., mesto)</span>';
				//print_r($_SERVER['REMOTE_ADDR']);
					
				echo '</span>';	
				echo '<div class="tel_desc">';
					
					echo '<span class="tel">';
					echo '<div id="search_address_tip2" class="none search_address_tip">'.__('Prosím, zadejte presnou adresu - napr. Pelclova 2500, Ostrava').'</div>';
					echo '<div class="address_clear fa fa-close none"></div>';
					$ips = ['89.103.18.65z','109.81.209.135a','46.135.76.210'];
					//print_r('a');
					if (!in_array($_SERVER['REMOTE_ADDR'],$ips)){ 
						echo '<div class="relative">';
						echo '<div id="adr_preloader" class="addressPreloader none"></div>';
						echo $this->Form->input('search_address_tel',['class'=>'form-control google_autocomplete2 searchAddress clear_address addressInput','data-button'=>'ShowProvozBtn','data-preloader'=>'adr_preloader','label'=>false,'placeholder'=>'napr. Nádražní 6, Ostrava']);
						echo '</div>';
						
					} else {
					echo $this->Form->input('search_address_tel',['class'=>'form-control google_autocomplete ','label'=>false,'placeholder'=>'Zadejte Vaši adresu']);
					
					}
					echo '</span>';
					if (!in_array($_SERVER['REMOTE_ADDR'],$ips)){
						echo $this->Form->button(((isset($simpleProvoz))?'Použít adresu':'Zobraz kontakt'),['class'=>'btn btn-primary ','label'=>false,'id'=>'ShowProvozBtn']);
				
					}
				echo '</div>';
				echo '<div id="tel_provoz_id"></div>';
			echo '</div>';
		echo '</div>';

echo '<div class="row new" id="provoz_list">';

		
	echo '<div id="provoz_list_info" class="new">';
	echo '<div id="jsonProvoz" class="none">'.json_encode($provoz_list).'</div>';
	foreach($provoz_list AS $provoz_cnt=>$p){
	
		echo '<div id="provoz_info_id_'.$p->id.'" class="provoz_info provoz_info_id'.$p->id.' " data-id="'.$p->id.'">';
			echo '<div class="row">';
				echo '<span class="provoz_name">'.$p->name.'</span>';
				echo '<span class="ico mobile">&nbsp;</span>';
				echo '<div class="tel_desc">';
					echo __('Telefon');
					echo '<span class="tel"><a data-name="'.$p->name2.'" class="tel_click" href="tel:'.strtr($p->telefon,[' '=>'']).'">'.$p->telefon.'</a></span>';
				echo '</div>';
			echo '</div>';
			echo '<div class="row">';
				
				$doba = explode("\n",$p->doba);
				$week_day = date('w') - 1;
				$week_list = [
					0=>__('Po'),
					1=>__('Út'),
					2=>__('St'),
					3=>__('Ct'),
					4=>__('Pá'),
					5=>__('So'),
					6=>__('Ne'),
				];
				//pr($p->doba);
				echo '<span class="ico open">&nbsp;</span>';
				echo '<div class="tel_desc">';
					echo __('Dnes otevreno');
					echo '<span class="tel">'.(isset($doba[$week_day])?$doba[$week_day]:'').'</span>';
					echo '<a class="open_more_date" href="#">'.__('Další dny').' <span class="fa  fa-angle-down">&nbsp;</span></a>';
					echo '<div class="clear"></div>';
					echo '<div class="more_date none">';
						foreach($week_list AS $k=>$w){
							echo '<div class="row">';
								echo '<div class="col col-xs-2">';
									echo $w.':';
								echo '</div>';
								echo '<div class="col col-xs-9">';
									echo (isset($doba[$k])?$doba[$k]:'');
								echo '</div>';
							echo '</div>';
						}
					echo '</div>';
				echo '</div>';
			echo '</div>';
			echo '<strong><span class="seo_txt">Pizzu </span>'.__('Rozvážíme').'</strong>';
			echo '<p class="lokalita">'.$p->lokalita.'</p>';
		echo '</div>';
	}
	echo '</div>';
echo '</div>';
echo '<div class="text_center '.(isset($only_once)?'none':'').'">';
if (!isset($simpleProvoz))
echo $this->Html->link(__d('shop','Otevírací doba provozoven'),'#',['title'=>__d('shop','Otevírací doba'),'id'=>'oteviraci_doba']);
echo '</div>';
echo '<div class="mobile_line">';


echo '</div>';
echo '</div>';


echo '<div  class="no_mobile">';
echo '<ul id="sidemenu">';
		echo '<li>'. $this->Html->link(__d('shop','100% spokojenost'),'/100-spokojenost/',['title'=>__d('shop','100% spokojenost')]) .'</li>';
		echo '<li>'. $this->Html->link(__d('shop','Obchodní podmínky'),'/obchodni-podminky/',['title'=>__d('shop','Obchodní podmínky')]) .'</li>';
		echo '<li>'. $this->Html->link(__d('shop','Vernostní program'),'/vernostni-program/',['title'=>__d('shop','Vernostní program')]) .'</li>';
		echo '<li>'. $this->Html->link(__d('shop','Žádost o zamestnání'),'/zadost-o-zamestnani/',['title'=>__d('shop','Žádost o zamestnání')]) .'</li>';
		echo '<li>'. $this->Html->link(__d('shop','Kontakt'),'/kontakt/',['title'=>__d('shop','Kontakt')]) .'</li>';
	echo '</ul>';
	echo $this->element('layout/copyright');
echo '</div>';


echo '<div id="oteviraci_doba_win" class="none">';
	echo '<div class="over"></div>';
	echo '<div class="content">'; 
	echo '<div id="close_doba"></div>';
	
	//if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
			echo '<div id="provoz_doba_list">';
				foreach($provoz_list AS $provoz_cnt=>$p){
					//pr($provoz_cnt);
					echo '<strong class="'.(($provoz_cnt == 0)?'active':'').'" data-id="'.$p->id.'">'.$p->name.'</strong>';
				}
			echo '</div>';
			
			echo '<div id="provoz_doba_element">';
				foreach($provoz_list AS $provoz_cnt=>$p){
					echo '<div class="doba_el doba_el'.$p->id.' '.(($provoz_cnt == 0)?'':'none').'">';
					$doba = explode("\n",$p->doba);
					$week_day = date('w') - 1;
					$week_list = [
						0=>__('Po'),
						1=>__('Út'),
						2=>__('St'),
						3=>__('Ct'),
						4=>__('Pá'),
						5=>__('So'),
						6=>__('Ne'),
					];
				
					echo '<strong data-id="'.$p->id.'">'.$p->name.' - otevírací doba</strong>';
			
					foreach($week_list AS $k=>$w){
						echo '<span class="week_day_item '.(($k==$week_day)?'active':'').'">';
							echo '<span>'.$w.': </span>';
							echo (isset($doba[$k])?$doba[$k]:'');
						echo '</span>';
					}
					echo '</div>';
				}
				
			echo '</div>';
	//} else {
	/*
	foreach($provoz_list AS $provoz_cnt=>$p){
		$doba = explode("\n",$p->doba);
		$week_day = date('w') - 1;
		$week_list = [
			0=>__('Po'),
			1=>__('Út'),
			2=>__('St'),
			3=>__('Ct'),
			4=>__('Pá'),
			5=>__('So'),
			6=>__('Ne'),
		];
		
		echo '<div class="row">';
			echo '<div class="col col-md-2">';
				echo '<strong>'.$p->name2.'</strong>';
			echo '</div>';
			echo '<div class="col col-md-10">'; 
				foreach($week_list AS $k=>$w){
					echo '<span class="week_day">';
						echo $w.': ';
						echo (isset($doba[$k])?$doba[$k]:'');
					echo '</span>';
				}
			echo '</div>';
			echo '<br class="clear" />';
		echo '</div>';
		
	}
	*/
	//}
	
	echo '</div>';
echo '</div>';
		
} 

else {	



echo '<div class="row" id="provoz_list">';
	if (isset($_COOKIE['provoz_id'])){
		$provoz_id = $_COOKIE['provoz_id'];
	}
	if (isset($this->request->query['provoz'])){
		$provoz_id = h($this->request->query['provoz']);
	}
	//pr($provoz_id);
	//if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
	//if (isset($this->request->query['addr'])){	
		if (isset($provoz_id)){
			$active_provoz = $provoz_id;
		}
	//}
	//echo $this->Html->link(__d('shop','Vyberte provozovnu'),'#',['title'=>__d('shop','Vyberte provozovonu'),'class'=>'btn btn-default no_mobile','id'=>'selectProvoz']);
	echo '<div id="provoz_select_list" class="no_mobile clearfix '.(isset($active_provoz)?'none':'').'">';
	foreach($provoz_list AS $i=>$p){
		echo '<div class="col col-xs-6 '. (($i++ % 2) ? "even" : "odd").'" >';
			//pr($p->name2);
			echo '<span id="provoz'.$p->id.'" class="name provoz_save provoz_select '.((isset($provoz_id) && $p->id == $provoz_id)?'active':'').'" data-id="'.$p->id.'">';
				echo $p->name2;
			echo '</span>';
		echo '</div>';
		if ($p->id == 10){
			echo '<div class="sep"></div>';
		}
	}
	echo '</div>';
	if (isset($active_provoz)){
		foreach($provoz_list AS $p){
			if ($p->id == $active_provoz){
				echo '<div id="h2_provoz_name_inject"><h2 id="h2_provoz_name">Zmenit provozovnu: <strong id="change_provoz">'.$p->name2.'</strong></h2></div>';
			}
		}
		//'<h2>'..'</h2>';
	} else {
		echo '<div id="h2_provoz_name_inject"></div>';
			
	}
	
	
	
	echo '<ul id="provoz_select_list_mobile" class="only_mobile close_mobile ">';
	echo '<li class="first" >'.__('Vyberte si svou provozovnu').'<span class="fa fa-angle-down">&nbsp;</span></li>';
	foreach($provoz_list AS $p){
		//pr($provoz_id);
		echo '<li id="provoz_mobile'.$p->id.'" class="mobile_provoz_name provoz_select_mobile provoz_save '.((isset($provoz_id) && $p->id == $provoz_id)?'active':'').'" data-id="'.$p->id.'">';
			//pr($p->name2);
			$doba = explode("\n",$p->doba);
			$week_day = date('w') - 1;
			
			echo '<span class="mobile_name">'.$p->name2.'</span>';
			echo '<div class="selected">';
				
				echo '<a  href="tel:'.strtr($p->telefon,[' '=>'']).'" class="tel tel_click" data-name="'.$p->name2.'">'.$p->telefon.'</a>';
				echo '<span class="doba">'. (isset($doba[$week_day])?$doba[$week_day]:'').'</span>';
				
			echo '</div>';
		echo '</li>';
	}
	echo '</ul>';
	echo '<div class="clear"></div>';
	echo '<div id="provoz_list_info" class="no_mobile">';
	foreach($provoz_list AS $provoz_cnt=>$p){
	
		echo '<div class="provoz_info provoz_info_id'.$p->id.' '.(($provoz_cnt > 0)?'none':'').'">';
			echo '<div class="row">';
				echo '<span class="ico mobile">&nbsp;</span>';
				echo '<div class="tel_desc">';
					echo __('Chcete nám zavolat');
					echo '<span class="tel"><a data-name="'.$p->name2.'" class="tel_click" href="tel:'.strtr($p->telefon,[' '=>'']).'">'.$p->telefon.'</a></span>';
				echo '</div>';
			echo '</div>';
			echo '<div class="row">';
				
				$doba = explode("\n",$p->doba);
				$week_day = date('w') - 1;
				$week_list = [
					0=>__('Po'),
					1=>__('Út'),
					2=>__('St'),
					3=>__('Ct'),
					4=>__('Pá'),
					5=>__('So'),
					6=>__('Ne'),
				];
				//pr($p->doba);
				echo '<span class="ico open">&nbsp;</span>';
				echo '<div class="tel_desc">';
					echo __('Dnes otevreno');
					echo '<span class="tel">'.(isset($doba[$week_day])?$doba[$week_day]:'').'</span>';
					echo '<a class="open_more_date" href="#">'.__('Další dny').' <span class="fa  fa-angle-down">&nbsp;</span></a>';
					echo '<div class="clear"></div>';
					echo '<div class="more_date none">';
						foreach($week_list AS $k=>$w){
							echo '<div class="row">';
								echo '<div class="col col-xs-2">';
									echo $w.':';
								echo '</div>';
								echo '<div class="col col-xs-9">';
									echo (isset($doba[$k])?$doba[$k]:'');
								echo '</div>';
							echo '</div>';
						}
					echo '</div>';
				echo '</div>';
			echo '</div>';
			echo '<strong><span class="seo_txt">Pizzu </span>'.__('Rozvážíme').'</strong>';
			echo '<p class="lokalita">'.$p->lokalita.'</p>';
		echo '</div>';
	}
	echo '</div>';
	echo '<ul id="sidemenu">';
		echo '<li>'. $this->Html->link(__d('shop','100% spokojenost'),'/100-spokojenost/',['title'=>__d('shop','100% spokojenost')]) .'</li>';
		echo '<li>'. $this->Html->link(__d('shop','Obchodní podmínky'),'/obchodni-podminky/',['title'=>__d('shop','Obchodní podmínky')]) .'</li>';
		echo '<li>'. $this->Html->link(__d('shop','Vernostní program'),'/vernostni-program/',['title'=>__d('shop','Vernostní program')]) .'</li>';
		echo '<li>'. $this->Html->link(__d('shop','Žádost o zamestnání'),'/zadost-o-zamestnani/',['title'=>__d('shop','Žádost o zamestnání')]) .'</li>';
		echo '<li>'. $this->Html->link(__d('shop','Kontakt'),'/kontakt/',['title'=>__d('shop','Kontakt')]) .'</li>';
	echo '</ul>';
	echo $this->element('layout/copyright');
echo '</div>';
}
}

?>
<?php //pr($products); ?>