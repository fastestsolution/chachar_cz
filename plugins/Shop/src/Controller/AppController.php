<?php

namespace Shop\Controller;

use App\Controller\AppController as BaseController;
use Cake\Event\Event;
use Cake\Cache\Cache;

class AppController extends BaseController
{
	
	public function beforeFilter(Event $event){
		$this->Session = $this->request->session();
		// shop global session
		$this->ShopSession();
		return parent::beforeFilter($event);
	}
	
	public function initialize(){
		parent::initialize();
		if(!$this->request->is("ajax")){
        	
        	//$this->loadShopProducts();
        }
		
	}
	
	
	
	
	// shop session
	private function ShopSession(){
		// clear basket products
		//$this->Session->delete('basket_products');
		
		$this->set('basket_products',$this->basket_products = $this->Session->read('basket_products'));
		//pr($this->basket_products);
	}
	
	
	// shop products
	private function loadShopProducts(){
		$this->loadModel('ShopProducts');
		$shopProducts = $this -> ShopProducts -> find()
		->where(['trash'=>0])
		->toArray()
		;
		//pr($shopProducts);
	}
}
