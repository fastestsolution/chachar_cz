<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace Shop\Controller;

use Shop\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\I18n\Time;
use lessc\lessc;
use Cake\Cache\Cache;
use Cake\Utility\Hash;
//use Cake\Cache\Cache;
define('DJ_API_URL','djApi');
class ShopDjApisController extends AppController
{		
	
	var $errorList = [ 
	    'TE0'=>'Internal server error.',
		'TE1'=>'The access token is missing.',
		'TE2'=>'The access token is invalid.',
		'TE3'=>'The request body is unparsable (not a json, trailing garbage etc.).',
		'TE4'=>'The request body is invalid (does not adhere to the defined json-schema).',
		'TE5'=>'Not found (should be accompanied by HTTP 404 status code).',
		'TE6'=>'Method not allowed (should be accompanied by HTTP 405 status code).',
		'TE7'=>'Parameters in URL are unparsable.',
	];
	
	function newOrder(){
		//pr($_POST);
		$this->jsonData = $_POST;
		
		$headers = apache_request_headers();
		//print_r($headers);
		
		if (!isset($headers['Authorization'])){
			$this->getError('TE1');
		}
		
		$this->attributes_list = $this->attributesList();
		$this->products_list = $this->productsList();
		$this->client = $this->parseClient();
		$this->orders = $this->parseOrders();
		
		
		
		//print_r($this->client);
		//print_r($this->orders);
		$this->loadModel('Shop.ShopOrders');
				
		if (!empty($this->orders)){
			foreach($this->orders AS $order){
				$order['user'] = $this->client;
				$save = $this->ShopOrders->newEntity($order,['associated' => ["Users"=>['validate'=>'OnlyCheckPass','accessibleFields'=>['id' => true]], "Users.UserAddresses","ShopOrderItems"]]);
				pr($save);
			}
		}
		//print_r($this->jsonData);
		
		if (!isset($this->jsonData['restaurant_id'])){
			$this->getError('TE0');
		}
		
		$result = ['result'=>true];
		
		die(json_encode($result));
	}
	
	/**
	* parsovani orders
	*/
	
	private function parseOrders(){
		$orders = [];
		if (isset($this->jsonData['orders'])){
			
			foreach($this->jsonData['orders'] AS $order){
				$order_items = [];
				
				$total_price_vat = 0;
				$total_price = 0;
				foreach($order['items'] AS $item){
					$item['code'] = '001A';
					$item['price'] = $item['price']['amount'] / $item['count'];
					//print_r($this->products_list);die();
					
					$attributes_ids = null;
					
					if (isset($item['additions'])){
						foreach($item['additions'] AS $ad){
							$ad['code'] = 610;
							$attributes_ids[$this->attributes_list[$ad['code']]['id']] = $ad['count'];
							//print_r($this->attributes_list[$ad['code']]);die();
						}
					
					}
					$total_price_vat += $item['price'];
					$total_price += $item['price'] - $item['price']*DPH_CON;
					
					$order_items[] = [
						'shop_product_id'=>$this->products_list[$item['code']]['id'],
						'code'=>$item['code'],
						'name'=>$this->products_list[$item['code']]['name'],
						'price_per_item'=>$item['count']*($this->products_list[$item['code']]['price'] - $this->products_list[$item['code']]['price']*DPH_CON),
						'price_vat_per_item'=>$item['count']*$this->products_list[$item['code']]['price'],
						//'price'=>$products_list[$p['code']]['price'] - $products_list[$p['code']]['price']*DPH_CON,
						//'price_vat'=>$products_list[$p['code']]['price'],
						'price'=>$item['price'] - $item['price']*DPH_CON,
						'price_vat'=>$item['price'],
						'count'=>$item['count'],
						'attributes_ids'=>$attributes_ids,
						'tax_id'=>0,
					];
				}
				$orders[] = [
					//'shop_provoz_id'=>'',
					'createdTime' => strtotime(date('Y-m-d H:i:s')),
					'system_id'=>$this->system_id,
					'zdroj_id'=>3,
					'reg'=>0,
					'price'=>$total_price,
					'price_vat'=>$total_price_vat,
					//'shop_payment_id'=>1,
					'note'=>$order['note'],
					'lat'=>$this->client['user_addresses'][0]['lat'],
					'lng'=>$this->client['user_addresses'][0]['lng'],
					'client_name'=>$this->client['first_name'],
					'client_telefon'=>$this->client['phone'],
					'dj_id'=>$order['order_id'],
					'order_items'=>$order_items,
				];
				
				//print_r($order);
			}
		}
		return $orders;
	}
	
	
	/**
	* attributes list
	*/
	
	private function attributesList(){
		$this->loadModel('Shop.ShopProductAttributes');
			$query = $this->ShopProductAttributes->find()
			  ->where(['code IS NOT'=>null])
			  ->select([
				'id',
				'name',
				'price_vat',
				'group_id',
				'code',
			  ])
			  ->order('name ASC')
			 
			  ;
			$load = $query->toArray();
			//pr($load);
			$json = array();
			foreach($load AS $l){
				$json[$l->code] = $l;
			}
			//pr($json); 
		return $json;	
	}
	
	
	/**
	* products list
	*/
	private function productsList(){
		$this->loadModel('Shop.ShopProducts');
		$mapper = function ($row, $key, $mapReduce) {
				// map group id from category
				$row->group_id = (isset($this->menu_data['shop_menu_items_group_id'][$row->shop_category_id])?$this->menu_data['shop_menu_items_group_id'][$row->shop_category_id]:'');
				
				
				// map varitanty
				if (isset($row->shop_product_variants) && !empty($row->shop_product_variants)){
					if (!isset($variants_list)){
						$variants_list = [];	
						$variants_select_list = [];	
					}
					foreach($row->shop_product_variants AS $var){
						$variants_list[$var->id] = $var;
						$variants_select_list[$var->id] = [
							'id'=>$var->id,
							'name'=>$var->name,
							'price'=>$var->price,
							'price_vat'=>$var->price_vat,
						];
					}
					$row->variants_list = $variants_list;
					$row->variants_select_list = $variants_select_list;
				}
				
				if (isset($row->shop_product_prices) && isset($row->shop_product_prices[0])){
						$row->price_vat  = $row->shop_product_prices[0]['price_vat'];
					
				} else {
					unset($row);
				}
				if (isset($row))
				$mapReduce->emit($row);
			};
		
		$query = $this->ShopProducts->find()
		  ->contain([
					'ShopProductVariants'=>function($q){
						$q->select(['id','shop_product_id','price_vat','code','name']);
						$q->where(['system_id'=>$this->system_id]);
						return $q;
					},
					'ShopProductPrices'=>function($q){
						//$q->select(['id','shop_product_id','price_vat','code','name']);
						$q->where(['system_id'=>$this->system_id,'status'=>1]);
						return $q;
					}
		  ])
		  ->where([])
		  ->select([
		  ])
		  ->mapReduce($mapper);
		  ;
		$data = $query->toArray();
		//print_r($data);
		$products_list = [];
			
		foreach($data AS $d){
			
			if (isset($d->shop_product_variants) && !empty($d->shop_product_variants)){
				foreach($d->shop_product_variants AS $v){
					$products_list[$v->code] = [
						//'id'=>$d->id.'v'.$v->id,
						'id'=>$d->id,
						'code'=>$v->code,
						'name'=>$d->name.' '.$v->name,
						'price'=>$v->price_vat,
					];
					
				}
			} else {
			
				$products_list[$d->code] = [
					'id'=>$d->id,
					'code'=>$d->code,
					'name'=>$d->name,
					'price'=>$d->price_vat,
				];
			}
		}
		//$products_list = array_merge($products_list,$this->cinajede(true));
		//$products_list = $products_list+$this->cinajede(true);
		//print_r($this->cinajede(true));

		return $products_list;
	}
	
	/**
	parsovani dat client
	*/
	private function parseClient(){
		$phone = substr($this->jsonData['customer']['phone_number'],-9,20);
		$phone_pref = strtr(substr($this->jsonData['customer']['phone_number'],0,4),['+'=>'00']);
		// client	
		$client_new =  [
			'first_name'=>$this->jsonData['customer']['name'],
			'last_name'=>'X',
			'email'=>(isset($this->jsonData['customer']['email'])?$this->jsonData['customer']['email']:''),
			'system_id'=>$this->system_id,
			'reg'=>0,
			'phone'=>$phone,
			'phone_pref'=>$phone_pref,
			'zdroj_id'=>3,
		];
		
		if (isset($this->jsonData['customer']['email'])){
			$client = $this->findClient($this->jsonData['customer']['email'],$user_new);
		}
		//pr($user);
		if (!isset($client)){
			$client = $client_new;
		} else {
			$client = array_merge($client,$client_new);
			unset($client['password']);			
		}
		
		
		// adresa
		$user_addresses = [];
		$match = preg_match("/(.*?) ((\d+)\/)?(\d+), (.*)/", $this->jsonData['customer']['delivery_address'], $matches, PREG_OFFSET_CAPTURE);
		$adr = [];
			if ($match == 1){
				$adr['street']= $matches[1][0];
				$adr['cp'] = $matches[3][0];
				if (!empty($matches[3][0])){
					$adr['co'] = $matches[4][0];
				} else {
					$adr['cp'] = $matches[4][0];
					$adr['co'] = '';
				}
				$adr['street'] = $adr['street'].' '.$adr['cp'].(!empty($adr['co'])?'/'.$adr['co']:'');
				$city = $matches[5][0];
				$city = explode(' ',$city);
				$cityCount = count($city);
				
				$adr['zip'] = $city[$cityCount-1];
				unset($city[$cityCount-1]);
				
				$adr['city'] = implode(' ',$city);
			}
			
		//print_r($adr);die();
		
		
		$address = $this->jsonData['customer']['delivery_address']; // Google HQ
        $prepAddr = str_replace(' ','+',$address);
        $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
        $output= json_decode($geocode);
		if (isset($output->results[0]->geometry->location->lat)){
			$lat = $output->results[0]->geometry->location->lat;
			$lng = $output->results[0]->geometry->location->lng;
			
		} else {
			$lat = '';
			$lng = '';
		}
		// find address if exist
		$client_addresses[0] = $this->findAddress($adr['street'],$adr['city'],$adr['cp'],(isset($client['id'])?$client['id']:''));
		//print_r($client_addresses);
		if (empty($client_addresses[0])){
			$client_addresses[0] = [
				'shop_address_type_id'=>1,
				'street'=>$adr['street'],
				'cp'=>$adr['cp'],
				'city'=>$adr['city'],
				'lat'=>$lat,
				'lng'=>$lng,
			];
		} else {
			$client_address[0]['lat'] = $lat;
			$client_address[0]['lng'] = $lng;
		}
		$client['user_addresses'] = $client_addresses;
		
		
		return $client;
	}
	
	
	/**
	* error api log
	*/
	function getError($code){
		$this->logEmail($code);
		$result = [
			'error'=>[
				'code'=>$code,
				'message'=>$this->errorList[$code],
			],
		];
		
		die(json_encode($result));
	}
	
	/**
	* find client address exist
	*/
	private function findAddress($street,$city,$cp,$user_id=null){
		$this->loadModel('Shop.ShopClientAddresses');
		$conditions = ['street'=>$street.'a','city'=>$city,'cp'=>$cp];
		if ($user_id != null){
			$conditions['shop_client_id'] = $user_id;
		}
		$query = $this->ShopClientAddresses->find()
		  ->where($conditions)
		  ->select([
		  ])
		  ->hydrate(false)
		  ;
		$data = $query->first();
		//print_r($data);
		
		return $data;
	}
	
	/**
	* find client by email
	*/
	private function findClient($email){
		$this->loadModel('Shop.ShopClients');
		$query = $this->ShopClients->find()
		  ->where(['email'=>$email])
		  ->select([
		  ])
		  ->hydrate(false)
		  ;
		$data = $query->first();
		
		return $data;
	}
	
	/**
	* poslat email na fastest o chybe
	*/
	function logEmail($code,$value=null){
		
		//print_r($this->msg_value);
			
			
			$opt = [
				'to'=>'test@fastest.cz',
				'template_id'=>5,
				'data'=>[
					'result_name'=>$this->errorList[$code],
					'result'=>$code,
					'result_value'=>$this->errorList[$code],
					'data'=>json_encode($this->jsonData),
				],
			];
		//	pr($opt);
			$this->loadComponent('Email');
			$this->Email->send($opt);
			//die('aa');
		
	}
	
	
	function testData(){
		
		$json = '
		{
  "order_id": "abcd-123",
  "restaurant_id": "55af539409393f3d00000001",
  "delivery_time": "2017-01-01T10:00:00+01:00",
  "delivery_on_time": false,
  "takeaway": false,
  "customer": {
    "name": "John Smith",
    "delivery_address": "Lihovarská 1060/12, Praha 19000",
    "phone_number": "+420123456789"
  },
  "orders": [
    {
      "order_id": "abcd-123",
      "name": "John Smith",
      "already_paid": true,
      "payment_method": "card",
      "note": "",
      "items": [
        {
          "external_id": "f-123-456",
          "count": 2,
          "price": {
            "amount": 500,
            "currency_code": "CZK"
          },
          "additions": [
            {
              "external_id": "a-777",
              "count": 1,
              "price": {
                "amount": 100,
                "currency_code": "CZK"
              }
            }
          ]
        },
        {
          "external_id": "p-852",
          "count": 2,
          "price": {
            "amount": 20,
            "currency_code": "CZK"
          }
        }
      ]
    },
    {
      "order_id": "abcd-124",
      "name": "John Doe",
      "already_paid": false,
      "payment_method": "meal_voucher",
      "note": "Ten šopák bych prosil bez oliv",
      "items": [
        {
          "external_id": "f-987-654",
          "count": 1,
          "price": {
            "amount": 100,
            "currency_code": "CZK"
          }
        },
        {
          "external_id": "p-46",
          "count": 1,
          "price": {
            "amount": 10,
            "currency_code": "CZK"
          }
        }
      ]
	}
	]
	}
	';
	$json = json_decode($json,true);
	return $json;
	}
	
	function test(){
		$json = $this->testData();
		$post = $json;
		//pr($json);
		
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPHEADER, [
			'Authorization:Token foo123-456-789'
		]);
		curl_setopt($ch, CURLOPT_URL, 'https://'.$_SERVER['HTTP_HOST'].'/'.DJ_API_URL.'/newOrder/');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec ($ch);
		pr($result);
		curl_close ($ch);
		die();		
	}
	
}
