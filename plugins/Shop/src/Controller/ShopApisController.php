<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace Shop\Controller;

use Shop\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\I18n\Time;
use lessc\lessc;
use Cake\Cache\Cache;
use Cake\Utility\Hash;
//use Cake\Cache\Cache;

class ShopApisController extends AppController
{		
	public function deliveryOrders($pokladna_id){
		//print_r($this->request->query['new']);
		//if ($this->request->is("ajax")){
		$date_from = date('Y-m-d 00:00:00');
		$date_to = date('Y-m-d 23:59:59');
		$date_from = strtotime($date_from);
		$date_to = strtotime($date_to);
			
		if (isset($this->request->query['new'])){
			$this->loadModel('Shop.ShopProvozs');
			$provoz_list_all = $this->ShopProvozs->provozListAll(true);
			$pokladna_list = $this->ShopProvozs->pokladnaList();
			$pokladna_id_new = $pokladna_list['list'][$pokladna_id];
			//print_r($pokladna_id);
		}
			
			$this->autoRender = false;
				$this->viewBuilder()->autoLayout(false);
				$this->loadModel('GpsDispecers');
				$mapper = function ($row, $key, $mapReduce) {
					if (!empty($row->update_gps))
					$row->update_gps = $row->update_gps->format('d.m. H:i');
					$row->count_orders = count($row->mobile_orders);
					unset($row->mobile_orders);
					$mapReduce->emit($row);
				};
				$this->con_orders = [
					'MobileOrders.rozvozce_date_nevylozeni IS' => null,
					'MobileOrders.rozvozce_date_vylozeni IS' => null,
					'MobileOrders.created_time >='=>$date_from,
					'MobileOrders.created_time <'=>$date_to,
			
					//'date(MobileOrders.created)' => date('Y-m-d')
				];
				$conDis = [
					'lat IS NOT'=>null,
					'pokladna_id'=>(isset($pokladna_id_new)?$pokladna_id_new:$pokladna_id),
					'date(update_gps)'=>date('Y-m-d'),
				];
				
				$this->loadModel('Rozvozces');
				$rozvozces_code = $this->Rozvozces->find('list', [
						'keyField' => 'rozvozce_id',
						'valueField' => 'code'
					])
					->where(['pokladna_id'=>(isset($pokladna_id_new)?$pokladna_id_new:$pokladna_id)])
					->toArray();
				//print_r($rozvozces_code);
				//pr($conDis);
				$query = $this->GpsDispecers->find()
				  ->where($conDis)
					->contain(['MobileOrders'=>function(\Cake\ORM\Query $q) {
						return $q->where($this->con_orders)->select(['id','rozvozce_id']);
					}])
					/*
					->matching('MobileOrders', function(\Cake\ORM\Query $q) { 
						//return $q;
						return $q->where($this->con_orders);
					})
					*/
					->select([
					
						'id',
						'pokladna_id',
						'rozvozce_name',
						'rozvozce_id',
						'lat',
						'lng',
						'update_gps',
						
					])
					
					->mapReduce($mapper);
				  ;
				  
				$find_rozvoces_load = $query->toArray();
				$find_rozvoces = [];
				if ($_SERVER['REMOTE_ADDR'] == '46.135.100.227'){
					//pr($find_rozvoces_load);
					//print_($find_rozvoces_load);
					//die();
				}
				foreach($find_rozvoces_load AS $k=>$d){ 
					//print_r($rozvozces_code[$d->rozvozce_id]);
					$find_rozvoces[$rozvozces_code[$d->rozvozce_id]] = $d;
				}
				
				if (isset($this->request->query['debug']))
				pr($find_rozvoces);
				if ($find_rozvoces){
					die(json_encode(['result'=>true,'data'=>$find_rozvoces]));
				} else {
					die(json_encode(['result'=>false]));
				
				}
				//$this->set('find_rozvoces',$find_rozvoces);
			//}
	}
	function deliveryOrders2($provoz_id){
		
		$this->loadModel('MobileOrders');
		$this->loadModel('MobileOrders');
		$conditions = [
			'date(created)'=>date('Y-m-d'),
			'shop_provoz_id'=>$provoz_id,
			'MobileOrders.stav_id NOT IN' => [4,5],
		];
		
		$dataMobileLoad = $this->MobileOrders->find()
		  ->where($conditions)
		  ->select([
			'id',
			'rozvozce_id'
		  ])
		  ->order('id DESC')
		  ->limit(20)
		  ->toArray()
		  ;
		  //pr($dataMobileLoad);
		if(!$dataMobileLoad){
			die(json_encode(['result'=>false]));
		}
		
		foreach($dataMobileLoad AS $m){
			
			$dataMobile[$m->rozvozce_id]['ids'][] = $m->id; 
		}
		foreach($dataMobile AS $k=>$d){
			$dataMobile[$k] = count($d['ids']);
		}
		//pr($dataMobile);
		die(json_encode(['result'=>true,'data'=>$dataMobile]));
	}
	
	
	function mobileOrders($provoz_id_array){
		
		$this->loadModel('MobileOrders');
			
			$date_from = date('Y-m-d 00:00:00');
			$date_to = date('Y-m-d 23:59:59');
			$date_from = strtotime($date_from);
			$date_to = strtotime($date_to);
		$conditions = [
			'MobileOrders.created_time >='=>$date_from,
			'MobileOrders.created_time <'=>$date_to,
			
			//'date(created)'=>date('Y-m-d'),
			'shop_provoz_id IN'=>$provoz_id_array,
			'MobileOrders.stav_id NOT IN' => [4,5],
		];
		
		$dataMobileLoad = $this->MobileOrders->find()
		  ->where($conditions)
		  ->select([
			
		  ])
		  ->order('id DESC')
		  ->limit(20)
		  ->toArray()
		  ;
		
		$dataMobile = [];  
		//pr($dataMobileLoad);
		foreach($dataMobileLoad AS $m){
			
			$dataMobile[$m->rozvozce_id]['ids'][] = $m->id; 
		}
		//$dataMobile = Hash::extract($dataMobileLoad, '{n}.rozvozce_id','{n}.rozvozce_id');  
		
		return($dataMobile);
	}
	
	function exportOrder($provoz_id,$order_id=null){
		$this->loadModel('Shop.ShopProvozs');
		$provoz_list_all = $this->ShopProvozs->provozListAll(true);
		$pokladna_list = $this->ShopProvozs->pokladnaList();
		//pr($pokladna_list);
		
		if (isset($this->request->query['newPokladna'])){
			$provoz_id = $pokladna_list['provoz_list'][$provoz_id];
		}
		//pr($provoz_id);
		//pr($this->system_id);
		
		if (!isset($provoz_list_all['provoz_api_ids'][$provoz_id])){
			die('spatny provoz');
			
		}
		
		$this->loadModel('Shop.ShopProductAttributes');
		$attributesList = $this->ShopProductAttributes->attributesList();
		//pr($attributesList);die();
		
		
		$this->loadModel('Shop.ShopOrders');
		$provoz_id_array = [$provoz_list_all['provoz_api_ids'][$provoz_id]];
		
		// slezska with centrum
		if ($provoz_list_all['provoz_api_ids'][$provoz_id] == 10){ $provoz_id_array = [10,2];} 
		if ($provoz_list_all['provoz_api_ids'][$provoz_id] == 2){ $provoz_id_array = [10,2];} 
		// zlin with otrokovice
		if ($provoz_list_all['provoz_api_ids'][$provoz_id] == 28){ $provoz_id_array = [28,29];} 
		
		
		$rozvozce_data = $this->mobileOrders($provoz_id_array);
			//pr($mobileIds);
		
		if (isset($this->request->query['date'])){
			
			$date = $this->request->query['date'];
		} else {
			$date = date('Y-m-d');
			$date_from = date('Y-m-d 00:00:00');
			$date_to = date('Y-m-d 23:59:59');
			$date_from = strtotime($date_from);
			$date_to = strtotime($date_to);
		}
		 
		
		$conditions = [
			//'date(ShopOrders.created)'=>$date,
			'ShopOrders.createdTime >='=>$date_from,
			'ShopOrders.createdTime <'=>$date_to,
			'ShopOrders.shop_provoz_id IN '=>$provoz_id_array,
		];
		//if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
			if ($order_id != null){
			
			$conditions = [
				'ShopOrders.id'=>$order_id,
			];
			
			}
		//}
		if (isset($this->request->query['debug'])){
			//pr($conditions);
			$time = '1519609027';
			//pr(date('y-d.m. H-i',$date_to));
		}
		$query = $this->ShopOrders->find()
		  ->contain(['Users','UserAddresses'=> function ($q) {
			   return $q ->where(['UserAddresses.id = ShopOrders.user_address_id']);
			},'ShopOrderItems'])
		  ->where($conditions)
		  ->select([
		  ])
		  ->order('ShopOrders.id DESC')
		  //->group('ShopOrders.dj_id')
		  ->limit(50)
		  ;
		  //pr($query->sql());
		$data = $query->toArray();
		//if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
		$this->loadModel('ShopProvozs');
		$online = $this->ShopProvozs->find()->where(['provoz_id'=>$provoz_id])->select(['id','online','name'])->hydrate(false)->first();
		//pr($provoz_id);
		if ($provoz_id == 1000){
			$online['online'] = 1;
		}
		//pr($online);
		//pr($provoz_id);
		//pr($data);die();
		
		//}
		$result = [];
		if (empty($data)){
			die('neni co zobrazit');
		} 
		$this->dj_ids = [];
		
		foreach($data AS $d){
			if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
			//if ($d->id == 253459){
				//pr($d);die();
			//}
			}
			// pokud neni zaplaceno
			if ($d->shop_payment_id == 2 && empty($d->payment_recieved)){
				unset($d);
				continue;
			}
			if ($d->credit_use > 0){
				$kupon_price = $d->credit_use*-1;
			} else {
				$kupon_price = null;
			}
				
			$products = [];
			$suroviny_list = [];
				//pr($attributesList);
				foreach($attributesList AS $atr){
					$suroviny_list[$atr->code] = [
						'id'=>$atr->id,
						'name'=>$atr->name,
						'kod'=>$atr->code,
						'cena'=>$atr->price_vat,
					];
				}
			foreach($d->shop_order_items AS $pi){
				
				
				$suroviny_add = [];
				$suroviny_delete = [];
				if (!empty($pi->attributes_ids)){
					$attributes_ids = json_decode($pi->attributes_ids,true);
					//pr($attributes_ids);
					//pr($attributesList);
					foreach($attributes_ids AS $aID=>$aKS){
						if ($d->zdroj_id > 1){
						
						// dame jidlo podminka
						if ($aKS > 0){
							//$suroviny_add[$aID] = $aKS;
							//if 
							//pr($suroviny_list[$aID]);
							if (isset($suroviny_list[$aID]))
							$suroviny_add[$aID] = $aKS;
						}
						if ($aKS < 0){
							//$suroviny_delete[$aID] = $aKS*-1;
							$suroviny_delete[$aID] = $aKS*-1;
						}
						
						} else {
						if ($aKS > 0){
							//$suroviny_add[$aID] = $aKS;
							//if 
							//pr($suroviny_list[$aID]);
							if (isset($attributesList[$aID]))
							$suroviny_add[$attributesList[$aID]['code']] = $aKS;
						}
						if ($aKS < 0){
							//$suroviny_delete[$aID] = $aKS*-1;
							$suroviny_delete[$attributesList[$aID]['code']] = $aKS*-1;
						}
							
						}
					}
					
				}
				//pr($pi);
				$products[] = [
					'id'=>$pi->id,
					'varianty'=>[],
					'varianta'=>0,
					'name'=>$pi->name,
					'price'=>$pi->price_vat,
					'price_default'=>$pi->price_vat,
					'ks'=>$pi->count,
					'code'=>$pi->code,
					'suroviny_add'=>$suroviny_add,
					'suroviny_delete'=>$suroviny_delete,
					'suroviny_price'=>0,
					//'suroviny_list'=>$suroviny_list,
				];
				
				
				
			}
			if (isset($kupon_price) && $kupon_price < 0){
				
				$products[] = [
					'id'=>-1,
					'varianty'=>array(),
					'name'=>'Slevový kupón',
					'price'=>$kupon_price,
					'num'=>999,
					'body'=>0,
					'code'=>'k',
					'suroviny'=>[],
					'suroviny_add'=>[],
					'suroviny_delete'=>[],
					'suroviny_price'=>0,
					'price_kredit'=>0,
					'rozvoz_food_group_id'=>0,
					'suroviny_list'=>[],
					'ks'=>1,
				];
				
			}
			
			if ($d->spropitne > 0){
			
			$products[] = array(
				'id'=>-2,
				'varianty'=>array(),
				'name'=>'Spropitné',
				'price'=>$d->spropitne,
				'num'=>998,
				'body'=>0,
				'code'=>'s',
				'suroviny'=>array(),
				'suroviny_add'=>array(),
				'suroviny_delete'=>array(),
				'suroviny_price'=>0,
				'price_kredit'=>0,
				'rozvoz_food_group_id'=>0,
				'ks'=>1,
			);
			}
			
			$item = [
				'RozvozXml'=>[
					 'id' => $d->id,
					 'online' => $online['online'],
					 'xml_type' => 1,
					 'dj_id' => $d->dj_id,
					 'dj_partner_id' => $d->dj_partner_id,
					 'rozvoz_provoz_id' => $d->shop_provoz_id,
					 'name' => $d->user->last_name.' '.$d->user->first_name,
					 'telefon' => $d->user->phone,
					 'ulice' => (!empty($d->user_address)?$d->user_address->street.' '.$d->user_address->cp:' '),
					 'mesto' => (!empty($d->user_address->city)?$d->user_address->city:''),
					 'created' => $d->created->format('Y-m-d H:i:s'),
					 'updated' => $d->modified->format('Y-m-d H:i:s'),
					 'created_preposlano' => (!empty($d->preposlano_date)?$d->preposlano_date->format('Y-m-d H:i:s'):''),
					 'predano_id' => $d->preposlano_provoz_id,
					 'poznamka' => $d->note,
					 'doprava' => $d->doprava,
					 'rozvoz_order_id' => $d->id,
					 'jmeno' => $d->user->first_name,
					 'prijmeni' => $d->user->last_name,
					 'email' => $d->user->email,
					 'city' => (!empty($d->user_address->city)?$d->user_address->city:''),
					 'paymentSessionId' => $d->gopay_id,
					 'platba_type' => $d->shop_payment_id,
					 'zdroj' => $d->zdroj_id,
					 'telefon_pref' => strtr($d->user->phone_pref,['00'=>'+']),
					 'products' => $products,
					 'suroviny_list'=>$suroviny_list,
					 'lat'=>$d->user_address->lat,
					 'lng'=>$d->user_address->lng,
				],
				'RozvozOrder'=>[
					 'paymentSessionId' => $d->gopay_id,
					 'id' => $d->id,
					 'note' => $d->note,
				
				]
			];
			if (!empty($d->id)){
				if (!isset($this->dj_ids[$d->dj_id])){
					$this->dj_ids[$d->dj_id] = $d->dj_id;
					$result[] = $item;
				}
			} else {
				$result[] = $item;	
			}
			
		}
		
		if (isset($this->request->query['json2'])){
			//sleep(5);
		}
		
		if (isset($this->request->query['debug'])){
			foreach($result AS $k=>$r){
				foreach($r['RozvozXml']['products'] AS $kp=>$p){
					//pr($p);
					$result[$k]['RozvozXml']['products'][$kp]['suroviny_list'] = json_encode($result[$k]['RozvozXml']['suroviny_list']);
					
				}
			}
			pr($result);
		}
		if (empty($result)){
			die('neni co zobrazit');
		}
		$result_json = [
			'data'=>$result,
			'rozvozce_data'=>(isset($rozvozce_data)?$rozvozce_data:[]),
		];
		//pr($result_json);
		
		
		die(json_encode($result_json));
	}
	
	
	public function statusOnline($provoz_id){
		$this->loadModel('ShopProvozs');
		//pr($this->system_id);
		$data = $this->ShopProvozs->find()->where(['pokladna_id'=>$provoz_id])->select(['id','online','name'])->first();
		if ($data){
			die(json_encode(['result'=>true,'status_online'=>($data->online == 1)?1:0,'name'=>$data->name,'message'=>($data->online == 1)?'Provozovna je online':'Provozovna je offline']));
				
		} else {
			die(json_encode(['result'=>false,'message'=>'Provoz nenalezen']));
			
		}
	}
	
	
	public function statusOnlineChange($provoz_id){
		$this->loadModel('ShopProvozs');
		$data = $this->ShopProvozs->find()->where(['pokladna_id'=>$provoz_id])->select(['id','online','name'])->first();
		if ($data){
			if ($this->ShopProvozs->updateAll(
					['online' => ($data->online == 1)?0:1], // fields
					['pokladna_id' => $provoz_id]
				)
			) {
				die(json_encode(['result'=>true,'status_online'=>($data->online == 1)?0:1,'name'=>$data->name,'message'=>'Online objednávky '.(($data->online == 1)?'vypnuty':'zapnuty')]));
				
			} else {
				die(json_encode(['result'=>false,'message'=>'Chyba uložení']));
				
			}
		} else {
			if ($provoz_id == 1000)
			die(json_encode(['result'=>true,'status_online'=>0,'name'=>'testovaci','message'=>'Online objednávky zapnuty']));
			
		}
			
	}
	
	public function logs(){
		$this->loadModel('ApiLogs');
		$query = $this->ApiLogs->find()
		  ->where([])
		  ->select([
		  
		  ])
		  ->limit(10)
		  ->order('id DESC')
		  ;
		$data = $query->toArray();
		pr($data);
		die('a'); 
	}
	
	function provoz(){
		//$this->loadModel('RozvozLoad');
		//$this->RozvozLoad->useDbConfig = 'import';
		//$this->RozvozLoad->useTable = 'chachar_order_clients';
		$this->loadModel('Shop.ShopProvozs');
		$provoz_list_all = $this->ShopProvozs->provozListAll(true);
		$provoz_all = [];
		//pr($provoz_list_all);
			
		
		//pr($provoz_all);
		unset($provoz_list_all['provoz_list_id']['']);
		unset($provoz_list_all['provoz_list_id'][1000]);
		
		$provoz = [];
		$i = 1;
		foreach($provoz_list_all['provoz_list_id'] AS $id=>$p){
			if (isset($this->request->query['poradi'])){
				$provoz[]['RozvozProvoz'] = [
					'id'=>$id,
					'name'=>$p,
					'poradi'=>$i,
				];
				
			} else {
				$provoz[$id] = $p;
				
			}
			/*
			*/
			$i++;
		}
		
		if (isset($this->request->query['new'])){
		die(json_encode(['result'=>true,'data'=>$provoz]));
			
		} else {
		die(json_encode($provoz));
			
		}
		
	}
	
	
	function addApi(){
		/*
		$products = array();
		$products[] = array('code'=>'001A','name'=>'Margarita','price'=>0,'gramaz'=>'500g','ks'=>'1');
		$products[] = array('code'=>'001B','name'=>'Margarita','price'=>229,'gramaz'=>'800g','ks'=>'1');
		//$products[] = array('code'=>'rozdil','name'=>'Doplatek','price'=>100,'gramaz'=>'','ks'=>'1');
		
		$post = array(
			'code'  => 54321,
			'jmeno'  => 'Fastest',
			'prijmeni'  => 'Test',
			'email'  => 'test51@fastest.cz',
			'telefon'  => '731956030',
			'ulice'  => 'Pelclova 2500',
			'mesto'  => 'Ostrava',
			'provoz'  => 12,
			'poznamka'  => 'poznamka',
			'total_price'  => 229,
			'placeno'  => 1,
			'products'  => $products,
		);
		*/
		/*
		200 - objednavka ulozena
		400 - spatny overovaci kod
		401 - chybny provoz
		403 - chybny parametr v POST
		404 - zadne data v POST
		405 - chyba ulozeni do DB
		406 - neni vyplnen email
		
		*/
		$this->starttime = time();
		
		$this->code_message = [
			200=>'',
			400=>'Spatny overovaci kod',
			401=>'Provozovona není online, nebo není aktivni',
			402=>'Zadne data na POST',
			403=>'Neni parametr: ',
			404=>' neni mozno poslat do systemu',
			405=>'Chyba uložení objednávky',
			406=>'Není vyplněn email',
			407=>'Vypršel čas spojení, odešlete znovu',
		];
		$this->loadModel('Shop.ShopProvozs');
		$provoz_list_all = $this->ShopProvozs->provozListAll();
		unset($provoz_list_all['provoz_list_id']['']);
		$provoz_list_all['provoz_list_id'][1000] = 'Testovaci';
		$post_fields = $_POST;
		//$post_fields = $post;
		
		if ($post_fields['email'] == 'panek@tvorimestranky.cz') die();
		
		if (!isset($post_fields) || empty($post_fields)) die(json_encode(array('result'=>false,'code'=>402,'message'=>$this->code_message[402])));
		if (empty($post_fields['jmeno'])) $post_fields['jmeno'] = 'X';
		
		
		$this->logApi($post_fields);
		//print_r($post_fields);
		//print_r($provoz_list_all['provoz_list_online_api']);
		//print_r($post_fields['provoz']);
		$provoz_list_all['provoz_list_online_api'][1000] = 'Testovaci';
		if (!isset($provoz_list_all['provoz_list_online_api'][$post_fields['provoz']])){
			$this->logResult(401);
			die(json_encode(['result'=>false,'message'=>$this->code_message[401],'code'=>401]));
		}
		
		$code_list = array(
			12345=>array('test'=>false,'zdroj'=>2,'email'=>'test@fastest.cz'),
			54321=>array('test'=>false,'zdroj'=>3,'email'=>'test@fastest.cz'),
			123321=>array('test'=>false,'zdroj'=>4,'email'=>'test@fastest.cz'),
		);
		
		$this->api_code = $post_fields['code'];
		//print_r($this->api_code);
		if (!isset($post_fields['code']) || !array_key_exists($post_fields['code'],$code_list)) {
			$this->logResult(400);
			die(json_encode(array('result'=>false,'code'=>400,'message'=>$this->code_message[400])));
		}
		if (empty($post_fields['email'])) {
			$this->logResult(406);
			die(json_encode(array('result'=>false,'code'=>406,'message'=>$this->code_message[406])));
		}
		
		if (!isset($post_fields['poznamka'])) $post_fields['poznamka'] = '';
		$valid_fields = array(
			'jmeno',
			'prijmeni',
			'email',
			'telefon',
			'ulice',
			'mesto',
			'provoz',
			'poznamka',
			'products',
			'total_price',
		);
		
		//print_r($post_fields);
		//print_r($valid_fields);
		
		foreach($valid_fields AS $f){
			if (!array_key_exists($f,$post_fields)){
				$this->logResult(403,$f);
				die(json_encode(array('result'=>false,'code'=>403,'message'=>$this->code_message[403].$f)));
			}
		}
		
		$post_fields_strip = array();
		foreach($post_fields AS $k=>$p){
			if (!is_array($p))
				$post_fields_strip[$k] = strip_tags(strtr($p,array('"'=>'','°'=>'','&'=>'&amp;')));
			else
				$post_fields_strip[$k] = $p;
		}
		$post_fields = $post_fields_strip;
		
			$len = strlen($post_fields['telefon']);
			if ($len>9){
				$phone = substr($post_fields['telefon'],-9);
				$phone_pref = substr($post_fields['telefon'],0,-9);
				
			} else {
				$phone = $post_fields['telefon'];
				$phone_pref = '00420';
			
			}
			if ($post_fields['provoz'] == 1000){
				//sleep(15);
		
			}
			$this->checkTimeLimit();
		
			
		//print_r($user);die();
		
		// client	
		$user_new =  [
			'first_name'=>$post_fields['jmeno'],
			'last_name'=>$post_fields['prijmeni'],
			'email'=>$post_fields['email'],
			'system_id'=>$this->system_id,
			'reg'=>0,
			'phone'=>0,
			'phone'=>$phone,
			'phone_pref'=>$phone_pref,
			'zdroj_id'=>$code_list[$post_fields['code']]['zdroj'],
		];
		
		$user = $this->findUser($post_fields['email'],$user_new);
		
		//pr($user);
		if (!$user){
			$user = $user_new;
		} else {
			$user = array_merge($user,$user_new);
			unset($user['password']);			
		}
		//print_r($user);die();
		
		// adresa
		$user_addresses = [];
		$ulice = explode(' ',$post_fields['ulice']);
		if (count($ulice) > 1){
			end($ulice);     
			$key = key($ulice);
			$cp = $ulice[$key];
			unset($ulice[$key]);
		} else {
			$cp = '';
		}
		
		$address = $post_fields['ulice'].' '.$post_fields['mesto']; // Google HQ
        $prepAddr = str_replace(' ','+',$address);
        $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false');
        $output= json_decode($geocode);
		if (isset($output->results[0]->geometry->location->lat)){
			$lat = $output->results[0]->geometry->location->lat;
			$lng = $output->results[0]->geometry->location->lng;
			
		} else {
			$lat = '';
			$lng = '';
		}
		
		// find address if exist
		$user_addresses[0] = $this->findAddress(implode(' ',$ulice),$post_fields['mesto'],$cp,(isset($user['id'])?$user['id']:''));
		if (!$user_addresses[0]){
			$user_addresses[0] = [
				'shop_address_type_id'=>1,
				'street'=>implode(' ',$ulice),
				'cp'=>$cp,
				'city'=>$post_fields['mesto'],
				'lat'=>$lat,
				'lng'=>$lng,
			];
		}
		$user['user_addresses'] = $user_addresses;
		
		// order items
		if ($post_fields['provoz'] == 1000){
			//print_r($post_fields);die();
		}
		//print_r($user_addresses);die();
		$order_items = [];
		if (!empty($post_fields['products'])){
			foreach($post_fields['products'] AS $p){
				$products_list = $this->productsList();
				//print($products_list);
				if (in_array($p['code'],['rozdil','doplatek'])){
					if (!isset($p['ks'])){
						$p['ks'] = 1;
					}
					$product_code_list = [
						'rozdil'=>2007,
						'doplatek'=>2006,
					];
					$order_items[] = [
						'shop_product_id'=>$product_code_list[$p['code']],
						'code'=>$p['code'],
						'name'=>$p['name'],
						'price_per_item'=>$p['ks']*($p['price'] - $p['price']*DPH_CON),
						'price_vat_per_item'=>$p['ks']*$p['price'],
						'price'=>$p['price'] - $p['price']*DPH_CON,
						'price_vat'=>$p['price'],
						'count'=>$p['ks'],
						
						'tax_id'=>0,
					];
					
				} else {
					
				//print_r($products_list);
				if (!isset($products_list[$p['code']])){
					$this->logResult(404,$p['code'],'Produkt '.$p['code']);
					die(json_encode(['result'=>false,'message'=>'Produkt '.$p['code'].$this->code_message[404],'code'=>404]));
				}
				if (!isset($p['ks'])){
					$p['ks'] = 1;
					//die(json_encode(['result'=>false,'message'=>'Produkt '.$p['code'].' nema definovan pocet KS','code'=>405]));
				}
				
				//$find_prod = $this->findProduct($p['code']);
				$order_items[] = [
					'shop_product_id'=>$products_list[$p['code']]['id'],
					'code'=>$p['code'],
					'name'=>$products_list[$p['code']]['name'],
					'price_per_item'=>$p['ks']*($products_list[$p['code']]['price'] - $products_list[$p['code']]['price']*DPH_CON),
					'price_vat_per_item'=>$p['ks']*$products_list[$p['code']]['price'],
					//'price'=>$products_list[$p['code']]['price'] - $products_list[$p['code']]['price']*DPH_CON,
					//'price_vat'=>$products_list[$p['code']]['price'],
					'price'=>$p['price'] - $p['price']*DPH_CON,
					'price_vat'=>$p['price'],
					'count'=>$p['ks'],
					'attributes_ids'=>(isset($p['attributes_ids']) && !empty($p['attributes_ids'])?json_encode($p['attributes_ids']):null),
					'tax_id'=>0,
				];
				}
				//print_r($p);
			}
		}
		//print_r($post_fields);
		if ($post_fields['provoz'] == 1000){
			//print_r($post_fields['products']);
			//print_r($order_items);die();
		
		}
		
		$save_order = [
			'createdTime' => strtotime(date('Y-m-d H:i:s')),
			'system_id'=>$this->system_id,
			'shop_provoz_id'=>$provoz_list_all['provoz_api_ids'][$post_fields['provoz']],
			'price'=>$post_fields['total_price']-$post_fields['total_price']*DPH_CON,
			'note'=>$post_fields['poznamka'],
			'dj_id'=>$post_fields['dj_id'],
			'dj_partner_id'=>$post_fields['dj_partner_id'],
			'doplatek'=>(isset($post_fields['doplatek'])?$post_fields['doplatek']:''),
			'spropitne'=>(isset($post_fields['spropitne'])?$post_fields['spropitne']:''),
			'doprava'=>(isset($post_fields['doprava'])?$post_fields['doprava']:''),
			'price_vat'=>$post_fields['total_price'],
			'zdroj_id'=>$code_list[$post_fields['code']]['zdroj'],
			'shop_payment_id'=>(($post_fields['placeno'] == 1)?2:1),
			'payment_recieved'=>(($post_fields['placeno'] == 1)?new Time():''),
			'reg'=>$user['reg'],
			'user'=>$user,
			'client_name'=>$post_fields['jmeno'].' '.$post_fields['prijmeni'],
			'client_telefon'=>$phone,
			
			'shop_order_items'=>$order_items,
		];
		//print_r($save_order);die();
		//print_r($post_fields);die();
		//pr($user);
		$this->loadModel('Shop.ShopOrders');
		$save = $this->ShopOrders->newEntity($save_order,['associated' => ["Users"=>['validate'=>'OnlyCheckPass','accessibleFields'=>['id' => true]], "Users.UserAddresses","ShopOrderItems"]]);
		$this->checkTimeLimit();
		
		//print_r($save);die();
		if ($result = $this->ShopOrders->save($save)){
			$this->ShopOrders->updateAll(
				['user_address_id' => $result->user->user_addresses[0]->id], // fields
				['id' => $result->id]
			);
			$this->timeSave();
			die(json_encode(['result'=>true,'order_id'=>$result->id,'message'=>'Objednávka byla uložena, pokud nepříjde do 10min potvrzení volejte na provozovnu','code'=>200]));
			
		} else {
			$this->logResult(405);
			die(json_encode(['result'=>false,'message'=>$this->code_message[405],'code'=>405]));
		
		}
		
		//pr($result);
		

		/*
			$provoz_email = $this->RozvozProvoz->find('list',array('conditions'=>array('kos'=>0,'online'=>1),'fields'=>array('id','order_email')));
			
			if ($post_fields['provoz'] == 1000){
				$provoz_email[1000] = 'test@fastest.cz';
			}
			
			if ($code_list[$post_fields['code']]['test'] == true){
				$provoz_email[100] = $code_list[$post_fields['code']]['email'];
			}
			$find_email = $this->RozvozClient->find('first',array('conditions'=>array('id'=>$client_id),'fields'=>array('id','email')));
			$save_email = array(
				'rozvoz_provoz_id'=>$post_fields['provoz'],
				'rozvoz_provoz_name'=>$provoz_list['RozvozProvoz']['name'],
				'rozvoz_provoz_email'=>$provoz_email[$post_fields['provoz']],
				'email'=>$find_email['RozvozClient']['email'],
				'rozvoz_order_id'=>$order_id,
				'potvrzeno'=>0,
			);
			if (!$this->RozvozEmail->save($save_email)){
				die(json_encode(array('result'=>false,'code'=>405,'message'=>'Chyba ulozeni Provoz neni online')));
			}
			
			
			if (!$this->RozvozXml->save($save_xml))
				die(json_encode(array('result'=>false,'code'=>405,'message'=>'Chyba ulozeni DB: XML ')));
				
			$this->RozvozXml->id = null;
				
			if ($code_list[$post_fields['code']]['test'] == true){
				$this->Rozvoz->send_email_client($order_id,'test@fastest.cz');
			} else {
				$this->Rozvoz->send_email_client($order_id);
			}
		
		//$diff = array_diff(array_flip($post_fields),$_POST);
		//print_r($diff);
		
		die(json_encode(array('result'=>true,'code'=>200,'order_id'=>$order_id,'message'=>'Objednávka byla uložena, pokud nepříjde do 10min potvrzení volejte na provozovnu')));
		*/
	}
	
	private function findProduct($code){
		$this->loadModel('Shop.ShopProducts');
		$query = $this->ShopProducts->find()
		  ->contains(['ShopProductVariants'])
		  ->where([])
		  ->select([
		  ]);
		$data = $query->first();
		//print_r($data);
		return $data;
	}
	
	// switch online offline from pokladna
	function switch_online($provoz_id,$stav){ 
		$this->loadModel('Shop.ShopProvozs');
		
		$query = $this->ShopProvozs->find()
		  ->where(['provoz_id'=>$provoz_id])
		  ->select([
		  ]);
		$load = $query->first();
		
		if (!$load){
			die(json_encode(array('result'=>false,'message'=>'Provoz nenalezen')));
		}
		//die('a');
		$save = array(
			'id'=>$load->id,
			'online'=>$stav,
		);
		$save_provoz = $this->ShopProvozs->newEntity($save);
		$this->ShopProvozs->save($save_provoz);
		
		if ($stav == 1){
		
			$message = 'Provoz byl zapnut';
		}
		if ($stav == 0){
			$message = 'Provoz byl vypnut';
		}
		die(json_encode(array('result'=>true,'message'=>$message)));
	}
	
	private function findUser($email){
		$this->loadModel('Shop.ShopClients');
		$query = $this->ShopClients->find()
		  ->where(['email'=>$email])
		  ->select([
		  ])
		  ->hydrate(false)
		  ;
		$data = $query->first();
		
		return $data;
	}
	
	private function findAddress($street,$city,$cp,$user_id=null){
		$this->loadModel('Shop.ShopClientAddresses');
		$conditions = ['street'=>$street,'city'=>$city,'cp'=>$cp];
		if ($user_id != null){
			$conditions['shop_client_id'] = $user_id;
		}
		$query = $this->ShopClientAddresses->find()
		  ->where($conditions)
		  ->select([
		  ])
		  ->hydrate(false)
		  ;
		$data = $query->first();
		
		return $data;
	}
	
	
	function test3(){
		$from_name = FROM_NAME;
				$from_email = FROM_EMAIL;
		$to_email = 'test@fastest.cz';
		if (!$this->Email->send_from_template(5,$from_name,$from_email,null,array($to_email)))
						die(json_encode(array('result'=>false,'message'=>lang_chyba_odeslani_formulare)));
		die();
	}
	
	function productsList(){
		$this->loadModel('Shop.ShopProducts');
		$mapper = function ($row, $key, $mapReduce) {
				// map group id from category
				$row->group_id = (isset($this->menu_data['shop_menu_items_group_id'][$row->shop_category_id])?$this->menu_data['shop_menu_items_group_id'][$row->shop_category_id]:'');
				
				
				// map varitanty
				if (isset($row->shop_product_variants) && !empty($row->shop_product_variants)){
					if (!isset($variants_list)){
						$variants_list = [];	
						$variants_select_list = [];	
					}
					foreach($row->shop_product_variants AS $var){
						$variants_list[$var->id] = $var;
						$variants_select_list[$var->id] = [
							'id'=>$var->id,
							'name'=>$var->name,
							'price'=>$var->price,
							'price_vat'=>$var->price_vat,
						];
					}
					$row->variants_list = $variants_list;
					$row->variants_select_list = $variants_select_list;
				}
				
				if (isset($row->shop_product_prices) && isset($row->shop_product_prices[0])){
						$row->price_vat  = $row->shop_product_prices[0]['price_vat'];
					
				} else {
					unset($row);
				}
				if (isset($row))
				$mapReduce->emit($row);
			};
		
		$query = $this->ShopProducts->find()
		  ->contain([
					'ShopProductVariants'=>function($q){
						$q->select(['id','shop_product_id','price_vat','code','name']);
						$q->where(['system_id'=>$this->system_id]);
						return $q;
					},
					'ShopProductPrices'=>function($q){
						//$q->select(['id','shop_product_id','price_vat','code','name']);
						$q->where(['system_id'=>$this->system_id,'status'=>1]);
						return $q;
					}
		  ])
		  ->where([])
		  ->select([
		  ])
		  ->mapReduce($mapper);
		  ;
		$data = $query->toArray();
		//print_r($data);
		$products_list = [];
			
		foreach($data AS $d){
			
			if (isset($d->shop_product_variants) && !empty($d->shop_product_variants)){
				foreach($d->shop_product_variants AS $v){
					$products_list[$v->code] = [
						//'id'=>$d->id.'v'.$v->id,
						'id'=>$d->id,
						'code'=>$v->code,
						'name'=>$d->name.' '.$v->name,
						'price'=>$v->price_vat,
					];
					
				}
			} else {
			
				$products_list[$d->code] = [
					'id'=>$d->id,
					'code'=>$d->code,
					'name'=>$d->name,
					'price'=>$d->price_vat,
				];
			}
		}
		//$products_list = array_merge($products_list,$this->cinajede(true));
		//$products_list = $products_list+$this->cinajede(true);
		//print_r($this->cinajede(true));

		return $products_list;
	}
	
	function checkTimeLimit(){
		
		$now = time()-$this->starttime;
		if ($now > 30) {             //assuming you're looking at 30 seconds
			$this->logResult(407,'Cas spojeni','Cas spojeni');
			die(json_encode(array('result'=>false,'code'=>407,'message'=>$this->code_message[407])));
		}

	}
	
	
	function test(){
		
		
		$products = array();
		$products[] = array('code'=>'001A','name'=>'Margarita','price'=>0,'gramaz'=>'500g','ks'=>'1','attributes_ids'=>[2=>-1,14=>1,65=>1]);
		$products[] = array('code'=>'001B','name'=>'Margarita','price'=>229,'gramaz'=>'800g','ks'=>'1');
		//$products[] = array('code'=>'rozdil','name'=>'Rozdil','price'=>100,'gramaz'=>'800g','ks'=>'1');
		$products[] = array('code'=>'doplatek','name'=>'Doplatek','price'=>200,'gramaz'=>'800g','ks'=>'1');
		$products[] = array('code'=>'rozdil','name'=>'Rozdil','price'=>100,'gramaz'=>'','ks'=>'1');
		
		$post = array(
			'code'  => 12345,
			'jmeno'  => 'test',
			'prijmeni'  => 'Test',
			'email'  => 'test3@fastest.cz',
			'telefon'  => '+420731956030',
			'ulice'  => 'Pelclova 2500',
			'mesto'  => 'Ostrava',
			'provoz'  => 1000,
			'poznamka'  => 'poznamka',
			'total_price'  => 229,
			'placeno'  => 1,
			'doprava'  => 19,
			'products'  => $products,
		);
		/*
		$post = array(
			'code'  => 54321,
			'jmeno'  => '',
			'prijmeni'  => 'Jana Jirků',
			'email'  => 'jajirku@seznam.cz',
			'telefon'  => '+420603142629',
			'ulice'  => 'třída Václava Klementa 822',
			'mesto'  => 'Jihlava',
			'provoz'  => 1000,
			'poznamka'  => 'Dvoje nudle bez masa. Děkuji.',
			'total_price'  => 229,
			'placeno'  => 1,
			'products'  => $products,
		);
		$data = '{
  "code": 54321,
  "jmeno": "",
  "prijmeni": "Test Fastest",
  "email": "test@fastest.cz",
  "telefon": "+420731956030",
  "ulice": "Pelclova 2500",
  "mesto": "Ostrava",
  "provoz": "1000",
  "poznamka": null,
  "placeno": 1,
  "total_price": 328,
  "dj_id": 1234546,
  "dj_partner_id": 1234546,
  "spropitne": 10,
  "products": [
    
    {
      "name": "Country - ",
      "code": "001B",
      "price": 100,
      "gramaz": "500g",
      "attributes_ids": []
    }
  ]
}
';
*/
	// "code"=>!001B!,!name'=>"Margarita","price"=>329,"gramaz"=>"800g","ks'=>"1"
		
		//$data = '{"code":"54321","jmeno":"X","prijmeni":"pokora","email":"pokorapetr@seznam.cz","telefon":"+420608620546","ulice":"17. listopadu 1033\/17","mesto":"Hav\u00ed\u0159ov","provoz":"1000","poznamka":"","placeno":"1","total_price":"428","products":[{"name":"Woodstock","code":"BRG5","price":"169","gramaz":""},{"name":"Woodstock","code":"BRG5","price":"169","gramaz":""},{"name":"Om. BBQ, Hranolky, Coca-Cola 0.33l","code":"BRG11","price":"45","gramaz":""},{"name":"Om. BBQ, Hranolky, Coca-Cola 0.33l","code":"BRG11","price":"45","gramaz":""}]}';
		
		//pr($post);
		
		//$post = json_decode($data,true);
		
		//pr($data);
		//die();
		

		$ch = curl_init();

		curl_setopt($ch, CURLOPT_URL, 'http://'.$_SERVER['HTTP_HOST'].'/api/add/');
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec ($ch);
		pr($result);
		curl_close ($ch);
		die();		
	}
	
	function logApi($data){
		$this->loadModel('Shop.ApiLogs');
		//print_r($data);
		$this->logResult(400,'aa');
		$save_log = [
			'system_id'=>$this->system_id,
			'provoz_id'=>$data['provoz'],
			'data'=>json_encode($data),
		];
		//print_r($save_log);
		$save_log_entity = $this->ApiLogs->newEntity($save_log);
		//print_r($save);die();
		$result_save_log = $this->ApiLogs->save($save_log_entity);
		$this->log_id = $result_save_log->id;
		//print_r($this->log_id);
		//die('a');
	}
	
	function logEmail($code,$value=null){
		
		if (isset($this->log_id)){
		//print_r($this->msg_value);
			
			
			$query = $this->ApiLogs->find()
			  ->where(['id'=>$this->log_id])
			  ->select([
				'data'
			  ])
			  
			  ;                
			$load_log = $query->first();
			$opt = [
				'to'=>'test@fastest.cz',
				'template_id'=>5,
				'data'=>[
					'id'=>$this->log_id,
					'result_name'=>$this->msg_value.' '.$this->code_message[$code],
					'result'=>$code,
					'result_value'=>$value,
					'data'=>$load_log->data,
					'api_code'=>$this->api_code,
				],
			];
			$this->loadComponent('Email');
			$this->Email->send($opt);
			//die('aa');
		}
	}
	
	function logResult($code,$value=null,$msg_value=null){
		$this->loadModel('Shop.ApiLogs');
		$this->msg_value = $msg_value;
		$this->logEmail($code,$value=null);
		
		if (isset($this->log_id)){
		//print_r($this->api_code);
		$this->ApiLogs->updateAll(
			['result' => $code,'result_value'=>$value,'api_code'=>$this->api_code], // fields
			['id' => $this->log_id]
		);
		
		}
	}
	function timeSave(){
		
		if (isset($this->log_id)){
		//print_r($this->api_code);
		$now = time()-$this->starttime;
		
		$this->ApiLogs->updateAll(
			['time_save' => $now], // fields
			['id' => $this->log_id]
		);
		
		}
	}

	function products($type=null){
		
		header('Content-Type: text/html; charset=utf-8');
		$this->loadModel('Shop.ShopProducts');
		$mapper = function ($row, $key, $mapReduce) {
				// map group id from category
				$row->group_id = (isset($this->menu_data['shop_menu_items_group_id'][$row->shop_category_id])?$this->menu_data['shop_menu_items_group_id'][$row->shop_category_id]:'');
				
				
				// map varitanty
				if (isset($row->shop_product_variants) && !empty($row->shop_product_variants)){
					if (!isset($variants_list)){
						$variants_list = [];	
						$variants_select_list = [];	
					}
					foreach($row->shop_product_variants AS $var){
						$variants_list[$var->id] = $var;
						$variants_select_list[$var->id] = [
							'id'=>$var->id,
							'name'=>$var->name,
							'price'=>$var->price,
							'price_vat'=>$var->price_vat,
						];
					}
					$row->variants_list = $variants_list;
					$row->variants_select_list = $variants_select_list;
				}
				
				if (isset($row->shop_product_prices) && isset($row->shop_product_prices[0])){
						$row->price_vat  = $row->shop_product_prices[0]['price_vat'];
					
				} else {
					unset($row);
				}
				if (isset($row))
				$mapReduce->emit($row);
			};
		//$conditions = ['ShopProducts.shop_category_id NOT IN'=>[13]];
		$conditions = [];
		
		if ($type == 'cina'){
			$conditions = [
				'ShopProducts.shop_category_id IN'=>[10,11,8]
			];
		}
		if ($type == 'records'){
			$conditions = [
				'ShopProducts.shop_category_id IN'=>[9,8,13]
			];
		}
		if ($type == 'chicken'){
			$conditions = [
				'ShopProducts.shop_category_id IN'=>[6,8]
			];
		}
		if ($type == 'halusky'){
			$conditions = [
				'ShopProducts.id IN'=>[409,410,411,412,413,414,415,93,94,106,107,116,117,174,331]
			];
		}
			
		$query = $this->ShopProducts->find()
		  
		  ->contain([
					'ShopProductConAttributes'=>function($q){
						$q->select(['shop_product_attribute_id','shop_product_id']);
						//$q->where(['system_id'=>$this->system_id]);
						return $q;
					},
					'ShopProductVariants'=>function($q){
						$q->select(['id','shop_product_id','price_vat','code','name']);
						$q->where(['system_id'=>$this->system_id]);
						return $q;
					},
					'ShopProductPrices'=>function($q){
						//$q->select(['id','shop_product_id','price_vat','code','name']);
						$q->where(['system_id'=>$this->system_id,'status'=>1]);
						return $q;
					}
			])
			/*
		  ->contain(['ShopProductVariants','ShopProductConAttributes','ShopProductPrices'=>function($q){
						//$q->select(['id','shop_product_id','price_vat','code','name']);
						$q->where(['system_id'=>$this->system_id,'status'=>1]);
						return $q;
			}])
			*/
		  ->where($conditions)
		  ->select([
		  ])
		  ->order('sort ASC')
		  ->mapReduce($mapper);
      
		$data = $query->toArray();
		
		//pr($data); 
		$product = array();
		foreach($data AS $l){
			if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
				//if ($l->code == 'C1A'){
				//die('a');
				//pr($l->code);	
				//}
			}
			
			//pr($l);
			$food_ids = array();
			foreach($l->shop_product_con_attributes AS $f){
				$food_ids[] = $f['shop_product_attribute_id'];
			}
			//die();
			$var_list = array();
			if ($l->shop_category_id == 10) $l->shop_category_id = 100;
			if (count($l->shop_product_variants)>0){
				foreach($l->shop_product_variants AS $v)
				$var_list[]['name'] = $v['name'];
				foreach($l->shop_product_variants AS $k=>$var){
					if ($l->code == 'C16'){
					//pr($var);
					}
				
					//pr($var);
					//pr($var_list);
					//pr($l['RozvozFood']['varianty'][$k]['name']);
					if ($var->code == '105A') $l->name = 'Stripsy KLASIK';
					if ($var->code == '105B') $l->name = 'Stripsy PIKANT';
					if ($var->code == '106A') $l->name = 'HTH KLASIK';
					if ($var->code == '106B') $l->name = 'HTH PIKANT';
					
					$product[] = array(
					
						'id'=>$l->id,
						'num'=>$l->number,
						'name'=>$l->name,
						'text'=>$l->description,
						'gramaz'=>(!empty($l->gramaz)?$l->gramaz:''),
						'varianty'=>$var_list,
						'price'=>$var->price_vat,
						'code'=>$var->code,
						'souvisejici'=>$l->souvisejici,
						'varianta'=>$k,
						'cat_id'=>$l->shop_category_id,
						'food_ids'=>$food_ids,
					);
				}
        
			} else {
					
					if ($l->code == '108') $l->name = 'PecCesnekColaHR';
					if ($l->code == '109') $l->name = 'BBQColaHran';
					if ($l->code == '110') $l->name = 'ZelPeprColaHra';
					if ($l->code == '111') $l->name = 'SweetChColaHran';
					//if ($l->code == '107') $l->name = 'SweetChColaHran';
					
					if ($type == null)   {
						if ($l->shop_category_id == 11) $l->shop_category_id = 10;
						if ($l->shop_category_id == 8) $l->shop_category_id = 11;
					}
					
						
					$product[] = array(
						'id'=>$l->id,
						'num'=>$l->number,
						'name'=>$l->name,
						'text'=>$l->description,
						'gramaz'=>(!empty($l->gramaz)?$l->gramaz:''),
						'varianty'=>$var_list,
						'price'=>$l->price_vat,
						'code'=>$l->code,
						'souvisejici'=>$l->souvisejici,
						'varianta'=>1,
						'cat_id'=>$l->shop_category_id,
						'food_ids'=>$food_ids,
					);
			
			}
			
		}
		//pr($type);                 
        //if ($type == null)               
		//$product = array_merge($product,$this->cinajede());
		if (isset($_GET['debug'])){
			pr($product);
			
		}
      
		die(json_encode($product));
	}
	
	
	function cinajede($api=false){
  
		//pr($this->system_id);die();
		switch($this->system_id){
			case 1: $model = 'Cinajedes';break;
			case 2: $model = 'CinajedeBrnos';break;
			case 3: $model = 'CinajedeMechovaceks';break;
			case 4: $model = 'CinajedeOgars';break;
			case 5: $model = 'CinajedePanars';break;
			case 6: $model = 'CinajedePepiks';break;
			case 7: $model = 'Cinajedes';break;
			case 8: $model = 'CinajedePetans';break;
			case 9: $model = 'CinajedePartaks';break;
			case 10: $model = 'CinajedeBoleslavs';break;
			case 13: $model = 'Cinajedes';break;
			default: $model = null;
		}
		//pr($model);die();
		if(!$model){
        return [];
    }          
		$this->loadModel('Shop.'.$model);

    
		$query = $this->$model->find()
		  ->where(['kos'=>0,'rozvoz_food_group_id IN'=>[36,39],'status'=>1])
		  ->select([
		  ])
		  ->hydrate(false)
		  ->order('num ASC')
		
		  ;                
		$load = $query->toArray();
		//pr($load);die();
		//$load = $this->Cinajede->find('all',array('conditions'=>array('status'=>1,'kos'=>0,'rozvoz_food_group_id'=>array(36,39))));
		//pr($load);
		//print_r($load);
		$product = array();
		foreach($load AS $l){
			/*
			if (isset($l['varianty']) && !empty($l['varianty'])){
				$var_list[1]['name'] = 'Normální';
				$var_list[2]['name'] = 'Big porce';
			} else {
				$var_list = array(
			);
			*/
			
			//}
			if ($l['rozvoz_food_group_id'] == 39){
				$catid = 101;
			} else {
				$catid = 100;
			}
			if ($api == false){   
				$product[] = array(
					'id'=>$l['id'],
					'num'=>$l['num'],
					'name'=>$l['name'],
					'text'=>strip_tags($l['text']),
					'gramaz'=>$l['gramaz'],
					'varianty'=>unserialize($l['varianty']),
					'price'=>$l['price'],
					'code'=>strtr($l['code'],array('A'=>'')).'A',
					'souvisejici'=>array(),
					'varianta'=>1,
					'cat_id'=>$catid,
					'food_ids'=>array(),
				);
			} else {
				//print_r($l);die();
				$product[$l['code']] = [
					'id'=>$l['id'],
					'code'=>$l['code'],
					'name'=>$l['name'],
					'price'=>$l['price_vat'],
				];
			}
			//pr($product);
			if (isset($l['varianty'])){
				$l['varianty'] = unserialize($l['varianty']);
			}
			//pr($l['varianty']);
			if (isset($l['varianty']) && !empty($l['varianty'])){
			if ($api == false){
			
				$product[] = array(
					'id'=>$l['id'],
					'num'=>$l['num'],
					'name'=>$l['name'],
					'text'=>$l['text'],
					'gramaz'=>$l['varianty'][1]['gramaz'],
					'varianty'=>$l['varianty'],
					'price'=>$l['varianty'][1]['price'],
					'code'=>$l['code'].'B',
					'souvisejici'=>array(),
					'varianta'=>2,
					'cat_id'=>$catid,
					'food_ids'=>array(),
				);
			} else {
				//print_r($l);
				$product[$l['varianty'][0]['code']] = [
					'id'=>$l['id'],
					'code'=>$l['varianty'][0]['code'],
					'name'=>$l['name'],
					'price'=>$l['varianty'][0]['price'],
				];
				$product[$l['varianty'][1]['code']] = [
					'id'=>$l['id'],
					'code'=>$l['varianty'][1]['code'],
					'name'=>$l['name'],
					'price'=>$l['varianty'][1]['price'],
				];
			}
			}
			
		}
		return ($product);
		
	}
	public function productAtr($type){
		if ($type == 'category_damejidlo'){
			$this->loadModel('Shop.ShopProducts');
			$query = $this->ShopCategories->find()
			  ->where([])
			  ->select([
				'id',
				'name',
				'group_id',
			  ])
			 
			  ;
			$load = $query->toArray();
			//pr($load);
			$json = array();
			foreach($load AS $l){
				$l->rozvoz_type_id = $l->group_id;
				unset($l->group_id);
				unset($l->_locale);
				$json[] = $l;
				if ($l->id == 11){
					//$l->id = 10;
				}
			}
			if (isset($this->request->query['debug'])){
				pr($json);
			}
			//pr($json); 
			die(json_encode($json));
		}if ($type == 'category'){
			$this->loadModel('Shop.ShopProducts');
			$query = $this->ShopCategories->find()
			  ->where([])
			  
			  ->select([
				'id',
				'name',
				'group_id',
			  ])
			 
			  ;
			$load = $query->toArray();
			//pr($load);
			$json = array();
			foreach($load AS $l){
				$l->rozvoz_type_id = $l->group_id;
				unset($l->group_id);
				unset($l->_locale);
				if(in_array($this->system_id,[2,6,10])){
					if ($l->id == 11){
						$l->name = 'Špecle / halušky';
					}
				}
				$json[] = $l;
			
			}
			//pr($json); 
			die(json_encode($json));
		}
		if ($type == 'suroviny'){
			$this->loadModel('Shop.ShopProductAttributes');
			$query = $this->ShopProductAttributes->find()
			  ->where(['code IS NOT'=>null])
			  ->select([
				'id',
				'name',
				'price_vat',
				'group_id',
				'code',
			  ])
			  ->order('name ASC')
			 
			  ;
			$load = $query->toArray();
			//pr($load);
			$json = array();
			foreach($load AS $l){
				$l->cena = $l->price_vat;
				$l->kod = $l->code;
				unset($l->code);
				unset($l->price_vat);
				unset($l->_locale);
				$json[] = $l;
			}
			//pr($json); 
			die(json_encode($json));
		}
	}
	
}
