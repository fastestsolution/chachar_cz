<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace Shop\Controller;

use Shop\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\I18n\Time;

use lessc\lessc;
use Cake\ORM\TableRegistry;
use Cake\I18n\I18n;

class ShopOrdersController extends AppController
{
	
	public $helpers = [
        'Fastest'
    ];
	
	public function basketContent(){
		$this->viewBuilder()->autoLayout(false);
		
		$this->render('Element/Basket/basket_content');
	}
	
	private function creditsFromOrder($total_price){
		$credits_total = 0;
		if($total_price > 100){
			$credits_total = ($total_price/100)*1;				
		}
		if($total_price > 300){
			$credits_total = ($total_price/100)*3;				
		}
		if($total_price > 500){
			$credits_total = ($total_price/100)*6;	
						
		}
		$product_credits = 0;
		foreach($this->basket_products['products'] AS $k=>$p){
			if (isset($p['credit'])){
				$product_credits += $p['credit'];
			}
		}
		
		$credits_total = ceil($credits_total+$product_credits);
		
		//pr($this->basket_products);die();
		return $credits_total;
	}
	
	// change provoz id from pokladna
	public function changeOrder($order_id=null,$provoz_id=null,$provoz_change_id = null){
		if ($order_id == null) die('neni order_id');
		if ($provoz_id == null) die('neni provoz_id');
		if ($provoz_change_id == null) die('neni provoz_change_id');
		
		$this->loadModel('Shop.ShopProvozs');
		$this->basket_provoz_list = $this->ShopProvozs->provozListAll();	
		//pr($this->basket_provoz_list['provoz_pokladna_id']);
		//pr($this->basket_provoz_list);
		//pr($this->basket_provoz_list['provoz_api_ids'][$provoz_id]);
		$this->loadModel('Shop.ShopOrders');
		$order = $this->ShopOrders->find()
			->where(['id'=>$order_id,'shop_provoz_id'=>$this->basket_provoz_list['provoz_api_ids'][$provoz_id]])
			->select([
			])
			->first();
		//pr($order);die('a');
		if (!$order){
			$json = json_encode(array('result'=>false,'message'=>'Nelze změnit'));
			echo isset($_GET['callback'])? "{$_GET['callback']}($json)": $json;
			die();
			
		}
		//pr($this->basket_provoz_list);
		$save = array(
			'id'=>$order['id'],
			'shop_provoz_id'=>$this->basket_provoz_list['provoz_api_ids'][$provoz_change_id],
			'preposlano_provoz_id'=>$this->basket_provoz_list['provoz_api_ids'][$provoz_id],
			'created'=>new Time(),
			'preposlano_date'=>$order->created,
			'shop_payment_id'=>$order->shop_payment_id,
		);
		//pr($save);die();
		$save_data = $this->ShopOrders->newEntity($save);
		if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
		//pr($save_data);
		//pr($this->basket_provoz_list['provoz_list']);
		//pr($this->basket_provoz_list['provoz_list'][$this->basket_provoz_list['provoz_api_ids'][$provoz_change_id]]);
		//die();
		}
		$this->ShopOrders->save($save_data);
		$json = json_encode(array('result'=>true,'message'=>'Posláno na provoz: '.$this->basket_provoz_list['provoz_list'][$this->basket_provoz_list['provoz_api_ids'][$provoz_change_id]]));
		echo isset($_GET['callback'])? "{$_GET['callback']}($json)": $json;
		die();
		/*
		$this->loadModel('RozvozXml');
		$xml = $this->RozvozXml->find('first',array('conditions'=>array('rozvoz_order_id'=>$order_id,'rozvoz_provoz_id'=>$provoz_id)));
		if (!$xml){
			$json = json_encode(array('result'=>false,'message'=>'Nelze změnit'));
			echo isset($_GET['callback'])? "{$_GET['callback']}($json)": $json;
			die();
			
		}
		*/
		$save = array(
			'id'=>$xml['RozvozXml']['id'],
			'rozvoz_provoz_id'=>$provoz_change_id,
			'predano_id'=>$provoz_id,
			'created'=>date('Y-m-d H:m:s'),
			'created_preposlano'=>$xml['RozvozXml']['created'],
		);
		
		if ($xml['RozvozXml']['zdroj'] == 2){
			$this->change_order_mpizza($order_id,$provoz_id,$provoz_change_id);
		}
		
		//pr($save);
		$this->RozvozXml->save($save);
		$this->RozvozXml->id = null;
		$json = json_encode(array('result'=>true,'message'=>'Posláno na provoz: '.$provoz_list[$provoz_change_id]));
		echo isset($_GET['callback'])? "{$_GET['callback']}($json)": $json;
		die();
		
	}
	
	// load logged user
	private function load_logged_user(){
		
		// load address
		$this->loadModel('UserAddresses');
		$address = $this->UserAddresses->find()
			->where(['shop_client_id'=>$this->loggedUser['id']])
			->select([
			])
			->toArray();
		$this->set('address_basket',true);
		if (isset($address[1]) && !empty($address[1]['street']) && $address[1]['street'] != $address[0]['street']){
			$this->request->data['check_dorucovaci'] = 1;
			//pr($this->request->data);
			//pr($address[1]['street']);
		}
		
		$this->loadModel('Users');
		$this->loadUserCredits();
		//pr($credits);
		//pr($this->loggedUser);
		
		$this->request->data['user'] = $this->loggedUser;
		$this->request->data['user'] = $this->Users->loadPhonePref($this->request->data['user']);
		$this->request->data['user']['user_addresses'] = $address;
		//pr($this->request->data['user']['user_addresses']);
		
		if (empty($this->request->data['user']['user_addresses'])){
			//if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
			if (isset($_COOKIE['addressChachar']) && !empty($_COOKIE['addressChachar'])){
				$cookieAddr = json_decode($_COOKIE['addressChachar']);
				//pr($cookieAddr);
				$this->request->data['user']['user_addresses'][0]['lat'] = $cookieAddr->address_lat;
				$this->request->data['user']['user_addresses'][0]['lng'] = $cookieAddr->address_lng;
				$this->request->data['user']['user_addresses'][0]['street'] = $cookieAddr->address_street;
				$this->request->data['user']['user_addresses'][0]['cp'] = $cookieAddr->address_cp;
				$this->request->data['user']['user_addresses'][0]['city'] = $cookieAddr->address_city;
				if (isset($_COOKIE['provoz_id']))
				$this->request->data['shop_provoz_id'] = $_COOKIE['provoz_id'];
				//pr($_COOKIE['provoz_id']);
			}
			//}
		}
	}
	
	private function checkUserCredits(){
		//pr($this->request->data);
		if (isset($this->loggedUser)){
			if ($this->request->data['credit_use'] > $this->loggedUser['credits']){
				
				die(json_encode(['r'=>false,'m'=>__('Váš účet neobsahuje požadovaný počet kreditů')]));
			}
			if ($this->request->data['credit_use'] > 0)
			if ($this->request->data['credit_use_before'] != ($this->request->data['credit_use_after'] + $this->request->data['credit_use'])){
				
				die(json_encode(['r'=>false,'m'=>__('Byl ovlivněn počet kreditů, posláno na technickou podporu')]));
			}
			
			//pr($this->request->data['credit_use']);
			//pr($this->loggedUser['credits']);
			//die();
		}
	}
	
	private function saveUserCredits($order_id = null,$user_credits=null){
		if (isset($this->loggedUser)){
			//pr($this->request->data);
			//die();
			//pr($order_id);
			if ($order_id != null){
				$this->loadModel('Shop.ShopOrders');
				$credits = $this->ShopOrders->find()->where(['id'=>$order_id])->select(['credit_use_after','credits_from_order'])->hydrate(false)->first();
				$this->request->data['credit_use_after'] = $credits['credit_use_after'];
				$this->request->data['credits_from_order'] = $credits['credits_from_order'];
			}
			//pr($this->request->data['credit_use_after']);
			if (isset($this->request->data['credit_use_after'])){
				//pr($this->loggedUser);
				$this->loadModel('Users');
				$this->Users->updateAll(
					['credits' => $this->request->data['credit_use_after'] + $this->request->data['credits_from_order']], // fields
					['id' => $this->loggedUser['id']]
				);
						
				$this->loggedUser['credits'] = $this->request->data['credit_use_after'];
				$this->set('loggedUser',$this->loggedUser);
				$this->Auth->setUser($this->loggedUser);
			}
			
			
		
		}
			if ($user_credits != null){
				//pr($user_credits);
				$this->loadModel('Users');
				$this->Users->updateAll(
					['credits' => $user_credits + $this->request->data['credits_from_order']], // fields
					['id' => $this->user_data['id']]
				);
						
				
			}
	}
	private function loadUserCredits(){
		if (isset($this->loggedUser)){
		//pr($this->loggedUser);
		$this->loadModel('Users');
		$credits = $this->Users->find()->where(['id'=>$this->loggedUser['id']])->select(['credits'])->hydrate(false)->first();
		$this->loggedUser['credits'] = $credits['credits'];
		$this->set('loggedUser',$this->loggedUser);
		$this->Auth->setUser($this->loggedUser);
					
		}
	}
	
	
	
	public function converProvozId($provozovna_id){
		if ($this->system_id == 1){ // pokud je mutace chachar
		$this->viewBuilder()->autoLayout(false);
		// presmerovani rano
		$redirect_morning = array(
			1=>[ // dubina rano stahuje
				4, // zabreh
				7, // vyskovice
				1, // dubina
				
			],
			5=>[ // poruba sever rano stahuje
				8, // poruba jih
				5, // poruba sever
			],
		);
		
		// presmerovani weekend
		$redirect_weekend = [
			7=>[ // vyskovice odpoledne a weekend stahuje
				//12, // hrabova
				4, // zabreh
				1, // dubina
				7, // vyskovice
			],
			8=>[ // poruba jih odpoledne a weekend
				5, // poruba sever
				8, // poruba jih
			],
		];
		
		$def_time_to = date('Y-m-d 10:15:00');
		$def_time_from = date('Y-m-d 21:45:00');
		$def_time_from2 = date('Y-m-d 22:45:00');
			
		$time_to = strtotime($def_time_to);
		$time_from = strtotime($def_time_from);
		$time_from2 = strtotime($def_time_from2);
			
		$current_time = strtotime(date('Y-m-d H:i:s'));
		$dw = date( "w", $current_time);
		$so_ne_list = array(5,6);
			
		$con_provoz_id = $provozovna_id;
			
		if ($current_time < $time_to){
			foreach($redirect_morning AS $k=>$p){
				if (in_array($provozovna_id,$p)){
					$con_provoz_id = $k;
								
				}
			}					
		}
			
		if (($current_time > $time_from2 && in_array($dw,$so_ne_list))){
			foreach($redirect_weekend AS $k=>$p){
				if (in_array($provozovna_id,$p)){
					$con_provoz_id = $k;
				}
			}
			
		}
		if ($current_time > $time_from && !in_array($dw,$so_ne_list)){
			if (isset($redirect_weekend[$provozovna_id])){
			}
			foreach($redirect_weekend AS $k=>$p){
				if (in_array($provozovna_id,$p)){
						
					$con_provoz_id = $k;
				}
			}
		}
		
			return $con_provoz_id;
		} else {
			return $provozovna_id;
		}
		
	}
	
	// kontrola zda posledni 3 objednavky nebyly placeny body
	private function checkPlatbaBody(){
		if(isset($this->loggedUser) && $this->request->data['shop_payment_id'] == 3){
			$find_orders = $this->ShopOrders->find()
			->where(['shop_client_id'=>$this->loggedUser['id']])
			->select(['id','shop_payment_id'])
			->limit(3)
			->order('id DESC')
			->toArray();
			$is_body_use = false;
			
			if ($find_orders){
				foreach($find_orders AS $f){
					if ($f->shop_payment_id == 3){
						$is_body_use = true;
					}
				}
				//pr($find_orders);
			}
			if ($is_body_use == true){
				die(json_encode(['r'=>false,'m'=>'Platbu pizza body lze použít pouze jednou za 3 objednávky']));
			}
		}
		//die();
	}
	
	// save order
	private function save_order($orders){
		$this->checkUserCredits();
		if ($this->basket_products['total']['total_vat'] < 119){
			die(json_encode(['r'=>false,'m'=>__d('shop','Minimální částka objednávky je 119,-')]));
		}
		//if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
			$this->checkPlatbaBody();
		//}
		
		if (in_array(date('Y-m-d'),['2017-12-24','2017-12-25','2017-12-26','2017-12-31'])){
		$this->request->data['shop_provoz_id'] = $this->request->data['shop_provoz_id'];
		
		} else {
		$this->request->data['shop_provoz_id'] = $this->converProvozId($this->request->data['shop_provoz_id']);
			
		}
		//pr($this->request->data);
		//pr($this->request->data['user']['user_addresses'][0]['cp']);
		if (empty($this->request->data['user']['user_addresses'][0]['cp']) || $this->request->data['user']['user_addresses'][0]['cp'] == ',' || $this->request->data['user']['user_addresses'][0]['cp'] == ',,'){
			die(json_encode(['r'=>false,'m'=>'Zadejte adresu včetně čísla popisného']));
		}
		
		
		$regex = '/^[0-9\-\\,.]+/';
		//if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
		//$regex = '/^[0-9\-\\,.]+/';
		//}
		$is_valid = preg_match($regex, $this->request->data['user']['user_addresses'][0]['cp']);
		if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
		//pr($this->request->data['user']['user_addresses'][0]['cp']);
		//pr($is_valid);die();
		}	
		if ($is_valid == 0){
			die(json_encode(['r'=>false,'m'=>'Zadejte adresu včetně čísla popisného']));
			
		}
		
		//}
		
		
		//if ($_SERVER['REMOTE_ADDR'] != '89.103.18.65'){
			// check doba list
			$this->loadModel('Shop.ShopProvozs');
			$this->basket_provoz_list = $this->ShopProvozs->provozListAll();	
			$day_week = date('w');
			if ($day_week > 0){
				$day_week = $day_week - 1;
			} else {
				$day_week = 6;
			}
			//date('w') = date('w') - 1;
			if (isset($this->basket_provoz_list['doba_list'][$this->request->data['shop_provoz_id']][$day_week])){
				$db_time = $this->basket_provoz_list['doba_list'][$this->request->data['shop_provoz_id']][$day_week];
				//pr($day_week);die();
				$time = explode(' - ',$db_time);
				if (isset($time[1])){
				$time[1] = date('H:i',strtotime('-30 minutes',strtotime($time[1])));	
				//pr($time[1]);die();
				$from = strtotime($time[0]);
				$to = strtotime($time[1]);
				
				$current = strtotime(date('H:i'));
				if ($current > $from && $current < $to){
					
				} else {
					die(json_encode(['r'=>false,'m'=>__d('shop','Provozovna otevřeno v době '.$db_time)]));
				}
				//pr(date('H:i'));
				}
				//pr($time);
			}
			
			//die();
		//}
		//pr($this->request->data);die();
		//pr($this->basket_products);
		$this->loadModel('Users'); 
		
		//if (strlen($this->request->data['user']['phone_pref']) != 4){
			//die(json_encode(['r'=>false,'m'=>'Kód země musí být ve formátu +420']));
		//}
		$this->request->data['user'] = $this->Users->savePhonePref($this->request->data['user']);
		
		$this->request->data['price'] = $this->basket_products['total']['total'];
		$this->request->data['price_vat'] = $this->basket_products['total']['total_vat'];
		
		if (isset($this->request->data['credit_use']) && $this->request->data['credit_use'] > 0){
			if ($this->request->data['credit_use'] < 100){
				die(json_encode(['r'=>false,'m'=>'Minimálně musíte použít 100 kreditů']));
			}
			$this->request->data['price_vat'] = $this->request->data['price_vat'] - $this->request->data['credit_use'];
		}
		if (isset($this->request->data['spropitne']) && $this->request->data['spropitne'] > 0){
			$this->request->data['price_vat'] = $this->request->data['price_vat'] + $this->request->data['spropitne'];
		}
		//pr($this->basket_products['total']);
		
		// modify sessio products to save 
		$save_products = [];
		if (empty($this->basket_products['products'])){
			die(json_encode(['r'=>false,'m'=>__d('shop','Nejsou vloženy žádné produkty')]));
		}	
		
		if (!empty($this->basket_products['products'])){
			
			foreach($this->basket_products['products'] AS $prod){
				//pr($prod);
				$save_products[] = [
					'shop_product_id'=>$prod['id'],
					'code'=>$prod['code'],
					'name'=>$prod['name'],
					'count'=>$prod['count'],
					'price'=>$prod['price']*$prod['count'],
					'price_vat'=>$prod['price_vat']*$prod['count'],
					'price_per_item'=>$prod['price'],
					'price_vat_per_item'=>$prod['price_vat'],
					'tax_vat'=>$prod['tax_id'],
					'attributes_ids'=>$prod['attributes_ids'],
					'attributes_price'=>$prod['attributes_price'],
				];
			}
		} else {
			die(json_encode(['r'=>false,'m'=>__d('shop','Nejsou vloženy žádné produkty')]));
			
		}
		
		$this->request->data['shop_order_items'] = $save_products;
		$this->request->data['system_id'] = $this->system_id;
		
		
		// save newsleter email
		if (isset($this->request->data['newsletter']))
		if($this->request->data['newsletter'] == 1){
			
			if (isset($this->loggedUser)){
				$this->request->data['user']['email'] = $this->loggedUser['email'];
				$noCheckEmail = true;
			}
			$this->saveNewsletter($this->request->data['user']['email']);
		}
		
		// pokud je neregistrovany a email existuje priradit user id
		if (isset($this->request->data['user']['email'])){
			
			$user_data = $this->findUser($this->request->data['user']['email']);
			if ($user_data){
				
				unset($user_data['password']);
				$this->request->data['user']['id'] = $user_data['id'];
				$noCheckEmail = true;
			}
		}
		
		// pokud je prihlasen
		if (isset($this->loggedUser)){
			$noCheckEmail = true;
			$this->request->data['reg'] = 1;
		}
		// pokud je neregistrovan
		if (!isset($this->loggedUser) && isset($this->request->data['user']['noreg'])){
			$noCheckEmail = true;
			$this->request->data['reg'] = 0;
			$this->request->data['user']['reg'] = 0;
			$this->request->data['user']['system_id'] = $this->system_id;
			
		
		} else if(!isset($this->request->data['user']['noreg'])){
			$this->request->data['reg'] = 1;
			$this->request->data['user']['reg'] = 1;
			$this->request->data['user']['system_id'] = $this->system_id;
			
		}
		
		if (isset($this->request->data['user']['password'])){
			$this->request->data['user']['password'] = md5($this->request->data['user']['password']);
			$this->request->data['user']['password2'] = md5($this->request->data['user']['password2']);
			$this->request->data['user']['credits'] = 20;
		}
		
		
		//pr($this->request->data);
		
		$this->request->data['client_name'] = $this->request->data['user']['first_name'].' '.$this->request->data['user']['last_name'];
		$this->request->data['client_telefon'] = $this->request->data['user']['phone'];
		$this->request->data['browser'] = $_SERVER['HTTP_USER_AGENT'];
		
		
		//pr($this->request->data);die();
		
		// add credits from order total price
		if (isset($this->request->data['credit_use_before']) && empty($this->request->data['credit_use_after'])){
			$this->request->data['credit_use_after'] = $this->request->data['credit_use_before'];
		}
		if (isset($this->request->data['credit_use']) && $this->request->data['credit_use'] > 0){
			if ($this->request->data['credit_use_after'] == $this->request->data['credit_use_before']){
				$this->request->data['credit_use_after'] = $this->request->data['credit_use_after'] - $this->request->data['credit_use'];
			}
		}
		$this->checkUserCredits();
		$this->request->data['credits_from_order'] = $this->creditsFromOrder($this->request->data['price_vat']);
		
		$this->dopravaPriceCheck();
		
		//if ($_SERVER['REMOTE_ADDR'] == '46.135.100.227'){
		$this->request->data['createdTime'] = strtotime(date('Y-m-d H:i:s'));
		
		//}
		$this->ShopOrders->patchEntity($orders, $this->request->data(),['associated' => ["Users"=>['validate' => (isset($noCheckEmail)?'OnlyCheckPass':'default'),'accessibleFields'=>['id' => true]], "Users.UserAddresses","ShopOrderItems"]]);
		//pr($orders);
		// if logged user add user id
		if (isset($this->loggedUser)){
			$orders->user->id = $this->loggedUser['id'];
		}
		
		//if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
		//pr($orders);die();
		if (isset($this->request->data['user']['email'])){
		
			if ($user_data = $this->findUserReg($this->request->data['user']['email'])){
				$orders->user->reg = 1;
				$orders->user->id = $user_data->id;
				$orders->reg = 1;
				$this->is_exist_client = true;
				$this->user_data = ['id'=>$user_data->id,'credits'=>$user_data->credits];
				//die(json_encode(['r'=>false,'m'=>__d('shop','Váš email je již zaregistrován, prosím přihlašte se')]));
			} else {
				
			}
		}
		//die();
		//}
		if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
			//pr($orders);die();
		
		}
		$this->check_error($orders);
		
   
    try{
       $result = $this->ShopOrders->save($orders);
    }catch(\Exception $e){
       if($e->getCode() == 23000){
       	    die(json_encode(['r'=>false,'m'=> __d('shop','Klient s tímto emailem  se již v databázi nachází. Prosím přihlašte se.')]));
       }                                                                                 
    }
        
  //     $result = $this->ShopOrders->save($orders);
		// save order
		if ($result) {
			//pr($result);die();
			$this->ShopOrders->updateAll(
				['user_address_id' => $result->user->user_addresses[0]->id], // fields
				['id' => $result->id]
			);
				
			
			// if logged user update data
			$this->update_user_data($result);
			
			// generate done hash
			$hash_url = $this->generateDoneHash($result);
			
			
			// heureka overeno
			//$this->heureka($result);
			
			// gopay redirect
			$this->gopay_check($result);
			
			// pokud je platba gopay
			if (isset($this->gopay_result)){
				
				die(json_encode(['r'=>true,'m'=>__d('shop','Objednávka byla uložena, nyní budete přesměrováni na platební bránu'),'gopay'=>$this->gopay_result]));
		
			} else {
				if ($_SERVER['REMOTE_ADDR'] != '89.103.18.65'){
				// delete basket session
				$this->Session->delete('basket_products');
				
				}
				$this->sendEmailCompleteOrder($result->id);
				// save credit
				if (isset($this->is_exist_client)){
					$this->saveUserCredits($result->id,$this->user_data['credits']);
				
				} else {
					$this->saveUserCredits();
					
				}
				
				die(json_encode(['r'=>true,'m'=>__d('shop','Objednávka byla uložena'),'redirect'=>'/'.KOSIK_LINK.'/complete/']));
				
			}
		} else {
			die(json_encode(['r'=>false,'m'=>__d('shop','Chyba uložení objednávky')]));
		}
		
		
		//die('aa');
		
	}
	
	private function findUserReg($email){
		$this->loadModel('ShopClients');
		$find_email = $this->ShopClients->find()
			->where(['email'=>$email,'reg'=>1])
			->order('id')
			->first();
		if ($find_email){
			return $find_email;
		} else {
			return false;
		}
	}
	
	// save newsletter email
	private function saveNewsletter($email){
		$this->loadModel('Newsletters');
		
		$find_email = $this->Newsletters->find()
			->where(['email'=>$email])
			->first();
			
		if (!$find_email){
			$save_email = [
				'email'=>$email,
			];
			
			$save_newsletter = $this->Newsletters->newEntity($save_email);
			$this->Newsletters->save($save_newsletter);
		}
			
	}
	
	public function rozvoceAddOrder(){
		//print_r($_POST);
		if (!isset($_POST['rozvozce_id']) || !isset($_POST['order_id'])|| !isset($_POST['pokladna_id'])){
			header('Content-Type: text/html; charset=utf-8');
			die('Chybné parametry');
		}
		$this->loadModel('Shop.ShopProvozs');
		$this->pokladna_list = $this->ShopProvozs->pokladnaList();	
		//print_r($this->pokladna_list);
		//print_r($this->pokladna_list['list'][$_POST['pokladna_id']]);
		$_POST['order_data']['zdroj_id'] = $this->pokladna_list['system_id'][$this->pokladna_list['list'][$_POST['pokladna_id']]];
		//print_r($_POST);die();
		$this->loadModel('MobileOrders');
		if (!isset($_POST['order_data']['lat']) || empty($_POST['order_data']['lat'])){
			
		
			$address = $_POST['order_data']['client_ulice'].' '.$_POST['order_data']['client_mesto']; // Google HQ
			$prepAddr = str_replace(' ','+',$address);
			$geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false&key=AIzaSyCa3rXQL2tniYzsafvWqIofD5ChB9fY5Dk');
			$output= json_decode($geocode);
			if (isset($output->results[0]->geometry->location->lat)){
			
				$lat = $output->results[0]->geometry->location->lat;
				$lng = $output->results[0]->geometry->location->lng;
				$_POST['order_data']['client_lat'] = $lat;
				$_POST['order_data']['client_lng'] = $lng;
			
			}
		}
		if (!empty($_POST['order_data']['order_web_id'])){
			$find_client = $this->ShopOrders->find()
			->where(['id'=>$_POST['order_data']['order_web_id']])
			->select('shop_client_id')
			->first();
			if ($find_client){
				$_POST['order_data']['shop_client_id'] = $find_client->shop_client_id;
			}
			//print_r($find_client->shop_client_id);die('aaa');
			//$find_order['Order']['web_id']
		}
		
		//print_r($lng);
		$time = new Time($_POST['order_data']['created_pokladna']);
		$_POST['rozvozce_date_nalozeni'] = new Time();
		switch($_POST['order_data']['doba_id']){	
			case 1: $doba_date = $time->modify('+1 hours');break;
			case 2: $doba_date = $time->modify('+75 minutes');break;
			case 3: $doba_date = $time->modify('+90 minutes');break;
			case 4: $doba_date = $time->modify('+2 hours');break;
			case 5: $doba_date = 'dovezeme na Vámi určený čas';break;
		}
		//print_r($_POST['order_data']['created']);
		//print_r($doba_date);die('a');
		$_POST['order_data']['created_pokladna'] = new Time($_POST['order_data']['created_pokladna']);
		$_POST['order_data']['doba_date'] = $doba_date;
		
			//pr($this->pokladna_list);
			
		
		$save_mobile_order = $this->MobileOrders->newEntity($_POST['order_data']);
		//print_r($save_mobile_order);die();
		
		$this->MobileOrders->save($save_mobile_order);
		
		//print_r($_POST);die();
		$this->ShopOrders->updateAll(
				[
					'rozvozce_id' => $_POST['rozvozce_id'],
					'stav_id' => 3,
					'rozvozce_date_nalozeni' => new Time(),
				
				], // fields
				['id' => $_POST['order_id'],'shop_provoz_id' => $this->pokladna_list['list'][$_POST['pokladna_id']],]
		);
				
		
		die(json_encode(['r'=>true]));
	}
	
	// send email order complete 
	public function sendEmailCompleteOrder($id,$type=null,$cas=null){
		$order = $this->ShopOrders->getOrder($id);
		
		
		// load provoz list
		$this->loadModel('Shop.ShopProvozs');
		$this->basket_provoz_list = $this->ShopProvozs->provozListAll();	
		
		//pr($order);die();
		$data_email = json_decode(json_encode($order),true);
		//pr($data_email);die();
		
		//pr($this->basket_provoz_list['data'][$data_email['shop_provoz_id']]);
		if (isset($this->basket_provoz_list['data'][$data_email['shop_provoz_id']])){
		
		$data_email['provoz_name'] = $this->basket_provoz_list['data'][$data_email['shop_provoz_id']]['name'];
		$data_email['provoz_telefon'] = $this->basket_provoz_list['data'][$data_email['shop_provoz_id']]['telefon'];
		$data_email['shop_payment']['name'] = $this->chachar_payment_list[$data_email['shop_payment_id']]['name'];
		}
		if ($type == null){
			$confirm_links = '<ul id="confirm_links">';
			$confirm_links .= '<li><a href="'.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/confirm_email/1/'.$data_email['id'].'">Dovezeme do 1 hodiny</a></li>';
			$confirm_links .= '<li><a href="'.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/confirm_email/2/'.$data_email['id'].'">Dovezeme do 1.25 hodiny</a></li>';
			$confirm_links .= '<li><a href="'.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/confirm_email/3/'.$data_email['id'].'">Dovezeme do 1.5 hodiny</a></li>';
			$confirm_links .= '<li><a href="'.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/confirm_email/4/'.$data_email['id'].'">Dovezeme do 2 hodin</a></li>';
			$confirm_links .= '</ul>';
			$data_email['confirm_link'] = $confirm_links;
		} else {
			
			$this->ShopOrders->updateAll(
					['stav_id' => 2,'provoz_date_potvrzeni' => new Time()], // fields
					['id' => $id]
			);
			
			switch($type){
				case 1: $cas = '1 hodina';break;
				case 2: $cas = '75 minut';break;
				case 3: $cas = '1.5 hodiny';break;
				case 4: $cas = '2 hodiny';break;
				case 5: $cas = 'Časovka';break;
			}
			$data_email['cas'] = $cas;
		}
		// load ShopProductAttributes
		$this->loadModel('Shop.ShopProductAttributes');
		$this->set('attributes_list',$this->attributes_list = $this->ShopProductAttributes->attributesList());
		//pr($this->attributes_list);
		$suroviny_list = [];
		foreach($this->attributes_list AS $atr){
			$suroviny_list[$atr->code] = [
				'id'=>$atr->id,
				'name'=>$atr->name,
				'kod'=>$atr->code,
				'cena'=>$atr->price_vat,
			];
		}
		//pr($suroviny_list);
		foreach($data_email['shop_order_items'] AS $ok=>$oi){
			$name = $data_email['shop_order_items'][$ok]['name'];
			if (isset($oi['attributes_ids']) && !empty($oi['attributes_ids'])){
				$attributes = json_decode($oi['attributes_ids']);
				foreach($attributes AS $atr=>$ks){
					if ($data_email['zdroj_id'] > 1){
						$name .= '<br />'.$ks.'ks '.$suroviny_list[$atr]['name'].' '.$suroviny_list[$atr]['cena'].',-';
					
					} else {
						$name .= '<br />'.$ks.'ks '.$this->attributes_list[$atr]['name'].' '.$this->attributes_list[$atr]['price_vat'].',-';
						
					}
					//pr($atr);
				}
				
			}
			$data_email['shop_order_items'][$ok]['name'] = $name;
			
		}
		// kredity
		if (isset($data_email['credit_use']) && $data_email['credit_use'] > 0){
			$data_email['shop_order_items'][1000]= [
				'name'=>'Slevový kupón',
				'count'=>1,
				'price_vat'=>'-'.$data_email['credit_use'],
				'price_vat_per_item'=>'-'.$data_email['credit_use'],
			];
				
		}
		//pr($data_email);die();
		
		$this->loadComponent('Email');
		//$this->basket_provoz_list['data'][$data_email['shop_provoz_id']]['order_email'] = 'test2@fastest.cz';
		if ($type == null){
			if (!empty($this->basket_provoz_list['data'][$data_email['shop_provoz_id']]['order_email']))
			$email_to = [$this->basket_provoz_list['data'][$data_email['shop_provoz_id']]['order_email']];
		} else {
			$email_to = [$order->user->email];
		}
		//pr($this->basket_provoz_list['data'][$data_email['shop_provoz_id']]);
		//pr($this->basket_provoz_list['data']);
		if (!empty($email_to)){
		if ($email_to[0] != ' '){
		
		$opt = [
			'to'=>$email_to,
			'template_id'=>3,
			'data'=>$data_email,
			'price_format'=>[
				'price_vat_per_item',
				'price_payment',
				'price_vat_payment',
				'price_transport',
				'price_vat_transport',
			],
		];
		//pr($opt);die();
		$this->Email->send($opt);
		}
		//pr($data_email);die();
		}
	}
	
	// generate done hash after save order 
	private function generateDoneHash($result){
		//pr($result);
		$hash = [
			'order'=>$result,
			'server'=>$_SERVER['HTTP_HOST'],
		];
		$hash_url = $this->encode_long_url(json_encode($hash));
		
		$this->Session->write('hash_url',$hash_url);
		
		return $hash_url;
				
	}
	
	// heureka overeno
	public function heureka($result){
		if (isset($this->setting['heureka_overeno']) && !empty($this->setting['heureka_overeno'])){
		
		$this->loadModel('Users');
		$user_data = $this->Users->getUser($result->user->id);
		
		require_once ROOT.'/vendor/heureka/heureka.php';
					 //224f6e3e7871feab35213eeda4518036
		try  {
			$overeno = new \HeurekaOvereno($this->setting['heureka_overeno']);
			// pro slovenske obchody $overeno = new HeurekaOvereno('API_KLIC', HeurekaOvereno::LANGUAGE_SK);
			$overeno->setEmail($user_data['email']);
			foreach($this->basket_products['products'] as $id => $product){
				$overeno->addProduct($product['name']);
			}
			//pr($overeno);
			$overeno->send();
		} catch (Exception $e) {
			print $e->getMessage();
		}

		}		
	}
	
	
	// check gopay payment from payment_id
	private function gopay_check($order_data){
		/*
		//pr($order_data);
		$this->loadModel('ShopPayments');
		$payment_data = $this->ShopPayments->find()
			->where(['id'=>$order_data->shop_payment_id])
			->select([
				'gopay_redirect'
			])
			->first();
			
		*/
		
		// pokud ma platba moznost gopay zavolej gopay generovani z ID objednavky a platby
		if ($order_data->shop_payment_id == 2){
			$this->gopay_payment($order_data->id);
		}
		
		if (isset($this->gopay_result)){
			//$this->redirect_order = '/'.KOSIK_LINK.'/'
			
		}
		
	}
	
	// check payment notify
	public function gopay_notify($gopay_id=null){
		//pr($_SERVER); 
		if ($_SERVER['REQUEST_SCHEME'] == 'http'){
			$redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
			header('HTTP/1.1 301 Moved Permanently');
			header('Location: ' . $redirect);
			exit();
		}
		$this->autoRender = false;
		if ($gopay_id == null && isset($this->request->query['id'])){
			$gopay_id = $this->request->query['id'];
		}
		if (!isset($gopay_id)){
			die('neni gopay id');
		}
		$this->loadModel('Shop.ShopOrders');
		$order = $this->ShopOrders->find()
			->where(['gopay_id'=>$gopay_id])
			->select([
			])
			->first();
		
		if (!$order){
			die('neni order');
		}
		
		if (isset($gopay_id)){
			//$order_id = $this->Session->read('order_id');
			//pr($order);
			
			// load provoz list
			$this->loadModel('Shop.ShopProvozs');
			$this->set('basket_provoz_list',$this->basket_provoz_list = $this->ShopProvozs->provozListAll());	
			
			
			
			if (empty($this->basket_provoz_list['data'][$order->shop_provoz_id]->gopay_id)){
				die(json_encode(['r'=>false,'m'=>__d('shop','Provoz nemá aktivní platební bránu')]));
			}
			
			
			$options = [
				'gopay_id'=>$this->basket_provoz_list['data'][$order->shop_provoz_id]->gopay_id,
				'gopay_client_id'=>$this->basket_provoz_list['data'][$order->shop_provoz_id]->gopay_client_id,
				'gopay_client_secure'=>$this->basket_provoz_list['data'][$order->shop_provoz_id]->gopay_secure,
			];
			
			$this->loadComponent('Shop.Gopay');		
			$this->payment_result = $this->Gopay->check_pay($options,$gopay_id);
			$this->set('payment_result',$this->payment_result);
			
			if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
			//pr($_SERVER['HTTP_USER_AGENT']);
			//$this->payment_result['r'] = true;
			
			}
			//pr($this->payment_result);
			// pokud je platba OK nastavit zaplaceno
			if ($this->payment_result['r'] == true){
				$this->ShopOrders->updateAll(
					['payment_recieved' => new Time()], // fields
					['id' => $this->payment_result['order_id']]
				);
				// delete basket session
				$this->Session->delete('basket_products');
				
				
				
				// save credit
				$this->saveUserCredits($this->payment_result['order_id']);
				
				$this->sendEmailCompleteOrder($this->payment_result['order_id']);
				
				// chatbot
				if ($this->Session->check('msi')){
								
					$ch = curl_init();
					$url = 'https://'.$this->Session->read('se').'-server.gobots.cz/'.$this->chatBotsProjects[$this->system_id].'/events/payment/';
					//pr($url);
					//pr($this->Session->read('msi'));
					$post = [
						'status'=>'paid',
						'messanger_id'=>$this->Session->read('msi'),
					];
					//pr($post);
					//die();
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));

					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

					$result = curl_exec ($ch);
					pr($result);
					curl_close ($ch);
				}
			} else {
				if ($this->Session->check('msi')){
					$ch = curl_init();
					$url = 'https://'.$this->Session->read('se').'-server.gobots.cz/'.$this->chatBotsProjects[$this->system_id].'/events/payment/';
					//pr($url);
					//pr($this->Session->read('msi'));
					$post = [
						'status'=>'canceled',
						'messanger_id'=>$this->Session->read('msi'),
					];
					//pr($post);
					//die();
					curl_setopt($ch, CURLOPT_URL, $url);
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));

					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

					$result = curl_exec ($ch);
					//pr($url);
					echo '<script type="text/javascript">window.close();</script>';
		
					die('chatbot');
					
				}
			}
				
				
		}
		if (isset($this->request->params['direct'])){
			die(json_encode($this->payment_result));
		}
	
	}
	
	// gopay payment
	public function gopay_payment($id){
		$model = 'ShopOrders';
		$order_data = $this->$model->find()
			->where(['ShopOrders.id'=>$id])
			->select([
			])
			->contain(['Users','Users.UserAddresses'])
			->first();
		
		$system_id = $order_data['system_id'];
		//pr($order_data);
		//$gp = HelperRegistry::get("Gopay");
		//pr($gp);
		//$loader = '../vendor/autoload.php';
		
		// load provoz list
		$this->loadModel('Shop.ShopProvozs');
		$this->set('basket_provoz_list',$this->basket_provoz_list = $this->ShopProvozs->provozListAll());	
		//pr($this->basket_provoz_list['data'][$order_data->shop_provoz_id]);
		//pr($order_data->shop_provoz_id);
		//die();
		if (empty($this->basket_provoz_list['data'][$order_data->shop_provoz_id]->gopay_id)){
			die(json_encode(['r'=>false,'m'=>__d('shop','Provoz nemá aktivní platební bránu')]));
		}
		$order = array(
			'gopay_id'=>$this->basket_provoz_list['data'][$order_data->shop_provoz_id]->gopay_id,
			'gopay_client_id'=>$this->basket_provoz_list['data'][$order_data->shop_provoz_id]->gopay_client_id,
			'gopay_client_secure'=>$this->basket_provoz_list['data'][$order_data->shop_provoz_id]->gopay_secure,
			//'gopay_secure_key'=>$this->basket_provoz_list['data'][$order_data->shop_provoz_id]->gopay_secure,
			'id'=>$order_data['id'],
			'description'=>$order_data['user']['last_name'].' '.$order_data['user']['first_name'],
			'price'=>$order_data['price_vat'],
			'client'=>array(
				'first_name'=>$order_data['user']['first_name'],
				'last_name'=>$order_data['user']['last_name'],
				'email'=>$order_data['user']['email'],
				'phone_number'=>$order_data['user']['phone'],
				'city'=>$order_data['user']['user_addresses'][0]['city'],
				'street'=>$order_data['user']['user_addresses'][0]['street'].' '.$order_data['user']['user_addresses'][0]['cp'],
				'country_code'=>'CZE',
			)
		);
		//pr($order);die();
		$this->loadComponent('Shop.Gopay');
		$this->gopay_result = $this->Gopay->create($order);
		if (isset($_GET['render'])){
			pr($this->gopay_result);die();
		}
		//pr($gopay_res);die();
		
	}
	
	// update logged user data 
	private function update_user_data($result){
		if (isset($this->loggedUser)){
			$this->loadModel('Users');
			$find = $this->Users->getUser($result->user->id);
			$this->Auth->setUser($find);
		}
	}
	
	// dokonceni objednavky
	private function basket_complete(){
		//$this->set('title',__d('shop','Děkujeme za Vaši objednávku'));
		$order = $this->Session->read('hash_url');
		//pr($order);
		if (isset($this->request->query['id'])){
			$gopay_id = $this->request->query['id'];
			
			$this->gopay_notify($gopay_id);
			//pr($this->payment_result);
			
			
		
		
		}
		//pr($this->request->query['id']);die();
		if ((isset($order) && !isset($this->payment_result)) || ($this->payment_result['r'] == true && !empty($order) )){
			$order = json_decode($this->decode_long_url($order),true);
			
			if (isset($this->loggedUser)){
				$this->set('credit_add',$order['order']['credits_from_order']);
			}	
			$this->loadModel('Shop.ShopProducts');
			$products = [];
			foreach($order['order']['shop_order_items'] AS $p){
				//$ean = $this->ShopProducts->getEan($p['shop_product_id']);
				$ean = ['ean'=>''];
				$products[] = [
					'name'=> $p['name'],    
					'sku'=> $ean['ean'],      
					'price'=> $p['price_vat'],                 
					'quantity'=> $p['count'],
				];
			}
			$ga_ecommerce = [
				'order'=>[
					'order_id'=>$order['order']['id'],
					'price_transport'=>0,
					'price'=>round($order['order']['price']),
					'domena'=>$order['server'],
				],
				'products'=>$products
			];
			$this->set('ga_ecommerce',$ga_ecommerce);
			
			//pr($ga_ecommerce);
			
			// send email done order
			//$this->sendEmailCompleteOrder($order['order']['id']);
		}
		
		
			
		
		$this->render('basket_complete');
	}
	
	// send confirm email from pokladna
	public function confirmEmail($type,$order_id,$later=null){ 
		//pr($order_id);
		$query = $this->ShopOrders->find()
		  ->contain([
			'ShopOrderItems',
			'Users'=> function ($q) {
				return $q->autoFields(false)
                    ->select([
						'id',
						'first_name',
						'last_name',
						'email',
						'phone',
						'phone_pref',
					]);
				}])
		 ->where(['ShopOrders.id'=>$order_id,'email_send IS'=>null])
		 // ->where(['ShopOrders.id'=>$order_id])
		  //->where(['ShopOrders.id'=>$order_id])
		  ->select([
		  ]);
		  
		$order = $query->first();
		if ($_SERVER['REMOTE_ADDR'] == '94.242.105.214'){
		
		}
		//pr($order);die();
		/*
		$price = 0;
		foreach($order->shop_order_items AS $it){
			pr($it->price_vat);
			$price += $it->price_vat;
				
			
		}
		pr($price);
		*/
		
		if (!$order){
			$result = array('message'=>'Potvrzení již bylo odesláno','result'=>false);
			$json = json_encode($result);
			echo isset($_GET['callback'])? "{$_GET['callback']}($json)": $json;
			die();
		}
		//pr($order);die();
		
		switch($type){
			case 1: $cas = 'do 1 hodiny';break;
			case 2: $cas = 'do 75 minut';break;
			case 3: $cas = 'do 1.5 hodiny';break;
			case 4: $cas = 'do 2 hodin';break;
			case 5: $cas = 'na Vámi určený čas';break;
		}
		
		if (isset($later) && $later == 1){
			$cas = 'na Vámi určený čas';
		}
		//pr($order);
		
		// load provoz list
		$this->loadModel('Shop.ShopProvozs');
		$this->set('basket_provoz_list',$this->basket_provoz_list = $this->ShopProvozs->provozListAll());	
		
		// sms text
		$sms_text = 'Vase objednavka na jmeno '.$order->user->first_name.' '.$order->user->last_name.' za cenu: '.$this->price($order->price_vat).' byla prijata na provozovnu '.$this->basket_provoz_list['data'][$order->shop_provoz_id]['name'].'. Objednavku dovezeme '.$cas.'. V pripade problemu volejte na provozovnu '.$this->basket_provoz_list['data'][$order->shop_provoz_id]['telefon'];
		//pr($sms_text);die();
		$this->loadComponent('Sms');
		$sms_tel = strtr($order->user->phone_pref,['00'=>'']).$order->client_telefon;
		$sms_tel = strtr($sms_tel,['++'=>'+']);
		if ($order->zdroj_id == 1){
			$this->Sms->send(1,[$sms_tel],$sms_text);
		}
		//pr($sms_tel);pr($sms_text);die();
		$this->sendEmailCompleteOrder($order_id,$type,$cas);
		//die('aa');
		 
		$this->ShopOrders->updateAll(
			['email_send' => new Time()], // fields
			['id' => $order_id]
		);
		
		$result = ['message'=>'Potvrzení SMS na telefon: '.$order->client_telefon,'result'=>true];
		$json = json_encode($result);
		
		echo isset($_GET['callback'])? "{$_GET['callback']}($json)": $json;
		
		die();
	}
	
	// find user if exist
	private function findUser($email){
		$this->loadModel('Shop.ShopClients');
		$query = $this->ShopClients->find()
		  ->where(['email'=>$email,'reg'=>0])
		  ->select([
		  ])
		  ->hydrate(false)
		  ;
		$data = $query->first();
		
		return $data;
	}
	
	/*** FORMAT PRICE **/
	public function price($price,$mena_suf = array()){
		
		//pr($mena_suf);
		$symbol_before = '';
		$kurz = null;
		$count = 1;
		$decimal = 0;
		$symbol_after = ',-';
		extract($mena_suf);
		
    	$white = array(" " => "");
		if ($kurz!=null)
			$price = ($price/strtr($kurz,',','.'))*$count;
		if ($price != null)
			return $symbol_before.number_format(strtr($price,$white), $decimal, '.', ' ').'<span class="price_suf">'.$symbol_after.'</span>';
		else
			return $symbol_before.number_format(strtr(0.00,$white), $decimal, '.', ' ').'<span class="price_suf">'.$symbol_after.'</span>';	
	}
	
	// detail basket
	public function basket_detail($params=null){
		if ($params == 'complete'){
			$this->basket_complete();
		}
		
		
		if (isset($this->request->query['noreg'])){
			$this->set('noreg',true);
		}
		if (isset($this->request->query['fastest'])){
			$this->set('fastest',true);
		}
		
		$this->set('col_class','col-md-3');
		$this->set('col_class2','col-md-4');
		$this->set('col_class3','col-md-2');
		$this->set('col_class_client','col-md-6');
		// load provoz list
		$this->loadModel('Shop.ShopProvozs');
		$this->set('basket_provoz_list',$this->basket_provoz_list = $this->ShopProvozs->provozListAll(true));
		
		// load ShopProductAttributes
		$this->loadModel('Shop.ShopProductAttributes');
		$this->set('attributes_list',$this->attributes_list = $this->ShopProductAttributes->attributesList());
		
		$this->dopravaPriceCheck();
		
		// load map area
		$this->loadModel('Shop.ShopMapAreas');
		$map_area_list = $this->ShopMapAreas->mapAreaList();
		$this->map_area_list = [];
		foreach($map_area_list AS $area_id=>$d){
			//pr($area_id);
			if (isset($this->basket_provoz_list['provoz_pokladna_id'][$area_id]) && $area_id != 1000){
				$this->map_area_list[$this->basket_provoz_list['provoz_pokladna_id'][$area_id]] = $d;
				
			}
		}
		$this->set('map_area_list',$this->map_area_list);
		//pr($map_area_list);
		//pr($this->map_area_list);
		
		
		$this->Session->delete('hash_url');
		
		$this->set('is_radio_button',true);
		$orders = $this->ShopOrders->newEntity();
		
		if ($this->request->is('ajax')){
			$this->save_order($orders);
		}
		
		$this->set('title',__d('shop','Váš košík'));
		
		//debug(get_class($this->ShopTransportPayments));die();
		
		$this->load_logged_user();
		
		$this->set(compact("orders"));
		
	}
	
	private function dopravaPriceCheck(){
		//if ($_SERVER['REMOTE_ADDR'] == '94.242.105.214' && in_array($this->system_id,[1])){
		//if (in_array($this->system_id,[4])){
			$this->set('is_doprava',true);
			$this->doprava_price_vat = 19;
			$this->doprava_price = 16;
			$this->request->data['doprava'] = $this->doprava_price_vat;
			$this->set('doprava_price',$this->doprava_price_vat);
			
		//}
		
	}
	
	// check doprava zdarma after before save order
	private function check_free_transport(){
		$transport_list = [];
		$this->transport_list = $this->Session->read('transport_list');
		foreach($this->transport_list AS $t){
			$transport_list[$t->id] = [
				'id'=>$t->id,
				'price'=>$t->price,
				'price_vat'=>$t->price_vat,
				'free_shipping_order_price'=>$t->free_shipping_order_price,
			];
		}
		//pr($transport_list);
		//pr($this->basket_products['transport']);
		//pr($this->basket_products['total']);
		//pr($this->basket_products['transport']['price_vat']);
		$product_price_vat = $this->basket_products['total']['products_vat'];
		if ($transport_list[$this->basket_products['transport']['id']]['free_shipping_order_price'] >0 && $transport_list[$this->basket_products['transport']['id']]['free_shipping_order_price'] < $product_price_vat){ 
			if ($this->basket_products['transport']['price_vat'] != 0){
				die(json_encode(['r'=>false,'m'=>__d('shop','Chyba nebyla aplikována doprava zdarma')]));
			}
		} else {
			if ($this->basket_products['transport']['price_vat'] != $transport_list[$this->basket_products['transport']['id']]['price_vat']){
				die(json_encode(['r'=>false,'m'=>__d('shop','Chyba ověření ceny dopravy, cena dopravy neodpovídá systémové ceně')]));
			}
		}
		
		
	}
	
	
	// add to kosik SESSION
	public function getTotalPrice(){
		$this->viewBuilder()->autoLayout(false);
		$this->basket_total_price = [
			'products'=>0,
			'products_vat'=>0,
			'count'=>0,
			'transport'=>0,
			'transport_vat'=>0,
			'payment'=>0,
			'payment_vat'=>0,
			'total'=>0,
			'total_vat'=>0,
		];
		$this->basket_products = $this->Session->read('basket_products');
		//pr('a');
		//pr($this->doprava_price);
		//pr($this->transport_price);
		//pr($this->payment_price);
		//pr('aaa');
		// add transport price
		if (isset($this->basket_products['transport'])){
			$this->basket_total_price['transport'] = $this->basket_products['transport']['price'];
			$this->basket_total_price['transport_vat'] = $this->basket_products['transport']['price_vat'];
		}
		
		// add payment price
		if (isset($this->basket_products['payment'])){
			$this->basket_total_price['payment'] = $this->basket_products['payment']['price'];
			$this->basket_total_price['payment_vat'] = $this->basket_products['payment']['price_vat'];
		}
		
		
		// add products price to total price	
		if (isset($this->basket_products) && isset($this->basket_products['products'])){
			foreach($this->basket_products['products'] AS $prod_id=>$prod){
				$this->basket_total_price['products'] += $prod['count'] * $prod['price'];
				$this->basket_total_price['products_vat'] += $prod['count'] * $prod['price_vat'];
				$this->basket_total_price['count'] += $prod['count'];
			}
		} else {
			die(json_encode(['r'=>false,'m'=>__d('shop','V košíku nemáte žádné produkty')]));
		}
		
		$total = 0;
		$total_vat = 0;
		//pr($this->basket_total_price);
		foreach($this->basket_total_price AS $k=>$t){
			//if (substr($k,-3))
			if ($k != 'count'){
				if (substr($k,-4) == '_vat'){
					$total_vat += $t;
			
				} else {
					$total += $t;
					
				}
			}
		}
		//pr($total);
		
		$this->dopravaPriceCheck();
		//pr($this->doprava_price);
		if (isset($this->doprava_price) && !empty($this->doprava_price)){
			$total = $total + $this->doprava_price;
			$total_vat = $total_vat + $this->doprava_price_vat;
		}
		//pr($total);
		
		$this->basket_total_price['total'] = $total;
		$this->basket_total_price['total_vat'] = $total_vat;
		//pr($this->basket_total_price);
		
		$this->basket_products['total'] = $this->basket_total_price;
		$this->Session->write('basket_products',$this->basket_products);
		//pr('a');
		if (isset($_GET['debug'])){
			pr($this->basket_total_price);
			die();
		}
		
	}	
	
	// delete from kosik SESSION
	public function delete_basket($product_id){
		$this->viewBuilder()->autoLayout(false);
		
		if ($this->request->is("ajax")){
			
			$basket_products = $this->Session->read('basket_products');
			if (!isset($basket_products['products'][$product_id])){
				die(json_encode(['r'=>false,'m'=>'Produkt nenalezen']));
			} else {
				unset($basket_products['products'][$product_id]);
			}
			$this->Session->write('basket_products',$basket_products);
			$this->getTotalPrice();
			
			
			
			
			die(json_encode(['r'=>true,'m'=>'Smazáno','basket_products'=>((count($this->basket_products['products'])>0)?$this->basket_products:'')]));
		} else {
			die(json_encode(['r'=>false,'m'=>'Chybne volani funkce']));
		}
	}

	
	// add to transport and payment SESSION
	public function set_order_transport($type,$id,$price_vat){
		$this->viewBuilder()->autoLayout(false);
		if (!$this->request->is("ajax")){
			die('chyba funkce');
		}
		
		// add to sessio transport
		if ($type == 'transport'){
			$this->transport_price = [
				'id'=>$id,
				'price'=>$price_vat - $price_vat*dphTypeConf,
				'price_vat'=>$price_vat,
			];
			//pr($this->transport_price);
			//$this->Session->write('transport_price',$this->transport_price);
			$this->basket_products['transport'] = $this->transport_price;
		}
		
		// add to session payment
		if ($type == 'payment'){
			$this->payment_price = [
				'id'=>$id,
				'price'=>$price_vat - $price_vat*dphTypeConf,
				'price_vat'=>$price_vat,
			];
			
			$this->basket_products['payment'] = $this->payment_price;
			//pr($this->transport_price);
			
		}
		//pr($this->basket_products);
		$this->Session->write('basket_products',$this->basket_products);
		
		$this->getTotalPrice();
		die(json_encode(['r'=>true,'total'=>$this->basket_total_price]));
	}	
		
	// add to kosik SESSION
	public function add_basket($params=null){
		//pr($_POST);
		//pr($this->request->data);
		if (isset($this->request->data['data'])){
			$stripsMenu = json_decode($this->request->data['data']);
			$stripsMenuConfig = json_decode($this->request->data['config']);
			$stripsMenuImg = $this->request->data['img'];
			//pr($stripsMenuConfig);
			//pr($stripsMenu);die();
			//pr($stripsMenuImg);
			$atrIds = [];
			if ($stripsMenuConfig->type == 1){
				$atrIds[103] = 1;
			}
			if ($stripsMenuConfig->type == 2){
				$atrIds[104] = 1;
			}
			$atrPrice = 0;
			foreach($stripsMenu AS $s){
				if (isset($s->id)){
					if (!isset($atrIds[$s->id])){
						$atrIds[$s->id] = 1;
					} else {
						$atrIds[$s->id] += 1;	
					}
					
				}
				if (isset($s->price)){
					$atrPrice += $s->price;
				}
			}
			/*
			$this->loadModel('ShopProducts');
			
			$query = $this->ShopProducts->find()
			  ->where(['code'=>211])
			  ->select([
			  ])
			;
			$data = $query->first();
			pr($data);
			*/
			
			// pokud session neexistuje vytvor
			if (!$this->Session->read('basket_products')){
				$basket_products = [];
			} else {
				$basket_products = $this->Session->read('basket_products');
			}
				
			
			$basket_products['products'][] = [
				'id'=>211,
				'shop_product_id'=>211,
				'name'=>'Strips MENU',
				'code'=>211,
				'number'=>211,
				'credit'=>0,
				'is_pizza'=>false,
				'price'=>$atrPrice,
				'price_vat'=>$atrPrice,
				'tax_id'=>1,
				'price_total'=>$atrPrice*1,
				'price_total_vat'=>$atrPrice*1,
				'count'=>1,
				'attributes_ids'=>json_encode($atrIds),
				'attributes_price'=>$atrPrice,
				'img'=>$stripsMenuImg,
						
			];
			//pr($basket_products);
			$this->Session->write('basket_products',$basket_products);
			$this->basket_products = $basket_products;	
			
			//pr($this->Session->read('basket_products'));
			//pr($_SESSION);
			$this->getTotalPrice();
			//$this->debug();
			$result = ['r'=>true,'m'=>'Přidáno do košíku','basket_products'=>$this->basket_products];
			//$this->viewBuilder()->className('Json');
			//die(json_encode(['r'=>true,'m'=>'Přidáno do košíku','basket_products'=>$this->basket_products,'product'=>$find_product]));
						
			return $this->jsonResult($result);
	
			//die();
		}
		if (!isset($this->request->data['id'])){
			die(json_encode(['r'=>false,'m'=>'Neni definovan produkt']));
		}
		$this->viewBuilder()->autoLayout(false);
		
		if ($this->request->is("ajax")){
			if ($this->request->data['id'] == 335){
				$current_time = strtotime(date('H:i:s'));
				$from_time = strtotime(date('10:00:00'));
				$to_time = strtotime(date('14:00:00'));
				if ($current_time < $from_time || $current_time > $to_time){
					die(json_encode(['r'=>false,'m'=>__d('shop','Strips menu lze objednat od 10:00 až 14:00')]));
				}
			}
			
			
			//pr($this->request->data);die();
			if (isset($this->request->data['variants_select_list_item'])){
				$this->request->data['variants_select_list'] = $this->request->data['variants_select_list_item'];
			}	
				
			if (isset($this->request->data['variants_select_list'])){
				$variants_list = json_decode($this->request->data['variants_list'],true);
				$select_variant = (int) $this->request->data['variants_select_list'];
				//pr($select_variant);
				//pr($variants_list);
				//pr($select_variant);
				//pr($variants_list[$select_variant]);
				//die();
				$varianta = [
					'id'=> $select_variant,
					'name'=> $variants_list[$select_variant]['name'],
					'code'=> $variants_list[$select_variant]['code'],
					'price'=> $variants_list[$select_variant]['price_vat'] - $variants_list[$select_variant]['price_vat']*DPH_CON,
					'price_vat'=> $variants_list[$select_variant]['price_vat'],
				];
				//pr($variants_list);
				//pr($varianta);die();
			}
			if (isset($this->request->data['zaklad_sugo']) && !empty($this->request->data['zaklad_sugo'])){
				$var_codes = json_decode($this->request->data['variants_list'],true);
				$size = $var_codes[$this->request->data['variants_select_list']]['code'];
				if ($_SERVER['REMOTE_ADDR'] == '94.242.105.214'){
				//pr($var_codes[$this->request->data['variants_select_list']]['code']);die();
				}	
				if ($size == '500A'){
					switch($this->request->data['zaklad_sugo']){
						case 1: $atr_id = 79;$atr_price = 5;break;
						case 2: $atr_id = 80;$atr_price = 10;break;
						case 3: $atr_id = 81;$atr_price = 10;break;
					}
				}
				if ($size == '500B'){
					switch($this->request->data['zaklad_sugo']){
						case 1: $atr_id = 85;$atr_price = 10;break;
						case 2: $atr_id = 86;$atr_price = 15;break;
						case 3: $atr_id = 87;$atr_price = 15;break;
					}
				}
				//pr($atr_id);
				$atr = [$atr_id=>1];
				//pr(json_encode($atr));
				if (!isset($this->request->data['attributes_ids']) || empty($this->request->data['attributes_ids'])){
					$this->request->data['attributes_ids'] = json_encode($atr);
						
				} else {
					
					$this->request->data['attributes_ids'] = json_encode(json_decode($this->request->data['attributes_ids'],true)+$atr);
				}
				/*
				if (!empty($this->request->data['attributes_price'])){
					$this->request->data['attributes_price'] = $this->request->data['attributes_price'] + $atr_price;
				} else {
					$this->request->data['attributes_price'] = $atr_price;
					
				}
				*/
			}
			//pr($this->request->data['attributes_price']);
			if (isset($this->request->data['zaklad_syr']) && !empty($this->request->data['zaklad_syr'])){
				if ($size == '500A'){
					switch($this->request->data['zaklad_syr']){
						case 1: $atr2_id = 82; $atr2_price = 20;break;
						case 2: $atr2_id = 83;$atr2_price = 30;break;
						case 3: $atr2_id = 84;$atr2_price = 40;break;
					}
				}
				if ($size == '500B'){
					switch($this->request->data['zaklad_syr']){
						case 1: $atr2_id = 88; $atr2_price = 40;break;
						case 2: $atr2_id = 89;$atr2_price = 50;break;
						case 3: $atr2_id = 90;$atr2_price = 65;break;
					}
				}
				$atr2 = [$atr2_id=>1];
				if (!isset($this->request->data['attributes_ids']) || empty($this->request->data['attributes_ids'])){
					$this->request->data['attributes_ids'] = json_encode($atr2);
						
				} else {
					//pr(json_encode(json_decode($this->request->data['attributes_ids'],true),$atr2)));
					$this->request->data['attributes_ids'] = json_encode(json_decode($this->request->data['attributes_ids'],true)+$atr2);
				}
				/*
				if (!empty($this->request->data['attributes_price'])){
					$this->request->data['attributes_price'] = $this->request->data['attributes_price'] + $atr2_price;
				} else {
					$this->request->data['attributes_price'] = $atr2_price;
					
				}
				*/
			}
			if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
				//pr($this->request->data);die();
			}
				
			//pr($this->request->data['attributes_price']);
			//$this->request->data['attributes_ids'] = 
			
			//die();
			$this->loadModel('Shop.ShopProducts');
			$find_product = $this->ShopProducts->find()
			->where(['ShopProducts.id'=>$this->request->data['id']])
			->select([
				'id',
				'name',
				'number',
				'code',
				'price',
				'price_vat',
				'tax_id',
				'is_pizza',
				'credit',
			])
			->first();
			
			if (!$find_product){
				die(json_encode(['r'=>false,'m'=>'Produkt nebyl nalezen']));
			} else {
				// clear session kosik
				//$this->Session->delete('basket_products');
				
				// pokud session neexistuje vytvor
				if (!$this->Session->read('basket_products')){
					$basket_products = [];
				} else {
					$basket_products = $this->Session->read('basket_products');
				}
				
				//$product_id = $this->request->data['id'];
				$product_id = uniqid(rand(), true);
				
				// pokud je varianta prepis product id a cenu
				//pr($varianta);
				if (isset($varianta) && !empty($varianta)){
					$product_id .= 'v'.$varianta['id'];
					$find_product->name = $find_product->name.' '.$varianta['name'];
					$find_product->code = $varianta['code'];
					$find_product->price = $price = $varianta['price'];
					$find_product->price_vat = $price_vat = $this->request->data['price'] = $varianta['price_vat'];
				}
				
				//if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
				
				
				if (isset($this->request->data['attributes_ids']) && !empty($this->request->data['attributes_ids'])){
					$this->request->data['attributes_ids'] = json_decode($this->request->data['attributes_ids'],true);
					foreach($this->request->data['attributes_ids'] AS $atr_id=>$count){
						if ($count == 0){
							unset($this->request->data['attributes_ids'][$atr_id]);
						}
					}
					$this->request->data['attributes_ids'] = json_encode($this->request->data['attributes_ids']);
				}
				
				
				//if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
				if (empty($this->request->data['attributes_ids'])){
					$this->request->data['attributes_price'] = $this->request->data['price'];
				
				}
				//pr($this->request->data);die();
				//}
				//}
				if (!isset($basket_products['products'][$product_id])){ // pokud produkt jeste neexistuje v session
					if (isset($this->request->data['attributes_price']) && !empty($this->request->data['attributes_price'])){
						$price = $this->request->data['attributes_price'] - $this->request->data['attributes_price']*DPH_CON;
						$price_vat = $this->request->data['attributes_price'];
					
					} else {
						//$price = $this->request->data['price'] - $this->request->data['price']*DPH_CON;
						//$price_vat = $this->request->data['price'];
						$price = $this->request->data['price'] - $this->request->data['price']*DPH_CON;;
						$price_vat = $this->request->data['price'];
						
					}
					//pr($price);
					$basket_products['products'][$product_id] = [
						'id'=>$this->request->data['id'],
						'shop_product_id'=>$this->request->data['id'],
						'name'=>$find_product->name,
						'code'=>$find_product->code,
						'number'=>$find_product->number,
						'credit'=>$find_product->credit,
						'is_pizza'=>$find_product->is_pizza,
						'price'=>$price,
						'price_vat'=>$price_vat,
						'tax_id'=>$find_product->tax_id,
						'price_total'=>$price*$this->request->data['count'],
						'price_total_vat'=>$price_vat*$this->request->data['count'],
						'count'=>$this->request->data['count'],
						'attributes_ids'=>(isset($this->request->data['attributes_ids'])?$this->request->data['attributes_ids']:''),
						'attributes_price'=>(isset($this->request->data['attributes_price'])?$this->request->data['attributes_price']:''),
						'img'=>$this->request->data['img'],
						
					];
				} else { // pokud je jiz v session uprav pocet ks a celkem cena	
					$basket_products['products'][$product_id]['count'] = $this->request->data['count'] + $basket_products['products'][$product_id]['count'];
					$basket_products['products'][$product_id]['price_total'] = $basket_products['products'][$product_id]['count'] * $basket_products['products'][$product_id]['price'];
					$basket_products['products'][$product_id]['price_total_vat'] = $basket_products['products'][$product_id]['count'] * $basket_products['products'][$product_id]['price_vat'];
					//pr($basket_products);die();
				}
				if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
				//pr($basket_products);die();
				
				}
				$this->Session->write('basket_products',$basket_products);
				
			}
			//pr($this->Session->read('basket_products'));
			//pr($_SESSION);
			$this->getTotalPrice();
			//$this->debug();
			$result = ['r'=>true,'m'=>'Přidáno do košíku','basket_products'=>$this->basket_products,'product'=>$find_product];
			//$this->viewBuilder()->className('Json');
			//die(json_encode(['r'=>true,'m'=>'Přidáno do košíku','basket_products'=>$this->basket_products,'product'=>$find_product]));
						
			return $this->jsonResult($result);
			//echo (json_encode(['r'=>true,'m'=>'Přidáno do košíku','basket_products'=>$this->basket_products,'product'=>$find_product]));
		} else {
			die(json_encode(['r'=>false,'m'=>'Chybne volani funkce']));
		}
	}
	
	// change ks in basket SESSION
	public function basket_change_ks($product_id,$value){
		if (!$this->request->is("ajax")){
			die('chybne volani funkce');
		}
		if ($value < 0) $value = 1;
		$this->viewBuilder()->autoLayout(false);
		if (!isset($this->basket_products['products'][$product_id])){
			die(json_encode(['r'=>false,'m'=>__d('shop','Produkt nenalezen')]));
			
		} else {
			$this->basket_products['products'][$product_id]['count'] = $value;
			$this->basket_products['products'][$product_id]['price_total'] = $value * $this->basket_products['products'][$product_id]['price'];
			$this->basket_products['products'][$product_id]['price_total_vat'] = $value * $this->basket_products['products'][$product_id]['price_vat'];
		}
		$this->Session->write('basket_products',$this->basket_products);
		$this->getTotalPrice();
		die(json_encode(['r'=>true,'m'=>__d('shop','Počet ks upraven'),'basket_products'=>$this->basket_products,]));
	}

	public function client_orders(){
		if (!isset($this->loggedUser)){
			$this->redirect('/');
		}
		$this->loadModel('Shop.ShopProvozs');
		$this->basket_provoz_list = $this->ShopProvozs->provozListAll();	
		$this->set('basket_provoz_list',$this->basket_provoz_list);
		
		
		$query = $this->ShopOrders->find()
		  ->contain(['Users','UserAddresses'=> function ($q) {
			   return $q ->where(['UserAddresses.id = ShopOrders.user_address_id']);
			},'ShopOrderItems'])
		  ->where(['ShopOrders.shop_client_id'=>$this->loggedUser['id']])
		  ->order('ShopOrders.id DESC')
		  ->select([
		  ])
		  ->hydrate(false)
		  ;
		  
		$data = $query->toArray();
		//pr($data);
		if (!empty($data)){
			$this->set('orders',$data);
		}
		
	}
	
	
	public function loadGps($id){
		$this->loadModel('MobileOrders');
		$query = $this->MobileOrders->find()
		  //->where(['id'=>$id,'stav_id'=>6])
		  ->where(['id'=>$id])
		  ->select([
			'lat',
			'lng',
			'client_lat',
			'client_lng',
			'update_gps',
		  ])
		  ->hydrate(false)
		  ;
		$data = $query->first();
		if (!empty($data['update_gps'])){
			$data['update_gps'] = $data['update_gps']->format('d.m. H:i');
		}
		//pr($data);
		die(json_encode($data));
	}
	
	public function checkEmail($email){
		$this->loadModel('ShopClients');
		$find_email = $this->ShopClients->find()
			->where(['email'=>$email,'reg'=>1])
			->first();
		if ($find_email){
			die(json_encode(['r'=>true,'m'=>__d('shop','Váš email je již registrován')]));
		} else {
			return false;
		}
	}
	
	public function getMap(){
		header('Content-Type: text/html; charset=utf-8');
		// load provoz list
		$this->loadModel('Shop.ShopProvozs');
		$this->set('pokladna_list',$this->pokladna_list = $this->ShopProvozs->pokladnaListAll());
		
		
		// load map area
		$this->loadModel('Shop.ShopMapAreas');
		$map_area_list = $this->ShopMapAreas->mapAreaList();
		$this->map_area_list = [];
		//pr($this->pokladna_list['provoz_pokladna_id']);
		//pr($map_area_list);
		foreach($map_area_list AS $area_id=>$d){
			//pr($area_id);
				if (isset($this->pokladna_list['provoz_pokladna_id'][$area_id]) && $area_id != 1000){
					$this->map_area_list[$this->pokladna_list['provoz_pokladna_id'][$area_id]] = $d;
				}
			
		}
		//pr($this->pokladna_list['provoz_pokladna_id']);
		//pr($this->map_area_list);
		//pr($this->pokladna_list['data']);
		$list = '<table>';
			//pr($this->basket_provoz_list);
			$list.='<tr>
				<th>Provoz</th>
				<th>ID pokladna</th>
				<th>Oblasti</th>
				<th>GOPAY</th>
			</tr>';
			foreach($this->pokladna_list['data'] AS $pokId=>$data){
				$list .= '<tr>';
					$list .= '<td>'.$data['name'].'</td>';
					$list .= '<td>'.$pokId.'</td>';
					$list .= '<td>';
					if (isset($this->map_area_list[$pokId])){
						$areas_name = [];
						foreach($this->map_area_list[$pokId] AS $d){
							$areas_name[] = $d['name'];
						}
						$areas_name = implode($areas_name,',');
						$list.= substr($areas_name,0,100).'...';
					} else {
						$list.= '<strong>Nevyplneno</strong>';
					}
					$list.='</td>';
					$list.='<td>'.(!empty($data['gopay_id'])?$data['gopay_id']:'<strong>Nevyplneno</strong>').'</td>';
					
				
				$list .= '</tr>';
			}
		$list .= '</table>';
		if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
			echo($list);
		
		}
		//pr($this->map_area_list);
		die();
	}
}
