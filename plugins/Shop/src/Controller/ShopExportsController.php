<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace Shop\Controller;

use Shop\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\I18n\Time;
use lessc\lessc;
use Cake\Cache\Cache;
//use Cake\Cache\Cache;

class ShopExportsController extends AppController
{		
	var $product_fields = [
		'id',
		'name',
		'alias',
		'price_vat',
		'shop_availability_id',
		'shop_manufacturer_id',
		'images',
		'ean',
	];
	
	
	// XML zbozi
    public function zbozi(){
		// find products
		$this->loadModel('Shop.ShopProducts');
		$products = $this->ShopProducts->find()
		->where()
		->select($this->product_fields)
		->toArray();
		
		// vyrobce list
		$this->loadModel('Shop.ShopManufacturers');
		$manufacturers_list = $this->ShopManufacturers->manufacturersList();
		
		// dostupnost
		$this->loadModel('Shop.ShopAvailabilities');
		$availabilities_list = $this->ShopAvailabilities->availabilitiesList();
		
		$xml = '<?xml version="1.0" encoding="UTF-8" ?>';
		$xml	.= '<SHOP>';
		foreach($products as $product):
		$xml	.= '	
			<SHOPITEM>
				<PRODUCT><![CDATA['.$product->name.']]></PRODUCT>
				<DESCRIPTION><![CDATA['.strip_tags($product->description).']]></DESCRIPTION>
				<URL>'.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].'/'.DETAIL_LINK.'/'.$product->alias.'/'.$product->id.'/</URL>';
				
				$product->images = json_decode($product->images,true);
				if (isset($product->images[0]['file'])){
					$img_path = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].'/uploaded/products/'.$product->images[0]['file'];
				} else {
					$img_path = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].'/css/layout/noimg.png';
				}
				
		$xml .='		
				<IMGURL>'.$img_path.'</IMGURL>
				<ITEM_TYPE>new</ITEM_TYPE>
				<EAN>'.$product->ean.'</EAN>
				<PRICE_VAT>'.$product->price_vat.'</PRICE_VAT>
				
				<MANUFACTURER><![CDATA['.
					((isset($manufacturers_list[$product->shop_manufacturer_id]))?$manufacturers_list[$product->shop_manufacturer_id]:'')
				.']]></MANUFACTURER>
				
				<DELIVERY_DATE>'. 
					((isset($availabilities_list[$product->shop_availability_id]))?$availabilities_list[$product->shop_availability_id]:'')
				.'</DELIVERY_DATE>
				<CATEGORYTEXT></CATEGORYTEXT>
			</SHOPITEM>
		';
			endforeach;
		$xml.= '</SHOP>';
		$this->create_zip($xml,'zbozi');
		
    }
	
	// XML heureak
    public function heureka(){
		// find products
		$this->loadModel('Shop.ShopProducts');
		$products = $this->ShopProducts->find()
		->where()
		->select($this->product_fields)
		->toArray();
		
		// vyrobce list
		$this->loadModel('Shop.ShopManufacturers');
		$manufacturers_list = $this->ShopManufacturers->manufacturersList();
		
		$this->loadModel('Shop.ShopAvailabilities');
		$availabilities_list = $this->ShopAvailabilities->availabilitiesList();
		
		$xml = '<?xml version="1.0" encoding="UTF-8" ?>';
		$xml	.= '<SHOP>';
		foreach($products as $product):
		$xml	.= '	
			<SHOPITEM>
				<PRODUCT><![CDATA['.$product->name.']]></PRODUCT>
				<DESCRIPTION><![CDATA['.strip_tags($product->description).']]></DESCRIPTION>
				<URL>'.$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].'/'.DETAIL_LINK.'/'.$product->alias.'/'.$product->id.'/</URL>';
				
				$product->images = json_decode($product->images,true);
				if (isset($product->images[0]['file'])){
					$img_path = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].'/uploaded/products/'.$product->images[0]['file'];
				} else {
					$img_path = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].'/css/layout/noimg.png';
				}
				
		$xml .='		
				<IMGURL>'.$img_path.'</IMGURL>
				<ITEM_TYPE>new</ITEM_TYPE>
				<EAN>'.$product->ean.'</EAN>
				<PRICE_VAT>'.$product->price_vat.'</PRICE_VAT>
				
				<MANUFACTURER><![CDATA['.
					((isset($manufacturers_list[$product->shop_manufacturer_id]))?$manufacturers_list[$product->shop_manufacturer_id]:'')
				.']]></MANUFACTURER>
				
				<DELIVERY_DATE>'. 
					((isset($availabilities_list[$product->shop_availability_id]))?$availabilities_list[$product->shop_availability_id]:'')
				.'</DELIVERY_DATE>
				<CATEGORYTEXT></CATEGORYTEXT>
			</SHOPITEM>
		';
			endforeach;
		$xml.= '</SHOP>';
		$this->create_zip($xml,'heureka');
		
    }
	
	
	// XML google
    public function google(){
		// find products
		$this->loadModel('Shop.ShopProducts');
		$products = $this->ShopProducts->find()
		->where()
		->select($this->product_fields)
		->toArray();
		
		// vyrobce list
		$this->loadModel('Shop.ShopManufacturers');
		$manufacturers_list = $this->ShopManufacturers->manufacturersList();
		
		$this->loadModel('Shop.ShopAvailabilities');
		$availabilities_list = $this->ShopAvailabilities->availabilitiesList();
		
		$xml = '<?xml version="1.0" encoding="UTF-8" ?>';
		$xml .= '
		<feed xmlns="http://www.w3.org/2005/Atom" xmlns:g="http://base.google.com/ns/1.0">
		<title>'.$this->setting['name'].'</title>
		<link href='.$_SERVER['REQUEST_SCHEME'].'"://'.$_SERVER['HTTP_HOST'].'/" rel="alternate" type="text/html" />
		<updated>'.date('Y-m-d h:m:s').'</updated>
		<author>
		<name>'.$this->setting['page_title'].'</name>
		</author>
		';
		foreach($products as $product):
		$xml	.= '	
			<entry>
			<title>'.$product->name.'</title>
			<id>'.$product->id.'</id>
			<link>http://'.$_SERVER['SERVER_NAME'].'/'.DETAIL_LINK.'/'.$product['ShopProduct']['alias'].'/'.$product['ShopProduct']['id'].'/</link>
			<summary>'.strip_tags($product->description).'</summary>
			<updated>'.strip_tags($product->modified).'</updated>
		';
				$product->images = json_decode($product->images,true);
				if (isset($product->images[0]['file'])){
					$img_path = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].'/uploaded/products/'.$product->images[0]['file'];
				} else {
					$img_path = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['SERVER_NAME'].'/css/layout/noimg.png';
				}
		$xml .= '			
			<g:image_link>'.$img_path.'</g:image_link> 
			<g:price>'.$product->price_vat.'</g:price> 
			<g:condition>nové</g:condition>
			<g:gtin>'.$product->ean.'</g:gtin>
			<g:condition>'.((isset($availabilities_list[$product->shop_availability_id]))?($availabilities_list[$product->shop_availability_id]<4)?'skladem':'available for order':'available for order').'</g:condition>
			</entry>';
		
			endforeach;
		$xml.= '</feed>';
		//pr($xml);die();
		
		$this->create_zip($xml,'google');
		
    }
	
	
	function create_zip($xml,$type){
		// Prepare File
		$file = tempnam("../tmp/zip/", "zip");
		$zip = new \ZipArchive();
		$zip->open($file, \ZipArchive::OVERWRITE);
		
		// Staff with content
		$zip->addFromString($type.'.xml', $xml);
		
		// Close and send to users
		$zip->close();
		
		
		header('Content-Type: application/zip');
		header('Content-Length: ' . filesize($file));
		header('Content-Disposition: attachment; filename="'.$type.'.zip"');
		
		echo readfile($file);
		die();
		//unlink($file); 
		
	}
	

	 
	
}
