<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace Shop\Controller;

use Shop\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\I18n\Time;
use Cake\Cache\Cache;

class ShopProductsController extends AppController
{
	var $product_fields2 = [
		'id',
		'name',
		'alias',
		'price',
		'price_vat',
		'price_discount',
		'price_vat_discount',
		'discount',
		'stock_units',
		'images',
		'shop_category_id',
		'description',
		'shop_product_prices',
		'shop_product_variants',
		'el_class',
	];
	
	var $product_fields = [];
	// pole povolenych get parametru
	var $filtration_fields = [
		'price_values'=>'price_range',
		'shop_manufacturer_id'=>'select',
		'name'=>'text_like',
	];
	var $order_fields = 'order';
		
	
	// article index
    public function articleDetail($path,$title){
		$this->set('title',$title);
		$this->render('Articles/articles/'.$path);
		
	}	
		
    public function index($params=null,$type=null){
		//pr($_COOKIE);die();
		if ($type == 'add'){
			//$this->render('stripsMenu');
			$this->set('addStrips',true);
		}
		// pregenerovat strom kategorii na LIST
		$path = func_get_args();
		if (isset($path[0]) && $path[0] == 'products'){
			$show_all = true;
		}
		if (empty($path)){
			$path = ['pizza'];
		}
		$article_urls = [
			'obchodni-podminky'=>__d('shop',__('Obchodní podmínky')),
			'100-spokojenost'=>__d('shop',__('100% spokojenost')),
			'vernostni-program'=>__d('shop',__('Věrnostní program')),
			'kontakt'=>__d('shop',__('Kontakt')),
			'zadost-o-zamestnani'=>__d('shop',__('Žádost o zaměstnání')),
		
		];
		if (isset($article_urls[$path[0]])){
			
			return $this->articleDetail($path[0],$article_urls[$path[0]]);
		}
		if ($path[0] == 'pizza'){
			$this->set('is_akce',true);
		}
		
		//pr($this->shop_menu_items);die();
		//pr($path);
		//pr($this->menu_items_path_group);die();
		$this->menu_data = $this->ShopProducts->shopMenuItems($path,$this->shop_menu_items,(isset($this->menu_items_path_group[$path[0]])?$this->menu_items_path_group[$path[0]]:0),$this->category_seo);
		//pr($path);
		//pr($this->menu_data);
		
		
		if (empty($this->menu_data) && !isset($show_all)){
			return $this->error404();
			
		}
		$seo = [
				'title'=>$this->menu_data['breadcrumb'],
		];
		$this->set('seo',$seo);
		$this->set('title',$this->menu_data['breadcrumb']);
		$this->set('menu_data',$this->menu_data);
		//pr($this->menu_data);
		
		// napoje
		if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
			if (in_array($this->menu_data['cat_group_id'],[1,2,4])){
			
			$query = $this->ShopProducts->find('list',[
				'keyField' => 'id',
				'valueField' => 'id'
			]);
			$query->where(['ShopProducts.shop_category_id'=>8])
			->cache(function ($query) {
				return 'napoje-' . md5(serialize($query->clause('where')).serialize($this->lang).serialize($this->system_id));
			});
			;
			
			$napoje = $query->toArray();
			
			$ids = array_merge($this->menu_data['ids'],$napoje);
			//pr($ids);
			$this->menu_data['ids'] = $ids;
			$this->set('addon_napoje',true);
			}
			//pr($napoje);
		}
		
        //pr($this->request->query);
		
		$conditions = [];
		
		// parsovani conditions z history
		$parse_query = $this->parse_conditions_from_history();
		
		// set conditions
		if (!empty($parse_query['conditions'])){
			$conditions = $parse_query['conditions'];
		}
		
		// set order
		if (!empty($parse_query['order'])){
			$order = $parse_query['order'];
		} else {
			$order = ['ShopProducts.sort ASC', 'ShopProducts.number ASC '];
		}
		
		// set result type
		if (!empty($parse_query['result_type'])){
			$result_type = $parse_query['result_type'];
			$this->Session->write('result_type',$result_type);
		
		} else {
			$result_type = 1;
			if (!$this->Session->read('result_type'))
			$this->Session->write('result_type',$result_type);
		}
		
		if (!empty($this->menu_data['ids'])){
			$conditions['ShopProducts.id IN'] = $this->menu_data['ids'];
		
		} else {
			if (!isset($show_all)){
				$conditions['ShopProducts.id'] = 0;
			
			}
		}
		//pr($this->menu_data);
		if (Configure::read('language')){
			$conditions = $this->convert_trans_conditions('ShopProducts',$conditions);
		}
		//pr($conditions);
			$mapper = function ($row, $key, $mapReduce) {
				// map group id from category
				$row->group_id = (isset($this->menu_data['shop_menu_items_group_id'][$row->shop_category_id])?$this->menu_data['shop_menu_items_group_id'][$row->shop_category_id]:'');
				
				// map varitanty
				if (isset($row->shop_product_variants) && !empty($row->shop_product_variants)){
					if (!isset($variants_list)){
						$variants_list = [];	
						$variants_select_list = [];	
					}
					foreach($row->shop_product_variants AS $var){
						$variants_list[$var->id] = $var;
						$variants_select_list[$var->id] = [
							'id'=>$var->id,
							'name'=>$var->name,
							'price'=>$var->price,
							'price_vat'=>$var->price_vat,
						];
					}
					$row->variants_list = $variants_list;
					$row->variants_select_list = $variants_select_list;
				}
				
				if (isset($row->shop_product_prices) && isset($row->shop_product_prices[0])){
						$row->price_vat  = $row->shop_product_prices[0]['price_vat'];
					
				} else {
					unset($row);
				}
				if (isset($row))
				$mapReduce->emit($row);
			};
			//pr($conditions);
			// find products
			$products = $this->ShopProducts->find()
				->where($conditions)
				
				->select($this->product_fields)
				->contain([
					'ShopProductConAttributes'=>function($q){
						$q->select(['shop_product_attribute_id','shop_product_id']);
						//$q->where(['system_id'=>$this->system_id]);
						return $q;
					},
					'ShopProductVariants'=>function($q){
						$q->select(['id','shop_product_id','price_vat','code','name']);
						$q->where(['system_id'=>$this->system_id]);
						return $q;
					},
					'ShopProductPrices'=>function($q){
						//$q->select(['id','shop_product_id','price_vat','code','name']);
						$q->where(['system_id'=>$this->system_id,'status'=>1]);
						return $q;
					}
				])
				->order($order)
				
				->cache(function ($products) {
					return 'products-' . md5(serialize($products->clause('where')).serialize($products->clause('order')).serialize($this->lang).serialize($this->system_id));
				})
				->mapReduce($mapper)
				;
			;
			//pr($products->toArray());
			if (($items = Cache::read('product_items'.md5(serialize($products->clause('where')).serialize($products->clause('order')).serialize($this->lang).serialize($this->system_id)))) === false) {
				$items = $this->paginate($products)->toArray();
				Cache::write('product_items'.md5(serialize($products->clause('where')).serialize($products->clause('order')).serialize($this->lang).serialize($this->system_id)), $items);
			}
		//pr($conditions);
		
		if ($items){
			$this->set('items', $items);
		}
		// get price range to slider
		//pr($this->menu_data['ids']);
		if (!empty($this->menu_data['ids'])){
			//$this->getPriceRangeToSlider($this->menu_data['ids']);
		}
		
		// get result type table / image
		$this->set('result_type', $this->Session->read('result_type'));
		
		//pr($this->request);
		$this->Session->write('ajax_load', true);
		
		
		// seo text
		if (isset($category_detail)){
		
			$seo = [
				'title'=>$category_detail->name,
				'description'=>((isset($category_detail->description) && !empty($category_detail->description))?$category_detail->description:''),
			];
			$this->set('seo',$seo);
		}
		if ($this->request->is("ajax")){
			$this->viewBuilder()->autoLayout(false);
			$this->render('items');
		} else {
			$this->render('index');
		}
    }
	
	// parsovani z query na conditions
	private function getPriceRangeToSlider($product_ids){
		$conditions = ['ShopProducts.id IN'=>$product_ids];
		$con_hash = md5(json_encode([$conditions]));
		
			$query = $this->ShopProducts->find('list',[
				'keyField' => 'shop_product_id',
				'valueField' => 'price_vat'
			]);
			$query->where($conditions)
			->order('price_vat ASC')
			->cache(function ($query) {
				return 'getPriceRange-' . md5(serialize($query->clause('where')).serialize($this->lang));
			});
			;
			
			$data = $query->toArray();	
		
		$price_range = [
			'min'=>$data[0],
			'max'=>$data[count($data)-1],
		];
		$this->set('price_range',$price_range);
		//pr($product_ids);
	}
	
	// parsovani z query na conditions
	private function parse_conditions_from_history(){
		
		
		$result = [];
		//pr($this->request->query);
		if (isset($this->request->query) && !empty($this->request->query)){
			foreach($this->request->query AS $key_query=>$val_query){
				// conditions
				if (isset($this->filtration_fields[$key_query])){
					if ($val_query != '0' && $val_query != ''){
						
						if ($this->filtration_fields[$key_query] == 'text_like'){
							$result['conditions'][$key_query.' LIKE'] = '%'.$val_query.'%';
					
						} 
						if ($this->filtration_fields[$key_query] == 'price_range'){
							list($price_from,$price_to) = explode(',',$val_query);
							$result['conditions']['price_vat >='] = $price_from;
							$result['conditions']['price_vat <='] = $price_to;
					
						} 
						if ($this->filtration_fields[$key_query] == 'select'){
							$result['conditions'][$key_query] = $val_query;
						}
					
					}
					
				}
				
				// order
				if ($key_query == $this->order_fields){
					$result['order'] = $val_query;
				}
				// result type
				if ($key_query == 'result_type'){
					$result['result_type'] = $val_query;
				}
			}
		
		}
		return $result;
	}
	
	
	
	// load detail article
	public function detail($alias=null,$id=null){
		
		// vyrobce list
		$this->loadModel('Shop.ShopManufacturers');
		$manufacturers_list = $this->ShopManufacturers->manufacturersList();
		$this->set('manufacturers_list',$manufacturers_list);
		
		// dostupnost
		$this->loadModel('Shop.ShopAvailabilities');
		$availabilities_list = $this->ShopAvailabilities->availabilitiesList();
		$this->set('availabilities_list',$availabilities_list);
		
		$conditions = [
			'ShopProducts.id'=>$id
		];
		// find products
		$query = $this->ShopProducts->find()
			->where($conditions)
			->select([
				'id',
				'name',
				'alias',
				'price',
				'price_vat',
				'price_discount',
				'price_vat_discount',
				'discount',
				'stock_units',
				'images',
				'description',
				'technical_specification',
				'shop_manufacturer_id',
				'shop_availability_id',
				'related_products',
			])
			->contain(['ShopProductCategories'=> function($q){
				return $q->select(['shop_category_id','shop_product_id']);
            }])
			->cache(function ($query) {
				return 'productsDetail-' . md5(serialize($query->clause('where')).serialize($this->lang));
			})
			->map(function($row){
				/*
				$row->images = (!empty($row->images)?json_decode($row->images,true):'');
				$row->images[1] = ['file'=>'2.png'];
				$row->images[2] = ['file'=>'2.png'];
				$row->images[3] = ['file'=>'2.png'];
				$row->images[4] = ['file'=>'2.png'];
				$row->images[5] = ['file'=>'2.png'];
				$row->images = json_encode($row->images);
				*/
				return $row;
			})
			;
		;
		$product = $query->first();
		// pokud neni produkt tak 404
		if (!$product){
			return $this->error404();
		}
		
		
		// zjisteni kategorie a poslani na view pro otevreni v menu
		if (isset($product->shop_product_categories[0]['shop_category_id'])){
			$cat_id = $product->shop_product_categories[0]['shop_category_id'];
			$this->set('open_cat_id',$cat_id);
		}
		
		// seo text
		$seo = [
			'title'=>$product->name,
			'description'=>$product->description,
		];
		
		if (!empty($product->images)){
			//pr($product->images);
			//$images = json_decode($product->images,true);
			if (isset($images[0]['file'])){
				$seo['img'] = '/uploaded/products/'.$images[0]['file'];
			}
		}
		$this->set('seo',$seo);
		
		
		$this->set("product", $product);
		$this->set("title", $product->name);
		
	}
	
	// products_search autocomplete
	public function products_search($value=null){
		$value = h($value);
		//pr($value);
		$img_size = '&w=30&h=30';
		
		$conditions_cats = ['name LIKE'=> '%'.$value.'%'];
		$conditions_products = [
			'ean'=> $value,
			'name LIKE'=> '%'.$value.'%',
			//'description LIKE'=> '%'.$value.'%',
		];
		
		if (Configure::read('language')){
			$conditions_cats = $this->convert_trans_conditions('ShopCategories',$conditions_cats);
			$conditions_products = $this->convert_trans_conditions('ShopProducts',$conditions_products);
		}
		// find categories
		$this->loadModel('Shop.ShopCategories');
		$categories_loadQ = $this->ShopCategories->find()
			->where([
				'OR'=>$conditions_cats
			])
			->select([
				'id',
				'name',
				'alias',
				'images',
			])
			->limit(15)
			->cache(function ($categories_loadQ) {
				return 'autocomplete_cats-' . md5(serialize($categories_loadQ->clause('where')).serialize($this->lang));
			})
			->map(function($row){
				$row->images = (!empty($row->images)?json_decode($row->images,true):'');
				return $row;
			})
			;
			
			$categories_load = $categories_loadQ->toArray();
		
		//pr($categories_load);die();
		if ($categories_load){
			$categories = [];
			$categories[0] = ['name'=>__d('shop','Kategorie'),'title'=>true];
			$path = $this->ShopCategories->getFullPath();
			foreach($categories_load AS $k=>$l){
				//pr($l);
				$categories[] = [
					'name'=>$l->name,
					'url'=>$this->url_lang_prefix.$path[$l->id],
					'img'=>'/img/?file='.(isset($l->images[0])?$l->images[0]['file']:'').$img_size,
				];
			}
		}
		
		
		// find products
		//$this->loadModel('ShopProducts');
		$products_loadQ = $this->ShopProducts->find()
			->where([
				'OR'=>$conditions_products
			])
			->select([
				'id',
				'name',
				'alias',
				'images',
			])
			->limit(15)
			
			->cache(function ($products_loadQ) {
				return 'autocomplete_products-' . md5(serialize($products_loadQ->clause('where')).serialize($this->lang));
			})
			
			->map(function($row){
				$row->images = (!empty($row->images)?json_decode($row->images,true):'');
				return $row;
			});
			
		;
		$products_load = $products_loadQ->toArray();
		//pr($products_load);
		if ($products_load){
			$products = [];
			$products[0] = ['name'=>__d('shop','Produkty'),'title'=>true];
			foreach($products_load AS $k=>$l){
				//pr($l);
				$products[] = [
					'name'=>$l->name,
					'url'=>$this->url_lang_prefix.'/'.DETAIL_LINK.'/'.$l->alias.'/'.$l->id.'/',
					'img'=>'/img/?file=./uploaded/products/'.(isset($l->images[0])?$l->images[0]['file']:'').$img_size,
				];
			}
		}
		//pr($products);
		//pr($products_load);
		/*
		$categories = [
			0=>['name'=>'test','urla'=>'/aaa/','img'=>'/img/?file=http://vegan-shop.cz/data/imagestore/product_thubnail2d40235b711a38d806b11474564513.jpg'.$img_size],
			1=>['name'=>'test2','url'=>'/aaa/','img'=>'/img/?file=http://vegan-shop.cz/data/imagestore/product_thubnail2d40235b711a38d806b11474564513.jpg'.$img_size],
			2=>['name'=>'test2','url'=>'/aaa/','img'=>'/img/?file=http://vegan-shop.cz/data/imagestore/product_thubnail2d40235b711a38d806b11474564513.jpg'.$img_size],
			3=>['name'=>'test2','url'=>'/aaa/','img'=>'/img/?file=http://vegan-shop.cz/data/imagestore/product_thubnail2d40235b711a38d806b11474564513.jpg'.$img_size],
			4=>['name'=>'test2','url'=>'/aaa/','img'=>'/img/?file=http://vegan-shop.cz/data/imagestore/product_thubnail2d40235b711a38d806b11474564513.jpg'.$img_size],
		];
		*/
		/*
		$products = [
			0=>['name'=>'test prod','url'=>'/aaa/'],
			1=>['name'=>'test2 prod','url'=>'/aaa/'],
		];
		*/
		if (!isset($products)){
			$products = [];
			$products[0] = ['name'=>__d('shop','Produkty'),'title'=>true];
			$products[1] = ['name'=>__d('shop','Nenalezeny žádné produkty'),'noresult'=>true];
		}
		if (!isset($categories)){
			$categories = [];
			$categories[0] = ['name'=>__d('shop','Kategorie'),'title'=>true];
			$categories[1] = ['name'=>__d('shop','Nenalezeny žádné kategorie'),'noresult'=>true];
		}
		
		$data = [
			'products'=>$products,
			'categories'=>$categories,
		];
		//$this->debug();
		$this->response->cache('-1 minute', '+5 days');
		//pr($this->response->header());
		$this->viewBuilder()->autoLayout(false);
		$this->autoRender = false;
		
		die(json_encode(['r'=>true,'data'=>$data]));
		
	}
	
	
	// homepage
	public function homepage($alias=null,$id=null){
		if (isset($this->redirect_template) && $this->redirect_template == 'cina'){
			$name = 'číny';
		} else {
			$name = 'pizzy';
		}
		
		$this->set("title", 'Rozvoz '.$name.' <span id="h1_change">'.$this->system_id_data['seo_category_city'].'</span>');
		$this->set("breadcrumb", ['Rozvoz '.$name.' '.$this->system_id_data['seo_category_city']]);
		
	}
	
	
	

	 
	
}
