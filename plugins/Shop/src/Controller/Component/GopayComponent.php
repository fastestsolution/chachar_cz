<?php

namespace Shop\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\Entity;

define('RETURN_URL',$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].GOPAY_DONE_URL);
define('NOTIFY_URL',$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/'.GOPAY_NOTIFY_URL.'/');

class GopayComponent extends Component{
	
	var $controller = null;
	/**
	ZMENIT NA PRODUKCNI VERZI
	**/
	var $test_api = false;
	
	
	var $url_token_test = 'https://testgw.gopay.cz/api/oauth2/token/';
	var $url_token_prod = 'https://gate.gopay.cz/api/oauth2/token/';
	var $url_pay_test = 'https://testgw.gopay.cz/api/payments/payment/';
	var $url_pay_prod = 'https://gate.gopay.cz/api/payments/payment/';
	
	
	
	function create($options){
		$this->options = $options;
		$this->create_token('created');
		if (isset($options['notify_url'])){
			$this->notifyOwn = $options['notify_url'];
		}
		
		return $this->gopay_result;
	}
	
	// overeni platby volane z controller
	function check_pay($options,$id){
		$this->options = $options;
		$this->payment_id = $id;
		$this->create_token('stav');
		return $this->pay_result;
		
	
	}
	
	function create_token($type){
		$ch = curl_init();
		switch($type){
			case 'created': $scope = 'payment-create'; break;
			case 'stav': $scope = 'payment-all'; break;
		
		}
		
		$fields = array(
			'grant_type'=>'client_credentials',
			'scope'=>$scope,
		);
		$postvars = '';
		foreach($fields as $key=>$value) {
			$postvars .= $key . "=" . $value . "&";
		}
		$postvars = rtrim($postvars,'&');
		
		if ($this->test_api == true){
		  $url = $this->url_token_test;
		
		} else {
		  $url = $this->url_token_prod;
		
		}
		
		$headers = array(
            //"POST ".$page." HTTP/1.0",
            "Content-type: application/x-www-form-urlencoded",
            "Accept: application/json",
            //"Authorization: ".Base64_encode(GOPAY_ID.":".GOPAY_SECURE),
        );
		//pr($this->options['gopay_client_id'].":".$this->options['gopay_client_secure']);die();
		//pr($postvars);die();
		//pr($url);
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		curl_setopt($ch, CURLOPT_USERPWD, $this->options['gopay_client_id'].":".$this->options['gopay_client_secure']);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
		  
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_POST, 1);                //0 for a get request
		
		curl_setopt($ch,CURLOPT_POSTFIELDS,$postvars);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
		curl_setopt($ch,CURLOPT_TIMEOUT, 20);
		
		$response = curl_exec($ch);
		//pr($response);die('a');
		
		
		//curl_setopt($ch, CURLINFO_HEADER_OUT, true);
		//$info = curl_getinfo($ch);
		//pr($info);
		
		$response = json_decode($response,true);
		//pr($response);
		$this->access_token = $response['access_token'];
		curl_close ($ch);
		
		
		switch($type){
			case 'created': 
				$this->create_payment();
			break;
			case 'stav': 
				$this->check_payment();
			break;
		
		}
	
	}
	
	
	function create_payment(){
		$this->controller = $this->_registry->getController();
		$order = $this->options;
		//pr($order['gopay_id']);
		//pr($order);die();
		//require_once('../vendor/gopay/api/gopay_helper.php');
		//die();
		$payment_data = array(
			'payer'=>array(
				'default_payment_instrument'=>'PAYMENT_CARD',
				'allowed_payment_instruments'=>array('PAYMENT_CARD'),
				//'default_swift'=>'GIBACZPX',
				
				'contact'=>$order['client'],
			),
			'target'=>array(
					'type'=>'ACCOUNT',
					'goid'=>$order['gopay_id'],
			),
			'amount'=>$order['price']*100,
			'currency'=>'CZK',
			'order_number'=>$order['id'],
			'order_description'=>$order['description'],
			'callback'=>array(
				'return_url'=>(isset($order['notify_url'])?$_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].$order['notify_url']:RETURN_URL),
				'notification_url'=>NOTIFY_URL,
			)
				
				
			
		);
		//pr($payment_data);
		
		if ($this->test_api == true){
		  $url = $this->url_pay_test;
		
		} else {
		  $url = $this->url_pay_prod;
		
		}
		
		$postvars = json_encode($payment_data);
		
		$headers = array(
            //"POST ".$page." HTTP/1.0",
            "Content-type: application/json",
            "Accept: application/json",
            "Authorization: Bearer ".$this->access_token,
        );
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		//curl_setopt($ch, CURLOPT_USERPWD, "Bearer ".$this->access_token);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
		  
		curl_setopt($ch,CURLOPT_URL,$url);
		curl_setopt($ch,CURLOPT_POST, 1);                //0 for a get request
		
		curl_setopt($ch,CURLOPT_POSTFIELDS,$postvars);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
		curl_setopt($ch,CURLOPT_TIMEOUT, 20);
		
		$response = curl_exec($ch);
		
		curl_close ($ch);
		
		
		$response = json_decode($response,true);
		//pr($payment_data);pr($response);die();
		if (isset($response['state']) && $response['state'] == 'CREATED'){
			
			//$this->controller->set('gw_url',$response['gw_url']);
			
			//pr($save_order);
			$this->controller->loadModel('Shop.ShopOrders');
			$this->controller->ShopOrders->updateAll(
				['gopay_id' => $response['id'],
				], // fields
				['id' => $response['order_number']]
			);
			$targetGoId = $order['gopay_id'];
			$paymentSessionId = $response['id'];
			$secureKey = $order['gopay_client_secure'];
			$gopay_result = [
				'r'=>true,
				'gateway_id'=>$response['id'],
				'gateway_url'=>$response['gw_url'],
			];
			
		} else {
			$gopay_result = [
				'r'=>false,
			];
		}
		$this->gopay_result = $gopay_result;
		//pr($gopay_result);
		//die($gopay_result);
	}
	
	
	// check payment gopay result
	function check_payment(){
		$this->controller = $this->_registry->getController();
		//$this->autoRender = false;
		if ($this->test_api == true){
		  $url = $this->url_pay_test;
		
		} else {
		  $url = $this->url_pay_prod;
		
		}
		$headers = array(
            //"POST ".$page." HTTP/1.0",
            "Content-type: application/x-www-form-urlencoded",
            "Accept: application/json",
            "Authorization: Bearer ".$this->access_token,
        );
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
		//curl_setopt($ch, CURLOPT_USERPWD, "Bearer ".$this->access_token);
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers); 
		  
		curl_setopt($ch,CURLOPT_URL,$url.$this->payment_id);
		curl_setopt($ch,CURLOPT_POST, 0);                //0 for a get request
		
		//curl_setopt($ch,CURLOPT_POSTFIELDS,$postvars);
		curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch,CURLOPT_CONNECTTIMEOUT ,3);
		curl_setopt($ch,CURLOPT_TIMEOUT, 20);
		
		$response = curl_exec($ch);
		
		curl_close ($ch);
		
		$response = json_decode($response,true);
		//pr($response);die();
		
		// rucne nastavit zaplaceno
		//$response['state'] = 'PAID';
		
		
		// je zaplaceno
		if (isset($response['state']) && $response['state'] == 'PAID'){
			$this->pay_result = array('r'=>true,'order_id'=>$response['order_number']);
		
		} else {
			$this->pay_result = array('r'=>false,'order_id'=>$response['order_number']);
			
		} 
		//pr($this->pay_result);	
		$this->controller->set('pay_result',$this->pay_result);
		
		
		
	}
	
}
?>