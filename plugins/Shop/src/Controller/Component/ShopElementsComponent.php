<?php

namespace Shop\Controller\Component;
use Cake\Controller\Component;
use Cake\Cache\Cache;
class ShopElementsComponent extends Component
{
	public function initialize(array $config){
		$this->controller = $this->_registry->getController();
		
	}
	
	// shop category
	public function loadCategories(){
			//die('a');
			$this->controller->loadModel('Shop.ShopCategories');
			if (($this->shop_menu_items = Cache::read('shop_menu_items'.$_SESSION['lang'])) === false) {
			$query = $this->controller->ShopCategories -> find('threaded')
			->cache(function ($query) {
				return 'load_categories-' . md5(serialize($query->clause('where')).serialize($_SESSION['lang']).serialize($this->system_id));
			})
			;
			
			
				$this->shop_menu_items = $query->toArray();
				Cache::write('shop_menu_items'.$_SESSION['lang'].$this->system_id, $this->shop_menu_items);
			}
			$menu = [];
			foreach($this->shop_menu_items AS $m){
				$menu[$m->id] =$m->name;
			}
			$this->shop_menu_items_list = $menu;
			
		//pr($this->shop_menu_items);	
		$this->shop_menu_items_path = $this->controller->ShopCategories->getFullPath();
		$this->controller->shop_menu_items = $this->shop_menu_items;
		$this->controller->set('shop_menu_items_path',$this->shop_menu_items_path);
		$this->controller->set('shop_menu_items',$this->shop_menu_items);
		$this->controller->set('shop_menu_items_list',$this->shop_menu_items_list);
		
	}
	



}