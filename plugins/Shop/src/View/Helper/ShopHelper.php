<?php

namespace Shop\View\Helper;

use Cake\View\Helper;
use Cake\View\View;
use Cake\View\Helper\HtmlHelper;
use Cake\View\Helper\FastestHelper;

class ShopHelper extends Helper
{

	public $helpers = ['Html','Form','Fastest'];
  
  
	// add basket element
	function addBasketElement($opt=null){
		/*
		$opt = [
			'id'=>1,
			'varianta_select'=>'',
			'variants_list'=>'',
			'price'=>555,
			'ks'=>2,
		];
		*/
		$out = $this->Form->create(false,['url'=>'/'.ORDERS_LINK.'/add_basket/']);
		$out .= $this->Form->input('count', ['label'=>false,'class'=>'form-control addBasket_ks','value'=>$opt['ks'],'type'=>(isset($opt['hidden_ks']) && $opt['hidden_ks'] == true?'hidden':'text'),'id'=>'count'.$opt['id']]); 
		if (!isset($opt['btn_class'])) $opt['btn_class'] = 'btn-lg';
		
		$out .= $this->Form->hidden('id', ['label'=>false,'value'=>$opt['id']]); 
		$out .= $this->Form->hidden('price', ['label'=>false,'value'=>$opt['price']]); 
		if (isset($opt['variants_list'])){
			$out .= $this->Form->hidden('variants_list', ['label'=>false,'value'=>json_encode($opt['variants_list']),'id'=>false,'class'=>'variants_list']); 
		}
		
		// pridavky
		if ($opt['code'] == 500){
			$opt['pridavky_list'] = [0];
		}
		if (isset($opt['pridavky_list']) && !empty($opt['pridavky_list'])){
			$out .= '<div class="pridavky_modal_in" data-id=\''.json_encode($opt['pridavky_list']).'\'>';
				
			$out .= '</div>';
			
			
		}
		
		
		// varianty pro vypis produktu
		if (isset($opt['variants_select_list_item']) && !empty($opt['variants_select_list_item'])){
			$is_varianta = true;
			$out .= '<ul class="varianta_list_item">';
			
			$variants_attributes = '<ul>';
			//pr($opt['variants_select_list_item']);
			foreach($opt['variants_select_list_item'] AS $v){
				$out.= '<li class="varianta_list_select clearfix '.(strlen($v['name'])>15?'txt_small':'').'" data-id="'.$v['id'].' ">';
				//$out .= '<span class="price">'.$this->Fastest->price($v['price_vat']).'</span>';
				// male
				//pr($v['name']);
				if ($v['name'] == 'malé'){
					$size = '';
					$order_title = __('chci běžné');
					$order_title2 = __('chci raději běžnou porci');
					$order_title_class = 'small';
				}
				// velke
				if ($v['name'] == 'velké'){
					$size = '';
					$order_title = __('chci velké');
					$order_title2 = __('chci raději velkou porci');
					$order_title_class = 'large';
				}
				
				// mala
				if ($v['name'] == 'malá'){
					$size = '&#x2300; 32 cm';
					$order_title = __('chci malou');
					$order_title2 = __('chci raději malou');
					$order_title_class = 'small';
				}
				// velka
				if ($v['name'] == 'velká'){
					$size = '&#x2300; 45 cm';
					$order_title = __('chci velkou');
					$order_title2 = __('chci raději velkou');
					$order_title_class = 'large';
				}
				//pr($v['name']);
				// velka
				if ($v['name'] == 'KLASIK (NEPÁLIVÉ)'){
					$size = 'NEPÁLIVÉ';
					$order_title = __('chci NEPÁLIVÉ');
					$order_title2 = __('chci raději velkou');
					$order_title_class = 'large';
				}
				if ($v['name'] == 'JEMNĚ PIKANTNÍ'){
					$size = 'JEMNĚ PIKANTNÍ';
					$order_title = __('chci PIKANTNÍ');
					$order_title2 = __('chci raději velkou');
					$order_title_class = 'large';
				}
				
				
				$variants_attributes.= '<li>'.$size.' '.$this->Fastest->price($v['price_vat']). $this->Html->link($order_title2,'#',['title'=>$order_title2,'data-title'=>__('máte již v košíku'),'data-title2'=>$order_title2,'class'=>'switch_varianta chbut','id'=>'Varianta'.$v['id'],'data-id'=>$v['id'],'data-price'=>$v['price_vat'],'data-prumer'=>$order_title_class]).'</li>';
				
				$out .= $size. ' '.$this->Fastest->price($v['price_vat']);
				$out .= '<span class="title '.$order_title_class.' ">'.$order_title.'</span>';
				$out .= '</li>';
			}
			
			$variants_attributes.= '</ul>';
			$out.= '</ul>';
			$out.= '<div class="variants_attributes" data-variants=\''.json_encode($variants_attributes).'\'></div>';
			$out .= $this->Form->input('zaklad_sugo', ['label'=>false,'type'=>'text','class'=>'zaklad_sugo none','id'=>false]); 
			$out .= $this->Form->input('zaklad_syr', ['label'=>false,'type'=>'text','class'=>'zaklad_syr none','id'=>false]); 
			$out .= $this->Form->input('variants_select_list', ['label'=>false,'type'=>'text','class'=>'variants_select_list_el none','id'=>false]); 
			$out .= $this->Form->input('attributes_price',['label'=>false,'class'=>'none AttributesPrice','id'=>false]);
			$out .= $this->Form->input('attributes_ids',['label'=>false,'class'=>'none AttributesIds','id'=>false]);
			$out .= $this->Form->input('img',['label'=>false,'class'=>'AttributesImg none','id'=>false]);
		} else {
			$out .= '<ul class="varianta_list_item">';
				$out.= '<li class="varianta_list_select clearfix  full" data-id="'.$opt['id'].'">';
					$out .= $this->Fastest->price($opt['price']);
					$out .= $this->Form->input('img',['label'=>false,'class'=>'AttributesImg none']);
					$out .= '<span class="title large">'.__('Chci to do košíku').'</span>';
				$out .= '</li>';
			$out.= '</ul>';
		}
		
		// varianty pro detail produktu
		if (isset($opt['variants_select_list']) && !empty($opt['variants_select_list'])){
			$out .= $this->Form->input('variants_select_list', ['label'=>false,'options'=>$opt['variants_select_list'],'type'=>'select','empty'=>false,'class'=>'form-control change_varianta',]); 
		}
		//pr($is_varianta);
		$out .= $this->Form->button(__d('shop','Přidat do košíku'), ['class' => 'btn '.$opt['btn_class'].' btn-default add_to_basket '.(isset($is_varianta)?'none':'')]);
		$out .= $this->Form->end();
		return $out;
	}
	
	
	// add basket element
	function selectTransportElement($opt=null){
		if (isset($opt['data'])){
			$dop = $opt['data'];
			
		}
		$basket_products = $opt['basket_products'];
		if (isset($opt['data_list'])){
			$data_list = $opt['data_list'];
			
		}
		$out = '';
		
			// radio button
			if (isset($opt['select_type']) && $opt['select_type'] == 'radio'){
				$inputId = $opt['name'].$dop->id;
				
				$out .= '<input type="radio"  
				name="'.$opt['name'].'" 
				data-id="'.$dop->id.'" 
				data-free-shipping="'.$dop->free_shipping_order_price.'" 
				data-price="'.$dop->price_vat.'" 
				data-price_default="'.$dop->price_vat.'" 
				data-type="'.$opt['type'].'" 
				id="'.$inputId.'" 
				value="'.$dop->id.'"
				class="check_'.$opt['type'].' add_transport" 
				data-payment_ids="'.json_encode($dop->payment_ids).'" '.((isset($basket_products[$opt['type']]) && $basket_products[$opt['type']]['id'] == $dop->id)?'checked':'').' />';
				$out .= '<label for="'.$inputId.'"></label>';	
		
			// select tag	
			} else {
			
				$options = [];
				foreach($data_list AS $dop){
					$options[$dop->id] = [
						'text'=>$dop->name,
						'value'=>$dop->id,
						'class'=>'check_'.$opt['type'].' add_transport '.(($opt['type'] == 'payment')?'payment_line payment_line_'.$dop->id:' check_transport'),
						'data-payment_ids'=>json_encode($dop->payment_ids),
						'data-id'=>$dop->id, 
						'data-free-shipping'=>$dop->free_shipping_order_price,
						'data-price'=>$dop->price_vat,
						'data-price_default'=>$dop->price_vat,
						'data-type'=>$opt['type'],
						];
				}
				//pr($options);
				$out .= $this->Form->input($opt['name'],['options'=>$options,'type'=>'select','class'=>'form-control','label'=>$opt['label'],'value'=>((isset($basket_products[$opt['type']]['id']))?$basket_products[$opt['type']]['id']:'')]);
			}
		return $out;
	}
	
	

}