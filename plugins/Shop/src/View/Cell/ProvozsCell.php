<?php  
namespace Shop\View\Cell;
use Cake\Core\Configure;
use Cake\Cache\Cache;
use Cake\View\Cell;

class ProvozsCell extends Cell
{
	public $helpers = [
        'Html'
    ];

    public function display($opt=null){
       
       $conditions = ['ShopProvozs.status'=>1];
		//require_once('../src/chachar_config.php');
		//pr($_SESSION['system_id']);
		//pr($_SESSION['new_address_list']);
		if (in_array($_SESSION['system_id'],$_SESSION['new_address_list']) || isset($this->request->query['new'])){
			$this->set('new_address',$this->new_address = true);
			
		}
		
		//pr($conditions);
		
		$this->loadModel('Shop.ShopProvozs');
        
		$query = $this->ShopProvozs->find()
			->select([
				'id',
				'provoz_id',
				'name',
				'name2',
				'telefon',
				'doba',
				'lokalita',
				'order_email',
				'poradi',
			])
			->where($conditions)
			->order('poradi ASC')
			->cache(function ($query) {
				return 'provozsCell-' . md5(serialize($query->clause('where'))).md5(serialize($query->clause('order'))).md5(serialize($query->clause('limit')));
			});
			//pr(Cache::read('provoz_list'.$_SESSION['lang']));
		if (($provoz_list = Cache::read('provoz_list'.$_SESSION['lang'])) === false) {	
			$provoz_list = $query->toArray();
			Cache::write('provoz_list'.$_SESSION['lang'], $provoz_list);
		}
		//pr($provoz_list);
		
		
		if (isset($provoz_list) && count($provoz_list)>0){
			$this->set('provoz_list', $provoz_list);
    	
		}
    }

}
?>