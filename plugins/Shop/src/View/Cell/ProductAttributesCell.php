<?php
namespace Shop\View\Cell;
use Cake\View\Cell;

use Cake\Cache\Cache;
class ProductAttributesCell extends Cell
{

  public function display($opt)
  {
	
	$this->set('sugo_list', $this->sugo_list = [
		0=>__('Vyberte si omáčku'),
		1=>__('Rajčata'),
		2=>__('Smetana'),
		3=>__('BBQ'),
	]);
	//pr($this->sugo_list);die();
	$this->set('sugo_zaklad_list', $this->sugo_zaklad_list = [
		0=>__('Vyberte si sýr'),
		1=>__('Eidam 30%'),
		2=>__('Mozzarela'),
		3=>__('Vegan'),
	]);
	$this->set('product_attributes_group_list',$opt['product_attributes_group_list']);
	$this->set('system_id',$opt['system_id']);
    
	$this->loadModel("Shop.ShopProductAttributes");
		if (($prod_attributes = Cache::read('prod_attributes'.$_SESSION['lang'])) === false) {
		$query = $this->ShopProductAttributes->find()
			//->where(['code NOT IN'=>[671,672,673,674,675,676]])
			->where(['group_id !='=>5])
			->select([
			])
			->cache(function ($query) {
				return 'productsAttributes-' . md5(serialize($query->clause('where')).serialize($_SESSION['lang']));
			})
			->order('name ASC')
			->map(function($row){
				return $row;
			})
			;
		;
		
			$prod_attributes = $query->toArray();
			Cache::write('prod_attributes'.$_SESSION['lang'], $prod_attributes);
		}
	$products_attributes = [];
	foreach($opt['product_attributes_group_list'] AS $k=>$o){
		$products_attributes[$k] = [];
	}
	foreach($prod_attributes AS $a){
		$products_attributes[$a->group_id][] = $a;
	}
	$this->set('products_attributes',$products_attributes);
	//pr($products_attributes);
	
  }

}