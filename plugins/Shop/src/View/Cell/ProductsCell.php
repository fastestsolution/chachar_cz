<?php  
namespace Shop\View\Cell;

use Cake\Cache\Cache;
use Cake\View\Cell;

class ProductsCell extends Cell
{
	public $helpers = [
        'Html'
    ];

    public function display($opt){
        if (!isset($opt['order'])) $opt['order'] = 'name ASC';
        if (!isset($opt['limit'])) $opt['limit'] = 3;
        if (!isset($opt['class'])) $opt['class'] = 'col col-sm-12';
		$this->set('class',$opt['class']);
		
		if (isset($opt['ids']) && empty($opt['ids'])){
			return false;
		}
			
		
        //pr($limit);
		$conditions = [];
		
		// posledni upravene
		if ($opt['type'] == 'last_change'){
			$conditions['ShopProducts.modified >='] = date('Y-m-d', strtotime("-30 days"));
		}
		// souvisejici
		if ($opt['type'] == 'related_products'){
			$conditions['ShopProducts.id IN'] = $opt['ids'];
			
		}
		
		// kategorie
		if ($opt['type'] == 'category'){
			$conditions['ShopProducts.shop_category_id IN'] = $opt['shop_category_id'];
			if ($opt['shop_product_ids']){
				$conditions['ShopProducts.id IN'] = $opt['shop_product_ids'];
				
			}
		}
		
		//pr($conditions);
		
		$this->loadModel('Shop.ShopProducts');
        
			$query = $this->ShopProducts->find()
			->select([
			])
			->where($conditions)
			->limit($opt['limit'])
			->order($opt['order'])
			->cache(function ($query) {
				return 'productsCell-' . md5(serialize($query->clause('where'))).md5(serialize($query->clause('order'))).md5(serialize($query->clause('limit')));
			});
			
			$products = $query->toArray();
		//pr($products);
		
		if (isset($opt['title']) && $opt['title'] != null){
			$this->set('cell_title',$opt['title']);
		}
		
		if (isset($products) && count($products)>0){
			$this->set('products', $products);
    	
		}
    }

}
?>