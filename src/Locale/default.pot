# LANGUAGE translation of CakePHP Application
# Copyright YEAR NAME <EMAIL@ADDRESS>
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PROJECT VERSION\n"
"POT-Creation-Date: 2017-02-22 07:33+0100\n"
"PO-Revision-Date: YYYY-mm-DD HH:MM+ZZZZ\n"
"Last-Translator: NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <EMAIL@ADDRESS>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: /config/shop_select_config.php:6
msgid "Od nejlevnější"
msgstr ""

#: /config/shop_select_config.php:7
msgid "Od nejdražší"
msgstr ""

#: /config/shop_select_config.php:11
msgid "Dlaždice"
msgstr ""

#: /config/shop_select_config.php:12
msgid "Tabulka"
msgstr ""

#: /src/Controller/ShopOrdersController.php:66;84
msgid "Nejsou vloženy žádné produkty"
msgstr ""

#: /src/Controller/ShopOrdersController.php:142
msgid "Objednávka byla uložena, nyní budete přesměrováni na platební bránu"
msgstr ""

#: /src/Controller/ShopOrdersController.php:148
msgid "Objednávka byla uložena"
msgstr ""

#: /src/Controller/ShopOrdersController.php:152
msgid "Chyba uložení objednávky"
msgstr ""

#: /src/Controller/ShopOrdersController.php:374
msgid "Děkujeme za Vaši objednávku"
msgstr ""

#: /src/Controller/ShopOrdersController.php:441
msgid "Váš košík"
msgstr ""

#: /src/Controller/ShopOrdersController.php:480
msgid "Chyba nebyla aplikována doprava zdarma"
msgstr ""

#: /src/Controller/ShopOrdersController.php:484
msgid "Chyba ověření ceny dopravy, cena dopravy neodpovídá systémové ceně"
msgstr ""

#: /src/Controller/ShopOrdersController.php:533
msgid "V košíku nemáte žádné produkty"
msgstr ""

#: /src/Controller/ShopOrdersController.php:699
msgid "Produkt nenalezen"
msgstr ""

#: /src/Controller/ShopOrdersController.php:708
msgid "Počet ks upraven"
msgstr ""

#: /src/Controller/ShopProductsController.php:148;419;448
msgid "Produkty"
msgstr ""

#: /src/Controller/ShopProductsController.php:378;453
msgid "Kategorie"
msgstr ""

#: /src/Controller/ShopProductsController.php:449
msgid "Nenalezeny žádné produkty"
msgstr ""

#: /src/Controller/ShopProductsController.php:454
msgid "Nenalezeny žádné kategorie"
msgstr ""

#: /src/Model/Table/ShopOrdersTable.php:84;86
msgid "Musíte zvolit Dopravu"
msgstr ""

#: /src/Model/Table/ShopOrdersTable.php:85;87
msgid "Musíte zvolit Platbu"
msgstr ""

#: /src/Template/Element/Basket/layout_basket.ctp:7
#: /src/Template/ShopOrders/elements/basket_content.ctp:20
msgid "Váš košík je prázdný"
msgstr ""

#: /src/Template/Element/Basket/layout_basket.ctp:12;12
msgid "Přejít do košíku"
msgstr ""

#: /src/Template/ShopOrders/basket_complete.ctp:5
msgid "Platba je v pořádku"
msgstr ""

#: /src/Template/ShopOrders/basket_complete.ctp:5
msgid "vaše objednávka je zaplacena."
msgstr ""

#: /src/Template/ShopOrders/basket_complete.ctp:9
msgid "Chyba platby"
msgstr ""

#: /src/Template/ShopOrders/basket_complete.ctp:9
msgid "vratťe se zpět do košíku a vyberte jinou platební metodu"
msgstr ""

#: /src/Template/ShopOrders/basket_complete.ctp:11;11
msgid "Zpět do košíku"
msgstr ""

#: /src/Template/ShopOrders/basket_complete.ctp:21
msgid "Děkujeme za objednávku"
msgstr ""

#: /src/Template/ShopOrders/basket_complete.ctp:21
msgid "Vaše objednávky byla dokončena, nyní počkejte na její potvrzení."
msgstr ""

#: /src/Template/ShopOrders/basket_detail.ctp:16
msgid "Musíte souhlasit s <a href=\"#\" target=\"_blank\" data-toggle=\"modal\" data-target=\"#vopModal\">všeobecnýma podmínkama</a>"
msgstr ""

#: /src/Template/ShopOrders/basket_detail.ctp:18
msgid "Chci zasílat novinky emailem"
msgstr ""

#: /src/Template/ShopOrders/basket_detail.ctp:25
msgid "Cena celkem bez DPH"
msgstr ""

#: /src/Template/ShopOrders/basket_detail.ctp:26
msgid "Cena celkem s DPH"
msgstr ""

#: /src/Template/ShopOrders/basket_detail.ctp:32
msgid "Odeslat objednávku"
msgstr ""

#: /src/Template/ShopOrders/elements/basket_content.ctp:4
msgid "Název"
msgstr ""

#: /src/Template/ShopOrders/elements/basket_content.ctp:5
msgid "Cena"
msgstr ""

#: /src/Template/ShopOrders/elements/basket_content.ctp:6
msgid "ks"
msgstr ""

#: /src/Template/ShopOrders/elements/basket_content.ctp:7
msgid "cena bez dph"
msgstr ""

#: /src/Template/ShopOrders/elements/basket_content.ctp:8
msgid "cena s dph"
msgstr ""

#: /src/Template/ShopOrders/elements/basket_content.ctp:17
msgid "Smazat"
msgstr ""

#: /src/Template/ShopOrders/elements/basket_content.ctp:17
msgid "Opravdu smazat?"
msgstr ""

#: /src/Template/ShopOrders/elements/gopay.ctp:8
msgid "Není založena platba! Kontaktujte telefonicky provozovnu."
msgstr ""

#: /src/Template/ShopOrders/elements/logged_info.ctp:4
msgid "Tip:"
msgstr ""

#: /src/Template/ShopOrders/elements/logged_info.ctp:4
msgid "Pokud jste již delali objednávku prihlašte se. <a href=\"#\" id=\"move_to_login\" class=\"btn btn-primary\">Prihlásit se</a>"
msgstr ""

#: /src/Template/ShopOrders/elements/payment.ctp:1;35
msgid "Platba"
msgstr ""

#: /src/Template/ShopOrders/elements/payment.ctp:45
msgid "Není definovaná žádná platba"
msgstr ""

#: /src/Template/ShopOrders/elements/transport.ctp:1;37
msgid "Doprava"
msgstr ""

#: /src/Template/ShopOrders/elements/transport.ctp:48
msgid "Není definovaná žádná doprava"
msgstr ""

#: /src/Template/ShopOrders/elements/vop_modal.ctp:8
msgid "Všeobecné obchodní podmínky"
msgstr ""

#: /src/Template/ShopOrders/elements/vop_modal.ctp:34
msgid "Zavřít"
msgstr ""

#: /src/Template/ShopProducts/detail.ctp:14
msgid "Technická specifikace produktu "
msgstr ""

#: /src/Template/ShopProducts/detail.ctp:49
msgid "Výrobce"
msgstr ""

#: /src/Template/ShopProducts/detail.ctp:49;50
msgid "Nevybráno"
msgstr ""

#: /src/Template/ShopProducts/detail.ctp:50
msgid "Dostupnost"
msgstr ""

#: /src/Template/ShopProducts/detail.ctp:51
msgid "Počet ks skladem"
msgstr ""

#: /src/Template/ShopProducts/detail.ctp:70
msgid "Související produkty"
msgstr ""

#: /src/Template/ShopProducts/items.ctp:27
msgid "Omlováme se"
msgstr ""

#: /src/Template/ShopProducts/items.ctp:27
msgid "nebyly nalezeny žádné produkty."
msgstr ""

#: /src/Template/ShopProducts/elements/filtration.ctp:9
msgid "jmeno"
msgstr ""

#: /src/View/Helper/ShopHelper.php:27
msgid "Ks"
msgstr ""

#: /src/View/Helper/ShopHelper.php:38
msgid "Přidat do košíku"
msgstr ""

