<?php

namespace App\View\Helper;

use Cake\View\Helper;
use Cake\View\View;

class AutotableHelper extends Helper
{

  public $helpers = ['Paginator','Fastest'];
  public $tableth = "";
  private $cname = [];

  public function th($ckey, $cvalue){
    //pr($ckey);
	if(!is_array($cvalue)){
      if (strpos($ckey,'.') !== false){
		  $ckey = explode('.',$ckey);
		  
		  $ckey = ucfirst($ckey[0]).'s.'.$ckey[1];
		
	  }
		  //pr($ckey);
		if (isset($this->format_row[0][$ckey]['format']['class']) && strpos($this->format_row[0][$ckey]['format']['class'],'none') !== false) {
			//pr($ckey);
		} else {
			$this->tableth .= '<th class="fixed_top"><div class="tab_th">'.$this->Paginator->sort($ckey, self::translateCol($ckey)).'</div></th>';
    		
		}
	}
    else{
		//pr($cvalue);
      foreach($cvalue as $k => $v) {
        //pr($v);
		self::th($ckey.".".$k, $v);
      }
    }
  }
  
  private function format_price($item_row,$db_col){
	$this->format_row[$item_row][$db_col]['type_format'] = ['price'=> 'price'];
			
  }
  
  private function format_km_prumer($item_row,$db_col,$item){
	if (isset($this->vIndex['spec_class'][$db_col][$item['car_type_id']])){
		foreach($this->vIndex['spec_class'][$db_col][$item['car_type_id']] AS $kf=>$f){
			$range = explode('-',$kf);
			if ($item['km_total'] < 2500){
				if (isset($this->vIndex['format'][$db_col][$db_col]['format']['km_prumer'])){
				
					$this->format_row[$item_row][$db_col] = array_merge($this->vIndex['format'][$db_col]['km_prumer'],['format'=>'color_mensi']);
					$this->format_row[$item_row][$db_col] = array_merge($this->vIndex['format'][$db_col]['km_prumer'],['type_format'=>'color_mensi']);
				} else {	
					$this->format_row[$item_row][$db_col]['type_format'] = ['color_mensi'=>'color_mensi'];
					$this->format_row[$item_row][$db_col]['format'] = ['class'=>'color_mensi'];
				
				}
			}
			if ($item['km_prumer'] > $range[0] && $item['km_prumer'] < $range[1]){
				if (isset($this->vIndex['format']['km_prumer'])){
					$this->format_row[$item_row][$db_col]['format'] = array_merge($this->vIndex['format']['km_prumer'],['data-color'=>$f,'class'=>'spec_color']);
				} else	{
					$this->format_row[$item_row][$db_col]['format'] = ['data-color'=>$f,'class'=>'spec_color'];
					
				};
			}		
			
		}
	}
			
  }
  
  private function format_note_check($item_row,$db_col,$item){
	if (isset($this->vIndex['detail']) && isset($this->vIndex['detail'][$db_col])){
		$this->format_row[$item_row][$db_col]['format'] = ['class'=>'detail'];
		$detail_value = $item[$this->vIndex['detail'][$db_col]];
		//pr($detail_value);
		if (!empty($detail_value)){
			$this->format_row[$item_row][$db_col]['type_format'] = ['detail_value'=>$detail_value];
		}
		$this->format_row[$item_row][$this->vIndex['detail'][$db_col]]['format'] = ['class'=>'none'];
		
	}
					
  }
  
  
  private function format_car_type_id($item_row,$db_col,$item){
	
	if (isset($this->vIndex['spec_class'][$db_col][$item['car_type_id']]))	{
	
		foreach($this->vIndex['spec_class'][$db_col][$item['car_type_id']] AS $kf=>$f){
			$range = explode('-',$kf);
			if ($item['km_total'] < 2500){
				$this->format_row[$item_row][$db_col]['format'] = ['class'=>'bold'];
			}
			if ($item['km_prumer'] > $range[0] && $item['km_prumer'] < $range[1]){
				$this->format_row[$item_row][$db_col]['format'] = ['class'=>'bold'];
					
			}
		}
	}				
  }
  
  private function format_driver_date_unload($item_row,$db_col,$item){
	//pr($item);
	if (isset($item['spatne_vylozeni']) && $item['spatne_vylozeni'] == 1){
		//pr($this->vIndex['format']['dispecer_date_unload']);
		//pr(array_merge($this->vIndex['format']['dispecer_date_unload'],['class'=>'bold date_vylozeni']));
		if (isset($this->vIndex['format']['dispecer_date_unload']))
			$this->format_row[$item_row][$db_col]['format'] = array_merge($this->vIndex['format']['dispecer_date_unload']['format'],['class'=>'bold date_vylozeni']);
		else	
			$this->format_row[$item_row][$db_col]['format'] = ['class'=>'bold date_vylozeni'];
		//pr($this->format_row[$item_row][$db_col]['format']);
	}
			
  }

  public function conditions_cols($items,$viewIndex){
		//pr($items);
		
		$this->format_row = null;
		$this->format_row = [];
		$this->vIndex = null;
		$this->vIndex = $viewIndex;
		foreach($items AS $item_row=>$item){
			$item = $item->toArray();
			if (isset($viewIndex['format'])){
				$this->format_row[$item_row] = $viewIndex['format'];
			}
		
			
			foreach($item AS $db_col=>$item_value){
			
			// price
			if ($db_col == 'price') $this->format_price($item_row,$db_col);
			// price
			if ($db_col == 'total_price') $this->format_price($item_row,$db_col);
			
			// cervene datum vylozeni
			if ($db_col == 'driver_date_unload') $this->format_driver_date_unload($item_row,$db_col,$item);
			
			// detail tooltip
			if ($db_col == 'note_check') $this->format_note_check($item_row,$db_col,$item);
			
			// prumerna spotreba
			if ($db_col == 'km_prumer') $this->format_km_prumer($item_row,$db_col,$item);
				
			// car_type_id format
			if ($db_col == 'car_type_id') $this->format_car_type_id($item_row,$db_col,$item);
				
			
			
			
			}
		}
		//pr($viewIndex['format']);
		//pr($this->format_row);die();
		//pr($viewIndex['format']);
		//return $params_viewIndex['format'];
	//die('aa');
  }
  
  public function td($row_id,$cvalue,$params_viewIndex=null,$spec_value=null,$db_col = null){
    //pr($db_col);
	//pr($params_viewIndex['format']);
	//pr($format);
	
	if(!is_array($cvalue)){
		if ($_SERVER['REMOTE_ADDR'] != '89.102.102.21a'){
			if (isset($this->format_row[$row_id][$db_col]['format'])){
				$format_value = '';
				//pr($this->format_row[$row_id][$db_col]['format']);
				foreach($this->format_row[$row_id][$db_col]['format'] AS $key_format =>$value_format){
					if (is_array($value_format)){
						
						foreach($value_format AS $val_key=>$val){
							$format_value .= ' '.$val_key.'="'.$val.'" ';
						}
					} else {
						$format_value .= ' '.$key_format.'="'.$value_format.'" ';
						
					}
					
				}
				
			}
			
				
				//echo '<td class="'.(isset($this->color)?'spec_color':'').'" data-color="'.(isset($this->color)?$this->color:'').'">'.($this->color_mensi?'('.$cvalue.')':$cvalue).'</td>'; 
				//pr($this->format_row[$row_id]);
				echo '<td '.(isset($format_value)?$format_value:'').'>';
					if (isset($this->format_row[$row_id][$db_col]['type_format'])){
						foreach($this->format_row[$row_id][$db_col]['type_format'] AS $key_format => $v_value){
							if ($key_format == 'short_date') echo (!empty($cvalue)?$cvalue->format('d.m.'):'');
							if ($key_format == 'price') echo $this->Fastest->price($cvalue);
							if ($key_format == 'color_mensi') echo '('.$cvalue.')';
							if ($key_format == 'detail_value') echo '<div class="detail_table"><div class="none">'.$v_value.'</div>'.$cvalue.'</div>';
							
						}
					} else {
						echo $cvalue;
						
					}
					
					
					
				echo '</td>'; 
				
		} else {
	
	 if (isset($this->format) && !isset($this->color)){
		if ($this->format == 'none') echo '<td class="'.$this->format.'">'.$cvalue.'</td>';
		if ($this->format == 'spec_color') echo '<td class="'.$this->format.'">'.$cvalue.'</td>';
		if ($this->format == 'bold') echo '<td class="'.$this->format.'">'.$cvalue.'</td>';
		if ($this->format == 'price') echo '<td class="'.$this->format.'">'.$this->Fastest->price($cvalue).'</td>';
		if ($this->format == 'date_vylozeni') echo '<td class="'.$this->format.'">'.$cvalue.'</td>';
		if ($this->format == 'detail') echo '<td class="'.$this->format.'"><div class="'.(isset($this->detail_value)?'detail_table':'').'"><div class="none">'.$this->detail_value.'</div>'.$cvalue.'</div></td>';
	  } else {
		 echo '<td class="'.(isset($this->color)?'spec_color':'').'" data-color="'.(isset($this->color)?$this->color:'').'">'.($this->color_mensi?'('.$cvalue.')':$cvalue).'</td>'; 
      }
	  	
		}
	
    }
    else{
		//pr($this->spotreba_color);
			
		//pr($cvalue);
      foreach($cvalue as $k=>$v) {
		
		if (isset($params_viewIndex['list'][$k])){
			
			if ($k == 'user_id'){
				$params_viewIndex['list']['user_id'][1000] = 'Superadmin';
				
			}
			if (isset($params_viewIndex['list'][$k][$v])){
				
				$v = $params_viewIndex['list'][$k][$v];
				
			} else {
				$v = __('Nevybráno');
			}
		}
		if ($k == 'client'){
			$v = $v['name'];
		}
		if ($k == 'adresa_nalozeni' || $k == 'adresa_vylozeni'){
			$v = $v['stat'].', '.$v['psc'].', '.$v['mesto'];
		}
		//pr($params_viewIndex['detail'][$k]);
		
		
		if ($k == 'price'){
			$this->format = 'price';
		} elseif ($k == 'driver_date_unload'){
			//pr($spec_value['spatne_vylozeni']);
			if (isset($spec_value['spatne_vylozeni']) && $spec_value['spatne_vylozeni'] == 1){
				//if (isset($params_viewIndex['format']['dispecer_date_unload']['class']))
				//$params_viewIndex['format']['dispecer_date_unload'] = ['class'=>'bold date_vylozeni'];
				//pr($params_viewIndex['format']);
				$this->format = 'date_vylozeni';
			}
		} elseif (isset($params_viewIndex['detail'][$k])){
			$params_viewIndex['format'][$k] = ['class'=>'detail'];
			$this->format = 'detail';
			if ($cvalue[$k] == 1)
				//$params_viewIndex['format'][$k]
				$this->detail_value = $cvalue[$params_viewIndex['detail'][$k]];
			else
				unset($this->detail_value);
			//pr($this->detail_value);
			//pr($params_viewIndex['detail'][$k]);
			//unset($cvalue[$params_viewIndex['detail'][$k]]);
			//pr($this->format);
		
		
		} elseif (isset($params_viewIndex['spec_class']) && isset($params_viewIndex['spec_class'][$k])){
				
			// prumerna spotreba
			if ($k == 'km_prumer'){
					
				if (isset($params_viewIndex['spec_class'][$k][$cvalue['car_type_id']]))	
				foreach($params_viewIndex['spec_class'][$k][$cvalue['car_type_id']] AS $kf=>$f){
					$range = explode('-',$kf);
					//pr($cvalue['km_prumer']);
					//pr($range);
					if ($cvalue['km_total'] < 2500){
						if (isset($params_viewIndex['format']['km_prumer']))
							$params_viewIndex['format']['km_prumer'] = array_merge($params_viewIndex['format']['km_prumer'],['type_format'=>'color_mensi']);
						else	
							$params_viewIndex['format']['km_prumer']['type_format'] = 'color_mensi';
						//pr($params_viewIndex['format']);
						$this->color_mensi = true;
					}
					if ($cvalue['km_prumer'] > $range[0] && $cvalue['km_prumer'] < $range[1]){
						if (isset($params_viewIndex['format']['km_prumer'])){
							$params_viewIndex['format']['km_prumer'] = array_merge($params_viewIndex['format']['km_prumer'],['data-color'=>$f,'class'=>'spec_color']);
							//pr($params_viewIndex['format']['km_prumer']);
							
						} else	
							$params_viewIndex['format']['km_prumer'] = ['data-color'=>$f,'class'=>'spec_color'];
						//pr($params_viewIndex['format']);
						$this->color = $f;
					} else {
						//unset($this->color);
					}
					
				}
				
				//$this->format = 'spec_color';
			}
			
			// car type id bold
			if ($k == 'car_type_id'){
				
				if (isset($params_viewIndex['spec_class'][$k][$cvalue['car_type_id']]))	
				foreach($params_viewIndex['spec_class'][$k][$cvalue['car_type_id']] AS $kf=>$f){
					$range = explode('-',$kf);
					//pr($cvalue['km_prumer']);
					//pr($cvalue);
					if ($cvalue['km_total'] < 2500){
						$params_viewIndex['format']['car_type_id'] = ['class'=>'bold'];
						$this->format = 'bold';
					}
					if ($cvalue['km_prumer'] > $range[0] && $cvalue['km_prumer'] < $range[1]){
						$params_viewIndex['format']['car_type_id'] = ['class'=>'bold'];
						$this->format = 'bold';
					} else {
						//unset($this->format);
					}
					
				}
				
			}
			
		} elseif (isset($params_viewIndex['detail']) && in_array($k,$params_viewIndex['detail'])){
			//pr($k);
			$params_viewIndex['format'][$k] = ['class'=>'none'];
			//pr($params_viewIndex['format']);
			$this->format = 'none';
			//pr($this->detail_value);
			//pr($params_viewIndex['detail'][$k]);
			//unset($cvalue[$params_viewIndex['detail'][$k]]);
			//pr($this->format);
		} else {
			if (isset($this->format)){
				unset($this->format);
			}
			if (isset($this->color)){
				unset($this->color);
			}
			if (isset($this->color_mensi)){
				unset($this->color_mensi);
			}
			
			if (isset($params_viewIndex['bold']) && in_array($k,$params_viewIndex['bold'])){
				$this->format = 'bold';
			}
		}
		//pr($cvalue);
		//pr($this->format);
		//pr($params_viewIndex['format']);
		self::td($row_id,$v,$params_viewIndex,$spec_value,$k,(isset($params_viewIndex['format'])?$params_viewIndex['format']:''));
      }
    }
  }


  // překládá název sloupce pokud je překlad vyplněn v config/autotable_translate.php kde je to děláno skrze get text.
  // může tam být jak překlad sloupce všeobecně, tak překlad sloupce konkrétně pro nějaký Model
  // Pokud ani to nepomůže, můžeš si ty překlady přepsat ve volání helperu, ale musíš si to trochu poupravit.
  // tzn. nastavíš si třeba proměnnou v controlleru a přidáš si ji jako parametr do volání fce th();
  private function translateCol($ckey)
  {
    if ($this->config("colTranslate." . @key($this->request->paging).".".$ckey)) {
      return $this->config("colTranslate." . @key($this->request->paging).".".$ckey);
    }
    else if($this->config("colTranslate.".$ckey)){
      return $this->config("colTranslate." . $ckey);
    }
    else{
      return $ckey;
    }
  }


}