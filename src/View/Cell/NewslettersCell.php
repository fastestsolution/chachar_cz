<?php
namespace App\View\Cell;

use Cake\View\Cell;

class NewslettersCell extends Cell
{

  public function display()
  {
    $this->loadModel("Newsletters");
    $newsletter = $this->Newsletters->newEntity();

    $this->set("newsletter", $newsletter);
	
	if ($this->request->is('ajax')){
		$this->Newsletters->patchEntity($newsletter, $this->request->data());
		pr($newsletter);die();
		$this->check_error($newsletter);
		
		die(json_encode(['r'=>true,'m'=>__('Váš email byl uložen')]));
	}
	
  }

}