<?php
//pr($_SERVER['HTTP_HOST']);
/*
./letsencrypt-auto --apache -d manikova-pizza.cz -d www.manikova-pizza.cz -d chachar.cz -d www.chachar.cz -d mechovackova-pizza.cz -d www.mechovackova-pizza.cz -d ogarova-pizza.cz -d www.ogarova-pizza.cz -d panarova-pizza.cz -d www.panarova-pizza.cz -d pepikova-pizza.cz -d www.pepikova-pizza.cz -d pepinova-pizza.cz -d www.pepinova-pizza.cz -d petanova-pizza.cz -d www.petanova-pizza.cz -d partakova-pizza.cz -d www.partakova-pizza.cz -d boleslav.partakova-pizza.cz -d krnov.pizzaprotebe.cz -d litomerice.partakova-pizza.cz -d opava.pizzaprotebe.cz -d boleslav.partakova-pizza.cz -d www.pizzarecords.cz -d pizzarecords.cz -d cinajede.cz -d cina-jede.cz -d www.cinajede.cz
*/

// detect system id from domain
	$system_ids_list = [
		'dev.manikova-pizza.cz'=>['system_id'=>2,'name'=>'manikova-pizza.cz','seo_category_city'=>'Brno','page_title'=>'Maníkova pizza Brno','page_title2'=>'Maníkova pizza','ga_superadmin'=>'','ga_verification'=>'','dev_project'=>true], // manikova brno DEVELOP
		'www.chachar.cz'=>['system_id'=>1,'name'=>'chachar.cz','seo_category_city'=>'Ostrava','page_title'=>'Chacharova pizza Ostrava','page_title2'=>'Chacharova pizza','ga_superadmin'=>'UA-12331366-11','ga_verification'=>'GoMlwg5SnuOkLcjeHBVUxXh9KHJHuPy8xd8MZvomjww'], // chachar
		'www.manikova-pizza.cz'=>['system_id'=>2,'name'=>'manikova-pizza.cz','seo_category_city'=>'Brno','page_title'=>'Maníkova pizza Brno','page_title2'=>'Maníkova pizza','ga_superadmin'=>'UA-12331366-55','ga_verification'=>'ZzPx_cLRdpH6B5L7Gq_9UzmIoOOV8x9qhu-oo1oDyjk'], // manikova brno
		'www.mechovackova-pizza.cz'=>['system_id'=>3,'name'=>'mechovackova-pizza.cz','seo_category_city'=>'Hradec Králové','page_title'=>'Mechováčkova pizza Hradec Králové','page_title2'=>'Mechováčkova pizza','ga_superadmin'=>'UA-31010675-9','ga_verification'=>'ZzPx_cLRdpH6B5L7Gq_9UzmIoOOV8x9qhu-oo1oDyjk'], // mechovackova hradec
		'www.ogarova-pizza.cz'=>['system_id'=>4,'name'=>'ogarova-pizza.cz','seo_category_city'=>'Zlín','page_title'=>'Ogarova pizza Zlín','ga_superadmin'=>'UA-12331366-53','ga_verification'=>'66FTX5ey2YymFQMVLrVBuJ-YlY39FG-oOy9cDBgmC9o'], // ogarova zlin
		'www.panarova-pizza.cz'=>['system_id'=>5,'name'=>'panarova-pizza.cz','seo_category_city'=>'Plzeň','page_title'=>'Paňárova pizza Plzeň','page_title2'=>'Paňárova pizza','ga_superadmin'=>'UA-31010675-32','ga_verification'=>''], // panarova plzen
		'www.pepikova-pizza.cz'=>['system_id'=>6,'name'=>'pepikova-pizza.cz','seo_category_city'=>'Praha','page_title'=>'Pepikova pizza Praha','page_title2'=>'Pepikova pizza','ga_superadmin'=>'UA-12331366-18','ga_verification'=>''], // pepikova praha
		'www.pepinova-pizza.cz'=>['system_id'=>7,'name'=>'pepinova-pizza.cz','seo_category_city'=>'Olomouc','page_title'=>'Pepinova pizza Olomouc','page_title2'=>'Pepinova pizza','ga_superadmin'=>'UA-12331366-3','ga_verification'=>'uLTvwJUIZwAo2QkEKYfbmi2305xI1NLXV7OeZ0n5ZPw'], // pepinova olomouc
		'www.petanova-pizza.cz'=>['system_id'=>8,'name'=>'petanova-pizza.cz','seo_category_city'=>'Jihlava','page_title'=>'Peťanova pizza Jihlava','page_title2'=>'Peťanova pizza','ga_superadmin'=>'UA-31010675-33','ga_verification'=>'RXBZiWDHRgsKCaNigSbJ0kDMCOS73LPJi2zKF4QXVNY'], // petanova jihlava
		'www.partakova-pizza.cz'=>['system_id'=>9,'name'=>'partakova-pizza.cz','seo_category_city'=>'Liberec','page_title'=>'Parťákova pizza Liberec','page_title2'=>'Parťákova pizza','ga_superadmin'=>'UA-31010675-38','ga_verification'=>'PVrxdgCcoVwMO_keOAyvgbzi9t5x4XmQmcewxj2DwDA','rozcestnik'=>true], // partakova liberec
		'boleslav.partakova-pizza.cz'=>['system_id'=>10,'name'=>'boleslav.partakova-pizza.cz','seo_category_city'=>'Mladá Boleslav','page_title'=>'Parťákova pizza Mladá Boleslav','page_title2'=>'Parťákova pizza','ga_superadmin'=>'UA-51636743-34','ga_verification'=>'PVrxdgCcoVwMO_keOAyvgbzi9t5x4XmQmcewxj2DwDA','rozcestnik'=>true], // partakova boleslav
		'krnov.pizzaprotebe.cz'=>['system_id'=>11,'name'=>'krnov.pizzaprotebe.cz','seo_category_city'=>'Krnov','page_title'=>'Pizza pro tebe Krnov','page_title2'=>'Pizza pro tebe','ga_superadmin'=>'UA-77335645-26','ga_verification'=>'Bc2n_FpRUvRVvtuGMGD2fmP7G7xjWWHvX51IkTS4SLY'], // krnov pizza pro tebe
		'litomerice.partakova-pizza.cz'=>['system_id'=>12,'name'=>'litomerice.partakova-pizza.cz','seo_category_city'=>'Litomeřice','page_title'=>'Parťákova pizza Litoměřice','page_title2'=>'Parťákova pizza','ga_superadmin'=>'UA-77335645-27','ga_verification'=>'Bc2n_FpRUvRVvtuGMGD2fmP7G7xjWWHvX51IkTS4SLY'], // krnov pizza pro tebe
		'opava.pizzaprotebe.cz'=>['system_id'=>13,'name'=>'opava.pizzaprotebe.cz','seo_category_city'=>'Opava','page_title'=>'Pizza pro tebe Opava','page_title2'=>'Pizza pro tebe','ga_superadmin'=>'UA-12331366-70','ga_verification'=>'Bc2n_FpRUvRVvtuGMGD2fmP7G7xjWWHvX51IkTS4SLY'], // opava pizza pro tebe
		'www.cinajede.cz'=>['system_id'=>14,'name'=>'www.cinajede.cz','seo_category_city'=>'Česká republika','page_title'=>'Čína jede','ga_superadmin'=>'UA-12331366-49','ga_verification'=>'Bc2n_FpRUvRVvtuGMGD2fmP7G7xjWWHvX51IkTS4SLY'], // opava pizza pro tebe
		'www.pizzarecords.cz'=>['system_id'=>15,'name'=>'www.pizzarecords.cz','seo_category_city'=>'Česká republika','page_title'=>'Pizza Records','ga_superadmin'=>'UA-51636743-13','ga_verification'=>'Bc2n_FpRUvRVvtuGMGD2fmP7G7xjWWHvX51IkTS4SLY'], // opava pizza pro tebe
		'chachar-novy.localhost'=>['system_id'=>1,'name'=>'www.pizzarecords.cz','seo_category_city'=>'Česká republika','page_title'=>'Pizza Records','ga_superadmin'=>'UA-51636743-13','ga_verification'=>'Bc2n_FpRUvRVvtuGMGD2fmP7G7xjWWHvX51IkTS4SLY'], // opava pizza pro tebe
		'chachar.fastestdev.cz'=>['system_id'=>1,'name'=>'www.pizzarecords.cz','seo_category_city'=>'Česká republika','page_title'=>'Pizza Records','ga_superadmin'=>'UA-51636743-13','ga_verification'=>'Bc2n_FpRUvRVvtuGMGD2fmP7G7xjWWHvX51IkTS4SLY'], // opava pizza pro tebe
	];

	$this->chatBotsProjects = [
		1=>'chachar',
		2=>'manik',
		3=>'mechovacek',
		4=>'ogar',
		5=>'panar',
		6=>'pepik',
		7=>'pepin',
		8=>'petan',
		9=>'partak',
		10=>'boleslav',
		11=>'krnov',
		12=>'litomerice',
		13=>'opava',
		14=>'cina',
		15=>'records',
	];
	$this->set('chatBotsProjects',$this->chatBotsProjects);

	$this->set('new_address_list',$this->new_address_list = [
		1, // chachar
		2, // manik
		9, // partak
		10, // boleslav
		16, // boleslav
		1000, // chachar
	]);

	$this->system_list = [];
	$this->redirect_system_ids = [];
	foreach($system_ids_list AS $system_url=>$s){
		$this->system_list[$s['system_id']] = $s['name'];
		$this->redirect_system_ids[$s['system_id']] = $system_url;
	}
	$this->set('system_list',$this->system_list);

	$this->set('sugo_list', $this->sugo_list = [
		1=>__('Rajčata'),
		2=>__('Smetana'),
		3=>__('BBQ'),
	]);
	//pr($this->sugo_list);die();
	$this->set('sugo_zaklad_list', $this->sugo_zaklad_list = [
		1=>__('Eidam'),
		2=>__('Mozzarela'),
		3=>__('Vegan'),
	]);

	//if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
	$domain = explode('.',$_SERVER['HTTP_HOST']);
	if(isset($domain[2]))
	$domain = $domain[1].'.'.$domain[2];
	else
	$domain = $domain[0].'.'.$domain[1];
	//pr($domain);
	//die('a');
	//}
	//pr($domain);
	if ($domain == 'cinajede.cz'){
		//$_SERVER['HTTP_HOST'] = 'www.manikova-pizza.cz';
		$this->request->query['template'] = 'cina';
	}
	if ($domain == 'pizzarecords.cz'){
		//$_SERVER['HTTP_HOST'] = 'www.manikova-pizza.cz';
		$this->request->query['template'] = 'records';
	}


	//pr($_SERVER['HTTP_HOST']);
	if (isset($system_ids_list[$_SERVER['HTTP_HOST']])){
		$this->system_id = $system_ids_list[$_SERVER['HTTP_HOST']]['system_id'];
		$this->system_id_data = $system_ids_list[$_SERVER['HTTP_HOST']];
		$this->set('system_id_data',$this->system_id_data);
		$this->set('system_id',$this->system_id);
	}

	if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
		//die('a');
	}


	if (isset($this->system_id_data['dev_project'])){
		$this->set('dev_project',$this->dev_project = true);
		$this->Session->write('dev_project',true);
	}

	if (!isset($this->request->query['system_id']) && $this->Session->read('tmp_system_id') && !empty($this->Session->read('tmp_system_id'))){
		$tmp_system_id = $this->Session->read('tmp_system_id');

	}

	if (isset($this->request->query['system_id']) || isset($tmp_system_id)){
		if (isset($tmp_system_id)){
			$this->system_id = $tmp_system_id;
		} else {
			$this->system_id = $this->request->query['system_id'];

		}
		$this->Session->write('tmp_system_id',$this->system_id);
		//pr($this->Session->read('tmp_system_id'));

		foreach($system_ids_list AS $system_url => $s){


			if ($s['system_id'] == $this->system_id){
				$this->system_id_data = $s;
				$this->set('system_id_data',$this->system_id_data);

			}

		}
		$this->set('system_id',$this->system_id);

	}
	$this->set('redirect_system_ids',$this->redirect_system_ids);
	//pr($this->system_id);
	$this->Session->write('system_id_data',$this->system_id_data);
	$this->Session->write('system_id',$this->system_id);

	// css styles
	$css_styles = [
		// chachar
		1=> [
			'logo'=>'chachar',
		],
		// manikova
		2=> [
			'logo'=>'manik',
		],
		// mechovacek
		3=> [
			'logo'=>'mechovacek',
		],
		// ogar
		4=> [
			'logo'=>'ogar',
		],
		// panar
		5=> [
			'logo'=>'panar',
		],
		// pepik
		6=> [
			'logo'=>'pizza',
		],
		// pepin
		7=> [
			'logo'=>'pepin',
		],
		// petan
		8=> [
			'logo'=>'petan',
		],

		// partak
		9=> [
			'logo'=>'partak',
		],
		// partak boleslav
		10=> [
			'logo'=>'partak',
		],
		// krnov
		11=> [
			'logo'=>'pizza',
		],
		// opava
		13=> [
			'logo'=>'pizza',
		],
		// cina
		14=> [
			'logo'=>'pizza',
		],
		// records
		15=> [
			'logo'=>'pizza',
		],
	];
	if (!isset($css_styles[$this->system_id])){
		$url = explode('.',$_SERVER['HTTP_HOST']);
		$redirect_link = 'http://www.'.$url[1].'.'.$url[2];
		header('Location: '.$redirect_link);
		//$this->redirect($redirect_link);
	}
	$this->css_select = $css_styles[$this->system_id];

	$this->set('css_styles',$this->css_select);

	// payment list
	$chachar_payment_list = [
		2=>['name'=>'Platební kartou'],
		//3=>['name'=>'Hotově + naspořené pizza body'],
		1=>['name'=>'Hotově'],
	];
	$this->set('chachar_payment_list',$this->chachar_payment_list = $chachar_payment_list);

	// payment list chatbot
	$chatbot_payment_list = [
		'online'=>2,
		'credits'=>1,
		'cash'=>1,
	];
	$this->set('chatbot_payment_list',$this->chatbot_payment_list = $chatbot_payment_list);

	$chachar_payment_list_short = [
		2=>['name'=>'Karta'],
		3=>['name'=>'Hot.body'],
		1=>['name'=>'Hotově'],
	];
	$this->set('chachar_payment_list_short',$this->chachar_payment_list_short = $chachar_payment_list_short);


	$this->set('phone_pref',$this->phone_pref = [
		'00420'=>'+420',
		'00421'=>'+421',
		'00380'=>'+380',
		'0048'=>'+48',
		'00375'=>'+375',
		'0049'=>'+49',
		'0043'=>'+43',
		'0036'=>'+36',
		'007'=>'+7',
		'0044'=>'+44',
	]);
	$this->set('zdroj_list',$this->zdroj_list = [
		1=>'Web',
		2=>'mPizza',
		3=>'Dáme jídlo',
	]);



	// menu
	$menu_items = [
		//1=>['id'=>1,'name'=>__('Úvod'),'alias'=>'/','group'=>0],
		2=>['id'=>2,'name'=>__('Pizza'),'alias'=>'/pizza/','group'=>1],
		3=>['id'=>3,'name'=>__('Stripsy / Saláty'),'alias'=>'/stripsy/','group'=>2],
		4=>['id'=>4,'name'=>__('Burgery'),'alias'=>'/burgery/','group'=>3],
		5=>['id'=>5,'name'=>__('Čína'),'alias'=>'/cina/','group'=>4],
		6=>['id'=>6,'name'=>__('Špecle'),'alias'=>'/specle/','group'=>5],
		8=>['id'=>8,'name'=>__('Nápoje'),'alias'=>'/napoje/','group'=>8],
		9=>['id'=>9,'name'=>__('Pro mazlíky'),'alias'=>'/pro-mazliky/','group'=>9],
	];
	if(in_array($this->system_id,[2,6,10,9,7])){
		$menu_items[6] = ['id'=>6,'name'=>__('Halušky a Špecle'),'alias'=>'/specle/','group'=>5];
	}
	if(in_array($this->system_id,[11])){
		unset($menu_items[5]);
	}
	//pr($menu_items);

	function convertToObject($array) {
        $object = new stdClass();
        foreach ($array as $key => $value) {
            if (is_array($value)) {
                $value = convertToObject($value);
            }
            $object->$key = $value;
        }
        return $object;
    }
	// vypnuti burgery pro Partakova pizza
	if ($this->system_id == 9){
		unset($menu_items[4]);
	}
	// vypnuti burgery pro Pepikovu pizza
	if ($this->system_id == 6){
		unset($menu_items[4]);
	}
	// vypnuti pro psy vsude krome ogara
	if ($this->system_id != 4){
		unset($menu_items[9]);
	}
	// vypnuti opava pizza
	if ($this->system_id == 13){
		unset($menu_items[2]);
		$menu_items[3]['name'] = 'Saláty a tortilly';
		//unset($menu_items[3]);
	}
	//pr($menu_items);
	if ($_SERVER['REMOTE_ADDR'] != '89.103.18.65'){

		//unset($menu_items[9]);
	}

	$this->menu_items = convertToObject($menu_items);

	$this->set('menu_items',$this->menu_items);
	$this->menu_items_path = [];
	$this->menu_items_path_group = [];
	foreach($menu_items AS $k=>$m){
		$this->menu_items_path[$k] = $m['alias'];
		$this->menu_items_path_group[trim($m['alias'],'/')] = $m['group'];
	}
	$this->set('menu_items_path',$this->menu_items_path);


?>
