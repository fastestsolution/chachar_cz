<?php 
define('dphType',1.21);
define('dphTypeConf',0.1736);

$select_config = [
	
	'order_stav_list' => [
		1=>__('Vytvořena'),
		2=>__('Potvrzena provozovnou'),
		3=>__('Naložena rozvozcem'),
		4=>__('Předána'),
		5=>__('Nebylo možné vyložit'),
		6=>__('Právě vezeme'),
	],
	
	'ano_ne' => [
		0=>__('Ne'),
		1=>__('Ano'),
	],
	
	'product_category_group_list' => [
		1=>__('Pizza'),
		2=>__('Stripsy'),
		3=>__('Burgery'),
		4=>__('Čína'),
		5=>__('Špecle'),
		8=>__('Nápoje'),
	],
	'category_seo' => [
		1=>__('Rozvoz pizzy '),
		2=>__('Rozvoz stripsy '),
		3=>__('Rozvoz burgerů '),
		4=>__('Rozvoz číny '),
		5=>__('Rozvoz špecle '),
		8=>__('Rozvoz nápoje '),
		9=>__('Rozvoz krmiva pro psy '),
	],
	'product_attributes_group_list' => [
		1=>__('Sýry'),
		2=>__('Uzeniny a maso'),
		3=>__('Zelenina'),
		4=>__('Ostatní'),
	],
	'food_type_list' => [
		0=>__('Ne'),
		2=>__('Doporučujeme'),
		3=>__('Doporučujeme - ostrá'),
		4=>__('Novinka'),
		5=>__('Nejprodávanější'),
		6=>__('Pikantní'),
		7=>__('Trvale nízká cena'),
		8=>__('Ostrá'),
		9=>__('Velmi ostrá'),
		10=>__('Giga ostrá'),
	],
	
	'gopay_keys' => [
		1=>['gopay_id'=>8174766688,'gopay_client_id'=>1433911952 ,'gopay_client_secure'=>'u4SpHaTK','secure_key'=>'aJLd7CtJ6e4T9NX4Za4yHGFU'], // test

	],
	
];
?>