<?php
echo '<ul class="pagination">';

//pr($this->Paginator->numbers());
//pr($this->Paginator->numbers());
//pr($this->Paginator->counter('{{pages}}'));
if ($this->Paginator->counter('{{pages}}') > 1){
	echo $this->Paginator->prev('<span class="fa fa-angle-left"></span> ' . __('Předchozí'), ['escape' => false, 'class' => 'btn blue fst_h']);
	echo $this->Paginator->next(__('Další').' <span class="fa fa-angle-right"></span>', ['escape' => false,'class'=>'fst_h']);
}
//echo '<span class="counter">'.$this->Paginator->counter('<span class="current">{{page}}</span>/{{pages}}').'</span>';
echo $this->Paginator->numbers(['modulus'=>5,'first' => __('První'),'last' => __('Poslední')]);
echo '</ul>';
//echo '<div id="total_result">Celkem nalezeno: '.$this->Paginator->counter('{{count}}').' záznamů</div>';
