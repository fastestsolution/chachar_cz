<?php
echo $this->Form->create($contactForm,['url'=>'/saveContactForm/']);
?>
	<?php echo $this->Form->input('email.value', ['label'=>false,'placeholder' => __('Email na který Vám mužete odpovedet'),'class'=>'form-control']); ?>
	<?php echo $this->Form->input('email.name', ['type'=>'hidden','value'=>'Email']); ?>
	<?php echo $this->Form->input('text.value', ['label'=>false,'placeholder' => __('Vaše zpráva'),'class'=>'form-control','type'=>'textarea']); ?>
	<?php echo $this->Form->input('text.name', ['type'=>'hidden','value'=>'Zpráva']); ?>
	
<?php
echo '<div class="row buttons">';
	echo $this->Form->button(__('Odeslat'), ['class' => 'btn btn-lg btn-primary btn-block SaveForm']);
echo '</div>';
echo $this->Form->end();
?>