<?php
echo $this->Form->create($contactForm,['url'=>'/saveContactForm/']);
?>
	<?php echo $this->Form->input('jmeno.value', ['label'=>false,'placeholder' => __('Vaše jméno'),'class'=>'form-control']); ?>
	<?php echo $this->Form->input('jmeno.name', ['type'=>'hidden','value'=>'Jméno']); ?>
	<?php echo $this->Form->input('email.value', ['label'=>false,'placeholder' => __('Email na který Vám můžete odpovědět'),'class'=>'form-control']); ?>
	<?php echo $this->Form->input('email.name', ['type'=>'hidden','value'=>'Email']); ?>
	<?php echo $this->Form->input('telefon.value', ['label'=>false,'placeholder' => __('Telefon, na který můžeme volat:'),'class'=>'form-control']); ?>
	<?php echo $this->Form->input('telefon.name', ['type'=>'hidden','value'=>'Telefon']); ?>
	<?php echo $this->Form->input('provoz.value', ['label'=>false,'placeholder' => __('Provozovna, na které chcete pracovat:'),'class'=>'form-control','options'=>$provoz_list]); ?>
	<?php echo $this->Form->input('provoz.name', ['type'=>'hidden','value'=>'Provoz']); ?>
	<?php echo $this->Form->input('text.value', ['label'=>false,'placeholder' => __('Vaše zpráva:'),'class'=>'form-control','type'=>'textarea']); ?>
	<?php echo $this->Form->input('text.name', ['type'=>'hidden','value'=>'Zpráva']); ?>
	
<?php
echo '<div class="row buttons">';
	echo '<div class="col-md-4 col-md-offset-4">';
		echo $this->Form->button(__('Odeslat'), ['class' => 'btn btn-lg btn-primary btn-block SaveForm']);
	echo '</div>';
echo '</div>';
echo $this->Form->end();
?>