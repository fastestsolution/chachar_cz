<div id="login_element">
<?php if (!isset($loggedUser)){ ?>
	<?php echo $this->Html->link(__('Přihlásit se'),'#',['title'=>__('Přihlásit se'),'class'=>'btn btn-primary show_login','id'=>'mobile_login']); ?>
<?php } else { ?>
	<?php echo $this->Html->link('','#',['title'=>__('Přihlásit se'),'class'=>'avatar avatar'.$loggedUser['avatar_id'].' show_login','id'=>'mobile_login']); ?>
	<?php //pr($loggedUser); ?>
<?php } ?>
<?php /* ?>
<div id="mobile_login" class="fa fa-user show_login"></div>
<?php */ ?>
<div class="row mobile_login_hide">
	<div class="col-md-12">
	<div id="login_line">
		<?php if (!isset($loggedUser)){ ?>
			<span class="login_sipka"><?php echo __('Chcete 20 kreditů zdarma?'); ?></span>
			<?php echo $this->Html->link(__('Přihlásit se'),'#',['title'=>__('Přihlásit se'),'class'=>'show_login btn btn-primary']); ?> 
			<?php //echo $this->Html->link(__('Registrace'),'/'.registrace_link.'/',['title'=>__('Zaregistrovat se')]); ?>
		<?php } else { ?>
		<div class="logged_data">
			<?php echo '<div class="show_login avatar avatar'.$loggedUser['avatar_id'].'"></div>';?>
			<?php echo $this->Html->link($loggedUser['first_name'].' '.$loggedUser['last_name'],'#',['class'=>'show_login']); ?>
			<?php echo '<div class="kredit_info">'.__('Máte').' '.$loggedUser['credits'].' '.__('kreditů').'</div>'; ?>
		</div>
		
			
		<?php } ?>
	</div>
	<div id="login_over" class="none show_login"></div>
		
	<div id="show_login_el" class="load">
		<div class="login_roh"></div>
		<div class="login_close show_login"></div>
		<div class="inner <?php echo (isset($loggedUser)?'right':'')?>">
		<?php if (!isset($loggedUser)){ ?>
		
		<?= $this->Flash->render('auth') ?>
		<?= $this->Form->create(null,['class'=>'form-signin','url' => '/login/']) ?>
			
			<strong class="form-signin-heading"><?= __('Nemám u Vás účet, ale chci 20 kreditů zdarma '). $this->Html->link(__('ZAREGISTROVAT'),'/'.registrace_link.'/',['title'=>__('Registrace')]) ?></strong>
			<div class="row">
				<div class="col col-md-5">
					<?= $this->Form->input('username', ['placeholder' => __('Váš email'),'class'=>'form-control','label'=>false]) ?>
				</div>
				<div class="col col-md-5">
					<?= $this->Form->input('password', ['placeholder' => __('Váše heslo'),'class'=>'form-control','label'=>false]) ?>
					<?php echo $this->Html->link(__('Zapomenuté heslo'),'/'.heslo_link.'/',['title'=>__('Zapomenuté heslo'),'class'=>'forgot_password'],null,false) ?>
		
				</div>
				<div class="col col-md-2">
					<?= $this->Form->submit(__('Přihlásit se'),['class'=>'btn btn-lg btn-primary btn-block SaveForm']); ?>
				
				</div>
			</div>
		<?php //echo $this->Html->link(__('Registrace'),'/'.registrace_link.'/',['title'=>__('Registrace')],null,false) ?>
		<?= $this->Form->end() ?>
		
		<?php } else { ?>
			<div id="logged_menu">
			<?php echo '<strong class="only_mobile">'.$loggedUser['first_name'].' '.$loggedUser['last_name'].'</strong>'; ?>
			<?php echo '<div class="kredit_info only_mobile">'.__('Máte').' '.$loggedUser['credits'].' '.__('kreditů').'</div>'; ?>
			
			<ul>
				<li><?php echo $this->Html->link(__('Vaše objednávky'),'/'.vase_objednavky.'/',['title'=>__('Vaše objednávky'),'class'=>'btn btn-default'],null,false) ?></li>
				<li><?php echo $this->Html->link(__('Změna osobních údajů'),'/'.registrace_link.'/',['title'=>__('Změna osobních údajů'),'class'=>'btn btn-default no-bg'],null,false) ?></li>
				<li><?php echo $this->Html->link(__('Odhlásit se'),'/logout/',['title'=>__('Odhlásit se'),'class'=>'btn btn-primary'],null,false) ?></li>
			</ul>
			<div class="clear"></div>
			</div>
		<?php } ?>
		</div>
		</div>
	</div>
</div>

</div>