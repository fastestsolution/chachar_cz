<?php if(date('Y-m-d') == '2017-12-24' || date('Y-m-d') == '2017-12-25' || date('Y-m-d') == '2017-12-26' || date('Y-m-d') == '2017-12-31' || date('Y-m-d') == '2018-01-01') { ?>
	<div id="intro-vanoce">

	<div class="table-responsive">
	<?php if($system_id == 1) { ?>
		  <table class="table table-bordered table-hover table-striped">
		  <tr>
		    <th>Provoz</th>
		    <th>24.12</th>
		    <th>25.12</th>
		    <th>26.12</th>
		    <th>31.12</th>
		    <th>1.1</th>
		  </tr>
		  <tr>
		    <td>Ostrava centrum<br></td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>10:30 - 24:00<br></td>
		    <td>10:30-16:00</td>
		    <td>10:30-24:00</td>
		  </tr>
		  <tr>
		    <td>Výškovice</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>10:30 - 24:00</td>
		    <td>10:30-16:00</td>
		    <td>10:30-24:00</td>
		  </tr>
		  <tr>
		    <td>Dubina</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>10:30 - 24:00</td>
		    <td>10:30-16:00</td>
		    <td>10:30-24:00</td>
		  </tr>
		  <tr>
		    <td>Zábřeh</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>10:30 - 24:00</td>
		    <td>10:30-16:00</td>
		    <td>10:30-24:00</td>
		  </tr>
		  <tr>
		    <td>Poruba - Sever<br></td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>10:30 - 24:00</td>
		    <td>Zavřeno</td>
		    <td>10:30-24:00</td>
		  </tr>
		  <tr>
		    <td>Poruba - Jih<br></td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>10:30 - 24:00</td>
		    <td>Zavřeno</td>
		    <td>10:30-24:00</td>
		  </tr>
		  <tr>
		    <td>Hrabová</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		  </tr>
		  <tr>
		    <td>Jablůňkov</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		  </tr>
		  <tr>
		    <td>Frýdek-Místek</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>11:00 - 22:00<br></td>
		    <td>11:00 - 17:00<br></td>
		    <td>16:00 - 22:00<br></td>
		  </tr>
		  <tr>
		    <td>Orlová</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>13:00 - 22:00<br></td>
		    <td>Zavřeno</td>
		    <td>13:00 - 22:00<br></td>
		  </tr>
		  <tr>
		    <td>Těšín<br></td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>11:00 - 22:00<br></td>
		    <td>Zavřeno</td>
		    <td>11:00 - 22:00<br></td>
		  </tr>
		  <tr>
		    <td>Karviná</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>11:00 - 22:00</td>
		  </tr>
		  <tr>
		    <td>Havířov</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		  </tr>
		  <tr>
		    <td>Třinec</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>9:30 - 24:00</td>
		    <td>9:30 - 18:00</td>
		    <td>12:00 - 22:30</td>
		  </tr>
		</table>
		<?php } ?>
	
		<?php if($system_id == 2) { ?>
			<table class="table table-bordered table-hover table-striped">
			  <tr>
			    <th>Provoz</th>
			    <th>24.12</th>
			    <th>25.12</th>
			    <th>26.12</th>
			    <th>31.12</th>
			    <th>1.1</th>
			  </tr>
			  <tr>
			    <td>Brno - Západ<br></td>
			    <td>Zavřeno</td>
			    <td>Zavřeno</td>
			    <td>Zavřeno<br></td>
			    <td>10:00 - 16:00</td>
			    <td>14:00 - 23:00<br></td>
			  </tr>
			  <tr>
			    <td>Brno - Jih<br></td>
			    <td>Zavřeno</td>
			    <td>Zavřeno</td>
			    <td>10:00 - 23:00</td>
			    <td>09:00-16:00</td>
			    <td>Zavřeno</td>
			  </tr>
			  <tr>
			    <td>Brno - Východ<br></td>
			    <td>Zavřeno</td>
			    <td>Zavřeno</td>
			    <td>Zavřeno</td>
			    <td>Zavřeno</td>
			    <td>Zavřeno</td>
			  </tr>
			</table>
		<?php } ?>
		
		<?php if($system_id == 4) { ?>
			<table class="table table-bordered table-hover table-striped">
			  <tr>
			    <th>Provoz</th>
			    <th>24.12</th>
			    <th>25.12</th>
			    <th>26.12</th>
			    <th>31.12</th>
			    <th>1.1</th>
			  </tr>
			  <tr>
			    <td>Zlín<br></td>
			    <td>Zavřeno</td>
			    <td>Zavřeno</td>
			    <td>10:00 - 23:00<br></td>
			    <td>10:00 - 18:00</td>
			    <td>12:00 - 23:00<br></td>
			  </tr>
			  <tr>
			    <td>Otrokovice<br></td>
			    <td>Zavřeno</td>
			    <td>Zavřeno</td>
			    <td>10:00 - 23:00</td>
			    <td>10:00 - 18:00</td>
			    <td>12:00 - 23:00</td>
			  </tr>
			  <tr>
			    <td>Valmez<br></td>
			    <td>Zavřeno</td>
			    <td>Zavřeno</td>
			    <td>13:00 - 22:00<br></td>
			    <td>10:00 - 17:00</td>
			    <td>13:00 - 22:00</td>
			  </tr>
			</table>	
		<?php } ?>
		
		<?php if($system_id == 7) { ?>
			<table class="table table-bordered table-hover table-striped">
			  <tr>
			    <th>Provoz</th>
			    <th>24.12.17</th>
			    <th>25.12.17</th>
			    <th>26.12.17</th>
			    <th>31.12.17</th>
			    <th>1.1.18</th>
			  </tr>
			  <tr>
			    <td>Olomouc</td>
			    <td>Zavřeno</td>
			    <td>Zavřeno</td>
			    <td>10:00 - 22:00</td>
			    <td>10:00 - 18:00</td>
			    <td>14:00 - 22:00</td>
			  </tr>
			  <tr>
			    <td>Kromeříž</td>
			    <td>Zavřeno</td>
			    <td>Zavřeno</td>
			    <td>10:00 - 22:00</td>
			    <td>Zavřeno</td>
			    <td>10:00 - 22:00</td>
			  </tr>
			  <tr>
			    <td>Přerov</td>
			    <td>Zavřeno</td>
			    <td>Zavřeno</td>
			    <td>10:00 - 23:00</td>
			    <td>Zavřeno</td>
			    <td>10:00-23:00</td>
			  </tr>
			  <tr>
			    <td>Prostějov</td>
			    <td>Zavřeno</td>
			    <td>Zavřeno</td>
			    <td>Zavřeno</td>
			    <td>Zavřeno</td>
			    <td>Zavřeno</td>
			  </tr>
			  <tr>
			    <td>Hranice</td>
			    <td>Zavřeno</td>
			    <td>Zavřeno</td>
			    <td>10:00 - 23:00</td>
			    <td>Zavřeno</td>
			    <td>Zavřeno</td>
			  </tr>
			</table>		
		<?php } ?>
		
		<?php if($system_id == 9) { ?>
		<table class="table table-bordered table-hover table-striped">
		  <tr>
		    <th>Provoz</th>
		    <th>24.12.17</th>
		    <th>25.12.17</th>
		    <th>26.12.17</th>
		    <th>31.12.17</th>
		    <th>1.1.18</th>
		  </tr>
		  <tr>
		    <td>Mladá Boleslav</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		  </tr>
		  <tr>
		    <td>Litoměřice</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		  </tr>
		  <tr>
		    <td>Liberec</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		  </tr>
		</table>	
		<?php } ?>
		
		
		<?php if($system_id == 3) { ?>
		<table class="table table-bordered table-hover table-striped">
		  <tr>
		    <th>Provoz</th>
		    <th>24.12.17</th>
		    <th>25.12.17</th>
		    <th>26.12.17</th>
		    <th>31.12.17</th>
		    <th>1.1.18</th>
		  </tr>
		  <tr>
		    <td>Hradec Králové</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		  </tr>
		</table>	
		<?php } ?>
		
		<?php if($system_id == 5) { ?>
		<table class="table table-bordered table-hover table-striped">
		  <tr>
		    <th>Provoz</th>
		    <th>24.12.17</th>
		    <th>25.12.17</th>
		    <th>26.12.17</th>
		    <th>31.12.17</th>
		    <th>1.1.18</th>
		  </tr>
		  <tr>
		    <td>Plzeň</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>10:30 – 19:30</td>
		    <td>16:00 – 22:00</td>
		  </tr>
		</table>	
		<?php } ?>
		
		<?php if($system_id == 8) { ?>
		<table class="table table-bordered table-hover table-striped">
		  <tr>
		    <th>Provoz</th>
		    <th>24.12.17</th>
		    <th>25.12.17</th>
		    <th>26.12.17</th>
		    <th>31.12.17</th>
		    <th>1.1.18</th>
		  </tr>
		  <tr>
		    <td>Jihlava</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		  </tr>
		</table>	
		<?php } ?>
		
		<?php if($system_id == 11) { ?>
		<table class="table table-bordered table-hover table-striped">
		  <tr>
		    <th>Provoz</th>
		    <th>24.12.17</th>
		    <th>25.12.17</th>
		    <th>26.12.17</th>
		    <th>31.12.17</th>
		    <th>1.1.18</th>
		  </tr>
		  <tr>
		    <td>Krnov</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>10:30 – 22:00</td>
		    <td>10:30 – 22:00</td>
		    <td>Zavřeno</td>
		  </tr>
		</table>	
		<?php } ?>
		
		<?php if($system_id == 13) { ?>
		<table class="table table-bordered table-hover table-striped">
		  <tr>
		    <th>Provoz</th>
		    <th>24.12.17</th>
		    <th>25.12.17</th>
		    <th>26.12.17</th>
		    <th>31.12.17</th>
		    <th>1.1.18</th>
		  </tr>
		  <tr>
		    <td>Opava</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>10:00 - 22:00</td>
		    <td>do 16:00</td>
		    <td>10:00 - 22:00</td>
		  </tr>
		</table>	
		<?php } ?>
				
		<?php if($system_id == 6) { ?>
		<table class="table table-bordered table-hover table-striped">
		  <tr>
		    <th>Provoz</th>
		    <th>24.12.17</th>
		    <th>25.12.17</th>
		    <th>26.12.17</th>
		    <th>31.12.17</th>
		    <th>1.1.18</th>
		  </tr>
		  <tr>
		    <td>Praha - Jih</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		  </tr>
		  <tr>
		    <td>Praha - Prosek</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		    <td>Zavřeno</td>
		  </tr>
		</table>	
		<?php } ?>
	   
	</div>
	   <p class="ps">Tabulku lze posouvat do stran.</p>
	   
	   <br class="clear" />
	   <a href="#" id="close-intro">Zavřít</a>
	</div>
<?php } ?>