<div id="intro_adresa" data-template="<?php echo (isset($redirect_template)?$redirect_template:'')?>" class="<?php echo (isset($redirect_template)?$redirect_template:'')?>">
<div id="intro_akce">
	<?php //pr($this->request->params); ?>
	<div id="intro_logo" class="logo <?php echo $css_styles['logo']?>"></div>
	<?php 
	$actual_time = date('H:i');
	$from_time = date('10:45');
	$to_time = date('15:00');
	// cina
	if (isset($this->request->query['template']) && $this->request->query['template'] == 'cina'){
		echo '<div id="akce1" class="akce_size all '.$css_styles['logo'].'"></div>';
		
	} 
	elseif (isset($this->request->query['template']) && $this->request->query['template'] == 'records'){
		echo '<div id="akce1" class="akce_size all '.$css_styles['logo'].'"></div>';
		
	} else {
	switch($system_id){
	
	case 2: { // manik
		if (strtotime($actual_time) > strtotime($from_time) && strtotime($actual_time) < strtotime($to_time)){
			echo '<div id="akce1" class="akce_size '.$css_styles['logo'].'"></div>';
		
		} else {
			echo '<div class="akce_switch_over">';
				echo '<div id="akce1" class="akce_size akce_switch '.$css_styles['logo'].'"><div class="akce_txt"><em>* </em> <strong>3 donaty k jakýmkoliv dvoum hlavním jídlům</strong> (pizza, stripsy, burgery, čína, špecle). <br /><strong>Akce platí každý den 10:45 až 15:00</strong></div></div>';
				echo '<div id="akce2" class="akce_size none akce_switch '.$css_styles['logo'].'"><div class="akce_txt"><em>* </em> <strong>Ke každým dvoum pizzám 2x Coca-Cola 0.33l nebo 2x Fanta 0.33l</div></div>';
			echo '</div>';
		}
		break;
	}
	default: {
		echo '<div id="akce2" class="akce_size all '.$css_styles['logo'].'"><div class="akce_txt"><em>* </em> <strong>Ke každým dvoum pizzám 2x Coca-Cola 0.33l nebo 2x Fanta 0.33l</div></div>';
		//echo '<div id="akce2" class="akce_size none akce_switch '.$css_styles['logo'].'"><div class="akce_txt"><em>* </em> <strong>Ke každým dvoum pizzám 2x Coca-Cola 0.33l nebo 2x Fanta 0.33l</div></div>';
		
	}
	}
	}
	
	?>
	
	<div id="search_address">
	<?php
	echo $this->Form->create(null,['url'=>'/searchAddress/']);
	?>
	<div class="address_input">
		<?php  
		$google_adopt = [
			'data-mesto'=>'address-city',
			'data-cp'=>'address-cp',
			'data-ulice'=>'address-street',
			'data-lat'=>'address-lat',
			'data-lng'=>'address-lng',
		]; 
		?>
		<?php 
		if ($_SERVER['REMOTE_ADDR'] == '46.135.104.174'){
			//$new = true;
			//echo '<div id="newAdr"></div>';
		}
		?>
		<?php echo '<div id="search_address_noresult" class="none">'.__('Pokud Vás našeptávač nenašel zadejte nejbližší velké město například Praha, Brno, Ostrava, Olomouc...').'</div>'; ?>
		<?php echo '<div id="search_address_tip" class="none search_address_tip">'.__('Prosím, zadejte přesnou adresu - např. Pelclova 2500, Ostrava').'</div>'; ?>
		<div id="adr_preloader2" class="addressPreloader none"></div>
				
		<?php 
		//if ($_SERVER['REMOTE_ADDR'] == '46.135.104.174'){
		//pr($provoz_list);
		echo $this->Form->input('address_value', ['label'=>false,'placeholder' => __('např. Nádražní 6, Ostrava'),'data-button'=>'SearchAdresa','data-preloader'=>'adr_preloader2','class'=>'google_autocomplete2 form-control searchAddress intro']+$google_adopt); 
		
		//} else {
		//echo $this->Form->input('address_value', ['label'=>false,'placeholder' => __('Zadejte adresu pro doručení'),'data-button'=>'SearchAdresa','data-preloader'=>'adr_preloader','class'=>'form-control google_autocomplete searchAddress']+$google_adopt); 
		
		//}
		?>
		<div id="address_clear" class="fa  fa-close "></div>
		<div class="none">
		<?php echo $this->Form->input('address_lat', ['label'=>false,'class'=>'clear_address']); ?>
		<?php echo $this->Form->input('address_lng', ['label'=>false,'class'=>'clear_address']); ?>
		<?php echo $this->Form->input('address_street', ['label'=>false,'class'=>'clear_address']); ?>
		<?php echo $this->Form->input('address_cp', ['label'=>false,'class'=>'clear_address']); ?>
		<?php echo $this->Form->input('address_city', ['label'=>false,'class'=>'clear_address']); ?>
		</div>
		
	<?php
		//echo '<div class="col-md-2">';
			
			echo $this->Form->button(__('Vyhledat nejbližší provozovnu'), ['class' => 'btn btn-lg btn-primary btn-block','id'=>'SearchAdresa','type'=>'button']);
		//echo '</div>';
		//echo '<div id="map_coords" class="none"  data-json=\''.(isset($map_coords_search)?$map_coords_search:'').'\'></div>';
		$redirect = [];
		
		//pr($redirect);
		echo '<div id="redirect_template" data-type="'.(isset($redirect_template)?$redirect_template:'').'"></div>';
		echo '<div id="redirect_system_ids" class="none" data-json=\''.json_encode($redirect).'\'></div>';
	echo '</div>';
	echo $this->Form->end();
	?>
	</div>
</div>
<div class="adresa_footer"></div>
</div>