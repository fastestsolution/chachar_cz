<nav id="navigation" class="navbar navbar-default el_adopt" data-el="menu_adopt">
  <div class="container-fluid">
    <?php echo $this->Html->link('','/',['title'=>$setting['page_title'],'class'=>'navbar-brand tooltip_active '.$css_styles['logo'],'id'=>'logo'],null,false) ?>
	<!-- Brand and toggle get grouped for better mobile display -->
	<?php /*'data-tooltip'=>"Homepage",'data-tooltip-pos'=>"down"*/ ?>
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
        <span class="sr-only"><?php echo __('Přepnout navigaci'); ?></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
    </div>

    <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
      <?php /* ?>
	  <ul class="nav navbar-nav">
        <li class="active"><a href="#">Link <span class="sr-only">(current)</span></a></li>
        <li><a href="#">Link</a></li>
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
          <ul class="dropdown-menu">
            <li><a href="#">Action</a></li>
            <li><a href="#">Another action</a></li>
            <li><a href="#">Something else here</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">Separated link</a></li>
            <li role="separator" class="divider"></li>
            <li><a href="#">One more separated link</a></li>
          </ul>
        </li>
      </ul>
      <?php */ ?>
	  <?php      
		$options = [
			'class'=>'nav navbar-nav',
			'prefix'=>'',
			'submenu_class'=>'dropdown',
			'href_class'=>'dropdown-toggle',
			'path'=>$menu_items_path,
		];
		//pr($menu_items);
		echo $this->Fastest->generateMenu($menu_items,$options);
		?>
	  
	  	
				
    </div><!-- /.navbar-collapse -->
  </div><!-- /.container-fluid -->
  <?php echo $this->element('Users/login_element'); ?>
</nav>