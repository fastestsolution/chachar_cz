<?= $this->Html->charset() ?>
<?php 
$this->Html->templates([
	'link' => '<a href="'.$url_lang_prefix.'{{url}}"{{attrs}}>{{content}}</a>'
]);
?>
		
<meta name='keywords' content='<?php echo $this->Fastest->seoText((isset($seo)?$seo:''),$setting,'keywords'); ?>'/>
<meta name='description' content='<?php echo $this->Fastest->seoText((isset($seo)?$seo:''),$setting,'description'); ?>'/>
<?php if (isset($on_main_page)){ ?>
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
<?php } else { ?>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<?php } ?>
<meta name="robots" content="all,index,follow" />
<meta name='author' content='Fastest Solution, 2017 All Rights Reserved url: http://www.fastest.cz' />
<meta name='dcterms.rights' content='Fastest Solution, 2017 All Rights Reserved url: http://www.fastest.cz' />
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<link rel="icon" href="/css/layout/favicon.ico" />
<meta name="google-site-verification" content="<?php echo $setting['ga_verification'];?>" />
<meta property="og:url" content="<?php echo $_SERVER['HTTP_HOST']; ?>" />
<meta property="og:title" content="<?php echo $this->Fastest->seoText((isset($seo)?$seo:''),$setting,'title'); ?>" />
<meta property="og:description" content="<?php echo $this->Fastest->seoText((isset($seo)?$seo:''),$setting,'description'); ?>" />
<meta property="og:image" content="<?php echo $this->Fastest->seoText((isset($seoa)?$seo:''),$setting,'img'); ?>" />

<!--[if lt IE 9]>
<meta http-equiv="refresh" content="2;url=http://browsers.fastest.cz">
<![endif]--> 	