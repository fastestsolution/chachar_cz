<?php if (in_array($system_id,[9])){ ?>
<div id="rozcestnik" data-id="<?php echo $system_id;?>" class="<?php echo(isset($rozcesnik_hide)?'none':'');?>">
	<div class="logo <?php echo $css_styles['logo'];?>"></div>
	<div class="roz_desc"><?php echo __('Který provoz je Vám nejblíže?');?></div>
	<?php 
	// partakova pizza
	if($system_id == 9){ 
		$plist = [
			2=>['name'=>'Litoměřice','link'=>'#','class'=>'partak_litomerice','id'=>47],
			1=>['name'=>'Mladá Boleslav','link'=>'http://boleslav.partakova-pizza.cz','class'=>'partak_mb','id'=>46],
			3=>['name'=>'Liberec','link'=>'#','class'=>'partak_liberec','id'=>45],
		];
	}
	echo '<div id="roz_list">';
	foreach($plist AS $k=>$p){
		echo '<a class="roz_item roz'.$k.' '.$p['class'].'" href="'.$p['link'].'" title="'.$p['name'].'" data-id="'.$p['id'].'">';
			echo '<h2>'.$p['name'].'</h2>';
		echo '</a>';
	}
	echo '</div>';
	?>
</div>
<?php } ?>