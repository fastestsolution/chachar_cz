<div id="vop">
<div class="row">
<div class="col col-md-6 col-lg-3">
<div class="txt_item banner"></div>
</div>


<div class="col col-md-6 col-lg-3">
<div class="txt_item">
<div class="icoProgram ico1"></div>
<h2>Co je věrnostní program?</h2>
<p>Za každý nákup všem registrovaným zákazníkům, připíšeme kredity do jejich elektronické peněženky. V každé kampani pro Vás budeme mít vybrané druhy pizz a jídel, za které získáte KREDITY NAVÍC!

Hledejte ikonky (ikona kredity navíc s aktuální počtem bonusových kreditů) a získáte EXTRA KREDITY!

1 KREDIT = 1 KORUNA 
</p>
</div>
</div>

<div class="col col-md-6 col-lg-3">
<div class="txt_item">
<div class="icoProgram ico2"></div>
<h2>Kolik kreditů za nákup získám?</h2>
<p>
To záleží jenom na Vás. A čím víc jíte, tím více šetříte.
100-300 Kč = 1%, 300-500 Kč = 3%, nad 500 = 5%

</p>
</div>
</div>


<div class="col col-md-6 col-lg-3">
<div class="txt_item">
<div class="icoProgram ico3"></div>
<h2>Jak a kdy mohu kredity uplatnit?</h2>
<p>
Při dosažení 100 kreditů ve Vaší elektronické peněžence můžete kredity rovnou zaplatit. 100 kreditu = 100 Kč sleva z objednávky. Sleva bude odečtena v posledním kroku objednávky - zaškrtněte políčko "Použít kredity".
</p>
</div>
</div>

<div class="col col-md-6 col-lg-3">
<div class="txt_item">
<div class="icoProgram ico4"></div>
<h2>Jakou mají kredity platnost?</h2>
<p>
Nemusíte se bát, že Vám Vaše kredity někdo po nějaké době vymaže. Vaše kredity u nás mají neomezenou platnost. Papírové body mají platnost do 28.2.

</p>
</div>
</div>

<div class="col col-md-6 col-lg-3">
<div class="txt_item">
<div class="icoProgram ico5"></div>
<h2>Jak zjistím kolik mám kreditů?</h2>
<p>
Při každém přihlášení na našich stránkách se Vám ukáže aktuální stav Vašich kreditu v elektronické peněžence.

</p>
</div>
</div>


<div class="col col-md-6 col-lg-3">
<div class="txt_item">
<div class="icoProgram ico6"></div>
<h2>Hrajeme fér.</h2>
<p>
Jako "Welcome Drink" Vám při registraci připíšeme 20 kreditů zcela zdarma. A protože hrajeme fér, tak našim zákazníkům, kteří u nás už u nás registraci mají, jsme body připsali automaticky.

</p>
</div>
</div>

</div>

</div>