<?php
echo '<div class="basket_done">';
	echo '<div class="ico spokojenost"></div>';
	echo '<strong class="title">'.__d('shop','Záruka 100% spokojenosti!').'</strong>';
	echo '<p>'.__d('shop','Nebyli jste spokojeni s kvalitou našich služeb?').'</p>';
	echo '<p class="small">'.__d('shop','(pizzou, jídlem, nebo chováním našich zaměstnanců)').'</p>';
	echo '<p class="bold">'.__d('shop','Napište nám a my Vaši stížnost určitě do koše nehodíme').'</p>';
	echo $this->Html->link(__d('shop', 'Chci si stěžovat'),'#',array('title'=>__d('shop', 'Chci si stěžovat'),'class'=>'btn btn-primary','id'=>'spokojenostShow'));
echo '</div>';
?>
<div id="spokojenost_form" class="none">
	<div class="load_contact_form" data-type="index"></div>
</div>