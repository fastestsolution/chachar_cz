<?php  
//pr($system_id);
$name_list = [
	1=>['name'=>'Chacharovy pizzy','name2'=>'Chacharova pizza'],
	2=>['name'=>'Maníkovy pizzy','name2'=>'Maníkova pizza'],
	3=>['name'=>'Machováčkovy pizzy','name2'=>'Mechováčkova pizza'],
	4=>['name'=>'Ogarovy pizzy','name2'=>'Ogarova pizza'],
	5=>['name'=>'Paňárovy pizzy','name2'=>'Paňárova pizza'],
	6=>['name'=>'Pepikovy pizzy','name2'=>'Pepikova pizza'],
	7=>['name'=>'Pepinovy pizzy','name2'=>'Pepinova pizza'],
	8=>['name'=>'Peťanovy pizzy','name2'=>'Peťanova pizza'],
	9=>['name'=>'Parťákovy pizzy','name2'=>'Parťákova pizza'],
	11=>['name'=>'Pizzy pro tebe','name2'=>'Pizza pro tebe'],
	13=>['name'=>'Pizzy pro tebe','name2'=>'Pizza pro tebe'],
];
//pr($name_list[$system_id]['name']
$vop = '
<div class="row">
<div class="col col-md-6 col-lg-3">
<div class="txt_item">
<div class="ico ico1"></div>
<h2>Předmět činnosti</h2>
<p>Předmětem činnosti je výroba a následný rozvoz hotových jídel a potravinářského zboží do obydlených částí měst a blízkého okolí.</p>
</div>
</div>

<div class="col col-md-6 col-lg-3">
<div class="txt_item">
<div class="ico ico2"></div>
<h2>Otevírací doba a rozvoz</h2>
<p>Jednotlivé provozovny '.$name_list[$system_id]['name'].' mají různé otevírací doby, které mohou být změněny. Telefonické objednávky se přijímají pouze v&nbsp;otevírací době a to maximálně do 15-ti minut před stanoveným časem ukončení provozu. On-line objednávky se přijímají pouze v&nbsp;otevírací době a to maximálně do 30-ti minut před stanoveným časem ukončení provozu.<br>'.$name_list[$system_id]['name2'].' si vyhrazuje právo stanovit individuální výši minimální objednávky na 119kč (nápoje a přílohy nevozíme samostatně) a právo nerozvážet do některých oblastí. '.(($system_id == 4)?'Zároveň si nárokuje jednotnou cenu za rozvoz, která činí 19,- Kč.':'').'</p>
</div>
</div>

<div class="col col-md-6 col-lg-3">
<div class="txt_item">
<div class="ico ico3"></div>
<h2>Platební podmínky</h2>
<p>Za produkty a služby je možno platit v&nbsp;hotovosti, gastrostravenkami (Ticket Restaurant, Sodexo, Cheque Dejeuner), nebo bankovním převodem v&nbsp;případě uskutečnění objednávky na www.chachar.cz, popřípadě přes smluvní partnery. '.$name_list[$system_id]['name2'].' si vyhrazuje právo do zbylé hodnoty stravenek peníze nevracet. Zákazník je povinen ihned přepočítat vrácené peníze. Na pozdější reklamace nelze brát zřetel.&nbsp;Pokud odebíráte více surovin, než přidáváte, tak výsledná celková cena za produkt nemůže být menší, než výchozí cena pizzy nebo jídla. Rozvozci (doručovatelé produktů) z bezpečnostních důvodů u sebe nemají více než 1000,- Kč v&nbsp;hotovosti. Prosíme, připravte si drobné nebo předem nahlaste, že budete platit vyšší bankovkou než je 1000,-Kč. Veškeré platby jsou prováděny v&nbsp;české měně (CZK). Všechny ceny našich produktů jsou uváděny včetně obalů a DPH.</p>
</div>
</div>

<div class="col col-md-6 col-lg-3">
<div class="txt_item">
<div class="ico ico4"></div>
<h2>Produkty</h2>
<p>Nabízené produkty nebo zboží mohou být dočasně vyprodány. V těchto případech vám bude nabídnuta alternativa. Nápoje a přílohy se rozváží pouze s&nbsp;pizzou nebo hlavním jídlem. Alkoholické nápoje a tabákové výrobky neprodáváme osobám mladším osmnácti let.</p>
</div>
</div>


<div class="col col-md-6 col-lg-3">
<div class="txt_item">
<div class="ico ico5"></div>
<h2>Akce, speciální nabídky, slevy</h2>
<p>'.$name_list[$system_id]['name2'].' si vyhrazuje právo kdykoliv odvolat, ukončit nebo zrušit jakoukoliv akci, speciální nabídku nebo slevu a to bez možnosti jakékoliv finanční nebo jiné náhrady, zejména při objednání prostřednictvím smluvních partnerů. Na jednu objednávku akceptujeme pouze 10 pizza bodů. Papírové body mají platnost do 28.2.</p>
</div>
</div>


<div class="col col-md-6 col-lg-3">
<div class="txt_item">
<div class="ico ico6"></div>
<h2>Doručení objednávek</h2>
<p>Cílem je doručení objednaných produktů a zboží v&nbsp;co nejkratší době. Doba dodání je od 30 minut do 90 minut. V&nbsp;případě, že nastanou situace, které přímo ovlivní zpoždění dodávky (dopravní komplikace, nehoda, zhoršené povětrnostní vlivy atd.), může dojít k&nbsp;opoždění objednávky. V&nbsp;tomto případě podá pracovník příjmu objednávek informace o čase dodání. Za zpoždění, či nedodání zaviněné třetí osobou (krádež, výpadek energií, přerušení dodávky vody atd.), nemůže '.$name_list[$system_id]['name2'].' nést odpovědnost, a proto toto není důvodem oprávněné reklamace. Veškeré provedené objednávky jsou závazné a kupující je povinen za objednané zboží zaplatit celkovou cenu. Hmotnosti veškerých jídel jsou uváděny v&nbsp;syrovém stavu. Objednávku doručujeme ke vchodovým dveřím. Cena dopravného je 19,- </p>
</div>
</div>

<div class="col col-md-6 col-lg-3">
<div class="txt_item">
<div class="ico ico7"></div>
<h2>Reklamace</h2>
<p>'.$name_list[$system_id]['name2'].' doporučuje na místě zkontrolovat, jestli doručené zboží souhlasí s&nbsp;objednávkou. Pokud je cokoliv v nepořádku, je zapotřebí neprodleně zavolat na pobočku, kde byla objednávka uskutečněna. Pokud nebyly dodrženy obchodní podmínky naší firmy, či kvalita jídla, bude nabídnuta výměna zboží. Nové zboží bude dodáno v co nejkratším termínu. Zboží lze reklamovat pouze v&nbsp;den, kdy bylo zakoupeno. Na reklamované objednávky z předchozích dní nelze brát zřetel. Firemní email bumerang@pizzaprotebe.cz neslouží pro podávání reklamací. Na tento e-mail můžete posílat vaše názory, nápady, nabídky spolupráce nebo žádosti o zaměstnání.</p>
</div>
</div>

<div class="col col-md-6 col-lg-3">
<div class="txt_item">
<div class="ico ico8"></div>
<h2>Ochrana osobních údajů</h2>
<p>Potvrzením a odesláním objednávky nebo sdělením při telefonické objednávce dáváte souhlas k dalšímu využití Vámi uvedených osobních údajů (jméno, příjmení, adresa, email, telefonní číslo) za účelem zásilkového prodeje, jejich uchovávání v centrální databázi společnosti Chachar catering, s.r.o. a dále v pokladním systému provozovny a to po dobu neurčitou. Zpracovatel databáze je společnost Fastest solutions, Pelclova 2500/5, 702 00, Ostrava, IČ: 28591232. Uskutečněním objednávky zároveň souhlasíte se zasíláním marketingových nabídek formou sms a emailů v rámci společnosti Chachar catering, s.r.o. a jednotlivých provozovatelů v rámci obchodní značky '.$name_list[$system_id]['name2'].' (viz kontakt). V zájmu zkvalitňování služeb může být Vás hovor zvukově zaznamenán. Máte právo kdykoliv požádat o výmaz osobních údajů prostřednictvím emailu: administrativa@chachar.cz.</p>
</div>
</div>

<div class="col col-md-6 col-lg-3">
<div class="txt_item">
<div class="ico ico9"></div>
<h2>Odstoupení od smlouvy</h2>
<p>Z hlediska nabízeného sortimentu služeb není možné od smlouvy odstoupit.</p>
</div>
</div>

<div class="col col-md-6 col-lg-3">
<div class="txt_item">
<div class="ico ico10"></div>
<h2>Ochranná známka &amp; copyright</h2>
<p>Logo '.$name_list[$system_id]['name2'].' ® je registrovanou ochrannou známkou a je ve výhradním vlastnictví společnosti Chachar catering,s.r.o. a je chráněno vlastnickým a autorským právem.&nbsp;</p>
</div>
</div>

</div>
'; 
echo '<div id="vop">';
	echo $vop;
echo '</div>';
?>