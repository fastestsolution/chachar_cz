<?php 
$kontakt[1] = '
<div class="row">
<div class="col col-md-6">
<h2>Majitel obchodní značky</h2>
<address>Chachar Catering s.r.o.<br>
Ludvíka Podéště 1974/25,<br>
708 00 Ostrava - Poruba<br>
<label>IČO: </label>28587740<br>
<label>DIČ: </label>CZ28587740</address>

<h2>Zájem o práci</h2>
<p>Pokud máte zájem o práci<br>kontaktujte <a href="/zadost-o-zamestnani/" title="Robota">naše provozovny</a>.</p>
</div>
<div class="col col-md-6">
<h2>Kontaktní údaje</h2>
<address>
	<label>Email: </label><a href="mailto:bumerang@chachar.cz">bumerang@chachar.cz</a>
</address>
</div>
</div>
<div class="clear"></div>
<div class="table-responsive">
<table class="table-striped table_provoz">
<tbody>
<tr><th>Místo</th><th>Provozovatel</th><th>Adresa</th><th>IČ</th><th>DIČ</th></tr>
<tr><td>Dubina – Šenk u Kozla</td><td>Chachar Catering,s.r.o.</td><td>Ludvíka Podéště 25, Ostrava-Poruba, 70800</td><td>28587740</td><td>CZ28587740</td></tr>
<tr><td>Poruba - sever,<br>restaurace Olympia - udírna na osmáku</td><td>Chachar Catering,s.r.o.</td><td>Ludvíka Podéště 25, Ostrava-Poruba, 70800</td><td>28587740</td><td>CZ28587740</td></tr>
<tr><td>Centrum a Slezská – Stará elektrárna</td><td>CHACHAROVA PIZZA SLEZSKÁ s.r.o.</td><td>Bámova 496/19,717 00 Ostrava</td><td>06649009</td><td>CZ06649009</td></tr>
<tr><td>Výškovice – U Jiřika</td><td>Martina Bednářová</td><td>Lechowiczova 23, Ostrava, 70200</td><td>74674013</td><td>CZ7662045545</td></tr><tr><td>Havířov – Restaurace Blesk</td><td>Martina Bednářová</td><td>Lechowiczova 23, Ostrava, 70200</td><td>74674013</td><td>CZ7662045545</td></tr>
<tr><td>Poruba jih – Sparta</td><td>Petr Kopčil</td><td>Rybova 4a, Opava, 746 01</td><td>73308200</td><td>CZ7404145419</td></tr>
<tr><td>Zábřeh – Assen</td><td>Petr Kopčil</td><td>Rybova 4a, Opava, 746 01</td><td>73308200</td><td>CZ7404145419</td></tr><tr><td>Karviná – Pizzerie u Chachachara</td><td>Lukáš Rychlý</td><td>B.Četyny 2/930, Ostrava-Bělský les,700 30</td><td>75577381</td><td>CZ9004255986</td></tr><tr><td>Frýdek – Sport Bar Excel</td><td>Milan Rychlý</td><td>Volonterská 315/10, Horní Suchá, 733 35</td><td>87077973</td><td>CZ6412071479</td></tr>
<tr><td>Třinec – U Švejka</td><td>Chachaři s.r.o.</td><td>Kytlická 780, Praha 9, 190 00</td><td>24702188</td><td>CZ24702188</td></tr>
<tr><td>Český Těšín – Restaurace u Lidlu</td><td>Rudolf Ligocký</td><td>Badatelů 1509/9, Ostrava-Poruba,708 00</td><td>64081281</td><td>CZ6709231727</td></tr>
<tr><td>Jablunkov – Zotavovna</td><td>Kateřina Di-Giustová</td><td>Milíkov 144, 739 81</td><td>04352882</td><td>CZ8660145450</td></tr>
<tr><td>Hlučín</td><td>Slezská podnikatelská s.r.o.</td><td>Suchardova 1687/1 70200 Ostrava</td><td>01814621</td><td>CZ01814621</td></tr>
<tr><td>Orlová - prostory Restaurace Orel</td><td>Lukáš Radkovič</td><td>Ivana Sekaniny 1802/11, Ostrava - Poruba, 708 00</td><td>04228138</td><td>CZ8311244612</td></tr>
<tr><td>Hrabová – Na vyhlídce</td><td>Veronika Baránková</td><td>Pospolitá 897/7 Ostrava Zábřeh 70030</td><td><p>05418607</p></td><td>CZ8556045619</td></tr>
<tr><td>Bohumín - prostory Restaurace Špica</td><td>Lukáš Radkovič</td><td>Bezručova 463 , Nový Bohumín 73581</td><td>04228138</td><td>CZ8311244612</td></tr>
</tbody>
</table>
</div>
';

$kontakt[2] = '
<div class="row">
<div class="col col-md-6">
<h2>Majitel obchodní značky</h2>
<address>Chachar Catering s.r.o.<br>
Ludvíka Podéště 1974/25,<br>
708 00 Ostrava - Poruba<br>
<label>IČO: </label>28587740<br>
<label>DIČ: </label>CZ28587740</address>

<h2>Zájem o práci</h2>
<p>Pokud máte zájem o práci<br>kontaktujte <a href="/zadost-o-zamestnani/" title="Robota">naše provozovny</a>.</p>
</div>
<div class="col col-md-6">
<h2>Kontaktní údaje</h2>
<address>
	<label>Email: </label><a href="mailto:bumerang@chachar.cz">bumerang@chachar.cz</a>
</address>
</div>
</div>
<div class="clear"></div>
<div class="table-responsive">
<table class="table-striped table_provoz">
<tbody>
<tr><th>Místo</th><th>Provozovatel</th><th>Adresa</th><th>IČ</th><th>DIČ</th></tr>
<tr><td>Brno západ</td><td>Maník Gastro s.r.o.</td><td>Pepřicova 1030/27,Ostrava 72400</td><td>03617092</td><td>CZ03617092</td></tr>
<tr><td>Brno východ</td><td>Tomáš Pivovarčík</td><td>V.Jiřikovského 136/22, Ostrava-Jih,700 30</td><td>88755754</td><td>CZ8410165533</td></tr>
<tr><td>Brno jih</td><td>Jan Raška GASTRO,s.r.o.</td><td>Staré Těchanovice 35, Vítkov 1, 749 01</td><td>29449120</td><td>CZ29449120</td></tr>
</tbody>
</table>
</div>
';

$kontakt[3] = '
<div class="row">
<div class="col col-md-6">
<h2>Majitel obchodní značky</h2>
<address>Chachar Catering s.r.o.<br>
Ludvíka Podéště 1974/25,<br>
708 00 Ostrava - Poruba<br>
<label>IČO: </label>28587740<br>
<label>DIČ: </label>CZ28587740</address>

<h2>Zájem o práci</h2>
<p>Pokud máte zájem o práci<br>kontaktujte <a href="/zadost-o-zamestnani/" title="Robota">naše provozovny</a>.</p>
</div>
<div class="col col-md-6">
<h2>Kontaktní údaje</h2>
<address>
	<label>Email: </label><a href="mailto:bumerang@chachar.cz">bumerang@chachar.cz</a>
</address>
</div>
</div>
<div class="clear"></div>
<div class="table-responsive">
<table class="table-striped table_provoz">
<tbody>
<tr><th>Místo</th><th>Provozovatel</th><th>Adresa</th><th>IČ</th><th>DIČ</th></tr>
<tr><td>Hradec Králové</td><td>DL Partner,s.r.o.</td><td>Revoluční 1082/8, 110 00 Praha 1</td><td>01726994</td><td>CZ01726994</td></tr>
</tbody>
</table>
</div>
';

$kontakt[4] = '
<div class="row">
<div class="col col-md-6">
<h2>Majitel obchodní značky</h2>
<address>Chachar Catering s.r.o.<br>
Ludvíka Podéště 1974/25,<br>
708 00 Ostrava - Poruba<br>
<label>IČO: </label>28587740<br>
<label>DIČ: </label>CZ28587740</address>

<h2>Zájem o práci</h2>
<p>Pokud máte zájem o práci<br>kontaktujte <a href="/zadost-o-zamestnani/" title="Robota">naše provozovny</a>.</p>
</div>
<div class="col col-md-6">
<h2>Kontaktní údaje</h2>
<address>
	<label>Email: </label><a href="mailto:bumerang@chachar.cz">bumerang@chachar.cz</a>
</address>
</div>
</div>
<div class="clear"></div>
<div class="table-responsive">
<table class="table-striped table_provoz">
<tbody>
<tr><th>Místo</th><th>Provozovatel</th><th>Adresa</th><th>IČ</th><th>DIČ</th></tr>
<tr><td>Zlín a Otrokovoce</td><td>Ogar catering s.r.o</td><td>Ostravice 760, 739 14 Ostravice</td><td>02412501</td><td>CZ02412501</td></tr>
<tr><td>Valašské Meziříčí</td><td>KNEBL, spol. s r.o.</td><td>Leoše Janáčka 379, 763 26 Luhačovice</td><td>25559214</td><td>CZ25559214</td></tr>
</tbody>
</table>
</div>
';

$kontakt[5] = '
<div class="row">
<div class="col col-md-6">
<h2>Majitel obchodní značky</h2>
<address>Chachar Catering s.r.o.<br>
Ludvíka Podéště 1974/25,<br>
708 00 Ostrava - Poruba<br>
<label>IČO: </label>28587740<br>
<label>DIČ: </label>CZ28587740</address>

<h2>Zájem o práci</h2>
<p>Pokud máte zájem o práci<br>kontaktujte <a href="/zadost-o-zamestnani/" title="Robota">naše provozovny</a>.</p>
</div>
<div class="col col-md-6">
<h2>Kontaktní údaje</h2>
<address>
	<label>Email: </label><a href="mailto:bumerang@chachar.cz">bumerang@chachar.cz</a>
</address>
</div>
</div>
<div class="clear"></div>
<div class="table-responsive">
<table class="table-striped table_provoz">
<tbody>
<tr><th>Místo</th><th>Provozovatel</th><th>Adresa</th><th>IČ</th><th>DIČ</th></tr>
<tr><td>Plzeň</td><td>Martin Kopčil	Sušická 18, 326 00 Plzeň</td><td>01887173</td><td>CZ7610265421</td></tr>
</tbody>
</table>
</div>
';

$kontakt[6] = '
<div class="row">
<div class="col col-md-6">
<h2>Majitel obchodní značky</h2>
<address>Chachar Catering s.r.o.<br>
Ludvíka Podéště 1974/25,<br>
708 00 Ostrava - Poruba<br>
<label>IČO: </label>28587740<br>
<label>DIČ: </label>CZ28587740</address>

<h2>Zájem o práci</h2>
<p>Pokud máte zájem o práci<br>kontaktujte <a href="/zadost-o-zamestnani/" title="Robota">naše provozovny</a>.</p>
</div>
<div class="col col-md-6">
<h2>Kontaktní údaje</h2>
<address>
	<label>Email: </label><a href="mailto:bumerang@chachar.cz">bumerang@chachar.cz</a>
</address>
</div>
</div>
<div class="clear"></div>
<div class="table-responsive">
<table class="table-striped table_provoz">
<tbody>
<tr><th>Místo</th><th>Provozovatel</th><th>Adresa</th><th>IČ</th><th>DIČ</th></tr>
<tr><td>Modřany</td><td>Radek Šiška	Krouzova3023/9, PRAHA 4</td><td>74596161</td><td>CZ8704240083</td></tr>
<tr><td>Jižní město</td><td>Radek Šiška</td><td>Krouzova3023/9, PRAHA 4</td><td>74596161</td><td>CZ8704240083</td></tr>
<tr><td>Prosek</td><td>Stanislav Schönwälder</td><td>Tř. Obránců míru 1580/1, BRUNTÁL</td><td>6052916</td><td>CZ6052916</td></tr>
</tbody>
</table>
</div>
';

$kontakt[7] = '
<div class="row">
<div class="col col-md-6">
<h2>Majitel obchodní značky</h2>
<address>Chachar Catering s.r.o.<br>
Ludvíka Podéště 1974/25,<br>
708 00 Ostrava - Poruba<br>
<label>IČO: </label>28587740<br>
<label>DIČ: </label>CZ28587740</address>

<h2>Zájem o práci</h2>
<p>Pokud máte zájem o práci<br>kontaktujte <a href="/zadost-o-zamestnani/" title="Robota">naše provozovny</a>.</p>
</div>
<div class="col col-md-6">
<h2>Kontaktní údaje</h2>
<address>
	<label>Email: </label><a href="mailto:bumerang@chachar.cz">bumerang@chachar.cz</a>
</address>
</div>
</div>
<div class="clear"></div>
<div class="table-responsive">
<table class="table-striped table_provoz">
<tbody>
<tr><th>Místo</th><th>Provozovatel</th><th>Adresa</th><th>IČ</th><th>DIČ</th></tr>
<tr><td>Olomouc – Na tabulovém vrchu</td><td>Milan Rychlý</td><td>B.Četyny 2/930, Ostrava-Bělský les,700 30</td><td>87379376</td><td>CZ8707175620</td></tr>
<tr><td>Prostějov</td><td>PEPIN PRO s.r.o.</td><td>Dolní 37, Prostějov</td><td>05747465</td><td>CZ05747465</td></tr>
<tr><td>Přerov – Pizzerie Pepin</td><td>ONIPEP, s.r.o.</td><td>Hlavní 210, Štítina, 747 91</td><td>28655541</td><td>CZ28655541</td></tr>
<tr><td>Kroměříž – U Nemocnice</td><td>Kantor Robert</td><td>Stražisko 60, Stražisko, 798 44</td><td>47983469</td><td>CZ6602162083</td></tr>
</tbody>
</table>
</div>
';

$kontakt[8] = '
<div class="row">
<div class="col col-md-6">
<h2>Majitel obchodní značky</h2>
<address>Chachar Catering s.r.o.<br>
Ludvíka Podéště 1974/25,<br>
708 00 Ostrava - Poruba<br>
<label>IČO: </label>28587740<br>
<label>DIČ: </label>CZ28587740</address>

<h2>Zájem o práci</h2>
<p>Pokud máte zájem o práci<br>kontaktujte <a href="/zadost-o-zamestnani/" title="Robota">naše provozovny</a>.</p>
</div>
<div class="col col-md-6">
<h2>Kontaktní údaje</h2>
<address>
	<label>Email: </label><a href="mailto:bumerang@chachar.cz">bumerang@chachar.cz</a>
</address>
</div>
</div>
<div class="clear"></div>
<div class="table-responsive">
<table class="table-striped table_provoz">
<tbody>
<tr><th>Místo</th><th>Provozovatel</th><th>Adresa</th><th>IČ</th><th>DIČ</th></tr>
<tr><td>Jihlava</td><td>Peťanovo s.r.o.,</td><td>Lipina 6, 58827 Jamné</td><td>06618642</td><td>CZ06618642</td></tr>
</tbody>
</table>
</div>
';

$kontakt[9] = '
<div class="row">
<div class="col col-md-6">
<h2>Majitel obchodní značky</h2>
<address>Chachar Catering s.r.o.<br>
Ludvíka Podéště 1974/25,<br>
708 00 Ostrava - Poruba<br>
<label>IČO: </label>28587740<br>
<label>DIČ: </label>CZ28587740</address>

<h2>Zájem o práci</h2>
<p>Pokud máte zájem o práci<br>kontaktujte <a href="/zadost-o-zamestnani/" title="Robota">naše provozovny</a>.</p>
</div>
<div class="col col-md-6">
<h2>Kontaktní údaje</h2>
<address>
	<label>Email: </label><a href="mailto:bumerang@chachar.cz">bumerang@chachar.cz</a>
</address>
</div>
</div>
<div class="clear"></div>
<div class="table-responsive">
<table class="table-striped table_provoz">
<tbody>
<tr><th>Místo</th><th>Provozovatel</th><th>Adresa</th><th>IČ</th><th>DIČ</th></tr>
<tr><td>Liberec</td><td>DL Partner,s.r.o.</td><td>Revoluční 1082/8, 110 00 Praha 1</td><td>01726994</td><td>CZ01726994</td></tr>
<tr><td>Litoměřice</td><td>LTGastro, s.r.o.</td><td>Nám. Sokolské 306/7, Liberec</td><td>28708512</td><td>CZ28708512</td></tr>
<tr><td>Mladá Boleslav</td><td>Jiří Kožuch</td><td>Tř. Obránců míru 1580/1, BRUNTÁL</td><td>04893182</td><td>CZ8912255297</td></tr>
</tbody>
</table>
</div>
';


$kontakt[10] = '
<div class="row">
<div class="col col-md-6">
<h2>Majitel obchodní značky</h2>
<address>Chachar Catering s.r.o.<br>
Ludvíka Podéště 1974/25,<br>
708 00 Ostrava - Poruba<br>
<label>IČO: </label>28587740<br>
<label>DIČ: </label>CZ28587740</address>

<h2>Zájem o práci</h2>
<p>Pokud máte zájem o práci<br>kontaktujte <a href="/zadost-o-zamestnani/" title="Robota">naše provozovny</a>.</p>
</div>
<div class="col col-md-6">
<h2>Kontaktní údaje</h2>
<address>
	<label>Email: </label><a href="mailto:bumerang@chachar.cz">bumerang@chachar.cz</a>
</address>
</div>
</div>
<div class="clear"></div>
<div class="table-responsive">
<table class="table-striped table_provoz">
<tbody>
<tr><th>Místo</th><th>Provozovatel</th><th>Adresa</th><th>IČ</th><th>DIČ</th></tr>
<tr><td>Mladá Boleslav</td><td>Jiří Kožuch</td><td>Tř. Obránců míru 1580/1, BRUNTÁL</td><td>04893182</td><td>CZ8912255297</td></tr>
</tbody>
</table>
</div>
';

$kontakt[11] = '
<div class="row">
<div class="col col-md-6">
<h2>Majitel obchodní značky</h2>
<address>Chachar Catering s.r.o.<br>
Ludvíka Podéště 1974/25,<br>
708 00 Ostrava - Poruba<br>
<label>IČO: </label>06530524<br>
<label>DIČ: </label>CZ06530524</address>

<h2>Zájem o práci</h2>
<p>Pokud máte zájem o práci<br>kontaktujte <a href="/zadost-o-zamestnani/" title="Robota">naše provozovny</a>.</p>
</div>
<div class="col col-md-6">
<h2>Kontaktní údaje</h2>
<address>
	<label>Email: </label><a href="mailto:bumerang@chachar.cz">bumerang@chachar.cz</a>
</address>
</div>
</div>
<div class="clear"></div>
<div class="table-responsive">
<table class="table-striped table_provoz">
<tbody>
<tr><th>Místo</th><th>Provozovatel</th><th>Adresa</th><th>IČ</th><th>DIČ</th></tr>
<tr><td>Krnov</td><td>První Villa Cafe s.r.o.</td><td>Zacpalova 1, Krnov</td><td>06530524</td><td>CZ06530524 </td></tr>
</tbody>
</table>
</div>
';
echo $kontakt[$system_id];
?>