<!DOCTYPE html>
<html>
<head>
  <?= $this->Html->charset() ?>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>
    <?= $title ?>
  </title>
  <?php /* ?>
  <script type="text/javascript">less = { env: 'development' };</script>
  <?= $this->Html->css("ajax.css", ["rel" => "stylesheet/less"]); ?>
  <?php */ ?>
  
  <link rel="stylesheet" type="text/css" href="/css/print.css"  media="all" />
  
 
  <?php
		if (isset($scripts)){if (is_array($scripts)){foreach ($scripts as $link){echo $this->Html->script($link);}} else {echo $this->Html->script($scripts);}}
		if (isset($styles)){if (is_array($styles)){foreach ($styles as $style){echo $this->Html->css($style);} } else {echo $this->Html->css($styles);}} 
	?>
</head>
<body class="ajax <?= strtolower($this->request->controller."-".$this->request->action) ?>">
	<?= $this->fetch('content') ?>
</body>
<script type="text/javascript">
//<![CDATA[
   
var onPrintFinished=function(printed){
	(function(){
		window.close();
	}).delay(2000);
	
	
}
//window.print(true);
//]]>
</script>
</html>
