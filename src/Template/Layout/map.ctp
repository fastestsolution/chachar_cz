<!DOCTYPE html>
<html lang="cs">
<head>
    <?= $this->element('layout/html_head'); ?>
    <?= $this->element('layout/js_css');  ?>
	<?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
	<?php
	
		if (isset($scripts)){if (is_array($scripts)){foreach ($scripts as $link){echo $this->Html->script($link);}} else {echo $this->Html->script($scripts);}}
		if (isset($styles)){if (is_array($styles)){foreach ($styles as $style){echo $this->Html->css($style);} } else {echo $this->Html->css($styles);}} 
	?>
	<?php if (!isset($seo) && isset($title)) $seo = ['title'=>$title]; ?>
	<title><?php echo (isset($breadcrumb)?$this->Fastest->pageTitle($breadcrumb).' | '.$setting['page_title']:$this->Fastest->seoText((isset($seo)?$seo:''),$setting,'title')) ?></title>
	
</head>
<body id="body">
    <?= $this->Flash->render() ?>
	<div class="container">
	<?php //pr($jsUri); ?>
		<div class="container_in">
			<div class="row">
				<div class="col-md-12">
					<div id="reload_mobile" class="">
						<?php echo $this->fetch('content'); ?>
					</div>
				</div>
				
			</div>
		</div>	
    </div>
	<div id="addon"></div>
</body>
</html>
