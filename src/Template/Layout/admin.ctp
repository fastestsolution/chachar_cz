<!DOCTYPE html>
<html>
<head>
    <?= $this->element('layout/html_head'); ?>
    <?= $this->element('layout/js_css');  ?>
	<?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
	<?php
	
		if (isset($scripts)){if (is_array($scripts)){foreach ($scripts as $link){echo $this->Html->script($link);}} else {echo $this->Html->script($scripts);}}
		if (isset($styles)){if (is_array($styles)){foreach ($styles as $style){echo $this->Html->css($style);} } else {echo $this->Html->css($styles);}} 
	?>
	<?php if (!isset($seo) && isset($title)) $seo = ['title'=>$title]; ?>
	<title><?php echo (isset($breadcrumb)?$this->Fastest->pageTitle($breadcrumb).' | '.$setting['page_title']:$this->Fastest->seoText((isset($seo)?$seo:''),$setting,'title')) ?></title>
	
</head>
<body id="body">
    <?= $this->Flash->render() ?>
	<div class="container">
		<div class="container_in">
			<div class="row">
				<?php 
				
				if (!isset($hideMenu)){ ?>
				<ul id="admin_menu">
					<li><?php echo $this->Html->link('Produkty','/admin/products/',['title'=>'Produkty']) ?></li>
					<li><?php echo $this->Html->link('Kategorie','/admin/categories/',['title'=>'Kategorie']) ?></li>
					<li><?php echo $this->Html->link('Přídavky','/admin/attributes/',['title'=>'Přídavky']) ?></li>
				</ul>
				<?php } ?>
				<div class="col-md-12 col-sm-12 bd-content pull-right">
					<h1 id="h1"><?php echo (isset($title)?$title:'');?></h1>
					<div id="contents">
						<?php echo $this->fetch('content'); ?>
						<?php //pr($menu_items); ?>
					</div>
				</div>
				
			</div>
		</div>	
    </div>
	
	
	<div id="addon"></div>
	<div id="lang_pref" class="none"><?php echo $url_lang_prefix;?></div>
    
</body>
</html>
