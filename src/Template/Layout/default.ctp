<?php  

header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");
?>
<!DOCTYPE html>
<html lang="cs">
<head>
    <?= $this->element('layout/html_head'); ?>
    <?= $this->element('layout/js_css');  ?>
	<?= $this->fetch('meta') ?>
    <?= $this->fetch('css') ?>
    <?= $this->fetch('script') ?>
	<?php
	
		if (isset($scripts)){if (is_array($scripts)){foreach ($scripts as $link){echo $this->Html->script($link);}} else {echo $this->Html->script($scripts);}}
		if (isset($styles)){if (is_array($styles)){foreach ($styles as $style){echo $this->Html->css($style);} } else {echo $this->Html->css($styles);}} 
	?>
	<?php if (!isset($seo) && isset($title)) $seo = ['title'=>$title]; ?>
	<?php /* ?>
	<link href="https://fonts.googleapis.com/css?family=Catamaran:300,400,500&amp;subset=latin-ext" rel="stylesheet"> 
	<?php */ ?>
	<title><?php echo (isset($breadcrumb)?$this->Fastest->pageTitle($breadcrumb).' | '.$setting['page_title']:$this->Fastest->seoText((isset($seo)?$seo:''),$setting,'title')) ?></title>
	<script type="text/javascript" src="https://api.mapy.cz/loader.js"></script>
    <script type="text/javascript">Loader.load();</script>
</head>
<body id="body">
	<?php 
	if (isset($dev_project)){
		echo '<div id="alert_dev">Vývojova verze, objednávky budou uloženy do testovací pokladny</div>';
	}	
	?>
	<?= $this->Flash->render() ?>
	<div class="container">
	<?php //pr($seo); ?>
		
					<?php if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'  || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') == true){?> 
					<?php } else { ?>		
					<?php } ?>
		<div class="container_in">
			<div id="menu_adopt"></div>
			<div id="header_adopt"></div>
			<div class="row">
				<div class="<?php echo (isset($layout_class)?$layout_class:'col-md-9 col-lg-10 col-sm-9');?> bd-content pull-right">
					<h1 id="h1" class="el_adopt" data-el="h1_adopt"><?php echo (isset($title)?$title:'');?></h1>
					<div id="contents">
						<?php echo $this->fetch('content'); ?>
						<?php //pr($menu_items); ?>
					</div>
				</div>
				<?php if (!isset($no_side)){ ?>
				<div class="col-md-3 col-lg-2 col-sm-3 bd-sidebar ">
					<div id="provoz_side" class="<?php echo $css_styles['logo'];?>">
						<?php echo $cell = $this->cell('Shop.Provozs',[], ['cache' => false]);?>
					</div>
					<?php 
					//if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
						//echo '<div id="basket_load"></div>
					//} else {
						echo $this->element('Shop.Basket/layout_basket'); 
					//}
					?>
					<?php //echo $this->element('Shop.Search/search_element'); ?>
					<?php //echo $this->element('Newsletter/registration'); ?>
				</div>
				<?php } ?>
				
			</div>
		</div>	
		
				
    </div>
	<?php //if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65a' || $_SERVER['REMOTE_ADDR'] == '89.239.31.226'){?>
	<?php 
	if (isset($system_id_data['rozcestnik'])){ 
		$link = explode('.',$_SERVER['SERVER_NAME']);
		$link = 'http://www.'.$link[1].'.'.$link[2];
	
		echo $this->Html->link('','#',['title'=>__('Přejít na rozcestník'),'id'=>'rozcestnik_link','class'=>'fa fa-home','data-link'=>$link,'data-id'=>$system_id]);
		//pr($_SERVER);
	}
	
	?>
	
	<?php //}	?> 
			
	<?php echo $this->element('layout/menu'); ?>
	<?php //echo $this->element('layout/header'); ?>
	<?php echo $this->element('layout/footer',[],[]); ?>
	
	<?php
		//if ($_SERVER['REMOTE_ADDR'] != '89.103.18.65'){
		//pr($addr);die('a');
		
		if (isset($this->request->query['addr'])){
			unset($new_address);
		}
		if (!isset($new_address)){
		if (isset($this->request->query['addr']) || isset($addr)){
		
			echo '<div id="search_address_load"></div>';
			echo $this->element('layout/search_adresa'); 
		}
		} else {
			echo '<div id="new_address"></div>';
		}
		//}
	?>
	
	<script>
		/*window.addEvent('load', function() {
			$('provoz1').fireEvent('click', $('provoz1'));	
		});*/
	</script>
	
	
	
	
	<?php echo $this->element('layout/disable_provoz'); ?>
	<?php echo $this->element('Shop.../ShopOrders/elements/basket_login'); ?>
	<?php 
	if (isset($this->request->query['show_map']) && !empty($this->request->query['show_map'])){
		echo $this->element('Shop.../ShopOrders/elements/map_orders',['map_id'=>$map_id]); 
	}
	?>
	
	<?php if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){?>
		<div id="IP"></div>
	<?php } ?>
	
	<div id="addon"></div>
	<div id="page_preloader" class="load">
		<div class="progress"><div class="over"></div></div>
		<div class="side left"></div>
		<div class="side right"></div>
		<div class="panak"></div>
	</div>
	<?php 
	
		$opt = [
			'product_attributes_group_list'=>$product_attributes_group_list,
			'system_id'=>$system_id,
		];
		echo $cell = $this->cell('Shop.ProductAttributes',[$opt]);
	?>
	<div id="lang_pref" class="none"><?php echo $url_lang_prefix;?></div>
	<?php /* ?>
	<div class="tips filtr none" id="tip_filtr">
		<div class="bull"></div>
		<strong><?php echo __('Náš TIP!'); ?></strong>
		<p><?php echo __('Jednoduše vyhledejte jídlo...')?></p>
		<div class="tria"></div>
	</div>
	<div class="tips filtr none" id="tip_basket">
		<div class="bull"></div>
		<strong><?php echo __('Náš TIP!'); ?></strong>
		<p><?php echo __('Po vložení do košíku dokončete objednávku...')?></p>
		<div class="tria"></div>
	</div>
	<div class="tips filtr none" id="tip_provoz">
		<div class="bull"></div>
		<p><?php echo __('Vyberte svoji provozovnu...')?></p>
		<div class="tria"></div>
	</div>
	<?php */ ?>
	<?php echo '<div id="map_coords" class="none"  data-json=\''.(isset($map_coords_search)?$map_coords_search:'').'\'></div>';?>	
	<?php echo $this->element('layout/ga'); ?>
    <?php if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
    echo '<div id="debug_js"></div>';
    } ?>
	<?php //if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){?>
		<?php echo $this->element('layout/rozcestnik'); ?>
	<?php //} ?>
	
	<?php if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65a'  || stripos($_SERVER['HTTP_USER_AGENT'], 'Speed Insights') == true){?> 
	<?php /**/ ?>
	<noscript id="deferred-styles">
				<link rel="stylesheet" type="text/css" href="<?php echo $cssUri;?>"  media="screen" id="load_css"/>
				<?php /* ?>
				<link rel="stylesheet" type="text/css" href="/css/css_compile/small.css"/>
				<script async type="text/javascript" src="/js/loader.js"></script> 	
				<?php */ ?>
			</noscript>
	<script async type="text/javascript" src="/js/loader.js"></script>	


	<?php } ?>
	
	<?php //if(!isset($_COOKIE['confirm']) || $_COOKIE['confirm'] != true) { ?>
		 <?php echo $this->element('layout/intro'); ?>
	<?php //} ?>
	
	<?php /* ?>

	<div id="test">
		<div id='foreground'>
		Content for the foreground x:<span id="gx">0</span> y:<span id="gy">0</span> z:<span id="gz">0</span> p<span id="gp">0</span>
		<div id="test_move"></div>
	  </div>
	</div>
	<script type="text/delayscript">
	//<![CDATA[
	  fst_gyro();
	//]]>
	</script>
	<?php /**/ ?>
	
	<?php 
	
		//if ($_SERVER['REMOTE_ADDR'] == '46.135.104.174'){
			$new = true;
			echo '<div id="newAdr"></div>';
		//}
	//echo '<div id="jsonProvoz" class="none">'.json_encode($provoz_list).'</div>';
	?>
</body>
</html>
