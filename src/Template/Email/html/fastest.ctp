<style>
body {
	background:#fff;
	font-size:15px;
}
#obal {
	background:#ffffff;
	width:80%;
	margin:auto;
	display:block;
	position:relative;
	padding:10px;
	text-align:center;
}
.text_right {
	text-align:right;
}
table {
	width:100%;
}
h2 {
	font-size:30px;
}
table tr th {font-size:15px;padding:5px;text-align:left;border-bottom:1px solid #ccc;}
table tr td {font-size:15px;padding:5px;text-align:left;border-bottom:1px solid #efefef;}
#confirm_links {margin:0px;padding:0;}
#confirm_links li {list-style-type:none;margin:15px;}
#confirm_links li a{background:#ED1C24;font-size:20px;color:#fff;margin:10px;padding:5px;}
</style>
<div id="obal">
<img src="http://www.manikova-pizza.cz/css/logo/<?php echo $css_styles['logo'];?>.png" alt="Logo" />
<?= $data ?>
</div>