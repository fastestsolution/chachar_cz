<h2>Děkujeme za registraci</h2>
<p>Tímto Vám děkujeme za registraci účtu <strong>{first_name} {last_name}</strong> na {server}. </p>

<h2>Rekapitulace Vaší registrace</h2>
<table>
<tr><th>Jméno:</th><td>{first_name}</td></tr>
<tr><th>Příjmení:</th><td>{last_name}</td></tr>
<tr><th>Společnost:</th><td>{company_name}</td></tr>
<tr><th>IČ:</th><td>{ico}</td></tr>
<tr><th>DIČ:</th><td>{dic}</td></tr>
<tr><th>Email:</th><td>{email}</td></tr>
<tr><th>Telefon:</th><td>{phone}</td></tr>
</table>

{podpis}