<p>Děkujeme za Vaši objednávku</p>
<h2>... pár minut a jsme u Vás!</h2>
<p>aktuální doba doručení je {cas}</p>
<img src="http://p-chachar.fastest.cz/css/email/email_time.png" alt="Objednavka" />
<h2>Doručovací adresa</h2>

{user.first_name} {user.last_name}<br />
{address1.street} {address1.cp}<br />
{address1.city}<br />

<h2>Rekapitulace objednávky</h2>
<table>
	<tr>
		<th>Název</th>
		<th>ks</th>
		<th class="text_right">Cena</th>
		<th class="text_right">Cena celkem</th>
	</tr>
		
	{LOOP:shop_order_items}
	<tr>
		<td>{shop_order_items.name}</td>
		<td>{shop_order_items.count}</td>
		<td class="text_right">{shop_order_items.price_vat_per_item}</td>
		<td class="text_right">{shop_order_items.price_vat}</td>
	</tr>
	{ENDLOOP:shop_order_items}
	
	
	<tr>
		<th>Platba</th>
		<th colspan="3"></th>
	</tr>
	<tr>
		<td>{shop_payment.name}</td>
		<td></td>
		<td class="text_right">{price_payment}</td>
		<td class="text_right">{price_vat_payment}</td>
	</tr>
	<tr>
		<td colspan="3"><h2>Celkem:</h2></td>
		<td class="text_right"><h2>{price_vat}</h2></td>
	</tr>
</table>
<p>Kdyby cokoliv, volejte provozovnu {provoz_name}</p>
<img src="http://p-chachar.fastest.cz/css/email/email_mobile.png" alt="Provoz" />
<h2>{provoz_telefon}</h2>

{confirm_link}

