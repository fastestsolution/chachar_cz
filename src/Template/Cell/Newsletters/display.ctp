<?php
echo $this->Form->create($newsletter);
?>
<div class="row">
	<?php echo $this->Form->input('email', ['label'=>false,'placeholder' => __('Váš email'),'class'=>'form-control']); ?>
</div>
<?php
echo '<div class="row buttons">';
	echo '<div class="col-md-12">';
		echo $this->Form->button(__('Zaregistrovat email'), ['class' => 'btn btn-lg btn-primary btn-block SaveForm']);
	echo '</div>';
echo '</div>';
echo $this->Form->end();
?>