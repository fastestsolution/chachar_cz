<?php
echo $this->Form->create($users);
?>
<div class="row">
<fieldset>
	<legend><?php echo __("Zadejte Váš email pro zaslání nového hesla")?></legend>
		<?php echo $this->Form->input('email', ['label'=>false,'placeholder' => __('Váš email'),'class'=>'form-control']); ?>
	
</fieldset>
</div>
<?php
echo '<div class="row buttons">';
	echo '<div class="col-md-4 col-md-offset-4">';
		echo $this->Form->button(__('Zaslat nové heslo'), ['class' => 'btn btn-lg btn-primary btn-block SaveForm']);
	echo '</div>';
echo '</div>';
echo $this->Form->end();
?>