<div class="row">
	<div class="col-sm-12 alert <?php echo $alert_class;?>">
	<?php echo $alert_text;?>
	</div>
</div>
<?php
if (isset($users)){
	echo $this->Form->create($users);
	echo '<div class="row">';
	echo '<fieldset>';
		echo '<legend>'.__("Zadejte nové heslo").'</legend>';
			echo $this->Form->input('password', ['label'=>false,'placeholder' => __('Nové heslo'),'class'=>'form-control reset_password','type'=>'password']);
			echo $this->Form->input('password2', ['label'=>false,'placeholder' => __('Heslo znovu'),'class'=>'form-control reset_password2','type'=>'password']);
		
	echo '</fieldset>';
	
	echo '<div class="row buttons">';
		echo '<div class="col-md-4 col-md-offset-4">';
			echo $this->Form->button(__('Zadat nové heslo'), ['class' => 'btn btn-lg btn-primary btn-block SaveForm']);
		echo '</div>';
	echo '</div>';
	echo $this->Form->end();
	echo '</div>';
}
?>