<div class="reg_field size">
	<div class="adopt_h1 none"></div>
	<h2 class="black"><?php echo __('Hotovo, zaregistrováno')?></h2>
	<h2 class="green big"><?php echo __('Gratulujeme')?></h2>
	<div class="row buttons field_button center">
		<?php echo $this->Form->input(__('Mám hlad a chci už jíst'), ['label'=>false,'type' => 'button', 'class'=>'btn btn-default redirect','data-url'=>'/pizza/']);	?>
	</div>
</div>