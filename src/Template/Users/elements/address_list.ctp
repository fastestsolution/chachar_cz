<?php if (isset($address_basket)){ ?>
<?php } else { ?>
<br />
<strong><?php echo __('Vaše adresy pro doručení'); ?></strong>
<?php } ?>

<ul id="address_list">
	
	<?php 
	if (isset($this->request->data['user']['user_addresses'])){
		$users = (object)[];
		$users->user_addresses = $this->request->data['user']['user_addresses'];
	}
	if (isset($users->user_addresses) && count($users->user_addresses)>0){
		foreach($users->user_addresses AS $adr){
			echo "<li class='clearfix ".(isset($address_basket)?'select_address':'')."' data-address='".json_encode($adr)."' ".(isset($address_basket)?"title='".__('Vybrat tuto adresu')."'":'').">";
				$address_title = $adr->street.' '.$adr->cp.', '.$adr->city;
				echo '<div class="row">';
				echo '<div class="col col-md-8">';
				
				echo $address_title;
				echo '</div>';
				echo '<div class="col col-md-4">';
				
				echo $this->Html->link(__('Smazat'),'/users/delete_address/'.$adr->id,['title'=>__('Smazat'),'data-confirm'=>__('Opravdu smazat adresu '.$address_title.'?'),'class'=>'btn btn-sm btn-primary pull-right delete_address']);
				if (isset($address_basket)){
				echo $this->Html->link(__('Použít'),'#',['title'=>__('Použít'),'class'=>'btn pointerNone btn-sm btn-default pull-right']);
				}
				//if (!isset($address_basket)){
					//echo $this->Html->link(__('Editovat'),'#',['title'=>__('Editovat'),'class'=>'btn btn-sm btn-default pull-right edit_address']);
				//}
				echo '</div>';
				echo '</div>';
			echo '</li>';
		}
	} else {
		echo '<li>'.__('Nenalezeny žádné adresy pro doručení').'</li>';
	}	
	?>
</ul>
<?php //pr($users); ?>