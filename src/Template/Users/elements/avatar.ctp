<?php if (!isset($pref)) $pref = ''; ?>
<div class="reg_field size">
	<h2 class="address_title add"><?php echo __('Vyberte si svou ikonu');?></h2>
	<div class="adopt_h1 none"><?php echo __('Vyberte si svou ikonu');?></div>
	<p class="desc"><?php echo __('Vyberte obličej, který Vás nejlépe vystihuje');?></p>
	<div id="avatar_list">
		<?php for ($i = 1; $i <= 9; $i++) { ?>
			<div class="avatar avatar<?php echo $i?>" data-id="<?php echo $i?>"><div class="avatar_select fa fa-check"></div></div>
		<?php } ?>
		<?php echo $this->Form->input($pref.'avatar_id', ['label'=>false,'class'=>'avatar_id none','type'=>'text']); ?>
	</div>
	<div class="row buttons field_button">
		<?php echo $this->Form->input(__('Zpět'), ['label'=>false,'type' => 'button', 'class'=>'prev_field btn btn-default btn-grey','data-id'=>1]);	?>
		<?php if (isset($loggedUser)){ ?>
			<?php echo $this->Form->input(__('Upravit údaje'), ['label'=>false,'type' => 'button', 'class'=>'btn btn-default SaveForm']);	?>
		<?php } else { ?>
			<?php echo $this->Form->input(__('Dokončit registraci'), ['label'=>false,'type' => 'button', 'class'=>'next_field btn btn-default','data-id'=>3]);	?>
		<?php } ?>
	</div>
</div>