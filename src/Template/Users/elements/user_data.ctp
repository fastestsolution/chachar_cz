<div class="reg_field size">
<div class="adopt_h1 none"><?php echo $title;?></div>
<?php
if (!isset($col_class_client)){
	$col_class_client = 'col-xs-12';
}
//pr($fastest);
?>
<?php if (!isset($pref)) $pref = ''; ?>
	<div class="row">
		<div class="col <?php echo $col_class_client;?>">
			<?php echo $this->Form->input($pref.'first_name', ['label'=>false,'placeholder' => __('Jméno'),'class'=>'form-control placeholder']); ?>
			<?php echo $this->Form->input($pref.'last_name', ['label'=>false,'placeholder' => __('Příjmení'),'class'=>'form-control placeholder']); ?>
		</div>
		<div class="col <?php echo $col_class_client;?>">		
			<?php 
			if (isset($this->request->data['user']['phone'])){			
				$users = (object)[];
				$users->phone = $this->request->data['user']['phone'];
				
			}
			?>
			<div class="row nopadding">
			<div class="col col-xs-4 col-md-3">
			<?php echo $this->Form->input($pref.'phone_pref', ['label'=>false,'placeholder' => __('+420'),'options'=>$phone_pref,'type'=>'select','class'=>'form-control bfh-phone2 placeholder phone_pref','maxlength'=>4,'data-number'=>(isset($users->phone_pref)?$users->phone_pref:'+420')]); ?>
			<?php //pr($users->phone_pref); ?>
			</div>
			<div class="col col-xs-8 col-md-9">
			<?php echo $this->Form->input($pref.'phone', ['label'=>false,'placeholder' => __('Telefon'),'class'=>'form-control bfh-phone2 placeholder','maxlength'=>9,'data-format'=>'(+4dd) ddd ddd ddd','data-number'=>(isset($users->phone)?$users->phone:'20')]); ?>
			</div> 
			</div>
			<?php 
			if (isset($loggedUser)){
				echo '<div class="text_input form-control placeholder" placeholder="Email">'.$loggedUser['email'].'</div>';
			} else {
				echo $this->Form->input($pref.'email', ['label'=>false,'placeholder' => __('Email'),'class'=>'form-control placeholder check_email']); 
			}
			?>
		</div>
	</div>
	<div class="row">
		<?php //if (!isset($loggedUser) //&& isset($noPassword) && !isset($noreg) || isset($userEdit)){ ?>
		<?php if (!isset($loggedUser)  && !isset($noreg) || isset($userEdit)){ ?>
		<div class="col <?php echo $col_class_client;?>">
			<?php echo $this->Form->input($pref."password", ['label'=>false,'type' => 'password', 'placeholder' => __('Určete si své heslo'),'class'=>'reset_password form-control placeholder']);?>
			<?php 
			if (isset($loggedUser)){ 
				echo $this->Form->hidden($pref."password_tmp", ['type' => 'password', 'label' => false,'value'=>$users->password]);
			
			}
			?>
		</div>
		<div class="col <?php echo $col_class_client;?>">
			<?php echo $this->Form->input($pref."password2", ['label'=>false,'type' => 'password', 'placeholder' => __('Napište své heslo znovu'),'class'=>'reset_password2 form-control placeholder']);	?>
		</div>
		<?php } else { ?>
			<?php echo $this->Form->input($pref."noreg", ['label'=>false,'type' => 'hidden', 'value'=>1]);?>
		
		<?php } ?>
		<div class="clear"></div>
		<div class="row buttons field_button">
				<?php //echo $this->Form->input(__('Zpět'), ['label'=>false,'type' => 'button', 'class'=>'prev_field btn btn-default btn-grey','data-id'=>0]);	?>
				<?php echo $this->Form->input(__('Pokračovat'), ['label'=>false,'type' => 'button', 'class'=>'next_field btn btn-default','data-id'=>1]);	?>
		</div>
	</div>
</div>