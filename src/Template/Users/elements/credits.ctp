<div class="reg_field size">
	<div class="adopt_h1 none"></div>
	<h2 class="black"><?php echo __('Perfektní! Máte připsáno')?></h2>
	<h2 class="green big"><?php echo __('20 kreditů!')?></h2>
	<p class="desc2">
		<?php echo $this->Html->link(__('Jak mohu využít své kredity?'),'#',['title'=>__('Jak mohu využít své kredity?'),'class'=>'show_element','data-el'=>'kredity_desc']); ?>
	</p>
	<div id="kredity_desc" class="none">
		<div class="line"></div>
		<h2 class="black"><?php echo __('Jak můžu využít své kredity?');?></h2>
		<p class="desc2"><?php echo __('20 kreditů = 20Kč');?></p>
		<p class="desc2"><?php echo __('Aktuální stav Vašich kreditů naleznete v elektronické peněžence u Vašeho účtu!');?></p>
		<p class="desc2"><?php echo __('Žádné časové omezení');?></p>
		<div class="line"></div>
	</div>
	<div class="row buttons field_button center">
		<?php echo $this->Form->input(__('Pokračovat'), ['label'=>false,'type' => 'button', 'class'=>'next_field only_next btn btn-default','data-id'=>4]);	?>
	</div>
</div>