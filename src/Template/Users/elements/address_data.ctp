<?php 
if ($address_type == 1){
	$title = __("Editace adresy pro doručení objednávky");
	$title2 = __("Adresa pro doručení objednávky ( ulice č.p., město)");
	$pref = 'user_addresses.0.';
	$google_id = 0;
}
if ($address_type == 2){
	$title = __("Fakturační adresa");
	$pref = 'user_addresses.1.';
	$google_id = 1;
}
if (!isset($address_type)) $address_type = 1; 
if (!isset($pref)) $pref = 'user_addresses.0.';
if (isset($pref_order)) $pref = $pref_order.$pref;
?>

<?php 
if ($address_type == 2){
	echo $this->Form->input('check_dorucovaci', ['label' => __('Fakturační adresa je jiná než doručovací'),'class'=>'checkbox','type'=>'checkbox']);
}
if (!isset($col_class)){
	$col_class = 'col-xs-12';
	$col_class2 = 'col-xs-12';
	$col_class3 = 'col-xs-12';
}
//if ($this->request->params['query']
?>
<div class="" id="address_type<?php echo $address_type;?>">
	
	<div class="adopt_h1 none"><?php echo $title2;?></div>
	<?php //pr($users['user_addresses'])); ?>
	<?php echo $this->Form->input($pref.'id', ['type'=>'hidden','class'=>'clear_address','data-type'=>'id']); ?>
	<?php echo $this->Form->input($pref.'shop_address_type_id', ['value' => $address_type,'type'=>'hidden','data-type'=>'shop_address_type_id']); ?>
	
	<?php if (!isset($address_basket)){ ?>
	<div class="row">
		<div class="col col-sm-12">
			<?php echo $this->Html->link(__('Nová adresa'),'#',['title'=>__('Nová adresa'),'class'=>'btn btm-sm btn-primary','id'=>'new_address']);?>
			<br />
			<br />
		</div>
	</div>
	<?php } ?>

	
	<div class="row">
		<?php if (isset($new)){ ?>
			<div class="col col-md-12">
				<div id="adr_preloader2" class="addressPreloader none"></div>
				<?php echo $this->Form->input('search_address_tel2',['class'=>'form-control google_autocomplete2 searchAddress  addressInput greenInput','data-button'=>'ShowProvozBtn2','data-preloader'=>'adr_preloader2','label'=>false,'placeholder'=>'např. Nádražní 6, Ostrava']); ?>
				<div id="provoz_name" class="none"></div>
			</div>			
			
			<div class="col col-md-2 none">
				<?php echo $this->Form->button('Vybrat provozovnu',['class'=>'btn btn-primary ','label'=>false,'id'=>'ShowProvozBtn2']);?>
			</div>
		<?php } ?>
		<div class="<?php echo (isset($new) && !isset($fastest)?'none':'')?>">
		<div class="col <?php echo $col_class2 ?>">
			<?php 
			
			if (isset($pref_order)){
				$google_adopt = [
					'data-mesto'=>'user-user-addresses-'.$google_id.'-city',
					'data-cp'=>'user-user-addresses-'.$google_id.'-cp',
					//'data-psc'=>'user-user-addresses-'.$google_id.'-zip',
					'data-ulice'=>'user-user-addresses-'.$google_id.'-street',
					'data-lat'=>'user-user-addresses-'.$google_id.'-lat',
					'data-lng'=>'user-user-addresses-'.$google_id.'-lng',
				]; 
			} else {
				$google_adopt = [
					'data-mesto'=>'user-addresses-'.$google_id.'-city',
					'data-cp'=>'user-addresses-'.$google_id.'-cp',
					//'data-psc'=>'user-addresses-'.$google_id.'-zip',
					'data-ulice'=>'user-addresses-'.$google_id.'-street',
					'data-lat'=>'user-addresses-'.$google_id.'-lat',
					'data-lng'=>'user-addresses-'.$google_id.'-lng',
				]; 

			}
			
			?>
			<div class="relative">
			<?php echo '<div id="search_address_tip3" class="none search_address_tip">'.__('Prosím, zadejte přesnou adresu - např. Pelclova 2500, Ostrava').'</div>';?>
			<?php echo $this->Form->input($pref.'street', ['label' => false,'placeholder'=>__('Zadejte Vaši ulici'),'data-type'=>'street','class'=>'form-control google_autocomplete clear_address placeholder']+$google_adopt); ?>
			</div>
		</div>
		<div class="col <?php echo $col_class3 ?>">
			<?php echo $this->Form->input($pref.'cp', ['label' => false,'placeholder' =>__('Číslo popisné / orientační'),'class'=>'form-control clear_address placeholder','data-type'=>'cp']); ?>
		</div>
	
		<div class="col <?php echo $col_class ?>">
			<?php echo $this->Form->input($pref.'city', ['label' => false,'placeholder' => __('Město'),'class'=>'form-control clear_address placeholder','data-type'=>'city']); ?>
			
		</div>
		<?php 
			if (isset($basket_provoz_list)){ 
				echo '<div class="col '.$col_class.'">';
				$provoz_list = [''=>'Vyberte provozovnu'] + $basket_provoz_list['provoz_list_online'];
				//pr($provoz_list);
				if (isset($_GET['fastest'])){
					$provoz_list[1000] = 'Testovaci';
				}
				echo $this->Form->input('shop_provoz_id', ['label' => false,'placeholder' => __('Provozovna'),'class'=>'form-control placeholder clear_address '.((isset($_GET['fastest']) || $_SERVER['REMOTE_ADDR'] == '89.103.18.65')?'fastest':''),'options'=>$provoz_list,'empty'=>false]);
				echo '<div id="provoz_error" class="none">'.__('Vaši adresu vozí provozovna').'<strong>Ostrava Slezska</strong></div>';
				echo '</div>';
			} 
		?>
			<?php //echo $this->Form->input($pref.'zip', ['label' => false,'placeholder' => __('PSČ'),'class'=>'form-control integer clear_address','maxlength'=>5,'data-type'=>'zip']); ?>
			<div class="none">
				<?php echo $this->Form->input($pref.'lat', ['label' => false,'class'=>'clear_address','data-type'=>'lat']); ?>
				<?php echo $this->Form->input($pref.'lng', ['label' => false,'class'=>'clear_address','data-type'=>'lng']); ?>
			</div>
		
		</div>
	</div>
	<?php 
	if (isset($loggedUser)){
		if (isset($address_basket)){
			echo '<br /><strong>Vaše poslední adresy pro doručení</strong>';
		}
		echo $this->element('../Users/elements/address_list'); 
	}
	?>
	<div class="row buttons field_button">
		<?php echo $this->Form->input(__('Zpět'), ['label'=>false,'type' => 'button', 'class'=>'prev_field btn btn-default btn-grey','data-id'=>0]);	?>
		<?php echo $this->Form->input(__('Uložit a pokračovat'), ['label'=>false,'type' => 'button', 'class'=>'next_field btn btn-default','data-id'=>2]);	?>
	</div>
		
</div>