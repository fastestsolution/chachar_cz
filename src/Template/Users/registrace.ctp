<div id="white_page">
	<div class="h1" id="h1_adopt"></div>
	<div id="reg_progress">
		<div id="bar" data-width="20"></div>
	</div>
<?php

echo $this->Form->create($users);
$this->Form->templates([
	//'inputContainer' => '<div class="form-group form-inline">{{content}}</div>'
]);
?>
<?php echo $this->element('../Users/elements/user_data'); ?>

<div id="hide_address" class="reg_field size <?php echo (isset($loggedUser)?'nonea':'') ?>">
<?php echo $this->element('../Users/elements/address_data',['address_type'=>1]); ?>
</div>
<?php echo $this->element('../Users/elements/avatar'); ?>
<?php if (!isset($loggedUser)){ ?>
<?php echo $this->element('../Users/elements/credits'); ?>
<?php echo $this->element('../Users/elements/registration_done'); ?>
<?php } ?>
<?php
echo '<div class="row buttons">';
	echo '<div class="col-md-4 col-md-offset-4">';
		echo $this->Form->button($button_title, ['class' => 'btn btn-lg btn-primary btn-block SaveForm','id'=>'RegButton']);
	echo '</div>';
echo '</div>';
echo $this->Form->end();
?>
</div>
<div class="none">

						<?php echo $cell = $this->cell('Shop.Provozs',[], ['cache' => false]);?>
</div>