<div id="mobileNoResponse" class="none">Máte špatné přípojení k internetu, zkuste obnovit</div>	

<?php if (isset($orders)){ ?>
<div id="mobile_page_preloader"></div>
<div id="map-canvas" class="none"></div>
<?php //echo($_SERVER['REMOTE_ADDR']);?>
<ul id="mobile_items">
	<?php 
	foreach($orders AS $k=>$o){ ?>
		<li data-id="item<?php echo $o->id;?>" class="order_item <?php echo (($o->rozvozce_date_vezu!=null)?'vezu_active':'')?>">
			<div class="map_data" data-lat="<?php echo $o->client_lat;?>" data-lng="<?php echo $o->client_lng;?>"></div>
			
			<div class="name">
			<?php echo '<span class="client_name">'.$o->client_name.'</span>';?>
			<?php echo $this->Html->link(strtr($o->client_telefon,['420'=>'']),'tel:+'.$o->client_telefon,['title'=>__('Zavolat'),'class'=>'tel']);?>
			</div>
			<?php echo $o->client_ulice.'<br />';?>
			<?php echo $o->client_mesto.' <span class="km"></span><br />';?>
			<?php echo $this->Html->link(__('Položky'),'#',['title'=>__('Položky'),'class'=>'btn btn-default btn-md polozky']); ?>
			
			<ul class="products none">
			<?php foreach($o->mobile_order_items AS $p){ ?>
				<li><?php echo '<span class="cnt">'.$p->count.'x</span> '.$p->name ?></li>
			<?php } ?>
			<span class="price">
				<?php 
				if ($o->shop_payment_id == 1){ 
					echo '<var>Zaplatit:</var> '.$this->Fastest->price($o->price_vat);
				} 
				if ($o->shop_payment_id == 2){ 
					echo '<var>Placeno kartou:</var> '.$this->Fastest->price($o->price_vat);
				} 
				if ($o->shop_payment_id == 3){ 
					echo '<var>Bude platit body:</var> '.$this->Fastest->price($o->price_vat);
				} 
				?>
			</span>
			</ul>
			<?php 
			//pr($o->doba_date);
			if (!empty($o->doba_date)){
			echo '<div class="doba_date" data-time="'.$o->doba_date->format('Y-m-d H:i:s').'">';
			//echo '<div class="doba_date" data-time="2017-07-24 03:48:26">';
				echo $o->doba_date->format('H:i');
			echo '</div>';
			}
			?>
			
			<div class="btns">
				
				<?php echo $this->Html->link(__('Vezu'),'/mobile_vezu/'.$o->id,['title'=>__('Právě vezu'),'data-id'=>$o->id,'onclick'=>'return false;','class'=>'btn btn-primary vezu '.(($o->rozvozce_date_vezu!=null)?'active':''),'data-title'=>'Opravdu vezete objednávku '.$o->client_ulice.', '.$o->client_name.'?']); ?>
				<?php //echo $this->Html->link(__('Navigovat'),'http://maps.google.com/?daddr='.$o->client_lat.','.$o->client_lng,['target'=>'_blank','title'=>__('Navigovat'),'class'=>'btn btn-default navigovat']); ?>
				<?php echo $this->Html->link(__('Navigovat'),'http://maps.google.com/?daddr='.$o->client_ulice.','.$o->client_mesto,['target'=>'_blank','title'=>__('Navigovat'),'class'=>'btn btn-default navigovat']); ?>
				<?php echo $this->Html->link(__('Vyloženo'),'/mobile_vylozeno/'.$o->id,['title'=>__('Vyloženo'),'onclick'=>'return false;','class'=>'btn btn-primary vylozeno','data-title'=>'Opravdu vyložit '.$o->client_ulice.', '.$o->client_name.'?']); ?>
				
				
			</div>
			<div class="move fa fa-arrows-v"></div>
			<?php echo $this->Html->link(__(''),'/mobile_nevylozeno/'.$o->id,['title'=>__('Nelze vyložit'),'class'=>'nevylozeno fa fa-close vylozeno','onclick'=>'return false;','data-title'=>'Opravdu NELZE VYLOŽIT '.$o->client_ulice.', '.$o->client_name.'?']); ?>
		</li>
	<?php } ?>
</ul>
<span id="gpsTime">GPS update: <?php echo (!empty($update_gps)?$update_gps->format('d.m. H:i:s'):'Nezjištěno');?></span>
<?php //pr($orders); ?>
<?php } else  {?>
		<div class="alert alert-info">
			Nemáte žádné přiřazené objednávky
		</div>
<?php } ?>
<div id="hash_url" data-value="<?php echo $hash_params;?>"></div>
<div id="noGps" class="<?php echo (isset($is_gps)?'none':'')?>">Neposíláte GPS polohu přes Big Brother</div>
<?php /* ?>
<script type="text/delayscript">
//<![CDATA[
/*
function sendGps(){
	//$('test').set('text',$('test').get('text').toInt()+1);
}
//setInterval(sendGps,1000);
//console.log(navigator);
/*
var gpsWorker = new Worker("/js/serviceWorker/gpsworker.js");

gpsWorker.onmessage = function (e) {
    console.log(e.data);
};

gpsWorker.postMessage("Start GPS!");

gpsWorker.onerror = function (e) {
    console.log("Error in file: " + e.filename + "\nline: " + e.lineno + "\nDescription: " + e.message);
};
//]]>
</script>
*/
?>