<div id="admin_products">
<?php echo $this->Html->link('Nový produkt','#',array('title'=>'Novy produkt','id'=>'new_product','class'=>'btn btn-primary',),null,false) ?>
<?php //pr($this->request->query['category_id']); ?>
<?php echo $this->Form->input('filtr_category_id',['options'=>$shop_menu_items_list,'empty'=>true,'label'=>'Kategorie','class'=>'filtr','value'=>(isset($this->request->query['category_id'])?$this->request->query['category_id']:'')]) ?>
<?php echo $this->Form->input('filtr_name',['label'=>'Jméno','class'=>'filtr','value'=>(isset($this->request->query['name'])?$this->request->query['name']:'')]) ?>
<div class="product">
<div id="new_prod_element" class="data none">

</div>
</div>
<div id="table_header">
	<div class="col col2">Číslo</div>
	<div class="col col2">Kod</div>
	<div class="col col3">Název</div>
	<div class="col col3">Kategorie</div>
	<div class="col col3">Foto</div>
	<div class="col">Aktivní u</div>
</div>
<?php
foreach($products AS $k=>$p){
	echo $this->element('../ChacharAdmins/elements/product',['p'=>$p,'k'=>$k]);
}
?>
</div>

<script type="text/delayscript">
//<![CDATA[
var myScript = Asset.javascript('/js/admin.js', {
    id: 'myScript',
    onLoad: function(){
    }
});
var myScript = Asset.javascript('/js/fst_uploader/fst_uploader.js', {
    id: 'myScript2',
    onLoad: function(){
    }
});
//]]>
</script>