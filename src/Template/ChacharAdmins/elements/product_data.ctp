<div id="product<?php echo (!empty($product->id)?$product->id:'new');?>">
<?php 
//pr($product);
echo $this->Form->create($product,['url'=>'/admin/save_product/']);?>
		<?php echo $this->Form->hidden('id',['label'=>'ID']); ?>
		<?php echo $this->Form->hidden('price_vat',['label'=>false,'class'=>'trans_price']); ?>
				
		<div class="row">
			<div class="col col-md-3">
				<?php echo $this->Form->hidden('desc',['label'=>'Popis: ','class'=>'text long trans_description','value'=>$product->description]); ?>
				<?php echo $this->Form->input('name_internal',['label'=>'Název: ','class'=>'text long trans_name']); ?>
				<?php 
				if (empty($product->id))
				echo $this->Form->input('price_vat',['label'=>'Cena: ','class'=>'text long']); ?>
				<?php echo $this->Form->input('is_pizza',['label'=>'Je pizza: ','class'=>'text long','options'=>$ano_ne]); ?>
				<?php echo $this->Form->input('el_class',['label'=>'Class: ','class'=>'text long']); ?>
				<?php echo $this->Form->input('number',['label'=>'Číslo: ','class'=>'text long']); ?>
				<?php echo $this->Form->input('credit',['label'=>'Kredit: ','class'=>'text long']); ?>
				<?php echo $this->Form->input('code',['label'=>'Kod: ','class'=>'text long']); ?>
				<?php echo $this->Form->input('gramaz',['label'=>'Gramáž: ','class'=>'text long']); ?>
				<?php echo $this->Form->input('sort',['label'=>'Pořadí: ','class'=>'text long integer']); ?>
				<?php echo $this->Form->input('food_type_id',['label'=>'Typ: ','class'=>'text long','options'=>$food_type_list]); ?>
				<?php echo $this->Form->input('shop_category_id',['label'=>'Kategorie: ','class'=>'text long','options'=>$shop_menu_items_list,'empty'=>true]); ?>
				<?php echo $this->Form->input('related_id',['label'=>'Související: ','class'=>'text long','options'=>$shop_menu_items_list,'empty'=>true]); ?>
				<?php foreach($system_list AS $system_id=>$system_name){ ?>
					<div class="<?php echo (($system_id > 1)?'none':'');?>">
					<?php echo $this->Form->input('_translations.cz'.$system_id.'.description',['label'=>'Popis CZ','class'=>'text long trans_cz_change_all trans_value_description']) ?>
					<?php echo $this->Form->input('_translations.en'.$system_id.'.description',['label'=>'Popis EN','class'=>'text long trans_en_change_all']) ?>
					</div>
				<?php } ?>
				<?php 
				echo '<ul class="attributes_list">';
				foreach($attributes_list AS $at_id=>$at){
					echo '<li>';
						echo $this->Form->input('shop_product_con_attributes.'.$at_id,['label'=>$at,'class'=>'checkbox','type'=>'checkbox','checked'=>(isset($attributes[$at_id])?'checked':false)]);
						
					echo '</li>';
				}
				echo '</ul>';
				?>
				<?php echo $this->Form->input('images',array('label'=>__('Foto:'),'class'=>'text file_upload fst_uploader','type'=>'text','id'=>'upload'.$product->id,
					'data-type'=>'multi',
					'data-ext'=>'jpg,png',
					'data-path'=>'/products/',
					'data-count'=>5,
					'file'=>true
				)); 
				?> 
				
			</div>
			<div class="col col-md-9">
				<table>
				<?php foreach($system_list AS $system_id=>$system_name){ ?>
					<tr>
						<th><?php echo $system_name;?></th>
						<td style="width:50px">
						<?php echo $this->Form->hidden('shop_product_prices.'.$system_id.'.id',['label'=>false]) ?>
						<?php echo $this->Form->hidden('shop_product_prices.'.$system_id.'.system_id',['label'=>false,'value'=>$system_id]) ?>
						<?php echo $this->Form->hidden('shop_product_prices.'.$system_id.'.tax_id',['label'=>false,'value'=>1]) ?>
						<?php //echo $this->Form->input('shop_product_prices.'.$system_id.'.status',['label'=>false,'data-label'=>'Aktivní','class'=>'text small data_label is_active','options'=>$ano_ne]) ?>
						<?php echo $this->Form->input('shop_product_prices.'.$system_id.'.status',['label'=>false,'data-label'=>'Aktivní','class'=>'text small data_label is_active','type'=>'checkbox']) ?>
						</td>
						<td>
							<?php echo $this->Form->input('shop_product_prices.'.$system_id.'.price_vat',['label'=>false,'data-label'=>'Cena s DPH','class'=>'text small data_label trans_value_price']) ?>
							<?php echo $this->Form->button('Varianty',['class'=>'btn btn-xs btn-primary open_varianta']); 
							if (isset($variants_count[$system_id])) echo ' ('.$variants_count[$system_id].' ks)';
							?>
							
							<div class="varianty none">
								<?php echo $this->Form->hidden('shop_product_variants.'.$system_id.'.0.id',[]) ?>
								<?php echo $this->Form->hidden('shop_product_variants.'.$system_id.'.1.id',[]) ?>
								<?php echo $this->Form->hidden('shop_product_variants.'.$system_id.'.2.id',[]) ?>
								<?php echo $this->Form->hidden('shop_product_variants.'.$system_id.'.0.system_id',['value'=>$system_id]) ?>
								<?php echo $this->Form->hidden('shop_product_variants.'.$system_id.'.1.system_id',['value'=>$system_id]) ?>
								<?php echo $this->Form->hidden('shop_product_variants.'.$system_id.'.2.system_id',['value'=>$system_id]) ?>
										
								<table>
									<tr>
										<td><?php echo $this->Form->input('shop_product_variants.'.$system_id.'.0.name',['label'=>false,'data-label'=>'Název','class'=>'text small data_label']) ?></td>
										<td><?php echo $this->Form->input('shop_product_variants.'.$system_id.'.0.code',['label'=>false,'data-label'=>'Kod','class'=>'text small data_label']) ?></td>
										<td><?php echo $this->Form->input('shop_product_variants.'.$system_id.'.0.price_vat',['label'=>false,'data-label'=>'Cena s DPH','class'=>'text small data_label']) ?></td>
									</tr>
									<tr>
										<td><?php echo $this->Form->input('shop_product_variants.'.$system_id.'.1.name',['label'=>false,'data-label'=>'Název','class'=>'text small data_label']) ?></td>
										<td><?php echo $this->Form->input('shop_product_variants.'.$system_id.'.1.code',['label'=>false,'data-label'=>'Kod','class'=>'text small data_label']) ?></td>
										<td><?php echo $this->Form->input('shop_product_variants.'.$system_id.'.1.price_vat',['label'=>false,'data-label'=>'Cena s DPH','class'=>'text small data_label']) ?></td>
									</tr>
									<tr>
										<td><?php echo $this->Form->input('shop_product_variants.'.$system_id.'.2.name',['label'=>false,'data-label'=>'Název','class'=>'text small data_label']) ?></td>
										<td><?php echo $this->Form->input('shop_product_variants.'.$system_id.'.2.code',['label'=>false,'data-label'=>'Kod','class'=>'text small data_label']) ?></td>
										<td><?php echo $this->Form->input('shop_product_variants.'.$system_id.'.2.price_vat',['label'=>false,'data-label'=>'Cena s DPH','class'=>'text small data_label']) ?></td>
									</tr>
								</table>
								
							</div>
						</td>
						<td><?php echo $this->Form->input('_translations.cz'.$system_id.'.name',['label'=>false,'data-label'=>'Název CZ','class'=>'text long data_label trans_value_name']) ?></td>
						<td><?php echo $this->Form->input('_translations.en'.$system_id.'.name',['label'=>false,'data-label'=>'Název EN','class'=>'text long data_label','value'=>'']) ?></td>
					</tr>
				<?php } ?>
				</table>
			</div>
		</div>
		<div class="buttons">
			<?php echo $this->Form->button('Uložit',['class'=>'btn btn-primary btn-md saveProduct']) ?>	
		</div>
<?php echo $this->Form->end(); ?>
</div>