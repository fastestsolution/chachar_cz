<?php //pr(); ?>
<div class="product <?php echo ($k++ % 2) ? "even" : "odd";?> ">
	<?php echo $this->Html->link('X','/admin/delete_products/'.$p->id,['title'=>'Smazat','class'=>'delete']); ?>
	<div class="headline" data-id="<?php echo $p->id;?>">
		<div class="col col2"><?php echo $p->number; ?></div>
		<div class="col col2"><?php echo $p->code.' ('.$p->sort.')'; ?></div>
		<div class="col col3"><?php echo $p->name_internal; ?></div>
		<div class="col col3"><?php echo (isset($shop_menu_items_list[$p->shop_category_id])?$shop_menu_items_list[$p->shop_category_id]:'Neni kategorie'); ?></div>
		<div class="col col3">
		<?php
				$img_set = array(
					'imgs'=>$p->images,
					//'link'=>$product_url,
					'class'=>'',
					'title'=>$p->name_internal,
					'width'=>50,
					'height'=>40,
					'bg'=>'#fff',		
					'path'=>'/uploaded/products/',		
					
				);
				echo $this->Fastest->img($img_set);
			?>
		</div>
		<?php 
		$aktivni = [];
		if (isset($p->shop_product_prices)){
			
			foreach($p->shop_product_prices AS $p){
				$aktivni[] = $system_list[$p->system_id];
			}
		}
		?>
		
		<div class="col"><?php echo implode(', ',$aktivni); ?></div>
	</div>
	<div class="data none">
		
	</div>
</div>