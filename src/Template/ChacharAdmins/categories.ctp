<?php echo $this->Html->link('Přidat','#',array('title'=>'Přidat','class'=>'btn btn-primary','id'=>'menu_add'),null,false); ?>
<?php echo $this->Form->create($menu_items,['url'=>'/admin/save_menu/']);?>
<div id="group_list" data-options='<?php echo json_encode($product_category_group_list)?>'></div>
<ul id="admin_menu_items">
	<?php 
	$product_category_group_list[9] = 'Pro mazlíky';
	//pr($product_category_group_list);
	if (!empty($menu)){ 
		foreach($menu AS $k=>$m){
			echo '<li id="li'.$k.'" class="clearfix">';
				echo '<div class="fa fa-arrows-v line_move"></div>';
				echo $m->id;
				echo $this->Form->input('shop_categories.'.$k.'.id',['placeholder'=>'CZ název','value'=>$m->id]);
				echo $this->Form->input('_translations.'.$k.'.cz.name',['placeholder'=>'CZ název','value'=>$m->_translations['cz']['name'],'label'=>false]);
				echo $this->Form->input('_translations.'.$k.'.en.name',['placeholder'=>'EN název','value'=>$m->_translations['en']['name'],'label'=>false]) ;
				echo $this->Form->button('Smazat',['class'=>'btn btn-primary delete_row right']);
				echo $this->Form->input('shop_categories.'.$k.'.group_id',['placeholder'=>'CZ název','value'=>$m->group_id,'options'=>$product_category_group_list,'label'=>false]);
				
			echo '</li>';
		}
	} 
	?>
</ul>
<div class="none">
<?php echo $this->Form->input('sort',[]); ?>
</div>
<?php echo $this->Form->button('Uložit',['class'=>'btn btn-primary','id'=>'SaveMenu']); ?>
<?php $this->Form->end(); ?>

<script type="text/delayscript">
//<![CDATA[
	// label
			$('admin_menu_items').getElements('input').each(function(item){
			
				new Element('span',{'class':'data_label none'}).set('data-label',item.get('placeholder')).inject(item,'before');
				item.addEvents({
					'mouseenter':function(e){
						e.target.getPrevious('span').removeClass('none');
					},
					'mouseleave':function(e){
						e.target.getPrevious('span').addClass('none');
					},
				});
			});
			
    $$('.delete_row').addEvent('click',function(e){
		e.stop();
		if (confirm('Opravdu smazat?')){
			e.target.getParent('li').destroy();
		}
   });
   $('menu_add').addEvent('click',function(e){
		count = $('admin_menu_items').getElements('li').length;
		li = new Element('li',{'id':'li'+count}).inject($('admin_menu_items'));
		input = new Element('input',{'type':'hidden','name':'shop_categories['+count+'][id]',}).inject(li);
		input = new Element('input',{'type':'text','name':'_translations['+count+'][cz][name]','placeholder':'CZ nazev'}).inject(li);
		input = new Element('input',{'type':'text','name':'_translations['+count+'][en][name]','placeholder':'EN nazev'}).inject(li);
		select = new Element('select',{'name':'shop_categories['+count+'][group_id]',}).inject(li);
		opt = JSON.decode($('group_list').get('data-options'));
		Object.each(opt,function(item,k){
			new Element('option',{'value':k}).set('text',item).inject(select);
		});
		sortable();
   });
   function sortable(){
	var mySortables = new Sortables($('admin_menu_items'), {
		handle: '.line_move',
		initialize: function(){
		},
		onComplete:function(el){
			var ser = mySortables.serialize();		
			$('sort').value = JSON.encode(ser);
		}
		
	 
	});
		var ser = mySortables.serialize();
		if (ser != '')
		$('sort').value = JSON.encode(ser);		
		
   }
	sortable();
	
   $('SaveMenu').addEvent('click',function(e){
		e.stop();
		form = e.target.getParent('form');
		button = e.target;
		button_preloader(button);
		this.req_form = new Request.JSON({
				url:'/admin/save_menu/',
				onError: this.req_error = (function(data){
					
					button_preloader(button);
				}).bind(this),

				onComplete :(function(json){
					
					button_preloader(button);
					if (json.r == true){
						FstAlert(json.m);
						window.location = '/admin/categories/';
					}
				}).bind(this)
			});
			this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			this.req_form.post(form);
   });
//]]>
</script>