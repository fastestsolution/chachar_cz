<div id="group_list" data-options='<?php echo json_encode($product_attributes_group_list);?>'></div>
<?php echo $this->Form->create($attributes,['url'=>'/admin/save_attributes/']);?>
<ul id="admin_menu_items">
	<?php 
	if (!empty($products)){ 
		foreach($products AS $k=>$m){
			if (!isset($m->_translations['cz']['name'])) $m->_translations['cz']['name'] = $m->name;
			if (!isset($m->_translations['en']['name'])) $m->_translations['en']['name'] = '';
			echo '<li id="li'.$k.'" class="clearfix">';
				echo $this->Form->input('shop_product_attributes.'.$k.'.id',['placeholder'=>'CZ název','value'=>$m->id]);
				echo $this->Form->input('_translations.'.$k.'.cz.name',['placeholder'=>'CZ název','value'=>$m->_translations['cz']['name'],'label'=>false]);
				echo $this->Form->input('_translations.'.$k.'.en.name',['placeholder'=>'EN název','value'=>$m->_translations['en']['name'],'label'=>false]) ;
				echo $this->Form->input('shop_product_attributes.'.$k.'.price_vat',['placeholder'=>'Cena','value'=>$m->price_vat,'label'=>false]);
				echo $this->Form->input('shop_product_attributes.'.$k.'.code',['placeholder'=>'Kod','value'=>$m->code,'label'=>false]);
				echo $this->Form->input('shop_product_attributes.'.$k.'.gramaz',['placeholder'=>'Gramáž','value'=>$m->gramaz,'label'=>false]);
				echo $this->Form->input('shop_product_attributes.'.$k.'.group_id',['placeholder'=>'Skupina','value'=>$m->group_id,'options'=>$product_attributes_group_list,'label'=>false]);
				echo $this->Form->button('Smazat',['class'=>'btn btn-primary delete_row right']);
			echo '</li>';
		}
	} 
	?>
</ul>
<?php echo $this->Html->link('Přidat','#',array('title'=>'Přidat','class'=>'btn btn-primary','id'=>'menu_add'),null,false); ?>
<?php echo $this->Form->button('Uložit',['class'=>'btn btn-primary','id'=>'SaveMenu']); ?>
<?php $this->Form->end(); ?>

<script type="text/delayscript">
//<![CDATA[
			$('admin_menu_items').getElements('input').each(function(item){
			
				new Element('span',{'class':'data_label none'}).set('data-label',item.get('placeholder')).inject(item,'before');
				item.addEvents({
					'mouseenter':function(e){
						e.target.getPrevious('span').removeClass('none');
					},
					'mouseleave':function(e){
						e.target.getPrevious('span').addClass('none');
					},
				});
			});
   $('menu_add').addEvent('click',function(e){
		e.stop();
		count = $('admin_menu_items').getElements('li').length;
		li = new Element('li',{'id':'li'+count}).inject($('admin_menu_items'));
		input = new Element('input',{'type':'hidden','name':'shop_product_attributes['+count+'][id]',}).inject(li);
		input = new Element('input',{'type':'text','name':'_translations['+count+'][cz][name]','placeholder':'CZ nazev'}).inject(li);
		input = new Element('input',{'type':'text','name':'_translations['+count+'][en][name]','placeholder':'EN nazev'}).inject(li);
		input = new Element('input',{'type':'text','name':'shop_product_attributes['+count+'][price_vat]','placeholder':'Cena'}).inject(li);
		input = new Element('input',{'type':'text','name':'shop_product_attributes['+count+'][code]','placeholder':'Kod'}).inject(li);
		input = new Element('input',{'type':'text','name':'shop_product_attributes['+count+'][gramaz]','placeholder':'Gramaz'}).inject(li);
		select = new Element('select',{'name':'shop_product_attributes['+count+'][group_id]',}).inject(li);
		opt = JSON.decode($('group_list').get('data-options'));
		Object.each(opt,function(item,k){
			new Element('option',{'value':k}).set('text',item).inject(select);
		});
   });
   
   $$('.delete_row').addEvent('click',function(e){
		e.stop();
		if (confirm('Opravdu smazat?')){
			e.target.getParent('li').destroy();
		}
   });
	
   $('SaveMenu').addEvent('click',function(e){
		e.stop();
		form = e.target.getParent('form');
		button = e.target;
		button_preloader(button);
		this.req_form = new Request.JSON({
				url:'/admin/save_attributes/',
				onError: this.req_error = (function(data){
					
					button_preloader(button);
				}).bind(this),

				onComplete :(function(json){
					
					button_preloader(button);
					if (json.r == true){
						FstAlert(json.m);
						window.location = '/admin/attributes/';
					}
				}).bind(this)
			});
			this.req_form.setHeader('X-CSRF-Token', Cookie.read('csrfToken'));
			this.req_form.post(form);
   });
//]]>
</script>