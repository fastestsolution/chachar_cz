<div id="intro_adresa" data-template="<?php echo (isset($this->request->query['template'])?$this->request->query['template']:'')?>" class="<?php echo (isset($this->request->query['template'])?$this->request->query['template']:'')?>">
<div id="intro_akce">
	<?php //pr($this->request->params); ?>
	<div id="intro_logo" class="logo <?php echo $css_styles['logo']?>"></div>
	<?php 
	$actual_time = date('H:i');
	$from_time = date('10:45');
	$to_time = date('15:00');
	switch($system_id){
	
	case 2: { // manik
		if (strtotime($actual_time) > strtotime($from_time) && strtotime($actual_time) < strtotime($to_time)){
			echo '<div id="akce1" class="'.$css_styles['logo'].'"></div>';
		
		} else {
			echo '<div class="akce_switch_over">';
				echo '<div id="akce1" class="akce_switch '.$css_styles['logo'].'"></div>';
				echo '<div id="akce2" class="none akce_switch '.$css_styles['logo'].'"></div>';
			echo '</div>';
		}
		break;
	}
	default: {
		echo '<div id="akce1" class="'.$css_styles['logo'].'"></div>';
		
	}
	}
	
	?>
	
	<div id="search_address">
	<?php
	echo $this->Form->create(null,['url'=>'/searchAddress/']);
	?>
	<div class="address_input">
		<?php  
		$google_adopt = [
			'data-mesto'=>'address-city',
			'data-cp'=>'address-cp',
			'data-ulice'=>'address-street',
			'data-lat'=>'address-lat',
			'data-lng'=>'address-lng',
		]; 
		?>
		<?php echo '<div id="search_address_noresult" class="none">'.__('Pokud Vás našeptávač nenašel zadejte nejbližší velké město například Praha, Brno, Ostrava, Olomouc...').'</div>'; ?>
		<?php echo '<div id="search_address_tip" class="none">'.__('Prosím, zadejte přesnou adresu - např. Pelclova 2500, Ostrava').'</div>'; ?>
		<?php echo $this->Form->input('address_value', ['label'=>false,'placeholder' => __('Zadejte adresu pro doručení...'),'class'=>'form-control google_autocomplete ']+$google_adopt); ?>
		<div class="none">
		<?php echo $this->Form->input('address_lat', ['label'=>false,'class'=>'clear_address']); ?>
		<?php echo $this->Form->input('address_lng', ['label'=>false,'class'=>'clear_address']); ?>
		<?php echo $this->Form->input('address_street', ['label'=>false,'class'=>'clear_address']); ?>
		<?php echo $this->Form->input('address_cp', ['label'=>false,'class'=>'clear_address']); ?>
		<?php echo $this->Form->input('address_city', ['label'=>false,'class'=>'clear_address']); ?>
		</div>
		
	<?php
		//echo '<div class="col-md-2">';
			
			echo $this->Form->button(__('Vyhledat nejbližší provozovnu'), ['class' => 'btn btn-lg btn-primary btn-block','id'=>'SearchAdresa']);
		//echo '</div>';
		echo '<div id="map_coords" class="none"  data-json=\''.$map_coords_search.'\'></div>';
		$redirect = [];
		foreach($pokladna_list['data'] AS $d){
			if (isset($d->system_id) && isset($redirect_system_ids[$d->system_id])){
				$redirect[$d->id] = $redirect_system_ids[$d->system_id];
			}
		}
		//pr($redirect);
		echo '<div id="redirect_system_ids" class="none" data-json=\''.json_encode($redirect).'\'></div>';
	echo '</div>';
	echo $this->Form->end();
	?>
	</div>
</div>
<div class="adresa_footer"></div>
</div>