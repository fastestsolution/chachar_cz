<style>
.table-striped {width:100%;}
.table-striped tr td ,.table-striped tr th {padding:3px;font-size:11px;}
.table-striped tr:hover td {background:#666;color:#fff;}
</style>
<div id="check_orders">
<?php
if (!isset($skladUser)){
echo $this->Form->create('',['url'=>'/check_orders_login/']);

echo '<div class="row">';
	echo $this->Form->input('user', ['label'=>false,'placeholder' => __('Provozovna'),'class'=>'form-control']);
	echo $this->Form->input('pass', ['label'=>false,'placeholder' => __('Heslo'),'class'=>'form-control','type'=>'password']);
echo '</div>';
echo '<div class="row buttons">';
	echo '<div class="col-md-12">';
		echo $this->Form->button(__('Přihlásit se'), ['class' => 'btn btn-lg btn-primary btn-block','id'=>'LoginCheckOrders']);
	echo '</div>';
echo '</div>';
echo $this->Form->end();

} else {
echo '<br />';
echo '<br />';
echo $this->Html->link('Odhlásit','/check_orders_logout/',['title'=>'']);

echo '<br />';
echo $this->Form->create('',['url'=>'/check_orders/']);

echo '<div class="row">';
	echo $this->Form->input('telefon', ['label'=>false,'placeholder' => __('Telefon zákazníka, nebo web_id objednávky z pokladny'),'class'=>'form-control','type'=>'text','id'=>'EnterPhone']);
echo '</div>';

echo '<div class="row buttons">';
	echo '<div class="col-md-12">';
		echo $this->Form->button(__('Vyhledat objednávky'), ['class' => 'btn btn-lg btn-primary btn-block','id'=>'FindOrders']);
	echo '</div>';
echo '</div>';
echo $this->Form->end();


//pr($orders);
if (isset($orders)){
echo '<div class="table-responsive">';
	echo '<table class="table-striped">';
		echo '<tr>';
			echo '<th>WEB ID</th>';
			echo '<th>Vytvořeno</th>';
			echo '<th>Klient ID</th>';
			echo '<th>Klient stav kreditů</th>';
			echo '<th>Jméno</th>';
			echo '<th>Telefon</th>';
			//echo '<th>Email</th>';
			echo '<th>Platba</th>';
			echo '<th>Provoz</th>';
			echo '<th>Přeposláno z</th>';
			//echo '<th>Přeposláno datum</th>';
			echo '<th>Potvrzení z pokladny</th>';
			echo '<th>Reg.</th>';
			echo '<th>Cena</th>';
			echo '<th>Spropitné</th>';
			echo '<th>Platba přijata</th>';
			//echo '<th>GOPAY ID</th>';
			echo '<th>Kredity před / po</th>';
			echo '<th>Kredity použity</th>';
			//echo '<th>Kredity po objednávce</th>';
			echo '<th>Kredity za objednávku</th>';
			echo '<th>Zdroj</th>';
			//echo '<th>Web</th>';
		echo '</tr>';
		foreach($orders AS $order){
			//pr($order);
			//pr($order->preposlano_provoz_id);
			//pr($pokladna_list['data']);
			//pr($pokladna_list);
			//pr($pokladna_list['data'][$order->shop_provoz_id]);
			echo '<tr>';
				echo '<td>'.$order->id.'</td>';
				echo '<td>'.$order->created->format('d.m.Y H:i:s').'</td>';
				echo '<td>'.$order->shop_client_id.'</td>';
				echo '<td>'.(isset($order->user->credits)?$order->user->credits:'Smazán').'</td>';
				echo '<td>'.$order->client_name.'</td>';
				echo '<td>'.$order->client_telefon.'<br />'.(isset($order->user->email)?$order->user->email:'Smazán').'</td>';
				//echo '<td>'.(isset($order->user->email)?$order->user->email:'Smazán').'</td>';
				echo '<td>'.$chachar_payment_list_short[$order->shop_payment_id]['name'].'</td>';
				echo '<td>'.$pokladna_list['data'][$order->shop_provoz_id]['name'].'</td>';
				echo '<td>'.(isset($pokladna_list['data'][$order->preposlano_provoz_id]['name'])?$pokladna_list['data'][$order->preposlano_provoz_id]['name']:'').(!empty($order->preposlano_date)?'<br />'.$order->preposlano_date->format('d.m.Y H:i:s'):'').'</td>';
				//echo '<td>'.(!empty($order->shop_preposlano_date)?$order->shop_preposlano_date->format('d.m.Y H:i:s'):'').'</td>';
				echo '<td>'.(!empty($order->email_send)?$order->email_send->format('d.m.Y H:i:s'):'').'</td>';
				echo '<td>'.$ano_ne[$order->reg].'</td>';
				echo '<td>'.$order->price_vat.'</td>';
				echo '<td>'.$order->spropitne.'</td>';
				echo '<td>'.(!empty($order->payment_received)?$order->payment_received->format('d.m.Y H:i:s').'<br />'.$order->gopay_id:'').'</td>';
				//echo '<td>'.$order->gopay_id.'</td>';
				echo '<td>'.(!empty($order->credit_use_before)?$order->credit_use_before.'/'.$order->credit_use_after:'Nepřihlášený').'</td>';
				echo '<td>'.$order->credit_use.'</td>';
				//echo '<td>'.$order->credit_use_after.'</td>';
				echo '<td>'.$order->credits_from_order.'</td>';
				echo '<td>'.$zdroj_list[$order->zdroj_id].'</td>';
				//echo '<td>'.$system_list[$order->system_id].'</td>';
			echo '</tr>';
		}
	echo '</table>';
echo '</div>';
} else {
	if (isset($noresult)){
		echo '<div class="alert alert-danger"><strong>Chyba:</strong> Nenalezeny žádné objednávky, zadejte jiný telefon</div>';
	} else {
		echo '<div class="alert alert-info"><strong>Info:</strong> Nenalezeny žádné objednávky, zadejte telefon</div>';
	}
}

//if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65a'){
//if (isset($this->request->query['fastest'])){

	
echo $this->Form->create('',['url'=>'/check_orders_client/']);
	
	echo '<div class="row">';
		echo $this->Form->input('telefon_client', ['label'=>false,'placeholder' => __('Telefon (email,id) zákazníka, pro úpravu kreditů'),'class'=>'form-control','type'=>'text','id'=>'EnterPhoneClient']);
		echo $this->Form->input('client_id', ['label'=>'ID','class'=>'form-control  clear_client','type'=>'hidden']);
		echo '<div class="show_client">';
			echo '<div class="col col-xs-6">';
				echo $this->Form->input('client_first_name', ['label'=>'Jméno','class'=>'form-control','type'=>'text clear_client']);
				echo $this->Form->input('client_credits', ['label'=>'Kredity','class'=>'form-control','type'=>'text clear_client integer']);
			echo '</div>';
			echo '<div class="col col-xs-6">';
				echo $this->Form->input('client_last_name', ['label'=>'Příjmení','class'=>'form-control','type'=>'text clear_client']);
				//echo $this->Form->input('telefon_client', ['label'=>'Telefon','class'=>'form-control','type'=>'text clear_client']);
			echo '</div>';
		echo '</div>';
	echo '</div>';

	echo '<div class="row buttons">';
		echo '<div class="col-md-12">';
			echo $this->Form->button(__('Vyhledat zákazníka'), ['class' => 'btn btn-lg btn-primary btn-block','id'=>'FindClients']);
			echo $this->Form->button(__('Upravit zákazníka'), ['class' => 'btn btn-lg btn-default btn-block show_client','id'=>'EditClients']);
		echo '</div>';
	echo '</div>';
	echo $this->Form->end();
}

//}
?>
</div>
<script type="text/javascript">
//<![CDATA[
//]]>
</script>