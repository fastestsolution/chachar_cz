<?php  
/*
echo __('<p>Je nám líto, ale zadaná stránka nemůže být zobrazena.</p>
		<h2>Možné příčiny Vašeho problému</h2>
			<ul>
				<li>Chybně zadaná adresa stránky</li>
				<li>Zadaná stránka neexistuje</li>
				<li>Stránka je dočasně nedostupná, zkuste se vrátit později</li>
				<li>Nemáte oprávnění k prohlížení zadané stránky</li>
				<li>Zkuste navštívit <a href="/" title="Zpět na Úvodní stránku">úvodní stránku</a></li>
			</ul>
');
echo '<h2>'.__('Mapa stránek').'</h2>';
$options = [
	'class'=>'sitemap',
	'sitemap'=>true,
	'prefix'=>(isset($modul_shop)?'/text/':''),
	'path'=>$menu_items_path,
];
//pr($menu_items);
echo $this->Fastest->generateMenu($menu_items,$options);
if (isset($modul_shop)){
	echo '<br />';
	$options = [
		'class'=>'sitemap',
		'sitemap'=>true,
		'path'=>$shop_menu_items_path,
	];
	echo $this->Fastest->generateMenu($shop_menu_items,$options);
}
*/
echo '<div id="page404">';
	echo '<div id="logo_adopt"></div>';
	echo '<div class="txt">';
		echo '<div class="title">404</div>';
		echo '<div class="ico"></div>';
		echo '<div class="title2">'.__('Ať koukám, jak koukám...').'</div>';
		echo '<div class="title3">'.__('... nic nevidím. Co dělat, dostali jste se na odkaz, který nelze nalézt.').'</div>';
		echo $this->Html->link(__('Přejít na úvodní stránku'),'/',['title'=>__('Přejít na úvodní stránku'),'class'=>'btn btn-primary']);
	echo '</div>';
echo '</div>';
?>
<script type="text/delayscript">
//<![CDATA[
window.addEvent('domready',function(){
	$('logo_adopt').adopt('logo');
	
});
//]]>
</script>