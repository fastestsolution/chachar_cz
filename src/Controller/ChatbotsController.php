<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;
header('Content-Type: text/html; charset=utf-8');
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\I18n\Time;
use lessc\lessc;

class ChatbotsController extends AppController
{
	var $errorMessages = [
		404=>'Nenalezeno',
		401=>'Chyba uložení',
		402=>'Nebylo ulozeno, zaznam jiz existuje',
		403=>'Platba nebyla nalezena',
		404=>'Produkt nebyl nalezen',
		405=>'Kredity nelze použít',
		406=>'Chyba uložení objednávky',
		407=>'Chyba zalozeni platby',
		408=>'Provoz nenalezen',
		409=>'Given address does not exist',
	];
	
	var $server = [
		'devel',
		'test',
		'prod',
	];
	
    public function index(){
        $list = [
			['url'=>'https://www.chachar.cz/chatbot/orders','desc'=>'Objednavky',],
			['url'=>'https://www.chachar.cz/chatbot/payment','desc'=>'online platby',],
			['url'=>'https://www.chachar.cz/chatbot/orders/','desc'=>'Odeslani objednavky',],
			['url'=>'https://www.chachar.cz/chatbot/products/','desc'=>'Seznam produktu',],
			['url'=>'https://www.chachar.cz/chatbot/products/C1A','desc'=>'Detail produktu',],
			['url'=>'https://www.chachar.cz/chatbot/provoz/','desc'=>'Seznam provozoven',],
			['url'=>'https://www.chachar.cz/chatbot/delivery_price/','desc'=>'Cena dopravy',],
			['url'=>'https://www.chachar.cz/chatbot/minimum_price/','desc'=>'Min. cena',],
			['url'=>'https://www.chachar.cz/chatbot/branches/1/openhours','desc'=>'Oteviraci doba',],
			['url'=>'https://www.chachar.cz/chatbot/users/104/','desc'=>'Detail uzivatele',],
			['url'=>'https://www.chachar.cz/chatbot/users/1172461/phone_number/','desc'=>'Zmena telefonu',],
			['url'=>'https://www.chachar.cz/chatbot/carousel/pizza','desc'=>'Carousel',],
			['url'=>'https://www.chachar.cz/chatbot/address/','desc'=>'Validace adresy',],
			//['url'=>'https://www.chachar.cz/chatbot/users/&lt;id&gt;/address/','desc'=>'Zmena adresy',],
			['url'=>'https://www.chachar.cz/chatbot/address/branch_code','desc'=>'Ziskani z adresy pobocku'],
			['url'=>'https://www.chachar.cz/chatbot/login?se=dev&msi=1','desc'=>'Login','onclick'=>true],
			['url'=>'https://www.chachar.cz/chatbot/register?se=dev&msi=1','desc'=>'Registrace','onclick'=>true],
			['url'=>'https://www.chachar.cz/chatbot/payment','desc'=>'Registrace','onclick'=>true],
		];
		echo '<table>';
			foreach($list AS $l){
				echo '<tr>';
					echo '<td>'.$l['desc'].'</td>';
					if (isset($l['onclick'])){
						$params = [
							'onclick'=>'window.open(\''.$l['url'].'\')',
							'target'=>'_blank',
						];
						$link = '<a  href="#" onclick="window.open(\''.$l['url'].'\',\'_blank\');">'.$l['url'].'</a>';
					
					} else {
						$link = '<a href="'.$l['url'].'">'.$l['url'].'</a>';
					}
					echo '<td>'. $link .'</td>';
					
				echo '</tr>';
			}
		echo '</table>';
		//echo 'https://www.chachar.cz/chatbot/users/&lt;id&gt;/address/ <br>';
		//echo "<a href=\"\" onclick=\"window.open('https://www.chachar.cz/chatbot/login?se=dev&msi=1','_blank');\" >https://www.chachar.cz/chatbot/login?se=dev&msi=1</a><br />";
		//echo "<a href=\"\" onclick=\"window.open('https://www.chachar.cz/chatbot/register?se=dev&msi=1','_blank');\" >https://www.chachar.cz/chatbot/register?se=dev&msi=1</a><br />";
    	die();
    }
	
	
	public function test2(){
		$ch = curl_init();
		$url = 'https://www.chachar.cz/chatbot/address/branch_code/';
		//$post = ['phone_number'=>'+420731956030'];
		//$post = json_encode($post);
		$json = [
				//'user_code'=>104,
				'address'=>[
					'formatted_address'=>"Pelclova 2500\/5, Ostrava-město", 
					"street"=>"Pelclova", 
					"city"=>"Ostrava-město", 
					"cp"=>"2500\/5", 
					"lat"=> 49.8481683, 
					"lng"=> 18.2856251,
					//"id"=> 263583
				],
				'phone_number'=>'+420731956030',
				'first_name'=>'Jakub',
				'last_name'=>'Fastest',
				'p_method'=>'credits',
				'server'=>'dev',
				'products'=>[
					['code'=>'001A'],
					['code'=>'004A'],
				],
				
				
			];
		$json =  ['formatted_address'=>"Pelclova 2500/5, Ostrava-město", "street"=>"Pelclova", "city"=>"Ostrava-město", "cp"=>"2500/5", "lat"=>49.8481683, "lng"=>18.2856251];	
		$json =  ['address'=>"Pelclova 5, Ostrava"];	
		$post = json_encode($json);	
		//pr($post); 
		//die('aa');
		//pr($post);die();	
		
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		curl_setopt( $ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec ($ch);
                var_dump($result);
		pr($result);
		curl_close ($ch);
		die('end');
		
	}
	
	public function test(){
		$this->loadComponent('Polygon');
		//$pointLocation = new pointLocation();
		//$points = array("50 70.1","70 40","-20 30","100 10","-10 -10","40 -20","110 -20");
		$points = array("55 74.2");
		$polygon = array("-50 30","50 70","55 75","80 10","110 -10","110 -30","-20 -50","-30 -40","10 -10","-10 10","-30 -20","-50 30");
		// The last point's coordinates must be the same as the first one's, to "close the loop"
		foreach($points as $key => $point) {
			echo "point " . ($key+1) . " ($point): " . $this->Polygon->pointInPolygon($point, $polygon) . "<br>";
		}
		//echo "<a href=\"\" onclick=\"window.open('https://www.chachar.cz/chatbot/login?se=dev&msi=1','_blank');\" >https://www.chachar.cz/chatbot/login?se=dev&msi=1</a><br />";
		//echo "<a href=\"\" onclick=\"window.open('https://www.chachar.cz/chatbot/register?se=dev&msi=1','_blank');\" >https://www.chachar.cz/chatbot/register?se=dev&msi=1</a><br />";
		die();
	}
	
	public function findSystemId($lat=null,$lng=null){
		if (!$lat){
			$lat = 49.8481683;
			$lng = 18.2856251;
		}
		//pr($lat);
		//pr($lng);
		$this->loadComponent('Polygon');
		$point = $lat.' '.$lng;
		
		$this->loadModel('Shop.ShopProvozs');
		$this->basket_provoz_list = $this->ShopProvozs->provozListAll();	
		
		$this->selectSystemId = false;
		
		$this->loadModel('Shop.ShopMapAreas');
		
		$map_area_list = $this->ShopMapAreas->mapAreaListData();
		//pr($map_area_list);
		//pr($this->basket_provoz_list['provoz_pokladna_id']);
		$this->map_area_list = [];
                //die(json_encode($this->basket_provoz_list['provoz_pokladna_id']));
		foreach($map_area_list AS $area_id=>$d){
			//pr($area_id);
			if (isset($this->basket_provoz_list['provoz_pokladna_id'][$area_id])){ 
				$this->map_area_list[$this->basket_provoz_list['provoz_pokladna_id'][$area_id]] = $d;
				
			} else {
				//$this->map_area_list[1000] = $d;
				
			}
		}
                //die($point);
		//pr($this->map_area_list);
                $d = '';
		foreach($this->map_area_list AS $system_id=>$mapData){
                        $d .= $system_id .',';
			//$this->map_area_list[$system_id][] = $mapData[0];
			//pr($mapData[0]);
			foreach($mapData AS $polygonId=>$md){
                              
				//echo "point " . ($polygonId) . " ($point): " . $this->Polygon->pointInPolygon($point, $md) . "<br>";
				if ($this->Polygon->pointInPolygon($point, $md) != 'outside'){
					$this->selectSystemId = $system_id;
				}
			}
		}
                //die($d);
		//pr($this->selectSystemId);
		//pr($this->map_area_list);
		//pr($point);
		if (!$this->selectSystemId){
			http_response_code(202);
			die(json_encode(['error_code'=>'OUT_OF_DELIVERY_AREA','error_message'=>'Given address is out of delivery area']));
		}
		return $this->selectSystemId;
		//die(json_encode(['result'=>true,'system_id'=>$this->selectSystemId]));
	}
	
	
	public function payment(){
		if (!isset($this->request->query['se'])){
			$this->request->query['se'] = 'dev';
			$this->request->query['msi'] = 1;
			$this->request->query['oc'] = 776788;
		}
		$this->Session->write('se',$this->request->query['se']);
		$this->Session->write('msi',$this->request->query['msi']);
		$this->Session->write('oc',$this->request->query['oc']);
		//pr($_SESSION);
		//die();		
		$this->gopay_payment($this->request->query['oc']);
		//pr($this->gopay_result);die();
		if ($this->gopay_result){
			
			$this->redirect($this->gopay_result['gateway_url']);
		} else {
			http_response_code(401);
			die(json_encode(['error_code'=>407,'error_message'=>$this->errorMessages[407]])); 
		}
		
		//$result = [];
		//die(json_encode($result));
	}
	
	public function complete(){
		echo '<script type="text/javascript">window.close();</script>';
		die();
	}
	
	// gopay payment
	public function gopay_payment($id){
		$model = 'Shop.ShopOrders';
		$this->loadModel($model);
		$order_data = $this->ShopOrders->find()
			->where(['ShopOrders.id'=>$id])
			->select([
			])
			->contain(['Users','Users.UserAddresses'])
			->first();
		//pr($order_data);die();
		
		$system_id = $order_data['system_id'];
		//pr($order_data);
		//$gp = HelperRegistry::get("Gopay");
		//pr($gp);
		//$loader = '../vendor/autoload.php';
		
		// load provoz list
		$this->loadModel('Shop.ShopProvozs');
		$this->set('basket_provoz_list',$this->basket_provoz_list = $this->ShopProvozs->provozListAll());	
		//pr($this->basket_provoz_list['data'][$order_data->shop_provoz_id]);
		//pr($order_data->shop_provoz_id);
		//die();
		if (empty($this->basket_provoz_list['data'][$order_data->shop_provoz_id]->gopay_id)){
			die(json_encode(['r'=>false,'m'=>__d('shop','Provoz nemá aktivní platební bránu')]));
		}
		$order = array(
			'gopay_id'=>$this->basket_provoz_list['data'][$order_data->shop_provoz_id]->gopay_id,
			'gopay_client_id'=>$this->basket_provoz_list['data'][$order_data->shop_provoz_id]->gopay_client_id,
			'gopay_client_secure'=>$this->basket_provoz_list['data'][$order_data->shop_provoz_id]->gopay_secure,
			//'gopay_secure_key'=>$this->basket_provoz_list['data'][$order_data->shop_provoz_id]->gopay_secure,
			'id'=>$order_data['id'],
			'description'=>$order_data['user']['last_name'].' '.$order_data['user']['first_name'],
			'price'=>$order_data['price_vat'],
			'client'=>array(
				'first_name'=>$order_data['user']['first_name'],
				'last_name'=>$order_data['user']['last_name'],
				'email'=>$order_data['user']['email'],
				'phone_number'=>$order_data['user']['phone'],
				'city'=>$order_data['user']['user_addresses'][0]['city'],
				'street'=>$order_data['user']['user_addresses'][0]['street'].' '.$order_data['user']['user_addresses'][0]['cp'],
				'country_code'=>'CZE',
			),
			//'notify_url'=>'/chatbot/complete/'
		);
		//pr($order);die();
		$this->loadComponent('Shop.Gopay');
		$this->gopay_result = $this->Gopay->create($order);
		if (isset($_GET['render'])){
			pr($this->gopay_result);die();
		}
		//pr($gopay_res);die();
		
	}
	
	/**
	* orders
	*/
	
	public function orders(){
		if (!isset($_POST['user_code'])){
			$json = [
				//'user_code'=>104,
				'address'=>[
					'formatted_address'=>"Pelclova 2500\/5, Ostrava-město", 
					"street"=>"Pelclova", 
					"city"=>"Ostrava-město", 
					"cp"=>"2500\/5", 
					"lat"=> 49.8481683, 
					"lng"=> 18.2856251,
					//"id"=> 263583
				],
				'phone_number'=>'+420731956030',
				'first_name'=>'Jakub',
				'last_name'=>'Fastest',
				'payment_method'=>'cash',
				'server'=>'dev',
				'products'=>[
					['code'=>'001A'],
					['code'=>'004A'],
				],
				
				
			];
			$_POST = $json;
		}
		if (!isset($this->chatbot_payment_list[$_POST['payment_method']])){
			http_response_code(401);
			die(json_encode(['error_code'=>401,'error_message'=>$this->errorMessages[403]])); 
		}
		//pr($_POST);
		$save_products = [];
		$this->loadModel('Shop.ShopProducts');
		
		
		$products_list = $this->getProductsList();
		$total_price = 0;
		foreach($_POST['products'] AS $p){
				
				//pr($products_list);die();
				if (!isset($products_list[$p['code']])){
					http_response_code(401);
					die(json_encode(['error_code'=>401,'error_message'=>$this->errorMessages[404].' '.$p['code']])); 
				} else { 
					$findProd = $products_list[$p['code']];
				}
				
				//pr($products_list);
				$save_products[] = [
					'shop_product_id'=>$findProd['id'],
					'code'=>$findProd['code'],
					'credit'=>$findProd['credit'],
					'name'=>$findProd['name'],
					'count'=>1,
					'price'=>$findProd['price'],
					'price_vat'=>$findProd['price'],
					'price_per_item'=>$findProd['price'],
					'price_vat_per_item'=>$findProd['price'],
					'tax_vat'=>$findProd['tax_id'],
					'attributes_ids'=>[],
					'attributes_price'=>0,
				];
				$total_price += $findProd['price'];
		}
		$this->save_products = $save_products;
		
		$phone_pref = substr($_POST['phone_number'],0,-9);
		$phone_pref = strtr($phone_pref,['+'=>'00']);
		$phone = substr($_POST['phone_number'],-9,10);
		
		$user = $this->findUser($phone,$phone_pref,(isset($_POST['user_code'])?$_POST['user_code']:''));
		
		$address = $this->findAddress($_POST);
		$provoz_id = $this->findSystemId($address['lat'],$address['lng']);
		//pr($system_id);pr($address);die();
		
		if (isset($_POST['address']['id'])){
			$address_id = $_POST['address']['id'];
		} else {
			$adresa = $address;
			//$adresa = 
			//pr($address);die();
		}
		//pr($user);
		if (!$user){
			$user = [
				'first_name'=>$_POST['first_name'],
				'last_name'=>$_POST['last_name'],
				'email'=>$_POST['email'],
				'phone'=>$phone,
				'phone_pref'=>$phone_pref,
				'credits'=>0,
			];
			if (isset($adresa)){
				
				$user['user_addresses']=$adresa;
			}
		}
		
		$credits_from_order = $this->creditsFromOrder($total_price);
		if ($_POST['payment_method'] == 'credits'){
			if ($user['credits'] < $total_price){
				http_response_code(401);
				die(json_encode(['error_code'=>401,'error_message'=>$this->errorMessages[405]])); 
			} else {
				$credit_use = $total_price;
				$credit_use_before = $user['credits'];
				$credit_use_after = $user['credits'] - $total_price;
			}
		}
		$provoz_id = 1000;
		$save = array(
			'system_id'=>$this->system_id, 
			'shop_provoz_id'=>$provoz_id,
			//'shop_client_id'=>'', // todo
			//'user_address_id'=>'', // todo
			'shop_payment_id'=>$this->chatbot_payment_list[$_POST['payment_method']],
			'createdTime'=>strtotime(date('Y-m-d H:i:s')),
			'shop_order_items'=>$save_products,
			'credit_from_order'=>(isset($credits_from_order)?$credits_from_order:null),
			'credit_use_before'=>(isset($credit_use_before)?$credit_use_before:null),
			'credit_use_after'=>(isset($credit_use_after)?$credit_use_after:null),
			'price'=>$total_price,
			'price_vat'=>$total_price,
			/*
			'user'=>[
				'first_name'=>$user['first_name'],
				'last_name'=>$user['last_name'],
				'email'=>$user['email'],
				'phone_pref'=>$phone_pref,
				'phone'=>$phone,
				
			]
			*/
			
		);
		if (isset($user)){
			//pr($user);die();
			$save['users'] = $user;
		}
		//pr($save);die();
		
		
		
		$this->loadModel('Shop.ShopOrders');
		$save_data = $this->ShopOrders->newEntity($save,['associated' => ["Users"=>['validate' => 'OnlyCheckPass','accessibleFields'=>['id' => true]], "Users.UserAddresses","ShopOrderItems"]]);
		//pr($save_data);die();
		
		$resultSave = $this->ShopOrders->save($save_data);
		
		if (isset($resultSave) && $resultSave){
		
			if (isset($credit_use_after)){
			
			$this->Users->updateAll(
				['credits' => $credit_use_after + $credits_from_order], // fields
				['id' => $user['id']]
			);
			}
			//pr($resultSave);
			
			$this->ShopOrders->updateAll(
				[
					'shop_client_id' => $resultSave->users->id,
					'user_address_id' => $resultSave->users->user_addresses[0]->id
				
				], // fields
				['id' => $resultSave->id]
			);
			
			$result = [
				'code'=>$resultSave->id,
			];
		} else {
			http_response_code(401);
			die(json_encode(['error_code'=>406,'error_message'=>$this->errorMessages[406]])); 
			
		}
		
		
		
		die(json_encode($result));
	}
	
	private function creditsFromOrder($total_price){
		$credits_total = 0;
		if($total_price > 100){
			$credits_total = ($total_price/100)*1;				
		}
		if($total_price > 300){
			$credits_total = ($total_price/100)*3;				
		}
		if($total_price > 500){
			$credits_total = ($total_price/100)*6;	
						
		}
		$product_credits = 0;
		foreach($this->save_products AS $k=>$p){
			if (isset($p['credit'])){
				$product_credits += $p['credit'];
			}
		}
		
		$credits_total = ceil($credits_total+$product_credits);
		
		//pr($this->basket_products);die();
		return $credits_total;
	}
	
	
	private function findUser($phone,$phone_pref,$user_id=null){
		
		$this->loadModel('Users');
		if ($user_id){
			$conditions = [
				'id'=>$user_id,
			];
			//pr($conditions);
		} else {
			$conditions = [
				'phone'=>$phone,
				'phone_pref'=>$phone_pref,
			];
			
		}
		//pr($conditions);die('bb');
		$query = $this->Users->find()
		  ->where($conditions)
		  ->contain('UserAddresses')
		  ->select([
			
			'id',
			'first_name',
			'last_name',
			'email',
			'phone',
			'phone_pref',
			'reg',
			'credits',
			//'user_addresses',
			
		  ])
		  ;
		$user = $query->first();
		//pr($user);
		if ($user){
			return $user;
		} else {
			return false;
		}
		//pr($user);die('aaa');
	}
	
	private function findAddress($data){
		
		$this->loadModel('UserAddresses');
		if (isset($_POST['address']['id'])){
			$id = $_POST['address']['id'];
		} elseif (isset($_POST['user_code'])){
			$street = $_POST['address']['street'];
			$city = $_POST['address']['city'];
			$cp = $_POST['address']['cp'];
			$user_id = $_POST['user_code'];
		} else {
			$address = [
				'street'=>$_POST['address']['street'],
				'cp'=>$_POST['address']['cp'],
				'city'=>$_POST['address']['city'],
				'lat'=>$_POST['address']['lat'],
				'lng'=>$_POST['address']['lng'],
			];
			return $address;
		}
		if ($id){
			$conditions = [
				'id'=>$id
			];
		} else {
			$conditions = [
				'street'=>$street,
				'cp'=>$cp,
				'city'=>$city,
				'shop_client_id'=>$user_id,
			];
		}
		//pr($conditions);
		$query = $this->UserAddresses->find()
		  ->where($conditions)
		  ->select([
			
			//'user_addresses',
			
		  ])
		  ;
		$address = $query->first();
		return $address;
	}
	
	
	public function getProductsList(){
		$this->loadModel('Shop.ShopProducts');
		$mapper = function ($row, $key, $mapReduce) {
				// map group id from category
				$row->group_id = (isset($this->menu_data['shop_menu_items_group_id'][$row->shop_category_id])?$this->menu_data['shop_menu_items_group_id'][$row->shop_category_id]:'');
				
				
				// map varitanty
				if (isset($row->shop_product_variants) && !empty($row->shop_product_variants)){
					if (!isset($variants_list)){
						$variants_list = [];	
						$variants_select_list = [];	
					}
					foreach($row->shop_product_variants AS $var){
						$variants_list[$var->id] = $var;
						$variants_select_list[$var->id] = [
							'id'=>$var->id,
							'name'=>$var->name,
							'price'=>$var->price,
							'price_vat'=>$var->price_vat,
						];
					}
					$row->variants_list = $variants_list;
					$row->variants_select_list = $variants_select_list;
				}
				
				if (isset($row->shop_product_prices) && isset($row->shop_product_prices[0])){
						$row->price_vat  = $row->shop_product_prices[0]['price_vat'];
					
				} else {
					unset($row);
				}
				if (isset($row))
				$mapReduce->emit($row);
			};
		
		$query = $this->ShopProducts->find()
		  ->contain([
					'ShopProductVariants'=>function($q){
						$q->select(['id','shop_product_id','price_vat','code','name']);
						$q->where(['system_id'=>$this->system_id]);
						return $q;
					},
					'ShopProductPrices'=>function($q){
						//$q->select(['id','shop_product_id','price_vat','code','name']);
						$q->where(['system_id'=>$this->system_id,'status'=>1]);
						return $q;
					}
		  ])
		  ->where([])
		  ->select([
		  ])
		  ->mapReduce($mapper);
		  ;
		$data = $query->toArray();
		//print_r($data);
		$products_list = [];
			
		foreach($data AS $d){
			
			if (isset($d->shop_product_variants) && !empty($d->shop_product_variants)){
				foreach($d->shop_product_variants AS $v){
					$products_list[$v->code] = [
						//'id'=>$d->id.'v'.$v->id,
						'id'=>$d->id,
						'code'=>$v->code,
						'name'=>$d->name.' '.$v->name,
						'price'=>$v->price_vat,
						'tax_id'=>$d->tax_id,
						'credit'=>$d->credit,
					];
					
				}
			} else {
			
				$products_list[$d->code] = [
					'id'=>$d->id,
					'code'=>$d->code,
					'name'=>$d->name,
					'price'=>$d->price_vat,
				];
			}
		}
		//$products_list = array_merge($products_list,$this->cinajede(true));
		//$products_list = $products_list+$this->cinajede(true);
		//print_r($this->cinajede(true));

		return $products_list;
	
	}
	
	/**
	registrace
	*/
	
	public function register(){
		$json = '{"address":{"street":"Pelclova","city":"Ostrava-m\u011bsto","cp":"2500\/5","lat":49.8481683,"lng":18.2856251}}';
		$json = json_decode($json,true);
		if (!isset($_POST['address'])){
			$_POST['address'] = $json;
		}
		//pr($json);die();
		
		$first_name = (isset($_POST['first_name'])?$_POST['first_name']:'test');
		$last_name = (isset($_POST['last_name'])?$_POST['last_name']:'test');
		$email = (isset($_POST['email'])?$_POST['email']:'tajsn@gmail.com');
		$phone_number = (isset($_POST['phone_number'])?$_POST['phone_number']:'+420 111 222 333');
		$address = (isset($_POST['address'])?$_POST['address']:'');
		//pr($address);
		
		$this->loadModel('Users');
		$findUser = $this->Users->find()
			->where([
				'reg'=>1,
				'email'=>$email,
			])
			->select()
			->first();
			
		if ($findUser){
			http_response_code(400);
			die(json_encode(['error_code'=>'EXISTING_EMAIL','error_message'=>$this->errorMessages[402]])); 
		}
		
		
		$phone_pref = strtr(substr($phone_number,0,4),['+'=>'00']);
		$phone = strtr(substr($phone_number,4,20),[' '=>'']);
		$addresses = [[
				'street'=>$address['address']['street'],
				'city'=>$address['address']['city'],
				'cp'=>$address['address']['cp'],
				'lat'=>$address['address']['lat'],
				'lng'=>$address['address']['lng'],
			]];
		
		$saveUser = [
			'first_name'=>$first_name,
			'last_name'=>$last_name,
			'email'=>$email,
			'phone'=>$phone,
			'phone_pref'=>$phone_pref,
			'user_addresses'=>$addresses,
		];
		//pr($saveUser);
		$save_data = $this->Users->newEntity($saveUser,['associated' => ["UserAddresses"]]);
		//pr($save_data);die();
		if (!$resultDb = $this->Users->save($save_data)){
			http_response_code(400);
			die(json_encode(['error_code'=>401,'error_message'=>$this->errorMessages[401]])); 
		
		}
		
		$result = ['code'=>$resultDb->id];
		die(json_encode($result));
	}
	
	public function searchGoogleGps($data=null){
		//if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
		$data = strtr($data,['|'=>'/']);
		//pr($data);
		//}
		$cp = explode(' ',$data);
		//pr($cp);
        $prepAddr = str_replace(' ','+',$data);
        $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false&key=AIzaSyCa3rXQL2tniYzsafvWqIofD5ChB9fY5Dk');
        $results= json_decode($geocode,true);
        //$latitude = $output->results[0]->geometry->location->lat;
        //$longitude = $output->results[0]->geometry->location->lng;
		//pr($results);
		// parse address
		$parts = array(
		  'street'=>array('route','neighborhood'),
		  'city'=>array('administrative_area_level_2','locality'),
		  'state'=>array('country'),
		  'zip'=>array('postal_code'),
		  'co'=>array('premise'),
		  'cp'=>array('street_number'),
		);
		$address_out = array(
			'result'=>true,
		);
		//pr($this->request->query['debug']);
		//pr($results);
		
		
		//pr($results['results'][0]['address_components']);
		if (!empty($results['results'][0]['address_components'])) {
		  $ac = $results['results'][0]['address_components'];
		  //pr($ac);
		  foreach($parts as $need=>&$types) {
			foreach($ac as &$a) {
			  if (in_array($a['types'][0],$types)) {
				  //pr($need);
				  //pr($address_out);
				  if (!isset($address_out[$need]) || empty($address_out[$need]))
				  $address_out[$need] = $a['short_name'];
			  }
			  elseif (empty($address_out[$need])) $address_out[$need] = '';
			}
		  }
		  if (empty($address_out['street'])){ 
			  $address_out['street'] = $address_out['city'];
		  }
		  $address_out['lng'] = $results['results'][0]['geometry']['location']['lng'];
		  $address_out['lat'] = $results['results'][0]['geometry']['location']['lat'];
		} else {
			http_response_code(202);
			die(json_encode(['error_code'=>'DOES_NOT_EXIST','error_message'=>$this->errorMessages[409]])); 
		}
		//pr($address_out);
		if (empty($address_out['cp']) && empty($address_out['co'])){
			if (isset($cp[1]) && !isset($cp[2])){
				//if (is_int($cp[1])){
					$address_out['cp'] = $cp[1];
				//}
			}
			if (isset($cp[1]) && isset($cp[2])){
				//if (is_int($cp[1])){
					$address_out['cp'] = $cp[2];
				//}
			}
		}
		if (empty($address_out['cp']) && !empty($address_out['co'])){
			$address_out['cp'] = $address_out['co'];
			$address_out['co'] = '';
		}
		
		if ($cp[0] == 'Poličná' || $cp[0] == 'policna' || $cp[0] == 'Policna' || $cp[0] == 'polična'){
			$address_out['street'] = 'Poličná';
		}
		//pr($cp);
		
		if (isset($this->request->query['debug'])){
			pr($address_out);
			pr($results);
			
			die();
		}
		
		return($address_out);
		//die(json_encode(['r'=>true,'data'=>$address_out]));
    }
	
	/**
	validace adresy
	*/
	private function validateAddress($id=null,$value){
		$this->loadModel('UserAddresses');
			$match = preg_match("/(.*?) ((\d+)\/)?(\d+), (.*)/", $value, $matches, PREG_OFFSET_CAPTURE);
			$adr = [];
			if ($match == 1){
				$adr['street']= $matches[1][0];
				$adr['cp'] = $matches[3][0];
				if (!empty($matches[3][0])){
					$adr['co'] = $matches[4][0];
				} else {
					$adr['cp'] = $matches[4][0];
					$adr['co'] = '';
				}
				$adr['street'] = $adr['street'].' '.$adr['cp'].(!empty($adr['co'])?'/'.$adr['co']:'');
				$adr['city'] = $matches[5][0];
			}
			//pr($adr);
			//pr($matches);
			if ($id){
			
			$address = $this->UserAddresses->find()
				->where([
					'shop_client_id'=>$id,
					'street'=>$adr['street'],
					'city'=>$adr['city'],
				])
				->select()
				->first();
			}
			if(!isset($address)){
				//pr($value);
				$address = $this->searchGoogleGps($value);
				//pr('google');
				//pr($address);
			
			}
			
			return $address;
	}
	
	
	/**
	validace adresy
	*/
	
	public function address($type=null){
		//if (!isset($_POST['address'])) $_POST['address'] = 'pelclova 2500/5, ostrava';
		if (!isset($_POST['address'])){
                    $_POST['address'] = 'Pelclova 5, Ostrava';
                }
		$adr = $_POST['address'];
               
		$address = $this->validateAddress(null,$adr);
		//die(json_encode($address));
		
		$formatted_address = $address['street'].' '.(!empty($address['co'])?$address['co'].'/':'') .$address['cp'].', '.$address['city'];
                //($adr)
                @file_put_contents(ROOT . '/tmp/chatbot_address.log', date('Y-m-d H:i:s') . ' - '. $_SERVER['REMOTE_ADDR'].': ' . $adr . ' | '. $formatted_address . "\r\n", FILE_APPEND);
		$provoz_id = $this->findSystemId((float)$address['lat'],(float)$address['lng']);
                @file_put_contents(ROOT . '/tmp/chatbot_address.log', date('Y-m-d H:i:s') . ' - '. $_SERVER['REMOTE_ADDR'].': result = ' . $provoz_id . "\r\n",  FILE_APPEND);
               
                //die(json_encode([$provoz_id, $address['lat'],$address['lng']]));
		if (!$type){
			$result = [
				'branch_code'=>$provoz_id,
				'formatted_address'=>$formatted_address,
				'address'=>[
					'street'=>$address['street'],
					'city'=>$address['city'],
					'cp'=>(!empty($address['co'])?$address['co'].'/':'') .$address['cp'],
					'lat'=>$address['lat'],
					'lng'=>$address['lng'],
				]
					
			];
			
		} else {
			if ($provoz_id > 0){
				
				$result = [
					'branch_code'=>$provoz_id,
				];
			} else {
				http_response_code(401);
				die(json_encode(['error_code'=>408,'error_message'=>$this->errorMessages[408]])); 
			}
		}
			
		if (isset($this->request->query['debug'])){
			pr($result);die();
		}
		die(json_encode($result));
	}
	
	
	/**
	zakaznik
	*/
	public function users($id=null,$type=null,$value=null){
		//if (!isset($_POST['address'])) $_POST['address'] = 'pelclova 2500/5, ostrava';
		//if (!isset($_POST['phone_number'])) $_POST['phone_number'] = '+420731956030';
		
		if (isset($_POST['phone_number'])) $value = $_POST['phone_number'];
		if (isset($_POST['address'])) $value = $_POST['address'];
		//print_r('a');
		//print_r($_POST);
		
		//die();
		// adresa
		if ($type == null){
			$this->loadModel('Users');
			$user = $this->Users->find()
				->contain(['UserAddresses'=>function($q){
                    return $q->select(['id','shop_client_id','street','cp','city','lat','lng']);
                }])
				->where([
					'id'=>$id,
				])
				->select([
					'id',
					'first_name',
					'last_name',
					'phone',
					'phone_pref',
					'credits',
				])
				->first();
			//pr($user);	
			
			if (!$user){
				http_response_code(400);
				die(json_encode(['error_code'=>404,'error_message'=>$this->errorMessages[404]]));
			}
			
			$formatted_address = [];
			$address_list = [];
			foreach($user->user_addresses AS $adr){
				//pr($adr)
				if (!isset($lat)){
					$lat = $adr->lat;
					$lng = $adr->lng;
				}
				$formatted_address = $adr->street.' '.$adr->cp.', '.$adr->city;
				$address_list[] = [
					'address' => $formatted_address,
					'code' => $adr->id,
				];
			}
			//pr($lat);
			$user->phone_pref = strtr($user->phone_pref,['00'=>'+']);
			$provoz_id = $this->findSystemId($lat,$lng);
			$result = [
				'branch'=>$provoz_id,
				'addresses'=>$address_list,
				'phone_number'=>$user->phone_pref.$user->phone,
				'credits'=>$user->credits,
				'first_name'=>$user->first_name,
				'last_name'=>$user->last_name,
			];
			
			if (isset($this->request->query['debug'])){
				pr($result);die();
			}
			die(json_encode($result));
		
		}
		
		// adresa
		if ($type == 'address'){
			//pr($value);
			$address = $this->validateAddress($id,$value);
			
			$findExist = $this->UserAddresses->find()
				->where([
					'shop_client_id'=>$id,
					'street'=>$address['street'],
					'city'=>$address['city'],
				])
				->select(['id'])
				->first();
			//pr($findExist);die();
			$saveAddress = 	[
				'shop_address_type_id'=>$id,
				'shop_client_id'=>$id,
				'street'=>$address['street'],
				'city'=>$address['city'],
				'lat'=>$address['lat'],
				'lng'=>$address['lng'],
				'cp'=>(!empty($address['co'])?$address['co'].'/':'').$address['cp'],
				'co'=>'',
				'zip'=>$address['zip'],
			];
			
			if($findExist){
				$saveAddress['id'] = $findExist->id;
			}
			
			$save_data = $this->UserAddresses->newEntity($saveAddress);
			//pr($save_data);die();
			if (!$resultDb = $this->UserAddresses->save($save_data)){
				http_response_code(400);
				die(json_encode(['error_code'=>401,'error_message'=>$this->errorMessages[401]]));
			}
			$result = [
				'formatted_address'=>$save_data->street.' '.$save_data->cp.(($save_data->co)?'/'.$save_data->co:'').', '.$save_data->city,
				'address_id'=>$resultDb->id,
			];
			//pr($address);
			//pr($saveAddress);
			die(json_encode($result));
		}
		
		// telefon
		if ($type == 'phone_number'){
			//pr($value);	
			$phonePref = substr($value,0,-9);
			$phone = substr($value,strlen($phonePref),20);
			$phonePref = strtr($phonePref,['+'=>'00',' '=>'00']);
			//pr(substr($phonePref,0,2));
			if (substr($phonePref,0,1) != '0'){
				$phonePref = '00'.$phonePref;
			}
			//pr($phonePref);die();
			
			//pr($phone);die();
			$this->loadModel('Users');
			$find = $this->Users->find()
				->where(['id'=>$id])
				->select(['id','first_name'])
				->first();
			if (!$find){
				http_response_code(400);
				die(json_encode(['error_code'=>400,'error_message'=>$this->errorMessages[404]]));
					
			} else {
				//pr($phone);
				//pr($phonePref);
				//pr($id);
				$result = $this->Users->updateAll(
				   array("phone" => $phone,'phone_pref'=>$phonePref),
				   array("id" => $id) 
				);
				//pr($result);
				if (!$result){
					http_response_code(202);
					die(json_encode(['error_code'=>202,'error_message'=>'PHONE_NUMBER_ALREADY_CHANGED']));
			
				} else {
					die('OK');
				}
			}
			
			
			}
		die();
			
	}
	
	private function searchCat($cat_id,$typeList){
		$this->result = null;
		foreach($typeList AS $k=>$val){
			if (is_array($val)){
				if (in_array($cat_id,$val)){
					$this->result = $k;
					
				}
				return $this->result;
			} else {
				$this->result = array_search($cat_id,$typeList);
			}
		}
		return $this->result;
	}
	
	/**
	oveviraci doba
	*/
	public function carousel($type){
		$this->loadModel('Shop.ShopProducts');
		$this->typeList = [
			"pizza"=>[1,2,3,4,5],
			"burger"=>9, 
			"china"=>10, 
			"strips"=>6, 
			"specle"=>11, 
			"drink"=>8, 
			"side_dish"=>7,
		];
		$this->convertArray = [
			'name'=>'title',
			'price_vat'=>'prices',
			'description'=>'subtitle',
			'images'=>'image_url',
			'code'=>'codes',
		];
		
		
		$mapper = function ($row, $key, $mapReduce) {
			//$row->size = $row->gramaz;
			unset($row->gramaz);
			$codes = [];
			$prices = [];
			//pr($row->codes);die();
			if (isset($row->shop_product_variants) && !empty(isset($row->shop_product_variants))){
				foreach($row->shop_product_variants AS $var){
					//pr($row);die();
					$key = $this->searchCat($row->shop_category_id,$this->typeList);
					//pr($key);die();
					if ($key != false){
						//pr('a'.$key);
						if ($key == 'china' || $key == 'pizza'){
							
							$codes[(strpos(strtoupper($var->code),'B')>0?'big':'small')] = $var->code;
							$prices[(strpos(strtoupper($var->code),'B')>0?'big':'small')] = $var->price_vat;
						}
						else if ($key == 'strips'){
						//	pr($var);
							$codes[(strpos(strtoupper($var->code),'B')>0?'spicy':'nonspicy')] = $var->code;
							$prices[(strpos(strtoupper($var->code),'B')>0?'spicy':'nonspicy')] = $var->price_vat;
							
						}
					}
					//pr($codes);
					//pr($this->typeList);die();
					//if ()
					//$codes[] = $var->code;
				}
				unset($row->shop_product_variants);
				if (!empty($codes)){
					$row->code = $codes;	
				}
				if (!empty($prices)){
					$row->price_vat = $prices;	
				}
					
				
			}
			//pr($this->findVariant);die('a');
			if (!empty($row->images)){
				$row->images = $this->getImage($row->images);
			}
			
			$mapReduce->emit($row);
		};
		
		$conditions = ['shop_category_id IN'=>$this->typeList[$type]];
		
		$products = $this->ShopProducts->find()
			->contain(['ShopProductVariants'=> function($q){
			        $q->where(['system_id'=>$this->system_id]);
					return $q->select(['shop_product_id','name','price_vat','code']);
            }])
			->where($conditions)
			->select([
				'id',
				'name',
				'description',
				'images',
				'code',
				'price_vat',
				'shop_category_id',
			])
			->mapReduce($mapper)
			->toArray();
			foreach($products AS $k=>$prod){
				
				$products[$k] = $this->convertEntity($prod);
			}
		if (isset($this->request->query['debug'])){
			pr($products);die();
		}	
		//pr($products);die();
		if (!$products){
			http_response_code(400);
			die(json_encode(['error_code'=>400,'error_message'=>$this->errorMessages[404]]));
			
		} else {
			die(json_encode($products));
		}
		
	}
	
	/**
	oveviraci doba
	*/
	public function branches($id,$type){
		if ($type == 'openhours'){
			$this->loadModel('Shop.ShopProvozs');
			$provoz = $this->ShopProvozs->find()
				->select(['id','name','doba'])
				->first();
			//pr($provoz);
			
			if (!$provoz){
				http_response_code(400);
				die(json_encode(['error_code'=>400,'error_message'=>$this->errorMessages[404]]));
			
			} else {
				$doba = explode("\n",$provoz->doba);
				$list = [
					'monday'=>[],
					'tuesday'=>[],
					'wednesday'=>[],
					'thursday'=>[],
					'friday'=>[],
					'saturday'=>[],
					'sunday'=>[]
				];
				$listCon = [
					0=>'monday',
					1=>'tuesday',
					2=>'wednesday',
					3=>'thursday',
					4=>'friday',
					5=>'saturday',
					6=>'sunday'
				];
				//pr($list);
				//pr($doba);
				foreach($doba AS $k=>$d){
					$time = explode(' - ',$d);
				//	pr($time);
					$list[$listCon[$k]] = ['from'=>$time[0],'to'=>$time[1]];
				}
				//pr($list);
				die(json_encode($list));
				
			}
		}
	}
	
	/**
	cena rozvozu
	*/
	public function deliveryPrice(){
		die(json_encode(['delivery_price'=>19]));
	}
	
	/**
	min cena
	*/
	public function minimumPrice(){
		die(json_encode(['minimum_price'=>119]));
	}
	
	private function genJwt(){
		require_once('Component/JwtComponent.php');
		$token = array();
		$token['id'] = 'chachar159';
		$hashToken = \JWT::encode($token, 'secret_server_key');
		//$decodeToken = \JWT::decode($_POST['token'], 'secret_server_key');
		$decodeToken = \JWT::decode($hashToken, 'secret_server_key');
		pr($decodeToken);
		

	}
	
	/**
	get image
	*/
	private function getImage($images){
		$url = 'http://'.$_SERVER['HTTP_HOST'].'/uploaded/products/';
		$images = json_decode($images);
		$image = ($url.$images[0]->file);
		
		return $image;
		
	}
	
	/**
	conver to chatbot entity
	*/
	private function convertEntity($product){
		if ($product)
		foreach($this->convertArray AS $current=>$new){
			$product->$new = $product->$current;
			unset($product->$current);
			unset($product->_locale);
		}
		return $product;
	}
	
	/**
	get products
	*/
	public function products($code=null){
		
		$this->loadModel('Shop.ShopProducts');
		
		$this->convertArray = [
			'name'=>'title',
			'price_vat'=>'price',
			'description'=>'subtitle',
			'images'=>'image_url',
		];
		
		$this->loadModel('ShopProductVariants');
		$this->findVariant = $this->ShopProductVariants->find()
			->select(['id','shop_product_id','price_vat','code'])
			->where(['code'=>$code,'system_id'=>$this->system_id])
			->first();
			
		if ($this->findVariant){
			$conditions = ['ShopProducts.id'=>$this->findVariant->shop_product_id];
		} else {
			if ($code){
				$conditions = ['ShopProducts.code'=>$code];
				
			} else {
				$conditions = [];
		
			}
		}
		//pr($conditions);
		//pr($this->findVariant);
		$mapper = function ($row, $key, $mapReduce) {
			//$row->size = $row->gramaz;
			unset($row->gramaz);
			if ($this->findVariant){
				$row->price_vat = $this->findVariant->price_vat;
				$row->code = $this->findVariant->code;
			}
			//pr($this->findVariant);die('a');
			if (!empty($row->images)){
				$row->images = $this->getImage($row->images);
			}
			
			$mapReduce->emit($row);
		};
		
		
		//pr($this->system_id);	
		//pr($findVariant);die();
		
		$query = $this->ShopProducts->find()
			/*
			->contain(['ShopProductVariants'=> function($q) use ($code){
			        $q->where(['code'=>$code]);
					return $q->select(['shop_product_id','name','price_vat','code']);
            }])
			*/
			->where($conditions)
			->select([
			
				'ShopProducts.id',
				'ShopProducts.name',
				'ShopProducts.price_vat',
				'ShopProducts.description',
				'ShopProducts.images',
				'ShopProducts.gramaz',
				'ShopProducts.shop_category_id',
			
			])
			//->limit(2)
			->mapReduce($mapper);
		
		if (!$this->findVariant){
			$query ->contain(['ShopProductVariants'=> function($q){
			        $q->where(['system_id'=>$this->system_id]);
					return $q->select(['shop_product_id','name','price_vat','code']);
                        }]);
			
		}
		
		
		if ($code){
			$product = $query->first();
			unset($product->id);
		} else {
			$product = $query->toArray();
			//pr($product);die();
			$product = $this->createVariants($product);
		}
		$pos = strpos($product->code,'A');
		if ($pos === false){
			$variant = ($product->shop_category_id == 6)?'pikant':'big';
		} else {
			$variant = ($product->shop_category_id == 6)?'nonpikant':'small';
		
		}
		unset($product->shop_category_id);
		$product->variant = $variant;
		// prevod na chatbot entity
		if ($code){
			$product = $this->convertEntity($product);
		} else {
			foreach($product AS $k=>$prod){
				
				$product[$k] = $this->convertEntity($prod);
			}
		}
		
		if (isset($this->request->query['debug'])){
			pr($product);die();
		}
		if ($product){
			die(json_encode($product));
		} else {
			http_response_code(400);
			die(json_encode(['error_code'=>400,'error_message'=>$this->errorMessages[404]]));
			
		}
	}
	
	/**
	create variants list
	*/
	private function createVariants($product){
		foreach($product AS $k=>$prod){
			
			if (!empty($prod->shop_product_variants)){
				//die('a');
				$i = 0;
				foreach ($prod->shop_product_variants AS $v){
					if (!isset($product[$k]['variant'])){
						$product[$k]['variant'] = [];
					}
					$product[$k]['variant'][] = $v->name;
					if ($i == 0){
						$product[$k]->code = $v->code;
						$product[$k]->price_vat = $v->price_vat;
						//pr($v);die();
					} else {
						$new = clone $product[$k];
						$new->code = $v->code;
						$new->price_vat = $v->price_vat;
						//unset($new->shop_product_variants);
						$product[] = $new;
					}
					$i++;
				}
				unset($prod->shop_product_variants);
			}
		}
		unset($prod->id);
		
		return $product;
	}
	
	/**
	seznam provozoven
	*/
	public function provoz(){
		$this->loadModel('Shop.ShopProvozs');
		$provoz_list_all = $this->ShopProvozs->provozListAll(true);
		$provoz_all = [];
		//pr($provoz_list_all);
			
		
		//pr($provoz_all);
		unset($provoz_list_all['provoz_list_id']['']);
		unset($provoz_list_all['provoz_list_id'][1000]);
		
		$provoz = [];
		$i = 1;
		foreach($provoz_list_all['provoz_list_id'] AS $id=>$p){
			if (isset($this->request->query['poradi'])){
				$provoz[]['RozvozProvoz'] = [
					'id'=>$id,
					'name'=>$p,
					'poradi'=>$i,
				];
				
			} else {
				$provoz[$id] = $p;
				
			}
			/*
			*/
			$i++;
		}
		die(json_encode($provoz));
	}
	
	
	

	 
	
}
