<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\I18n\Time;
use lessc\lessc;
class ArticlesController extends AppController
{
	var $article_fields = [
		'name',
		'alias',
		'id',
		'created',
		'content', 
	];

    public function homepage(){
        $this->set('title',__('Prim hodinky'));
		
    }
	
	
	
	
	// article index
    public function index(){
        $path = func_get_args();
		$this->menu_data = $this->Articles->MenuItems($path,$this->menu_items);
		
		// pokud nenalezeno
		if (count($this->menu_data['ids']) == 0){
			return $this->error404();
		}
		
		// pokud nenalezeno 1 clanek presmeruj na detail
		if (count($this->menu_data['ids']) == 1){
			return $this->detail('detai',$this->menu_data['ids'][0]);
		}
		
		$conditions = ['Articles.status'=>1];
		$conditions['Articles.id IN'] = $this->menu_data['ids'];
		
		// find articles
		$articles = $this->Articles->find()
			->where($conditions)
			->select($this->article_fields)
			->order('id DESC')
			->cache(function ($articles) {
				return 'articles-' . md5(serialize($articles->clause('where')).serialize($articles->clause('order')));
			});
		;
		$items = $this->paginate($articles)->toArray();
		
		if (isset($items) && count($items)>0){
		
			$this->set('items',$items);
		} else {
			return $this->error404();
		}
		$this->set("breadcrumb",$this->menu_data['breadcrumb']);
		$this->menu_data['breadcrumb'] = reset($this->menu_data['breadcrumb']);
		$this->set("title", $this->menu_data['breadcrumb']);
		
		if ($this->request->is("ajax")){
			$this->viewBuilder()->autoLayout(false);
			$this->render('items');
		} else {
			$this->render('index');
		}
    }
	
	
	// load detail article
	public function detail($alias=null,$id=null){
		$conditions = [
			'id'=>$id,
			'status'=>1,
		];
		// find articles
		$article = $this->Articles->find()
			->where($conditions)
			->cache(function ($article) {
				return 'articles_id-' . md5(serialize($article->clause('where')).serialize($article->clause('order')));
			});
		;
		$article = $article->first();
		
		
		
		if (!$article){
			return $this->error404();
		}
		
		// seo text
		$seo = [
			'title'=>$article->name,
			'description'=>(!empty($article->seo_description)?$article->seo_description:$article->content),
			'keywords'=>(!empty($article->seo_keywords)?$article->seo_keywords:''),
		];
		if (!empty($product->images)){
			$images = json_decode($article->images,true);
			if (isset($images[0]['file'])){
				$seo['img'] = '/uploaded/articles/'.$images[0]['file'];
			}
		}
		$this->set('seo',$seo);
		
		$this->set("title", $article->name);
		$this->set('article',$article);
		$this->render('detail');
	}
	
	
	

	 
	
}
