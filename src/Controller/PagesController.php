<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\I18n\Time;
use lessc\lessc;
use Cake\Cache\Cache;


/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class PagesController extends AppController
{

    public function homepage(){
        $this->set('title',__('Prim hodinky'));
		
    } 
	
	public function short(){
        $this->loadComponent('GoogleShort');
		// Create instance with key
		$key = 'AIzaSyBL5F9sS-Uaatuh8lCmVErPkEUzllGlN_0';
		$googer = $this->GoogleShort->GoogleURLAPI($key);
		$shortDWName = $this->GoogleShort->shorten("https://davidwalsh.name");
		echo $shortDWName; // returns http://goo.gl/i002

		// Test: Expand a URL
		//$longDWName = $this->GoogleShort->expand($shortDWName);
		//echo $longDWName; // returns https://davidwalsh.name
		die('a');
    } 
	
	public function intranet(){
        $this->redirect('http://intrchachar.fastest.cz/intranet/');
    }
	
	public function checkOrdersLogout(){
		$this->Session->delete('sklad_user');
		//die();
		$this->redirect('/check_orders/?logout');
	}
	
	public function checkOrdersLogin(){
		if ($this->request->is('ajax')){
			$this->loadModel('SkladUsers');
			$sklad_user = $this->SkladUsers->find()
			->where([
				'username'=>$this->request->data['user'],
				'password'=>md5($this->request->data['pass']),
			])
			->select([
			])
			->first();
			if (!$sklad_user){
				die(json_encode(['r'=>false,'m'=>'Chyba přihlášení']));
			} else {
				$sklad_user_data = [
					'id'=>$sklad_user->id,
					'name'=>$sklad_user->name,
				];
				//pr($sklad_user_data);
				$this->Session->write('sklad_user',$sklad_user_data);
				die(json_encode(['r'=>true,'m'=>'Přihlášení v pořádku']));
				
			}
		} else {
			die('');
		}
	}
	
	public function checkSkladUser(){
		if ($this->Session->check('sklad_user')){
			$this->set('skladUser',$this->skladUser = $this->Session->read('sklad_user'));
			//pr($this->Session->read('sklad_user'));
		} else {
			//die('neprihlasen');
		}
	}
	
	// nalezeni zakaznika pro upravu kreditu
	public function findClient(){
		$this->checkSkladUser();
		if (!empty($this->request->data['client_id'])){
			$this->saveClientCredits();
		}
		//pr($this->request->data);
		$this->loadModel('Shop.ShopClients');
		$client = $this->ShopClients->find()
			->where([
				'OR'=>[
					'id'=>$this->request->data['telefon_client'],
					'phone'=>$this->request->data['telefon_client'],
					'email'=>$this->request->data['telefon_client'],
				]])
			->select([
			])
			->first();
		if ($client){
			die(json_encode(['r'=>true,'data'=>$client]));
		} else {
			
			die(json_encode(['r'=>false,'m'=>'Zákazník nenalezen']));
		}
	}
	
	// ulozeni kreditu
	public function saveClientCredits(){
		$this->checkSkladUser();
		$this->loadModel('Shop.ShopClients');
		$client = $this->ShopClients->find()
			->where(['id'=>$this->request->data['client_id']])
			->select([
				'id',
				'credits',
			])
			->first();
		if ($client){
			//pr($this->request->data['client_credits']);	
			//pr($client['credits']);	
			$credits_add = $this->request->data['client_credits'] - $client['credits'];
			if ($credits_add > 0){
				$this->checkCreditAdd($credits_add,$client['credits']);
				
			} else {
				$this->loadModel('CreditLogs');
				$save = array(
					'provoz_id'=>$this->skladUser['id'],
					'credits_add'=>$credits_add,
					'shop_client_id'=>$this->request->data['client_id'],
					'month'=>date('m'),
					'year'=>date('Y'),
				);
				//pr($save);die();
				$save_data = $this->CreditLogs->newEntity($save);
				//pr($save_data);
				$this->CreditLogs->save($save_data);
			}
			
			//pr($client['credits'] + $credits_add);die();
			$this->loadModel('Shop.ShopClients');
			$this->ShopClients->updateAll(
				['credits' => $client['credits'] + $credits_add], // fields
				['id' => $this->request->data['client_id']]
			);
			//pr($credits_add);
			die(json_encode(['r'=>true,'m'=>'Uloženo']));
		} else {
			
			die(json_encode(['r'=>false,'m'=>'Zákazník nenalezen']));
		}
	}
	
	private function checkCreditAdd($credits_add){
		$this->loadModel('CreditLogs');
		$credits = $this->CreditLogs->find()
			->where(['provoz_id'=>$this->skladUser['id'],'month'=>date('m'),'year'=>date('Y'),'credits_add >'=>0])
			->select([
				'credits_add'
			])
			;
		$sum = $credits->sumOf('credits_add'); 
		//pr($sum);
		if ($this->skladUser['id'] == 48){
			$credit_limit = 1000000;
		
		} else {
			$credit_limit = 500000;
			
		}
		if ($sum+$credits_add > $credit_limit){
			die(json_encode(['r'=>false,'m'=>'Nemáte oprávnění přídávat více kreditů']));
		} else {
			$save = array(
				'provoz_id'=>$this->skladUser['id'],
				'credits_add'=>$credits_add,
				'shop_client_id'=>$this->request->data['client_id'],
				'month'=>date('m'),
				'year'=>date('Y'),
			);
			//pr($save);die();
			$save_data = $this->CreditLogs->newEntity($save);
			//pr($save_data);
			$this->CreditLogs->save($save_data);
			
		}
		//pr($sum);
	}
	
	// kontrola objednavek zakaznika
	public function checkOrders(){
		$this->checkSkladUser();
		header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
		header("Cache-Control: post-check=0, pre-check=0", false);
		header("Pragma: no-cache");
		//pr($sklad_users);
		
		if (!empty($this->request->data)){
			//pr($this->request->data);
			$this->loadModel('Shop.ShopOrders');
			
			$this->loadModel('Shop.ShopProvozs');
			$this->set('pokladna_list',$this->pokladna_list = $this->ShopProvozs->pokladnaListAll());
			
			$conditions = [
				'OR'=>[
					'ShopOrders.shop_client_id'=>$this->request->data['telefon'],
					'ShopOrders.client_telefon'=>$this->request->data['telefon'],
					'ShopOrders.id'=>$this->request->data['telefon'],
				]
			];
			
			$orders = $this->ShopOrders->find()
			->contain(['Users'])
			->where($conditions)
			->select([
			])
			->order('ShopOrders.id DESC')
			->limit(20)
			->toArray();
			//pr($this->pokladna_list);
			//pr($orders);
			if (!empty($orders)){
				$this->set('orders',$orders);
			} else {
				$this->set('noresult',true);
			}
			//pr($orders);
		}
    }
	
	public function ips(){
		print_r($_SERVER);
		die();
	}
	
	public function searchGoogleGps($data=null){
		//if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
		$data = strtr($data,['|'=>'/']);
		//pr($data);
		//}
		$cp = explode(' ',$data);
		$match = preg_match("/(.*?) ((\d+)\/)?(\d+), (.*)/", $data, $matches, PREG_OFFSET_CAPTURE);
		$adr = [];
			if ($match == 1){
				$adr['street']= $matches[1][0];
				$adr['cp'] = $matches[3][0];
				if (!empty($matches[3][0])){
					$adr['co'] = $matches[4][0];
				} else {
					$adr['cp'] = $matches[4][0];
					$adr['co'] = '';
				}
				$adr['street'] = $adr['street'].' '.$adr['cp'].(!empty($adr['co'])?'/'.$adr['co']:'');
				$adr['city'] = $matches[5][0];
			}
			
		//pr($adr);	
		//pr($cp);
        $prepAddr = str_replace(' ','+',$data);
        $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false&key=AIzaSyCa3rXQL2tniYzsafvWqIofD5ChB9fY5Dk');
        $results= json_decode($geocode,true);
        //$latitude = $output->results[0]->geometry->location->lat;
        //$longitude = $output->results[0]->geometry->location->lng;
		
		// parse address
		$parts = array(
		  'street'=>array('route','neighborhood'),
		  'city'=>array('administrative_area_level_2','locality'),
		  'state'=>array('country'),
		  'zip'=>array('postal_code'),
		  'co'=>array('premise'),
		  'cp'=>array('street_number'),
		);
		$address_out = array(
			'result'=>true,
		);
		//pr($this->request->query['debug']);
		//pr($results);
		
		
		
		if (!empty($results['results'][0]['address_components'])) {
		  $ac = $results['results'][0]['address_components'];
		  //pr($ac);
		  foreach($ac as $k=>&$a) {
			  //pr($a['short_name']);
			if ($a['types'][0] == 'premise' && !is_numeric($a['short_name'])){
				unset($ac[$k]);
				//pr($a['short_name']);
			}
			
		  }
		  //pr($ac);
		  
		  
		  foreach($parts as $need=>&$types) {
			foreach($ac as &$a) {
			  //pr($a['types'][0]);
			  //pr($a['short_name']);
					
			  if (in_array($a['types'][0],$types)) {
				  //pr($need);
				  //pr($a);
				  if (!isset($address_out[$need]) || empty($address_out[$need])){
					$address_out[$need] = $a['short_name'];
					
				  }
			  }
			  elseif (empty($address_out[$need])) $address_out[$need] = '';
			}
		  }
		  //pr($address_out);
		  if (empty($address_out['street'])){ 
			  $address_out['street'] = $address_out['city'];
		  }
		  $address_out['lng'] = $results['results'][0]['geometry']['location']['lng'];
		  $address_out['lat'] = $results['results'][0]['geometry']['location']['lat'];
		} else {
			die(json_encode(['result'=>false,'message'=>__('Adresa nenalezena'),'adr'=>$adr])); 
		}
		if (empty($address_out['cp']) && empty($address_out['co'])){
			//pr($cp);
			if (isset($cp[1]) && !isset($cp[2])){
				//if (is_int($cp[1])){
					$address_out['cp'] = $cp[1];
				//}
			}
			if (isset($cp[1]) && isset($cp[2])){
				$cp[1] = rtrim($cp[1],',');
				$cp[2] = trim($cp[2],',');
				//pr($cp[1]);
				if (is_int($cp[1])){
					$address_out['cp'] = $adr['cp'];
				} else {
					$address_out['cp'] = (isset($adr['cp'])?$adr['cp']:'');
					
				}
			}
		}
		if (empty($address_out['cp']) && !empty($address_out['co'])){
			$address_out['cp'] = $address_out['co'];
			$address_out['co'] = '';
		}
		
		if ($cp[0] == 'Poličná' || $cp[0] == 'policna' || $cp[0] == 'Policna' || $cp[0] == 'polična'){
			$address_out['street'] = 'Poličná';
		}
		//pr($cp);
		
		if (isset($this->request->query['debug'])){
			pr($address_out);
			pr($results);
			
			die();
		}
		
		//pr($address_out);
		die(json_encode(['r'=>true,'data'=>$address_out]));
    }
	
	
	
	
	public function loadIntro(){
		$this->viewBuilder()->autoLayout(false);
		
		$this->mapAreasList();
		$redirect = [];
		//pr($this->pokladna_list);
		if (isset($this->pokladna_list['data']))
		foreach($this->pokladna_list['data'] AS $d){
			if (isset($d->system_id) && isset($this->redirect_system_ids[$d->system_id])){
				$redirect[$d->id] = $this->redirect_system_ids[$d->system_id];
			}
		}
		$result = [
			'redirect'=>$redirect,
			'map_coords_search'=>$this->map_area_list_search,
		];
		die(json_encode($result));
		
	}
	public function systemList(){
		pr($this->system_list);
		die('');
	}
	
	private function mapAreasList(){
		
		$this->loadModel('Shop.ShopProvozs');
		$this->set('pokladna_list',$this->pokladna_list = $this->ShopProvozs->pokladnaListAll());
		
		$this->loadModel('Shop.ShopMapAreas');
		$map_area_list = $this->ShopMapAreas->mapAreaList();
		$this->map_area_list_search = [];
		//pr($this->pokladna_list['provoz_pokladna_id']);
		//pr($map_area_list);
		
		foreach($map_area_list AS $area_id=>$d){
			//pr($area_id);
				
				if (isset($this->pokladna_list['provoz_pokladna_id'][$area_id]) && $area_id != 1000){
					$this->map_area_list_search[$this->pokladna_list['provoz_pokladna_id'][$area_id]] = $d;
				}
				
			
		}
		//pr($this->pokladna_list['provoz_system_id']);
		$this->set('map_coords_search',json_encode($this->map_area_list_search));
		//pr($this->map_area_list_search);
		
	}
	
	
	public function clearCache(){
        Cache::clear(false);
		die('cache smazana');
    }
	
	
	
	public function searchAddress(){
		
		die(json_encode(['r'=>true]));
	}
	
	
    /**
     * Displays a view
     *
     * @return void|\Cake\Network\Response
     * @throws \Cake\Network\Exception\NotFoundException When the view file could not
     *   be found or \Cake\View\Exception\MissingTemplateException in debug mode.
     */
    public function display()
    {
        $path = func_get_args();

        $count = count($path);
        if (!$count) {
            return $this->redirect('/');
        }
        $page = $subpage = null;

        if (!empty($path[0])) {
            $page = $path[0];
        }
        if (!empty($path[1])) {
            $subpage = $path[1];
        }
        $this->set(compact('page', 'subpage'));

        try {
            $this->render(implode('/', $path));
        } catch (MissingTemplateException $e) {
            if (Configure::read('debug')) {
                throw $e;
            }
            throw new NotFoundException();
        }
    }
	
	
	// load contact form
	public function loadContactForm($type=null){
		$this->set("title", '');
		$this->loadModel('ContactForms');
		$contactForm = $this->ContactForms->newEntity();
		$this->viewBuilder()->autoLayout(false);
		
		$this->loadModel('Shop.ShopProvozs');
		$provoz_list = $this->ShopProvozs->provozListAll();
		//pr($provoz_list['provoz_list']);
		$this->set('provoz_list',$provoz_list['provoz_list']);
		
		$this->set(compact("contactForm"));
		$this->render('../Element/ContactForm/'.(($type != null)?$type:'index'));
	}
	
	// load contact form
	public function loadGoogleMap($groupId=null){
		$this->set("title", '');
		$this->loadModel('GoogleMaps');
		$this->viewBuilder()->autoLayout(false);
		
		$conditions = ['google_map_group_id'=>$groupId,'trash'=>0];
		
			$query = $this->GoogleMaps->find()
				->where($conditions)
				->select([
					'id',
					'name',
					'ulice',
					'mesto',
					'psc',
					'lat',
					'lng',
				])
				->hydrate(false)
				->cache(function ($query) {
					return 'google_maps-' . md5(serialize($query->clause('where')));
				});
				$find_google_map = $query ->toArray();
			
		
		$data = [];
		foreach($find_google_map AS $f){
			$data[]['GoogleMap'] = $f;
		}
		//$this->set('data',$data);	
		die(json_encode(['r'=>true,'data'=>$data]));
	}
	
	
	// save newsletter
	public function saveNewsletter(){
		$this->loadModel("Newsletters");
		$this->request->data['email'] = h($this->request->data['email']);
		$newsletter = $this->Newsletters->newEntity();
		if ($this->request->is('ajax')){
			$this->Newsletters->patchEntity($newsletter, $this->request->data());
			//pr($newsletter);die();
			$this->check_error($newsletter);
			
			if ($result = $this->Newsletters->save($newsletter)) {
				die(json_encode(['r'=>true,'m'=>__('Váš email byl uložen'),'clear'=>true]));
				
			} else {
				die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
				
			}
		} else {
			die(json_encode(['r'=>false,'m'=>__('Chyba volání')]));
			
		}
	}
	
	// save contact form
	public function saveContactForm(){
		if ($this->request->is("ajax")){
			$this->loadModel('ContactForms');
			$contactForm = $this->ContactForms->newEntity();
			
			if (empty($this->request->data['provoz']['value'])){
				die(json_encode(['r'=>false,'m'=>__('Musíte vybrat provozovnu')]));
			}
			  
			$this->request->data['name'] = $this->request->data['email']['value'];
			$this->request->data['email'] = $this->request->data['email']['value'];
			
			if (isset($this->request->data['jmeno']['value']))
				$this->request->data['jmeno'] = $this->request->data['jmeno']['value'];
			if (isset($this->request->data['text']['value']))
				$this->request->data['text_tmp'] = $this->request->data['text']['value'];
			//pr($this->request->data);
			
			$html = '<table>';
			
			if (isset($this->request->data['email'])){
				$html .= '<tr><th>Email: </th><td>'.$this->request->data['email'].'</td></tr>';
				
			}
			if (isset($this->request->data['jmeno'])){
				$html .= '<tr><th>Jméno: </th><td>'.$this->request->data['jmeno'].'</td></tr>';
				
			}
			if (isset($this->request->data['provoz']['value'])){
				$this->loadModel('Shop.ShopProvozs');
				$provoz_list = $this->ShopProvozs->provozListAll();
				                   // pr($provoz_list);
				$email_to = $provoz_list['provoz_email'][$this->request->data['provoz']['value']];
				$this->request->data['provoz']['value'] = $provoz_list['provoz_list'][$this->request->data['provoz']['value']];
				if (empty($email_to)){
					die(json_encode(['r'=>false,'m'=>__('Email nenalezen')]));
				}
			}
			
			foreach($this->request->data AS $k=>$d){
				if (isset($d['name'])){
					$html .= '<tr><th>'.$d['name'].': </th><td>'.(!empty($d['value'])?$d['value']:'Nevyplněno').'</td></tr>';
					if (isset($this->request->data[$k]))
					unset($this->request->data[$k]);
				}
			}
			$html .= '</table>';
			
			
			//pr($html);die();
			
			$this->request->data['text'] = $html;
			//pr($this->request->data);die();
			
			
			$this->ContactForms->patchEntity($contactForm, $this->request->data());
			$this->check_error($contactForm);
			
			// send email
			$html_email = '<h2>Email z kontaktního formuláře '.$_SERVER['HTTP_HOST'];
			$html_email .= $html;
			
			$this->loadComponent('Email');
			$opt = [
					'to'=>$email_to,
					'subject'=>'Email z kontaktního formuláře '.$_SERVER['HTTP_HOST'],
					'data'=>$html_email,
			];
			$this->Email->send($opt);
			
			  
			// save form to db
			if ($result = $this->ContactForms->save($contactForm)) {
				die(json_encode(['r'=>true,'m'=>__('Formulář byl odeslán'),'clear'=>true]));
			
			} else {
				die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
			}
		}
	}
	
	
	// google sitemap
	public function googleSitemap(){
		$this->autoRender = false;
		$sitemap = [];
		/*
		$this->loadModel('MenuItems');
		$sitemap = array_merge($sitemap,$this->MenuItems->getFullPath('google','text/'));
		
		
		
		// shop sitemap
		if (Configure::read('modul_shop')){
			$this->loadModel('ShopCategories');
			$sitemap = array_merge($sitemap,$this->ShopCategories->getFullPath('google'));
			
			$this->loadModel('Shop.ShopProducts');
			$sitemap = array_merge($sitemap,$this->ShopProducts->getFullPath());
		
		}
		*/
		//pr($this->menu_items);die(); 
		$sitemap = $this->menu_items;
		//pr($sitemap);die();
		$xml = '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">';
			
			if (isset($sitemap))
			foreach($sitemap as $s){
				if (strpos($s->alias,'http://') !== false){
					$url = $s->alias;
				
				} else {
					$url ='https://'.$_SERVER['SERVER_NAME'].$s->alias;
					
				}
				$xml .='
				<url>
					<loc>'.$url.'</loc>
					<lastmod>2017-06-20</lastmod>
					<changefreq>weekly</changefreq>
					<priority>'.(($s->alias!='/')?'0.6':'1').'</priority>
				</url>';
			}
			
		$xml .= '</urlset>';
		//pr($xml);
		Header("Content-Type: text/xml; charset=UTF-8");
		die($xml);
		echo $xml;
		
		//pr($xml);
		//die();
	}
	
	
	// funkce pro testovani emailu
	public function testEmail($id=null){
		if ($id == null) die('neni id template');
		
		$data_email = [];
		$this->loadComponent('Email');
		
		
		// test objednavka
		$this->loadModel('Shop.ShopOrders');
		$order = $this->ShopOrders->getOrder(36);
		//pr($order);die();
		$data_email = json_decode(json_encode($order),true);
		//pr($data_email);
		
		// test user 
		$this->loadModel('Users');
		$find = $this->Users->find()
			->where([])
			->select([
			])
			->first();
		
		
		$data_email = json_decode(json_encode($find),true);
		//pr($data_email);die();
		
		$opt = [
			'to'=>[$this->system_email,'skramusky@fastest.cz'],
			'template_id'=>$id,
			'data'=>$data_email,
			'price_format'=>[
				'price_vat_per_item',
				'price_payment',
				'price_vat_payment',
				'price_transport',
				'price_vat_transport',
			],
		];
		$this->Email->send($opt);
		die('send to '.$this->system_email);
	}
	
	// load OLD data
	public function loadOldUsers(){
		if ($_SERVER['REMOTE_ADDR'] != '89.103.18.65'){
			die('stop');
		}
		$this->loadModel('OldUsers');
		$query = $this->OldUsers->find()
			->where(['reg'=>1,'imported'=>0])
			//->where(['reg'=>1,'id'=>494515])
			//->where(['reg'=>1])
			->limit(1000)
			->order('updated DESC')
			->map(function($row){
				return $row;
			})
			;
		$find_data = $query ->toArray();
		$save_user = [];
		$this->loadModel('Users');
		$cnt_new = 0;
		$cnt_exist = 0;
		$cnt_body = 0;
		foreach($find_data AS $d){
			
			$query = $this->Users->find()
				->where(['id'=>$d->id])
				->select([
					'id',
					'credits',
				])
				;
			$exist = $query ->first();
			//pr($d);
			//die('aa');
			if (!$exist ){
			//pr($exist->credits);
			//pr($d->body);
			
			$cnt_new ++;
			//pr($d);
			$save_user[] = [
				'id'=>$d->id,
				'reg'=>1,
				'first_name'=>$d->jmeno,
				'last_name'=>$d->prijmeni,
				'created'=>$d->created,
				'modified'=>$d->updated,
				'email'=>$d->email,
				'password'=>$d->password,
				'password2'=>$d->password,
				'phone'=>$d->telefon,
				'phone_pref'=>$d->telefon_pref,
				'system_id'=>$d->zdroj,
				'avatar_id'=>1,
				'credits'=>$d->body,
			];
			} else {
				if ($exist->credits < $d->body){
					$cnt_body ++;
					$save_user[] = [
						'id'=>$d->id,
						'credits'=>$d->body,
					];
					//pr($save_user);
				} else {
				$cnt_exist ++;
				}
				//pr($d);
				//die('exist');
			}
			
			$this->OldUsers->updateAll(
				['imported' => 1], // fields
				['id' => $d->id]
			);
			
		}
		//pr($save_user);
		//die('end '.count($find_data) .'exist:'.$cnt_exist.' new:'.$cnt_new);
		//pr(count($find_data));die();
		
		foreach ($save_user as $save_data){
			$save = $this->Users->newEntity($save_data,['accessibleFields' => ['id' => true],'validate' => false]);
				//pr($save);
			//die();
			$this->Users->save($save);
        }
		die('end '.count($find_data) .'exist:'.$cnt_exist.' new:'.$cnt_new.' body:'.$cnt_body);
	}
	
	// load OLD data
	public function loadOld(){
		header('Content-Type: text/html; charset=utf-8');
		$time_start = microtime(true); 
		
		
		function save_file($file){
			$current = @file_get_contents('http://www.chachar.cz/uploaded/rozvoz/large/'.$file);
			$save_file = './uploaded/products/'.$file;
			if (!file_exists($save_file) && !empty($current))
				file_put_contents($save_file, $current);
			//die('a');
		}
		
		
		$this->loadModel('OrgDatas');
		$conditions = [
			'kos'=>0,
			'status'=>1,
		];
		$query = $this->OrgDatas->find()
			->where($conditions)
			->contain('OrgDataCons')
			->map(function($row){
				$row->imgs = (!empty($row->imgs)?unserialize($row->imgs):'');
				$row->varianty = (!empty($row->varianty)?unserialize($row->varianty):'');
				$con = [];
				if (!empty($row->org_data_cons)){
					foreach	($row->org_data_cons AS $c){
						$con[] = [
							'shop_product_id'=>$row->id,
							'shop_product_attribute_id'=>$c->rozvoz_food_group4_id,
						];
					}
				}
				$row->shop_product_con_attributes = $con;
				
				return $row;
			})
			;
		$find_data = $query ->toArray();
		//pr($find_data);
		$save_atr = [];
		$save_products = [];
		foreach($find_data AS $d){
			if (isset($d->imgs[0])){
				list($file,$fname) = explode('|',$d->imgs[0]);
				if (!empty($file)){
					//save_file($file);
					$images = [];
					$images[] = [
						'file'=>$file,
						'name'=>$fname,
					];
				}
			} else {
				$images = '';
			}
			//if ($d->id < 2)
			if (isset($d->varianty) && !empty($d->varianty)){
				$varianty = [];
				foreach($d->varianty AS $v){
					foreach($this->system_list AS $system_id=>$system_name){
						$varianty[] = [
							'system_id'=>$system_id,
							'code'=>$v['code'],
							'name'=>$v['name'],
							'tax_id'=>1,
							'price_vat'=>$v['price'],
							'price'=>$v['price'] - ($v['price']*0.1736),
						];
					}
				}
			} else {
				$varianty = '';
			}
			
			$save_products[] = [
				'id'=>$d->id,
				'number'=>($d->num > 0)?$d->num:null,
				'code'=>$d->code,
				'name'=>$d->name,
				'name_internal'=>$d->name,
				'alias'=>$d->alias_,
				'description'=>$d->text,
				'price'=>$d->price - ($d->price*0.1736),
				'price_vat'=>$d->price,
				'tax_id'=>1,
				'sort'=>$d->poradi,
				'gramaz'=>$d->gramaz,
				'food_type_id'=>$d->rozvoz_food_group2_id,
				'oblibene'=>$d->oblibene,
				'created'=>$d->created,
				'modified'=>$d->updated,
				'shop_product_con_attributes'=>$d->shop_product_con_attributes,
				'images'=>json_encode($images),
				'shop_product_variants'=>$varianty,
			];
			
			//pr($d);die();
		}
		$this->loadModel('Shop.ShopProducts');
		$this->loadModel('Shop.ShopProductConAttributes');
		//pr($save_data);die();
		
        foreach ($save_products as $save_data){
			$save = $this->ShopProducts->newEntity($save_data,['associated' => ['ShopProductVariants','ShopProductConAttributes']]);
		    //pr($save);die();
			$this->ShopProducts->save($save);
        }
		//pr($save_data);
		//pr($find_data);
		$time_script = 'Total execution time in seconds: ' . (microtime(true) - $time_start);
		die($time_script);
	}
	
	// upload files
	function fstUpload($delete = null){
		$fn = (isset($_SERVER['HTTP_X_FILENAME']) ? $_SERVER['HTTP_X_FILENAME'] : false);
		//pr($this->request->query);
		if (isset($this->request->query['params'])){
			$params = json_decode(base64_decode($this->request->query['params']));
		}
		
		$this->loadComponent('Upload');
		//pr($params);
		if ($params->file_ext){
			$params->file_ext = explode(',',$params->file_ext);
		} else {
			$params->file = json_decode($params->file,true);
			
		}
		//pr($params->file);die();
		if ($delete != null){
			$setting = array(
				'file'=>$params->file,
				'upload_path'=>$params->upload_path,
			);
			$this->Upload->delete_file($setting);
			
		
		} else if (isset($_FILES['file']['type'])){
			
			$setting = array(
				'fn'=>(!isset($fn)?$fn:$_FILES['file']['name'][0]),
				'files'=>$_FILES,
				'filename'=>$_FILES['file']['name'][0],
				'tmp_name'=>$_FILES['file']['tmp_name'][0],
				'type'=>$_FILES['file']['type'][0],
				'file_ext'=>$params->file_ext,
				'count_file'=>$params->count_file,
				'upload_path'=>$params->upload_path,
			);
			if (isset($params->special_name) && !empty($params->special_name)){
				$setting['filename_type'] = 'special';
				$setting['filename_special'] = $params->special_name;
			}
			
			$this->Upload->doit($setting);
		} else {
			die(json_encode(array('result'=>false,'message'=>'Chyba nahrání souboru')));
	
		}
	
	}
	
	function tst(){
		//pr(getcwd());
		unlink('./uploaded/products/chachar564445ad11c6a.png');
		die('a');
	}

	 
	
}
