<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\ORM\TableRegistry;
use Cake\I18n\Time;

class UsersController extends AppController
{
	public function initialize(){
		parent::initialize();
		//$this->Auth->allow("add");
	}

	public function login(){
		if (!$this->request->is('ajax')) {
			if (isset($this->request->query['se'])){
				$this->Session->write('chatbot_server',$this->request->query['se']);
				$this->Session->write('chatbot_msi',$this->request->query['msi']);
			}
			//$this->viewBuilder()->autoLayout(false);
		
		} else {
				$this->Auth->config('authenticate', [
                    'Form' => [
                        'fields' => ['username' => 'email']
                    ]
                ]);
                $this->Auth->constructAuthenticate();
				$this->request->data['email'] = $this->request->data['username'];
				$this->request->data['password'] = md5($this->request->data['password']);
                unset($this->request->data['username']);
			$user = $this->Auth->identify();
			//pr($user);
			if ($user) {
				//pr($user);
				if ($this->Session->check('chatbot_msi')){
					$data = [
						'code'=>$user['id'],
						'project'=>$this->chatBotsProjects[$this->system_id],
						'm_id'=>$this->Session->read('chatbot_msi'),
						'server'=>$this->Session->read('chatbot_server'),
					];
				
					$this->chatbotLogin($data);
					die();
				}
				$this->Auth->setUser($user);
				//pr($this->Auth);
				setcookie("favorite_list", $user['favorite'], time()+360000,'/');
				//pr($user);
				//$_COOKIE['favorite_list'] = user
				die(json_encode(['r'=>true,'m'=>__('Přihlášení v pořádku'),'redirect'=>'self','user_id'=>$user['id']]));
				
			} else {
				die(json_encode(['r'=>false,'m'=>__('Chybné přihlašovací údaje')]));
			}
		}
		
    }
	
	/**
	* chatbot login
	*/
	private function chatbotLogin($data){
		$ch = curl_init();
		$post = [
			'm_id'=>$data['m_id'],
			'code'=>$data['code'],
		];
		$url = 'https://'.$data['server'].'-server.gobots.cz/'.$data['project'].'/events/login';
		//pr($url);
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec ($ch);
		//print_r($result);
		curl_close ($ch);

		$this->Session->delete('chatbot_msi');
		$this->Session->delete('chatbot_server');
		die(json_encode(['r'=>true,'m'=>__('Přihlášení v pořádku'),'close'=>true]));
		
	}
	
	/**
	* chatbot registrace
	*/
	private function chatbotRegistrate($data){
		$ch = curl_init();
		$post = [
			'm_id'=>$data['m_id'],
			'code'=>$data['code'],
		];
                
		$url = 'https://'.$data['server'].'-server.gobots.cz/'.$data['project'].'/events/register';
		//pr($data);
		//pr($url);die();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($post));

		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$result = curl_exec ($ch);
		//print_r($result);
		curl_close ($ch);
		$this->Session->delete('chatbot_msi');
		$this->Session->delete('chatbot_server');
		//die('aa');
		die(json_encode(['r'=>true,'m'=>__('Registrace v pořádku'),'close'=>true]));
		
	}
	
	public function logout(){
		$this->Auth->logout();
		return $this->redirect('/');
	}

	public function registrace(){
		
		
		
		$this->set("no_side", true);
		$this->set("layout_class", 'col-sm-12 txt_page');
		$this->set("title", __("Registrujte se a získejte 20 kreditů ZDARMA! "));
		$this->set("button_title", __('Zaregistrovat se'));
		$done_title = __('Váš účet byl zaregistrován');
		
		if (isset($this->request->query['se'])){
			$this->Session->write('chatbot_server',$this->request->query['se']);
			$this->Session->write('chatbot_msi',$this->request->query['msi']);
		}
		//$read = $this->Users->find()->where(['id'=>1])->contain('UserAddresses')->toArray();
		//pr($read);
	   
		$users = $this->Users->newEntity();
		if (isset($this->loggedUser)){
			//pr($this->loggedUser);die();
			
			$users = $this->Users->find()
				->where(['id'=>$this->loggedUser['id']])
				->first();
			
			if (empty($users)){
				return $this->redirect('/');
			}
			$this->set('userEdit',true);
			$this->set("title", __("Editace uživatele - ".$users->first_name.' '.$users->last_name));
			$this->set("button_title", __('Upravit údaje'));
			$done_title = __('Váš účet byl upraven');
	   
			$users->password2 = $users->password;
			$users = $this->Users->loadPhonePref($users);
			//pr($users);
			
			// load adresa	
			$this->loadModel('UserAddresses');
			$address = $this->UserAddresses->find()
				->where(['shop_client_id'=>$this->loggedUser['id']])
				->select([
				])
				->order('id DESC')
				->toArray();
			//pr($address);
			
			
			
			$users->user_addresses = $address;
			
			//pr($users);
			
		}
		if ($this->request->is("ajax")){
			
			if (empty($this->request->data['password'])){		
				die(json_encode(['r'=>false,'m'=>'Zadejte Vaše heslo']));
			}
			
			// editace hesla
			if (isset($this->request->data['password_tmp'])){
				if ($this->request->data['password_tmp'] == $this->request->data['password']){
					unset($this->request->data['password']);
				}
			}
			
			if (isset($this->request->data['password'])){
				
				$this->request->data['password'] = md5($this->request->data['password']);
				$this->request->data['password2'] = md5($this->request->data['password2']);
			}
			
			// convert phone pref
			$this->request->data = $this->Users->savePhonePref($this->request->data);
			
			// nastavit odkud se zaregistroval
			if (empty($this->request->data['id']) && !isset($this->loggedUser)){
				$this->request->data['system_id'] = $this->system_id;
				$this->request->data['credits'] = 20;
				$this->request->data['reg'] = 1;
			}
			
			// validace kroku
			if (isset($this->request->data['step'])){
				if ($this->request->data['step'] == 1){
					$this->Users->patchEntity($users, $this->request->data(),[
						'validate' => 'Step1',
						'associated' => [
							'UserAddresses' => ['validate' => 'onlyCheck']
						]
					]);
					//pr($this->request->data);
					if (isset($this->request->data['email']))
					if ($this->findUserReg($this->request->data['email'])){
						die(json_encode(['r'=>false,'m'=>__d('shop','Váš email je již zaregistrován, prosím přihlašte se')]));
					}
					$this->check_error($users);
					die(json_encode(['r'=>true]));
				}
				if ($this->request->data['step'] == 2){
					if (isset($this->loggedUser)){
			
						$this->Users->patchEntity($users, $this->request->data(),[
							'validate' => 'onlyCheck',
							'associated' => [
								'UserAddresses' => ['validate' => 'onlyCheck']
							]
						]);
					} else {
						$this->Users->patchEntity($users, $this->request->data());
						
					}
					$this->check_error($users);
					die(json_encode(['r'=>true]));
					
				}
			}
			
			
			
			
			if (isset($this->loggedUser)){
				// neukladat adresu
				if (isset($this->request->data['user_addresses'][0]['city']) && empty($this->request->data['user_addresses'][0]['city'])){
					unset($this->request->data['user_addresses']);
				}
				$this->Users->patchEntity($users, $this->request->data(),[
					'validate' => 'onlyCheck',
					'associated' => [
						'UserAddresses' => ['validate' => 'onlyCheck']
					]
				]);
			} else {
				$this->Users->patchEntity($users, $this->request->data());
			}
			
			
			
			//pr($users);die();
			$this->check_error($users);
			
			if ($result = $this->Users->save($users)) {
					$res = $result->toArray();
					//unset($res['password']);
					//unset($res['password2']);
					$find = $this->Users->getUser($res['id']);
					$this->Auth->setUser($find);
					//pr($find);
					//pr($this->request->data);
					if (empty($this->request->data['id']) && !isset($this->loggedUser))
					$this->send_email_registrace($find);
				
					if ($this->Session->check('chatbot_msi')){
						//pr($find);
						$data = [
							'code'=>$find['id'],
							'project'=>$this->chatBotsProjects[$this->system_id],
							'm_id'=>$this->Session->read('chatbot_msi'),
							'server'=>$this->Session->read('chatbot_server'),
						];
					
						$this->chatbotRegistrate($data);
						die();
					}
				
				if (isset($this->loggedUser)){
					die(json_encode(['r'=>true,'m'=>$done_title,'redirect'=>'/']));
				} else {
					die(json_encode(['r'=>true]));
				
				}
				
			} else {
				die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
			}
		}

		$this->set(compact("users"));
	}
	
	
	private function findUserReg($email){
		$this->loadModel('ShopClients');
		
		$find_email = $this->ShopClients->find()
			->where(['email'=>$email,'reg'=>1])
			->first();
		if ($find_email){
			return true;
		} else {
			return false;
		}
	}
	
	// send email registrace
	private function send_email_registrace($data){
		$data_email = json_decode(json_encode($data),true);
			//pr($data);
			$this->loadComponent('Email');
			$opt = [
				'to'=>$data['email'],
				'template_id'=>2,
				'data'=>$data_email,
			];
			$this->Email->send($opt);
					
	}
	
	
	public function zapomenuteHeslo(){
		$this->set("title", __("Zapomenuté heslo"));
		
		$users = $this->Users->newEntity();
		
		
		//$this->sendEmail($opt);
		
		if ($this->request->is("ajax")){
			$this->Users->patchEntity($users, $this->request->data());
			
			$find = $this->Users->find()
			->where(['email'=>$this->request->data['email']])
			->select([
				'id',
				'email',
			])
			->first();
			//pr($this->request->data());die();
			
			//pr($find);
			if ($find) {
				$this->Users->updateAll(
					['password_send' => new Time(date('Y-m-d'))], // fields
					['id' => $find->id]
				);
				
				$hash = date('Y-m-d').$find->email.$find->id;
				//pr($hash);die();
				
				// send to email
				$this->loadComponent('Email');
				$link = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/'.heslo_reset_link.'/'.sha1($hash);
				$data = [
					'Users_link'=>'<a href="'.$link.'">'.$link.'</a>',
				];
				//pr($data);die();
				
				$opt = [
					'to'=>$find->email,
					'template_id'=>1,
					'data'=>$data,
				];
				$this->Email->send($opt);
				
				
				die(json_encode(['r'=>true,'m'=>__('Na váš email byly zaslány požadované údaje'),'redirect'=>'self']));
			
			} else {
				die(json_encode(['r'=>false,'m'=>__('Zadaný email nebyl nalezen')]));
			}
		}

		$this->set(compact("users"));
	}
	
	
	public function resetHeslo($hash){
		$this->set('title',__('Obnovení hesla'));
		
		$find = $this->Users->find()
			->where([
				'SHA1(CONCAT(password_send,email,id)) ='=>$hash,
				'password_send'=>date('Y-m-d')
			])
			->select([
				'id',
				'password_send',
				'email',
			])
			->first()
			;
		if (!$find){
			$this->set('alert_class','alert-danger');
			$this->set('alert_text','<strong>'.__('Pozor').'!</strong> '.__('Špatné ověření emailu, nebo vypršení expirace. Zkuste zaslat heslo znovu.'));
		} else {
			$this->set('alert_class','alert-success');
			$this->set('alert_text','<strong>'.__('V pořádku').'!</strong> '.__('Zadejte Vaše nové heslo.'));
			$users = $this->Users->newEntity();
			$this->set(compact("users"));
			
			if ($this->request->is("ajax")){
				$this->request->data['password'] = md5($this->request->data['password']);
				$this->request->data['password2'] = md5($this->request->data['password2']);
				
				$users->id = $find->id;
				$this->Users->patchEntity($users, $this->request->data());
				$this->check_error($users);
				if ($result = $this->Users->save($users)) {
						$res = $result->toArray();
						//unset($res['password']);
						//unset($res['password2']);
						
						$find = $this->Users->getUser($res['id']);
						$this->Auth->setUser($find);
					die(json_encode(['r'=>true,'m'=>__('Heslo bylo změněno, budete přihlášen(a)'),'redirect'=>'/']));
				
				} else {
					die(json_encode(['r'=>false,'m'=>__('Chyba uložení')]));
				}
				
				
			}	
		}
		
		
		//pr($find);
		//a
		//die();
	}
	
	// delete adresa
	public function deleteAddress($id){
		if (!$this->request->is("ajax")){
			die('Chyba volani funkce');
		}
		$this->loadModel('UserAddresses');
		$find = $this->UserAddresses->find()
			->where([
				'shop_client_id'=>$this->loggedUser['id'],
				'id'=>$id,
			])
			->select([
				'id',
			])
			->first()
			;
		
		if (!$find){
			die(json_encode(['r'=>false,'m'=>__('Adresa nenalezena')]));
		}
		//pr($find);
		if (!$result = $this->UserAddresses->delete($find)){
			die(json_encode(['r'=>false,'m'=>__('Chyba vymazání adresy')]));
		} else {
			die(json_encode(['r'=>true,'m'=>__('Adresa vymazána')]));
			
		}
	}
	
	public function saveFavorite(){
		if (!isset($this->loggedUser)){
			die(json_encode(['r'=>false]));
		}
		$this->Users->updateAll(
			['favorite' => $_COOKIE['favorite_list']], // fields
			['id' => $this->loggedUser['id']]
		);
		//pr($_COOKIE['favorite_list']);
		die(json_encode(['r'=>true]));
	}
}
