<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\I18n\Time;
use lessc\lessc;
use Cake\Cache\Cache;


/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class MobilesController extends AppController
{
	
	public function index($hash_params=null){
		/*
		$this->loadModel('Shop.ShopProvozs');
		$this->pokladna_list = $this->ShopProvozs->pokladnaList();	
		pr($this->pokladna_list);
		*/
		$hash = [
			'pokladna_id'=>1000,
			'rozvozce_id'=>1,
			'code'=>'001',
		];
		
		$hash = $this->encode_long_url($hash);
		$url = 'http://www.chachar.cz/mobile/'.$hash;
		
		if ($hash_params != null){
			
			if(empty($_SERVER['HTTPS']) || $_SERVER['HTTPS'] == "off"){
				$redirect = 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
				header('HTTP/1.1 301 Moved Permanently');
				header('Location: ' . $redirect);
				exit();
			}
			
			$this->set('hash_params',$hash_params);
			$params = $this->decode_long_url($hash_params);
			//pr($params);
			//unset($params['code']);
			//pr($params);
			$this->loadModel('Shop.ShopProvozs');
			$this->pokladna_list = $this->ShopProvozs->pokladnaList();	
			$this->basket_provoz_list = $this->ShopProvozs->provozListAll();	
			//pr($this->pokladna_list);
			//pr($this->basket_provoz_list);
			//die();
			
			//pr($params);
			if (!isset($params['rozvozce_id']) || !isset($params['code'])|| !isset($params['pokladna_id'])){
				header('Content-Type: text/html; charset=utf-8');
				die('Chybné parametry');
			}
			$conditions = [
				'rozvozce_id'=>$params['rozvozce_id'],
				'code'=>$params['code'],
				'pokladna_id'=>$this->pokladna_list['list'][$params['pokladna_id']],
			   ];
			      
			$this->Session->write('rozvozce_params',$params);
			$rozvozce_params = $this->Session->read('rozvozce_params');
			$this->saveGspRozvoce();
			$checkBigBrother = $this->checkBigBrother();
			$this->set('update_gps',$checkBigBrother['time']);
			if ($checkBigBrother['r'] == true){
				$this->set('is_gps',true);
			}
		
			
			//pr($conditions);
			$this->loadModel('Rozvozces');
			$query = $this->Rozvozces->find()
			   ->where($conditions)
			  ->select([
				'id',
				'name',
			  ])
			  ;
			  
			$find_rozvozces = $query->first();
			
			if (isset($this->basket_provoz_list['provoz_list'][$this->pokladna_list['list'][$params['pokladna_id']]]))
			$provoz_name = $this->basket_provoz_list['provoz_list'][$this->pokladna_list['list'][$params['pokladna_id']]];
			else
				$provoz_name = 'Testovaci';
			$this->set('find_rozvozces',$find_rozvozces);
			$this->set('provoz_name',$provoz_name);
			//pr($find_rozvozces);
			if (!$find_rozvozces){
				header('Content-Type: text/html; charset=utf-8');
				die('Chybné přihlášení');
			}
			
			//pr($this->pokladna_list);
			$date_from = date('Y-m-d 00:00:00');
			$date_to = date('Y-m-d 23:59:59');
			$date_from = strtotime($date_from);
			$date_to = strtotime($date_to);
			
			$conditions = [
				//'date(MobileOrders.created)' => date('Y-m-d'),
				'MobileOrders.created_time >='=>$date_from,
				'MobileOrders.created_time <'=>$date_to,
			
				'MobileOrders.stav_id NOT IN' => [4,5],
				'MobileOrders.rozvozce_id' => $params['rozvozce_id'],
				//'MobileOrders.shop_provoz_id' => $this->pokladna_list['list'][$params['pokladna_id']],
				'MobileOrders.shop_provoz_id' => $params['pokladna_id'],
			];
			//pr($conditions);
			//pr($this->pokladna_list);
		} else {
			
			//!!!!!!! zakazat
			if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
			echo '<br /><br /><a href="http://www.manikova-pizza.cz/mobile/eNqrVirIz85JTMlLjM9MUbJSMjQwMFDSUSrKryrLr0pOhQgaGBgCxZLzU1KhnFpcMOf4EDk,">Prejit na rozvozce</a><br /><br />';
			
			}
			die('Nemate pristup');
			//$conditions = ['ShopOrders.shop_client_id'=>104];
		}
		//pr($url);
		
		$this->set('isMobile',true);
		$this->viewBuilder()->layout("mobile");
		$this->set('scripts',['/js/sortable/sortable.js']);
		
		
		//$this->loadModel('Shop.ShopOrders');
		$this->loadModel('MobileOrders');
		if ($this->request->is("ajax")){
		
		$query = $this->MobileOrders->find()->first();
		
		
		$query = $this->MobileOrders->find()
		  ->contain(['MobileOrderItems'])
		  ->where($conditions)
		  ->select([
		  ])
		  ->group('MobileOrders.order_id')
		 // ->limit(4)
		  ->order('MobileOrders.id DESC')
		  ;
		$load = $query->toArray();
		//pr($load);
		//sleep(5);
		if (!empty($load)){
			$this->set('orders',$load);
		}
		}
		if ($this->request->is("ajax")){
			$this->viewBuilder()->autoLayout(false);
		}
		//pr($load);
	}
	
	public function checkBigBrother(){
		
		$rozvozce_params = $this->Session->read('rozvozce_params');
		$this->loadModel('GpsDispecers');	
		$query = $this->GpsDispecers->find()
			->where([
				'rozvozce_id'=>$rozvozce_params['rozvozce_id'],
				'pokladna_id'=>$this->pokladna_list['list'][$rozvozce_params['pokladna_id']]
				])
				->select(['id','lat','lng','update_gps'])
			;
			  
		$find_current = $query->first();
		$result = ['r'=>false,'time'=>'Nezjištěno'];
		if($find_current){
			if (empty($find_current->lat)){
				
				$result = ['r'=>false,'time'=>''];
			} else {
				$result = ['r'=>true,'time'=>$find_current->update_gps];
			}
		}
		return($result);
	}
	
	public function saveGspRozvoce($direct=false){
		//if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
			if ($direct == true){
				$this->loadModel('Shop.ShopProvozs');
				$this->pokladna_list = $this->ShopProvozs->pokladnaList();	
			}
			$rozvozce_params = $this->Session->read('rozvozce_params');
			//pr($rozvozce_params);
			$conditions = [
				'rozvozce_id'=>$rozvozce_params['rozvozce_id'],
				'pokladna_id'=>$this->pokladna_list['list'][$rozvozce_params['pokladna_id']],
			   ];
			   
			//pr($conditions);
			$this->loadModel('Rozvozces');
			$query = $this->Rozvozces->find()
			   ->where($conditions)
			  ->select([
				
			  ])
			  ;
			  
			$find_rozvozces = $query->first();
			//pr($conditions);
			
			
			$this->loadModel('GpsDispecers');
			$query = $this->GpsDispecers->find()
			  ->where([
				'rozvozce_id'=>$rozvozce_params['rozvozce_id'],
				'pokladna_id'=>$this->pokladna_list['list'][$rozvozce_params['pokladna_id']]
				])
				->select('id')
			  ;
			  
			$find_current = $query->first();
			//pr($find_rozvozces);
			$save_gps = [
				'logged_date'=>new Time(),
				'system_id'=>$this->system_id,
				'rozvozce_id'=>$rozvozce_params['rozvozce_id'],
				'rozvozce_name'=>$find_rozvozces->name,
				'rozvozce_ip'=>strtr($_SERVER['REMOTE_ADDR'],['.'=>'']),
				'pokladna_id' => $this->pokladna_list['list'][$rozvozce_params['pokladna_id']],
			];
			if ($find_current){
				$save_gps['id'] = $find_current['id'];
			}
			//pr($save_gps);die('a');
			$save_data_gps = $this->GpsDispecers->newEntity($save_gps);
			//pr($save_data_gps);
			$this->GpsDispecers->save($save_data_gps);
			if ($direct == true){
				die(json_encode(['r'=>true]));
			}
			//die('a');
		//}
	}
	
	
	public function logGps(){ 
		//pr($this->system_id);
		//die();
		if ($_SERVER['REMOTE_ADDR'] != '89.103.18.65'){
			//die();
		}
		
		$this->loadModel('MobileOrders');
		$update_time = new Time();
		$this->MobileOrders->updateAll(
			[
				'lat' => $_POST['latitude'],
				'lng' => $_POST['longitude'],
				'update_gps' => $update_time->format('Y-m-d H:i:s'),
				
			], // fields
			[
				//'zdroj_id' => $this->system_id,
				'stav_id' => 6,
				'driver_ip' => strtr($_SERVER['REMOTE_ADDR'],['.'=>''])
			
			]
		);
		
		
		//if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
			$this->loadModel('GpsDispecers');
			$this->GpsDispecers->updateAll(
				[
					'lat' => $_POST['latitude'],
					'lng' => $_POST['longitude'],
					'update_gps' => $update_time->format('Y-m-d H:i:s'),
					
				], // fields
				[
					//'zdroj_id' => $this->system_id,
					'rozvozce_ip' => strtr($_SERVER['REMOTE_ADDR'],['.'=>''])
				
				]
			);
		//}
		
		//throw new Exception(json_encode($update_time));
		
		
		//die('save'); 
		$file = 'gps.txt';
		// Open the file to get existing content
		$current = file_get_contents($file);
		// Append a new person to the file
		$_POST['ip'] = strtr($_SERVER['REMOTE_ADDR'],['.'=>'']);
		$_POST['browser'] = $_SERVER['HTTP_USER_AGENT'];
		$_POST['update_time'] = $update_time;
		$data = json_encode($_POST);
		$current .= "John Smith\n";
		$current.= $data."\n";
		// Write the contents back to the file
		file_put_contents($file, $current);
		die('save');
	}
	
	public function rozvoceAdd(){
		if (!isset($_POST['pokladna_id']) || empty($_POST['pokladna_id'])){
			die(json_encode(['r'=>false]));
		}
		$this->loadModel('Shop.ShopProvozs');
		$this->pokladna_list = $this->ShopProvozs->pokladnaList();	
		
		$this->loadModel('Rozvozces');
		$query = $this->Rozvozces->find()
		  ->where(['pokladna_id'=>$this->pokladna_list['list'][$_POST['pokladna_id']]])
		  ;
		$find_current = $query->toArray();
		
		//print_r($this->pokladna_list[$_POST['pokladna_id']]);
		//print_r($find_current);
		//print_r($_POST['list']);
		$id_list_current = [];
		if ($find_current){
			
			foreach($find_current AS $l){
				//print_r($l);
				$id_list_current[$l->id] = $l->rozvozce_id;
			}
		}
		//print_r($id_list_current);
		$id_list = [];
		foreach($_POST['list'] AS $l){
			//print_r($l);
			$id_list[$l['Rozvozce']['id']] = $l['Rozvozce'];
		}
		//print_r($id_list);
		
		// smazani neaktivnich
		$delete_list = [];
		$save_list = [];
		foreach($id_list_current AS $del_id=>$i){
			if (!isset($id_list[$i])){
				$delete_list[] = $del_id;
			} else {
				$save_list[$i] = [
					'id'=>$del_id,
				];
				//print_r($id_list[$i]);
			}
		}
		foreach($id_list AS $rozvozce_id=>$i){
			if (isset($save_list[$rozvozce_id])){
				
				$save_list[$rozvozce_id]['name'] = $i['name'] ;
				$save_list[$rozvozce_id]['code'] = $i['code'] ;
				$save_list[$rozvozce_id]['rozvozce_id'] = $i['id'] ;
				$save_list[$rozvozce_id]['pokladna_id'] = $this->pokladna_list['list'][$_POST['pokladna_id']] ;
			} else {
				$save_list[$rozvozce_id] = [
					'name'=>$i['name'],
					'code'=>$i['code'],
					'rozvozce_id'=>$i['id'],
					'pokladna_id' => $this->pokladna_list['list'][$_POST['pokladna_id']],
				];
			}
			//$save_list[] = [
				//'id'=>$del_id,
			//];
			//print_r($id_list[$i]);
			
		}
		
		foreach($save_list AS $save){
			$save_data = $this->Rozvozces->newEntity($save);
			//print_r($save_data);
			$this->Rozvozces->save($save_data);
		}
		if (!empty($delete_list)){
			$this->Rozvozces->deleteAll(['id IN' => $delete_list]);
		}
		
		//print_r($delete_list);
		
		die(json_encode(['r'=>true]));
	}
	
	public function vylozeno($id){
		$this->loadModel('Shop.ShopOrders');
		$this->loadModel('MobileOrders');
		if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
		//die(json_encode(['r'=>true]));
		}
		$this->MobileOrders->updateAll(
			[
				'stav_id' => 4,
				'rozvozce_date_vylozeni' => new Time(),
				
			], // fields
			['id' => $id]
		);
		$query = $this->MobileOrders->find()
		  ->where(['MobileOrders.id'=>$id])
		  ->select([
			'order_web_id'
		  ])
		  ->hydrate(false)
		  ;
		$find_web_id = $query->first();
		if (isset($find_web_id) && !empty($find_web_id)){
		
		$this->ShopOrders->updateAll(
			[
				'stav_id' => 4,
				'rozvozce_date_vylozeni' => new Time(),
				
			], // fields
			['id' => $find_web_id['order_web_id']]
		);
		}
		die(json_encode(['r'=>true]));
	}
	
	public function mobileVezu($id){
		$this->loadModel('Shop.ShopOrders');
		$this->loadModel('MobileOrders');
		
		$this->loadModel('Shop.ShopProvozs');
		$this->pokladna_list = $this->ShopProvozs->pokladnaList();	
		
		
		
		$this->MobileOrders->updateAll(
			[
				'driver_ip' => strtr($_SERVER['REMOTE_ADDR'],['.'=>'']),
				'stav_id' => 6,
				'rozvozce_date_vezu' => new Time(),
				
			], // fields
			['id' => $id]
		);
		
		$query = $this->MobileOrders->find()
		  ->where(['MobileOrders.id'=>$id])
		  ->select([
			'order_web_id'
		  ])
		  ->hydrate(false)
		  ;
		$find_web_id = $query->first();
		if (isset($find_web_id) && !empty($find_web_id)){
		
		$this->ShopOrders->updateAll(
			[
				'stav_id' => 6,
				'rozvozce_date_vezu' => new Time(),
				
			], // fields
			['id' => $find_web_id['order_web_id']]
		);
		}
		//$this->sendSms($id);
		die(json_encode(['r'=>true]));
	}
	public function saveGps($lat,$lng,$id){
		$this->loadModel('Shop.ShopOrders');
		$this->loadModel('MobileOrders');
		
		$this->MobileOrders->updateAll(
			[
				'lat' => $lat,
				'lng' => $lng,
				
			], // fields
			['id' => $id]
		);
		
		$query = $this->MobileOrders->find()
		  ->where(['MobileOrders.id'=>$id])
		  ->select([
			'order_web_id'
		  ])
		  ->hydrate(false)
		  ;
		$find_web_id = $query->first();
		
		if (isset($find_web_id) && !empty($find_web_id)){
			$this->ShopOrders->updateAll(
			[
				'lat' => $lat,
				'lng' => $lng,
				
			], // fields
			['id' => $find_web_id['order_web_id']]
		);
		}
		die(json_encode(['r'=>true]));
	}
	
	public function nevylozeno($id){
		$this->loadModel('ShopOrders');
		$this->loadModel('MobileOrders');
		
		
		$this->MobileOrders->updateAll(
			[
				'stav_id' => 5,
				'rozvozce_date_nevylozeni' => new Time(),
				
			], // fields
			['id' => $id]
		);
		
		$query = $this->MobileOrders->find()
		  ->where(['MobileOrders.id'=>$id])
		  ->select([
			'order_web_id'
		  ])
		  ->hydrate(false)
		  ;
		$find_web_id = $query->first();
		if (isset($find_web_id) && !empty($find_web_id)){
		
		$this->ShopOrders->updateAll(
			[
				'stav_id' => 5,
				'rozvozce_date_nevylozeni' => new Time(),
				
			], // fields
			['id' => $find_web_id['order_web_id']]
		);
		
		
		}
		
		die(json_encode(['r'=>true]));
	}
	
	public function testSms(){
		$this->sendSms(7764);
	}
	
	private function sendSms($id=null){
		$this->loadModel('MobileOrders');
		$query = $this->MobileOrders->find()
		  ->where(['MobileOrders.id'=>$id,'MobileOrders.stav_id'=>6])
		  ->select([
		  ])
		  ->hydrate(false)
		  ;
		$data = $query->first();
		if ($_SERVER['REMOTE_ADDR'] == '46.135.104.174a'){
		
		//pr($this->pokladna_list['system_id'][$this->pokladna_list['list'][1000]]);
		//pr($this->system_list);
		//pr('a');
		//pr($data);die();
		$this->loadComponent('GoogleShort');
		$key = 'AIzaSyBL5F9sS-Uaatuh8lCmVErPkEUzllGlN_0'; 
		$googer = $this->GoogleShort->GoogleURLAPI($key);
		$shortDWName = $this->GoogleShort->shorten("https://davidwalsh.name");
		}
		//pr($this->system_list);
		$_SERVER['REQUEST_SCHEME'] = 'https';
		if ($data['shop_provoz_id'] == 1000){
			
		$url_sms = $_SERVER['REQUEST_SCHEME'].'://www.'.$this->system_list[$data['zdroj_id']].'?show_map='.$this->encode_long_url($id);
			
		} else {
		$url_sms = $_SERVER['REQUEST_SCHEME'].'://www.'.$this->system_list[$data['zdroj_id']].'?show_map='.$this->encode_long_url($id);
			
		}
		//pr($url_sms);
		//$short = $this->make_bitly_url($url_sms,'fastestsolution','R_146672892c7d4182a1c106e7661848d9','json');
		//if ($_SERVER['REMOTE_ADDR'] == '46.135.104.174'){
		//pr('a');;
		$this->loadComponent('GoogleShort');
		$key = 'AIzaSyBL5F9sS-Uaatuh8lCmVErPkEUzllGlN_0';
		$googer = $this->GoogleShort->GoogleURLAPI($key);
		$short = $this->GoogleShort->shorten($url_sms);
		//pr($url_sms);
		//$longDWName = $this->GoogleShort->expand($short);
		//pr($longDWName); // returns https://davidwalsh.name
		//pr($short);die('a');
		//} 
		/*
		if (empty($data['lat'])){
			$short = '';
		}
		*/
		
		//echo 'The short URL is:  '.$short; 
		//if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
		$sms_text = 'Jedeme k Vam :), behem nekolika min. Vam bude volat nas kuryr. Celkova cena objednavky je: '.$data['price_vat'].'Kč ';
		
		//} else {
		//$sms_text = $this->system_id_data['page_title2'].': Jedeme k Vam, behem nekolika min. Vam zavolame a domluvime se na prevzeti objednavky.';
			
		//}
		//Jedeme k Vam :), behem nekolika min. Vam bude volat nas kuryr. Celkova cena objednavky je: 577KÄŤ Vasi objednavku muzete sledovat na http://bit.ly/2CIHQPH
		//if (!empty($short))
		$sms_text .= 'Vasi objednavku muzete sledovat na '.$short;
		if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
		//pr($this->system_id_data);
		pr($sms_text);die();
		die('a');
		
		}
		
		$this->loadComponent('Sms');
		$sms_tel = strtr($data['client_telefon'],['00'=>'']).$data['client_telefon'];
		//pr($sms_tel);die();//pr($sms_text);die();
		
		//if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
		$this->loadComponent('Email');
		$email_to = 'test@fastest.cz';
		$data_email = [
			'SLEDUJ_LINK'=>'ID:'.$id.'<a href="'.$short.'">'.$short.'</a>'
		];
		$opt = [
			'to'=>$email_to,
			'template_id'=>4,
			'data'=>$data_email,
			'price_format'=>[
				'price_vat_per_item',
				'price_payment',
				'price_vat_payment',
				'price_transport',
				'price_vat_transport',
			],
		];
		//pr($opt);die();
		//$this->Email->send($opt);
		//}
		
		if (empty($data->rozvozce_date_vezu)){
		
		$this->Sms->send(1,[$sms_tel],$sms_text);
		}
		
	}
	
	public function getMap($pokladna_id=null){
		if (!isset($this->request->query['render'])){
			die('nemate pristup');
		} else {
			//$this->set('scripts',['/js/RozvozceMap/RozvozceMap']);
		
			$this->set('isMobile',true);
			$this->viewBuilder()->layout("map");
			
			$this->loadModel('Shop.ShopProvozs');
			
			
			$this->provoz_list = $this->ShopProvozs->provozListAll();	
			$this->provoz_list['provoz_list_online_api'][1000] = 'Testovaci';
			if (!isset($this->provoz_list['provoz_list_online_api'][$pokladna_id])){
				die('chybne parametry');
			}
			$this->set('pokladna_name',$this->provoz_list['provoz_list_online_api'][$pokladna_id]);
			$pokladna_id = $this->provoz_list['provoz_api_ids'][$pokladna_id];
			
			$this->set('pokladna_id',$pokladna_id);
			//pr($pokladna_id);die();
				
				//die();
			//pr($find_rozvoces);
		}
	}
	
	public function loadMap($pokladna_id){
		//if ($this->request->is("ajax")){
			$this->autoRender = false;
				$this->viewBuilder()->autoLayout(false);
				$this->loadModel('GpsDispecers');
				$mapper = function ($row, $key, $mapReduce) {
					if (!empty($row->update_gps))
					$row->update_gps = $row->update_gps->format('d.m. H:i');
					$row->count_orders = count($row->mobile_orders);
					$mapReduce->emit($row);
				};
				$this->con_orders = [
					'MobileOrders.rozvozce_date_nevylozeni IS' => null,
					'MobileOrders.rozvozce_date_vylozeni IS' => null,
					'date(MobileOrders.created)' => date('Y-m-d')
				];
				$conDis = [
					'pokladna_id'=>$pokladna_id,
					'date(update_gps)'=>date('Y-m-d'),
				];
				//pr($conDis);
				$query = $this->GpsDispecers->find()
				  ->where($conDis)
					->contain(['MobileOrders'=>function(\Cake\ORM\Query $q) {
						return $q->where($this->con_orders);
					}])
					/*
					->matching('MobileOrders', function(\Cake\ORM\Query $q) { 
						//return $q;
						return $q->where($this->con_orders);
					})
					*/
					->select([
						
						'id',
						'pokladna_id',
						'rozvozce_name',
						'rozvozce_id',
						'lat',
						'lng',
						'update_gps',
						
					])
					
					->mapReduce($mapper);
				  ;
				  
				$find_rozvoces = $query->toArray();
				if (isset($this->request->query['debug']))
				pr($find_rozvoces);
				if ($find_rozvoces){
					die(json_encode(['r'=>true,'data'=>$find_rozvoces]));
				} else {
					die(json_encode(['r'=>false]));
				
				}
				//$this->set('find_rozvoces',$find_rozvoces);
			//}
	}
	
	public function test2($id=null){
		$this->loadModel('Shop.ShopOrders');
		$query = $this->ShopOrders->find()
		  ->contain(['Users'])
		  ->where(['ShopOrders.id'=>$id,'ShopOrders.stav_id'=>6])
		  ->select([
		  ])
		  ->hydrate(false)
		  ;
		$data = $query->first();
		//pr($data);
		//pr($this->system_list);
		$url_sms = $_SERVER['REQUEST_SCHEME'].'://www.'.$this->system_list[$data['zdroj_id']].'?show_map='.$this->encode_long_url($id);
		//pr($url_sms);
		$short = $this->make_bitly_url($url_sms,'fastestsolution','R_146672892c7d4182a1c106e7661848d9','json');
		//echo 'The short URL is:  '.$short; 
		$sms_text = 'Vasi objednavku prave vezeme k Vam rozvozce muzete sledovat na '.$short;
		//pr($sms_text);die();
		//die('a');
		
		$this->loadComponent('Sms');
		$sms_tel = strtr($data['user']['phone_pref'],['00'=>'']).$data['user']['phone'];
		//pr($sms_tel);die();//pr($sms_text);die();
		$this->Sms->send(1,[$sms_tel],$sms_text);
		
		
		die();
	}
	
	function make_bitly_url($url,$login,$appkey,$format = 'xml',$version = '2.0.1'){
		//create the URL
		$bitly = 'http://api.bit.ly/shorten?version='.$version.'&longUrl='.urlencode($url).'&login='.$login.'&apiKey='.$appkey.'&format='.$format;
		
		//get the url
		//could also use cURL here
		$response = file_get_contents($bitly);
		
		//parse depending on desired format
		if(strtolower($format) == 'json')
		{
			$json = @json_decode($response,true);
			return $json['results'][$url]['shortUrl'];
		}
		else //xml
		{
			$xml = simplexml_load_string($response);
			return 'http://bit.ly/'.$xml->results->nodeKeyVal->hash;
		}
	}
}
