<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Core\Configure;
use Cake\Network\Exception\NotFoundException;
use Cake\View\Exception\MissingTemplateException;
use Cake\I18n\Time;
use lessc\lessc;
use Cake\Cache\Cache;


/**
 * Static content controller
 *
 * This controller will render views from Template/Pages/
 *
 * @link http://book.cakephp.org/3.0/en/controllers/pages-controller.html
 */
class ChacharAdminsController extends AppController
{
	
	public function attributes(){
		$this->viewBuilder()->layout("admin");
		$this->loadModel('Shop.ShopProductAttributes');
		$attributes = $this->ShopProductAttributes->newEntity([],['translations' => true]);
		$this->set(compact("attributes"));
		
			$products = $this->ShopProductAttributes->find('translations')
			->where([])
			->order('name_internal ASC')
			->toArray()
			;
			$this->set('products',$products);
			
			//pr($menu);
		//pr($this->shop_menu_items);
		
		$this->set('title','Sprava přídavků');
	}
	
	function authenticate() {
		header('WWW-Authenticate: Basic realm="Prihlaseni"');
		header('HTTP/1.0 401 Unauthorized');
		echo "Musíte být přihlášen\n";
		exit;
	}
	
	function secure_area(){
			$user = 'admin';
			$pass = 'cHachar159';
				
			if ( !isset($_SERVER['PHP_AUTH_USER']) ||
				 (($user != $_SERVER['PHP_AUTH_USER']) || ($pass != ($_SERVER['PHP_AUTH_PW'])))) {
				$this->authenticate();
				return false;
			} else {
				return true;
			}
	}

	public function categories(){
		$this->secure_area();
		$this->set('title','Sprava kategorií');
		$this->viewBuilder()->layout("admin");
		$this->loadModel('Shop.ShopProductCategories');
		$menu_items = $this->ShopProductCategories->newEntity([],['translations' => true]);
		$this->set(compact("menu_items"));
		
			$this->loadModel('Shop.ShopCategories');
			$menu = $this->ShopCategories->find('translations')
			->where([])
			->order('lft ASC')
			->toArray()
			;
			$this->set('menu',$menu);
			
			//pr($menu);
		//pr($this->shop_menu_items);
		
		
	}
	
	public function mapList(){
		
		//$this->secure_area();
		header('Content-Type: text/html; charset=utf-8');
		$this->autoRender = false;
		$this->loadModel('ShopProvozs');
		$provozs = $this->ShopProvozs->pokladnaListAll();
		//pr($provozs);
		echo '<table>';
		echo '<tr>';
			echo '<th>System</th>';
			echo '<th>Nazev</th>';
			echo '<th>ID</th>';
			echo '<th>Provoz id pro mapu</th>';
			echo '<th>Pokladna ID</th>';
			echo '<th>Mapa</th>';
		echo '</tr>';
		foreach($provozs['data'] AS $p){
			if (isset($p->system_id)){
			
			echo '<tr>';
				echo '<td>'.(isset($this->system_list[$p->system_id])?$this->system_list[$p->system_id]:'').'</td>';
				echo '<td>'.$p->name.'</td>';
				echo '<td>'.$p->id.'</td>';
				echo '<td><strong>'.$p->provoz_id.'</strong></td>';
				echo '<td>'.$p->pokladna_id.'</td>';
				echo '<td><a href="http:/\/'.$this->system_list[$p->system_id].'/provoz_gps_map/'.$p->provoz_id.'?render" target="_blank">Mapa</a></td>';
			echo '</tr>';
			}
		}
		
		echo '</table>';
		//pr($provozs);
	}

    public function maps(){
		$this->set('hideMenu',true);
		//$this->secure_area();
		$this->viewBuilder()->layout("admin");
		//$this->set('scripts',['/js/mapAreas/mapAreas.js']);
		
		$this->loadModel('Shop.ShopMapAreas');
		$data = $this->ShopMapAreas->mapAreaList();
		//pr($data);
		//pr($this->system_list);
		$this->set('map_data',$data);
		
		$this->loadModel('Shop.ShopProvozs');
		$this->pokladna_list_load = $this->ShopProvozs->pokladnaListAll();	
		$pokladna_list = [];
		foreach($this->pokladna_list_load['data'] AS $p){
			$p = json_decode(json_encode($p),true);
			//pr($p);
			if (!isset($p['pokladna_id'])) $p['pokladna_id'] = 1000;
			$pokladna_list[$p['pokladna_id']] = $p['name'];
		}
               
               // pr($this->pokladna_list_load);
		$this->set('pokladna_list',$pokladna_list);
	}
	
	public function products(){
		$this->secure_area();
		
		//$this->set('scripts', ['/js/fst_uploader/fst_uploader']);
	
		$this->viewBuilder()->layout("admin");
        $this->set('title',__('Správa produktů'));
		//pr($this->request->query['category_id']);
		//pr($this->system_list);
		$conditions = [
		];
		if (isset($this->request->query['category_id']) && !empty($this->request->query['category_id'])){
			$conditions['ShopProducts.shop_category_id'] = $this->request->query['category_id'];
		}
		if (isset($this->request->query['name']) && !empty($this->request->query['name'])){
			$conditions['ShopProducts.name_internal LIKE'] = '%'.$this->request->query['name'].'%';
		}
		$this->loadModel('Shop.ShopProducts');
		$products = $this->ShopProducts->find()
			->where($conditions)
			->contain([
				'ShopProductPrices' => function ($q) {
					return $q->where(['ShopProductPrices.status' => 1]);
				}
			])
			->order('number ASC')
			//->limit(3)
		;
		$products = $products->toArray();
		//pr($products);
		$this->set('products',$products);
    }
	
	public function saveProduct($id=null){
		$this->secure_area();
		
		$this->loadComponent('Shop.ShopElements');
		$this->ShopElements->loadCategories();
		if ($id != null){
			$this->viewBuilder()->autoLayout(false);
			
			$this->loadModel('Shop.ShopProducts');
			if ($id != 'new'){
			$product = $this->ShopProducts->find('translations')
				->where(['ShopProducts.id'=>$id])
				->contain([
					'ShopProductVariants' => function ($q) {
						return $q;
						//return $q->where(['ShopProductPrices.status' => 1]);
					},
					'ShopProductPrices' => function ($q) {
						return $q;
						//return $q->where(['ShopProductPrices.system_id' => $this->system_id]);
					},
					'ShopProductConAttributes' => function ($q) {
						return $q;
						//return $q->where(['ShopProductPrices.system_id' => $this->system_id]);
					}
				])
				->first()
			;
			//pr($product['shop_product_con_attributes']);
			$prices = [];
			$variants = [];
			$variants_count = [];
			$attributes = [];
			
			if (!empty($product['shop_product_con_attributes'])){
				foreach($product['shop_product_con_attributes'] AS $atr){
					$attributes[$atr->shop_product_attribute_id] = 1;
				}
			}
			$this->set('attributes',$attributes);
			
			foreach($product['shop_product_prices'] AS $k=>$p){
				$prices[$p->system_id] = $p;
			
			}
			foreach($product['shop_product_variants'] AS $k=>$p){
				$variants_count[$p->system_id] = 0;
				$variants[$p->system_id][] = $p;
			
			}
			foreach($variants_count AS $k=>$v){
				$variants_count[$k] = count($variants[$k]);
			}
			//pr($variants_count);
			$this->set('variants_count',$variants_count);
			
			$product['shop_product_prices'] = $prices;
			$product['shop_product_variants'] = $variants;
			//pr($product['shop_product_variants']);
			} else {
				$product = $this->ShopProducts->newEntity([],['translations' => true]);
			}
			$this->loadModel('Shop.ShopProductAttributes');
			$attributes_list = $this->ShopProductAttributes->find('list',['keyField' => 'id','valueField' => 'name'])
				->where([])
				->toArray()
				;
			$this->set('attributes_list',$attributes_list);
			
			
			$this->set(compact("product"));
			//pr($product);
			$this->render('../ChacharAdmins/elements/product_data');
		} else {
			$this->request->data['name'] = $this->request->data['name_internal'];
			$this->request->data['alias'] = $this->createAlias($this->request->data['name_internal']);
			$this->request->data['tax_id'] = 1;
			$this->request->data['price'] = $this->request->data['price_vat'] - ($this->request->data['price_vat']*0.1736);
			
			
			
			$this->loadModel('Shop.ShopProducts');
			$find = $this->ShopProducts->find()
				->where(['ShopProducts.code'=>$this->request->data['code'],'ShopProducts.id !='=>$this->request->data['id']])
				->first()
			;
			if ($find){
				die(json_encode(['r'=>false,'m'=>'Kod je jiz pouzit']));
			}
			
			foreach($this->request->data['shop_product_prices'] AS $k=>$p){
				$this->request->data['shop_product_prices'][$k]['price'] = $this->request->data['shop_product_prices'][$k]['price_vat'] - ($this->request->data['shop_product_prices'][$k]['price_vat']*0.1736);
			}
			
			// attributes
			$attributes = [];
			foreach($this->request->data['shop_product_con_attributes'] AS $at_id=>$value){
				if ($value == 1)
				$attributes[] = [
					'shop_product_attribute_id'=>$at_id,
				];
			}
			$this->request->data['shop_product_con_attributes'] = $attributes;
			
			
			//pr($this->request->data['shop_product_variants']);die();
			$variants = [];
			$delete_list = [];
			foreach($this->request->data['shop_product_variants'] AS $system_id=>$data){
				foreach($data AS $key=>$p){
				if (empty($this->request->data['shop_product_variants'][$system_id][$key]['name'])){
					if (!empty($this->request->data['shop_product_variants'][$system_id][$key]['id'])){
						$delete_list[] = $this->request->data['shop_product_variants'][$system_id][$key]['id'];
					}
					unset($this->request->data['shop_product_variants'][$system_id][$key]);
				} else {
					$this->request->data['shop_product_variants'][$system_id][$key]['tax_id'] = 1;
					$this->request->data['shop_product_variants'][$system_id][$key]['price'] = $this->request->data['shop_product_variants'][$system_id][$key]['price_vat'] - ($this->request->data['shop_product_variants'][$system_id][$key]['price_vat']*0.1736);
				}
				if (isset($this->request->data['shop_product_variants'][$system_id][$key]))
				$variants[] = $this->request->data['shop_product_variants'][$system_id][$key];
				}
			}
			if (!empty($delete_list)){
				$this->loadModel('ShopProductVariants');
				$this->ShopProductVariants->deleteAll(['id IN' => $delete_list]);
			}
			//pr($delete_list);
			//pr($variants);
			//die();
			$this->request->data['shop_product_variants'] = $variants;
			
			if (!empty($this->request->data['images'])){
				$this->request->data['images'] = json_decode($this->request->data['images'][0]);
				//pr($this->request->data['images']);
				$images = [];
				$images[0] = $this->request->data['images'];
				$this->request->data['images'] = json_encode($images);
				//pr($this->request->data['images']);
			}
			
			$save_data = $this->ShopProducts->newEntity($this->request->data,['translations' => true]);
			
			$this->loadModel('Shop.ShopProductConAttributes');
			$this->ShopProductConAttributes->deleteAll(['shop_product_id' => $save_data->id]);
			//pr($this->request->data['shop_product_con_attributes']);
			/*
			unset($save_data->shop_product_variants);
			unset($save_data->shop_product_prices);
			unset($save_data->_translations);
			*/
			//pr($save_data);die();
			$result = $this->ShopProducts->save($save_data);
			//pr($this->request->data);
			
			//pr($result);
			die(json_encode(['r'=>true,'m'=>'Uloženo','redirect'=>(empty($this->request->data['id'])?'self':'')]));
		}
	}
	
	private function createAlias($string){
		$trans = array('-'=>'-',","=>"","á"=>"a", "ä"=> "a","ć"=>"c" ,"č"=>"c", "ď"=>"d", "é"=>"e", "ě"=>"e", "ë"=>"e", "í"=>"i", "&#239;"=>"i", "ň"=>"n", "ó"=>"o", "ö"=>"o", "ř"=>"r", "š"=>"s", "ś"=>"s", "ť"=>"t", "ú"=>"u", "ů"=>"u", "ü"=>"u", "ý"=>"y", "&#255;"=>"y", "ž"=>"z", "Á"=>"A", "Ä"=>"A", "Č"=>"C", "Ď"=>"D", "É"=>"E", "Ě"=>"E", "Ë"=>"E", "Í"=>"I", "&#207;"=>"I", "Ň"=>"N", "Ó"=>"O", "Ö"=>"O", "Ř"=>"R", "Š"=>"S","Ť"=>"T", "Ú"=>"U", "Ů"=>"U", "Ü"=>"U", "Ý"=>"Y", "&#376;"=>"Y", "Ž"=>"Z"," "=>"-","."=>"","/"=>"-","+"=>'-','",'=>'-','"'=>'-','?'=>'',';'=>'','('=>'-',')'=>'-',' - '=>'','&'=>'-','“'=>'-','´'=>'','\''=>'-','"'=>'-',':'=>'-','±'=>'-',','=>'-','='=>'-',', '=>'-',' - '=>'-',"'' ("=>"-",' '=>'-',"' %"=>"-",'“ - '=>'-','“,'=>'-',"' "=>'-','.'=>'','%'=>'','™'=>'','ô'=>'o','ĺ'=>'l','ľ'=>'l','('=>'',')'=>'','ú'=>'u','ä'=>'a','\"'=>'',' \"'=>'','´´'=>'','ľ'=>'l','ľ'=>'l','ĺ'=>'l','ŕ'=>'r','ô'=>'o','е'=>'e','ą'=>'a','Э'=>'e','®'=>'', '!'=>'',"\n"=>'-','Ą'=>'a');
		$trans2 = array('-–-'=>'-','--'=>'-','---'=>'-','\-'=>'-','ľ'=>'l','ľ'=>'l','ĺ'=>'l','ľ'=>'l','ô'=>'o','Ł'=>'L','ł'=>'l','ę'=>'e','ż'=>'z','Ы'=>'N','ы'=>'n','»'=>'','«'=>'','э'=>'e','|'=>'');
		
		$string2 = strtolower(strtr($string, $trans));
		$return = strtolower(strtr($string2, $trans2));
		
		
		//$return = $this->transliterate($return);
		
		return $return;
	}
	

	public function saveMenu(){
		
		$this->loadModel('Shop.ShopCategories'); 
		
		//pr($this->request->data());
		foreach($this->request->data['_translations'] AS $key=>$data){
			
			foreach($data AS $lang=>$d){
				$this->request->data['_translations'][$key][$lang]['alias'] = $this->createAlias($this->request->data['_translations'][$key][$lang]['name']);
				foreach($this->system_list AS $system_id=>$system_name){
					$this->request->data['_translations'][$key][$lang.$system_id]['name'] = $this->request->data['_translations'][$key][$lang]['name'];
					$this->request->data['_translations'][$key][$lang.$system_id]['alias'] = $this->createAlias($this->request->data['_translations'][$key][$lang]['name']);
				}
		
			}
		}
		//pr($this->request->data);
		//pr($cats);
        //debug($splits);

        //foreach($this->request->data['shop_categories'] AS $key=>$data){
        $this->request->data['sort'] = json_decode($this->request->data['sort']);
		foreach($this->request->data['sort'] AS $sort){
			$sort = strtr($sort,['li'=>'']);
			//pr($sort);
			//pr($sort);die();
			$this->request->data['shop_categories'][$sort]['level'] = 1;
			$this->request->data['shop_categories'][$sort]['parent_id'] = null;
			//pr($this->request->data['shop_categories'][$sort]);
			$cats = $this->ShopCategories->newEntity($this->request->data['shop_categories'][$sort],['validation'=>false]);
			foreach($this->request->data['_translations'][$sort] AS $key_tran=>$trans){
				$cats->translation($key_tran)->set($trans);
			}
			//pr($cats);
			$res = $this->ShopCategories->save($cats);
		}
		//pr($this->request->data);
		die(json_encode(['r'=>true,'m'=>'Uloženo']));
	}

	public function deleteProducts($id){
		$this->loadModel('ShopProducts');
		$entity = $this->ShopProducts->get($id);
		$result = $this->ShopProducts->delete($entity);
		die(json_encode(['r'=>true]));
	}

	public function saveAttributes(){
		
		$this->loadModel('Shop.ShopProductAttributes'); 
		
		//pr($this->request->data());
		foreach($this->request->data['_translations'] AS $key=>$data){
			
			foreach($data AS $lang=>$d){
				foreach($this->system_list AS $system_id=>$system_name){
					$this->request->data['_translations'][$key][$lang.$system_id]['name'] = $this->request->data['_translations'][$key][$lang]['name'];
				}
		
			}
		}
		//pr($this->request->data);
		//pr($cats);
        //debug($splits);
		$delete_list = [];
		foreach($this->request->data['shop_product_attributes'] AS $key=>$data){
        	//pr($this->request->data['shop_categories'][$sort]);
			$this->request->data['shop_product_attributes'][$key]['name_internal'] = $this->request->data['_translations'][$key]['cz']['name'];
			
			$cats = $this->ShopProductAttributes->newEntity($this->request->data['shop_product_attributes'][$key],['validation'=>false]);
			foreach($this->request->data['_translations'][$key] AS $key_tran=>$trans){
				$cats->translation($key_tran)->set($trans);
			}
			$delete_list[] = $cats->id;
			
			//pr($cats);
			$res = $this->ShopProductAttributes->save($cats);
		}
		$find = $this->ShopProductAttributes->find('list',['keyField' => 'id','valueField' => 'id'])
			->where(['ShopProductAttributes.id NOT IN'=>$delete_list])
			->toArray()
			;
		if ($find){
			$this->ShopProductAttributes->deleteAll(['ShopProductAttributes.id IN' => $find]);
		}
		//pr($delete_list);
		//pr($this->request->data);
		die(json_encode(['r'=>true,'m'=>'Uloženo']));
	}
	
}
