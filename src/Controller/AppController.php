<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Database\Schema\Table;
use Cake\Event\Event;

// jazyky pro preklad
use Cake\I18n\I18n;
use Cake\I18n\Date;
use Cake\I18n\Time;
//use Cake\I18n\FrozenDate;
use Cake\Core\Configure;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Cache\Cache;
use Cake\View\CellTrait;
use Cake\Controller\Component\CookieComponent;

Date::$defaultLocale = 'cs-CZ';
Date::setToStringFormat('dd.MM.yyyy');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{
	var $pagination_limit = 54;
	var $conditions_trans = [
		'name',
		'alias',
		'description',
	];
	
	
	private function genJwt(){
		require_once('Component/JwtComponent.php');
		$token = array();
		$token['id'] = 'chachar159';
		$hashToken = \JWT::encode($token, 'secret_server_key');
		//pr($hashToken);
		//$decodeToken = \JWT::decode($_POST['token'], 'secret_server_key');
	//	$decodeToken = \JWT::decode($hashToken, 'secret_server_key');
		//pr($decodeToken);
	}
	private function checkJwt(){
		require_once('Component/JwtComponent.php');
		$jsonStr = file_get_contents("php://input");
		//$_POST = json_decode($jsonStr);
		$json = json_decode($jsonStr,true);
		$_POST = $json;
		$_POST['token'] = 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpZCI6ImNoYWNoYXIxNTkifQ.iK3DHOfvsx6qr2q2rhfoQLlvDuPi7dJ7Hl9roGv5z6s'; 
		
		if (!isset($_POST['token'])){
			die('neni token');
		} else {
			$decodeToken = \JWT::decode($_POST['token'], 'secret_server_key');
			if (!isset($decodeToken)){
				die('chyba token');
			}
		}
		//pr($decodeToken);
	}
	
    public function beforeFilter(Event $event)
    {
		//if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
		if ($this->request->params['controller'] == 'Chatbots'){
			
			$this->checkJwt();
		}
		//}
		$this->Session = $this->request->session();
		//$this->Session->write('test','test');
		//pr($this->Session->read('test'));
		//pr(date('Y-m-d H:i:s'));
		//pr($_SESSION);
		if(in_array($this->request->params['action'],['test','apiAdd']));
			$this->eventManager()->off($this->Csrf);
		
		//pr($this->request->params['action']);
		$this->Security->config('unlockedFields', ['spam','step','shop_transport_id','shop_payment_id','user_addresses.0.id','shop_product_variants','name','spropitne','credit_use','credit_use_after']);
		$this->Security->config('unlockedActions', [
			'fstUpload',
			'saveMenu',
			'saveAttributes',
			'addApi',
			'newOrder',
			'rozvoceAdd',
			'rozvoceAddOrder',
			'addBasket',
			'add_basket',
			'logGps',
			'findClient',
			'saveProduct',
			'users',
			'orders',
			'address',
		]);
		//pr($_SESSION);
		
	    // load JS and CSS from MIN JS
		$this->load_js();
		
		
		// shop session
		if (Configure::read('modul_shop')){
			$this->set('basket_products',$this->basket_products = $this->Session->read('basket_products'));
		}
		
		if (isset($this->request->query['show_map']) && !empty($this->request->query['show_map'])){
			$map_id = $this->decode_long_url($this->request->query['show_map']);
			if (isset($map_id)){
				$this->set('map_id',$map_id);
			}
			//pr($map_id);
		}
		
		//pr($_SESSION);die();
		return parent::beforeFilter($event);
    }
	
	
    
	public function initialize()
    {
        parent::initialize();
		//if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
			if ($_SERVER['REQUEST_SCHEME'] == 'http'){
				$redirect = strtr($_SERVER['HTTP_HOST'],['http'=>'https']);
				//pr($redirect);
				//$this->redirect($redirect,[],['status' => 301]);
			}
			//pr($_SERVER); 
			//die('a');
		//}
		
		$this->Session = $this->request->session();
		//$session = $this->request->session();
		//pr($this->Session->read('loggedUser'));
		//pr($this->Session);
		// load select config
		require_once('../src/chachar_config.php');
		
		$this->load_select_config();
		
		if (isset($this->dev_project)){
			if (preg_match('/bot|crawl|curl|dataprovider|search|get|spider|find|java|majesticsEO|google|yahoo|teoma|contaxe|yandex|libwww-perl|facebookexternalhit/i', $_SERVER['HTTP_USER_AGENT'])) {
				// is bot
				die('bot');
			} else {
				
			}
			
		}
		if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
		//pr($this->system_id);
		
		}
		//pr($this->system_id);
		//pr($this->request->query['new']);
		//if (in_array($this->system_id,$this->new_address_list) || isset($this->request->query['new'])){
			$this->Session->write('new_address_list',$this->new_address_list);
			if (!in_array($_SERVER['SERVER_NAME'],['www.cinajede.cz','www.pizzarecords.cz']))
			$this->set('new_address',$this->new_address_list);
			
		//}
		
		if ($this->request->url == '' || isset($this->request->query['template'])){
			if (isset($this->request->query['template'])){
				$this->redirect_template = $this->request->query['template'];
				$this->set('redirect_template',$this->redirect_template);
				//pr($this->redirect_template);
			}
			$this->set('on_main_page',true);
			$addr_list = [
				1, // chachar
				2, // manik
				9, // partak
				10, //boleslav
				14, // cina
				15, // records
			];
			//if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
			$this->loadModel('Shop.ShopProvozs');
			$this->provoz_list = $this->ShopProvozs->provozListAll();	
			//pr($this->provoz_list);
			
			if (isset($this->request->query['provoz'])){
				if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
					//$this->Cookie->write('provoz_id', $this->request->query['provoz']);
					setcookie('provoz_id', $this->request->query['provoz'], time() + (86400 * 30));
					//pr($_COOKIE['provoz_id']);
					//die();
				}
			}
			if (isset($_COOKIE['provoz_id']) && isset($_COOKIE['addressChachar']) && !empty($_COOKIE['addressChachar']) && isset($this->provoz_list['provoz_list'][$_COOKIE['provoz_id']])){
				//pr($_COOKIE);
				
			} else {
				if (in_array($this->system_id,$addr_list)){
					$this->set('addr',true);
				}
					
			}
			
			if ($this->system_id == 1){
				if ($_SERVER['REQUEST_SCHEME'] == 'http'){
					
					//$this->redirect('https://www.chachar.cz',301);
				}
			}
			//pr($this->system_id);
			//pr($_SERVER);
			//pr($this->new_address_list);
			$this->Session->write('system_id',$this->system_id);
			$this->Session->write('new_address_list',$this->new_address_list);
			/*
			} else {
			
			if (in_array($this->system_id,$addr_list)){
				$this->set('addr',true);
			}
			
			}
			*/
		}
		
		
		//pr(Configure::version());
		
		// presmerovani na burgery
		if (isset($this->request->query['records'])){
			//pr($this->request->query);
			$this->redirect('/burgery/?provoz='.(isset($this->request->query['provoz'])?$this->request->query['provoz']:''));
		}
		// presmerovani na cina
		if (isset($this->request->query['cina'])){
			$this->redirect('/cina/?provoz='.(isset($this->request->query['provoz'])?$this->request->query['provoz']:''));
		}
		//pr($this->request->query);
		
		$this->loginComponent();
		
        $this->loadComponent('Paginator',['limit' => $this->pagination_limit]);
        $this->loadComponent('RequestHandler');
        //$this->loadComponent('Flash');
        $this->loadComponent('Csrf');
		
        $this->loadComponent('Security');
        
		if (isset($_COOKIE['rozcesnik_hide_'.$this->system_id])){
			$this->set('rozcesnik_hide',true);
			
			
		}
		//pr($this->request);
		
		// load shop elements
		if (Configure::read('modul_shop')){
			$this->set('modul_shop',true);
			$enable_controllers = [
				'ShopProducts'=>'index',
			];
			if(!$this->request->is("ajax") || (in_array($this->request->params['action'],$enable_controllers) && isset($enable_controllers[$this->request->params['controller']]))){
				$this->loadComponent('Shop.ShopElements');
				$this->ShopElements->loadCategories();
				//die('aa');
				//pr('load');
			}
			
			
		} 
		// pokud je ajax request
		if($this->request->is("ajax")){
          //$this->viewBuilder()->layout("ajax");
          $this->viewBuilder()->autoLayout(false);
			
		} else {
			
			// render css file from LESS
			if (!$this->request->is("ajax")){
				if ($this->system_id == 2){
					$this->renderCss();
					
				}
			}
			// load menu items
			//$this->loadMenuItems();
		
			//$this->loadProvozList();	
			// check online provoz
			$this->onlineProvoz();
		
        }
		$this->set('system_id',$this->system_id);
		
		
		$this->check_cache();
		
		//if ($_SERVER['REMOTE_ADDR'] == '46.135.104.174'){
			$this->set('new',true);
		//}
		
		$this->response->cache('-1 minutes','+5 days');
    }
	
	private function loadProvozList(){
		//$this->loadModel('ShopProvozs');
        $conditions = ['ShopProvozs.status'=>1];
		//$conditions = [];
		//pr($conditions);
		$query = $this->ShopProvozs->find()
			->select([
				'id',
				'provoz_id',
				'name',
				'name2',
				'telefon',
				'doba',
				'lokalita',
				'order_email',
				'poradi',
			])
			->where($conditions)
			->order('poradi ASC');
			//->cache(function ($query) {
				//return 'provozsCell-' . md5(serialize($query->clause('where'))).md5(serialize($query->clause('order'))).md5(serialize($query->clause('limit')));
			//});
			//pr(Cache::read('provoz_list'.$_SESSION['lang']));
		//if (($provoz_list = Cache::read('provoz_list'.$_SESSION['lang'])) === false) {	
			//$provoz_list = $query->toArray();
			//Cache::write('provoz_list'.$_SESSION['lang'], $provoz_list);
		//}
		$provoz_list = $query->toArray();
		pr($provoz_list);
		
		
		if (isset($provoz_list) && count($provoz_list)>0){
			$this->set('provoz_list', $provoz_list);
    	
		}
	}
	
	private function onlineProvoz(){
		$this->loadModel('Shop.ShopProvozs');
		
		$query = $this->ShopProvozs->find('list')
		  ->where(['online'=>0,'status'=>1])
		  ->select([
			'name'
		  ]);
		$load = $query->toArray();
		//pr($load);
		if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){
			//$load = ['Slezska'];
		}
		if (!empty($load)){
			$provoz_list = implode(', ',$load);
			$provoz_count = count($load);
			//if ($_SERVER['REMOTE_ADDR'] == '89.103.18.65'){ 
			if (in_array(date('Y-m-d'),['2017-12-24','2017-12-25','2017-12-26','2017-12-31'])){
			
			$provoz_disable_online = (($provoz_count == 1)?__('Provozovna'):__('Provozovny')).' <strong>'.$provoz_list.'</strong> '.__('{0,plural,=0{} =1{ je } other{ jsou }}   z důvodu svátků ZAVŘENA. Děkujeme za pochopení',$provoz_count); 
			//}
			} else {
			
			$provoz_disable_online = (($provoz_count == 1)?__('Provozovna'):__('Provozovny')).' <strong>'.$provoz_list.'</strong> '.__('{0,plural,=0{} =1{ momentálně příjmá} other{ momentálně příjmájí}}   pouze telefonické objednávky. Děkujeme za pochopení',$provoz_count); 
			}
			//pr($provoz_disable_online);
			$this->set('provoz_disable_online',$provoz_disable_online);
			//pr($provoz_list);
		}
		//Provozovna Hlučín 
	}
	
	// json result 
	public function jsonResult($result){
		$this->response->cache('-1 minutes','+5 days');
		$this->response->type('application/json');
		$this->response->body(json_encode($result)) ;
		return  $this->response;
			
	}
	
	
	// clear cache if mysql change
	public function check_cache(){
		$conn = ConnectionManager::get('default');
		$rs = $conn->execute('SHOW TABLE STATUS');
		
		$rows = $rs->fetchAll('assoc');
		
		$updated = '';  
		$disable_check_table = [
			'shop_orders',
			'shop_order_items',
			'shop_clients',
			'shop_client_addresses',
			'users',
		];
		foreach($rows AS $row){
			if ($updated < $row['Update_time'] && !in_array($row['Name'],$disable_check_table)){
				$updated = $row['Update_time'];
			}
		}
		$file = "../tmp/cache_date.php";
		$myfile = fopen($file, "r") or die("Unable to open file!");
		if (filesize($file) > 0){
			$date = fread($myfile,filesize($file));
			if ($date < $updated){
				//pr('clear');
				//pr($updated);
				$myfileW = fopen($file, "w") or die("Unable to open file!");
				fwrite($myfileW, $updated);
				Cache::clear(false);
				
			} else {
			}
		} else {
			fwrite($myfile, $updated);
		}
		fclose($myfile);
		
	}
	
	
	// convert condition from translate
	public function convert_trans_conditions($model,$condititions){
		$new_con = [];
		
		foreach($condititions AS $key=>$value){
			$key_split = explode(' ',$key);
			
			if (in_array($key_split[0],$this->conditions_trans)){
				$new_con[] = [$model.'_'.$key_split[0].'_translation.content'.(isset($key_split[1])?' '.$key_split[1]:'') => $value];
			} else {
				$new_con[] = [$key_split[0].' '.(isset($key_split[1])?' '.$key_split[1]:'') => $value];
			}
		}
		return $new_con;
		//pr($new_con);
	}
	
	
    private function renderCss(){
    	// run compile LESS to CSS
		$ch = curl_init();
		$link = $_SERVER['REQUEST_SCHEME']."://".$_SERVER['HTTP_HOST']."/compile.php?render";
		curl_setopt($ch, CURLOPT_URL, $link);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_exec($ch);
		curl_close($ch);
		
	}
	
	
	private function loginComponent(){
		$this->loadComponent('Auth', [
			'loginAction' => [
				'controller' => 'Users',
				'action' => 'login'
			],
			
			'authError' => __('Nemáte přístup na tuto stránku'),
			'authenticate' => [
				'Form' => [
					'fields' => ['username' => 'email', 'password' => 'password'],
					
				]
			]
		]);
		$this->Auth->allow();
		//pr($_SESSION);
		//pr($this->Auth->user());
		if ($this->Auth->user()){
			$this->set('loggedUser',$this->loggedUser = $this->Auth->user());
			//pr($this->loggedUser);
		}
	}
	
	private function loadMenuItems(){
		$this->loadModel('MenuItems');
			$query = $this -> MenuItems -> find('threaded')
			->where([])
			->cache(function ($query) {
					return 'load_menu_items-' . md5(serialize($query->clause('where')));
			});
			$this->menu_items = $query->toArray();
		
		$this->menu_items_path = $this->MenuItems->getFullPath();
		$this->set('menu_items_path',$this->menu_items_path);
		$this->set('menu_items',$this->menu_items);
	}
	
	public function error404(){
		$this->response->statusCode(404);
		$this->set('title',__('Chyba 404 - stránka nenalezena'));
		
		return $this->render('/Pages/error404');
		
	}
	
	private function load_select_config(){
		$setting = [
			'page_title'=>$this->system_id_data['page_title'],
			'description'=>__('Rozvoz originální české pizzy').' '.$this->system_id_data['seo_category_city'].', '.$this->system_id_data['page_title'],
			'keywords'=>'rozvoz pizzy '.$this->system_id_data['seo_category_city'],
			'ga_superadmin'=>$this->system_id_data['ga_superadmin'],
			'ga_verification'=>$this->system_id_data['ga_verification'],
			'heureka_overeno'=>'',
			'system_email'=>'test@fastest.cz',
			'email_podpis'=>'<p>S pozdravem Vaše Pizza pro tebe</p>',
			'domena'=>$_SERVER['HTTP_HOST']
		];
		$seo = [];
		$seo['img'] = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'].'/css/logo/'.$this->css_select['logo'].'.png';
		$this->set('seo',$seo);
		
		$this->system_email = $setting['system_email'];
		$this->setting = $setting;
		
		$this->load_languages();
		
		$this->set('setting',$setting);
		$sc = Configure::read('select_config');
		foreach($sc AS $k=>$item){
			$this->$k = $item;
			$this->set($k,$item);
		}
		
		if (Configure::read('shop_select_config')){
		
			$sc = Configure::read('shop_select_config');
			foreach($sc AS $k=>$item){
				$this->$k = $item;
				$this->set($k,$item);
			}
		}
	}
	
	// load languages params and setting
	function load_languages(){
		if (isset($this->request->params['lang']) && !empty($this->request->params['lang'])){
			$this->lang = $this->request->params['lang'];
		} else {
			$this->lang = 'cz'.$this->system_id;
		}
		//pr($this->lang);
		//pr($this->request->params);
		
		
		
		$this->set('lang',$this->lang);
		
		$this->url_lang_prefix = ((Configure::read('default_language') != $this->lang)?'/'.$this->lang:'');
		
		if (substr($this->lang,0,2) == 'cz'){
			$this->url_lang_prefix = '';
		}
		
		$this->set('url_lang_prefix',$this->url_lang_prefix);
		
		$this->Session->write('url_lang_prefix',$this->url_lang_prefix);
		$this->Session->write('lang',$this->lang);

		I18n::locale($this->lang);
		
	}

	
	
	/*** ENCODE LONG URL **/	
	public function encode_long_url($data){
         return $output = strtr(base64_encode(addslashes(gzcompress(json_encode($data),9))), '+/=', '-_,');    
    }
    /*** DECODE LONG URL **/	
	public function decode_long_url($data){
    	$data2 = explode('?',$data);
		$data = $data2[0];
		return json_decode(@gzuncompress(stripslashes(base64_decode(strtr($data, '-_,', '+/=')))),true);
    }
	


    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return void
     */
    public function beforeRender(Event $event)
    {               
	    if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }
    
	
	// check error if form save
	public function check_error($entity=null){
		
		if (!isset($entity->spam) || $entity->spam != 123){
			die(json_encode(['r'=>false,'m'=>'Jste spam']));
		}
			
		//pr($entity);die();
		
		if ($entity != null && $entity->errors()){
			$message = '';
			$invalid_list = array();
			//pr($entity->errors());
			$convert_valid = [
				'date_'=>'date-',
				'user_'=>'user-',
			];
			//pr($entity->errors());
			foreach($entity->errors() AS $k=>$mm){
				foreach($mm AS $mk=>$m){
					if (is_array($m)){
						$invalid_list[] = $k.'-'.strtr($mk,$convert_valid);
						foreach($m AS $kk=>$m2){
							if (is_array($m2)){
								foreach($m2 AS $mk2=>$m3){
									//pr($kk);
									if (is_array($m3)){
										$invalid_list[] = $k.'-'.$mk.'-'.$kk.'-'.strtr($mk2,$convert_valid);
										
										foreach($m3 AS $mm3){
											$message.= $mm3.'<br />';
										}
									} else {
										//pr($kk);
										$invalid_list[] = $k.'-'.$mk.'-'.strtr($kk,$convert_valid);
										$message.= $m3.'<br />';
										
									}
								}
							} else {
								$message .= $m2.'<br />';
							
							}
							
						}
						
					} else {
						$message .= $m.'<br />';
						
					}
					
				}
				$invalid_list[] = strtr($k,$convert_valid);
			}
			die(json_encode(['r'=>false,'m'=>$message,'invalid'=>$invalid_list]));
		}
	}
	
	private function load_js(){
        require_once('min/lib/Minify.php');
        require_once('min/lib/Minify/Source.php');
        require_once('min/utils.php');
		$debug_list_load = include('./min/groupsConfig.php');
		if (Configure::read('compress_html')){
			$this->set('compress_html',true);
			
			if ($this->request->params['controller'] == 'Mobiles'){
				$jsUriMobile = Minify_getUri('js_mobile'); // a key in groupsConfig.php
				$this->set('jsUriMobile',$jsUriMobile.JS_VERSION);		
			
				$cssUriMobile = Minify_getUri('css_mobile'); // a key in groupsConfig.php
				$this->set('cssUriMobile',$cssUriMobile.CSS_VERSION);
			
			} else {
			
			$jsUri = Minify_getUri('js'); // a key in groupsConfig.php
			$this->set('jsUri',$jsUri.JS_VERSION);		
			$cssUri = Minify_getUri('css'); // a key in groupsConfig.php
			$this->set('cssUri',$cssUri.CSS_VERSION);
			
			
			
			//pr($cssUri);
		
			}
			//Cache::disable();
			Cache::enable();
		} else {
			Cache::disable();
			//Cache::enable();
			$debug_css = [
				'css'=>$debug_list_load['css_links'],
				'js'=>$debug_list_load['js_links'],
				'css_mobile'=>$debug_list_load['css_links_mobile'],
				'js_mobile'=>$debug_list_load['js_links_mobile'],
			];
			$this->set('debug_css',$debug_css);
		}
		//pr(Configure::read('compress_html'));die();
		//unset(debug_list_load)
				
    }
	
	
	public function debug(){
		$this->viewBuilder()->autoLayout(true);
		$this->viewBuilder()->layout('debug');
		$this->render('./Pages/debug');
		
	}
}
