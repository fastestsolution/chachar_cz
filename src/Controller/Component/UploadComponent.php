<?php 

namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\Entity;
use Cake\ORM\TableRegistry;
use Cake\Utility\Hash;
class UploadComponent extends Component{
	
	var $controller = null;
	var $data_upload = null;//$_FILES['upload_file'];
	var $filename = '';
	var $filename_type = 'unique'; // unique|alias|name
	var $basicUploadPath = '';
	var $tmpFilename = '';
	var $outputFilename = '';
	var $enabledExt = array('jpg',);
	var $error_message = '';
	var $mime_types = array(
		'stl' 		=>	array('application/SLA'),
		'step' 		=>	array('application/STEP'),
		'stp' 		=>	array('application/STEP'),
		'dwg' 		=>	array('application/acad'),
		'ez' 		=>	array('application/andrew-inset'),
		'ccad' 		=>	array('application/clariscad'),
		'drw' 		=>	array('application/drafting'),
		'tsp' 		=>	array('application/dsptype'),
		'dxf' 		=>	array('application/dxf'),
		'xls' 		=>	array('application/excel'),
		'unv' 		=>	array('application/i-deas'),
		'jar' 		=>	array('application/java-archive'),
		'hqx' 		=>	array('application/mac-binhex40'),
		'cpt' 		=>	array('application/mac-compactpro'),
		'pot' 		=>	array('application/vnd.ms-powerpoint'),
		'pps' 		=>	array('application/vnd.ms-powerpoint'),
		'ppt' 		=>	array('application/vnd.ms-powerpoint'),
		'ppz' 		=>	array('application/vnd.ms-powerpoint'),
		'doc' 		=>	array('application/msword'),
		'bin' 		=>	array('application/octet-stream'),
		'class' 	=>	array('application/octet-stream'),
		'dms' 		=>	array('application/octet-stream'),
		'exe' 		=>	array('application/octet-stream'),
		'lha' 		=>	array('application/octet-stream'),
		'lzh' 		=>	array('application/octet-stream'),
		'oda' 		=>	array('application/oda'),
		'ogg' 		=>	array('application/ogg'),
		'ogm' 		=>	array('application/ogg'),
		'pdf' 		=>	array('application/pdf'),
		'pgp' 		=>	array('application/pgp'),
		'ai' 		=>	array('application/postscript'),
		'eps' 		=>	array('application/postscript'),
		'ps' 		=>	array('application/postscript'),
		'prt' 		=>	array('application/pro_eng'),
		'rtf' 		=>	array('application/rtf'),
		'set' 		=>	array('application/set'),
		'smi' 		=>	array('application/smil'),
		'smil' 		=>	array('application/smil'),
		'sol' 		=>	array('application/solids'),
		'vda' 		=>	array('application/vda'),
		'mif' 		=>	array('application/vnd.mif'),
		'xlc' 		=>	array('application/vnd.ms-excel'),
		'xll' 		=>	array('application/vnd.ms-excel'),
		'xlm' 		=>	array('application/vnd.ms-excel'),
		'xls' 		=>	array('application/vnd.ms-excel'),
		'xlw' 		=>	array('application/vnd.ms-excel'),
		'cod' 		=>	array('application/vnd.rim.cod'),
		'arj' 		=>	array('application/x-arj-compressed'),
		'bcpio' 	=>	array('application/x-bcpio'),
		'vcd' 		=>	array('application/x-cdlink'),
		'pgn' 		=>	array('application/x-chess-pgn'),
		'cpio' 		=>	array('application/x-cpio'),
		'csh' 		=>	array('application/x-csh'),
		'deb' 		=>	array('application/x-debian-package'),
		'dcr' 		=>	array('application/x-director'),
		'dir' 		=>	array('application/x-director'),
		'dxr'		=>	array('application/x-director'),
		'dvi' 		=>	array('application/x-dvi'),
		'pre' 		=>	array('application/x-freelance'),
		'spl' 		=>	array('application/x-futuresplash'),
		'gtar' 		=>	array('application/x-gtar'),
		'gz' 		=>	array('application/x-gunzip'),
		'gz' 		=>	array('application/x-gzipv'),
		'hdf' 		=>	array('application/x-hdf'),
		'ipx' 		=>	array('application/x-ipix'),
		'ips' 		=>	array('application/x-ipscript'),
		'js' 		=>	array('application/x-javascript'),
		'skd' 		=>	array('application/x-koan'),
		'skm' 		=>	array('application/x-koan'),
		'skp' 		=>	array('application/x-koan'),
		'skt' 		=>	array('application/x-koan'),
		'latex' 	=>	array('application/x-latex'),
		'lsp' 		=>	array('application/x-lisp'),
		'scm' 		=>	array('application/x-lotusscreencam'),
		'mif' 		=>	array('application/x-mif'),
		'bat' 		=>	array('application/x-msdos-program'),
		'com' 		=>	array('application/x-msdos-program'),
		'exe' 		=>	array('application/x-msdos-program'),
		'cdf' 		=>	array('application/x-netcdf'),
		'nc' 		=>	array('application/x-netcdf'),
		'pl' 		=>	array('application/x-perl'),
		'pm' 		=>	array('application/x-perl'),
		'rar' 		=>	array('application/x-rar-compressed'),
		'sh' 		=>	array('application/x-sh'),
		'shar' 		=>	array('application/x-shar'),
		'swf' 		=>	array('application/x-shockwave-flash'),
		'sit' 		=>	array('application/x-stuffit'),
		'sv4cpio' 	=>	array('application/x-sv4cpio'),
		'sv4crc' 	=>	array('application/x-sv4crc'),
		'tar.gz' 	=>	array('application/x-tar-gz'),
		'tgz' 		=>	array('application/x-tar-gz'),
		'tar' 		=>	array('application/x-tar'),
		'tcl' 		=>	array('application/x-tcl'),
		'tex' 		=>	array('application/x-tex'),
		'texi' 		=>	array('application/x-texinfo'),
		'texinfo' 	=>	array('application/x-texinfo'),
		'man' 		=>	array('application/x-troff-man'),
		'me' 		=>	array('application/x-troff-me'),
		'ms' 		=>	array('application/x-troff-ms'),
		'roff' 		=>	array('application/x-troff'),
		't' 		=>	array('application/x-troff'),
		'tr' 		=>	array('application/x-troff'),
		'ustar' 	=>	array('application/x-ustar'),
		'src' 		=>	array('application/x-wais-source'),
		'zip' 		=>	array('application/x-zip-compressed','application/octet-stream','application/zip'),
		'tsi' 		=>	array('audio/TSP-audio'),
		'au' 		=>	array('audio/basic'),
		'snd' 		=>	array('audio/basic'),
		'kar' 		=>	array('audio/midi'),
		'mid' 		=>	array('audio/midi'),
		'midi' 		=>	array('audio/midi'),
		'mp2' 		=>	array('audio/mpeg'),
		'mp3' 		=>	array('application/x-download',"audio/mpeg",'application/force-download','audio/mp3'),
		'mpga' 		=>	array('audio/mpeg'),
		'au' 		=>	array('audio/ulaw'),
		'aif' 		=>	array('audio/x-aiff'),
		'aifc' 		=>	array('audio/x-aiff'),
		'aiff' 		=>	array('audio/x-aiff'),
		'm3u' 		=>	array('audio/x-mpegurl'),
		'wax' 		=>	array('audio/x-ms-wax'),
		'wma' 		=>	array('audio/x-ms-wma'),
		'rpm' 		=>	array('audio/x-pn-realaudio-plugin'),
		'ram' 		=>	array('audio/x-pn-realaudio'),
		'rm' 		=>	array('audio/x-pn-realaudio'),
		'ra' 		=>	array('audio/x-realaudio'),
		'wav' 		=>	array('audio/x-wav'),
		'pdb' 		=>	array('chemical/x-pdb'),
		'xyz' 		=>	array('chemical/x-pdb'),
		'ras' 		=>	array('image/cmu-raster'),
		'gif' 		=>	array('image/gif'),
		'ief' 		=>	array('image/ief'),
		'jpe' 		=>	array('image/jpeg'),
		'jpeg' 		=>	array('image/jpeg'),
		'jpg' 		=>	array('image/jpeg','image/pjpeg'),
		'png' 		=>	array('image/png'),
		'tif' 		=>	array('image/tiff'),
		'tiff' 		=>	array('image/tiff'),
		'ras' 		=>	array('image/x-cmu-raster'),
		'pnm' 		=>	array('image/x-portable-anymap'),
		'pbm' 		=>	array('image/x-portable-bitmap'),
		'pgm' 		=>	array('image/x-portable-graymap'),
		'ppm' 		=>	array('image/x-portable-pixmap'),
		'rgb' 		=>	array('image/x-rgb'),
		'xbm' 		=>	array('image/x-xbitmap'),
		'xpm' 		=>	array('image/x-xpixmap'),
		'xwd' 		=>	array('image/x-xwindowdump'),
		'iges' 		=>	array('model/iges'),
		'igs' 		=>	array('model/iges'),
		'mesh' 		=>	array('model/mesh'),
		'msh' 		=>	array('model/mesh'),
		'silo' 		=>	array('model/mesh'),
		'vrml' 		=>	array('model/vrml'),
		'wrl' 		=>	array('model/vrml'),
		'css' 		=>	array('text/css'),
		'htm' 		=>	array('text/html'),
		'html' 		=>	array('text/html'),
		'asc' 		=>	array('text/plain'),
		'c' 		=>	array('text/plain'),
		'cc' 		=>	array('text/plain'),
		'f90' 		=>	array('text/plain'),
		'f' 		=>	array('text/plain'),
		'h' 		=>	array('text/plain'),
		'hh' 		=>	array('text/plain'),
		'm' 		=>	array('text/plain'),
		'txt' 		=>	array('text/plain'),
		'rtx' 		=>	array('text/richtext'),
		'rtf' 		=>	array('text/rtf'),
		'sgm' 		=>	array('text/sgml'),
		'sgml' 		=>	array('text/sgml'),
		'tsv' 		=>	array('text/tab-separated-values'),
		'jad' 		=>	array('text/vnd.sun.j2me.app-descriptor'),
		'etx' 		=>	array('text/x-setext'),
		'xml' 		=>	array('text/xml'),
		'dl' 		=>	array('video/dl'),
		'fli' 		=>	array('video/fli'),
		'flv' 		=>	array('video/flv'),
		'gl' 		=>	array('video/gl'),
		'mp2' 		=>	array('video/mpeg'),
		'mp4' 		=>	array('video/mp4'),
		'mpe' 		=>	array('video/mpeg'),
		'mpeg' 		=>	array('video/mpeg'),
		'mpg' 		=>	array('video/mpeg'),
		'mov' 		=>	array('video/quicktime'),
		'qt' 		=>	array('video/quicktime'),
		'viv' 		=>	array('video/vnd.vivo'),
		'vivo' 		=>	array('video/vnd.vivo'),
		'fli' 		=>	array('video/x-fli'),
		'asf' 		=>	array('video/x-ms-asf'),
		'asx' 		=>	array('video/x-ms-asx'),
		'wmv' 		=>	array('video/x-ms-wmv'),
		'wmx' 		=>	array('video/x-ms-wmx'),
		'wvx' 		=>	array('video/x-ms-wvx'),
		'avi' 		=>	array('video/x-msvideo'),
		'movie' 	=>	array('video/x-sgi-movie'),
		'mime' 		=>	array('www/mime'),
		'ice' 		=>	array('x-conference/x-cooltalk'),
		'vrm' 		=>	array('x-world/x-vrml'),
		'vrml' 		=>	array('x-world/x-vrml'),
		'ods'		=> 	array('application/vnd.oasis.opendocument.spreadsheet'),
		'odt'		=> 	array('application/vnd.oasis.opendocument.text'),
		'csv'		=> 	array('text/csv','application/vnd.ms-excel','text/comma-separated-values')
	); 
	
	
	
	function startup( &$controller ) {
		$this->controller = &$controller;
    }
	
	function set($var,$value){
		$this->$var = $value;
	}
	
	function get($var){
		return $this->$var;
	}
	
	function applySetting($setting){
		if (isset($setting['filename_type'])) $this->filename_type 	= $setting['filename_type'];
		if (isset($setting['filename_special'])) $this->filename_special 	= $setting['filename_special'];
		if (isset($setting['upload_path'])) $this->basicUploadPath 	= $setting['upload_path'];
		if (isset($setting['file_ext'])) $this->enabledExt 		= $setting['file_ext'];
		
		if (isset($setting['filename'])) $this->filename 		= $setting['filename'];
		if (isset($setting['tmp_name'])) $this->tmpFilename 		= $setting['tmp_name'];
		if (isset($setting['type'])) $this->file_type 		= $setting['type'];
		if (isset($setting['fn'])) $this->fn 		= $setting['fn'];
	
	}
	
	function file_ext_verify(){
		$ext = strtolower($this->getFileExt($this->filename));
		
		if (in_array($ext, $this->enabledExt)){
			if (isset($this->mime_types[$ext])){
				if (in_array($this->file_type, $this->mime_types[$ext])){
					return true;
				}
			}
		}
		return false;
	}
	
	function doit($setting = array()){
		$this->applySetting($setting);
		if (!$this->file_ext_verify()){
			die(json_encode(array('result'=>false,'message'=>'Nepovoleny format souboru: "'.$this->getFileExt($this->filename).'" - "'.$setting['type'].'"')));;
			return false;
		}
		
			
		switch ($this->filename_type){
			case 'unique':
				$this->outputFilename = $this->makeUniqueName();
				break;
			case 'alias':
				$this->outputFilename = $this->makeAliasName();
				break;
			case 'name':
				$this->outputFilename = $this->outputFilename;
				break;
			case 'special':
				$this->outputFilename = $this->filename_special;
				break;
			default:
				die('Nepovoleny typ filename');
		}
		//if ($setting['fn']) {
			
			$destination = '.'.$setting['upload_path'].$this->outputFilename;
			if (!@move_uploaded_file($this->tmpFilename,$destination))
				die(json_encode(array('result'=>false,'message'=>'Chyba uložení souboru na server')));
			
			die(json_encode(array('result'=>true,'uploaded_file'=>$this->outputFilename,'original_file'=>$setting['fn'],'db_file'=>$this->outputFilename.'|'.$setting['fn'])));

		//}		
	}
	
	function delete_file($setting = array()){	
		$this->applySetting($setting);
		//pr($setting);
		//list($file,$name) = explode('|',$setting['file']);
		$file = $setting['file']['file'];
		$name = $setting['file']['name'];
		if (!unlink('.'.$setting['upload_path'].$file))
			die(json_encode(array('result'=>false,'message'=>'Chyba smazání souboru')));
		die(json_encode(array('result'=>true,'message'=>'Smazano: '.$name)));
	}

	function makeUniqueName(){
		return uniqid(strtr($_SERVER['HTTP_HOST'],array('.'=>'_')).'_').'.'.$this->getFileExt($this->filename);
	}
	
	function makeAliasName(){
		$trans = array(","=>"","á"=>"a", "ä"=> "a", "c"=>"c", "d"=>"d", "é"=>"e", "e"=>"e", "ë"=>"e", "í"=>"i", "&#239;"=>"i", "n"=>"n", "ó"=>"o", "ö"=>"o", "r"=>"r", "š"=>"s", "t"=>"t", "ú"=>"u", "u"=>"u", "ü"=>"u", "ý"=>"y", "&#255;"=>"y", "ž"=>"z", "Á"=>"A", "Ä"=>"A", "C"=>"C", "D"=>"D", "É"=>"E", "E"=>"E", "Ë"=>"E", "Í"=>"I", "&#207;"=>"I", "N"=>"N", "Ó"=>"O", "Ö"=>"O", "R"=>"R", "Š"=>"S","T"=>"T", "Ú"=>"U", "U"=>"U", "Ü"=>"U", "Ý"=>"Y", "&#376;"=>"Y", "Ž"=>"Z"," "=>"-","/"=>"-","&"=>'-',"+"=>'-','"'=>'','?'=>'',';'=>'','*'=>'','%'=>'','"'=>'&#34','>'=>'&#62','<'=>'&#60');
		return strtolower(strtr($this->filename, $trans));
	}
	
	function getFileExt($file){
		$dots = explode('.',$file);
		return $dots[sizeOf($dots)-1];
	}
	
	
}
?>