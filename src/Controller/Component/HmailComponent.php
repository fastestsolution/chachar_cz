<?php

namespace App\Controller\Component;
use Cake\Controller\Component;
use Cake\ORM\Entity;
use Ddeboer\Imap\Server;
use Ddeboer\Imap\SearchExpression;
use Ddeboer\Imap\Search\Email\To;
use Ddeboer\Imap\Search\Email\FromAddress;
use Ddeboer\Imap\Search\Date\After;
use Cake\I18n\Time;

class HmailComponent extends Component
{

  public $connection;
  public $mailboxes;
  public $inbox;
  public $messages;

  public function connect($pass, Entity $config, $prot = 'imap'){
    if($prot == 'imap') {
      $server = new Server($config->imap_server);
    }
    $this->connection = $server->authenticate($config->email, $pass);
  }

  // @TODO není hotové
  public function getMailBoxes(){

  }

  public function getInbox(){
    $this->inbox = $this->connection->getMailbox("INBOX");
  }

  public function getMessages($mailBox, array $search = null){

    $condition = null;

    if(isset($search)){
      $condition = new SearchExpression();

      foreach($search as $k => $c){
        switch($k){
          case "to": $condition->addCondition(new To($c));
          case "from": $condition->addCondition(new FromAddress($c));
          case "dateFrom" : $condition->addCondition(new After(new Time($c)));
        }
      }
    }

    $this->messages = $this->inbox->getMessages($condition);
  }



}