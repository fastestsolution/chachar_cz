<?php

namespace App\Controller\Component;

use Cake\Mailer\Email;
use Cake\Controller\Component;
use Cake\ORM\Entity;
use App\View\Helper\FastestHelper;
/*
$data = [
	'Users_link'=>'fastest.cz',
];
$opt = [
	'to'=>$find->email,
	'fromEmail'=>'test2@fastest.cz',
	'fromName'=>'test',
	'subject'=>'test',
	'template_id'=>1,
	'data'=>$data,
	'template'=>'fastest',
];
$this->Email->send($opt);

{LOOP:people}
<b>{name}</b> {surname}<br />
{ENDLOOP:people}
				
*/

class EmailComponent extends Component
{
  public $fromEmail = "noreply@fastest.cz";
  public $fromName = "Pizza pro tebe";
	
	
  // enable debug template send
  var $debug_template = false;
  
  // set price format fields
  var $options = [
	'price_format'=>[
		'price_vat'
	]
  ];
  
  // send from template
  public function send(array $options = null){
    
	
	$this->controller = $this->_registry->getController();
	$options['server'] = $_SERVER['REQUEST_SCHEME'].'://'.$_SERVER['HTTP_HOST'];
	$options['podpis'] = $this->controller->setting['email_podpis'];
	if (isset($options['data'])){
		$this->data = $options['data'];
		
	}
	if (is_array($this->data)){
	
		$this->data['server'] = $options['server'];
		$this->data['podpis'] = $options['podpis'];
	}
	//pr($this->data);die();
	
	
	//pr($options);
    $this->options = array_merge_recursive($this->options,$options);
	//pr($this->options);die();
	
	if (!is_array($options['to'])){
		$options['to'] = [
			0=>$options['to'],
		];
	}
	foreach($options['to'] AS $to){
			
		
		
		$email = new Email();
		// odesilatel
		$email->from($this->fromEmail, $this->fromName);
		
		// prijemce
		$email->to($to)->emailFormat('html');
		
		// pokud je fromName
		if (isset($options['fromName'])){
			$email->from((isset($options['fromEmail'])?$options['fromEmail']:$this->fromEmail), $options['fromName']);
			
		}
		// load template from db
		if (isset($options['template_id'])){
			$this->loadTemplate($options['template_id']);
		}
		// subject email
		$email->subject((isset($this->template_data)?$this->template_data['subject']:$options['subject']));
		
		// pokud jsou data ze sablony
		if (isset($this->template_data['content'])){
			$options['data'] = $this->template_data['content'];
		}
		
		// predani promenych do view
		$email->viewVars(['css_styles'=>$this->controller->css_select,'data'=>(isset($options['data'])?$options['data']:'')]);
		//pr($this->controller->css_select);
		//pr($_SESSION['system_id']);die('a');
		
		
		// pokud je nastaven jiny template jinak pouzi template Templates/Email/html/fastest.ctp
		if(isset($options["template"])){
		  $email->template($options["template"]);
		} else {
		  $email->template('fastest');
		}
		//pr($options['to']);
		//pr($email);
		$email->send();
	
	
	}

  }
  // nahrazeni textu pres class TemplateEngine
  private function replaceText($content){
	if (isset($this->data)){
		$engine = new TemplateEngine();
		
		$template = $engine->process($content, $this->data,$this->options);
		
		return $template;
	}
  }
	  
  // nacteni z template ID	  
  private function loadTemplate($id){
	  $controller = $this->_registry->getController();
	  $controller->loadModel('EmailTemplates');
	  $template = $controller->EmailTemplates->find()
		->where(['id'=>$id])
		->select(['name','content','subject'])
		->hydrate(false)
		->first();
		if (!$template){
			die(json_encode(['r'=>false,'m'=>'Není definována emailová šablona']));
		}
	  $template['subject'] = $this->replaceText($template['subject']);
	  $template['content'] = $this->replaceText($template['content']);
	 
	  if ($this->debug_template == true){
	  
		pr($template);die();
	  }
	  $this->template_data = $template;
  }




}

/**
<h1>{subtitle}</h1>
{LOOP:people}
<b>{name}</b> {surname}<br />
{ENDLOOP:people}
**/
class TemplateEngine {
    function showVariable($name) {
        $this->els = explode('.',$name);
		
		$array_el = [];
		$this->value = [];
		
		
		
		// nalezeni dle pole napr. user.first_name
		$this->find_recursive($this->data);
		
		
		if (isset($this->value)){
			if (empty($this->value) && $this->value != 0){
				echo __('Nevyplněno');
			} else {
				// format price
				$this->price_format();
				echo ($this->value);
			}
			
		} else {
			echo '';
		}
		
    }
	function find_recursive($data){
		if(count($this->els) == 1){
			if (!isset($data[$this->els[0]])) {
				//pr($this->els);die('aa');
				unset($this->els);
			}
		}
		//pr($this->els);
		if (isset($this->els)){
		
			foreach ($this->els AS $el){
				
				if (isset($data[$el])) {
					if (is_array($data[$el])){
						$this->find_recursive($data[$el]);
						
					} else {
						$this->value = $data[$el];
						$this->key = $el;
					}
				}
			}
		} else {
			unset($this->value);
		}
	}
	
	function price_format(){
		$fastest = new FastestHelper(new \Cake\View\View());
		if (isset($this->options['price_format']) && !empty($this->options['price_format'])){
		
			if (in_array($this->key,$this->options['price_format'])){	
				$this->value = $fastest->price($this->value);

			}
		}
	}
	
    function wrap($element) {
        $this->stack[] = $this->data;
        foreach ($element as $k => $v) {
            $this->data[$k] = $v;
        }
    }
    function unwrap() {
        $this->data = array_pop($this->stack);
    }
    function run() {
        ob_start ();
        eval (func_get_arg(0));
        return ob_get_clean();
    }
    function process($template, $data,$options) {
        $this->options = $options;
        $this->data = $data;
        $this->stack = array();
        $template = str_replace('<', '<?php echo \'<\'; ?>', $template);
        
		$template = preg_replace('~\{(\w+)}~', '<?php $this->showVariable(\'$1\'); ?>', $template);
		$template = preg_replace('~\{(\w+[.]\w+)}~', '<?php $this->showVariable(\'$1\'); ?>', $template);
        $template = preg_replace('~\{LOOP:(\w+)\}~', '<?php foreach ($this->data[\'$1\'] as $ELEMENT): $this->wrap($ELEMENT); ?>', $template);
        $template = preg_replace('~\{ENDLOOP:(\w+)\}~', '<?php $this->unwrap(); endforeach; ?>', $template);
        $template = '?>' . $template;
		//pr($template);
		return $this->run($template);
    }
}