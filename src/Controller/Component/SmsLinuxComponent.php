<?php 
namespace App\Controller\Component;
define('BAID', 'demo');
// klientsky certifikat slouzici pro autentizaci
define('CERT', 'demo.pem');
// certifikat autority RapidSSL, ktera vydala serverovy certifikat
define('CACERT', 'geotrust.crt');
// URL adresa HTTP GET/POST API (LinuxBox.cz SMS brana)
define('URL', 'https://www.ipsms.cz:8443/smsconnector/getpost/GP');

use Cake\Mailer\Email;
use Cake\Controller\Component;
use Cake\ORM\Entity;
use Cake\I18n\Date;
use Cake\I18n\Time;

class SmsLinuxComponent extends Component
{    var $text_body 		= null;
    var $to 			= null;
    var $controller;
		
	var $baid = 'chachar';
	var $cert_path = './uploaded/linuxbox/';
	var $cert_autorita = './uploaded/linuxbox/geotrust.crt';
	var $data = array();

    function startup( &$controller ) {
		$this->controller = &$controller;
			//$htmlRequest = new HTTP_Request();
		
	}

    
	function send($data=array()){
		$this->data = $data;
		$cert_list = array(
			-1 => 'pospiech07', // SUperadmin  778421701 - 775426281 
			//1 => 'pospiech07', // Sznapka Michal  778421701 - 775426281
			
			148 => 'pospiech01', // kaleta 731335908 - 770110970 
			24 => 'pospiech02', // heblak 739379005 - 770110971 
			145 => 'pospiech03', // Malik 734170443 - 770110972 
			149 => 'pospiech04', // Kube Jan  731644317 - 770110973 
			150 => 'pospiech05', // Tesarčík  Radek  733531318 - 770110974 
			26 => 'pospiech06', // Steiger David  739252103 - 770110975 
			
			25 => 'pospiech07', // Sznapka Michal  778421701 - 775426281  
			27 => 'pospiech08', // Mašlík Zbyněk¨  731602117 - 775426287  
			111 => 'pospiech09', // Jadrný Libor  733531316 - 775426291 
			157 => 'pospiech10', // Pospiech David  603578571 - 775426292  
 
			
		);
		if (!isset($cert_list[$this->data['user_id']])){
			die(json_encode(['r'=>false,'m'=>'neni user id']));
		}
		$this->user_id = $this->data['user_id'];
		$this->baid = $cert_list[$this->data['user_id']];
		$this->cert_path = $this->cert_path.$cert_list[$this->data['user_id']].'.pem';
		
		$result = $this->smsgw('ping');
		if ($result['code'] !== 'ISUC_000') {
			echo "<strong>ERROR: " . $result['description'] . "</strong>\n";
			exit();
		}
		if (isset($this->data['confirm']) && $this->data['confirm'] == true){
			$this->dorucenka();
		
		}else if (isset($this->data['receive']) && $this->data['receive'] == true){
			
			$this->getSms();
		
		} else {
			$this->send_sms();
		
		}
		//$this->dorucenka();
		//$this->confirm();
		//pr($this->return_result);
		//pr($data);
		return $this->return_result;
	}
	
	function save_sms($result,$doruceno=null){
		$this->controller->loadModel('SmsClient');		
		
		$save_data = array(
			'user_id'=>$this->data['user_id'],
			'phone'=>$this->data['phone'],
			'message'=>$this->data['message'],
			'order_id'=>$this->data['order_id'],
			'msg_id'=>$result['refMsgID'],
		);
		//pr($result);
		//pr($doruceno);
		
		if ($doruceno != null){
			$id = $this->controller->SmsClient->find('first',array('conditions'=>array('msg_id'=>$doruceno)));
			
			$save_data = am($save_data,array(
				'id'=>$id['SmsClient']['id'],
				'doruceno'=>1,
			));
			//pr($save_data);die('aaa save');
			if ($id == ''){
				return false;
			}
		}
		
		
		$this->controller->SmsClient->save($save_data);
		$this->save_id = $this->controller->SmsClient->id;
		$this->controller->SmsClient->id = null;
		
		
	}
	
	function send_sms(){
		$controller = $this->_registry->getController();
		$controller->loadModel('SmsLinuxs');
		$save_sms = $controller->SmsLinuxs->newEntity([
			'type'=>1,
			'cert'=>$this->baid,
			'text'=>$this->data['message'],
			'phone'=>(isset($this->data['phone'])?$this->data['phone']:''),
			'user_id'=>$this->user_id,
			'driver_id'=>(($this->data['driver_id'])?$this->data['driver_id']:''),
		]);
		$controller->SmsLinuxs->save($save_sms);
		
		
		
		
		// send - poslani SMS
		$data = array(
			// cilove cislo
			'toNumber'       => $this->data['phone'],
			// text SMS
			'text'           => $this->data['message'],
			// standardni SMS
			'intruder'       => false,
			// s dorucenkou
			'deliveryReport' => true,
			// delsi SMS se rozdeli na vice casti
			'multipart'      => true
		);
		//pr($data);die();
		$result = $this->smsgw('send', $data);
		
		if (!empty($result) && $result['code'] == 'ISUC_001'){
			//$this->save_sms($result);
			$this->return_result = array('result'=>true,'message'=>'SMS byla odeslána na číslo '.$this->data['phone']);
			//pr($result);
			
			//$this->dorucenka($result['refMsgID']);

		} else {
			$this->return_result = array('result'=>false,'message'=>'Chyba odeslaní na SMS bránu');
			
		}
		
	}
	
	function getSms($msg_id=null){
		$data = array(
			'baID' => $this->baid,
		);
		$result = $this->smsgw('receive', $data);
		$controller = $this->_registry->getController();
		$controller->loadModel('SmsLinuxs');
		
		$controller->loadModel('Users');
		$this->user_list = $controller->Users->userList();
		
		
		if (!empty($result)){
			
			//pr($result);
			if (isset($result['code']) && $result['code'] == 'ISUC_005'){
				$sms_text_desc = $result['description'];
			}
			if (isset($result['selector']) && $result['selector'] == 'TextSms'){
				$sms_text = $result['text'];
				//pr($result);
				//die('a');
			}
			if (isset($sms_text)){
				$controller->loadModel('Phones');
				$find_phone = $controller->Phones->find()
				->where(['name'=>strtr($result['fromNumber'],['+420'=>''])])
				->select([])
				->limit(10)
				->order('id DESC')
				->first();
				//pr($result);
				//pr($find_phone->user_id);
				//die();
				$save_sms = $controller->SmsLinuxs->newEntity([
					'type'=>2,
					'cert'=>$result['baID'],
					'text'=>$sms_text,
					'phone'=>(isset($result['fromNumber'])?$result['fromNumber']:''),
					'created'=>new Time(substr($result['msgID'],0,19)),
					'user_id'=>$this->user_id,
					'driver_id'=>(($find_phone)?$find_phone->user_id:''),
				]);
				$controller->SmsLinuxs->save($save_sms);
			}	
			$data = array(
				'refMsgID' => $result['msgID'],
				'refBaID'  => $result['baID']
			);
			$result = $this->smsgw('confirm', $data);
		}
		if ($this->user_id == -1){
			$conditions['user_id IN'] = [25,-1];
		}else if ($this->user_id == 25){
			$conditions['user_id IN'] = [25,-1];
		} else {
			$conditions['user_id'] = $this->user_id;
			
		}
		$conditions['type'] = 2;
		$sms_data = $controller->SmsLinuxs->find()
			->where($conditions)
			->select([])
			->limit(10)
			->order('id DESC')
			->toArray();
			
		$sms_list = [];
		if ($sms_data){
			foreach($sms_data AS $d){
				$sms_list[] = [
					'date'=>$d->created->format('d.m. Y H:i:s'),
					'tel'=>$d->phone,
					'user_name'=>(isset($this->user_list[$d->driver_id])?$this->user_list[$d->driver_id]:''),
					'text'=>$d->text,
					'id'=>$d->id,
					'done'=>(($d->done == 0)?'button':'none'),
				];
			}
		}
		//pr($sms_list);
		
		//pr($result);
		die(json_encode(['r'=>true,'sms_list'=>$sms_list]));
	}
	
	function dorucenka($msg_id=null){
				
		while (true) {
			// receive - precteni dorucenky nebo prichozi SMS
			$result = $this->smsgw('receive');
			//pr('res dorucenka');
			//pr($result);
			if (empty($result)) {
				$this->return_result = array('result'=>false,'message'=>'Není žádná doručenka');
				
				break;
			}
			//echo "<h2>receive</h2>\n";
			//pr($result);
			//die();
			$return_result = array('result'=>false,'message'=>'Nedoručeno');
			if ($result['selector'] === 'Response') {
				// dorucenka
				if ($result['code'] === 'ISUC_005') {
					//pr($result);
					$this->save_sms($result,$result['refMsgID']);
					$return_result = array('result'=>true,'message'=>'SMS byla doručena');
					//echo "<strong>Zprava byla dorucena - ID: " . $result['msgID'] . "</strong>\n";
				}
			} else if ($result['selector'] === 'TextSms') {
				// prichozi zprava
				//echo "<strong>Prichozi SMS od " . $result['fromNumber'] .", text zpravy: " . $result['text'] . "</strong>\n";
			}
					
			// confirm - potvrzeni, ze byla zprava systemem zpracovana
			// a muze se pri pristim volani receive zobrazit dalsi zprava v poradi 
			
			$data = array(
				'refMsgID' => $result['msgID'],
				'refBaID'  => $result['baID']
			);
			$result = $this->smsgw('confirm', $data);
			//echo "<h2>confirm</h2>\n";
			//pr($result);
			
			if ($result['code'] !== 'ISUC_002') {
				echo "<strong>ERROR: " . $result['description'] . "</strong>\n";
				exit();
			}
			
			$this->return_result = $return_result;
		}
	
	}
	
		
	function smsgw($action = 'ping', $data = null) {
		$postData = array(
			'baID'   => $this->baid,
			'action' => $action
		);
		if (isset($data)) {
			$postData = array_merge($postData, $data);
		}
		//pr($postData);
		$postData = http_build_query($postData);
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, URL);
		curl_setopt($ch, CURLOPT_POST, TRUE);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, true);
		curl_setopt($ch, CURLOPT_CAINFO, $this->cert_autorita);
		curl_setopt($ch, CURLOPT_SSLCERT, $this->cert_path);
		$response = curl_exec($ch);
		
		
		$output = array();
		$arr = explode("\n", $response);
		foreach ($arr as &$line) {
			$line = trim($line);
			if (!empty($line)) {
				list($key, $value) = explode('=', $line, 2);
				$output[$key] = $value;
			}
		}
		curl_close($ch);
		
		return $output;
	}

}
?>