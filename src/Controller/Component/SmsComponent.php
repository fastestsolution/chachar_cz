<?php

namespace App\Controller\Component;

use Cake\Mailer\Email;
use Cake\Controller\Component;
use Cake\ORM\Entity;


class SmsComponent extends Component
{

	function send($user, $recipient_list = array(), $message = ''){
				$output = array();
				//$message = 'Vaše objednávka je na ceste.';
				foreach($recipient_list as $phone_number){
					$phone_number = strtr($phone_number,['+'=>'']);
					//pr($phone_number);die();
					$SMS = '<?xml version="1.0" encoding="utf-8" ?> <batch id=""> <request>textSMS</request>  <recipient>+'.$phone_number.'</recipient> <content>'.$message.'</content>  <udh />  <delivery_report>0</delivery_report><custid>Chachar</custid>   </batch>'; 			
					$context = stream_context_create( array( 'http' => array( 'method'=>"POST", 'Host' => 'apiserver', 'header'=>"Content-Type: text/xml", 'content'=>$SMS ) ) );
					//pr($SMS);
					//die();
								
					$file = @file_get_contents('http://directsmsapi.sluzba.cz/apifastest/receiver.asp', false, $context);
					if (!$file){
						$output = array('r'=>false,'m'=>'Chyba odeslani '.$phone_number, 'phone'=>$phone_number);
						return $output;
					}
					$doc = simplexml_load_string($file);
					//pr($doc);
					switch ($doc->status){
						case '200':
							$output[] = array('r'=>true,'m'=>'SMS odeslána na tel. '.$phone_number);
							break;
						case '400':
							$output[] = array('r'=>false,'m'=>'Neplatné telefonní císlo', 'phone'=>$phone_number);
						default:
							$output[] = array('r'=>false,'m'=>'Neošetrená chyba pri odesílání SMS', 'phone'=>$phone_number);
							break;
					}
				}
				//pr($output);
				if (count($output) == 1){
					$output = $output[0];
				}
				return $output;
	}

}