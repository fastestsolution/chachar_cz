<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

class Driver extends Entity
{


	protected function _getVek(){
		$vek = floor((date("Ymd") - date("Ymd", strtotime($this->_properties['datum_narozeni']))) / 10000);
		return $vek ;
	}

}
