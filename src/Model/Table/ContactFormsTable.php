<?php
namespace App\Model\Table;

use App\Model\Entity\Project;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Utility\Hash;
use Cake\I18n\Time;

class ContactFormsTable extends Table
{

  public function initialize(array $config)
  {
	  
    parent::initialize($config);
	
	$this->addBehavior('Timestamp');
  }
  
  public function validationDefault(Validator $validator)
	{
		$validator
		->notEmpty('email',__("Musíte zadat email"))
		->add('email', 'validFormat', [
			'rule' => 'email',
			'message' => __('Email není ve správném formátu')
		])
		->notEmpty('jmeno',__("Musíte zadat Vaše jméno"))
		->notEmpty('text_tmp',__("Musíte zadat Vaši zprávu"))
		
		;
		return $validator;
	}

  

}
