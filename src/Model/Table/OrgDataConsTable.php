<?php

namespace App\Model\Table;

use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;


class OrgDataConsTable extends Table
{
     
    public function initialize(array $config)
    {
        parent::initialize($config);
        $this->table('fastest__rozvoz_food_connections');
    }
    
    public static function defaultConnectionName() {
        return 'old';
    }
      
}