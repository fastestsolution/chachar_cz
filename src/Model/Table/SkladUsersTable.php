<?php

namespace App\Model\Table;

use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;


class SkladUsersTable extends Table
{
     
    public function initialize(array $config)
    {
        parent::initialize($config);
		$this->table('fastest__sklad_users');
		
    }
    
    public static function defaultConnectionName() {
        return 'sklad';
    }
      
}