<?php
namespace App\Model\Table;

use App\Model\Entity\Project;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Utility\Hash;
use Cake\I18n\Time;

class MenuItemsTable extends Table
{

  public function initialize(array $config)
  {
	  
    parent::initialize($config);
	
	$this->addBehavior('Timestamp');
  }
	
	
	// get full path
	public function getFullPath($type=null,$prefix=null){
		$MenuItems = TableRegistry::get("MenuItems");
		$this->prefix = $prefix;
		$this->path_list = [];
		$this->path_list_google = [];
		
			$query = $this->find('threaded')
				->where([])
				->select([
					'id',
					'name',
					'alias',
					'level',
					'parent_id',
					'lft',
					'rght',
					'spec_url',
				])
				->cache(function ($query) {
					return 'path_data_menu_items-' . md5(serialize($query->clause('where')).serialize($_SESSION['lang']));
				});
			
			$path_data = $query->toArray();
		
		if ($path_data){
			$this->genPath($path_data);
		}
		//pr($path_data);
		if ($type == 'google'){
			return $this->path_list_google;  
			
		} else {
			return $this->path_list;  
			
		}
		//pr($this->path_list_google);
		return $this->path_list;  
	}	
  
	// gen path recursive
	function genPath($data,$parent=null){
			
		foreach($data AS $d){
			
			
			if (empty($d->spec_url)){
				$url = (($parent != null)?$this->path_list[$parent]:'/').$d->alias.'/';
				$url_google = (($parent != null)?$this->path_list_google[$parent]['url']:'/').(($parent == null)?$this->prefix:'').$d->alias.'/';
			} else {
				$url = $url_google = $d->spec_url;
			}
			$this->path_list[$d->id] = $url;
			$this->path_list_google[$d->id] = [
				'url'=>$url_google,
				'modified'=>$d->modified,
			];
			if (!empty($d->children)){
				$this->genPath($d->children,$d->id);
			}
		}
	}
  
  

  

}
