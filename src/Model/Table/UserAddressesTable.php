<?php
namespace App\Model\Table;

use App\Model\Entity\Project;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Utility\Hash;
use Cake\I18n\Time;
use Cake\Network\Session;

class UserAddressesTable extends Table
{
	

  public function initialize(array $config)
  {
	  
    parent::initialize($config);
	// define table
	$this->table('shop_client_addresses');
	
	$this->addBehavior('Timestamp');
  }
  
  
  
	public function beforeSave($event){
		//$event->data['entity']["name"] = $event->data['entity']['prijmeni'].' '.$event->data['entity']['jmeno'];
		return $event;
	}

	public function validationDefault(Validator $validator)
	{
		//pr($validator);
		$validator
		->notEmpty('street',__("Musíte zadat ulici"))
		->notEmpty('city',__("Musíte zadat město"))
		//->notEmpty('zip',__("Musíte zadat PSČ"))
		;
		return $validator;
	}
	
	public function validationOnlyCheck($validator) {
        $validator = $this->validationDefault($validator);
		//pr($validator);
		$validator->remove('street');
		$validator->remove('city');
		//$validator->remove('zip');
		
        return $validator;
	}

  

}
