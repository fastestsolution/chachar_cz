<?php

namespace App\Model\Table;

use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;


class OldUsersTable extends Table
{
     
    public function initialize(array $config)
    {
        parent::initialize($config);
		$this->table('fastest__rozvoz_clients');
		
    }
    
    public static function defaultConnectionName() {
        return 'central';
    }
      
}