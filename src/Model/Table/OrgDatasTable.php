<?php

namespace App\Model\Table;

use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;


class OrgDatasTable extends Table
{
     
    public function initialize(array $config)
    {
        parent::initialize($config);
		$this->hasMany('OrgDataCons',[
			'foreignKey' => 'rozvoz_food_id',
		]);
        $this->table('fastest__rozvoz_foods');
		
    }
    
    public static function defaultConnectionName() {
        return 'old';
    }
      
}