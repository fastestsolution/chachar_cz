<?php
namespace App\Model\Table;

use App\Model\Entity\Project;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Utility\Hash;
use Cake\I18n\Time;

class ArticlesTable extends Table
{

  public function initialize(array $config)
  {
	  
    parent::initialize($config);
	
	$this->addBehavior('Timestamp');
  }

  
	// generate list from threaded
	public function findCatsId($path,$data){
		$ids = [];
		foreach($path AS $k=>$p){
			$ids[] = array_search($p,$data);
			
		}
		if (empty($ids[0])){
			$ids = '';
		}
		return $ids;
		
	}
  
	// generate list from threaded
	public function menuItems($path,$data){
		$this->menu_items_list = [];
		$this->menu_items_list_name = [];
		
		$this->generateList($data);
		$ids_cats = $this->findCatsId($path,$this->menu_items_list);
		if (!empty($ids_cats)){
		
		
		rsort($ids_cats);
		//pr($ids_cats);die();
		$breadcrumb = [];
		foreach($ids_cats AS $i){
			$breadcrumb[$i] = $this->menu_items_list_name[$i];
		}
		//pr($this->shop_menu_items_list_name);
		//pr($breadcrumb);die();
		$ids_menu = $this->getConnectionList($ids_cats);
		
		
		$result = [
			'menu_items_list_name'=>$this->menu_items_list_name,
			'menu_list'=>$this->menu_items_list,
			'ids'=>$ids_menu,
			'breadcrumb'=>$breadcrumb,
		]; 
		} else {
			$result = '';
		}
		return $result;
	}
	
	
	public function generateList($data){
			foreach($data AS $d){
				$this->menu_items_list[$d->id] = $d->alias;
				$this->menu_items_list_name[$d->id] = $d->name;
				if (isset($d->children)){
					$this->generateList($d->children);
				}
			}	
	}
	
	
	
	// ziskani product id z connection dle cat_id
	public function getConnectionList($cat_id){
		$ArticleMenuItems = TableRegistry::get("ArticleMenuItems");
		
		$conditions = ['menu_item_id IN'=>$cat_id];
		
			$query = $ArticleMenuItems->find('list',[
				'keyField' => 'article_id',
				'valueField' => 'menu_item_id'
			]);
			$query->where($conditions);
			$query->cache(function ($query) {
				return 'menu_items_-' . md5(serialize($query->clause('where')).serialize($_SESSION['lang']));
			});
			$data_load = $query->toArray();
		
		$data = [];
		foreach($data_load AS $k=>$d){
			$data[] = $k;
		}
		//$data = array_flip($data);
		//pr($data);
		return $data;  
	}	
	
	
  

}
