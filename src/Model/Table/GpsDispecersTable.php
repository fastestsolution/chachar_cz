<?php
namespace App\Model\Table;

use App\Model\Entity\Project;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Utility\Hash;
use Cake\I18n\Time;

class GpsDispecersTable extends Table
{

  public function initialize(array $config)
  {
	  
    parent::initialize($config);
	$this->belongsTo('Rozvozces');
	$this->hasMany('MobileOrders',['foreignKey' => 'rozvozce_id','bindingKey'=>'rozvozce_id','conditions'=>['stav_id IN'=>[1,2,3]]]);
	$this->addBehavior('Timestamp');
  }
  
  
	
  

  

}
