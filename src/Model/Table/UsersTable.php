<?php
namespace App\Model\Table;

use App\Model\Entity\Project;
use Cake\ORM\Entity;
use Cake\ORM\Query;
use Cake\ORM\RulesChecker;
use Cake\ORM\Table;
use Cake\Validation\Validator;
use Cake\ORM\TableRegistry;

use Cake\Utility\Hash;
use Cake\I18n\Time;
use Cake\Network\Session;

class UsersTable extends Table
{
	

  public function initialize(array $config)
  {
	  
    parent::initialize($config);
	
	$this->hasMany("UserAddresses");
	
	// define table
	$this->table('shop_clients');
	
	$this->addBehavior('Timestamp');
  }
	/*
	public static function defaultConnectionName() {
        return 'central';
    }
	*/
	
  
  
  
	public function beforeSave($event){
		$event->data['entity']["name"] = $event->data['entity']['prijmeni'].' '.$event->data['entity']['jmeno'];
		return $event;
	}
	
	public function getUser($id){
		$Users = TableRegistry::get("Users");
		$find = $Users->find()
			->where(['id'=>$id])
			->hydrate(false)
			->first();
			//pr($find);
			unset($find['password']);
		return $find;
	}

	public function loadPhonePref($data){
		if (isset($data->phone_pref)){
			$data->phone = $data->phone;
			
		}
		if (isset($data['phone_pref'])){
			//$data['phone'] = strtr($data['phone_pref'],['004'=>'']).$data['phone'];
			
		//	$data['phone_pref'] = '+'.$data['phone_pref'];
			//$data['phone_pref'] = strtr($data['phone_pref'],['+420'=>'']);
			//$data['phone_pref'] = strtr($data['phone_pref'],['00'=>'']);
			//pr($data['phone_pref']);
		}
		
		return $data;
	}

	public function savePhonePref($data){
		if (isset($data['phone']))
			//$phone = explode(' ',$data['phone']);
			/*
			if (!isset($phone[2]) || !isset($phone[3])){
				die(json_encode(['r'=>false,'m'=>__('Telefon je ve špatném formátu')]));
			}
			*/
			//$data['phone'] = $phone[1].$phone[2].$phone[3];
			//$data['phone_pref'] = strtr($phone[0],['('=>'',')'=>'','+'=>'00']);
			//$data['phone_pref'] = strtr($data['phone_pref'],['('=>'',')'=>'','+'=>'']);
			//$data['phone_pref'] = '00'.$data['phone_pref'];
		return $data;
	}

	public function validationDefault(Validator $validator)
	{
		$validator
		->notEmpty('email',__("Musíte zadat email"))
		->add('email', 'validFormat', [
			'rule' => 'email',
			'message' => __('Email není ve správném formátu')
		])
		//->add("email", "unique", ['rule' => 'validateUnique', 'provider' => 'table', 'message' => __("Tuto emailovou adresu již někdo používá, můžete se přihlásit")]) 
		
		->requirePresence('password', 'create',  __("Musíte vyplnit heslo"))
		->notEmpty('password',__("Musíte vyplnit heslo"))
		->add('password', 'passwordsEqual', [
		  'rule' => function ($value, $context) {
			return
			  isset($context['data']['password2']) &&
			  $context['data']['password2'] === $value;
		  },
		  'message' => __("Hesla se navzájem neshodují")
		])
		->notEmpty('first_name',__("Musíte zadat jméno"))
		->notEmpty('last_name',__("Musíte zadat příjmení"))
		->notEmpty('phone',__("Musíte zadat telefon"))
		->add('phone', [
			'length' => [
				'rule' => ['minLength', 9],
				'message' => 'Telefon musí mít 9 číslic a mezinárodní kód země',
			]
		])
		->notEmpty('phone_pref',__("Musíte zadat předvolací znak země např. +420"))
		->add('phone_pref', [
			'length' => [
				'rule' => ['minLength', 3],
				'message' => 'Předvolací znak musí mít formát např +420',
			]
		])
		;
		return $validator;
	}
	
	public function validationOnlyCheck($validator) {
        $validator = $this->validationDefault($validator);
		//pr($validator);
		$validator->remove('email','unique');
		
        return $validator;
	}

	public function validationOnlyCheckPass($validator) {
        $validator = $this->validationDefault($validator);
		//pr($validator);
		$validator->remove('email','unique');
		$validator->remove('password');
		
        return $validator;
	}

	public function validationStep1($validator) {
        $validator = $this->validationDefault($validator);
		//$validator->remove('first_name');
        return $validator;
	}

  

}
