<?php

namespace App\Model\Table;

use Cake\Utility\Text;
use Cake\Event\Event;
use Cake\ORM\Table;


class MobileOrdersTable extends Table
{
     
    public function initialize(array $config)
    {
        parent::initialize($config);
		$this->hasMany('MobileOrderItems');
    	$this->addBehavior('Timestamp');
    }
    
    
      
}