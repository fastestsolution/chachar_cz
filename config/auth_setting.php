<?php
$auths = ([
  'Auths' => [
    'all' => 'all',
    'users' => [
      'index',
      'add',
      'edit',
      'delete',
      'editauth'
    ]
  ],
  'AuthNames' => [
    'controller' => [
      'users' => __('uživatelé'),
      'all' => __("všechna oprávnění"),
    ],
    'action' => [
      'all' => __('všechna oprávnění'),
      'index' => __('zobrazit'),
      'add' => __('vložit'),
      'edit' => __("upravit"),
      'delete' =>  __('smazat'),
      'editauth' => __("udělovat oprávnění"),
    ]
  ]
]);
