<?php
define('article_prefix','/text/');
define('article_detail','detail/');
define('registrace_link','registrace');
define('heslo_link','zapomenute-heslo');
define('heslo_reset_link','reset-password');
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\Router;

use Cake\Core\Configure;
/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass('DashedRoute');


Router::scope('/', function ($routes) {

    $routes->connect('/', ['controller' => 'Pages', 'action' => 'homepage']);

    $routes->connect('/admin/:action/*', ['controller' => 'ChacharAdmins']);
    $routes->connect('/chatbot/:action/*', ['controller' => 'Chatbots']);
    
	
	$routes->connect('/users/:action/*', ['controller' => 'Users']);
    //$routes->connect('/ares/*', ['controller' => 'Pages', 'action' => 'display']);
	$routes->connect('/check_orders_client/*', ['controller' => 'Pages', 'action' => 'findClient']);
	$routes->connect('/check_orders_login/*', ['controller' => 'Pages', 'action' => 'checkOrdersLogin']);
	$routes->connect('/check_orders_logout/*', ['controller' => 'Pages', 'action' => 'checkOrdersLogout']);
	$routes->connect('/check_orders/*', ['controller' => 'Pages', 'action' => 'checkOrders']);
	$routes->connect('/intranet/*', ['controller' => 'Pages', 'action' => 'intranet']);
	$routes->connect('/searchAddress/*', ['controller' => 'Pages', 'action' => 'searchAddress']);
	$routes->connect('/search_google_gps/*', ['controller' => 'Pages', 'action' => 'searchGoogleGps']);
	$routes->connect('/gps/*', ['controller' => 'Mobiles', 'action' => 'logGps']);
	$routes->connect('/provoz_gps_map/*', ['controller' => 'Mobiles', 'action' => 'getMap']);
	$routes->connect('/load_intro/*', ['controller' => 'Pages', 'action' => 'load_intro']);
	$routes->connect('/mobiles/:action/*', ['controller' => 'Mobiles', ]);
	$routes->connect('/mobile/*', ['controller' => 'Mobiles', 'action' => 'index']);
	$routes->connect('/mobile_test/*', ['controller' => 'Mobiles', 'action' => 'test']);
	$routes->connect('/saveGps/*', ['controller' => 'Mobiles', 'action' => 'saveGps']);
	$routes->connect('/mobile_add/*', ['controller' => 'Mobiles', 'action' => 'rozvoceAdd']);
	$routes->connect('/mobile_vylozeno/*', ['controller' => 'Mobiles', 'action' => 'vylozeno']);
	$routes->connect('/mobile_nevylozeno/*', ['controller' => 'Mobiles', 'action' => 'nevylozeno']);
	$routes->connect('/mobile_vezu/*', ['controller' => 'Mobiles', 'action' => 'mobileVezu']);
	$routes->connect('/login/*', ['controller' => 'Users', 'action' => 'login']);
	$routes->connect('/chatbot/login/*', ['controller' => 'Users', 'action' => 'login']);
	$routes->connect('/logout/*', ['controller' => 'Users', 'action' => 'logout']);
	$routes->connect('/'.registrace_link.'/*', ['controller' => 'Users', 'action' => 'registrace']);
	$routes->connect('/chatbot/register/*', ['controller' => 'Users', 'action' => 'registrace']);
	$routes->connect('/'.heslo_link.'/*', ['controller' => 'Users', 'action' => 'zapomenute_heslo']);
	$routes->connect('/'.heslo_reset_link.'/*', ['controller' => 'Users', 'action' => 'reset_heslo']);
	$routes->connect('/sitemap.xml/*', ['controller' => 'Pages', 'action' => 'google_sitemap']);

	$routes->connect('/pages/:action/*', ['controller' => 'Pages']);
	$routes->connect('/fst_upload/*', ['controller' => 'Pages', 'action' => 'fstUpload']);
	$routes->connect('/clear_cache/*', ['controller' => 'Pages', 'action' => 'clearCache']);
	$routes->connect('/system_list/*', ['controller' => 'Pages', 'action' => 'systemList']);
	$routes->connect('/newsletter/*', ['controller' => 'Pages', 'action' => 'saveNewsletter']);
	$routes->connect('/loadGoogleMap/*', ['controller' => 'Pages', 'action' => 'loadGoogleMap']);
	$routes->connect('/loadContactForm/*', ['controller' => 'Pages', 'action' => 'loadContactForm']);
	$routes->connect('/saveContactForm/*', ['controller' => 'Pages', 'action' => 'saveContactForm']);

	$routes->connect('/test_email/*', ['controller' => 'Pages', 'action' => 'test_email']);

	$routes->connect(article_prefix.article_detail.'/*', ['controller' => 'Articles', 'action' => 'detail']);
	$routes->connect(article_prefix.'*', ['controller' => 'Articles', 'action' => 'index']);
  
	$routes->connect('/', ['controller' => 'Pages', 'action' => 'homepage']);
    $routes->fallbacks('DashedRoute');
});

$scopes = function ($routes) {
	$routes->connect('/'.registrace_link.'/*', ['controller' => 'Users', 'action' => 'registrace']);
	$routes->connect('/'.heslo_link.'/*', ['controller' => 'Users', 'action' => 'zapomenute_heslo']);
	$routes->connect('/'.heslo_reset_link.'/*', ['controller' => 'Users', 'action' => 'reset_heslo']);
	$routes->connect(article_prefix.article_detail.'/*', ['controller' => 'Articles', 'action' => 'detail']);
	$routes->connect(article_prefix.'*', ['controller' => 'Articles', 'action' => 'index']);
  
	$routes->connect('/', ['controller' => 'Pages', 'action' => 'homepage']);
    
};

$languages = Configure::read('languages_list');
foreach ($languages as $lang) {
	Router::scope("/$lang", ['lang' => $lang], $scopes);
}
	
Plugin::routes();
